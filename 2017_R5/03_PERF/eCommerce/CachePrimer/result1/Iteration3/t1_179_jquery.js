/*
* rwdImageMaps jQuery plugin v1.4
*
* Allows image maps to be used in a responsive design by recalculating the area coordinates to match the actual image size on load and window.resize
*
* Copyright (c) 2012 Matt Stow
* https://github.com/stowball/jQuery-rwdImageMaps
* http://mattstow.com
* Licensed under the MIT license
*/
;(function(a){a.fn.rwdImageMaps=function(){var d=this,c=parseFloat(a.fn.jquery);var b=function(){d.each(function(){if(typeof(a(this).attr("usemap"))=="undefined"){return}var f=this,e=a(f);a("<img />").load(function(){var o,k,i="width",n="height";if(c<1.6){o=f.getAttribute(i),k=f.getAttribute(n)}else{o=e.attr(i),k=e.attr(n)}if(!o||!k){var p=new Image();p.src=e.attr("src");if(!o){o=p.width}if(!k){k=p.height}}var g=e.width()/100,l=e.height()/100,j=e.attr("usemap").replace("#",""),m="coords";a('map[name="'+j+'"]').find("area").each(function(){var s=a(this);if(!s.data(m)){s.data(m,s.attr(m))}var r=s.data(m).split(","),q=new Array(r.length);for(var h=0;h<q.length;++h){if(h%2===0){q[h]=parseInt(((r[h]/o)*100)*g)}else{q[h]=parseInt(((r[h]/k)*100)*l)}}s.attr(m,q.toString())})}).attr("src",e.attr("src"))})};a(window).resize(b).trigger("resize");return this}})(jQuery);