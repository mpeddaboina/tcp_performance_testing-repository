/*
* Skeleton V1.1
* Copyright 2011, Dave Gamache
* www.getskeleton.com
* Free to use under the MIT license.
* http://www.opensource.org/licenses/mit-license.php
* 8/17/2011
*/
var isAutoClose=false;
var pointHistoryClicked = false;
//<!-- info modals -->    
function initLinkClick(){
	$('a.openModalMyPlaceEdit').each(function() {
		var $link = $(this);
		$link.click(function() {
			$("#tcpEditEmployeeFormDiv").html("");
			isAutoClose=false;
			if($link.attr("id")=="TcpRemoveEmployeeId"&&!($("#employee").is(":checked"))){
				if($("#tcpEmployeeId").html().Trim()==""){
					return false;
				}
			}else{
				if($("#tcpEmployeeId").html().Trim()==""){
					$("#employee").removeAttr("checked");
					$(".id-num").hide();
				}else{
					if ($("#employee").is(':checked') ) {
						$(".id-num").show();
					}
				}
				setCurrentId($link.attr('id'));
			}
			if (($link.attr("id")=="TCPMyAddressBook_shipping_method") && ($('#WC_TCPMyAddressBook_countryInDefaultAddress').val() == "US")) {
				if   (!$('#WC_TCPMyAddressBook_shipModeCodeStringForUSStateInDefaultAddress').val()) {
					// calculate ship mode code string
					
					var newShipModeCodeString = CheckoutHelperJS.shipCodeStringForUSState(
						$('#WC_TCPMyAddressBook_address1InDefaultAddress').val(),
						$('#WC_TCPMyAddressBook_address2InDefaultAddress').val(),
						$('#WC_TCPMyAddressBook_stateOrProvinceInDefaultAddress').val());
					$('#WC_TCPMyAddressBook_shipModeCodeStringForUSStateInDefaultAddress').val(newShipModeCodeString);
				}
			
				var curhref = $link.attr("href");
				var shipModeCodeStringSuffix = "&shipCodeString=" + $('#WC_TCPMyAddressBook_shipModeCodeStringForUSStateInDefaultAddress').val();
				if (curhref.indexOf("&shipCodeString=")!=-1) {
					curhref.replace(/&shipCodeString=\S+$/i, shipModeCodeStringSuffix);
				} else {
					curhref += shipModeCodeStringSuffix;
				}
				$link.attr("href", curhref);
			};
			
			if($link.attr("id")=="TcpRemoveEmployeeId" ||$link.attr("id")=="TcpUpdateEmployeeId" ){
				showEidtEmployeeHtml($link.attr("id"),'notfromCheckbox');
				return false;
			}
			if(!submitRequest()){
				return;
			}
			if($(".modal-contentEditMyaccount")[0]){
				$(".modal-contentEditMyaccount").parent().empty();
				$(".modal-contentEditMyaccount").parent().remove();
			}
			if($("#updateTcpAccountDialog")){
				$("#updateTcpAccountDialog").empty();
				$("#updateTcpAccountDialog").remove();
			}
			showProgressBar();
			setTimeout(function(){
				$("#progress_bar").hide();
			},2000);
			var $dialog = $('<div id="updateTcpAccountDialog"></div>').load(
			  $link.attr('href').replace('#','')+ ' .modal-contentEditMyaccount',
			  function (responseText, textStatus, XMLHttpRequest) {     
			    cursor_clear();
				if (textStatus == "success") {    
					 $('.bundlePageModal').css('display','none'); 
					if(responseText.Trim()=='notLogon'){
						window.location.href=getAbsoluteURL()+"AjaxLogonForm?myAcctMain=1" +
								"&catalogId="+MyAccountServicesDeclarationJS.catalogId+
								"&langId=" +MyAccountServicesDeclarationJS.langId+
								"&storeId=" +MyAccountServicesDeclarationJS.storeId;
						return;
					}
					// to allow thumbnail switch script to work inside product detail modals
					$dialog.bind( "dialogopen", function(event, ui) {
						if(getUserCookieError()!=0){
							//alert(getUserCookieErrorMsg(getUserCookieError()));
						}
						$(".thumbnail-zoom").click(function(evt) {
							evt.preventDefault();
							$("#product-xlarge").empty().append($("<img>", { src: $(this).attr('data-target'), title:this.title}));
						});
							// to allow outfit carousel to work inside outfit detail modals	
						$( "#carousel").rcarousel({margin: 10, visible: 4, step: 1, speed: 300});		
					});
					$dialog.dialog('open');
				} 
			}).dialog({
				//beforeClose: function () {},
				//hide:true,
				//show:true,
				//dialogClass: 'main-dialog-class',
				close: function() {
					if(getUserCookieError()!=0){
						window.location.href=getAbsoluteURL()+"AjaxLogonForm?myAcctMain=1" +
						"&catalogId="+MyAccountServicesDeclarationJS.catalogId+
						"&langId=" +MyAccountServicesDeclarationJS.langId+
						"&storeId=" +MyAccountServicesDeclarationJS.storeId;
					}
				 // Remove the dialog elements
					$(this).dialog("destroy");
	                // Remove the left over element (the original div element)
					$(this).remove();
				},
				open: function() {
					$("#tcpEditEmployeeFormDiv").html("");
				},
				//closeOnEscape: true,
				autoOpen: false,
				modal:true,
				resizable:false,
				draggable:false,
				width: 'auto',
				height: 'auto',
				time:0,
				title:$link.attr('title')+'<img id="bundlePageModal" class="bundlePageModal" src="'+getImageDirectoryPath()+'images/tcp/ajax-loading.gif" />' 
			});
			return false;		
		
		});	
	});
}
$(document).ready(initLinkClick);

function reloadEditPart(editpart,form){
	if(editpart=='EditEmail'){
		//$('#tcpEmail1').html(form.email1.value);
		if(MyAccountServicesDeclarationJS.storeId == 10151){
			var Country = 'US';
		}
		else{
			var Country = 'CA';
		}
		resetLogoutCookie();
		window.location.href=getAbsoluteURL()+"Logoff?myAcctMain=1" +
		"&catalogId="+MyAccountServicesDeclarationJS.catalogId+
		"&langId=" +MyAccountServicesDeclarationJS.langId+
		"&storeId=" +MyAccountServicesDeclarationJS.storeId+
		"&URL=LogonForm";
		return;
	}else if(editpart=='EditEmployeeId'&&form.demographicField2!=null&&form.demographicField2.value!=null){
		if(form.demographicField2.value==1){
			$('#tcpEmployeeId').html(form.demographicField5.value);
			$('#TcpUpdateEmployeeId').html(MessageHelper.messages["EDIT"]);
			$('#TcpUpdateEmployeeId').attr({title:MessageHelper.messages["EDIT_EMPLOYEE_ID"]});
			$('#TcpUpdateEmployeeId').removeClass('button-blue');
			$("#tcpEmployeeDivId").attr({style:"display:none;"});
			$("#employee").attr({checked:"checked"});
			$("#TcpUpdateEmployeeId").attr({style:"display:inline;"});
			$(".id-num").show();
		}else{
		//	$("#tcpEmployeeDivId").attr({style:"width:180px;padding-bottom:10px"});
			$('#tcpEmployeeId').html("");
			$('#TcpUpdateEmployeeId').attr({title:MessageHelper.messages["ADD_EMPLOYEE_ID"]});
			$('#TcpUpdateEmployeeId').html(MessageHelper.messages["ADD_EMPLOYEE_ID"]);
			$('#TcpUpdateEmployeeId').addClass('button-blue');
			$("#employee").removeAttr("checked");
			$("#TcpRemoveEmployeeId").attr({style:"display:none;"});
			$(".id-num").hide();
		}
		$("#tcpEditEmployeeFormDiv").html("");
	}else if(editpart=='EditShippingMethod'){
		$('#TCPMyAddressBook_forminput_shipModeApplied').html(form.shipModeAppliedDesc.value);
	}else if(editpart=='EditHomeAddress'){
		$('#tcpUserName0').html(form.firstName.value+' '+form.lastName.value);
		$('#tcpUserName').html(form.firstName.value+' '+form.lastName.value);
		$('#tcpStreets').html(form.address1.value+'&nbsp;&nbsp;'+form.address2.value);
		if(form.addressField3!=null&&form.addressField3.value!=null){
			form.zipCode.value=form.addressField3.value;
		}
		$('#tcpCity').html(form.city.value+'&nbsp;,&nbsp;'+$('#'+form.state.id+' option:selected').val()+'&nbsp;&nbsp;'+form.zipCode.value);
		var haPhoneToDisplay=form.phone1.value.split(".");
		var haPhone1=form.phone1.value.replace(/\./g,"");
		if(haPhoneToDisplay.length==1){
			$("#tcpPhone").html(haPhoneToDisplay[0]);
		}else if(haPhoneToDisplay.length==2){
			$("#tcpPhone").html(haPhoneToDisplay[0]+"-"+haPhoneToDisplay[1]);
		}else if(haPhoneToDisplay.length==3){
			$("#tcpPhone").html(haPhoneToDisplay[0]+"-"+haPhoneToDisplay[1]+"-"+haPhoneToDisplay[2]);
		}else if(haPhoneToDisplay.length==4){
			$("#tcpPhone").html(haPhoneToDisplay[0]+"-"+haPhoneToDisplay[1]+"-"+haPhoneToDisplay[2]+" Extn-"+haPhoneToDisplay[3]);
		}else{
			$("#tcpPhone").html(haPhone1);
		}
	}else if(editpart=='EditBirthday'){
		$('#tcpMemberBirthdayMonth').
		html($('#WC_TCPEditBirthdayClub_FormInput_birth_month_In_Register_1').find('option:selected').text());

	 	var  childrenAmountValue=parseInt($("#TCPchildrenIndex").attr("value"),10)+1;
	 	var  childrenAmmountValue=parseInt($("#TCPchildrenAmmount").attr("value"),10)+1;
		var isInvalid=false;
		var FieldNames="ChildName-ChildBirthMonth".split("-");
		var months='January, February, March, April, May, June,July, August, September, October, November, December'.split(',');
		var initHtml='';
		var childCount = childrenAmountValue;
				
		for(var i=1;i<childCount;i++){
			var name="ChildName_null_r_"+i;
			if(form[name]){
				var monthName="ChildBirthMonth_null_r_"+i;
				var month=parseInt(trim(form[monthName].value),10)-1;
				var initHtml=initHtml+'<div class="summary-field"><span>'+trim(form[name].value)+':'+months[month]+'</span></div>';
			}
		}
		$("#tcpDisplayChildBirth").html(initHtml);
	
	}
	setTimeout(function(){
		$("#updateTcpAccountDialog").dialog("close");
		initLinkClick();
		isAutoClose=true;
	},2000);
}


function submitEditForm(editpart,form){
	
	if(editpart=='EditEmployeeId'&&eval(form.demographicField2.value)==0){
		setCurrentId("tcpRemoveEmployee");
	}else{
		setCurrentId("tcpUpdateAccount");
	}
	if(!submitRequest()){
		return;
	}
	showProgressBar();
	var params="&storeId="+MyAccountServicesDeclarationJS.storeId+
				              "&catalogId="+MyAccountServicesDeclarationJS.catalogId+
				              "&langId="+MyAccountServicesDeclarationJS.langId;
	$.ajax({
		type: "POST",
		url: getAbsoluteURL()+$('#editRegister').attr("action"),
		data: (params ? params + "&" : "") +$('#editRegister').serialize(),
		dataType: "html",//json
		timeout: 210000,
		success: function(result){
				cursor_clear();
				if(editpart=="EditPassword"){
					form.logonPassword.name = "logonPassword_old";
					form.logonPasswordVerify.name = "logonPasswordVerify_old";
				}
                result = result.replace("/*","");
				result = result.replace("*/","");
				var obj = jQuery.parseJSON(result);
				if(obj.errorMessage){
					
					if(editpart == 'EditPassword'){
						MessageHelper.tcpFormErrorHandleClient('WC_hidden',obj.errorMessage);
					}
					else{
						if(obj.errorMessage != null && obj.errorMessage != undefined && obj.errorMessage.indexOf("The email address you provided matches an existing account") != -1){
							MessageHelper.tcpFormErrorHandleClient('WC_hidden', 'The email address you provided matches an existing account. Please try another email address.');
						}
						else{
							MessageHelper.tcpFormErrorHandleClient('WC_hidden',obj.errorMessage);
						}						
					}
					
					/*if(editpart=="EditPassword"){
						MessageHelper.tcpFormErrorHandleClient('WC_TCPEditPersonalPassword_FormInput_logonPassword_In_Register_1', MessageHelper.messages["ERROR_PasswordEmpty"]);
						MessageHelper.tcpFormErrorHandleClient('WC_TCPEditPersonalPassword_FormInput_logonPasswordVerify_In_Register_1', MessageHelper.messages["ERROR_VerifyPasswordEmpty"]);
					}
					else {
						MessageHelper.tcpFormErrorHandleClient('WC_hidden', obj.errorMessage);
					}*/
				}else{
					if(editpart!="EditShippingMethod"){
						
						
						var params = [];
						params.storeId		= MyAccountServicesDeclarationJS.storeId;
						params.catalogId	= MyAccountServicesDeclarationJS.catalogId;
						params.langId		= MyAccountServicesDeclarationJS.langId;
						params.orderId = ".";
						params.calculationUsage = "-1,-2,-3,-4,-5,-6,-7";

						wc.service.invoke("MyAccountAjaxUpdateOrderItem",params);
						
				
						
						MessageHelper.tcpFormErrorHandleClient('WC_hidden', 'Your account has been updated.');
					}
					
					
					if(editpart === "EditHomeAddress"){
						
						var countryIndex = form['country'].selectedIndex;
						var stateIndex = form['state'].selectedIndex;
						
							utag_data.customer_city = form["city"].value;
							utag_data.customer_country = form["country"].options[countryIndex].text;
							utag_data.customer_first_name = form["firstName"].value;
							utag_data.customer_last_name = form["lastName"].value;
							utag_data.customer_state = form["state"].options[stateIndex].value;
							utag_data.customer_postal_code = form["addressField3"].value;
						
						TCP.Tealium.Link.userUpdate();
						
						try{console.debug("hiding complete your address link");}catch(e){}
						try{$("#tcpCompleteAddressLink").css("display","none");}catch(e){}
						try{$(".address-labels").css("display","none");}catch(e){}
						try{$("#tcpCity").addClass("on");}catch(e){}
						//dojo.cookie("tcp_firstname",form.firstName.value);
						setCookie("tcp_firstname",form.firstName.value,"/",domainName);
						// For JIRA #1200 - commented the 2 lines
						/*var fname = dojo.byId("fname");
						if(fname)
							fname.innerHTML=form.firstName.value;*/
					}
					else if (editpart === "EditBirthday") {
						TCP.Tealium.Link.birthdaySignup();
					}
					
					reloadEditPart(editpart,form);
				}
		},
		error:function(XMLHttpRequest, textStatus, errorThrown)  {
			//console.debug(XMLHttpRequest.status+" "+XMLHttpRequest.statusText);
			//console.debug(XMLHttpRequest.responseText);
			//console.debug(errorThrown);
			//console.debug(textStatus);
			cursor_clear();
			return false;
		}
	});
}

function prepareEditFormSubmit(editpart,form){
	
	/*if(!submitRequest()){
		return;
	}  */ 		
	var reWhiteSpace = new RegExp(/^\s+$/);	    
	
	if(form){}else{form=$('#editRegister');}
	
	if(editpart=='EditPassword'){
		submitPassword(form,reWhiteSpace);
	}
	if(editpart=='EditEmail'){
		submitEmail(form,reWhiteSpace);
	}
	if(editpart=='EditHomeAddress'){
		submitHomeAddress(form,reWhiteSpace);
	}
	if(editpart=='EditBirthday'){
		submitBirthday(form);
	}
	if(editpart=='EditEmployeeId'){
		submitEditEmployeeId(form,reWhiteSpace);
	}
	if(editpart=='EditShippingMethod'){
		submitEditShippingMethod(form,reWhiteSpace);
	}
	
}
function submitEditShippingMethod(form,reWhiteSpace){
	var selectedShipModeId = 0;
	var shipModeAppliedDesc = "";
	$("div.ship-method input:checkbox").each(function(){
		if (this.checked == true){
			selectedShipModeId = this.value;
			shipModeAppliedDesc = $(this).next("label").text();
		}
	});
	
	if(selectedShipModeId == 0){
		//MessageHelper.tcpFormErrorHandleClient(form["WC_hidden"].id, MessageHelper.messages["ERROR_EmployeeIdEmpty"]);
		return false;
	}
		
	form.demographicField6.value = selectedShipModeId;
	form.shipModeAppliedDesc.value = shipModeAppliedDesc;
	
	submitEditForm('EditShippingMethod',form);
	
}

function cleanParamValue(data) {
	try{
    	if (data == null || $.trim(data) == "null" || ($.trim(data)).length == 0) {
    		data = "";
    	}
    	data = $.trim(data);
    } catch(err) {
    	try{
    		//console.debug(err);
		}catch(e){}
    }
	return data;
}

function submitEditEmployeeId(form,reWhiteSpace){

	if($(".label-error")){
		$(".label-error").hide();
	}	
	if(eval(form.demographicField2.value)==1){
		form.demographicField5.value=form.demographicField5.value.Trim();
		if(form["demographicField5"].value == "" || reWhiteSpace.test(form["demographicField5"].value)){
			MessageHelper.tcpHideErrorHandleClientSpan("WC_hidden");
			MessageHelper.tcpFormErrorHandleClient(form["demographicField5"].id, MessageHelper.messages["ERROR_EmployeeIdEmpty"]);
			return false;
		}else{
			MessageHelper.tcpHideErrorHandleClientSpan("WC_hidden");
			MessageHelper.tcpHideErrorHandleClientSpan(form["demographicField5"].id);
		}
		if(/^[0-9]*$/.test(form["demographicField5"].value)){
			var params="&storeId="+MyAccountServicesDeclarationJS.storeId+
			"&catalogId="+MyAccountServicesDeclarationJS.catalogId+
			"&langId="+MyAccountServicesDeclarationJS.langId;
			$.ajax({
				type: "POST",
				url: getAbsoluteURL()+"TCPEmplyeeDataView?employeeId="+form.demographicField5.value,
				data: (params ? params + "&" : "") ,
				dataType: "html",//json
				timeout: 210000,
				success: function(result){
				
				
				
				
				if(cleanParamValue(result)=='true'){
					dojo.addClass(form["demographicField5"],'error-message');
					MessageHelper.tcpFormErrorHandleClient(form["demographicField5"].id,'');
					MessageHelper.tcpFormErrorHandleClient("WC_hidden", MessageHelper.messages["EDIT_EMPLOYEE_ID_ERROR"]);
				}else{
					dojo.removeClass(form["demographicField5"],'error-message');
					MessageHelper.tcpHideErrorHandleClientSpan(form["demographicField5"].id);
					MessageHelper.tcpHideErrorHandleClientSpan("WC_hidden");
					var targetFirstName=form.TCPEditEmployeeId_firstName.value;
					if(cleanParamValue(result)==cleanParamValue(targetFirstName)){
						
						
						submitEditForm('EditEmployeeId',document.editRegister);
								
					
						
						
						
						
						
						
					}else{
						MessageHelper.tcpFormErrorHandleClient(form["demographicField5"].id, '');
						MessageHelper.tcpFormErrorHandleClient("WC_hidden", MessageHelper.messages["EDIT_EMPLOYEE_ID_FIRSTNAME_ERROR"]);
					}
				}
			},
			error:function(XMLHttpRequest, textStatus, errorThrown)  {
				//console.debug(XMLHttpRequest.status+" "+XMLHttpRequest.statusText);
				//console.debug(XMLHttpRequest.responseText);
				//console.debug(errorThrown);
				//console.debug(textStatus);
				cursor_clear();
				return false;
			}
			});
		}else{
			dojo.addClass(form["demographicField5"],'error-message');
			MessageHelper.tcpFormErrorHandleClient("WC_hidden", MessageHelper.messages["EDIT_EMPLOYEE_ID_ERROR"]);
			return false;
		}
	}else{
		MessageHelper.tcpHideErrorHandleClientSpan(form["demographicField5"].id);
		MessageHelper.tcpHideErrorHandleClientSpan("WC_hidden");
		submitEditForm('EditEmployeeId',form);
	}
}
function submitBirthday(form){

	form.birth_year.value=form.birth_year.value.Trim();
	form.birth_month.value="01";
	form.curr_year.value=form.curr_year.value.Trim();
	form.curr_month.value=form.curr_month.value.Trim();
	form.curr_date.value=form.curr_date.value.Trim();
	var isInvalid=false;
	MessageHelper.tcpHideErrorHandleClientSpan(form.birth_month.id);
	MessageHelper.tcpHideErrorHandleClientSpan(form.birth_year.id);
	if(form.birth_year.value < 0 ||form.birth_year.value ==''){
		MessageHelper.tcpFormErrorHandleClient(form.birth_year.id, MessageHelper.messages["ERROR_SpecifyYear"]);
		isInvalid=true;
	}else if(form.curr_year != null && form.curr_month != null && form.curr_date != null){
		var birth_year = parseInt(form.birth_year.value,10);
		var birth_month = parseInt(form.birth_month.value,10);
		var curr_year = parseInt(form.curr_year.value,10);
		var curr_month = parseInt(form.curr_month.value,10);
		var curr_date = parseInt(form.curr_date.value,10);
		MessageHelper.tcpHideErrorHandleClientSpan('WC_hidden');
		if(birth_year > curr_year){
			/* if birth year entered is in the future. */
			MessageHelper.tcpFormErrorHandleClient('WC_hidden',MessageHelper.messages["ERROR_InvalidDate2"]);
			isInvalid=true;
		}else if((birth_year == curr_year) && (birth_month > curr_month)){
			/* if birth year entered is the same as the current year, then check the month entered. */
			MessageHelper.tcpFormErrorHandleClient('WC_hidden',MessageHelper.messages["ERROR_InvalidDate2"]);
			isInvalid=true;
		}else{
			/* the date of birth provided is valid, now verify if the user is under age. */
			if((!isInvalid)&&form.dateOfBirthTemp != null&&TCPValidateYearAndMonth(form)){
				var final_birth_year = birth_year;
				var final_birth_month = birth_month;
				var final_birth_date = curr_date;
				if(birth_year == 0){
					/* If the user does not specify the year in his/her date of birth, set the year to 1896. */
					final_birth_year = 1896;
				}
				if(birth_month < 10){
					final_birth_month = '0' + birth_month;
				}
				if(curr_date < 10){
					final_birth_date = '0' + curr_date;
				}
				form.dateOfBirthTemp.value = final_birth_year + '-' + final_birth_month + '-' + final_birth_date;
				$('#WC_TCPEditBirthdayClub_FormInput_dateOfBirth_In_Register_1').attr({name:'dateOfBirth'});
			}else{
				isInvalid=true;
			}
		}
	}
	var isBirthdayFieldsInvalid=validateBirthdayFields(form);
	if(isInvalid ||isBirthdayFieldsInvalid)
	{
		$('#WC_TCPEditBirthdayClub_FormInput_dateOfBirth_In_Register_1').attr({name:'dateOfBirthTemp'});
		return false;
	}
	submitEditForm('EditBirthday',form);
}
function submitHomeAddress(form,reWhiteSpace){
	form.firstName.value =form.firstName.value.Trim();
	form.lastName.value =form.lastName.value.Trim();
	form.address1.value =form.address1.value.Trim();
	form.address2.value =form.address2.value.Trim();
	form.city.value =form.city.value.Trim();
	form.state.value =form.state.value.Trim();
	form.zipCode.value =form.zipCode.value.Trim();
	form.addressField3.value =form.addressField3.value.Trim();
	form.country.value =form.country.value.Trim();
	form.phone1.value =form.phone1.value.AllTrim();
	var isInvalid=false;
	MessageHelper.tcpHideErrorHandleClientSpan(form["firstName"].id);
	if(form["firstName"].value == "" || reWhiteSpace.test(form["firstName"].value)){
		MessageHelper.tcpFormErrorHandleClient(form["firstName"].id, MessageHelper.messages["ERROR_FirstNameEmpty"]);
		if(!isInvalid) {form.firstName.focus();}
		isInvalid=true;
	}
	if(!MessageHelper.isValidUTF8length(form["firstName"].value, 256)){ 
		MessageHelper.tcpFormErrorHandleClient(form["firstName"].id, MessageHelper.messages["ERROR_FirstNameTooLong"]);
		if(!isInvalid) {form.firstName.focus();}
		isInvalid= true;
	}
	if(MessageHelper.IsContainNumber(form.firstName.value)){ 
		MessageHelper.tcpFormErrorHandleClient(form.firstName.id, MessageHelper.messages["ERROR_NAMECONTAINNUMBER"]); 
		if(!isInvalid) {form.firstName.focus();}
		isInvalid= true;
	}
	if(!MessageHelper.isContainsOnlyAlphabet(form.firstName.value)){ 
		MessageHelper.tcpFormErrorHandleClient(form.firstName.id, MessageHelper.messages["ERROR_FirstNameHasSpecialChar"]); 
		if(!isInvalid) {form.firstName.focus();}
		isInvalid= true;
	}	
	
	
	
	
	MessageHelper.tcpHideErrorHandleClientSpan(form["lastName"].id);
	if(form["lastName"].value == "" || reWhiteSpace.test(form["lastName"].value)){
		MessageHelper.tcpFormErrorHandleClient(form["lastName"].id, MessageHelper.messages["ERROR_LastNameEmpty"]);
		if(!isInvalid) {form.lastName.focus();}
		isInvalid=true;
	}
	if(!MessageHelper.isValidUTF8length(form["lastName"].value, 256)){ 
		MessageHelper.tcpFormErrorHandleClient(form["lastName"].id, MessageHelper.messages["ERROR_LastNameTooLong"]);
		if(!isInvalid) {form.lastName.focus();}
		isInvalid= true;
	}
	if(MessageHelper.IsContainNumber(form.lastName.value)){ 
		MessageHelper.tcpFormErrorHandleClient(form.lastName.id, MessageHelper.messages["ERROR_NAMECONTAINNUMBER"]); 
		if(!isInvalid) {form.lastName.focus();}
		isInvalid= true;
	}
	if(!MessageHelper.isContainsOnlyAlphabet(form.lastName.value)){ 
		MessageHelper.tcpFormErrorHandleClient(form.lastName.id, MessageHelper.messages["ERROR_LastNameHasSpecialChar"]); 
		if(!isInvalid) {form.lastName.focus();}
		isInvalid= true;
	}	
	
	
	
	
	MessageHelper.tcpHideErrorHandleClientSpan(form["address1"].id);
	if(form["address1"].value == "" || reWhiteSpace.test(form["address1"].value)){
		MessageHelper.tcpFormErrorHandleClient(form["address1"].id, MessageHelper.messages["ERROR_AddressEmpty"]);
		if(!isInvalid) {form.address1.focus();}
		isInvalid=true;
	}
	if(!MessageHelper.isValidUTF8length(form["address1"].value, 100)){ 
		MessageHelper.tcpFormErrorHandleClient(form["address1"].id, MessageHelper.messages["ERROR_AddressTooLong"]); 
		if(!isInvalid) {form.address1.focus();}
		isInvalid = true;
	}
	if(!MessageHelper.isValidCharStr(form["address1"].value)){ 
		MessageHelper.tcpFormErrorHandleClient(form["address1"].id, MessageHelper.messages["ERROR_Address1HasSpecialChar"]); 
		if(!isInvalid) {form.address1.focus();}
		isInvalid = true;
	}
	
	
	
	
	MessageHelper.tcpHideErrorHandleClientSpan(form["address2"].id);
	if(!MessageHelper.isValidUTF8length(form["address2"].value, 50)){ 
		MessageHelper.tcpFormErrorHandleClient(form["address2"].id, MessageHelper.messages["ERROR_AddressTooLong"]);
		if(!isInvalid) {form.address2.focus();}
		isInvalid = true;
	}
	if(!MessageHelper.isValidCharStr(form["address2"].value)){ 
		
		MessageHelper.tcpFormErrorHandleClient(form["address2"].id, MessageHelper.messages["ERROR_Address2HasSpecialChar"]);
		if(!isInvalid) {form.address2.focus();}
		isInvalid = true;
	}	
	
	
	
	MessageHelper.tcpHideErrorHandleClientSpan(form["city"].id);
	if((form["city"].value == "" || reWhiteSpace.test(form["city"].value))){ 
		MessageHelper.tcpFormErrorHandleClient(form["city"].id, MessageHelper.messages["ERROR_CityEmpty"]);
		if(!isInvalid) {form.city.focus();}
		isInvalid = true;
	}
	if(!MessageHelper.isValidUTF8length(form["city"].value, 128)){
		MessageHelper.tcpFormErrorHandleClient(form["city"].id, MessageHelper.messages["ERROR_CityTooLong"]);
		if(!isInvalid) {form.city.focus();}
		isInvalid = true;
	}
	if(!MessageHelper.isValidCharStr(form["city"].value)){
		MessageHelper.tcpFormErrorHandleClient(form["city"].id, MessageHelper.messages["ERROR_CityHasSpecialChar"]);
		if(!isInvalid) {form.city.focus();}
		isInvalid = true;
	}	

	
	
	
	
	MessageHelper.tcpHideErrorHandleClientSpan(form["state"].id);
	if(form["state"].value == "" || reWhiteSpace.test(form["state"].value)){
		MessageHelper.tcpFormErrorHandleClient(form["state"].id, MessageHelper.messages["ERROR_StateEmpty"]);
		if(!isInvalid) {form.state.focus();}
		isInvalid=true;
	}
	if(!MessageHelper.isValidUTF8length(form["state"].value, 128)){ 
		MessageHelper.tcpFormErrorHandleClient(form["state"].id, MessageHelper.messages["ERROR_StateTooLong"]);
		if(!isInvalid) {form.state.focus();}
		isInvalid= true;
	}
	
	
	
	
	MessageHelper.tcpHideErrorHandleClientSpan(form["country"].id);
	if(form["country"].value == "" || reWhiteSpace.test(form["country"].value)){
		MessageHelper.tcpFormErrorHandleClient(form["country"].id, MessageHelper.messages["ERROR_CountryEmpty"]);
		if(!isInvalid) {form.country.focus();}
		isInvalid=true;
	}
	if(!MessageHelper.isValidUTF8length(form["country"].value, 128)){ 
		MessageHelper.tcpFormErrorHandleClient(form["country"].id, MessageHelper.messages["ERROR_CountryTooLong"]);
		if(!isInvalid) {form.country.focus();}
		isInvalid= true;
	}
	
	
	
	MessageHelper.tcpHideErrorHandleClientSpan(form["zipCode"].id);	
	var isZipValid = AddressHelper.performZipValidation(form,true);
	
	/*
	MessageHelper.tcpHideErrorHandleClientSpan(form["zipCode"].id);
	if(form["zipCode"].value == "" || reWhiteSpace.test(form["zipCode"].value)){
		MessageHelper.tcpFormErrorHandleClient(form["zipCode"].id, MessageHelper.messages["ERROR_ZipCodeEmpty"]);
		if(!isInvalid) {form.addressField3.focus();}
		isInvalid=true;
	}
	if(!MessageHelper.isValidUTF8length(form["zipCode"].value, 40)){ 
		MessageHelper.tcpFormErrorHandleClient(form["zipCode"].id, MessageHelper.messages["ERROR_ZipCodeTooLong"]);
		if(!isInvalid) {form.addressField3.focus();}
		isInvalid= true;
	}
	if(!MessageHelper.isValidCharStr(form["zipCode"].value)){ 
		MessageHelper.tcpFormErrorHandleClient(form["zipCode"].id, MessageHelper.messages["ERROR_ZipCodeHasSpecialChar"]);
		if(!isInvalid) {form.addressField3.focus();}
		isInvalid= true;
	}*/
	
	
	
	
	MessageHelper.tcpHideErrorHandleClientSpan(form["addressField3"].id);
	if(form["addressField3"].value == "" || reWhiteSpace.test(form["addressField3"].value)){
		MessageHelper.tcpFormErrorHandleClient(form["addressField3"].id, MessageHelper.messages["ERROR_ZipCodeEmpty"]);
		if(!isInvalid) {form.addressField3.focus();}
		isInvalid=true;
	}
	if(!MessageHelper.isValidUTF8length(form["addressField3"].value, 40)){ 
		MessageHelper.tcpFormErrorHandleClient(form["addressField3"].id, MessageHelper.messages["ERROR_ZipCodeTooLong"]);
		if(!isInvalid) {form.addressField3.focus();}
		isInvalid= true;
	}
	if(!MessageHelper.isValidCharStr(form["addressField3"].value)){ 
		MessageHelper.tcpFormErrorHandleClient(form["addressField3"].id, MessageHelper.messages["ERROR_ZipCodeHasSpecialChar"]);
		if(!isInvalid) {form.addressField3.focus();}
		isInvalid= true;
	}	

	
	
	
	
	
	MessageHelper.tcpHideErrorHandleClientSpan(form["phone1"].id);
	form["phone1"].value = form["phone1"].value.replace(/[^0-9]/g, "");
	if(form["phone1"].value == "" || reWhiteSpace.test(form["phone1"].value)){
		MessageHelper.tcpFormErrorHandleClient(form["phone1"].id, MessageHelper.messages["ERROR_PhonenumberEmpty"]);
		if(!isInvalid) {form.phone1.focus();}
		isInvalid=true;
	}
	if(!MessageHelper.isValidUTF8length(form["phone1"].value, 32)){ 
		MessageHelper.tcpFormErrorHandleClient(form["phone1"].id, MessageHelper.messages["ERROR_PhoneTooLong"]);
		if(!isInvalid) {form.phone1.focus();}
		isInvalid= true;
	}
	if(!MessageHelper.IsValidPhone2(form["phone1"].value)){
		MessageHelper.tcpFormErrorHandleClient(form["phone1"].id, MessageHelper.messages["ERROR_INVALIDPHONE"]);
		if(!isInvalid) {form.phone1.focus();}
		isInvalid = true;
	}
	if(!MessageHelper.isValidCharStr(form["phone1"].value)){
		MessageHelper.tcpFormErrorHandleClient(form["phone1"].id, MessageHelper.messages["ERROR_PhoneHasSpecialChar"]);
		if(!isInvalid) {form.phone1.focus();}
		isInvalid = true;
	}
	
	
	
	
	if(isInvalid || !isZipValid){
		return false;
	}
	//submitEditForm('EditHomeAddress',form);
	
	try{
		executeSearch(form,5000,'EditHomeAddress');
	}catch(e){
		submitEditForm('EditHomeAddress',form);
	}
}

function submitEmail(form,reWhiteSpace){
	form.email1.value =form.email1.value.Trim();
	form.emailVerify.value =form.emailVerify.value.Trim();
	var isInvalid=false;
	if(isBVEnabled) {
		if(MessageHelper.currentEmailElementId != "") {
			if(!isInvalid) {
				//commented due to #638
				//form.mail1.focus();
			}
			isInvalid = true;
		}
	} 
	
	else {
		MessageHelper.tcpHideErrorHandleClientSpan(form["email1"].id);
		if((form["email1"].value == "" || reWhiteSpace.test(form["email1"].value))){
			MessageHelper.tcpFormErrorHandleClient(form["email1"].id, MessageHelper.messages["ERROR_EmailEmpty"]);
			if(!isInvalid) {form.email1.focus();}
			isInvalid= true;
		}
		
		if(!MessageHelper.isValidEmail(form["email1"].value)){
			MessageHelper.tcpFormErrorHandleClient(form["email1"].id, MessageHelper.messages["ERROR_INVALIDEMAILFORMAT"]);
			if(!isInvalid) {form.email1.focus();}
			isInvalid= true;
		}

		if(!MessageHelper.isValidUTF8length(form["email1"].value, 256)){ 
			MessageHelper.tcpFormErrorHandleClient(form["email1"].id, MessageHelper.messages["ERROR_EmailTooLong"]);
			if(!isInvalid) {form.email1.focus();}
			isInvalid= true;
		}
	}

	MessageHelper.tcpHideErrorHandleClientSpan(form["emailVerify"].id);
	if((form["emailVerify"].value == "" || reWhiteSpace.test(form["emailVerify"].value))){
		MessageHelper.tcpFormErrorHandleClient(form["emailVerify"].id, MessageHelper.messages["EMAILREENTER_DO_NOT_MATCH"]);
		if(!isInvalid) {form.emailVerify.focus();}
		isInvalid= true;
	}
	if((form["email1"].value == "" || reWhiteSpace.test(form["email1"].value))){
		MessageHelper.tcpFormErrorHandleClient(form["email1"].id, MessageHelper.messages["ERROR_INVALIDEMAILFORMAT"]);
		if(!isInvalid) {form.email1.focus();}
		isInvalid= true;
	}
	if(form["email1"].value!= form["emailVerify"].value){
		MessageHelper.tcpFormErrorHandleClient(form.emailVerify.id, MessageHelper.messages["EMAILREENTER_DO_NOT_MATCH"]);
		if(!isInvalid) {form.emailVerify.focus();}
		isInvalid= true;
	}
//Commented due to issue 1 of #638
/*	if($('#tcpEmail1')){
		console.log("call 4");
		if($('#tcpEmail1').text().trim() == form.email1.value){
			MessageHelper.tcpFormErrorHandleClient(form.email1.id, MessageHelper.messages["EMAILREENTER_SAMEACC"]);
			if(!isInvalid) {form.email1.focus();}
			isInvalid= true;
		}
	}
*/
	if(!MessageHelper.isValidEmail(form["email1"].value)){
		MessageHelper.tcpFormErrorHandleClient(form["email1"].id, MessageHelper.messages["ERROR_INVALIDEMAILFORMAT"]);
		MessageHelper.tcpFormErrorHandleClient(form["emailVerify"].id, MessageHelper.messages["EMAILREENTER_DO_NOT_MATCH"]);
		if(!isInvalid) {form.email1.focus();}
		isInvalid= true;
	}
	if(isInvalid){
		return false;
	}
	form.logonId.value=form.email1.value;
	

	checkLoyaltyExistingCustomer(form,form.email1.value,form.myPlaceId.value);
}

function submitPassword(form,reWhiteSpace){
	
	if(form.logonPassword_old.name == "logonPassword"){
		form.logonPassword_old.name = "logonPassword_old";
		form.logonPasswordVerify_old.name = "logonPasswordVerify_old";
	}
	
	form.logonPassword_old.value =form.logonPassword_old.value.Trim();
	form.logonPasswordVerify_old.value =form.logonPasswordVerify_old.value.Trim();
	//form.challengeAnswer.value =form.challengeAnswer.value.Trim();
	var isInvalid=false;
	MessageHelper.tcpHideErrorHandleClientSpan(null,form["logonPassword_old"].id);
	/*check whether the values in password and verify password fields match, if so, update the password. */ 
	if(form["logonPassword_old"].value == "" || reWhiteSpace.test(form["logonPassword_old"].value)){
		MessageHelper.tcpFormErrorHandleClient(form["logonPassword_old"].id, MessageHelper.messages["ERROR_PasswordEmpty"]);
		if(!isInvalid) {form.logonPassword_old.focus();}
		isInvalid=true;
	}
	MessageHelper.tcpHideErrorHandleClientSpan(null,form["logonPasswordVerify_old"].id);
	if(form["logonPasswordVerify_old"].value == "" || reWhiteSpace.test(form["logonPasswordVerify_old"].value)){
		MessageHelper.tcpFormErrorHandleClient(form.logonPasswordVerify_old.id, MessageHelper.messages["ERROR_VerifyPasswordEmpty"]);
		if(!isInvalid) {form.logonPasswordVerify_old.focus();}
		isInvalid=true;
	}
	/*MessageHelper.tcpHideErrorHandleClientSpan(form["challengeQuestion"].id);
	if(form["challengeQuestion"].value == "" || reWhiteSpace.test(form["challengeQuestion"].value)){
		MessageHelper.tcpFormErrorHandleClient(form.challengeQuestion.id, MessageHelper.messages["ERROR_challengeAnswerEmpty"]);
		if(!isInvalid) {form.challengeQuestion.focus();}
		isInvalid=true;
	}
	MessageHelper.tcpHideErrorHandleClientSpan(form["challengeAnswer"].id);
	if(form["challengeAnswer"].value == "" || reWhiteSpace.test(form["challengeAnswer"].value)){
		MessageHelper.tcpFormErrorHandleClient(form.challengeAnswer.id, MessageHelper.messages["ERROR_challengeAnswerEmpty"]);
		if(!isInvalid) {form.challengeAnswer.focus();}
		isInvalid=true;
	}*/	
	
	if(form.logonPassword_old.value!= form.logonPasswordVerify_old.value){
	   MessageHelper.tcpFormErrorHandleClient(form.logonPasswordVerify_old.id, MessageHelper.messages["PWDREENTER_DO_NOT_MATCH"]);
	   if(!isInvalid) {form.logonPasswordVerify_old.focus();}
	   isInvalid=true;
	}
	
	var rePwdRule = /(?=.*\d)(?=.*[a-zA-Z]).{8,}/;
	MessageHelper.tcpHideErrorHandleClientSpan(null,'WC_hidden');
	if(form.logonPassword_old.value != null && form.logonPassword_old.value != "" && !rePwdRule.test(form.logonPassword_old.value)){
		try{
		MessageHelper.tcpFormErrorHandleClient('WC_hidden', MessageHelper.messages["PWD_EDIT_UPDATE_ERROR"]);
		$("#WC_TCPEditPersonalPassword_FormInput_logonPassword_In_Register_1").unbind("focus");
		$("#WC_TCPEditPersonalPassword_FormInput_logonPasswordVerify_In_Register_1").unbind("focus");
		$("#WC_TCPEditPersonalPassword_FormInput_logonPassword_In_Register_1").addClass("error-message");
		$("#WC_TCPEditPersonalPassword_FormInput_logonPasswordVerify_In_Register_1").addClass("error-message");
		$("#WC_TCPEditPersonalPassword_FormInput_logonPassword_In_Register_1").bind("focus", function(){
			$("#WC_TCPEditPersonalPassword_FormInput_logonPassword_In_Register_1").removeClass("error-message");
		});
		$("#WC_TCPEditPersonalPassword_FormInput_logonPasswordVerify_In_Register_1").bind("focus", function(){
			$("#WC_TCPEditPersonalPassword_FormInput_logonPasswordVerify_In_Register_1").removeClass("error-message");
		});
		}catch(e){console.log(e);}
		isInvalid=true;
	}
	if(isInvalid){
		return false;
	}
	form.logonPassword_old.name = "logonPassword";
	form.logonPasswordVerify_old.name = "logonPasswordVerify";
	
	submitEditForm('EditPassword',form);
}


$('body').on('click', 'ul.tabs > li > a', function(e) {

    //Get Location of tab's content
    var contentLocation = $(this).attr('href');

    //Let go if not a hashed one
    if(contentLocation.charAt(0)=="#") {

        e.preventDefault();

        //Make Tab Active
        $(this).parent().siblings().children('a').removeClass('active');
        $(this).addClass('active');

        //Show Tab Content & add active class
        $(contentLocation).show().addClass('active').siblings().hide().removeClass('active');

    }
});
	String.prototype.Trim = function(){
	  return this.replace(/(^\s*)|(\s*$)/g, "");
	};
	
	String.prototype.AllTrim = function(){
		return this.replace(/\s/g, "");
	};

	String.prototype.LTrim = function(){
	  return this.replace(/(^\s*)/g, "");
	};

	String.prototype.RTrim = function(){
	  return this.replace(/(\s*$)/g, "");
	};
	function TCPValidateAge(form,selectObject) {
		if(selectObject&&selectObject!=null){
			initSelecthis(selectObject);
		}
		TCPValidateYearAndMonth(form);
	}
	function initSelecthis(selectObject) {
		var id=selectObject.id;
		var targetSelect=$('#'+id);
		if(isNaN(parseInt($.trim(selectObject.value)))){
			return;
		}
		var whichselect=selectObject.value;
		var start=selectObject.value-75;
		if(parseInt($.trim(selectObject.value))<1975){
			start=1900;
		}
		var end=eval(start)+150;
		if(parseInt(MessageHelper.getCurrentYear(),10)-14<end){
			end=parseInt(MessageHelper.getCurrentYear(),10)-14;
		}
		if(start<1900){
			start=1900;
		}
		
		targetSelect.html("");
		for(var i=start;i<=end;i++){
			 var selectOpt = $("<option></option>");   
			if(i==whichselect){
				selectOpt.attr({selected:true});   
			}
			selectOpt.attr({id:"opt"+i});   
			selectOpt.attr({value:""+i});   
			selectOpt.html(""+i);
			targetSelect.append(selectOpt);
		}
	}
	
	/**
	 *  This function validates the customer's input for age. If the user is under age, pop up a message to ask the user to review the store policy.
	 *  @param {string} The name of the form containing personal information of the customer.
	 */
	 function TCPValidateYearAndMonth(form){
		/*Check whether age is less than 13, if so, pop up a message to ask the user to review the store policy. */
		 form.birth_year.value=form.birth_year.value.Trim();
		 if(form.birth_year.value=='' ){
			 return false;
		 }
		 var birth_year = parseInt(form.birth_year.value,10);
		 var curr_year = parseInt(form.curr_year.value,10);
		 MessageHelper.tcpHideErrorHandleClientSpan("WC_hidden");
		if((curr_year - birth_year) < 13){
			MessageHelper.tcpFormErrorHandleClient('WC_hidden',MessageHelper.messages["AGE_WARNING_ALERT"]);
			 return false;
		}else if((curr_year - birth_year) == 13){
			form.birth_month.value=form.birth_month.value.Trim();
			 if(form.birth_month.value==''){
				 return false;
			 }
			 var birth_month = parseInt(form.birth_month.value,10);
			 var curr_month = parseInt(form.curr_month.value,10);
			if(curr_month < birth_month){
				MessageHelper.tcpFormErrorHandleClient('WC_hidden',MessageHelper.messages["AGE_WARNING_ALERT"]);
				 return false;
			}
		}
		return true;
	}
	
	/**
	 * Clears the First Name string in First Name field.
	 */

	function clearFirstName(obj) {
		obj.value="";
		obj.focus();
	}

	/**
	 * Displays the First Namestring in First Name field.
	 */
	function fillFirstName(obj) {
		if(obj.value=='')
			obj.value="Child's First Name";
	}

	 function TCPCreateNewChildren(){
		 var newTCPchildrenAmmountValue=parseInt($("#TCPchildrenAmmount").attr("value"),10)+1;
		if(newTCPchildrenAmmountValue-1 < 4) {
		 var newTCPchildrenAmmountValue=parseInt($("#TCPchildrenAmmount").attr("value"),10)+1;
		 $("#TCPchildrenAmmount").attr({value:newTCPchildrenAmmountValue});
		var newChildrenIndex  =  parseInt($("#TCPchildrenIndex").attr("value"),10)+1;
		$("#TCPchildrenIndex").attr({value:newChildrenIndex});
		var newNameValue="_null_r_"+ newChildrenIndex;
		
		/*if($('#TCPchildrenAmmount') <= $('#TCPchildrenAmount')){
			var temp = parseInt($('#TCPchildrenAmount').attr("value"),10)+1;
			newNameValue = "_null_r_"+ temp;
		}*/
		var birthdayTemplate=$("#birthdayTemplate").clone(true);
		//birthdayTemplate.html().replace(new RegExp("_birthdayTemplate", 'g'),newNameValue);
		 var birthdayTemplateHTML=birthdayTemplate.html().replace(/\_birthdayTemplate/g,newNameValue);
		 birthdayTemplate.html(birthdayTemplateHTML);
		 birthdayTemplate.css("display","block");
		 birthdayTemplate.attr({id:newNameValue});
		 $("#TCPChildsBirthday").append(birthdayTemplate);
 		 document.getElementById("WC_TCPEditBirthdayClub_FormInput_ChildName_null_r_"+newChildrenIndex+"__In_Register_1").value="Child's First Name";
		}
		else{
			//var buttonElement = document.getElementById("addChild").style.display = "none";
			var buttonElement = document.getElementById("addChild");
			buttonElement.removeChild(document.getElementById("addChildButton"));
		}
	 }
	 function TCPDeleteChildren(index){
		 //if(window.confirm("Are you sure you want to delete this Child's Birthday ?")){
			 var intIndex=parseInt(index,10);
			 $("#exist__null_r_"+intIndex).hide();
			 var newTCPchildrenAmmountValue=parseInt($("#TCPchildrenAmmount").attr("value"),10)-1;
			 $("#TCPchildrenAmmount").attr({value:newTCPchildrenAmmountValue}); 
			 $("#WC_TCPEditBirthdayClub_FormInput_ChildName_null_r_"+intIndex+"__In_Register_1").attr(
					 {name:"ChildName_null_d_"+intIndex});
			 $("#WC_TCPEditBirthdayClub_FormInput_ChildBirthYear_null_r_"+intIndex+"__In_Register_1").attr(
					 {name:"ChildBirthYear_null_d_"+intIndex});
			 $("#WC_TCPEditBirthdayClub_FormInput_ChildBirthMonth_null_r_"+intIndex+"__In_Register_1").attr(
					 {name:"ChildBirthMonth_null_d_"+intIndex});
			 $("#WC_TCPEditBirthdayClub_FormInput_ChildGender_null_r_"+intIndex+"__In_Register_1").attr(
					 {name:"ChildGender_null_d_"+intIndex});
			 $("#WC_TCPEditBirthdayClub_FormInput_ChildDateOfBirth_null_r_"+intIndex+"__In_Register_1").attr(
					 {name:"ChildDateOfBirth_null_d_"+intIndex});
		 //}
	 }
	 function TCPDeleteNewChildren(obj){
		
		  var buttonElement = document.getElementById("addChildButton");
		  if(buttonElement == null){
			    var parentEle = document.getElementById("addChild");
	   			var link = document.createElement('a');
	   			link.setAttribute('href', 'javascript:void(0)');
	   			link.setAttribute('onclick', 'javascript:TCPCreateNewChildren();return false;');
	   			link.setAttribute("class","button-blue");
	   			link.setAttribute("id", "addChildButton");
	   			link.setAttribute("style","color:#fff;");
	   			link.innerHTML = '+ add another child';
	   			parentEle.appendChild(link);
		  }     		     
			 var a_id=$(obj).attr("id");
			 index=a_id.replace("WC_TCPEditBirthdayClub_FormInput_Delete_null_r_","").replace("__In_Register_1","");
			 var intIndex=parseInt(index,10);
			 $("#_null_r_"+intIndex).remove();
			 var newTCPchildrenAmmountValue=parseInt($("#TCPchildrenAmmount").attr("value"),10)-1;
			 $("#TCPchildrenAmmount").attr({value:newTCPchildrenAmmountValue});
			 for(var i=intIndex+1;i<newTCPchildrenAmmountValue+2;i++){
				 $("#_null_r_"+i).attr({id:"_null_r_"+(i-1)});
				 $("#WC_TCPEditBirthdayClub_FormInput_Delete_null_r_"+i+"__In_Register_1").attr(
						 {id:"WC_TCPEditBirthdayClub_FormInput_Delete_null_r_"+(i-1)+"__In_Register_1"});
				 $("#WC_TCPEditBirthdayClub_FormInput_ChildName_null_r_"+i+"__In_Register_1").attr(
						 {name:"ChildName_null_r_"+(i-1)});
				 $("#WC_TCPEditBirthdayClub_FormInput_ChildBirthYear_null_r_"+i+"__In_Register_1").attr(
						 {name:"ChildBirthYear_null_r_"+(i-1)});
				 $("#WC_TCPEditBirthdayClub_FormInput_ChildBirthMonth_null_r_"+i+"__In_Register_1").attr(
						 {name:"ChildBirthMonth_null_r_"+(i-1)});
				 $("#WC_TCPEditBirthdayClub_FormInput_ChildGender_null_r_"+i+"__In_Register_1").attr(
						 {name:"ChildGender_null_r_"+(i-1)});
				 $("#WC_TCPEditBirthdayClub_FormInput_ChildDateOfBirth_null_r_"+i+"__In_Register_1").attr(
						 {name:"ChildDateOfBirth_null_r_"+(i-1)});
				 $("#WC_TCPEditBirthdayClub_FormInput_ChildName_null_r_"+i+"__In_Register_1").attr(
						 {id:"WC_TCPEditBirthdayClub_FormInput_ChildName_null_r_"+(i-1)+"__In_Register_1"});
				 $("#WC_TCPEditBirthdayClub_FormInput_ChildBirthYear_null_r_"+i+"__In_Register_1").attr(
						 {id:"WC_TCPEditBirthdayClub_FormInput_ChildBirthYear_null_r_"+(i-1)+"__In_Register_1"});
				 $("#WC_TCPEditBirthdayClub_FormInput_ChildBirthMonth_null_r_"+i+"__In_Register_1").attr(
						 {id:"WC_TCPEditBirthdayClub_FormInput_ChildBirthMonth_null_r_"+(i-1)+"__In_Register_1"});
				 $("#WC_TCPEditBirthdayClub_FormInput_ChildGender_null_r_"+i+"__In_Register_1").attr(
						 {id:"WC_TCPEditBirthdayClub_FormInput_ChildGender_null_r_"+(i-1)+"__In_Register_1"});
				 $("#WC_TCPEditBirthdayClub_FormInput_ChildDateOfBirth_null_r_"+i+"__In_Register_1").attr(
						 {id:"WC_TCPEditBirthdayClub_FormInput_ChildDateOfBirth_null_r_"+(i-1)+"__In_Register_1"});
			 }
			  
		 //}else{
			// return false;
		 //}
		 
	 }
	 
	 function validateBirthdayFields(form){
		 	var  childrenAmmountValue=parseInt($("#TCPchildrenAmount").attr("value"),10)+1;
		 	childrenAmmountValue = parseInt($('#TCPchildrenIndex').attr("value"),10)+1;
			
		 	var isInvalid=false;
			var FieldNames="ChildName-ChildBirthYear-ChildBirthMonth-ChildGender-ChildDateOfBirth".split("-");
			for(var j=0;j<FieldNames.length;j++){
				for(var i=1;i<childrenAmmountValue;i++){
					var name=FieldNames[j]+"_null_r_"+i;
					if(typeof(form[name]) != 'undefined'){
						MessageHelper.tcpHideErrorHandleClientSpan(form[name].id);
						if(FieldNames[j]=='ChildName'){
							if((form[name])&&$.trim(form[name].value)=="Child's First Name" || ((form[name])&&$.trim(form[name].value)=="")){
								MessageHelper.tcpFormErrorHandleClient(form[name].id,"please indicate the value of "+FieldNames[j]);
								isInvalid= true;
							}		
						}else if((form[name])&&$.trim(form[name].value)==""){
							MessageHelper.tcpFormErrorHandleClient(form[name].id,"please indicate the value of "+FieldNames[j]);
							isInvalid= true;
						}
					}
				}
			}
			MessageHelper.tcpHideErrorHandleClientSpan(form["terms"].id);
			if((form["terms"])&& !form.terms.checked){
				MessageHelper.tcpFormErrorHandleClient(form["terms"].id,"Please accept the terms by selecting the box below and re-submit");
				isInvalid= true;
			}
			return isInvalid;
		}	 
	 
	 
	 function tcpPrepareEditAddressFormSubmit(form, linkObj){
		 var reWhiteSpace = new RegExp(/^\s+$/);	   
		 
			form.firstName.value =form.firstName.value.Trim();
			form.lastName.value =form.lastName.value.Trim();
			form.address1.value =form.address1.value.Trim();
			form.address2.value =form.address2.value.Trim();
			form.city.value =form.city.value.Trim();
			form.state.value =form.state.value.Trim();
			form.zipCode.value =form.zipCode.value.Trim();
			form.addressField3.value =form.addressField3.value.Trim();
			form.country.value =form.country.value.Trim();
			form.phone1.value =form.phone1.value.Trim();
			
			if(form.primary &&  form.primary.checked){
			    form.primary.value = true;
			    form.newPrimaryValue.value = "1";
			}
			else {
				form.primary.value = false;
				form.newPrimaryValue.value = "0";
			}		
			
			if(form["oldPrimary"].value != form["newPrimaryValue"].value ) {
				//user is changing default shipping method
				form["primaryChangedFlag"].value = 1;
			}	
			
			
			
			var isInvalid=false;
			
			
			
			MessageHelper.tcpHideErrorHandleClientSpan(form["firstName"].id);
			if(form["firstName"].value == "" || reWhiteSpace.test(form["firstName"].value)){
				MessageHelper.tcpFormErrorHandleClient(form["firstName"].id, MessageHelper.messages["ERROR_FirstNameEmpty"]);
				if(!isInvalid) {form.firstName.focus();}
				isInvalid=true;
			}
			if(!MessageHelper.isValidUTF8length(form["firstName"].value, 256)){ 
				MessageHelper.tcpFormErrorHandleClient(form["firstName"].id, MessageHelper.messages["ERROR_FirstNameTooLong"]);
				if(!isInvalid) {form.firstName.focus();}
				isInvalid= true;
			}
			if(MessageHelper.IsContainNumber(form.firstName.value)){ 
				MessageHelper.tcpFormErrorHandleClient(form.firstName.id, MessageHelper.messages["ERROR_NAMECONTAINNUMBER"]); 
				if(!isInvalid) {form.firstName.focus();}
				isInvalid= true;
			}
			if(!MessageHelper.isContainsOnlyAlphabet(form.firstName.value)){ 
				MessageHelper.tcpFormErrorHandleClient(form.firstName.id, MessageHelper.messages["ERROR_FirstNameHasSpecialChar"]); 
				if(!isInvalid) {form.firstName.focus();}
				isInvalid= true;
			}				
			
			
			
			
			
			
			MessageHelper.tcpHideErrorHandleClientSpan(form["lastName"].id);
			if(form["lastName"].value == "" || reWhiteSpace.test(form["lastName"].value)){
				MessageHelper.tcpFormErrorHandleClient(form["lastName"].id, MessageHelper.messages["ERROR_LastNameEmpty"]);
				if(!isInvalid) {form.lastName.focus();}
				isInvalid=true;
			}
			if(!MessageHelper.isValidUTF8length(form["lastName"].value, 256)){ 
				MessageHelper.tcpFormErrorHandleClient(form["lastName"].id, MessageHelper.messages["ERROR_LastNameTooLong"]);
				if(!isInvalid) {form.lastName.focus();}
				isInvalid= true;
			}
			if(MessageHelper.IsContainNumber(form.lastName.value)){ 
				MessageHelper.tcpFormErrorHandleClient(form.lastName.id, MessageHelper.messages["ERROR_NAMECONTAINNUMBER"]); 
				if(!isInvalid) {form.lastName.focus();}
				isInvalid= true;
			}
			if(!MessageHelper.isContainsOnlyAlphabet(form.lastName.value)){ 
				MessageHelper.tcpFormErrorHandleClient(form.lastName.id, MessageHelper.messages["ERROR_LastNameHasSpecialChar"]); 
				if(!isInvalid) {form.lastName.focus();}
				isInvalid= true;
			}			
			
			
			
			
			MessageHelper.tcpHideErrorHandleClientSpan(form["address1"].id);
			if(form["address1"].value == "" || reWhiteSpace.test(form["address1"].value)){ 
				MessageHelper.tcpFormErrorHandleClient(form["address1"].id, MessageHelper.messages["ERROR_AddressEmpty"]);
				if(!isInvalid) {form.address1.focus();}
				isInvalid=true;
			}
			if(!MessageHelper.isValidUTF8length(form["address1"].value, 100)){ 
				MessageHelper.tcpFormErrorHandleClient(form["address1"].id, MessageHelper.messages["ERROR_AddressTooLong"]); 
				if(!isInvalid) {form.address1.focus();}
				isInvalid=true;
			}
			if(!MessageHelper.isValidCharStr(form["address1"].value)){ 
				MessageHelper.tcpFormErrorHandleClient(form["address1"].id, MessageHelper.messages["ERROR_Address1HasSpecialChar"]); 
				if(!isInvalid) {form.address1.focus();}
				isInvalid=true;
			}			
			
			
			
			
			MessageHelper.tcpHideErrorHandleClientSpan(form["address2"].id);
			if(!MessageHelper.isValidUTF8length(form["address2"].value, 50)){ 
				MessageHelper.tcpFormErrorHandleClient(form["address2"].id, MessageHelper.messages["ERROR_AddressTooLong"]);
				if(!isInvalid) {form.address2.focus();}
				isInvalid=true;
			}
			if(!MessageHelper.isValidCharStr(form["address2"].value)){ 
				MessageHelper.tcpFormErrorHandleClient(form["address2"].id, MessageHelper.messages["ERROR_Address2HasSpecialChar"]);
				if(!isInvalid) {form.address2.focus();}
				isInvalid=true;
			}			
			
			
			
			MessageHelper.tcpHideErrorHandleClientSpan(form["city"].id);
			if((form["city"].value == "" || reWhiteSpace.test(form["city"].value))){ 
				MessageHelper.tcpFormErrorHandleClient(form["city"].id, MessageHelper.messages["ERROR_CityEmpty"]);
				if(!isInvalid) {form.city.focus();}
				isInvalid = true;
			}
			if(!MessageHelper.isValidUTF8length(form["city"].value, 128)){
				MessageHelper.tcpFormErrorHandleClient(form["city"].id, MessageHelper.messages["ERROR_CityTooLong"]);
				if(!isInvalid) {form.city.focus();}
				isInvalid = true;
			}
			if(!MessageHelper.isValidCharStr(form["city"].value)){
				MessageHelper.tcpFormErrorHandleClient(form["city"].id, MessageHelper.messages["ERROR_CityHasSpecialChar"]);
				if(!isInvalid) {form.city.focus();}
				isInvalid = true;
			}			
			
			
			
			MessageHelper.tcpHideErrorHandleClientSpan(form["state"].id);
			if(form["state"].value == "" || reWhiteSpace.test(form["state"].value)){
				MessageHelper.tcpFormErrorHandleClient(form["state"].id, MessageHelper.messages["ERROR_StateEmpty"]);
				if(!isInvalid) {form.state.focus();}
				isInvalid=true;
			}
			if(!MessageHelper.isValidUTF8length(form["state"].value, 128)){ 
				MessageHelper.tcpFormErrorHandleClient(form["state"].id, MessageHelper.messages["ERROR_StateTooLong"]);
				if(!isInvalid) {form.state.focus();}
				isInvalid= true;
			}
			
			
			
			
			
			MessageHelper.tcpHideErrorHandleClientSpan(form["country"].id);
			if(form["country"].value == "" || reWhiteSpace.test(form["country"].value)){
				MessageHelper.tcpFormErrorHandleClient(form["country"].id, MessageHelper.messages["ERROR_CountryEmpty"]);
				if(!isInvalid) {form.country.focus();}
				isInvalid=true;
			}
			if(!MessageHelper.isValidUTF8length(form["country"].value, 128)){ 
				MessageHelper.tcpFormErrorHandleClient(form["country"].id, MessageHelper.messages["ERROR_CountryTooLong"]);
				if(!isInvalid) {form.country.focus();}
				isInvalid= true;
			}
			
			
			
			
			
			MessageHelper.tcpHideErrorHandleClientSpan(form["addressField3"].id);
			/*if(form["addressField3"].value == "" || reWhiteSpace.test(form["addressField3"].value)){
				MessageHelper.tcpFormErrorHandleClient(form["addressField3"].id, MessageHelper.messages["ERROR_ZipCodeEmpty"]);
				if(!isInvalid) {form.addressField3.focus();}
				isInvalid=true;
			}
			if(!MessageHelper.isValidUTF8length(form["addressField3"].value, 40)){ 
				MessageHelper.tcpFormErrorHandleClient(form["addressField3"].id, MessageHelper.messages["ERROR_ZipCodeTooLong"]);
				if(!isInvalid) {form.addressField3.focus();}
				isInvalid= true;
			}
			if(!MessageHelper.isValidCharStr(form["addressField3"].value)){ 
				MessageHelper.tcpFormErrorHandleClient(form["addressField3"].id, MessageHelper.messages["ERROR_ZipCodeHasSpecialChar"]);
				if(!isInvalid) {form.addressField3.focus();}
				isInvalid= true;
			}*/			
			
			var isZipValid = AddressHelper.performZipValidation(form, true);
			
			
			
			MessageHelper.tcpHideErrorHandleClientSpan(form["phone1"].id);
			form["phone1"].value = form["phone1"].value.replace(/[^0-9]/g, "");
			if(form["phone1"].value == "" || reWhiteSpace.test(form["phone1"].value)){
				MessageHelper.tcpFormErrorHandleClient(form["phone1"].id, MessageHelper.messages["ERROR_PhonenumberEmpty"]);
				if(!isInvalid) {form.phone1.focus();}
				isInvalid=true;
			}
			if(!MessageHelper.isValidUTF8length(form["phone1"].value, 10)){ 
				MessageHelper.tcpFormErrorHandleClient(form["phone1"].id, MessageHelper.messages["ERROR_PhoneTooLong"]);
				if(!isInvalid) {form.phone1.focus();}
				isInvalid= true;
			}
			if(!MessageHelper.IsValidPhone2(form["phone1"].value)){
				MessageHelper.tcpFormErrorHandleClient(form["phone1"].id, MessageHelper.messages["ERROR_INVALIDPHONE"]);
				if(!isInvalid) {form.phone1.focus();}
				isInvalid = true;
			}
			if(!MessageHelper.isValidCharStr(form["phone1"].value)){
				MessageHelper.tcpFormErrorHandleClient(form["phone1"].id, MessageHelper.messages["ERROR_PhoneHasSpecialChar"]);
				if(!isInvalid) {form.phone1.focus();}
				isInvalid = true;
			}		
			
			
			
			
			/*
			if((form["primaryChangedFlag"].value == "1") && (form["defaultShippingMethodProvided"].value == "0") ){
				MessageHelper.tcpFormErrorHandleClient(form["primary"].id, MessageHelper.messages["ERROR_MYPLACE_SHIPPING_METHOD_NOT_PROVIDED"]);
				isInvalid=true;
			}	*/		
			
			if(isInvalid || !isZipValid){
				return false;
			}
			
			if(form["oldAddress1"].value != form["address1"].value ) {
				//user is changing address line 1
				form["address1ChangedFlag"].value = 1;
			}	
			
			setCurrentId(linkObj.id);
			
			try{
				executeSearch(form,5000,'tcpEditAddressFormSubmit');
			}catch(e){
				tcpEditAddressFormSubmit(form);
			}
	 }
	 
	 function tcpEditAddressFormSubmit(form){
		if(!submitRequest()){
			return;
		}
		 showProgressBar();
		 var params="&storeId="+MyAccountServicesDeclarationJS.storeId+
         "&catalogId="+MyAccountServicesDeclarationJS.catalogId+
         "&langId="+MyAccountServicesDeclarationJS.langId;
		$.ajax({
			type: "POST",
			url: getAbsoluteURL()+$('#EditAddress').attr("action"),
			data: (params ? params + "&" : "") +$('#EditAddress').serialize(),
			dataType: "html",//json
			timeout: 210000,
			success: function(result){
				cursor_clear();
				result = result.replace("/*","");
				result = result.replace("*/","");
				var obj = jQuery.parseJSON(result);
				MessageHelper.tcpHideErrorHandleClientSpan("WC_hidden");
				if(obj.errorMessage){
					MessageHelper.tcpFormErrorHandleClient('WC_hidden', obj.errorMessage);
				}else{
					var displayMessage = 'Address updated';
					if (((form["primaryChangedFlag"].value == 1) || (form["address1ChangedFlag"].value == 1))
							&&(form["defaultShippingMethodProvided"].value == "1"))  {
						displayMessage += " and your default shipping method is cleared, please reset your default shipping method.";
					}
					MessageHelper.tcpFormErrorHandleClient('WC_hidden', displayMessage);
					refreshDiv();
					setTimeout(function(){
						$("#updateTcpAccountDialog").dialog("close");
					},2000);
				}
			},
			error:function(XMLHttpRequest, textStatus, errorThrown)  {
				//console.debug(XMLHttpRequest.status+" "+XMLHttpRequest.statusText);
				//console.debug(XMLHttpRequest.responseText);
				//console.debug(errorThrown);
				//console.debug(textStatus);
				cursor_clear();
				return false;
			}
		});	
	 }
	 function refreshDiv(){
		 var params="&storeId="+MyAccountServicesDeclarationJS.storeId+
         "&catalogId="+MyAccountServicesDeclarationJS.catalogId+
         "&langId="+MyAccountServicesDeclarationJS.langId;
		 $.ajax({
				type: "POST",
				url: getAbsoluteURL()+"TCPMyAddressBookDisplayView",
				data: (params ? params + "&" : ""),
				dataType: "html",//json
				timeout: 210000,
				success: function(result){
			 		$("#tcp-myaddressbook-display").html(result);
			 		initLinkClick();
				},
				error:function(XMLHttpRequest, textStatus, errorThrown)  {
					//console.debug(XMLHttpRequest.status+" "+XMLHttpRequest.statusText);
					//console.debug(XMLHttpRequest.responseText);
					//console.debug(errorThrown);
					//console.debug(textStatus);
					return false;
				}
			});	
	 }
	 function tcpPrepareAddAddressFormSubmit(form, linkObj){
		 var reWhiteSpace = new RegExp(/^\s+$/);	   
		 
			form.firstName.value =form.firstName.value.Trim();
			form.lastName.value =form.lastName.value.Trim();
			form.address1.value =form.address1.value.Trim();
			form.address2.value =form.address2.value.Trim();
			form.city.value =form.city.value.Trim();
			form.state.value =form.state.value.Trim();
			form.zipCode.value =form.zipCode.value.Trim();
			form.country.value =form.country.value.Trim();
			form.phone1.value =form.phone1.value.Trim();
			
			if(form.primary &&  form.primary.checked){
			    form.primary.value = true;
			}
			else {
				form.primary.value = false;
			}				
			
			var isInvalid=false;
			MessageHelper.tcpHideErrorHandleClientSpan(form["firstName"].id);
			if(form["firstName"].value == "" || reWhiteSpace.test(form["firstName"].value)){
				MessageHelper.tcpFormErrorHandleClient(form["firstName"].id, MessageHelper.messages["ERROR_FirstNameEmpty"]);
				if(!isInvalid) {form.firstName.focus();}
				isInvalid=true;
			}
			if(!MessageHelper.isValidUTF8length(form["firstName"].value, 256)){ 
				MessageHelper.tcpFormErrorHandleClient(form["firstName"].id, MessageHelper.messages["ERROR_FirstNameTooLong"]);
				if(!isInvalid) {form.firstName.focus();}
				isInvalid= true;
			}
			if(MessageHelper.IsContainNumber(form.firstName.value)){ 
				MessageHelper.tcpFormErrorHandleClient(form.firstName.id, MessageHelper.messages["ERROR_NAMECONTAINNUMBER"]); 
				if(!isInvalid) {form.firstName.focus();}
				isInvalid= true;
			}
			if(!MessageHelper.isContainsOnlyAlphabet(form.firstName.value)){ 
				MessageHelper.tcpFormErrorHandleClient(form.firstName.id, MessageHelper.messages["ERROR_FirstNameHasSpecialChar"]); 
				if(!isInvalid) {form.firstName.focus();}
				isInvalid= true;
			}			
			
			
			
			MessageHelper.tcpHideErrorHandleClientSpan(form["lastName"].id);
			if(form["lastName"].value == "" || reWhiteSpace.test(form["lastName"].value)){
				MessageHelper.tcpFormErrorHandleClient(form["lastName"].id, MessageHelper.messages["ERROR_LastNameEmpty"]);
				isInvalid=true;
			}
			if(!MessageHelper.isValidUTF8length(form["lastName"].value, 256)){ 
				MessageHelper.tcpFormErrorHandleClient(form["lastName"].id, MessageHelper.messages["ERROR_LastNameTooLong"]);
				isInvalid= true;
			}
			if(MessageHelper.IsContainNumber(form.lastName.value)){ 
				MessageHelper.tcpFormErrorHandleClient(form.lastName.id, MessageHelper.messages["ERROR_NAMECONTAINNUMBER"]); 
				if(!isInvalid) {form.lastName.focus();}
				isInvalid= true;
			}
			if(!MessageHelper.isContainsOnlyAlphabet(form.lastName.value)){ 
				MessageHelper.tcpFormErrorHandleClient(form.lastName.id, MessageHelper.messages["ERROR_LastNameHasSpecialChar"]); 
				if(!isInvalid) {form.lastName.focus();}
				isInvalid= true;
			}			
			
			
			
			MessageHelper.tcpHideErrorHandleClientSpan(form["address1"].id);
			if(form["address1"].value == "" || reWhiteSpace.test(form["address1"].value)){ 
				MessageHelper.tcpFormErrorHandleClient(form["address1"].id, MessageHelper.messages["ERROR_AddressEmpty"]);
				if(!isInvalid) {form.address1.focus();}
				isInvalid=true;
			}
			if(!MessageHelper.isValidUTF8length(form["address1"].value, 100)){ 
				MessageHelper.tcpFormErrorHandleClient(form["address1"].id, MessageHelper.messages["ERROR_AddressTooLong"]); 
				if(!isInvalid) {form.address1.focus();}
				isInvalid=true;
			}
			if(!MessageHelper.isValidCharStr(form["address1"].value)){ 
				MessageHelper.tcpFormErrorHandleClient(form["address1"].id, MessageHelper.messages["ERROR_Address1HasSpecialChar"]); 
				if(!isInvalid) {form.address1.focus();}
				isInvalid=true;
			}			
			
			
			
			MessageHelper.tcpHideErrorHandleClientSpan(form["address2"].id);
			if(!MessageHelper.isValidUTF8length(form["address2"].value, 50)){ 
				MessageHelper.tcpFormErrorHandleClient(form["address2"].id, MessageHelper.messages["ERROR_AddressTooLong"]);
				if(!isInvalid) {form.address2.focus();}
				isInvalid=true;
			}
			if(!MessageHelper.isValidCharStr(form["address2"].value)){ 
				MessageHelper.tcpFormErrorHandleClient(form["address2"].id, MessageHelper.messages["ERROR_Address2HasSpecialChar"]);
				if(!isInvalid) {form.address2.focus();}
				isInvalid=true;
			}			
			
			
			
			
			MessageHelper.tcpHideErrorHandleClientSpan(form["city"].id);
			if((form["city"].value == "" || reWhiteSpace.test(form["city"].value))){ 
				MessageHelper.tcpFormErrorHandleClient(form["city"].id, MessageHelper.messages["ERROR_CityEmpty"]);
				if(!isInvalid) {form.city.focus();}
				isInvalid = true;
			}
			if(!MessageHelper.isValidUTF8length(form["city"].value, 128)){
				MessageHelper.tcpFormErrorHandleClient(form["city"].id, MessageHelper.messages["ERROR_CityTooLong"]);
				if(!isInvalid) {form.city.focus();}
				isInvalid = true;
			}
			if(!MessageHelper.isValidCharStr(form["city"].value)){
				MessageHelper.tcpFormErrorHandleClient(form["city"].id, MessageHelper.messages["ERROR_CityHasSpecialChar"]);
				if(!isInvalid) {form.city.focus();}
				isInvalid = true;
			}			
			
			
			
			
			MessageHelper.tcpHideErrorHandleClientSpan(form["state"].id);
			if(form["state"].value == "" || reWhiteSpace.test(form["state"].value)){
				MessageHelper.tcpFormErrorHandleClient(form["state"].id, MessageHelper.messages["ERROR_StateEmpty"]);
				if(!isInvalid) {form.state.focus();}
				isInvalid=true;
			}
			if(!MessageHelper.isValidUTF8length(form["state"].value, 128)){ 
				MessageHelper.tcpFormErrorHandleClient(form["state"].id, MessageHelper.messages["ERROR_StateTooLong"]);
				if(!isInvalid) {form.state.focus();}
				isInvalid= true;
			}
			
			
			
			
			
			MessageHelper.tcpHideErrorHandleClientSpan(form["country"].id);
			if(form["country"].value == "" || reWhiteSpace.test(form["country"].value)){
				MessageHelper.tcpFormErrorHandleClient(form["country"].id, MessageHelper.messages["ERROR_CountryEmpty"]);
				if(!isInvalid) {form.country.focus();}
				isInvalid=true;
			}
			if(!MessageHelper.isValidUTF8length(form["country"].value, 128)){ 
				MessageHelper.tcpFormErrorHandleClient(form["country"].id, MessageHelper.messages["ERROR_CountryTooLong"]);
				if(!isInvalid) {form.country.focus();}
				isInvalid= true;
			}
			
			
			
			
			
			MessageHelper.tcpHideErrorHandleClientSpan(form["addressField3"].id);
		/*	if(form["addressField3"].value == "" || reWhiteSpace.test(form["addressField3"].value)){
				MessageHelper.tcpFormErrorHandleClient(form["addressField3"].id, MessageHelper.messages["ERROR_ZipCodeEmpty"]);
				if(!isInvalid) {form.addressField3.focus();}
				isInvalid=true;
			}
			if(!MessageHelper.isValidUTF8length(form["addressField3"].value, 40)){ 
				MessageHelper.tcpFormErrorHandleClient(form["addressField3"].id, MessageHelper.messages["ERROR_ZipCodeTooLong"]);
				if(!isInvalid) {form.addressField3.focus();}
				isInvalid= true;
			}
			if(!MessageHelper.isValidCharStr(form["addressField3"].value)){ 
				MessageHelper.tcpFormErrorHandleClient(form["addressField3"].id, MessageHelper.messages["ERROR_ZipCodeHasSpecialChar"]);
				if(!isInvalid) {form.addressField3.focus();}
				isInvalid= true;
			}	*/		
			
			var isZipValid = AddressHelper.performZipValidation(form,true);
			
			
			
			
			MessageHelper.tcpHideErrorHandleClientSpan(form["phone1"].id);
			form["phone1"].value = form["phone1"].value.replace(/[^0-9]/g, "");
			if(form["phone1"].value == "" || reWhiteSpace.test(form["phone1"].value)){
				MessageHelper.tcpFormErrorHandleClient(form["phone1"].id, MessageHelper.messages["ERROR_PhonenumberEmpty"]);
				if(!isInvalid) {form.phone1.focus();}
				isInvalid=true;
			}
			if(!MessageHelper.isValidUTF8length(form["phone1"].value, 10)){ 
				MessageHelper.tcpFormErrorHandleClient(form["phone1"].id, MessageHelper.messages["ERROR_PhoneTooLong"]);
				if(!isInvalid) {form.phone1.focus();}
				isInvalid= true;
			}
			if(!MessageHelper.IsValidPhone2(form["phone1"].value)){
				MessageHelper.tcpFormErrorHandleClient(form["phone1"].id, MessageHelper.messages["ERROR_INVALIDPHONE"]);
				if(!isInvalid) {form.phone1.focus();}
				isInvalid = true;
			}
			if(!MessageHelper.isValidCharStr(form["phone1"].value)){
				MessageHelper.tcpFormErrorHandleClient(form["phone1"].id, MessageHelper.messages["ERROR_PhoneHasSpecialChar"]);
				if(!isInvalid) {form.phone1.focus();}
				isInvalid = true;
			}		
			
			
			

			/*
			if((form["primary"].value == "true") && (form["defaultShippingMethodProvided"].value == "0") ){
				MessageHelper.tcpFormErrorHandleClient(form["primary"].id, MessageHelper.messages["ERROR_MYPLACE_SHIPPING_METHOD_NOT_PROVIDED"]);
				isInvalid=true;
			}
			*/
			
			if(isInvalid || !isZipValid){
				return false;
			}
			
			setCurrentId(linkObj.id);
			
			try{
				executeSearch(form,5000,'tcpAddAddressFormSubmit');
			}catch(e){
				tcpAddAddressFormSubmit(form);
			}
		}	 
	 
	 function tcpAddAddressFormSubmit(form){
		 if(!submitRequest()){
				return;
		}
		 showProgressBar();
		 var params="&storeId="+MyAccountServicesDeclarationJS.storeId+
		 "&catalogId="+MyAccountServicesDeclarationJS.catalogId+
		 "&langId="+MyAccountServicesDeclarationJS.langId;
		 $.ajax({
			 type: "POST",
			 url: getAbsoluteURL()+$('#AddAddress').attr("action"),
			 data: (params ? params + "&" : "") +$('#AddAddress').serialize(),
			 dataType: "html",//json
			 timeout: 210000,
			 success: function(result){
			 cursor_clear();
			 result = result.replace("/*","");
			 result = result.replace("*/","");
			 var obj = jQuery.parseJSON(result);
			 MessageHelper.tcpHideErrorHandleClientSpan("WC_hidden");
			 if(obj.errorMessage){
				 MessageHelper.tcpFormErrorHandleClient('WC_hidden', obj.errorMessage);
			 }else{
				 var displayMessage = 'Address added';
				 if ((form["primary"].value == "true") && (form["defaultShippingMethodProvided"].value == "1")) {
					 displayMessage += " and your default shipping method is cleared, please reset your default shipping method.";
				 }
				 MessageHelper.tcpFormErrorHandleClient('WC_hidden', displayMessage);
				 refreshDiv();
				 setTimeout(function(){
					 $("#updateTcpAccountDialog").dialog("close");
				 },2000);
			 }
		 },
		 error:function(XMLHttpRequest, textStatus, errorThrown)  {
				//console.debug(XMLHttpRequest.status+" "+XMLHttpRequest.statusText);
				//console.debug(XMLHttpRequest.responseText);
				//console.debug(errorThrown);
				//console.debug(textStatus);
				cursor_clear();
				return false;
		 }
		 });
	 }
	//Clearing the value in the filed
	 function clearField(obj)
	 {
	 	
	 	obj.value="";
	 	
	 }
	 function tcpDeleteAddress(url,addressTarget){
		 if(!window.confirm('delete it ?')){
			 return;
		 }
		 
			setCurrentId(addressTarget.id);
			if(!submitRequest()){
				return;
			}
			showProgressBar();
		 
		 $.ajax({
			 type: "POST",
			 url: url,
			 data: null,
			 dataType: "html",//json
			 timeout: 210000,
				 success: function(result){
				 cursor_clear();
				 result = result.replace("/*","");
				 result = result.replace("*/","");
				 var obj = jQuery.parseJSON(result);
				 MessageHelper.tcpHideErrorHandleClientSpan(addressTarget.id);
				 if(obj.errorMessage){
					 MessageHelper.tcpFormErrorHandleClient(addressTarget.id, obj.errorMessage);
				 }else{
					 MessageHelper.tcpFormErrorHandleClient(addressTarget.id, 'Delete successful');
					 setTimeout(function(){
						 $("#tcp_address_"+addressTarget.id).remove();
						// refreshDiv();
					 },2000);
				 }
			 },
			 error:function(XMLHttpRequest, textStatus, errorThrown)  {
					//console.debug(XMLHttpRequest.status+" "+XMLHttpRequest.statusText);
					//console.debug(XMLHttpRequest.responseText);
					//console.debug(errorThrown);
					//console.debug(textStatus);
					cursor_clear();
					return false;
			 }
		 });
		 
		 
	 }
	 
	 
	 function tcpShowOrderHistoryTab(url, linkObj){
		 setCurrentId(linkObj.id);
			if(!submitRequest()){
				return;
			}
			showProgressBar();
		 
		 $.ajax({
			 type: "POST",
			 url: url,
			 data: null,
			 dataType: "html",//json
			 timeout: 210000,
			 success: function(result){
				 cursor_clear();
				 //result = result.replace("/*","");
				 //result = result.replace("*/","");
				 // Sometime user has WC_AUTHENTICATION_-1002 cookie as well as users session specific cookie WC_AUTHENTICATION_12345, Int that case server is not sending any error. We should paint server response if there is no error from server  
				 if(result.indexOf("TCPGeneralError") != -1){
					 //alert(getUserCookieErrorMsg(getUserCookieError()));
					 document.location.href="Logoff?langId="+CommonContextsJS.langId+"&storeId="+CommonContextsJS.storeId+"&catalogId="+CommonContextsJS.catalogId+"&URL=LogonForm";
				 }else{
					 $("li#orderHistory").html(result);
				 }
			 },
			 error:function(XMLHttpRequest, textStatus, errorThrown)  {
					//console.debug(XMLHttpRequest.status+" "+XMLHttpRequest.statusText);
					//console.debug(XMLHttpRequest.responseText);
					//console.debug(errorThrown);
					//console.debug(textStatus);
					cursor_clear();
					return false;
			 }
		 });
		 
	 }
	 
	 function tcpOpenOrderInAnothersite(url, storeId){
		 if(!window.confirm('You will navigate to another store, are you sure ?')){
			 return;
		 }
		 document.location.href = url;
	 }
	 
	 function changePasswordServiceReset(form){
		 	 var reWhiteSpace = new RegExp(/^\s+$/);	  
		 	 var re = /(?=.*\d)(?=.*[a-zA-Z]).{8,}/;
		 	 var passwordPattern =  /^(?=.*\d).{8,}$/;
	         var logonPasswordOld=form.logonPasswordOld;
	         var logonPassword=form.logonPassword;
	         var logonPasswordVerify=form.logonPasswordVerify;
	         logonPasswordOld.value =logonPasswordOld.value.replace(/(^\s*)|(\s*$)/g, "");;
	         logonPassword.value =logonPassword.value.replace(/(^\s*)|(\s*$)/g, "");;
	         logonPasswordVerify.value =logonPasswordVerify.value.replace(/(^\s*)|(\s*$)/g, "");
			 var isInvalid=false;
			 MessageHelper.tcpHideErrorHandleClientSpan(null,'WC_hidden');
			/*check whether the values in password and verify password fields match, if so, update the password. */ 
			 MessageHelper.tcpHideErrorHandleClientSpan(logonPasswordOld.id);
			if(logonPasswordOld.value == "" || reWhiteSpace.test(logonPasswordOld.value)){
				MessageHelper.tcpFormErrorHandleClient(logonPasswordOld.id, MessageHelper.messages["ERROR_CurrentPasswordEmpty"]);
				isInvalid=true;
			}
			MessageHelper.tcpHideErrorHandleClientSpan(logonPassword.id);
			if(logonPassword.value == "" || reWhiteSpace.test(logonPassword.value)){
				MessageHelper.tcpFormErrorHandleClient(logonPassword.id, MessageHelper.messages["ERROR_PasswordEmpty"]);
				isInvalid=true;
			}		
			
			MessageHelper.tcpHideErrorHandleClientSpan(logonPasswordVerify.id);
			if(logonPasswordVerify.value == "" || reWhiteSpace.test(logonPasswordVerify.value)){
				MessageHelper.tcpFormErrorHandleClient(logonPasswordVerify.id, MessageHelper.messages["ERROR_VerifyPasswordEmpty"]);
				isInvalid=true;
			}
			if(logonPassword.value!= form.logonPasswordVerify.value){
			   MessageHelper.tcpFormErrorHandleClient(form.logonPasswordVerify.id, MessageHelper.messages["PWDREENTER_DO_NOT_MATCH"]);
			   isInvalid=true;
			}
			if(logonPassword.value != "" && logonPassword.value.search(passwordPattern) == -1){
				MessageHelper.tcpFormErrorHandleClient(form.logonPassword.id, MessageHelper.messages["PWORD_EXPIRED"]);
				isInvalid = true;
			}
			if(isInvalid){
				return false;
			}
			if(logonPassword.value != "" && !re.test(logonPassword.value)){
				MessageHelper.tcpFormErrorHandleClient(logonPassword.id, MessageHelper.messages["ERROR_INVALID_PWD"]);
				isInvalid=true;
				return false;
			}
			submitForgotPasswordUpdateForm9(form);
	 }
	 
	 function submitForgotPasswordUpdateForm9(form){
			setCurrentId("WC_TCPPasswordUpdateForm_Link_1");
			if(!submitRequest()){
				return;
			}
			showProgressBar();
			setTimeout(function(){
				$("#progress_bar").hide();
			},2000);
			form.submit(); 
		}


	  function submitMyOrderHistoryPaginatedSearch(pageIndex, pageSize){
		/* Handles multiple clicks */
		if(!submitRequest()){
			return;
		}
		cursor_wait();
		
		showProgressBar();
		 var params="&storeId="+MyAccountServicesDeclarationJS.storeId+
		 "&catalogId="+MyAccountServicesDeclarationJS.catalogId+
		 "&langId="+MyAccountServicesDeclarationJS.langId;
		 $.ajax({
			 type: "POST",
			 url: getAbsoluteURL()+"TCPMyOrderHistoryDisplayContentView?currentPage="+pageIndex+"&pageSize="+pageSize,
			 data: (params ? params + "&" : "") ,
			 dataType: "html",//json
			 timeout: 210000,
			 success: function(result){
				cursor_clear();
				 $("#myorderhistory_display_div").html(result);
				 //refreshDiv();
				 
			 },
			 error:function(XMLHttpRequest, textStatus, errorThrown)  {
				//console.debug(XMLHttpRequest.status+" "+XMLHttpRequest.statusText);
				//console.debug(XMLHttpRequest.responseText);
				//console.debug(errorThrown);
				//console.debug(textStatus);
				cursor_clear();
				return false;
			}
		 });
	  }
 
	 
	  function initShippingMod(Obj){
		 var teps=document.getElementsByName("shipModeId");
		 for(var i=0;i<teps.length;i++){
			 if (teps[i].id==Obj.id)
				 {
				 teps[i].checked = true;
				 }
			 else {
				 teps[i].checked = false;
			 }
		 }
		    //var group = ".ship-method input:checkbox[name='"+Obj.attr("name")+"']";
		    //$(group).attr("checked",false);
		    //Obj.attr("checked",true);
		}
	  
	  function tcpShowPointsHistoryTab(url, linkObj){
		  	
		  	if(!pointHistoryClicked){
		  		pointHistoryClicked = true;
		  		setCurrentId(linkObj.id);
				if(!submitRequest()){
					return;
				}
				showProgressBar();
			 
			 $.ajax({
				 type: "POST",
				 url: url,
				 data: null,
				 dataType: "html",//json
				 timeout: 210000,
				 success: function(result){
					 cursor_clear();
					 //result = result.replace("/*","");
					 //result = result.replace("*/","");
					 $("li#pointsHistory").html(result);
					 //Make Tab Active
					 $('#WC_TCPMyPlace_Formlink_PointsrHistoryTab').parent().siblings().children('a').removeClass('active');
					 $('#WC_TCPMyPlace_Formlink_PointsrHistoryTab').addClass('active');
					 //Show Tab Content & add active class
					 $('#pointsHistory').show().addClass('active').siblings().hide().removeClass('active'); 
				 },
				 error:function(XMLHttpRequest, textStatus, errorThrown)  {
						//console.debug(XMLHttpRequest.status+" "+XMLHttpRequest.statusText);
						//console.debug(XMLHttpRequest.responseText);
						//console.debug(errorThrown);
						//console.debug(textStatus);
						cursor_clear();
						return false;
				 }
			 });
		  	}
		 }

	  function checkLoyaltyExistingCustomer(form,email1,myPlaceId) {


		 var params = [];
		 params.email1 = email1;
		 params.myPlaceId =myPlaceId;
		 params.storeId = 	 CommonContextsJS.storeId;
		 params.langId = 	 CommonContextsJS.langId;
			
		 
		 wc.service.invoke("TCPAjaxCheckLoyaltyUserExists",params); 
		
	  }
	  
function showEidtEmployeeHtml(linkId,notCheckbox){
	if($("#updateTcpAccountDialog")){
		$("#updateTcpAccountDialog").html("");
	}
	if(notCheckbox){
		setCurrentId(linkId);
	}else{
		setCurrentId("employee");
	}
	if(!submitRequest()){
		return;
	}
	showProgressBar();
	$(".id-num").hide();
	if(linkId=='TcpRemoveEmployeeId'){
		$("#employee").removeAttr("checked");
		if($.trim($("#tcpEditEmployeeFormDiv").html())!=""){
			$("#tcpEditEmployeeFormDiv").hide();
		}
	}else if (linkId == 'TcpUpdateEmployeeId'){
		$("#tcpEditEmployeeFormDiv").hide();
	}else{
		$("#tcpEditEmployeeFormDiv").show();
	}

	$.ajax({
		url: $('#'+linkId).attr('href').replace('#','')+ '&EditEmployeeId=0 .modal-contentEditMyaccount',
		type: 'GET',
		dataType:'html',
		success: function(data) {
			if($("#updateTcpAccountDialog")){
				$("#updateTcpAccountDialog").html("");
			}
			cursor_clear();
			if(data!=null){
				$("#tcpEditEmployeeFormDiv").html($.trim(data.replace("modal-content","")));
				if(linkId=='TcpRemoveEmployeeId'){
					$("#tcpEditEmployeeFormDiv").hide();
					
					
					
					
					
					
					prepareEditFormSubmit('EditEmployeeId',document.editRegister,'','');
					
					return false;
				}else{
					$("#tcpEditEmployeeFormDiv").show();
					$(".required-info").hide();
				}
				$(".edit_myplace_cancel").hide();
				$(".id-num").hide();
			}
			initLinkClick();
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			// console.debug(XMLHttpRequest.status+" "+XMLHttpRequest.statusText);
			// console.debug(XMLHttpRequest.responseText);
			// console.debug(errorThrown);
			// console.debug(textStatus);
			cursor_clear();
		},
		complete: function(XMLHttpRequest, textStatus){
			// console.debug(XMLHttpRequest.status+" "+XMLHttpRequest.statusText);
			// console.debug(XMLHttpRequest.responseText);
			// console.debug(textStatus);
			cursor_clear();
		}
	}); 

	return false;
}
