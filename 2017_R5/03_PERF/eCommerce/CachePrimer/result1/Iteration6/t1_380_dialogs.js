$(document).ready(function() {
	$('a.openModal, area.openModal').each(function() {
		var $link = $(this);
		//$link.preventDefault();
        
		$link.click(function() {
			if($link.attr("id")!=null&&$link.attr("id")!=""){
				setCurrentId($link.attr('id'));
			}else{
				$link.attr({id:"TCPCommon_dialogId_link"});
				setCurrentId($link.attr('id'));
			}
			if(!submitRequest()){
				return;
			}
			showProgressBar();
			//$('.selector').dialog({ autoOpen: false });
			//dialog( 'destroy' ) 
			setTimeout(function(){
				$("#progress_bar").hide();
			},2000);
		var $dialog = $('<div id="TCPCommon_dialogId"></div>')
			.load($link.attr('href') + ' .modal-content'
			,
						function (responseText, textStatus, XMLHttpRequest) {    
							cursor_clear();
							if (textStatus == "success") {        
							
								// to allow thumbnail switch script to work inside product detail modals
								$dialog.bind( "dialogopen", function(event, ui) {
									CloudZoom.quickStart();
									$(".thumbnail-zoom").click(function(evt) {
										evt.preventDefault();
										$("#product-xlarge").empty().append(
											$("<img>", { src: $(this).attr('data-target')})
										);
									});
									
									// to allow outfit carousel to work inside outfit detail modals	
									$( "#carousel").rcarousel({
										margin: 10, visible: 4, step: 1, speed: 300
									});		
								
									try {
										// to allow image carousel to work inside Denim Shop 2013 compare fit modal		
										$('#ca-container').contentcarousel({
											sliderSpeed: 500,// speed for the sliding animation
											scroll: 1 // number of items to scroll at a time
										});
									} catch(e) {
										// don't do anything	
									}
								});
								
								
								$dialog.dialog('open');
									
							}  if (textStatus == "error") {        
							}   
						} 
			
			)	
			.dialog({
				autoOpen: false,
				hide:true,
				show:true,
			//	dialogClass: 'main-dialog-class',
				close: function() {
				 // Remove the dialog elements
					$(this).dialog("destroy");
	                // Remove the left over element (the original div element)
					$(this).remove();
				},
				modal:true,
				open: function (event, ui) { window.setTimeout(function () {
       			jQuery(document).unbind('mousedown.dialog-overlay').unbind('mouseup.dialog-overlay'); }, 100);
				},
				title: $link.attr('title'),
				resizable:false,
				draggable:false,
				width: 'auto',
				height: 'auto'
			});


			return false;		

         	
		});	
	});
});