if (typeof (TCPBriteVerify) == "undefined" || !TCPBriteVerify) {
	wc.service.declare( {
		id : "TCPAjaxBriteVerifyService",
		actionId : "TCPAjaxEmailVerificationCmd",
		url : getAbsoluteURL() + "TCPAjaxEmailVerificationCmd",
		formId : ""

		/**
		 * display a success message
		 * 
		 * @param (object)
		 *            serviceResponse The service response object, which is the
		 *            JSON object returned by the service invocation
		 */
		,
		successHandler : function(serviceResponse) {

			response = JSON.parse(serviceResponse.status);
			console.log(response);
			TCPBriteVerify.createResponseField(response);
			var emailElementId = MessageHelper.currentEmailElementId;
			
			if (response.status != "invalid") {
				MessageHelper.currentEmailElementId = "";
			} else {
				MessageHelper.tcpFormErrorHandleClient(
						MessageHelper.currentEmailElementId,
						TCPBriteVerify.statusCode[response.error_code]
								.toString());

				// count will be set in the billing flow
				if (BV_max_attempts > 0 && response.count == BV_max_attempts) {
					MessageHelper.currentEmailElementId = "";
				}
			}

			if(typeof(submitForm) != "undefined" && submitForm == true) {
				submitForm = false;
				if( MessageHelper.currentEmailElementId != "" ) {
					$('#'+emailElementId).focus();
				}
				else{
					$('#'+emailElementId).closest('form').find('a[id*=Submit]')[0].click();
				}
			}
			if(typeof(reviewOrder) != "undefined" && reviewOrder == true){

				reviewOrder = false;
				if( MessageHelper.currentEmailElementId != "" ) {
					$('#'+emailElementId).focus();
				}
				else{
					$('#billingPageNext').click();
				}
			} 
			

		}

		/**
		 * display an error message
		 * 
		 * @param (object)
		 *            serviceResponse The service response object, which is the
		 *            JSON object returned by the service invocation
		 */
		,
		failureHandler : function(serviceResponse) {
			serviceResponse.status = "no_response";
			TCPBriteVerify.createResponseField(serviceResponse);

			var emailElementId = MessageHelper.currentEmailElementId;
			
			MessageHelper.currentEmailElementId = "";
			if(typeof(submitForm) != "undefined" && submitForm == true) {
				submitForm = false;
				if( MessageHelper.currentEmailElementId != "" ) {
					$('#'+emailElementId).focus();
				}
				else{
					$('#'+emailElementId).closest('form').find('a[id*=Submit]')[0].click();
				}
			}
			if(typeof(reviewOrder) != "undefined" && reviewOrder == true){

				reviewOrder = false;
				if( MessageHelper.currentEmailElementId != "" ) {
					$('#'+emailElementId).focus();
				}
				else{
					$('#billingPageNext').click();
				}
			}		
		}

	});
	/**
	 * declaring a service to call email verification command from email us
	 * modal
	 */
	wc.service.declare( {
		id : "TCPAjaxBriteVerifyService_emailus",
		actionId : "TCPAjaxEmailVerificationCmd",
		url : getAbsoluteURL() + "TCPAjaxEmailVerificationCmd",
		formId : ""

		/**
		 * display a success message
		 * 
		 * @param (object)
		 *            serviceResponse The service response object, which is the
		 *            JSON object returned by the service invocation
		 */
		,
		successHandler : function(serviceResponse) {

			response = JSON.parse(serviceResponse.status);
			
			TCPBriteVerify.createResponseField(response);
			if (response.status != "invalid") {
				document.signupFormPage.submit();
			} else {
				MessageHelper.tcpFormErrorHandleClient(
						MessageHelper.currentEmailElementId,
						TCPBriteVerify.statusCode[response.error_code]
								.toString());

				// count will be set in the billing flow
				if (BV_max_attempts_email_us > 0
						&& response.count == BV_max_attempts_email_us) {
					document.signupFormPage.submit();
				}
			}

		}

		/**
		 * display an error message
		 * 
		 * @param (object)
		 *            serviceResponse The service response object, which is the
		 *            JSON object returned by the service invocation
		 */
		,
		failureHandler : function(serviceResponse) {
			serviceResponse.status = "no_response";
			TCPBriteVerify.createResponseField(serviceResponse);
			document.signupFormPage.submit();
		}

	});

	/**
	 * declaring a service to call email verification command from newsletter
	 * sign up page
	 */
	wc.service.declare( {
		id : "TCPAjaxBriteVerifyService_signup",
		actionId : "TCPAjaxEmailVerificationCmd",
		url : getAbsoluteURL() + "TCPAjaxEmailVerificationCmd",
		formId : ""

		/**
		 * display a success message
		 * 
		 * @param (object)
		 *            serviceResponse The service response object, which is the
		 *            JSON object returned by the service invocation
		 */
		,
		successHandler : function(serviceResponse) {

			response = JSON.parse(serviceResponse.status);

			TCPBriteVerify.createResponseField(response);
			if (response.status != "invalid") {
				document.forms["signupFormPage_1"].submit();
			} else {
				MessageHelper.tcpFormErrorHandleClient(
						MessageHelper.currentEmailElementId,
						TCPBriteVerify.statusCode[response.error_code]
								.toString());

				// count will be set in the billing flow
				if (BV_max_attempts_news_signup > 0
						&& response.count == BV_max_attempts_news_signup) {
					document.forms["signupFormPage_1"].submit();
				}
			}

		}

		/**
		 * display an error message
		 * 
		 * @param (object)
		 *            serviceResponse The service response object, which is the
		 *            JSON object returned by the service invocation
		 */
		,
		failureHandler : function(serviceResponse) {
			serviceResponse.status = "no_response";
			TCPBriteVerify.createResponseField(serviceResponse);
			document.forms["signupFormPage_1"].submit();
		}

	});
	TCPBriteVerify = {

		//Flag to turn of BV from client side
		isClientSideBVEnabled_NEWS_SIGN_UP : true,
		
		statusCode : {},
		/**
		 * This function is used to initialize the statusCode object with all
		 * the required messages. It is used to setup a JS object with any
		 * key/value.
		 * 
		 * @param (string)
		 *            key The key used to access this statusCode.
		 * @param (string)
		 *            msg The message in the correct language.
		 * 
		 */
		setStatusCode : function(key, msg) {
			this.statusCode[key] = msg;
		},
		responseCode : "",
		createResponseField : function(response) {

			var responseCode = response.status;
			if (responseCode != "no_response" && response.exception == null) {

				if (response.error_code != null) {

					responseCode += ":" + response.error_code;
				} else {

					responseCode += ":";
				}
				if (response.disposable) {
					responseCode += ":true";
				} else {

					responseCode += ":false";
				}
				if (response.role_address) {

					responseCode += ":true";
				} else {

					responseCode += ":false";
				}
			}
			if ($('#' + MessageHelper.currentEmailElementId).parents('form')
					.children('input[name=response]').length == 0) {
				var responseField = $('<input />');
				responseField.attr('type', 'hidden');
				responseField.attr('name', 'response');
				responseField.attr('value',responseCode);
				$('#' + MessageHelper.currentEmailElementId).parents('form')
						.append(responseField);

			} else {
				var responseField = $('#' + MessageHelper.currentEmailElementId)
						.parents('form').children('input[name=response]');
				responseField.attr('value', responseCode);
			}
		}

	}
}
