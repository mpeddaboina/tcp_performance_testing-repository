if (!$.fn.CloudZoom) {
    $.ajaxSetup({cache:true});
	$('head').append('<link rel="stylesheet" href="/wcsstore/GlobalSAS/css/tcp/cloudzoom.css" type="text/css" />');
	$('head').append('<script type="text/javascript" src="/wcsstore/GlobalSAS/javascript/tcp/cloudzoom.js" />');
}

$(document).ready( function(){
	dialogReplaceInsecureLinks();
	dialogAttachModalHandlers();
	$.ajaxSetup({cache:true})
});

function dialogReplaceInsecureLinks() {
	//log("->dialogReplaceInsecureLinks()" );

	$('.openModal1, .openOutfitModal').each(function(){
   		if('https:' == document.location.protocol){
         	var src = $(this).attr('link');
         	if(src){
         		src=src.replace('http:','https:');
         		$(this).attr('link',src);
         	}
         }
	});

	$('.openModal2, .openProductModal').each(function(){
  		if('https:' == document.location.protocol){
        	var src = $(this).attr('link');
        	if(src){
        		src=src.replace('http:','https:');
        		$(this).attr('link',src);
        	}
        }
    });

	$('.openRegularLink ').each(function(){
  		if('https:' == document.location.protocol){
        	var src = $(this).attr('link');
        	if(src){
        		src=src.replace('http:','https:');
        		$(this).attr('link',src);
        	}
        }
    });
}

window.modalHandlerQV = null;


function dialogAttachModalHandlers() {
	//log("->dialogAttachModalHandlers()" );

	//This refers to outfit  modals, name should be renamed to .openOutfitModal
	$(".openModal1, .openOutfitModal").on(touchStartOrClick, function() {
		var openLink = this;
		var linkAttribute = $(this).attr('link');

		if (( linkAttribute != null ) && ( linkAttribute != "" )) {

			$("#product-quickview").empty();
			$("#outfit-quickview").empty();
			
			Rapp.Ropis.ProductQuickView.open(Rapp.Util.QueryString.get("productId", $(this).attr("link") || $(this).attr("href")), {withCompleteTheLook: true});
//			$.ajax({
//				url: linkAttribute,
//				type: 'GET',
//				dataType:'html',
//				success: function(data) {
//					if(data!=null){
//						$("#outfit-quickview").html(data);
//						$('#bundlePageModal').css('display','none');
//					}
//					//showPoints();
//					// $("#product-quickview .product-description").before($(".my-wishlist:eq(0)").clone(true));
//					// $(".my-wishlist:eq(1)").show();
//				},
//				error: function(XMLHttpRequest, textStatus, errorThrown) {
//					alert(textStatus);
//				},
//				complete: function(XMLHttpRequest, textStatus) {
//				}
//			});
//
//			$("#outfit-quickview" ).dialog({
//				position: ['center', 50],
//				width: ($(window).width() >= 1400) ? '925px' : '760px',
//				height: 'auto',
//				resizable:false,
//				draggable:true,
//				modal: true,
//				close: function() { openLink.focus(); },
//				title:'<img id="bundlePageModal"  src="'+getImageDirectoryPath()+'images/tcp/ajax-loading.gif" />'
//			});

		}//end if conditional check for null value
    });

	//This refers to product modals, name should be renamed to .openProductModal
	$(document).off("click.quckview touchstart.quckview").on("click.quckview touchstart.quckview", ".openModal2, .openProductModal", function(e) {
		e.stopPropagation();
		e.preventDefault();
		var openLink = this,
		linkAttribute = $(this).attr('link');

		if (( linkAttribute != null ) && ( linkAttribute != "" )) {

			$("#product-quickview-modal").empty();
			$("#outfit-quickview").empty();

			Rapp.Ropis.ProductQuickView.open(Rapp.Util.QueryString.get("productId", $(this).attr("link") || $(this).attr("href")));

			/*
			$.ajax({
		        url: linkAttribute,
		        type: 'GET',
		        dataType:'html',
				success: function(data) {
					console.log("success modal");

					if(data!=null){
						$("#product-quickview-modal").html(data);
						$('#productModal').css('display','none');
					}
					var swatchid = 'product_'+document.getElementById('prodId').value;
					showPoints(swatchid);

					// not allowing to clone the CTA while on whislist until we introduce a solution for the component inclusion.
					// I'll try the same approach on PDP for now. becuase the target of the insertion get lost
					if (window.location.href.indexOf("wishlist") == -1 || $(".product-details").length >= 1) {
						App.AddWishlist.initialize();
						$("#online-only-icon").before($(".my-wishlist:eq(0)").clone(true));
						$(".my-wishlist:eq(1)").show();
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					alert(textStatus);
				},
				complete: function(XMLHttpRequest, textStatus){
				}
		    });

			$("#product-quickview-modal" ).dialog({
				position: ['center', 50],
				width: ($(window).width() >= 1400) ? '925px' : '760px',
				height:'auto',
				resizable:false,
				draggable:true,
				modal: true,
				close: function() {
					openLink.focus();
				},
				title:'<img id="productModal"  src="'+getImageDirectoryPath()+'images/tcp/ajax-loading.gif" />'
			});
			*/

		} //end if conditional check for null value
	});


	$('.openRegularLink ').on( touchStartOrClick, function(e){
		e.preventDefault();
  		window.location = $(this).attr('link');
    });
}
