/**
 * @authors Constantin Bigu, Carlos Henriquez
 * @description This houses espots controllers. The file is loaded in the footer across the app. 
 * @restrictions Use vanilla JS 
 */

(function(){

    /* MPR Promo Banner Above Header - cookies */
    var mprNode;

    var MPRinterval = setInterval(function() {
        mprNode = document.getElementById('header-global-banner-container');

        if (mprNode) {
            requestSubmitted = false;

            if (window.location.href.indexOf('ResetPasswordGuestErrorView') > 0) {
                return;
            }

            var uuidCookieString = 'WC_USERACTIVITY_';
            var uuid = getUUID(uuidCookieString);
            var mprAboveHead = _readCookie('mprAboveHead_' + uuid);
            if (!mprAboveHead) {
                mprNode.style.display = 'block';
            } else {
                mprNode.style.display = 'none';
            }

            document.getElementById('myPlace-rewards-close').addEventListener('click', function() {
                mprNode.style.display = 'none';
                
                var cookieDomain = '.childrensplace.com';
                var currentDate = new Date();
                var cookieValue = currentDate.toGMTString();
                _setCookie('mprAboveHead_' + uuid, cookieValue, cookieDomain);
            });

            function getUUID(uuidCookieString) {
                var _UUID = _findCookieStartingWithString(uuidCookieString);

                if (_UUID) {
                    return _UUID.split(',')[0];
                } 

                return '-1002';
            }

            clearInterval(MPRinterval);
        }
    }, 300);

    /* Free Shipping Global Header banner Modal Functionality */
    var detailsModal = setInterval(function() {
        var seeDetails = document.querySelector('.openModal-FS');
        var modalContent = document.querySelector('.fs-global-modal');
        var closeModalButton = document.querySelector('.button-overlay-close');

        if (seeDetails) {
            function openModal(e) {
                e.preventDefault();
                modalContent.style.display = 'block';
            }

            function closeModal(e) {
                e.preventDefault();
                modalContent.style.display = 'none';
            }
            closeModalButton.addEventListener('click', closeModal);
            seeDetails.addEventListener('click', openModal);

            clearInterval(detailsModal);
        }
    }, 300);
   

    /* Free Shipping Bag/Cart Modal Functionality */
    var bagDetailsModal = setInterval(function() {
        var seeDetailsBag = document.querySelector('.openModal-bag-FS');
        var modalContentBag = document.querySelector('.bag-modal');
        var closeModalButtonBag = document.querySelector('.close-bag-modal');

        if (seeDetailsBag) {
            function openModal(e) {
                e.preventDefault();
                modalContentBag.style.display = 'block';
            }

            function closeModal(e) {
                e.preventDefault();
                modalContentBag.style.display = 'none';
            }
            closeModalButtonBag.addEventListener('click', closeModal);
            seeDetailsBag.addEventListener('click', openModal);
            clearInterval(bagDetailsModal);
        }
    }, 300);


    /* ---------------- UTILS -----------------*/
    function _setCookie(cookieName, cookieValue, cookieDomain){
        var d = new Date();

        d.setTime(d.getTime() + 10 * 24 * 60 * 60 * 1000);
        document.cookie = cookieName + '=' + cookieValue + '; path=/' + '; domain=' + cookieDomain + '; expires=' + d.toGMTString();
    };

    function _readCookie(cookieName) {
        var a_all_cookies = document.cookie.split(';');

        var a_temp_cookie = '';
        var cookie_name = '';
        var cookie_value = '';
        var b_cookie_found = false;

        for (i = 0; i < a_all_cookies.length; i++) {
            a_temp_cookie = a_all_cookies[i].split('=');
            cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g, '');
            if (cookie_name == cookieName) {
                b_cookie_found = true;
                if (a_temp_cookie.length > 1) {

                    cookie_value = unescape(a_temp_cookie[1].replace(/^\s+|\s+$/g, ''));
                }
                return cookie_value;
                break;
            }
            a_temp_cookie = null;
            cookie_name = '';
        }
        if (!b_cookie_found) {
            return null;
        }
    };

    function _findCookieStartingWithString(keyword) {
        var cookieArr = document.cookie.split(';'),
            cookies = {},
            name_value,
            equals_pos,
            name,
            value;

        for (var i = 0; i < cookieArr.length; i++) {
            name_value = cookieArr[i];
            equals_pos = name_value.indexOf('=');
            name = unescape(name_value.slice(0, equals_pos)).trim();
            value = unescape(name_value.slice(equals_pos + 1));

            cookies[name] = value;
        }

        for (name in cookies) {
            var value = cookies[name];
            if (name.indexOf(keyword) == 0) {
                return value;
            }
        }
    }  

})();