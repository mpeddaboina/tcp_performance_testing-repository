//-----------------------------------------------------------------
// Licensed Materials - Property of IBM Chad
//
// WebSphere Commerce
//
// (C) Copyright IBM Corp. 2007, 2010 All Rights Reserved.
//
// US Government Users Restricted Rights - Use, duplication or
// disclosure restricted by GSA ADP Schedule Contract with
// IBM Corp.
//-----------------------------------------------------------------

/**
 *@fileOverview This javascript file defines the methods ,variables used in ishipping dialog.
 */
InternationalShippingJS={
		ishippingdialog:null,
		url:null,
		shopUrl:null,
		shopCartUrl:null,
		popupFlag:null,
		USURL:null,
		CAURL:null,
		countryFlagPath:null,
		shippingFlag:false,
		flagArray:{ "in":"IN.gif", "cn": "CN.gif", "us": "US.gif", "globalsas": "AU.gif", "ca": "CA.gif"},
		langArray:{ "en_US":"<b>English</b> | Espa&ntilde;ol","en_us":"<b>English</b> | Espa&ntilde;ol", "es_ES": "English | <b>Espa&ntilde;ol</b>",  "es_es": "English | <b>Espa&ntilde;ol</b>", "fr_FR": "English | <b>Fran&ccedil;ais</b>",  "fr_fr": "English | <b>Fran&ccedil;ais</b>", "en_ca": "<b>English</b> | Fran&ccedil;ais"},
		flxArray:null,
		flcArray:null,
		disableLanguageTranslationFlag: false,
		buildModalLink:function() 
		{
			$('a.openModalIShipping').each(function() {
				var $link = $(this);
				$link.click(function() {
					setCurrentId($link.attr('id'));
					if(!submitRequest()){
						return;
					}
					
					if ($link.attr('shippingFlag') == undefined) {
						//alert($link.attr('shippingFlag'));
						InternationalShippingJS.setShippingFlag(false);
					} else {
						InternationalShippingJS.setShippingFlag(true);
					}
					
					
					if($("#iShippingModalDialog")){
						$("#iShippingModalDialog").empty();
						$("#iShippingModalDialog").remove();
					}
					//showProgressBar();
					
					//setTimeout(function(){$("#progress_bar").hide();
						//cursor_clear();
					//},2000);
					
					var $dialog = $('<div id="iShippingModalDialog"></div>').load(
						$link.attr('href').replace('#',''),
					  function (responseText, textStatus, XMLHttpRequest) {   
						cursor_clear();
						if (textStatus == "success") {
							$('.bundlePageModal').css('display','none');
							
							$('.ui-dialog-title').html('Ship To');
							$dialog.dialog('open');
						} 
						if (textStatus == "error") {     
							console.debug(XMLHttpRequest.responseText);
						}   
					}).dialog({
						autoOpen: false,
						modal:true,
						resizable:false,
						draggable:false,
						width: 'auto',
						height: 'auto',
						close: function(event, ui){
		            	    // Remove the dialog elements
							$(this).dialog("destroy");
			                // Remove the left over element (the original div element)
							$(this).remove();
						},
						title:'<img id="bundlePageModal" class="bundlePageModal" src="'+getImageDirectoryPath()+'images/tcp/ajax-loading.gif" />'   
					});
					return false;		
				
				});	
			});
		},
		languageToggle:function(selectedCountry){
			var spanishLangValue = 'es_ES';
			var spanishLangText = 'Espa\&ntilde;ol';
			var frenchLangValue = 'fr_FR';
			var frenchLangText = 'Fran\&ccedil;ais';
			if( selectedCountry != 'CA'){
			// for US add spanish 
			
				if($('#languageTCP option[value="'+spanishLangValue+'"]')==null || $('#languageTCP option[value="'+spanishLangValue+'"]').text() ==""){
					$("#languageTCP option:eq(0)").after("<option value='"+spanishLangValue+"'>"+spanishLangText+"</option>");
				
				}
				if($('#languageTCP option[value="'+frenchLangValue+'"]')!=null || $('#languageTCP option[value="'+frenchLangValue+'"]').text() !=""){
					$('#languageTCP option[value="'+frenchLangValue+'"]').remove();
				
				}
				if($("#languageTCP").val() != 'es_ES')
					$("#languageTCP").val('en_US').prop('selected',true);
			
			} else if( selectedCountry == 'CA'){
			// for Canada remove Spanish
				if($('#languageTCP option[value="'+spanishLangValue+'"]')!=null || $('#languageTCP option[value="'+spanishLangValue+'"]').text() !=""){
					$('#languageTCP option[value="'+spanishLangValue+'"]').remove();
				
				}
				if($('#languageTCP option[value="'+frenchLangValue+'"]')==null || $('#languageTCP option[value="'+frenchLangValue+'"]').text() ==""){
					$("#languageTCP option:eq(0)").after("<option value='"+frenchLangValue+"'>"+frenchLangText+"</option>");
				
				}
				if($("#languageTCP").val() != 'fr_FR')
					$("#languageTCP").val('en_US').prop('selected',true);
				
			
			}
		},
		 countrySel:function(storeIdObj){
			//alert("ppp");
			// if country is Indian or China,  no need  to go to ajax
			if ((storeIdObj.value == -1) || (storeIdObj.value == -2))
			{
				var seleDiv=document.getElementById('enLang');
				seleDiv.lastChild.id="languageCode";
				seleDiv.lastChild.name="language";
				document.getElementById('languageSelDiv').innerHTML =seleDiv.innerHTML;
				
				if (storeIdObj.value == -1) { // Indian
					document.getElementById('countryflag').src = this.countryFlagPath + "IN.gif";
				} else {  // China
					document.getElementById('countryflag').src = this.countryFlagPath + "CN.gif";
				}
				return;
			}
			
			// otherwise, go to ajax to get language and flag
			var http_request = this.getHttpRequest();
			if (http_request) {
				http_request.onreadystatechange = function(){
					if (http_request.readyState == 4) {
						if (http_request.status == 200) {
							if (http_request.responseText){
								document.getElementById('languageSelDiv').innerHTML = http_request.responseText.split("||")[0];
								document.getElementById('countryflag').src = InternationalShippingJS.countryFlagPath + http_request.responseText.split("||")[1];
							}
						} else {
							//alert('There was a problem with the request.');
						}
					}
				};
				http_request.open('GET', this.url+"&selectSid="+storeIdObj.value, true);
				http_request.send();
			}
		},
		
		setShippingFlag:function(shippingFlag)
		{
			this.shippingFlag  = shippingFlag;
		},
        setUrl:function(url){
			this.url=url;
		},
	setshopUrl:function(shopUrl){
			this.shopUrl=shopUrl;
		},
		setshopCartUrl:function(shopCartUrl){
			this.shopCartUrl=shopCartUrl;
		},
		setUSURL:function(USURL){
			this.USURL=USURL;
		},
		
		setCAURL:function(CAURL){
			this.CAURL=CAURL;
		},
        setDialog:function(dialog){
			this.ishippingdialog=dialog;
		},
		setCountryFlagPath:function(path){
			this.countryFlagPath=path;
		},
		getShippingFlag:function()
		{
			return this.shippingFlag;
		},
		getHttpRequest:function() {
			var http_request;
			if (window.XMLHttpRequest) {
				http_request = new XMLHttpRequest();
			} else if (window.ActiveXObject) {
				// IE
				try {
					http_request = new ActiveXObject("Msxml2.XMLHTTP");
				} catch (e) {
					try {
						http_request = new ActiveXObject("Microsoft.XMLHTTP");
					} catch (e) {
						//alert("Ajax not supported");
						return null;
					}
				}
			}
			return http_request;
		},
		
		setNotSelectedAllAlert:function(message) {
			this.notSelectedAllAlertMessage = message;
		},
		
		setTermAndConditionAlert:function(message) {
			this.termAndConditionAlertMessage = message;
		},
		setSubmittedOK:function(message) {
			this.submittedOKMessage = message;
		},	
		
		setArrayValues:function(fxArray,flcArray){
			
			
			this.flxArray = fxArray;
			this.flcArray = flcArray;
		},
		
		getCookie:function(key) {  
		   var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');  
		   return keyValue ? keyValue[2] : null;  
		},  
		// transform "United States English" to "English", "Canadian French" to "French",..
		getLanguageShortDesc:function(text) {
			var result = text;
			var temp = text.split(/\s+/);
			if (temp.length>1)
			{
				result = temp.pop();
			}
			return result;
		},
		
			switchLanguage:function(previousLanguage,lang,countrySelected,previousCountry) {
			MP.SrcUrl=decodeURIComponent(mp_origin_url);MP.UrlLang=mp_origin_lang;MP.init(previousLanguage,countrySelected);
			
			//MP.switchLanguage(lang);
			MP.switchLanguage(lang,countrySelected,previousCountry);
			return false;
			},
		// changes country flag, language information in topheader and close ishipping modal
		changeTopHeaderAndCloseModal:function(flagName, languageShortDesc){
			$("#WC_TCPCachedCommonToolbar_FormLink_IShipping2 img").attr("src", InternationalShippingJS.countryFlagPath + flagName);
        	$("#WC_TCPCachedCommonToolbar_FormLink_IShipping3").html(languageShortDesc);
			$("#iShippingModalDialog").dialog("close");
		},
		shippingRecord:function(mode,url,errMsg){
			var frm = document.IntlShippingForm;
			var currencySelected ;
			var countryDrop = document.getElementById('countryCode');
			var countrySelected = countryDrop.options[countryDrop.selectedIndex].value;
			var languageDrop = document.getElementById('languageTCP');
			var languageSelected = languageDrop.options[languageDrop.selectedIndex].value;
			var currencyDrop = document.getElementById('currencyCode');		
			currencySelected = currencyDrop.options[currencyDrop.selectedIndex].value;
			
			InternationalShippingJS.setArrayValues(fxArray,flcArray);	

			if(currencySelected == 'emp'&& countrySelected == 'US'){
					
				currencySelected = "USD";
			}if(currencySelected == 'emp'&& countrySelected == 'CA'){
						
				currencySelected = "CAD";
			}
				
			if(countryDrop.options[0].selected || languageDrop.options[0].selected ||  countryDrop.options[0].selected || currencySelected == 'emp' )
			{
				alert(this.notSelectedAllAlertMessage);
			} 
			
		
		
			
			else
			{
				var previousContCookie = InternationalShippingJS.getCookie("iShippingCookie");
				var selCountry = "";
				var numOfItems = InternationalShippingJS.getCookie("numOfItems");
				if(null != previousContCookie && undefined != previousContCookie){
				selCountry= previousContCookie.split("|")[0];
				if(selCountry == 'CA' && countryDrop.value != 'CA' && numOfItems >0){
					$("#errMsg").css('display', 'inline-block');
					$("#errMsg").css('color', '#FF0000');
					$("#errMsg").text(errMsg);
				}
				}
				
				var languageDesc = languageDrop.options[languageDrop.selectedIndex].text;
				var languageShortDesc = InternationalShippingJS.getLanguageShortDesc(languageDesc);
				
				if(mode == 'us' || countrySelected == 'US'  ){
					frm.cc.value = "us";	
					frm.ccd.value = "US";	
					frm.curr.value = "USD";
				}else if(countrySelected == 'CA' || countrySelected == 'ca')
					{
					frm.cc.value = "CA";
					frm.curr.value = "CAD";
					}
				else{
					frm.er.value = this.flxArray[frm.ccd.options[frm.ccd.selectedIndex].value];
					frm.mm.value = this.flcArray[frm.ccd.options[frm.ccd.selectedIndex].value];
					frm.cc.value = frm.ccd.value;
				}
				var usurl = $("#USURL").val();
				var caurl = $("#CAURL").val();
				
				  
				var d=$("#IntlShippingForm").serialize();
				
				$.post("TCPLocaleSelection", d, 
						function (result) { 
						// parse ajax result to get language and flag information
						result = result.replace("/*","");
						result = result.replace("*/","");
						var obj = jQuery.parseJSON(result);
						var flagName = obj.flagName;
						var languageName = obj.languageTCP;
						var previousLanguage = $("#selLanguage").val();
						if (InternationalShippingJS.getShippingFlag())
						{ // if this modal pops up from Shipping page, simply close it
							InternationalShippingJS.equipContry4Shipping('country', 'countryCode');
							
						} else 
						{ // otherwise, redirect to proper eSite if applicable
							InternationalShippingJS.changeTopHeaderAndCloseModal(flagName, languageShortDesc);
							if (obj.redirectToStore && obj.redirectToStore=="usstore") {
								var languageName1 = languageName[0].split("_")[0];
								if(languageName[0]!= previousLanguage ){
									
								InternationalShippingJS.switchLanguage(previousLanguage,languageName1,countrySelected,selCountry);
								
								}else{
									if(languageName[0] == "fr_FR"){
										//location.reload();
										InternationalShippingJS.switchLanguage(previousLanguage,languageName1,countrySelected,selCountry);
										//window.location.href=window.location.protocol+'//'+window.location.host+"/sduatlive/webapp/wcs/stores/servlet/"+"en/usstore/home";
										
									}else if(languageName[0] == "es_ES"){
										InternationalShippingJS.switchLanguage(previousLanguage,languageName1,countrySelected,selCountry);
									}
									
									else if(languageName[0] == "en_US")
										{
										window.location.href=window.location.protocol+'//'+window.location.host+'/shop/us/home';
										}
									
									
									//our code
								}
								
								
								
							}
							else if (obj.redirectToStore && obj.redirectToStore=="canadastore") {
								var languageName1 = languageName[0].split("_")[0];
								if(languageName[0]!= previousLanguage ){
									InternationalShippingJS.switchLanguage(previousLanguage,languageName1,'CA',selCountry);
								}
								else{
									if(languageName[0] == "fr_FR"){
										InternationalShippingJS.switchLanguage(previousLanguage,languageName1,'CA',selCountry);
										//window.location.href=window.location.protocol+'//'+window.location.host+"/sduatlive/webapp/wcs/stores/servlet/"+"en/usstore/home";
										
									}
									else if(languageName[0] == "es_ES"){
										InternationalShippingJS.switchLanguage(previousLanguage,languageName1,countrySelected,selCountry);
									}
									else if(languageName[0] == "en_US")
										
										{
										window.location.href=window.location.protocol+'//'+window.location.host+'/shop/ca/home';
										}
									
								}
								
							}else if(obj.redirectToStore != null){
								var url1 = "";
								languageName = languageName[0].split("_")[0];
								InternationalShippingJS.switchLanguage(previousLanguage,languageName,'US',selCountry);
							}
							else {
								InternationalShippingJS.changeTopHeaderAndCloseModal(flagName, languageShortDesc);
							}
						}
					}
				, 
				"text");
				
			}		
		},
		/*changeLangURL:function() {
		    
	    	var languageSelected = $("#languageTCP").val();
	    	if(languageSelected == 'es_ES'){
	    		var spanishURL = $('#spanishURL').val();
	    		var eshref = $('#shiptosave').attr('href').replace('fr.childrensplace.com','es.childrensplace.com');
	    		$('#shiptosave').attr('href',eshref);
	    	}
	    	if(languageSelected == 'fr_FR'){
	    		var frenchURL = $('#frenchURL').val();
	    		var eshref = $('#shiptosave').attr('href').replace('es.childrensplace.com','fr.childrensplace.com');
	    		$('#shiptosave').attr('href',eshref);
	    	}
	    	
	    },*/
		setPopupFlag:function(popupFlag) {
			this.popupFlag = popupFlag;
		},
		getContryCode: function(spcContry){
			var uniqueConCode = '';
			if (spcContry == '10151'){
				uniqueConCode = 'US';
			}else if (spcContry == '10152'){
				uniqueConCode = 'CA';
			}else if (spcContry == '10101'){
				uniqueConCode = 'AU';
			}else if (spcContry == '-1'){
				uniqueConCode = 'IN';
			}else if (spcContry == '-2'){
				uniqueConCode = 'CN';
			}
			return uniqueConCode;
		}, 
		equipContry4Shipping: function(toId, fromId){
			if (toId != null && fromId != null){
				document.getElementById(toId).value=this.getContryCode(document.getElementById(fromId).value);
				var conCode = document.getElementById(toId).value;
				var dplName = document.getElementById('contryDisplay');
				if (conCode == 'US'){
					dplName.innerHTML = 'United States';
				}else if (conCode == 'CA'){
					dplName.innerHTML = 'Canada';
				}else if(conCode == 'AU'){
					dplName.innerHTML = 'Australia';
				}else if(conCode == 'IN'){
					dplName.innerHTML = 'India';
				}else if(conCode == 'CN'){
					dplName.innerHTML = 'China';
				}
			}
		},
		//shippingPageOpenDialog: function(){
		//	this.shippingFlag = true;
			//InternationalShippingJS.ishippingdialog.dialog('open');
		//	$("#iShippingModalDialog").dialog("open");
		//},
		/**
		 * This function returns country flag pic name and language description approporiate for an ishipping cookie value.
		 * The text value for ishippingCookie is in format like "IN|en-us|INR|401|4", or "CA|en_ca".  
		 */
		getCountryFlagLanguageFromCookie: function(defaultFlag, defaultLocale,path){
			result = [defaultFlag,defaultLocale];
			try {
				iShippingCookieValue = InternationalShippingJS.getCookie("iShippingCookie");
				var temp = '';
				var imgVal = '';
				var altVal ='';
				if(null != iShippingCookieValue){
				temp = iShippingCookieValue.toLowerCase().split("|");
				if(temp[1] == undefined) 
					temp[1] = 'en_US';
				else if(temp[1] == 'en_us' && temp[0] == 'ca'){
					temp[1] = 'en_ca';
					result = [[temp[0]], temp[1]];
				}
				else
					result = [[temp[0]], temp[1]];
				
				var flag = temp[0].toUpperCase();
			
				path = path+"images/tcp/international_shipping/flags/";				
				imgVal = path + flag +".gif";
				altVal = flag;
				} else{
					imgVal = path+"images/tcp/international_shipping/flags/"+defaultFlag;					
					altVal = defaultFlag;
				}
				
				if(altVal.indexOf(".")!=-1){
					altVal = altVal.substring(0,altVal.lastIndexOf("."));
				}
				altVal="Ship to Country: "+altVal;
				document.getElementById('countryFlagInToolbar').src = imgVal;
				document.getElementById('countryFlagInToolbar').title = altVal;
				document.getElementById('countryFlagInToolbar').alt = altVal;
			}
			catch (ex) {}
			return result;
		},
		
		 ReadCookie:function(name)
		{
		  	var a_all_cookies = document.cookie.split( ';' );
		  
			var a_temp_cookie = '';
			var cookie_name = '';
			var cookie_value = '';
			var b_cookie_found = false; // set boolean t/f default f
			
			for ( i = 0; i < a_all_cookies.length; i++ ) {
				a_temp_cookie = a_all_cookies[i].split( '=' );
				cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g, '');
				if ( cookie_name == name ) {
					b_cookie_found = true;
					if ( a_temp_cookie.length > 1 ) {
						
						cookie_value = unescape( a_temp_cookie[1].replace(/^\s+|\s+$/g, '') );
					}
					return cookie_value;
					break;
				}
				a_temp_cookie = null;
				cookie_name = '';
			}
			if ( !b_cookie_found ) {
				return null;
			}
		},
		 
			
		updateMiniCartCookie:function(){
			
				//alert('insidse updateminicart');
				var iShippingCookieValue =ReadCookie("minicartcookie");
				
				var numOfItems = ReadCookie("numOfItems");
				
				var url1 = this.url;
				var shopCartUrl = this.shopCartUrl;
				var shopUrl = this.shopUrl;
				var amt = '';
				var currency;
				var amount ;
				if(iShippingCookieValue != null ){
					amt = iShippingCookieValue.split("|")[1];
					
					if(amt.indexOf("$")!=-1){

						currency = amt.substring(0,1);
						
						amount = amt.substring(1);
					
						} else {
					
					currency = amt.substring(0,3);
					
					amount = amt.substring(3);
					
				}
				}
				if(amount!=null){
					amount=amount.replace(/\s/g,"");
					
					if (amount.indexOf(".") == 0) {
						amount = "0." + amount.split(".")[1];
						
					}
				}
				//amount=parseFloat(amount);
				if(isNaN(amount)){
					  amount = "0.00";
				}else if(amount==0){
					amount = "0.00";
				}
				var span_shopBagPrice = document.getElementById('amt');
				if (numOfItems == 0){
					document.getElementById('amt').setAttribute("style","width:50px");
					
					
					amount = amount.slice(0, (amount.indexOf("."))+3);

					$("#INTcurrency").text(currency);
					$("#INTAmt").text(amount);
					$("#INTcurrency").attr('class','openModalIShipping');
					$("#INTcurrency").attr('href',this.shopUrl);
					$("#qty").text(numOfItems);
					$("#qty").css('cursor', 'pointer');
					$("#qty").bind('click', function(e){
						  window.location = shopCartUrl;
					}); 
					$("#qty").css("color","#FFFFFF");
				}
					var newShopBagItemsText = numOfItems+ '&nbsp;items';
					if (numOfItems == 1) {
							newShopBagItemsText = numOfItems + '&nbsp;item';
					}
						//span_shopBagItems.innerHTML = newShopBagItemsText;
					
						if (numOfItems  > 0)
						{
							document.getElementById('amt').setAttribute("style","width:100px");
							$("#INTcurrency").text(currency);
							
							//document.getElementById("INTAmt").Value = amount;
							
							amount = amount.slice(0, (amount.indexOf("."))+3);
							$("#INTAmt").text(amount);
							$("#INTcurrency").attr('class','openModalIShipping');
							$("#INTcurrency").attr('href',this.shopUrl);
							$("#qty").css('cursor', 'pointer');
							$("#qty").text(numOfItems);
							$("#qty").bind('click', function(e) 
							        {   
							//$("#qty").click(function(){
								
								  window.location = shopCartUrl;
							}); 
							
							$("#qty").css("color","#FFFFFF");
						}
						if(numOfItems == null){
							$("#qty").text("0");
							$("#INTcurrency").text(currency);
							//$("#INTAmt").text("0.00");
							$("#qty").css('cursor', 'pointer');
							$("#qty").bind('click', function(e){
							//$("#qty").click(function(){
								
								  window.location = shopCartUrl;
							}); 
						}
						if(iShippingCookieValue == null){
							
							$("#qty").text("0");
							var iShippingCookie = ReadCookie("iShippingCookie");
							if(null !=iShippingCookie && undefined != iShippingCookie){
								if(iShippingCookie.split("|")[2] != undefined)
									currency = iShippingCookie.split("|")[2];
							
							
								if(currency == undefined ){
									if(iShippingCookie == 'ca' || iShippingCookie == 'CA'){
										currency = 'CAD';
									}else if(iShippingCookie == 'us' || iShippingCookie == 'US'){
										currency = 'USD';
									}
								}
							}else{
								var pathName = window.location.pathname;
								if(window.location.search.indexOf('10151') != -1 || pathName.indexOf('us') != -1){
									currency = 'USD';
								}else{
									currency = 'CAD';
								}
								
							}
							
							$("#INTcurrency").text(currency);
							$("#INTAmt").text("0.00");
							$("#qty").css('cursor', 'pointer');
							$("#qty").bind('click', function(e){
							//$("#qty").click(function(){
								
								  window.location = shopCartUrl;
							}); 
						}
		},
		updateShipTo:function(){
			
			var selCountry =  $('#WC_TCPShippingAddress_FormInput_country').find('option:selected').text();
			
		
				document.getElementById("conValue").innerHTML = selCountry;
		
			
			
		},
		loadShipPopUp:function(url){
			var selectedCoun = $('#WC_TCPShippingAddress_FormInput_country').find('option:selected').val();
			
			var reloadlink = $('#shipTool');
			
			var url = url + "&selCountry="+selectedCoun;
			
		
			reloadlink.attr("href",url);
		},
		
		checkIShippingCookie: function(path,storeId){
			path = path+"images/tcp/international_shipping/flags/";
			var ishippingCookie = InternationalShippingJS.getCookie("iShippingCookie");
			domainIndex = window.location.hostname.indexOf('.');
			domain = window.location.hostname.substring(domainIndex)+";";
			if(ishippingCookie != null || undefined != ishippingCookie){
				country = ishippingCookie.split('|')[0];
				
				if(country.toLowerCase() == 'us' && storeId == 10152){
					 var date = new Date();
					 date.setDate(date.getDate() -1);
					 var expires = "; expires="+date.toGMTString();
					 document.cookie = "iShippingCookie=CA|en_US|CAD|1.0|1.0"+expires+"; path=/;domain="+ domain ;
					 document.cookie = "minicartcookie"+"="+expires+"; path=/;domain="+ domain ;
					 document.cookie = "numOfItems"+"="+expires+"; path=/;domain="+ domain ;
					 imgVal = path + "CA.gif";
					 document.getElementById('countryFlagInToolbar').src = imgVal;
					 document.getElementById('countryFlagInToolbar').title = "Ship to Country: CA";
					 document.getElementById('countryFlagInToolbar').alt = "Ship to Country: CA";
					 $('#INTcurrency').html('CAD');
					 
				}
				else if(country.toLowerCase() == 'ca' && storeId == 10151){
					 var date = new Date();
					 date.setDate(date.getDate() -1);
					 var expires = "; expires="+date.toGMTString();
					 document.cookie = "iShippingCookie=US|en_US|USD|1.0|1.0"+expires+"; path=/;domain="+ domain ;
					 document.cookie = "minicartcookie"+"="+expires+"; path=/;domain="+ domain ;
					 document.cookie = "numOfItems"+"="+expires+"; path=/;domain="+ domain ;
					 imgVal = path + "US.gif";
					 document.getElementById('countryFlagInToolbar').src = imgVal;
					 document.getElementById('countryFlagInToolbar').title = "Ship to Country: US";
					 document.getElementById('countryFlagInToolbar').alt = "Ship to Country: US";
					 $('#INTcurrency').html('USD');
				}
			}
			else if(ishippingCookie == null || undefined == ishippingCookie){
				if(storeId == 10152){
					if(window.location.host.indexOf('fr.') > -1 ){
					
						document.cookie = "iShippingCookie=CA|fr_FR|CAD|1.0|1.0"+"; path=/;domain="+ domain ;
					}
					else{
						document.cookie = "iShippingCookie=CA|en_US|CAD|1.0|1.0"+"; path=/;domain="+ domain ;
					}
				}
				else if(storeId == 10151){
					if(window.location.host.indexOf('es.') > -1 ){
						document.cookie = "iShippingCookie=US|es_ES|USD|1.0|1.0"+"; path=/;domain="+ domain ;
					}
					else{
						document.cookie = "iShippingCookie=US|en_US|USD|1.0|1.0"+"; path=/;domain="+ domain ;
					}
				}
			}
		}
		
};

$(document).ready(InternationalShippingJS.buildModalLink);