/* 1-4-2016 15:02 */
var TCP = TCP || {};


TCP.LocalStore = {

    getItem: function (sKey) {
        var store = docCookies.getItem(sKey); // || return null;
        if (!store) {
            return null;
        }
        return JSON.parse(JSON.stringify(store));
    },

    setItem: function (sKey, sValue) {
        if (!sValue) {
            return null;
        }
        var sExpire = '';
        docCookies.setItem(sKey, JSON.stringify(sValue), '/', sExpire, TCP.Tealium.config.domain, null);
    },

    removeItem: function (sKey) {
        docCookies.removeItem(sKey, '/', TCP.Tealium.config.domain);
    }
};

/**
 * TCP.Tealium
 * 
 * Common functions for Datalayer Manipulater
 */
$.extend(TCP, {

    Tealium: {

        itemSku: '',

        config: {
            cartName: 'tiq_cart',
            cartLimit: 10,
            enabledFlag: '0',
            tealiumLoaded: '0',
            env: '',
            domain: ''
        },

        isTealiumLoaded: function () {
            if (TCP.Tealium.config.tealiumLoaded === '0') {
                TCP.Tealium.config.tealiumLoaded = '1';
                return false;
            }
            return true;
        },

        isEnabled: function () {
            if (TCP.Tealium.config.enabledFlag === '1') {
                return true;
            }
            return false;
        },

        init: function (flag, env, dom, cartLimit) {
            if (typeof utag_data === 'undefined' || !utag_data || utag_data == null) {
                utag_data = {};
            }
            TCP.Tealium.config.enabledFlag = flag;
            TCP.Tealium.config.env = env;
            TCP.Tealium.config.domain = dom;
            TCP.Tealium.config.cartLimit = parseInt(cartLimit);
            if ('localhost' === dom) {
                TCP.Tealium.config.domain = null;
            }
        },

        clean: function (obj) {
            var tealiumObj = obj;
            $.each(tealiumObj, function (k, v) {
                if (v == null || typeof v == 'undefined') {
                    delete tealiumObj[k];
                } else if (v.length == 0) {
                    delete tealiumObj[k];
                }
            });
            return tealiumObj;
        },

        /**
         * Resets datalayer, by removing current data.
         * 
         * datalayer : the datalayer to be reset, i.e. TCP.Tealium.Customer.Datalayer
         */
        reset: function (tag) {
            var tagName = tag;

            console.log('reset TCP.Tealium[' + tagName + '].Datalayer');

            for (var prop in TCP.Tealium.Datalayer[tagName]) {
                if (TCP.Tealium.Datalayer[tagName].hasOwnProperty(prop)) {
                    if (typeof TCP.Tealium.Datalayer[tagName][prop] === 'object' && Object.prototype.toString.call(TCP.Tealium.Datalayer[tagName][prop]) === '[object Array]') {
                        TCP.Tealium.Datalayer[tagName][prop] = [];
                    } else {
                        TCP.Tealium.Datalayer[tagName][prop] = undefined;
                    }
                }
            }
        },

        /**
         * Merge obj2 into obj1, modifying obj1, shallow copy
         */
        merge: function (obj1, obj2) {
            $.extend(obj1, obj2);
        }
    }
});

/**
 * TCP.Tealium.CartCookie
 *  
 * Cookie holds the following JSON. Please update this description if it ever changes.
 * cookie name = tiq_cart
 * {
 *  qty,
 *  price,
 *  details[]
 * }
 * details is an array of length 5 with the following values:
 * ['product ID','product SKU, 'item SKU', 'price', 'qty']
 */
$.extend(TCP.Tealium, {

    CartCookie: {

        addCart: function (prodID, prodSKU, prodPRICE, prodQTY) {
            console.log('add to cart cookie');
            var t_cookie = JSON.parse(TCP.LocalStore.getItem(TCP.Tealium.config.cartName));
            var currentArr = [],
                obj = {},
                cartObj = {},
                newItem = true,
                newQty = 0,
                qty = 0,
                price = 0,
                index = 0,
                len = 0,
                sku = TCP.Tealium.itemSku;
            cartObj.details = [];

            if (t_cookie !== null) {
                var arr = t_cookie.details;
                len = arr.length;

                // loop through current items in tiq_cart cookie
                while (index < len) {
                    currentArr = arr[index];

                    // if item to add exists, update quantity of cart. Otherwise copy existing.
                    if (currentArr[2] === sku) {
                        newItem = false;
                        newQty = parseInt(prodQTY) + parseInt(currentArr[4]);
                        obj = [prodID, prodSKU, sku, prodPRICE, '' + newQty];
                        cartObj.details.push(obj);
                    } else {
                        obj = [currentArr[0], currentArr[1], currentArr[2], currentArr[3], currentArr[4]];
                        cartObj.details.push(obj);
                    }

                    qty = qty + parseInt(obj[4]);
                    price = price + (parseFloat(currentArr[3]) * parseInt(obj[4]));
                    ++index;
                }
            }

            cartObj.qty = '' + qty;
            cartObj.price = parseFloat(price).toFixed(2).toString();

            if (newItem && len < TCP.Tealium.config.cartLimit) {
                obj = [prodID, prodSKU, sku, prodPRICE, prodQTY];
                cartObj.details.push(obj);
                cartObj.qty = (qty + parseInt(prodQTY)).toString();
                cartObj.price = (price + (parseFloat(prodPRICE) * parseInt(prodQTY))).toFixed(2).toString();
            }

            // Still updating qty and price to current total, even if cart size limit is reached.
            TCP.LocalStore.setItem(TCP.Tealium.config.cartName, cartObj);

        },

        getCart: function () {
            console.log('get cart cookie');
            var t_cookie = JSON.parse(TCP.LocalStore.getItem(TCP.Tealium.config.cartName));
            var cartObj = {},
                index = 0,
                len = 0;
            var currentArr;
            if (t_cookie !== null) {
                var arr = t_cookie.details;
                len = arr.length;

                if (len === 0) return cartObj;

                cartObj.cart_product_id = [];
                cartObj.cart_product_price = [];
                cartObj.cart_product_quantity = [];
                cartObj.cart_product_sku = [];
                cartObj.cart_product_upc = [];
                while (index < len) {
                    currentArr = arr[index];
                    cartObj.cart_product_id.push(currentArr[0]);
                    cartObj.cart_product_sku.push(currentArr[1]);
                    cartObj.cart_product_upc.push(currentArr[2]);
                    cartObj.cart_product_price.push(currentArr[3]);
                    cartObj.cart_product_quantity.push(currentArr[4]);
                    ++index;
                }

                cartObj.cart_total_items = t_cookie.qty;
                cartObj.cart_total_value = t_cookie.price;
            }
            return cartObj;
        },

        createCart: function () {
            console.log('create cart cookie');
            TCP.LocalStore.removeItem(TCP.Tealium.config.cartName);
            var cartObj = {},
                arr = [],
                len = 0,
                index = 0,
                qty = 0,
                price = 0;
            cartObj.details = [];
            len = utag_data.cart_product_upc ? utag_data.cart_product_upc.length : 0;

            // proceed if shopping bag is Not empty
            if (len !== 0) {
                while (index < len) {
                    if (index === TCP.Tealium.config.cartLimit) break;
                    arr = [];
                    arr.push(utag_data.cart_product_id[index]);
                    arr.push(utag_data.cart_product_sku[index]);
                    arr.push(utag_data.cart_product_upc[index]);
                    arr.push(utag_data.cart_product_price[index]);
                    arr.push(utag_data.cart_product_quantity[index]);
                    cartObj.details.push(arr);
                    ++index;
                }

                // currently breaks at max, so these value show values of entire cart.
                // Even when the cart does array doesnt have whole shopping bag.
                cartObj.qty = utag_data.cart_total_items;
                cartObj.price = utag_data.cart_total_value;

                TCP.LocalStore.setItem(TCP.Tealium.config.cartName, cartObj);
            }
        }
    }
});

/**
 * TCP.Tealium.Link
 * 
 * All utag Link functions
 */
$.extend(TCP.Tealium, {

    Link: {

        birthdaySignup: function () {
            if (TCP.Tealium.isEnabled()) {
                var userBirthdaySignupObj = {
                    'event_name': 'birthday_signup'
                };
                console.log('Tealium: birthday_signup');

                try {
                    utag.link(userBirthdaySignupObj);
                } catch (err) {
                    console.log('Error in link call : ' + err.message);
                }
            }
        },

        userLogout: function () {
            if (TCP.Tealium.isEnabled()) {
                console.log('Tealium: user_logout');
                var userLogoutObj = TCP.Tealium.Datalayer.buildCustomer();
                userLogoutObj.event_name = 'user_logout';

                try {
                    utag.link(TCP.Tealium.clean(userLogoutObj));
                } catch (err) {
                    console.log('Error in link call : ' + err.message);
                }
            }
        },

        userUpdate: function () {
            if (TCP.Tealium.isEnabled()) {
                console.log('Tealium: user_update');
                var userUpdateObj = TCP.Tealium.Datalayer.buildCustomer();
                userUpdateObj.event_name = 'user_update';

                try {
                    utag.link(TCP.Tealium.clean(userUpdateObj));
                } catch (err) {
                    console.log('Error in link call : ' + err.message);
                }
            }
        },

        loyaltyRedemption: function () {
            if (TCP.Tealium.isEnabled()) {
                var loyaltyObj = {
                    'event_name': 'redeem_loyalty_points',
                    'customer_id': utag_data.customer_id
                };

                console.log('Tealium: loyaltyRedemption');

                try {
                    utag.link(TCP.Tealium.clean(loyaltyObj));
                } catch (err) {
                    console.log('Error in link call : ' + err.message);
                }
            }
        },

        giftCardBalance: function () {
            if (TCP.Tealium.isEnabled()) {
                var giftObj = {
                    'page_name': 'check balance',
                    'sc_page_type': 'gift cards'
                };

                console.log('Tealium: giftCardBalance');

                try {
                    utag.link(giftObj);
                } catch (err) {
                    console.log('Error in link call : ' + err.message);
                }
            }
        },

        fireEmptyCart: function () {
            if (TCP.Tealium.isEnabled()) {
                console.log("Tealium : cart_empty");
                var emptyCartObj = {};
                emptyCartObj.event_name = "cart_empty";

                try {
                    utag.link(emptyCartObj);
                } catch (error) {
                    console.log("Error in link call : " + error.message);
                }
            }
            _satellite.track("FireEmptyCart");
        },

        fireCartRemove: function (index) {
            if (TCP.Tealium.isEnabled()) {
                console.log("Tealium cart_remove : ");

                var TIndex = index - 1;
                var cartRemoveObj = {};

                cartRemoveObj.product_id = [];
                cartRemoveObj.product_sku = [];
                cartRemoveObj.product_upc = [];
                cartRemoveObj.product_name = [];
                cartRemoveObj.product_price = [];
                cartRemoveObj.product_quantity = [];
                cartRemoveObj.event_name = "cart_remove";

                try {
                    cartRemoveObj.product_id.push(utag_data.cart_product_id[TIndex]);
                    cartRemoveObj.product_sku.push(utag_data.cart_product_sku[TIndex]);
                    cartRemoveObj.product_upc.push(utag_data.product_upc[TIndex]);
                    cartRemoveObj.product_name.push(utag_data.product_name[TIndex]);
                    cartRemoveObj.product_price.push(utag_data.cart_product_price[TIndex]);
                    cartRemoveObj.product_quantity.push(utag_data.cart_product_quantity[TIndex]);

                    utag_data.cart_total_items = (parseInt(utag_data.cart_total_items) - utag_data.cart_product_quantity[TIndex]).toString();
                    utag_data.cart_total_value = (parseFloat(utag_data.cart_total_value) - (parseFloat(utag_data.cart_product_price[TIndex]) * parseFloat(utag_data.cart_product_quantity[TIndex]))).toFixed(2).toString();

                    utag_data.product_id.splice(TIndex, 1);
                    utag_data.product_sku.splice(TIndex, 1);
                    utag_data.product_upc.splice(TIndex, 1);
                    utag_data.product_name.splice(TIndex, 1);
                    utag_data.product_price.splice(TIndex, 1);
                    utag_data.product_quantity.splice(TIndex, 1);
                    utag_data.cart_product_id.splice(TIndex, 1);
                    utag_data.cart_product_sku.splice(TIndex, 1);
                    utag_data.cart_product_price.splice(TIndex, 1);
                    utag_data.cart_product_quantity.splice(TIndex, 1);
                    utag_data.cart_product_upc.splice(TIndex, 1);

                    // create new cart cookie with updated values.
                    TCP.Tealium.CartCookie.createCart();

                    TCP.Tealium.merge(cartRemoveObj, TCP.Tealium.CartCookie.getCart());

                    utag.link(TCP.Tealium.clean(cartRemoveObj));

                    if (parseInt(utag_data.cart_total_items) <= 0) {
                        TCP.Tealium.Link.fireEmptyCart();
                    }
                } catch (error) {
                    console.log("Error in link call : " + error.message);
                }
            }
            _satellite.track("FireCartRemove");
        },

        addToCart: function () {
            if (TCP.Tealium.isEnabled()) {
                console.log('Tealium : cart_add ');
                var addToCartObj = {};

                addToCartObj.product_id = [];
                addToCartObj.product_upc = [];
                addToCartObj.product_sku = [];
                addToCartObj.product_name = [];
                addToCartObj.product_quantity = [];
                addToCartObj.product_price = [];
                addToCartObj.product_url = [];
                addToCartObj.product_image_url = [];
                addToCartObj.event_name = 'cart_add';

                var from = $.trim($('.tealium-add-to-cart-location').text());

                if (from === 'pdp') {
                    addToCartObj.add_to_cart_location = 'product detail page';
                } else if (from === 'quickview') {
                    addToCartObj.add_to_cart_location = 'quick view product';
                } else if (from === 'outfit') {
                    addToCartObj.add_to_cart_location = 'quick view outfit';
                } else if (from === 'gift') {
                    addToCartObj.add_to_cart_location = 'gift card product detail page';
                } else if (from === 'giftoptions') {
                    addToCartObj.add_to_cart_location = 'gift options';
                }

                try {
                    if (from === 'gift') {
                        addToCartObj.product_id.push(document.getElementById('TGiftProductId').value.toString());
                        addToCartObj.product_quantity.push(document.getElementById('quantity').value.toString());
                        addToCartObj.product_price.push(parseFloat(document.getElementById('select_TCPSize').value).toFixed(2).toString());

                        var gift_product_name_element = document.getElementById('swapCard');
                        addToCartObj.product_name.push(gift_product_name_element.options[gift_product_name_element.selectedIndex].innerHTML);
                        var TSKuVar = 'TSku_' + addToCartObj.product_id;
                        addToCartObj.product_sku.push(document.getElementById(TSKuVar).value.toString());

                    } else if (from === 'giftoptions') {

                        var product_id = $('input[type="radio"][name="catEntryId_0"]:checked').val();
                        var gift_details = $('.gift-details-item-' + product_id);
                        addToCartObj.product_id.push(product_id);

                        var sku = $('input[type="radio"][name="catEntryId_0"]:checked').attr('data_TSku');
                        var prod_sku = $('input[type="radio"][name="catEntryId_0"]:checked').attr('data_TProductSku');
                        addToCartObj.product_sku.push(prod_sku);
                        addToCartObj.product_upc.push(sku);
                        addToCartObj.product_quantity.push('1');

                        var giftDetailsPrice = $.trim($(gift_details).find('.gift-details-price').text());
                        if (giftDetailsPrice != 'FREE' && giftDetailsPrice != 'free' && giftDetailsPrice != '') {
                            giftDetailsPrice = giftDetailsPrice.slice(1); //To remove the currency symbol
                        }
                        addToCartObj.product_price.push(giftDetailsPrice);
                        addToCartObj.product_name.push($.trim($(gift_details).find('.gift-details-name').text()));

                    } else {

                        var prod_id = document.getElementById('prodId').value.toString();
                        addToCartObj.product_id.push(prod_id);
                        addToCartObj.product_sku.push(document.getElementById('TCurrentProductSkuId').value.toString());
                        addToCartObj.product_upc.push(TCP.Tealium.itemSku);
                        addToCartObj.product_quantity.push(document.getElementById('quantity').value.toString());
                        addToCartObj.product_name.push(document.getElementById('TCurrentProductName').value.toString());

                        //addToCartObj.product_url.push("//"+window.location.host+"/webapp/wcs/stores/servlet/ProductDisplay?productId="+prod_id+"&catalogId="+utag_data.catalog_id+"&storeId="+utag_data.store_id);

                        var product_price_temp = document.getElementById('TCurrentProductPrice').value.toString();
                        var product_price_final = '';
                        var price_check = product_price_temp.indexOf('-');

                        var singlePrice;
                        if (price_check == -1) { //No Deal Offers
                            product_price_final = parseFloat(product_price_temp) * parseFloat(document.getElementById('quantity').value);
                            singlePrice = parseFloat(product_price_temp).toFixed(2).toString();
                        } else {
                            var price_list = product_price_temp.split('-');
                            var quantity_check = price_list.indexOf(document.getElementById('quantity').value.toString());
                            if (quantity_check != -1) {
                                product_price_final = price_list[quantity_check + 1];
                                singlePrice = parseFloat(price_list[quantity_check + 1]).toFixed(2).toString();
                            } else {
                                singlePrice = parseFloat(price_list[price_list.length - 1]).toFixed(2).toString();
                                if (parseInt(document.getElementById('quantity').value) < parseInt(price_list[0])) {
                                    product_price_final = parseFloat(price_list[price_list.length - 1]) * parseFloat(document.getElementById('quantity').value);
                                } else {
                                    product_price_final = parseFloat(price_list[price_list.length - 1]) * parseFloat(document.getElementById('quantity').value);
                                }
                            }
                        }
                        addToCartObj.product_price.push(parseFloat(product_price_final).toFixed(2).toString());
                    }

                    TCP.Tealium.CartCookie.addCart(addToCartObj.product_id[0], addToCartObj.product_sku[0], '' + singlePrice, '' + addToCartObj.product_quantity[0]);
                    TCP.Tealium.merge(addToCartObj, TCP.Tealium.CartCookie.getCart());

                    // blank out prior itemSku
                    TCP.Tealium.itemSku = '';
                    utag.link(TCP.Tealium.clean(addToCartObj));
                } catch (err) {
                    console.log('Error in link call : ' + err.message);
                }
            }
            _satellite.track("AddToCart");
        }
    }

});

/**
 * TCP.Tealium.View
 * 
 * All utag View functions
 */
$.extend(TCP.Tealium, {

    View: {

        accountTabChange: function (option) {
            if (TCP.Tealium.isEnabled()) {
                console.log(':::accountTabChange(' + option + ')');
                var message = '';

                switch (option) {
                case '1':
                    message = 'your profile';
                    break;
                case '2':
                    message = 'address book';
                    break;
                case '3':
                    message = 'payment info';
                    break;
                case '4':
                    message = 'order status and history';
                    break;
                case '5':
                    message = 'points history';
                    break;
                case '6':
                    message = 'view program details';
                    break;
                case '7':
                    message = 'edit password';
                    break;
                case '8':
                    message = 'add new address';
                    break;
                case '9':
                    message = 'add credit card';
                    break;
                case '10':
                    message = 'membership levels';
                    break;
                case '11':
                    message = 'edit email address';
                    break;
                case '12':
                    message = 'edit birthdays';
                    break;
                case '13':
                    message = 'edit address';
                    break;
                case '14':
                    message = 'edit credit card';
                    break;
                case '15':
                    message = 'edit shipping method';
                    break;
                case '16':
                    message = 'edit home address';
                    break;
                case '17':
                    message = 'see tier details';
                    break;
                }
                var myPlaceObj = {
                    'sc_page_type': 'my place',
                    'page_name': message
                };

                try {
                    var myPlaceObj = $.extend({},
                        TCP.Tealium.Datalayer.buildPage(),
                        TCP.Tealium.Datalayer.buildCustomer(),
                        myPlaceObj);

                    utag.view(TCP.Tealium.clean(myPlaceObj));
                } catch (err) {
                    console.log('Error in view call : ' + err.message);
                }
            }
        },

        logonTabChange: function (tab, user, center) {
            if (TCP.Tealium.isEnabled()) {
                var accountObj = {},
                    selectedTab = tab,
                    userType = user,
                    centerCol = center;

                console.log('logonTabChange tab: ' + selectedTab);

                try {

                    // create My Account View for Tealium 
                    if (selectedTab === 'login') {
                        if (userType === 'G') {
                            if (centerCol === 'false') {
                                accountObj.page_type = 'account';
                                accountObj.page_name = 'signin';
                                accountObj.sc_page_type = 'checkout';

                                console.log('checkout signin link fired');
                            } else {
                                accountObj.page_type = 'account';
                                accountObj.page_name = 'login';
                                accountObj.sc_page_type = 'my place';

                                console.log('account login link fired');
                            }
                        }
                    } else if (selectedTab === 'register') {
                        accountObj.page_type = 'account';
                        accountObj.page_name = 'register';
                        accountObj.sc_page_type = 'my place';

                        console.log('account register link fired');
                    }

                    try {
                        if (document.readyState === 'loading') {
                            // copy data into utag_data as well
                            TCP.Tealium.merge(utag_data, accountObj);
                        } else {
                            var loginPageObj = $.extend({},
                                TCP.Tealium.Datalayer.buildPage(),
                                TCP.Tealium.Datalayer.buildCustomer(),
                                accountObj);

                            // create utag view
                            utag.view(TCP.Tealium.clean(loginPageObj));
                        }
                    } catch (err) {
                        console.log('utag is not defined');
                    }

                } catch (err) {
                    console.log('Error in view call : ' + err.message);
                }
            }
        },

        giftRegistry: function () {
            if (TCP.Tealium.isEnabled()) {
                console.log('Tealium: giftRegistry');
                try {
                    var giftObj = TCP.Tealium.clean(TCP.Tealium.Datalayer.buildPage());

                    //overide for gift registry
                    giftObj.page_name = 'order status';
                    giftObj.sc_page_type = 'my place';

                    utag.view(giftObj);
                } catch (err) {
                    console.log('Error in view call : ' + err.message);
                }
            }
        },

        internationalShipping: function () {
            if (TCP.Tealium.isEnabled()) {
                console.log('Tealium: internationalShipping');
                var shipObj = {
                    'page_name': 'selections',
                    'sc_page_type': 'international ship'
                };
                try {
                    var iShipObj = $.extend({}, TCP.Tealium.Datalayer.buildPage(), shipObj);

                    // Tealium does not need these page info here
                    iShipObj.page_department = '';
                    iShipObj.page_category = '';
                    iShipObj.page_subcategory = '';
                    utag.view(TCP.Tealium.clean(iShipObj));
                } catch (err) {
                    console.log('Error in view call : ' + err.message);
                }
            }
        },

        orderStatus: function () {
            if (TCP.Tealium.isEnabled()) {
                console.log('Tealium: orderStatus');
                var osObj = {
                    'page_name': 'order status',
                    'sc_page_type': 'my place',
                    'page_type': 'account'
                };
                try {
                    var oStatusObj = $.extend({},
                        TCP.Tealium.Datalayer.buildPage(),
                        TCP.Tealium.Datalayer.buildCustomer(),
                        osObj);

                    utag.view(TCP.Tealium.clean(oStatusObj));
                } catch (err) {
                    console.log('Error in view call : ' + err.message);
                }
            }
        },

        giftOptions: function () {
            if (TCP.Tealium.isEnabled()) {
                var giftObj = {
                    'sc_page_type': 'checkout',
                    'page_type': 'checkout',
                    'page_department': 'gift options'
                };

                console.log('Tealium: giftOptions');

                try {
                    var giftOptionsObj = $.extend({},
                        TCP.Tealium.Datalayer.buildPage(),
                        giftObj);

                    utag.view(giftOptionsObj);
                } catch (err) {
                    console.log('Error in link call : ' + err.message);
                }
            }
        },

        fireCartUpdate: function (index) {
            if (TCP.Tealium.isEnabled()) {
                console.log("Tealium cart_update : ");
                var TIndex = index - 1;
                var TNewQuantityString = document.getElementById('quantity_' + index).value.toString();
                var TOldQuantityString = utag_data.cart_product_quantity[TIndex];
                var TNewQuantityInt = parseInt(TNewQuantityString);
                var TOldQuantityInt = parseInt(TOldQuantityString);
                var TCurrentUpdateProductPrice = utag_data.cart_product_price;
                var TOldCartTotalValue = utag_data.cart_total_value;

                if (TNewQuantityInt > TOldQuantityInt) {
                    var TDifferenceQuantity = TNewQuantityInt - TOldQuantityInt;
                    utag_data.cart_total_value = (parseFloat(utag_data.cart_total_value) + (parseFloat(TCurrentUpdateProductPrice) * TDifferenceQuantity)).toFixed(2).toString();
                } else if (TNewQuantityInt < TOldQuantityInt) {
                    var TDifferenceQuantity = TOldQuantityInt - TNewQuantityInt;
                    utag_data.cart_total_value = (parseFloat(utag_data.cart_total_value) - (parseFloat(TCurrentUpdateProductPrice) * TDifferenceQuantity)).toFixed(2).toString();
                }

                utag_data.cart_product_quantity[TIndex] = TNewQuantityString;
                utag_data.product_quantity[TIndex] = TNewQuantityString;
                var cart_total_items = 0;
                for (var i in utag_data.cart_product_quantity) {
                    cart_total_items += parseInt(utag_data.cart_product_quantity[i]);
                }
                utag_data.cart_total_items = cart_total_items.toString();

                TCP.Tealium.CartCookie.createCart();

                try {
                    utag.view(utag_data);
                } catch (error) {
                    console.log("Error in view call : " + error.message);
                }
            }
        }

    }

});


/**
 * TCP.Tealium.Datalayer
 * 
 * All UDO variables defined in datalayer provided by Tealium.
 */
$.extend(TCP.Tealium, {

    Datalayer: {

        /**
         *  Product Datalayer
         * 
         */
        Product: {
            referring_product: undefined,
            product_id: [],
            product_sku: [],
            product_upc: [],
            product_name: [],
            product_price: [],
            product_quantity: [],
            product_brand: [],
            product_category: [],
            product_subcategory: [],
            product_discount_amount: [],
            product_original_price: [],
            product_promo_code: [],
            product_image_url: [],
            product_url: [],
            product_on_page: []

        },


        /**
         * Customer Datalayer
         */
        buildCustomer: function () {
            return {
                'customer_id': utag_data.customer_id,
                'customer_email': utag_data.customer_email,
                'customer_city': utag_data.customer_city,
                'customer_country': utag_data.customer_country,
                'customer_first_name': utag_data.customer_first_name,
                'customer_last_name': utag_data.customer_last_name,
                'customer_state': utag_data.customer_state,
                'customer_postal_code': utag_data.customer_postal_code
            };
        },


        /**
         * Cart Datalayer
         */
        Cart: {
            cart_total_items: undefined,
            cart_total_value: undefined,
            add_to_cart_location: undefined,
            cart_product_id: [],
            cart_product_sku: [],
            cart_product_quantity: [],
            cart_product_price: [],
            cart_product_upc: []
        },


        /**
         * Order Datalayer
         */
        Order: {
            new_customer: undefined,
            site_registration_source: undefined,
            order_id: undefined,
            order_grand_total: undefined,
            order_subtotal: undefined,
            order_discount_amount: undefined,
            order_merchandise_total: undefined,
            order_payment_type: undefined,
            order_currency_code: undefined,
            order_promo_code: undefined,
            order_shipping_amount: undefined,
            order_shipping_type: undefined,
            order_store: undefined,
            order_tax_amount: undefined,
            order_type: undefined

        },

        /**
         * Page View Datalayer
         */
        buildPage: function () {
            return {
                'website_mode': utag_data.website_mode,
                'language_code': utag_data.language_code,
                'catalog_id': utag_data.catalog_id,
                'store_id': utag_data.store_id,
                'site_currency_code': utag_data.site_currency_code,
                'country_code': utag_data.country_code,
                'page_department': utag_data.page_department,
                'page_category': utag_data.page_category,
                'page_subcategory': utag_data.page_subcategory,
                'sc_page_type': utag_data.sc_page_type,
                'page_type': utag_data.page_type,
                'page_name': utag_data.page_name,
                'category_id': utag_data.category_id,
                'category_name': utag_data.category_name
            };
        },

        /**
         * Not currently being defined in UDO, but handled by Tealium configuration. 
         */
        Misc: {
            product_finding_method: undefined,
            email_signup_source: undefined,
            link_name: undefined,
            link_category: undefined
        },

        /**
         * Search Datalayer
         */
        Search: {
            search_keyword: undefined,
            search_results: undefined,
            browse_refine_type: [],
            browse_refine_value: []
        }

    }
});



/**
 * TCP.Tealium.Legacy
 * TCP.Tealium.Commerce
 * 
 * from previous omniture impl
 */
$.extend(TCP.Tealium, {

    Commerce: {
        Omniture: {
            visitorId: undefined,
            receiptId: undefined
        }
    },

    Legacy: {

        doGetVisitorId: function () {
            var visitorId;
            try {
                TCP.Tealium.Commerce.Omniture.visitorId = TCP.LocalStore.getItem('s_vi');
                if (TCP.Tealium.Commerce.Omniture.visitorId === null) {
                    TCP.Tealium.Commerce.Omniture.visitorId = '';
                }
            } catch (err) {
                console.log('Error detected in doGetVisitorId() ' + err.message);
            }

            visitorId = TCP.Tealium.Commerce.Omniture.visitorId;

            return visitorId;
        },

        doGetCustomParams: function () {
            var customParams = {};
            try {
                TCP.Tealium.Commerce.Omniture.visitorId = TCP.Tealium.Legacy.doGetVisitorId();
                customParams.visitorId = TCP.Tealium.Commerce.Omniture.visitorId;
            } catch (err) {
                console.log('Error detected in doGetCustomParams() ' + err.message);
            }
            return customParams;
        },

        getInternationalCurrency: function () {
            var intlPricingCookie = TCP.LocalStore.getItem('iShippingCookie');
            var intCode = {};
            var arr = [];
            if (intlPricingCookie != null) {
                try {
                    arr = intlPricingCookie.split('|');
                    intCode.country = arr[0];
                    intCode.currency = arr[2];
                    return intCode;
                } catch (err) {
                    console.log('Error detected in getInternationalCurrency() ' + err.message);
                }
            }
            return null;
        }

    }

});



/*\
|*|
|*|  :: cookies.js ::
|*|  A complete cookies reader/writer framework with full unicode support.
|*|  Revision #1 - September 4, 2014
|*|  https://developer.mozilla.org/en-US/docs/Web/API/document.cookie
|*|  https://developer.mozilla.org/User:fusionchess
|*|  This framework is released under the GNU Public License, version 3 or later.
|*|  http://www.gnu.org/licenses/gpl-3.0-standalone.html
|*|
\*/

var docCookies = {
    getItem: function (sKey) {
        if (!sKey) {
            return null;
        }
        return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;
    },
    setItem: function (sKey, sValue, sPath, sExpire, sDomain, bSecure) {
        if (!sKey || /^(?:expires|max\-age|path|domain|secure)$/i.test(sKey)) {
            return false;
        }
        document.cookie = encodeURIComponent(sKey) + "=" + encodeURIComponent(sValue) + sExpire + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "");
        return true;
    },
    removeItem: function (sKey, sPath, sDomain) {
        if (!this.hasItem(sKey)) {
            return false;
        }
        document.cookie = encodeURIComponent(sKey) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT" + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "");
        return true;
    },
    hasItem: function (sKey) {
        if (!sKey) {
            return false;
        }
        return (new RegExp("(?:^|;\\s*)" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=")).test(document.cookie);
    },
    keys: function () {
        var aKeys = document.cookie.replace(/((?:^|\s*;)[^\=]+)(?=;|$)|^\s*|\s*(?:\=[^;]*)?(?:\1|$)/g, "").split(/\s*(?:\=[^;]*)?;\s*/);
        for (var nLen = aKeys.length, nIdx = 0; nIdx < nLen; nIdx++) {
            aKeys[nIdx] = decodeURIComponent(aKeys[nIdx]);
        }
        return aKeys;
    }
};