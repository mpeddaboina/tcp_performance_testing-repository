//This variable can be used sitewide to choose between click or touchstart in the case of touch devicest
var touchStartOrClick = ( document.ontouchstart===null ) ? 'touchstart' : 'click'; 

// usage: log('inside coolFunc',this,arguments);
// http://paulirish.com/2009/log-a-lightweight-wrapper-for-consolelog/
window.log = function(){
  log.history = log.history || [];   // store logs to an array for reference
  log.history.push(arguments);
  if(this.console){
    console.log( Array.prototype.slice.call(arguments) );
  }
};

/* Author:
Joseph Chagan 2012
*/
(function ($) {
	$.leftNav = function (element, options) {
		var defaults = {
				autoCollapse : true,
				speed : 300
			},
			plugin = this,
			$element = $(element), // reference to the jQuery version of DOM element
			element = element,    // reference to the actual DOM element
			clicked,
			clickedContainer,
			clickedMenu,
			containers = $(element).find('li.leftNavLevel0'),
			menus = $(element).find('.leftNavLevel0 > ul'),
			foo_private_method;

		plugin.settings = {};


		plugin.init = function () {
			plugin.settings = $.extend({}, defaults, options);
		};
		// public methods
		// plugin.methodName(arg1, arg2, ... argn) from inside the plugin or
		// element.data('leftNav').publicMethod(arg1, arg2, ... argn) from outside 

		$element.find('a').click(function(event){
			event.stopPropagation();
		});
		
		$element.find('button').click(function(event){
			event.stopPropagation();
		});

		plugin.showClicked = function (clicked) {
			if (clickedMenu.css('display') !== "block") {
				clickedMenu.slideDown(plugin.settings.speed);
				clicked.addClass('selected');
			}
		};
		plugin.toggleClicked = function(){
			clickedMenu.slideToggle(plugin.settings.speed);
			clickedContainer.toggleClass('selected');
		};
		plugin.hideAll = function (clicked) {
			menus.slideUp(plugin.settings.speed);
			containers.removeClass('selected');
			if (typeof clicked != 'undefined') {
				plugin.showClicked(clicked);
			}
		};
		plugin.showAll = function () {
			containers.addClass('selected');
			menus.slideDown(plugin.settings.speed);
		};

		plugin.clickHandler = function (){
			if(plugin.settings.autoCollapse){
				if (!clickedMenu.is(':animated')) {
					if(!clickedContainer.hasClass('selected')){
						plugin.hideAll(clickedContainer);
						//log("open a new menu");	
					} else {
						plugin.hideAll();
						//log("close the open menu");
					}
				}
			} else {
				plugin.toggleClicked();
				//log("toggle just the clicked menu");
			}
		};

		$element.delegate("div .leftNavLevel0 .leftNavLevel0Title", 'click', function () {
			clicked = $(this);
			clickedContainer = clicked.parent();
			clickedMenu = clickedContainer.find('ul').first();
			plugin.clickHandler();
		});


		// private methods
		// methodName(arg1, arg2, ... argn)

		// a private method. for demonstration purposes only - remove it!
		foo_private_method = function () {
			// code goes here
		};

		// fire up the plugin!
		// call the "constructor" method
		plugin.init();

	};

	// add the plugin to the jQuery.fn object
	$.fn.leftNav = function (options) {

		// iterate through the DOM elements we are attaching the plugin to
		return this.each(function () {

			// if plugin has not already been attached to the element
			if (undefined === $(this).data('leftNav')) {

				// create a new instance of the plugin
				// pass the DOM element and the user-provided options as arguments
				var plugin = new $.leftNav(this, options);

				// in the jQuery version of the element
				// store a reference to the plugin object
				// you can later access the plugin and its methods and properties like
				// element.data('leftNav').publicMethod(arg1, arg2, ... argn) or
				// element.data('leftNav').settings.propertyName
				$(this).data('leftNav', plugin);

			}

		});

	};

})(jQuery);


/*PRODUCT SIZE CONTROLS*/
function productDisplaySizes(){
	var $swatches = $('.swatches');
	var $products = $('.product');
	var	$productSizeButtons = $('.productDisplaySizes').find('.imageSprite');

	$('.productDisplaySizes .productDisplaySmall').on( touchStartOrClick, function() {
		log('.productDisplaySmall:' + touchStartOrClick);
		if($swatches && $swatches.data('swatches')){
			$swatches.data('swatches').closeSwatchReveal();
		}	
		$productSizeButtons.removeClass('selected');
		$products.removeClass('productLarge').addClass('productSmall');	
		$(this).addClass('selected');
		createCookie("productDisplaySizes","productDisplaySmall");
		if($swatches && $swatches.data('swatches')){
			$swatches.data('swatches').setSwatchContainers();
		}		
	});

	$('.productDisplaySizes .productDisplayLarge').on( touchStartOrClick, function() {
		log('.productDisplayLarge:' + touchStartOrClick);
		if($swatches && $swatches.data('swatches')){
			$swatches.data('swatches').closeSwatchReveal();
		}
		$productSizeButtons.removeClass('selected');
		$products.removeClass('productSmall').addClass('productLarge');
		$(this).addClass('selected');
		createCookie("productDisplaySizes","productDisplayLarge");
		if($swatches && $swatches.data('swatches')){
			$swatches.data('swatches').setSwatchContainers();
		}			
	});

};

function createCookie(name,value,days) {
	if (days) {
	var date = new Date();
	date.setTime(date.getTime()+(days*24*60*60*1000));
	var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	var domain=location.hostname.substring(location.hostname.indexOf('.'));
	document.cookie = name+"="+value+expires+"; domain="+domain+"; path=/";
} 

/**
* This function gets a cookie
* @param {String} c the name of the cookie to be get.
*/
function getCookie(c) {
	var cookies = document.cookie.split(";");
	for (var i = 0; i < cookies.length; i++) {
		var index = cookies[i].indexOf("=");
		var name = cookies[i].substr(0,index);
		name = name.replace(/^\s+|\s+$/g,"");
		if (name == c) {
			return unescape(cookies[i].substr(index + 1));
		}
	}
}


function refreshProductDisplaySizes(c) {
//If no cookie value stored means leave default selection of "productDisplayLarge", otherwise change selection to productDisplaySmall
	var cookievalue = getCookie("productDisplaySizes");
	if(cookievalue && cookievalue == "productDisplaySmall"){
		var selectedproductSize=$('.productDisplaySizes').find('.productDisplaySmall');
		if(selectedproductSize){
			selectedproductSize.trigger('click');
		} 
	}
}

$(document).ready(function () {
	'use strict';
	$('body').removeClass('no-js');


	/*Place Shops Nav*/
	var placeShopsNav = $('.placeShops-menu'),
		placeShopsNavLinks = placeShopsNav.find('li'),
		placeShopsNavImage = placeShopsNav.find('img'),
		placeShopsNavImageLink = placeShopsNavImage.parent('a'),
		placeShopsNavLinksImageStore = {},
		placeShopsNavLinksImageStoreLink = {};
		placeShopsNavLinks.each(function(index){
		placeShopsNavLinksImageStore[index] = ($.trim($(this).children('a').attr('id'))) + ".png";
		if(placeShopsNavImageLink.length > 0){
		placeShopsNavLinksImageStoreLink[index] = $(this).children('a').attr('href');
		}
		$(this).mouseenter(function(){
			placeShopsNavImage.attr('src', getImageDirectoryPath()+'images/tcp/header/place_shops/' + placeShopsNavLinksImageStore[index]);
			if(placeShopsNavImageLink.length > 0){
			placeShopsNavImageLink.attr('href', placeShopsNavLinksImageStoreLink[index]);
			}
		});
});
	/*END Place Shops Nav*/




	/*QMedia Query Check*/
	/*var setupMediaQueryListener = function(mediaQueryString, ifTrue, ifFalse){
		var mql = window.matchMedia(mediaQueryString)

		mql.addListener(handleOrientationChange);

		handleOrientationChange(mql);

		function handleOrientationChange(mql) {  
			if (window.matchMedia("(min-width: 400px)").matches) {  
				//console.log("the view port is at least 400 pixels wide ");
				//ifTrue();
			} else {  
				//console.log("the view port is less than 400 pixels wide");
			} 
		}
	}
	setupMediaQueryListener("(min-width: 400px)");*/
	/*END Media Query Check*/











	productDisplaySizes();
	// Below method call will setup the image size based on "ProductDisplaySizes" cookie value in session.
	refreshProductDisplaySizes();
	/*END PRODUCT SIZE CONTROLS*/



	/*leftNav*/
	if ($('.leftNav').length > 0) {
		$('.leftNav').leftNav({'autoCollapse': false});
	}
	/*End leftNav*/

	

	//setDisplayed Vars//
	var looksContainer = $("#LooksContainer"),//long thing
		wrapper = $("#LooksInnerWrapper"),//hides over flow
		prev = $("#LooksPrev"),
		next = $("#LooksNext"),
		displayed,
		looks = $(".look"),
		animateLength = 600,
		current = 0,
		pages = 0,
	//END setDisplayed Vars//
	//createPagination Vars//
		paginationContainer = $("#LooksPagination"),
		indicators = $(".looksPage"),
		x,
		insert,
	//END createPagination Vars//
	//setOverflows//
		overflow,
		leftOverflow,
		rightOverflow,
	//END setOverflows
	//resizeCheck//
		windowWidth = $(window).width(),
		documentContainer = $("#Content"),
		min = 960,
		max = 1400,
		margin = 20,
	//END resizeCheck//
	//resizePromo//
		promo = $("#DepartmentPromo"),
		promoContainer,
		promoWidth,
		curWidth,
		images,
		imagesWidth,
	//END resizePromo//
	//buttons//
		buttons = $(".contentRightNavItem"),
		adds = $(".contentRightDisplayItem"),
		counter = 0,
		timer,
		clicked,
	//End buttons//
	//setBorders//
		borderSet,
	//END setBorders//
	//Functions//
		setDisplayed,
		createPagination,
		setOverflows,
		updatePaginationDisplay,
		updatePagination,
		resizeSite,
		resizeCheck,
		resizePromo,
		carouselNav; //todo: use this in place of 2 seperate prev/next functions
	//END Functions//

	//setup setDisplayed vars
	looksContainer.data('animation', 0);
	//setup resizeCheck vars
	min = min - margin;
	max = max - margin;


	updatePaginationDisplay = function (current) {
		indicators.removeClass("selected");
		indicators.eq(current).addClass("selected");
	};

	updatePagination = function (direction) {
		if (direction === "next") { current += 1; }
		if (direction === "prev") { current -= 1; }
		if (direction === "start") { current = 0; }
		updatePaginationDisplay(current);
		looksContainer.data('animation', 0);
	};

	createPagination = function (count) {
		indicators = $(".looksPage");
		indicators.remove();
		insert = "";
		for (x = 0; x < count; x += 1) {
			if (x === 0) {
				insert += "<div class='looksPage selected'></div>";
			} else {
				insert += "<div class='looksPage'></div>";
				if (x === count - 1) {
					paginationContainer.html(insert);
					updatePagination("start");
				}
			}
		}
		indicators = $(".looksPage");
	};

	setOverflows = function () {
		overflow = looksContainer.width() - wrapper.width();
		leftOverflow = Math.abs(parseInt(looksContainer.css("left"), 10));
		rightOverflow = overflow -  leftOverflow;
	};
	setOverflows();

	setDisplayed = function () {
		looksContainer.animate({left: 0}, animateLength, function () {
			displayed = wrapper.width() / looks.width();
			pages = looks.length / displayed;
			createPagination(pages);
			setOverflows();
		});
	};
	setDisplayed();

	//adds size classes to the targeted element
	/*resizeSite = function (target, size) {
		target.removeClass("small medium large");
		target.addClass(size);
		setDisplayed();
	};*/

	/*resizePromo = function (init) {
		if (init) {
			promoContainer = $(".contentRightDisplayItem");
			promoWidth = promoContainer.width();
			images = $(".contentRightDisplayItem img");
			imagesWidth = images.width();
			images.each(function (i) {
				$(this).width(curWidth - 205);
			});
		}
		if ((promoWidth - 250) !== imagesWidth) {
			images.each(function (i) {
				$(this).width(promoWidth - 205);
			});
		}
	};
	resizePromo(true);*/

	//called every time window is resized. Very small function can run frequently with littl eimpact
	//this is just a check, more complex functions are only called as neccesary.
	resizeCheck = function () {
		windowWidth = $(window).width();
		//promoWidth = promoContainer.width();
		if(windowWidth < 1400){

		}
		if(windowWidth >= 1400){

		}
		
		//adds small class if window is below threshold and class doesnt exist
		/*if (windowWidth < min && !documentContainer.hasClass("small")) {
			resizeSite(documentContainer, "small");
		}*/
		//if widow is between min/max medium class is added
		/*if (windowWidth >= min && windowWidth < max) {
			//resizePromo is called every time so a smooth animation of the transition can occur
			resizePromo("false"); //false  = initialize call
			if (!documentContainer.hasClass("medium")) {
				resizeSite(documentContainer, "medium");
			}
		}*/
		//adds large is window is above threshold and class doesnt exist
		/*if (windowWidth >= max && !documentContainer.hasClass("large")) {
			resizeSite(documentContainer, "large");
		}*/
	};


	buttons.click(function () {
		if (!adds.is(':animated')) {
			clicked = $(this).index();
			buttons.removeClass("selected");
			$(this).addClass("selected");
			adds.each(function (i) {
				if (i === clicked) {
					adds.eq(i).fadeIn(500);
				}
				if (adds.eq(i).css('display') === 'block' && i !== clicked) {
					adds.eq(i).fadeOut(500);
				}
			});
			counter = $(this).index();
		}
	});

	function update(killSwitch) {
		if (killSwitch === false) {
			if (counter < 3) {
				buttons.eq(counter).trigger('click');
				timer = setTimeout(function () {update(killSwitch); }, 5000);
				counter += 1;
			} else {
				counter = 0;
				update(false);
			}
		} else {
			clearTimeout(timer);
		}
	}
	update(false);

	promo.mouseenter(function () {
		update(true);
	}).mouseleave(function () {
		update(false);
	});

	next.click(function () {
		if (rightOverflow > 0 && looksContainer.data('animation') === 0) {
			looksContainer.data('animation', 1);
			looksContainer.animate({
				left : -leftOverflow - (looks.width() * displayed)
			}, animateLength, function () {setOverflows(); updatePagination("next"); });
		}
	});

	prev.click(function () {
		if (leftOverflow > 0 && looksContainer.data('animation') === 0) {
			looksContainer.data('animation', 1);
			looksContainer.animate({
				left : -leftOverflow + (looks.width() * displayed)
			}, animateLength, function () {setOverflows(); updatePagination("prev"); });
		}
	});

	function carouselNav(direction) {
		if (direction === 'next') {

		}
		if (direction === "prev") {

		}
	}

	var borderized;

	function setBorder() {
		borderized = $('nav[borderize]');

		/*$('nav')find('*borderize').each(function(index){
			log(this);
		});*/
		/*$(this).data('top', $(borderSet + "Top"));
		$(this).data('bottom', $(borderSet + "Bottom"));
		$(this).data('left', $(borderSet + "Left"));
		$(this).data('right', $(borderSet + "Right"));*/
		borderized.each(function(index){
			//log(borderized[index]);
		});
	};
	setBorder();

	$(window).resize(function () {
		resizeCheck();
	});
	resizeCheck();
    
    // Show rewards popover on click on corresponding link in the toolbar.
    var $rewardsPopoverDetails = $("#toolbar-right #rewards-popover-details");
    $(document).off("click.toolbar-rewards-popover").on("click.toolbar-rewards-popover", "#toolbar-right .rewards-popover", function (e) {
        e.preventDefault();
        $rewardsPopoverDetails.addClass("visible");
    }).off("mouseenter.toolbar-rewards-popover-close").on("mouseenter.toolbar-rewards-popover-close", function(e) {
        if ($(e.target).closest("#rewards-popover-details").length === 0 && $(e.target).closest(".rewards-popover").length === 0) {
            $rewardsPopoverDetails.removeClass("visible");
        }
    }).off("click.toolbar-rewards-popover-close").on("click.toolbar-rewards-popover-close", function(e) {
        if ($(e.target).closest("#rewards-popover-details").length === 0 && $(e.target).closest(".rewards-popover").length === 0) {
            $rewardsPopoverDetails.removeClass("visible");
        }
    }).off("mouseleave.toolbar-rewards-popover-close").on("mouseleave.toolbar-rewards-popover-close", "#toolbar-right #rewards-popover-details", function () {
        $rewardsPopoverDetails.removeClass("visible");
    });
    
    // In My Account, show my rewards program details popover on click on DETAILS link.
    var $mprInfoPopup = $("#my-account-mpr-info-popup");
    $mprInfoPopup.addClass("mpr-info-popup");
    $(document).off("click.mpr-info-popup").on("click.mpr-info-popup", ".my-account-mpr-info-button", function (e) {
        e.preventDefault();
        $.ajax({
            url: "/wcs/resources/store/10151/espots/my-account-mpr-info?responseFormat=json",
            success: function (data) {
                if (data.List && data.List[0] && data.List[0].maketingText) {
                    $mprInfoPopup.html(data.List[0].maketingText);
                    $mprInfoPopup.append("<a class=\"close-button\" href=\"#\">Close</a>");
                    $mprInfoPopup.addClass("visible");
                }
            }
        });
    }).off("click.mpr-info-popup-close").on("click.mpr-info-popup-close", function(e) {
        if ($(e.target).closest("#my-account-mpr-info-popup").length === 0 || $(e.target).hasClass("close-button")) {
            $mprInfoPopup.removeClass("visible");
        }
    });
});
