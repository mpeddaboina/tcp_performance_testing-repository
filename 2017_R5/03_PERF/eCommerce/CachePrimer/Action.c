Action()
{
	web_set_max_html_param_len ( "3072" ) ;
	web_cleanup_cookies ( ) ;
	web_cache_cleanup();
	web_set_sockets_option("SSL_VERSION", "TLS1.1"); 
	
	web_custom_request("Browse Request",  
		"URL=https://tcp-perf.childrensplace.com/{cacheURL}", 
		"Method=GET", 
		"RecContentType=text/html", 
		"Mode=HTML", 
		LAST);
	
	return 0;
}
