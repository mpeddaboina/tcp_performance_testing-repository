Action()
{
	web_set_sockets_option("SSL_VERSION", "TLS"); 
	
	//This is R5 version E:\Performance\Scripts\2017_R5
	web_set_max_html_param_len ( "3072" ) ;
	web_cleanup_cookies () ;
	web_cache_cleanup();
	isLoggedIn=0;
	ONLINE_ORDER_SUBMIT=0;
	API_SUB_TRANSACTION_SWITCH = 1;

	if (CACHE_PRIME_MODE == 1)
		RATIO_CHECKOUT_LOGIN_FIRST = 0;
	
//	Temp Debugging Parameters
//	RATIO_PROMO_MULTIUSE = 0;
//	CACHE_PRIME_MODE = 0; //1 - cacheprime only, 0 - full scenario mode
//	RATIO_PROMO_SINGLEUSE = 100;
	RATIO_DROP_CART = 0; //45;
	RATIO_CHECKOUT_GUEST = 100; 
//	RATIO_CHECKOUT_LOGIN_FIRST = 100;
//	RATIO_POINTS_HISTORY = 0;
//	RATIO_ORDER_HISTORY = 0;
//	RATIO_REDEEM_LOYALTY_POINTS = 0;
	lr_save_string("10151", "storeId");
	lr_save_string("10551", "catalogId");
	lr_save_string("us", "country");
	lr_save_string("false", "addBopisToCart");
	lr_save_string("false", "addEcommToCart");
	lr_save_string(lr_eval_string("{RANDOM_PERCENT}"), "akamaiRatio" );
//	lr_save_string( lr_eval_string("{logonid}"), "userEmail" );
//	lr_save_string( "manny123456@gmail.com", "userEmail" ); //uatlive3
//	lr_save_string( "manny987654@gmail.com", "userEmail" ); //uatlive2
//	lr_save_string( "manny456789@gmail.com", "userEmail" ); //uatlive1
//	lr_save_string( "manny654321@gmail.com", "userEmail" ); //perflive
/*	
loginFirst	
loginFromHomePage();	
viewCart();
proceedToCheckout_ShippingView();
submitShippingRegistered();
submitBillingRegistered();
submitOrderRegistered();
*/
/*
//browse first
//buildCartCheckout(0);
login();
accountRewards();	
lr_abort();
submitShippingBrowseFirst();
submitBillingRegistered();
submitOrderRegistered();
lr_abort();

//	StoreLocator();
//	profile();
//	storeLocator();
//	viewMyAccount();
//	webInstantCredit();
//buildCartCheckout(0);	
*/
/*	viewHomePage();
topNav();
paginate();			
lr_abort();
*/
	viewHomePage();
	
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_DROP_CART ) {
		dropCart();
	} else {
		checkOut(); 
	}

	return 0;
}
