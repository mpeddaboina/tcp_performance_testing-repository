dropCart()
{
	lr_user_data_point("Abandon Cart Rate", 1);
	lr_user_data_point("Checkout Rate", 0);
	lr_save_string("dropCartFlow", "currentFlow");
	
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CHECKOUT_GUEST ) {

		dropCartGuest();
	} 
	else {
		lr_save_string( lr_eval_string("{loginid_drop}"), "userEmail" );
//		lr_save_string( "manny456789@gmail.com", "userEmail" ); //uatlive1		
		
		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CHECKOUT_LOGIN_FIRST ) { //login first

			dropCartLoginFirst();
 
        }
        else { //build cart first

			dropCartBuildCartFirst();
            
        }
		
	} 

	return 0;
}

dropCartGuest()
{
	int WIC_Submit = 0;
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_WIC )
	{	if (webInstantCredit() != LR_FAIL )
			WIC_Submit = 1;
	}
	
	if( atoi(lr_eval_string("{akamaiRatio}")) <= NO_BROWSE_RATIO )
    	buildCartDropNoBrowse(0);//
	else {
		buildCartDrop(0);
	}

	if (CACHE_PRIME_MODE == 1)
		return; 

	inCartEdits();
	
	if (proceedAsGuest() == LR_PASS)
	{
		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= SHIP_PAGE_DROP )
		{		dropCartTransaction();
				return 0;
		}
		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_RTPS && WIC_Submit == 0 )
		{	if (submitRTPS() == LR_FAIL)
				return 0;
		}
		else
		{	if ( submitShipping() == LR_FAIL)  //former submitShippingAddressAsGuest()
			  return 0;
		}
		
		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= BILL_PAGE_DROP )
		{		dropCartTransaction();
				return 0;
		}
		if (BillingAsGuest() == LR_PASS) //submitBillingAddressAsGuest()
		{
			dropCartTransaction();
			
		}
	}
	
	return 0;
}

dropCartLoginFirst()
{
	int viewHistory = atoi(lr_eval_string("{RANDOM_PERCENT}"));
	isLoggedIn=1;	
	if (loginFromHomePage() == LR_PASS)
	{
        if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ACCOUNT_OVERVIEW )
        	viewMyAccount();
         
	    viewCart();  // 0712TookOut, 0713Added back
		 
		if( atoi(lr_eval_string("{akamaiRatio}")) <= NO_BROWSE_RATIO )
	    	buildCartDropNoBrowse(1);//
		else {
			buildCartDrop(1);
		}

		if (CACHE_PRIME_MODE == 1)
			return; 

        if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ACCOUNT_OVERVIEW )
        	viewMyAccount();
        
		if ( strcmp(strupr(lr_eval_string("{buildCart}")),  "TRUE") != 0) 
			inCartEdits();

        if ( atoi(lr_eval_string("{RANDOM_WL_PERCENT}")) <= RATIO_WISHLIST )
            wishList();

		if (proceedToCheckout_ShippingView() == LR_PASS) 
		{
    	
	        if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= SHIP_PAGE_DROP )
			{		dropCartTransaction();
					return 0;
			}

			if (submitShippingRegistered() == LR_PASS)
			{
	            if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= BILL_PAGE_DROP )
				{		dropCartTransaction();
						return 0;
				}

				if (submitBillingRegistered() == LR_PASS)
					dropCartTransaction();

            }
        }

	}
	
	return 0;
}

dropCartBuildCartFirst()
{
	int viewHistory = atoi(lr_eval_string("{RANDOM_PERCENT}"));
	int WIC_Submit = 0;
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_WIC )
	{	if (webInstantCredit() != LR_FAIL )
			WIC_Submit = 1;
	}
	
	if( atoi(lr_eval_string("{akamaiRatio}")) <= NO_BROWSE_RATIO )
    	buildCartDropNoBrowse(0);//
	else {
		buildCartDrop(0);
	}

	if (CACHE_PRIME_MODE == 1)
		return; 

	inCartEdits();
	
    if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= LOGIN_PAGE_DROP )
	{		dropCartTransaction();
			return 0;
	}
	isLoggedIn=1;
	if (login() == LR_PASS) 
	{
        if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ACCOUNT_OVERVIEW )
        	viewMyAccount();
        
	    if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= SHIP_PAGE_DROP )
		{		dropCartTransaction();
				return 0;
		}
		if (submitShippingBrowseFirst() == LR_PASS) 
		{
	        if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= BILL_PAGE_DROP )
			{		dropCartTransaction();
					return 0;
			}
		     if (submitBillingRegistered() == LR_PASS ) {
				dropCartTransaction();
			}
		}
		            
	}

 	return 0;
}