checkOut()
{
	lr_user_data_point("Abandon Cart Rate", 0);
	lr_user_data_point("Checkout Rate", 1);
	lr_save_string("checkoutFlow", "currentFlow");
	
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CHECKOUT_GUEST ) {
		
		checkoutGuest();
	} 
	else {
		lr_save_string( lr_eval_string("{logonid}"), "userEmail" );
//		lr_save_string( "manny123456@gmail.com", "userEmail" ); //uatlive3
//		lr_save_string( "manny456789@gmail.com", "userEmail" ); //uatlive1		
//		lr_save_string( "manny654321@gmail.com", "userEmail" ); //perflive
  		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CHECKOUT_LOGIN_FIRST ) { //login first
			
			checkoutLoginFirst();
        }
        else { 
			
			checkoutBuildCartFirst();
        } 
	}

	return 0;
}

checkoutGuest() 
{
	if( atoi(lr_eval_string("{akamaiRatio}")) <= NO_BROWSE_RATIO )
    	buildCartCheckoutNoBrowse(0);//
	else {
		buildCartCheckout(0);
	}

	if (CACHE_PRIME_MODE == 1)
		return; 

	if (strcmp( lr_eval_string("{addBopisToCart}"), "true") != 0 ) 
		inCartEdits();
		
	if (proceedAsGuest() == LR_PASS)
	{
		if (strcmp( lr_eval_string("{addBopisToCart}"), "true") == 0 && strcmp( lr_eval_string("{addEcommToCart}"), "false") == 0 ) //if bopis ONLY, no need to submitShipping
		{
			if (BillingAsGuest() == LR_PASS) //submitBillingAddressAsGuest()
			{
				if( submitOrderAsGuest() == LR_PASS)
				{
					if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_REGISTER ) {
						createAccount(); // former //registerUser();
					}
					if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_STORE_LOCATOR ) {
						StoreLocator();
					}
					
//					forgetPassword();	
				}
			}
		}
		else {
			if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_RTPS )
			{	if (submitRTPS() == LR_FAIL)
					return 0;
			}
			else
			{	if ( submitShipping() == LR_FAIL)  //former submitShippingAddressAsGuest()
				  return 0;
			}

			
			if (BillingAsGuest() == LR_PASS) //submitBillingAddressAsGuest()
			{
				if( submitOrderAsGuest() == LR_PASS)
				{
					if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ORDER_STATUS ) { //added 06/07 rhy
            			viewOrderStatusGuest();
					}
					if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_REGISTER ) {
						createAccount(); // former //registerUser();
					}
					if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_STORE_LOCATOR ) {
						StoreLocator();
					}
					
				    if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_FORGOT_PASSWORD ) 
						forgetPassword();	
				}
			}
			
		}
	}

	
	return 0;
}

checkoutLoginFirst()
{
	int viewHistory = atoi(lr_eval_string("{RANDOM_PERCENT}"));
	isLoggedIn=1;	
	if (loginFromHomePage() == LR_PASS)
	{
	    viewCart();

		if( atoi(lr_eval_string("{akamaiRatio}")) <= NO_BROWSE_RATIO )
	    	buildCartCheckoutNoBrowse(1);//
		else {
			buildCartCheckout(1);
		}

		if (CACHE_PRIME_MODE == 1)
			return; 

		if (strcmp( lr_eval_string("{addBopisToCart}"), "true") != 0 ) 
			inCartEdits();
		
	    if ( atoi(lr_eval_string("{RANDOM_WL_PERCENT}")) <= RATIO_WISHLIST )
	        wishList();
	    
	    if (proceedToCheckout_ShippingView() == LR_PASS) 
	    {
			if (strcmp( lr_eval_string("{addBopisToCart}"), "true") == 0 && strcmp( lr_eval_string("{addEcommToCart}"), "false") == 0 ) //if bopis ONLY, no need to submitShipping
			{
				if (submitBillingRegistered() == LR_PASS)
					submitOrderRegistered();
			}
			else
			{
/*				if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_RTPS )
				{	if (submitRTPS() == LR_FAIL)
						return 0;
				}
				else
				{	if ( submitShippingRegistered() == LR_FAIL)  //former submitShippingAddressAsGuest()
					  return 0;
				}
*/			
				if (submitShippingRegistered() == LR_PASS)
				{
					if (submitBillingRegistered() == LR_PASS) 
					{
						if (submitOrderRegistered() == LR_PASS) 
						{
							if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 5 ) 
				        		viewMyAccount();
						}
					}
				}
			}
	    }
	    
	    if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_LOGOUT ) 
	    	logoff();
	    
	    if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_FORGOT_PASSWORD ) 
			forgetPassword();	
	    
	    if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_STORE_LOCATOR ) {
			StoreLocator();
		}
	    
	    if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 50 ) 
	   		newsLetterSignup();
	    
	}

	return 0;
}

checkoutBuildCartFirst()
{
	int viewHistory = atoi(lr_eval_string("{RANDOM_PERCENT}"));

	if( atoi(lr_eval_string("{akamaiRatio}")) <= NO_BROWSE_RATIO )
    	buildCartCheckoutNoBrowse(0);
	else {
		buildCartCheckout(0);
	}

	if (CACHE_PRIME_MODE == 1)
		return; 

	inCartEdits(); 
//	if (atoi(lr_eval_string("{cartCount}")) >= 1) 
//	{
	isLoggedIn=1;		
	if (login() == LR_PASS) // login->Proceed to Checkout
	{
		if (strcmp( lr_eval_string("{addBopisToCart}"), "true") == 0 && strcmp( lr_eval_string("{addEcommToCart}"), "false") == 0 ) //if bopis ONLY, no need to submitShipping
		{
			if (submitBillingRegistered() == LR_PASS)
				submitOrderRegistered();
		}
		else if (submitShippingBrowseFirst() == LR_PASS)
		{
			if (submitBillingRegistered() == LR_PASS)
				submitOrderRegistered();
		}
		
	    if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_LOGOUT ) 
	    	logoff();
	    
	    if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_FORGOT_PASSWORD ) 
			forgetPassword();	
	    
	}
    if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_STORE_LOCATOR ) {
		StoreLocator();
	}
    
    if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 50 ) 
   		newsLetterSignup();
//	}// if (atoi(lr_eval_string("{cartCount}")) >= 1) 
	return 0;
	
} // END - if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CHECKOUT_LOGIN_FIRST ) { //login first		
