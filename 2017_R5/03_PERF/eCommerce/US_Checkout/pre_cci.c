# 1 "e:\\performance\\scripts\\2017_r5\\03_perf\\ecommerce\\us_checkout\\\\combined_US_Checkout.c"
# 1 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h" 1
 
 












 











# 103 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h"





















































		


		typedef unsigned size_t;
	
	
        
	

















	

 



















 
 
 
 
 


 
 
 
 
 
 














int     lr_start_transaction   (char * transaction_name);
int lr_start_sub_transaction          (char * transaction_name, char * trans_parent);
long lr_start_transaction_instance    (char * transaction_name, long parent_handle);
int   lr_start_cross_vuser_transaction		(char * transaction_name, char * trans_id_param); 



int     lr_end_transaction     (char * transaction_name, int status);
int lr_end_sub_transaction            (char * transaction_name, int status);
int lr_end_transaction_instance       (long transaction, int status);
int   lr_end_cross_vuser_transaction	(char * transaction_name, char * trans_id_param, int status);


 
typedef char* lr_uuid_t;
 



lr_uuid_t lr_generate_uuid();

 


int lr_generate_uuid_free(lr_uuid_t uuid);

 



int lr_generate_uuid_on_buf(lr_uuid_t buf);

   
# 273 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h"
int lr_start_distributed_transaction  (char * transaction_name, lr_uuid_t correlator, long timeout  );

   







int lr_end_distributed_transaction  (lr_uuid_t correlator, int status);


double lr_stop_transaction            (char * transaction_name);
double lr_stop_transaction_instance   (long parent_handle);


void lr_resume_transaction           (char * trans_name);
void lr_resume_transaction_instance  (long trans_handle);


int lr_update_transaction            (const char *trans_name);


 
void lr_wasted_time(long time);


 
int lr_set_transaction(const char *name, double duration, int status);
 
long lr_set_transaction_instance(const char *name, double duration, int status, long parent_handle);


int   lr_user_data_point                      (char *, double);
long lr_user_data_point_instance                   (char *, double, long);
 



int lr_user_data_point_ex(const char *dp_name, double value, int log_flag);
long lr_user_data_point_instance_ex(const char *dp_name, double value, long parent_handle, int log_flag);


int lr_transaction_add_info      (const char *trans_name, char *info);
int lr_transaction_instance_add_info   (long trans_handle, char *info);
int lr_dpoint_add_info           (const char *dpoint_name, char *info);
int lr_dpoint_instance_add_info        (long dpoint_handle, char *info);


double lr_get_transaction_duration       (char * trans_name);
double lr_get_trans_instance_duration    (long trans_handle);
double lr_get_transaction_think_time     (char * trans_name);
double lr_get_trans_instance_think_time  (long trans_handle);
double lr_get_transaction_wasted_time    (char * trans_name);
double lr_get_trans_instance_wasted_time (long trans_handle);
int    lr_get_transaction_status		 (char * trans_name);
int	   lr_get_trans_instance_status		 (long trans_handle);

 



int lr_set_transaction_status(int status);

 



int lr_set_transaction_status_by_name(int status, const char *trans_name);
int lr_set_transaction_instance_status(int status, long trans_handle);


typedef void* merc_timer_handle_t;
 

merc_timer_handle_t lr_start_timer();
double lr_end_timer(merc_timer_handle_t timer_handle);


 
 
 
 
 
 











 



int   lr_rendezvous  (char * rendezvous_name);
 




int   lr_rendezvous_ex (char * rendezvous_name);



 
 
 
 
 
char *lr_get_vuser_ip (void);
void   lr_whoami (int *vuser_id, char ** sgroup, int *scid);
char *	  lr_get_host_name (void);
char *	  lr_get_master_host_name (void);

 
long     lr_get_attrib_long	(char * attr_name);
char *   lr_get_attrib_string	(char * attr_name);
double   lr_get_attrib_double      (char * attr_name);

char * lr_paramarr_idx(const char * paramArrayName, unsigned int index);
char * lr_paramarr_random(const char * paramArrayName);
int    lr_paramarr_len(const char * paramArrayName);

int	lr_param_unique(const char * paramName);
int lr_param_sprintf(const char * paramName, const char * format, ...);


 
 
static void *ci_this_context = 0;






 








void lr_continue_on_error (int lr_continue);
char *   lr_unmask (const char *EncodedString);
char *   lr_decrypt (const char *EncodedString);


 
 
 
 
 
 



 







 















void   lr_abort (void);
void lr_exit(int exit_option, int exit_status);
void lr_abort_ex (unsigned long flags);

void   lr_peek_events (void);


 
 
 
 
 


void   lr_think_time (double secs);

 


void lr_force_think_time (double secs);


 
 
 
 
 



















int   lr_msg (char * fmt, ...);
int   lr_debug_message (unsigned int msg_class,
									    char * format,
										...);
# 513 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h"
void   lr_new_prefix (int type,
                                 char * filename,
                                 int line);
# 516 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h"
int   lr_log_message (char * fmt, ...);
int   lr_message (char * fmt, ...);
int   lr_error_message (char * fmt, ...);
int   lr_output_message (char * fmt, ...);
int   lr_vuser_status_message (char * fmt, ...);
int   lr_error_message_without_fileline (char * fmt, ...);
int   lr_fail_trans_with_error (char * fmt, ...);

 
 
 
 
 
# 540 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h"

 
 
 
 
 





int   lr_next_row ( char * table);
int lr_advance_param ( char * param);



														  
														  

														  
														  

													      
 


char *   lr_eval_string (char * str);
int   lr_eval_string_ext (const char *in_str,
                                     unsigned long const in_len,
                                     char ** const out_str,
                                     unsigned long * const out_len,
                                     unsigned long const options,
                                     const char *file,
								     long const line);
# 574 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h"
void   lr_eval_string_ext_free (char * * pstr);

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
int lr_param_increment (char * dst_name,
                              char * src_name);
# 597 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h"













											  
											  

											  
											  
											  

int	  lr_save_var (char *              param_val,
							  unsigned long const param_val_len,
							  unsigned long const options,
							  char *			  param_name);
# 621 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h"
int   lr_save_string (const char * param_val, const char * param_name);



int   lr_set_custom_error_message (const char * param_val, ...);

int   lr_remove_custom_error_message ();


int   lr_free_parameter (const char * param_name);
int   lr_save_int (const int param_val, const char * param_name);
int   lr_save_timestamp (const char * tmstampParam, ...);
int   lr_save_param_regexp (const char *bufferToScan, unsigned int bufSize, ...);

int   lr_convert_double_to_integer (const char *source_param_name, const char * target_param_name);
int   lr_convert_double_to_double (const char *source_param_name, const char *format_string, const char * target_param_name);

 
 
 
 
 
 
# 700 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h"
void   lr_save_datetime (const char *format, int offset, const char *name);









 











 
 
 
 
 






 



char * lr_error_context_get_entry (char * key);

 



long   lr_error_context_get_error_id (void);


 
 
 

int lr_table_get_rows_num (char * param_name);

int lr_table_get_cols_num (char * param_name);

char * lr_table_get_cell_by_col_index (char * param_name, int row, int col);

char * lr_table_get_cell_by_col_name (char * param_name, int row, const char* col_name);

int lr_table_get_column_name_by_index (char * param_name, int col, 
											char * * const col_name,
											size_t * col_name_len);
# 761 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h"

int lr_table_get_column_name_by_index_free (char * col_name);

 
 
 
 
# 776 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h"
int   lr_zip (const char* param1, const char* param2);
int   lr_unzip (const char* param1, const char* param2);

 
 
 
 
 
 
 
 

 
 
 
 
 
 
int   lr_param_substit (char * file,
                                   int const line,
                                   char * in_str,
                                   size_t const in_len,
                                   char * * const out_str,
                                   size_t * const out_len);
# 800 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h"
void   lr_param_substit_free (char * * pstr);


 
# 812 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h"





char *   lrfnc_eval_string (char * str,
                                      char * file_name,
                                      long const line_num);
# 820 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h"


int   lrfnc_save_string ( const char * param_val,
                                     const char * param_name,
                                     const char * file_name,
                                     long const line_num);
# 826 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h"

int   lrfnc_free_parameter (const char * param_name );







typedef struct _lr_timestamp_param
{
	int iDigits;
}lr_timestamp_param;

extern const lr_timestamp_param default_timestamp_param;

int   lrfnc_save_timestamp (const char * param_name, const lr_timestamp_param* time_param);

int lr_save_searched_string(char * buffer, long buf_size, unsigned int occurrence,
			    char * search_string, int offset, unsigned int param_val_len, 
			    char * param_name);

 
char *   lr_string (char * str);

 
# 929 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h"

int   lr_save_value (char * param_val,
                                unsigned long const param_val_len,
                                unsigned long const options,
                                char * param_name,
                                char * file_name,
                                long const line_num);
# 936 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h"


 
 
 
 
 











int   lr_printf (char * fmt, ...);
 
int   lr_set_debug_message (unsigned int msg_class,
                                       unsigned int swtch);
# 958 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h"
unsigned int   lr_get_debug_message (void);


 
 
 
 
 

void   lr_double_think_time ( double secs);
void   lr_usleep (long);


 
 
 
 
 
 




int *   lr_localtime (long offset);


int   lr_send_port (long port);


# 1034 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h"



struct _lr_declare_identifier{
	char signature[24];
	char value[128];
};

int   lr_pt_abort (void);

void vuser_declaration (void);






# 1063 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h"


# 1075 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h"
















 
 
 
 
 







int    _lr_declare_transaction   (char * transaction_name);


 
 
 
 
 







int   _lr_declare_rendezvous  (char * rendezvous_name);

 
 
 
 
 


typedef int PVCI;






typedef int VTCERR;









PVCI   vtc_connect(char * servername, int portnum, int options);
VTCERR   vtc_disconnect(PVCI pvci);
VTCERR   vtc_get_last_error(PVCI pvci);
VTCERR   vtc_query_column(PVCI pvci, char * columnName, int columnIndex, char * *outvalue);
VTCERR   vtc_query_row(PVCI pvci, int rowIndex, char * **outcolumns, char * **outvalues);
VTCERR   vtc_send_message(PVCI pvci, char * column, char * message, unsigned short *outRc);
VTCERR   vtc_send_if_unique(PVCI pvci, char * column, char * message, unsigned short *outRc);
VTCERR   vtc_send_row1(PVCI pvci, char * columnNames, char * messages, char * delimiter, unsigned char sendflag, unsigned short *outUpdates);
VTCERR   vtc_update_message(PVCI pvci, char * column, int index , char * message, unsigned short *outRc);
VTCERR   vtc_update_message_ifequals(PVCI pvci, char * columnName, int index,	char * message, char * ifmessage, unsigned short 	*outRc);
VTCERR   vtc_update_row1(PVCI pvci, char * columnNames, int index , char * messages, char * delimiter, unsigned short *outUpdates);
VTCERR   vtc_retrieve_message(PVCI pvci, char * column, char * *outvalue);
VTCERR   vtc_retrieve_messages1(PVCI pvci, char * columnNames, char * delimiter, char * **outvalues);
VTCERR   vtc_retrieve_row(PVCI pvci, char * **outcolumns, char * **outvalues);
VTCERR   vtc_rotate_message(PVCI pvci, char * column, char * *outvalue, unsigned char sendflag);
VTCERR   vtc_rotate_messages1(PVCI pvci, char * columnNames, char * delimiter, char * **outvalues, unsigned char sendflag);
VTCERR   vtc_rotate_row(PVCI pvci, char * **outcolumns, char * **outvalues, unsigned char sendflag);
VTCERR   vtc_increment(PVCI pvci, char * column, int index , int incrValue, int *outValue);
VTCERR   vtc_clear_message(PVCI pvci, char * column, int index , unsigned short *outRc);
VTCERR   vtc_clear_column(PVCI pvci, char * column, unsigned short *outRc);
VTCERR   vtc_ensure_index(PVCI pvci, char * column, unsigned short *outRc);
VTCERR   vtc_drop_index(PVCI pvci, char * column, unsigned short *outRc);
VTCERR   vtc_clear_row(PVCI pvci, int rowIndex, unsigned short *outRc);
VTCERR   vtc_create_column(PVCI pvci, char * column,unsigned short *outRc);
VTCERR   vtc_column_size(PVCI pvci, char * column, int *size);
void   vtc_free(char * msg);
void   vtc_free_list(char * *msglist);

VTCERR   lrvtc_connect(char * servername, int portnum, int options);
VTCERR   lrvtc_disconnect();
VTCERR   lrvtc_query_column(char * columnName, int columnIndex);
VTCERR   lrvtc_query_row(int columnIndex);
VTCERR   lrvtc_send_message(char * columnName, char * message);
VTCERR   lrvtc_send_if_unique(char * columnName, char * message);
VTCERR   lrvtc_send_row1(char * columnNames, char * messages, char * delimiter, unsigned char sendflag);
VTCERR   lrvtc_update_message(char * columnName, int index , char * message);
VTCERR   lrvtc_update_message_ifequals(char * columnName, int index, char * message, char * ifmessage);
VTCERR   lrvtc_update_row1(char * columnNames, int index , char * messages, char * delimiter);
VTCERR   lrvtc_retrieve_message(char * columnName);
VTCERR   lrvtc_retrieve_messages1(char * columnNames, char * delimiter);
VTCERR   lrvtc_retrieve_row();
VTCERR   lrvtc_rotate_message(char * columnName, unsigned char sendflag);
VTCERR   lrvtc_rotate_messages1(char * columnNames, char * delimiter, unsigned char sendflag);
VTCERR   lrvtc_rotate_row(unsigned char sendflag);
VTCERR   lrvtc_increment(char * columnName, int index , int incrValue);
VTCERR   lrvtc_noop();
VTCERR   lrvtc_clear_message(char * columnName, int index);
VTCERR   lrvtc_clear_column(char * columnName); 
VTCERR   lrvtc_ensure_index(char * columnName); 
VTCERR   lrvtc_drop_index(char * columnName); 
VTCERR   lrvtc_clear_row(int rowIndex);
VTCERR   lrvtc_create_column(char * columnName);
VTCERR   lrvtc_column_size(char * columnName);



 
 
 
 
 

 
int lr_enable_ip_spoofing();
int lr_disable_ip_spoofing();


 




int lr_convert_string_encoding(char * sourceString, char * fromEncoding, char * toEncoding, char * paramName);
int lr_read_file(const char *filename, const char *outputParam, int continueOnError);

int lr_get_char_count(const char * string);


 
int lr_db_connect (char * pFirstArg, ...);
int lr_db_disconnect (char * pFirstArg,	...);
int lr_db_executeSQLStatement (char * pFirstArg, ...);
int lr_db_dataset_action(char * pFirstArg, ...);
int lr_checkpoint(char * pFirstArg,	...);
int lr_db_getvalue(char * pFirstArg, ...);







 
 



















# 1 "e:\\performance\\scripts\\2017_r5\\03_perf\\ecommerce\\us_checkout\\\\combined_US_Checkout.c" 2

# 1 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/SharedParameter.h" 1



 
 
 
 
# 100 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/SharedParameter.h"






typedef int PVCI2;






typedef int VTCERR2;


 
 
 

 
extern PVCI2    vtc_connect(char *servername, int portnum, int options);
extern VTCERR2  vtc_disconnect(PVCI2 pvci);
extern VTCERR2  vtc_get_last_error(PVCI2 pvci);

 
extern VTCERR2  vtc_query_column(PVCI2 pvci, char *columnName, int columnIndex, char **outvalue);
extern VTCERR2  vtc_query_row(PVCI2 pvci, int columnIndex, char ***outcolumns, char ***outvalues);
extern VTCERR2  vtc_send_message(PVCI2 pvci, char *column, char *message, unsigned short *outRc);
extern VTCERR2  vtc_send_if_unique(PVCI2 pvci, char *column, char *message, unsigned short *outRc);
extern VTCERR2  vtc_send_row1(PVCI2 pvci, char *columnNames, char *messages, char *delimiter,  unsigned char sendflag, unsigned short *outUpdates);
extern VTCERR2  vtc_update_message(PVCI2 pvci, char *column, int index , char *message, unsigned short *outRc);
extern VTCERR2  vtc_update_message_ifequals(PVCI2 pvci, char	*columnName, int index,	char *message, char	*ifmessage,	unsigned short 	*outRc);
extern VTCERR2  vtc_update_row1(PVCI2 pvci, char *columnNames, int index , char *messages, char *delimiter, unsigned short *outUpdates);
extern VTCERR2  vtc_retrieve_message(PVCI2 pvci, char *column, char **outvalue);
extern VTCERR2  vtc_retrieve_messages1(PVCI2 pvci, char *columnNames, char *delimiter, char ***outvalues);
extern VTCERR2  vtc_retrieve_row(PVCI2 pvci, char ***outcolumns, char ***outvalues);
extern VTCERR2  vtc_rotate_message(PVCI2 pvci, char *column, char **outvalue, unsigned char sendflag);
extern VTCERR2  vtc_rotate_messages1(PVCI2 pvci, char *columnNames, char *delimiter, char ***outvalues, unsigned char sendflag);
extern VTCERR2  vtc_rotate_row(PVCI2 pvci, char ***outcolumns, char ***outvalues, unsigned char sendflag);
extern VTCERR2	vtc_increment(PVCI2 pvci, char *column, int index , int incrValue, int *outValue);
extern VTCERR2  vtc_clear_message(PVCI2 pvci, char *column, int index , unsigned short *outRc);
extern VTCERR2  vtc_clear_column(PVCI2 pvci, char *column, unsigned short *outRc);

extern VTCERR2  vtc_clear_row(PVCI2 pvci, int rowIndex, unsigned short *outRc);

extern VTCERR2  vtc_create_column(PVCI2 pvci, char *column,unsigned short *outRc);
extern VTCERR2  vtc_column_size(PVCI2 pvci, char *column, int *size);
extern VTCERR2  vtc_ensure_index(PVCI2 pvci, char *column, unsigned short *outRc);
extern VTCERR2  vtc_drop_index(PVCI2 pvci, char *column, unsigned short *outRc);

extern VTCERR2  vtc_noop(PVCI2 pvci);

 
extern void vtc_free(char *msg);
extern void vtc_free_list(char **msglist);

 


 




 




















 




 
 
 

extern VTCERR2  lrvtc_connect(char *servername, int portnum, int options);
extern VTCERR2  lrvtc_disconnect();
extern VTCERR2  lrvtc_query_column(char *columnName, int columnIndex);
extern VTCERR2  lrvtc_query_row(int columnIndex);
extern VTCERR2  lrvtc_send_message(char *columnName, char *message);
extern VTCERR2  lrvtc_send_if_unique(char *columnName, char *message);
extern VTCERR2  lrvtc_send_row1(char *columnNames, char *messages, char *delimiter,  unsigned char sendflag);
extern VTCERR2  lrvtc_update_message(char *columnName, int index , char *message);
extern VTCERR2  lrvtc_update_message_ifequals(char *columnName, int index, char 	*message, char *ifmessage);
extern VTCERR2  lrvtc_update_row1(char *columnNames, int index , char *messages, char *delimiter);
extern VTCERR2  lrvtc_retrieve_message(char *columnName);
extern VTCERR2  lrvtc_retrieve_messages1(char *columnNames, char *delimiter);
extern VTCERR2  lrvtc_retrieve_row();
extern VTCERR2  lrvtc_rotate_message(char *columnName, unsigned char sendflag);
extern VTCERR2  lrvtc_rotate_messages1(char *columnNames, char *delimiter, unsigned char sendflag);
extern VTCERR2  lrvtc_rotate_row(unsigned char sendflag);
extern VTCERR2  lrvtc_increment(char *columnName, int index , int incrValue);
extern VTCERR2  lrvtc_clear_message(char *columnName, int index);
extern VTCERR2  lrvtc_clear_column(char *columnName);
extern VTCERR2  lrvtc_clear_row(int rowIndex);
extern VTCERR2  lrvtc_create_column(char *columnName);
extern VTCERR2  lrvtc_column_size(char *columnName);
extern VTCERR2  lrvtc_ensure_index(char *columnName);
extern VTCERR2  lrvtc_drop_index(char *columnName);

extern VTCERR2  lrvtc_noop();

 
 
 

                               


 
 
 





















# 2 "e:\\performance\\scripts\\2017_r5\\03_perf\\ecommerce\\us_checkout\\\\combined_US_Checkout.c" 2

# 1 "globals.h" 1



 
 

# 1 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/web_api.h" 1







# 1 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/as_web.h" 1



























































 




 



 











 





















 
 
 

  int
	web_add_filter(
		const char *		mpszArg,
		...
	);									 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_add_auto_filter(
		const char *		mpszArg,
		...
	);									 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
	
  int
	web_add_auto_header(
		const char *		mpszHeader,
		const char *		mpszValue);

  int
	web_add_header(
		const char *		mpszHeader,
		const char *		mpszValue);
  int
	web_add_cookie(
		const char *		mpszCookie);
  int
	web_cleanup_auto_headers(void);
  int
	web_cleanup_cookies(void);
  int
	web_concurrent_end(
		const char * const	mpszReserved,
										 
		...								 
	);
  int
	web_concurrent_start(
		const char * const	mpszConcurrentGroupName,
										 
										 
		...								 
										 
	);
  int
	web_create_html_param(
		const char *		mpszParamName,
		const char *		mpszLeftDelim,
		const char *		mpszRightDelim);
  int
	web_create_html_param_ex(
		const char *		mpszParamName,
		const char *		mpszLeftDelim,
		const char *		mpszRightDelim,
		const char *		mpszNum);
  int
	web_custom_request(
		const char *		mpszReqestName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	spdy_custom_request(
		const char *		mpszReqestName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_disable_keep_alive(void);
  int
	web_enable_keep_alive(void);
  int
	web_find(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_get_int_property(
		const int			miHttpInfoType);
  int
	web_image(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_image_check(
		const char *		mpszName,
		...);
  int
	web_java_check(
		const char *		mpszName,
		...);
  int
	web_link(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

	
  int
	web_global_verification(
		const char *		mpszArg1,
		...);							 
										 
										 
										 
										 
										 
  int
	web_reg_find(
		const char *		mpszArg1,
		...);							 
										 
										 
										 
										 
										 
										 
										 
				
  int
	web_reg_save_param(
		const char *		mpszParamName,
		...);							 
										 
										 
										 
										 
										 
										 

  int
	web_convert_param(
		const char * 		mpszParamName, 
										 
		...);							 
										 
										 


										 

										 
  int
	web_remove_auto_filter(
		const char *		mpszArg,
		...
	);									 
										 
				
  int
	web_remove_auto_header(
		const char *		mpszHeaderName,
		...);							 
										 



  int
	web_remove_cookie(
		const char *		mpszCookie);

  int
	web_save_header(
		const char *		mpszType,	 
		const char *		mpszName);	 
  int
	web_set_certificate(
		const char *		mpszIndex);
  int
	web_set_certificate_ex(
		const char *		mpszArg1,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_set_connections_limit(
		const char *		mpszLimit);
  int
	web_set_max_html_param_len(
		const char *		mpszLen);
  int
	web_set_max_retries(
		const char *		mpszMaxRetries);
  int
	web_set_proxy(
		const char *		mpszProxyHost);
  int
	web_set_pac(
		const char *		mpszPacUrl);
  int
	web_set_proxy_bypass(
		const char *		mpszBypass);
  int
	web_set_secure_proxy(
		const char *		mpszProxyHost);
  int
	web_set_sockets_option(
		const char *		mpszOptionID,
		const char *		mpszOptionValue
	);
  int
	web_set_option(
		const char *		mpszOptionID,
		const char *		mpszOptionValue,
		...								 
	);
  int
	web_set_timeout(
		const char *		mpszWhat,
		const char *		mpszTimeout);
  int
	web_set_user(
		const char *		mpszUserName,
		const char *		mpszPwd,
		const char *		mpszHost);

  int
	web_sjis_to_euc_param(
		const char *		mpszParamName,
										 
		const char *		mpszParamValSjis);
										 

  int
	web_submit_data(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	spdy_submit_data(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_submit_form(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										  
										 
										 
										 
										 
										 
										  
										 
										 
										 
										 
										 
										 
										 
										  
										 
										 
										 
										 
										 
										  
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_url(
		const char *		mpszUrlName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	spdy_url(
		const char *		mpszUrlName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int 
	web_set_proxy_bypass_local(
		const char * mpszNoLocal
		);

  int 
	web_cache_cleanup(void);

  int
	web_create_html_query(
		const char* mpszStartQuery,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int 
	web_create_radio_button_param(
		const char *NameFiled,
		const char *NameAndVal,
		const char *ParamName
		);

  int
	web_convert_from_formatted(
		const char * mpszArg1,
		...);							 
										 
										 
										 
										 
										 
										
  int
	web_convert_to_formatted(
		const char * mpszArg1,
		...);							 
										 
										 
										 
										 
										 

  int
	web_reg_save_param_ex(
		const char * mpszParamName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_reg_save_param_xpath(
		const char * mpszParamName,
		...);							
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_reg_save_param_json(
		const char * mpszParamName,
		...);							
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_reg_save_param_regexp(
		 const char * mpszParamName,
		 ...);							
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_reg_save_param_attrib(
		const char * mpszParamName,
		...);
										 
										 
										 
										 
										 
										 
										 		
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_js_run(
		const char * mpszCode,
		...);							
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_js_reset(void);

  int
	web_convert_date_param(
		const char * 		mpszParamName,
		...);










# 789 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/as_web.h"


# 802 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/as_web.h"



























# 840 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/as_web.h"

 
 
 


  int
	FormSubmit(
		const char *		mpszFormName,
		...);
  int
	InitWebVuser(void);
  int
	SetUser(
		const char *		mpszUserName,
		const char *		mpszPwd,
		const char *		mpszHost);
  int
	TerminateWebVuser(void);
  int
	URL(
		const char *		mpszUrlName);
























# 908 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/as_web.h"


  int
	web_rest(
		const char *		mpszReqestName,
		...);							 
										 
										 
										 
										 

  int
web_stream_open(
	const char *		mpszArg1,
	...
);
  int
	web_stream_wait(
		const char *		mpszArg1,
		...
	);

  int
	web_stream_close(
		const char *		mpszArg1,
		...
	);

  int
web_stream_play(
	const char *		mpszArg1,
	...
	);

  int
web_stream_pause(
	const char *		mpszArg1,
	...
	);

  int
web_stream_seek(
	const char *		mpszArg1,
	...
	);

  int
web_stream_get_param_int(
	const char*			mpszStreamID,
	const int			miStateType
	);

  double
web_stream_get_param_double(
	const char*			mpszStreamID,
	const int			miStateType
	);

  int
web_stream_get_param_string(
	const char*			mpszStreamID,
	const int			miStateType,
	const char*			mpszParameterName
	);

  int
web_stream_set_param_int(
	const char*			mpszStreamID,
	const int			miStateType,
	const int			miStateValue
	);

  int
web_stream_set_param_double(
	const char*			mpszStreamID,
	const int			miStateType,
	const double		mdfStateValue
	);

  int
web_stream_set_custom_mpd(
	const char*			mpszStreamID,
	const char*			aMpdBuf
	);

 
 
 






# 9 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/web_api.h" 2

















 







 















  int
	web_reg_add_cookie(
		const char *		mpszCookie,
		...);							 
										 

  int
	web_report_data_point(
		const char *		mpszEventType,
		const char *		mpszEventName,
		const char *		mpszDataPointName,
		const char *		mpszLAST);	 
										 
										 
										 

  int
	web_text_link(
		const char *		mpszStepName,
		...);

  int
	web_element(
		const char *		mpszStepName,
		...);

  int
	web_image_link(
		const char *		mpszStepName,
		...);

  int
	web_static_image(
		const char *		mpszStepName,
		...);

  int
	web_image_submit(
		const char *		mpszStepName,
		...);

  int
	web_button(
		const char *		mpszStepName,
		...);

  int
	web_edit_field(
		const char *		mpszStepName,
		...);

  int
	web_radio_group(
		const char *		mpszStepName,
		...);

  int
	web_check_box(
		const char *		mpszStepName,
		...);

  int
	web_list(
		const char *		mpszStepName,
		...);

  int
	web_text_area(
		const char *		mpszStepName,
		...);

  int
	web_map_area(
		const char *		mpszStepName,
		...);

  int
	web_eval_java_script(
		const char *		mpszStepName,
		...);

  int
	web_reg_dialog(
		const char *		mpszArg1,
		...);

  int
	web_reg_cross_step_download(
		const char *		mpszArg1,
		...);

  int
	web_browser(
		const char *		mpszStepName,
		...);

  int
	web_control(
		const char *		mpszStepName,
		...);

  int
	web_set_rts_key(
		const char *		mpszArg1,
		...);

  int
	web_save_param_length(
		const char * 		mpszParamName,
		...);

  int
	web_save_timestamp_param(
		const char * 		mpszParamName,
		...);

  int
	web_load_cache(
		const char *		mpszStepName,
		...);							 
										 

  int
	web_dump_cache(
		const char *		mpszStepName,
		...);							 
										 
										 

  int
	web_reg_find_in_log(
		const char *		mpszArg1,
		...);							 
										 
										 

  int
	web_get_sockets_info(
		const char *		mpszArg1,
		...);							 
										 
										 
										 
										 

  int
	web_add_cookie_ex(
		const char *		mpszArg1,
		...);							 
										 
										 
										 

  int
	web_hook_java_script(
		const char *		mpszArg1,
		...);							 
										 
										 
										 

 
 
 
 
 
 
 
 
 
 
 
 
  int
	web_reg_async_attributes(
		const char *		mpszArg,
		...
	);

 
 
 
 
 
 
  int
	web_sync(
		 const char *		mpszArg1,
		 ...
	);

 
 
 
 
  int
	web_stop_async(
		const char *		mpszArg1,
		...
	);

 
 
 
 
 

 
 
 

typedef enum WEB_ASYNC_CB_RC_ENUM_T
{
	WEB_ASYNC_CB_RC_OK,				 

	WEB_ASYNC_CB_RC_ABORT_ASYNC_NOT_ERROR,
	WEB_ASYNC_CB_RC_ABORT_ASYNC_ERROR,
										 
										 
										 
										 
	WEB_ASYNC_CB_RC_ENUM_COUNT
} WEB_ASYNC_CB_RC_ENUM;

 
 
 

typedef enum WEB_CONVERS_CB_CALL_REASON_ENUM_T
{
	WEB_CONVERS_CB_CALL_REASON_BUFFER_RECEIVED,
	WEB_CONVERS_CB_CALL_REASON_END_OF_TASK,

	WEB_CONVERS_CB_CALL_REASON_ENUM_COUNT
} WEB_CONVERS_CB_CALL_REASON_ENUM;

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

typedef
int														 
	(*RequestCB_t)();

typedef
int														 
	(*ResponseBodyBufferCB_t)(
		  const char *		aLastBufferStr,
		  int				aLastBufferLen,
		  const char *		aAccumulatedStr,
		  int				aAccumulatedLen,
		  int				aHttpStatusCode);

typedef
int														 
	(*ResponseCB_t)(
		  const char *		aResponseHeadersStr,
		  int				aResponseHeadersLen,
		  const char *		aResponseBodyStr,
		  int				aResponseBodyLen,
		  int				aHttpStatusCode);

typedef
int														 
	(*ResponseHeadersCB_t)(
		  int				aHttpStatusCode,
		  const char *		aAccumulatedHeadersStr,
		  int				aAccumulatedHeadersLen);



 
 
 

typedef enum WEB_CONVERS_UTIL_RC_ENUM_T
{
	WEB_CONVERS_UTIL_RC_OK,
	WEB_CONVERS_UTIL_RC_CONVERS_NOT_FOUND,
	WEB_CONVERS_UTIL_RC_TASK_NOT_FOUND,
	WEB_CONVERS_UTIL_RC_INFO_NOT_FOUND,
	WEB_CONVERS_UTIL_RC_INFO_UNAVIALABLE,
	WEB_CONVERS_UTIL_RC_INVALID_ARGUMENT,

	WEB_CONVERS_UTIL_RC_ENUM_COUNT
} WEB_CONVERS_UTIL_RC_ENUM;

 
 
 

  int					 
	web_util_set_request_url(
		  const char *		aUrlStr);

  int					 
	web_util_set_request_body(
		  const char *		aRequestBodyStr);

  int					 
	web_util_set_formatted_request_body(
		  const char *		aRequestBodyStr);


 
 
 
 
 

 
 
 
 
 

 
 
 
 
 
 
 
 

 
 
  int
web_websocket_connect(
		 const char *	mpszArg1,
		 ...
		 );


 
 
 
 
 																						
  int
web_websocket_send(
	   const char *		mpszArg1,
		...
	   );

 
 
 
 
 
 
  int
web_websocket_close(
		const char *	mpszArg1,
		...
		);

 
typedef
void														
(*OnOpen_t)(
			  const char* connectionID,  
			  const char * responseHeader,  
			  int length  
);

typedef
void														
(*OnMessage_t)(
	  const char* connectionID,  
	  int isbinary,  
	  const char * data,  
	  int length  
	);

typedef
void														
(*OnError_t)(
	  const char* connectionID,  
	  const char * message,  
	  int length  
	);

typedef
void														
(*OnClose_t)(
	  const char* connectionID,  
	  int isClosedByClient,  
	  int code,  
	  const char* reason,  
	  int length  
	 );
 
 
 
 
 





# 7 "globals.h" 2

# 1 "lrw_custom_body.h" 1
 




# 8 "globals.h" 2


 
 
int LINK_TT, FORM_TT, TYPE_SPEED;  
	 
	LINK_TT = 10;
	FORM_TT = 10;
	TYPE_SPEED = 1;
int authcookielen , authtokenlen ;
authcookielen = 0;	
int orderItemIdscount = 0;
 



 
# 73 "globals.h"

# 3 "e:\\performance\\scripts\\2017_r5\\03_perf\\ecommerce\\us_checkout\\\\combined_US_Checkout.c" 2

# 1 "vuser_init.c" 1
# 1 "..\\..\\browseFunctions.c" 1
# 1 "..\\..\\browseWorkloadModel.c" 1
int NAV_BROWSE, NAV_SEARCH, NAV_CLEARANCE, NAV_PLACE;  
int DRILL_ONE_FACET, DRILL_TWO_FACET, DRILL_THREE_FACET, DRILL_SUB_CATEGORY;  
int PDP, QUICKVIEW, PRODUCT_QUICKVIEW_SERVICE, RESERVE_ONLINE, MIXED_CART_RATIO, ECOMM_CART_RATIO, NO_BROWSE_RATIO_HOME, NO_BROWSE_RATIO_CATEGORY, NO_BROWSE_RATIO_SUBCATEGORY, NO_BROWSE_RATIO_PDP;  
int APPLY_SORT, RATIO_TCPSkuSelectionView; 	PDP_to_WL;   
int APPLY_PAGINATE, RATIO_STORE_LOCATOR, RATIO_SEARCH_SUGGEST, API_SUB_TRANSACTION_SWITCH, ESPOT_FLAG, GETPOINTSSERVICE_FLAG, CACHE_PRIME_MODE;  

	CACHE_PRIME_MODE = 0;  

	 

	NAV_BROWSE = 88;   
	NAV_SEARCH = 12;  
	NAV_CLEARANCE = 0;
	NAV_PLACE = 0;

	 
	DRILL_ONE_FACET = 11;  
	DRILL_TWO_FACET = 3;  
	DRILL_THREE_FACET = 1;  
	DRILL_SUB_CATEGORY = 85;  
 





	 
	APPLY_SORT = 25;

	 
	APPLY_PAGINATE = 25;  

	 
	 
 
 
	PDP = 100;
	QUICKVIEW = 0;  

	RATIO_STORE_LOCATOR  = 5;
	RATIO_SEARCH_SUGGEST = 0;  
	RATIO_TCPSkuSelectionView = 20;

	PRODUCT_QUICKVIEW_SERVICE = 15;  
	RESERVE_ONLINE = 100;  
	ECOMM_CART_RATIO = 100;  
	BOPIS_CART_RATIO = 0;  


	API_SUB_TRANSACTION_SWITCH = 1;  
	BROWSE_OPTIONS_FLAG = 0;  
	ESPOT_FLAG = 0;
	MULTI_USE_COUPON_FLAG = 0;
	GETPOINTSSERVICE_FLAG = 0;
	PDP_to_WL = 20;  
   






	NO_BROWSE_RATIO = 80;  
	NO_BROWSE_RATIO_HOME		= 25;
	NO_BROWSE_RATIO_CATEGORY	= 25;
	NO_BROWSE_RATIO_SUBCATEGORY	= 25;
	NO_BROWSE_RATIO_PDP			= 30;

# 1 "..\\..\\browseFunctions.c" 2


int index , length , i, randomPercent, isLoggedIn;
char *searchString ;
char *nav_by ;  
char *drill_by ;  
char *sort_by;  
char *product_by;  
 
typedef long time_t;
time_t t;
 
 

void addHeader()
{
	web_add_header("storeId", lr_eval_string("{storeId}") );
	web_add_header("catalogId", lr_eval_string("{catalogId}") );
	web_add_header("langId", "-1");

}

void webURL(char *Url, char *pageName)
{
	lr_save_string(Url, "Url");
	lr_save_string(pageName, "pageName");
	web_url ( lr_eval_string("{pageName}") ,
    	     "URL={Url}" ,
             "Resource=0" ,
             "Mode=HTML" ,
             "LAST" ) ;
} 

void api_getESpot_second(){
	if (ESPOT_FLAG == 1) {
		if (isLoggedIn == 1) {  
			addHeader();
			web_add_header("espotName","GlobalHeaderBannerAboveHeader,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1");
			web_reg_find("TEXT/IC=espotName", "SaveCount=apiCheck", "LAST");
			 
			web_custom_request("getESpots",
				"URL=https://{api_host}/getESpot",
				"Method=GET",
				"Resource=0",
				"RecContentType=application/json",
				"LAST");
			 
			 
			 
			 
		}
	}
}

void TCPGetWishListForUsersView()
{
	lr_start_transaction("T25_Common_S01_TCPGetWishListForUsersView");

	web_custom_request("TCPGetWishListForUsersView",
		"URL=http://{host}/webapp/wcs/stores/servlet/TCPGetWishListForUsersView?tcpCallBack=jQuery1113014590106982485151_1473881708524&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&sortBy=&_=1473881708525",
		"Method=GET",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		"EncType=application/x-www-form-urlencoded",
		"LAST");

	lr_end_sub_transaction("T25_Common_S01_TCPGetWishListForUsersView", 2);

}


int tcp_api(char *apiCall, char *method)
{	int rc;
	lr_param_sprintf ( "apiTransactionName" , "T30_API %s" , apiCall ) ;
	lr_param_sprintf ( "apiURL" , "https://%s/%s" , lr_eval_string("{api_host}"), apiCall ) ;
	lr_param_sprintf ( "apiMethod" , "%s" , method ) ;
	web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", "LAST");  
	web_reg_save_param("errorMessage", "LB=\"errorMessage\": \"", "RB=\",\n", "NotFound=Warning", "LAST");  

	lr_start_transaction(lr_eval_string("{apiTransactionName}"));

	web_custom_request(lr_eval_string(apiCall),
		"URL={apiURL}",
		"Method={apiMethod}",
		"RecContentType=text/html",
		"Mode=HTML",
		"LAST");
	 

 
		if ( strcmp( lr_eval_string("{errorCode}") ,"") != 0 && strcmp(lr_eval_string("{errorMessage}") ,"") != 0 ) {
			lr_fail_trans_with_error( lr_eval_string("{apiTransactionName} Failed with  Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_transaction( lr_eval_string("{apiTransactionName}"), 1);
		}
		else
			lr_end_transaction( lr_eval_string("{apiTransactionName}"), 0);
	 
	 
		 
		 
	 
	return 0;
}

void callAPI()
{
	 

	if ( isLoggedIn == 1	) {
		tcp_api2("tcpstore/getListofDefaultWishlist", "GET", lr_eval_string("{mainTransaction}") );
	}


	if (strcmp(lr_eval_string("{callerId}"), "home") != 0)
	{
		web_add_header("locStore", "False");
		web_add_header("pageName", "orderSummary");
 
		tcp_api2("getOrderDetails", "GET", lr_eval_string("{mainTransaction}") );                       

	}
	else{

		web_add_header("locStore", "False");
		web_add_header("pageName", "orderSummary");
		tcp_api2("getOrderDetails", "GET", lr_eval_string("{mainTransaction}") );                       
		lr_save_string("0,","cartCount");

 

	}

	web_reg_save_param ( "userId" , "LB=\"userId\": \"" , "RB=\",\n" , "NotFound=Warning", "LAST" ) ;  
	tcp_api2("payment/getRegisteredUserDetailsInfo", "GET",  lr_eval_string("{mainTransaction}") );  

	if (ESPOT_FLAG == 1)
	{
		tcp_api2("getESpot", "GET", lr_eval_string("{mainTransaction}") );                              
	}

 
	web_add_header("partNumber", "2089039_1361-2083215_10-2089763_01-2083216_10-2088607_10-2083213_01");
	tcp_api2("tcpproduct/getPriceDetails", "GET",  lr_eval_string("{mainTransaction}") );


	if (strcmp(lr_eval_string("{callerId}"), "productDisplay") == 0)
	{
		web_reg_save_param("atc_catentryIds", "LB=catentryId\": \"", "RB=\",", "ORD=All", "NotFound=Warning", "LAST");  
		web_reg_save_param("atc_quantity"  ,  "LB=quantity\": \"", "RB=.0\",", "ORD=All", "NotFound=Warning", "LAST");  
		web_add_header( "productId", "{pdpProdID}" );  
 
		tcp_api2("tcpproduct/getSKUInventoryandProductCounterDetails", "GET",  lr_eval_string("{mainTransaction}") );
	}
	 
}

void call_OPTIONS(char *apiCall)
{
	lr_param_sprintf ( "apiURL" , "https://%s/%s" , lr_eval_string("{api_host}"), apiCall ) ;
	web_add_header("Accept", "*/*");
	web_add_header("Access-Control-Request-Method", "GET");
	web_add_header("Origin", "https://{host}");

	if (strcmp(apiCall, "payment/getPointsService") == 0 ){

		web_add_header("Access-Control-Request-Headers", "catalogid,langid,storeid");

	}else if (strcmp(apiCall, "getOrderDetails") == 0 ){

		web_add_header("Access-Control-Request-Headers", "calc,catalogid,langid,locstore,pagename,storeid");

	}else if (strcmp(apiCall, "getESpot") == 0 ){

		web_add_header("Access-Control-Request-Headers", "catalogid,devicetype,espotname,langid,storeid");

	}else if (strcmp(apiCall, "tcporder/getAllCoupons") == 0 ){

		web_add_header("Access-Control-Request-Headers", "catalogid,langid,storeid");

	}else if (strcmp(apiCall, "payment/getRegisteredUserDetailsInfo") == 0 ){

		web_add_header("Access-Control-Request-Headers", "catalogid,langid,storeid");

	}else if (strcmp(apiCall, "payment/giftOptionsCmd") == 0 ){

		web_add_header("Access-Control-Request-Headers", "catalogid,langid,storeid");

	}else if (strcmp(apiCall, "payment/getCreditCardDetails") == 0 ){

		web_add_header("Access-Control-Request-Headers", "catalogid,isrest,langid,storeid");

	}else if (strcmp(apiCall, "payment/getAddressFromBook") == 0 ){

		web_add_header("Access-Control-Request-Headers", "catalogid,frompage,langid,storeid");

	}
 
# 249 "..\\..\\browseFunctions.c"
}

void executeOptions()
{ 






}

int tcp_api2(char *apiCall, char *method, char *mainTransaction)
{	int rc;

	lr_save_string("", "body");

	if (strcmp(apiCall, "tcpstore/getListofDefaultWishlist") == 0 )
	{
		lr_save_string("{","CheckString");
		lr_save_string("{\"productId\":[\"624004\"]}", "body");
	}

	if (strcmp(apiCall, "getESpot") != 0 )
	{
		web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", "LAST");  
		web_reg_save_param("errorMessage", "LB=\"errorMessage\": \"", "RB=\",\n", "NotFound=Warning", "LAST");  
	}

	if (strcmp(apiCall, "payment/getRegisteredUserDetailsInfo") == 0 )
	{
		web_reg_save_param ( "addressId" , "LB=\"addressId\": \"" , "RB=\",\n" , "ORD=1", "NotFound=Warning", "LAST" ) ;  
		web_reg_save_param ( "addressIdAll" , "LB=\"addressId\": \"" , "RB=\",\n" , "ORD=ALL", "NotFound=Warning", "LAST" ) ;  
		lr_save_string("resourceName","CheckString");
	}
	if (strcmp(apiCall, "payment/getPointsService") == 0 ){
		lr_save_string("LoyaltyWebsiteInd","CheckString");
	}else if (strcmp(apiCall, "getOrderDetails") == 0 ){
		lr_save_string("{","CheckString");
	}else if (strcmp(apiCall, "getESpot") == 0 ){
		lr_save_string("espotName","CheckString");
		 
		                           
		web_add_header("espotName","GlobalHeaderBannerAboveHeader,GlobalHeaderBannerWithinHeader,GlobalHeaderLogo,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,create_account_drawer,MinicartLoginBanner,MinicartRegisterBanner,drawer_myaccount_overview_banner,coupon_help,bag_empty_banner,drawer_empty_bag,MinicartLoginFromFavoritesBanner,bag_ledger_promo_banner,gift_services_espot,CheckoutConfirmationBanner,CheckoutConfirmationMPRPromo,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1");
		 
		web_add_header("deviceType","desktop");

	}else if (strcmp(apiCall, "tcporder/getAllCoupons") == 0 ){
		lr_save_string("availableCoupons","CheckString");
	}else if (strcmp(apiCall, "tcpproduct/getPriceDetails") == 0 ){
		lr_save_string("{","CheckString");
	}else if (strcmp(apiCall, "tcporder/getXAppConfigValues") == 0 ){
		lr_save_string("xAppAttrValues","CheckString");
	}else if (strcmp(apiCall, "tcpproduct/getProductviewbyCategory") == 0 ){
		lr_save_string("recordSetTotalMatches","CheckString");
	}else if (strcmp(apiCall, "tcpproduct/getSKUInventoryandProductCounterDetails") == 0 ){
		lr_save_string("getAllSKUInventoryByProductId","CheckString");
	}else if (strcmp(apiCall, "tcporder/getFavouriteStoreLocation") == 0 ){
		lr_save_string("{","CheckString");
	}else if (strcmp(apiCall, "tcpproduct/getWishListbyId") == 0 ){
		lr_save_string("accessSpecifier","CheckString");
	}else if (strcmp(apiCall, "tcpstore/getListofWishList") == 0 ){
		lr_save_string("giftListExternalIdentifier","CheckString");
	}else if (strcmp(apiCall, "tcpproduct/getTopCategories") == 0 ){
		lr_save_string("{","CheckString");
	}


	if (API_SUB_TRANSACTION_SWITCH == 1)  
	{
		lr_param_sprintf ( "apiMainTransactionName" , "%s" , mainTransaction ) ;

		if (strcmp(lr_eval_string("{apiMainTransactionName}"),"") !=0 ) {
			lr_param_sprintf ( "apiTransactionName" , "%s_%s" , lr_eval_string("{apiMainTransactionName}"), apiCall ) ;
			lr_start_sub_transaction(lr_eval_string("{apiTransactionName}"), lr_eval_string("{apiMainTransactionName}"));
		} else {
			lr_param_sprintf ( "apiTransactionName" , "%s_%s" , lr_eval_string("{noBrowseTransaction}"), apiCall ) ;
			lr_start_transaction(lr_eval_string("{apiTransactionName}"));
		}
 
# 337 "..\\..\\browseFunctions.c"
				# 347 "..\\..\\browseFunctions.c"


		lr_param_sprintf ( "apiURL" , "https://%s/%s" , lr_eval_string("{api_host}"), apiCall ) ;
		lr_param_sprintf ( "apiMethod" , "%s" , method ) ;

		web_add_header("storeId", lr_eval_string("{storeId}") );
		web_add_header("catalogId", lr_eval_string("{catalogId}") );
		web_add_header("langId", "-1");
		web_reg_find("TEXT/IC={CheckString}","Fail=NotFound", "SaveCount=Mycount","LAST");

		web_custom_request(lr_eval_string(apiCall),
			"URL={apiURL}",
			"Method={apiMethod}",
			"RecContentType=text/html",
			"Mode=HTML",
 
			"LAST");

 
		if ( atoi(lr_eval_string("{Mycount}")) ==0 )
		{
			lr_fail_trans_with_error( lr_eval_string("{apiTransactionName} Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			if (strcmp(lr_eval_string("{apiMainTransactionName}"),"") !=0 )
				lr_end_sub_transaction( lr_eval_string("{apiTransactionName}"), 1);
			else
				lr_end_transaction( lr_eval_string("T30_API {apiTransactionName}"), 1);
		}
		else
		{
			if (atoi(lr_eval_string("{Mycount}")) > 0 )  {
				if (strcmp(lr_eval_string("{apiMainTransactionName}"),"") !=0 )
					lr_end_sub_transaction( lr_eval_string("{apiTransactionName}"), 2);
				else
					lr_end_transaction( lr_eval_string("T30_API {apiTransactionName}"), 2);
			}
			else{
				if (strcmp(lr_eval_string("{apiMainTransactionName}"),"") !=0 )
					lr_end_sub_transaction( lr_eval_string("{apiTransactionName}"), 1);
				else
					lr_end_transaction( lr_eval_string("T30_API {apiTransactionName}"), 1);
			}
		}

	}
	else
	{
		lr_param_sprintf ( "apiTransactionName" , "%s_%s" , lr_eval_string("{mainTransaction}"), apiCall ) ;
		lr_start_transaction(lr_eval_string("{apiTransactionName}"));
		lr_param_sprintf ( "apiURL" , "https://%s/%s" , lr_eval_string("{api_host}"), apiCall ) ;
		lr_param_sprintf ( "apiMethod" , "%s" , method ) ;

		web_add_header("storeId", lr_eval_string("{storeId}") );
		web_add_header("catalogId", lr_eval_string("{catalogId}") );
		web_add_header("langId", "-1");
		web_reg_find("TEXT/IC={CheckString}","Fail=NotFound", "SaveCount=Mycount","LAST");

		web_custom_request(lr_eval_string(apiCall),
			"URL={apiURL}",
			"Method={apiMethod}",
			"RecContentType=text/html",
			"Mode=HTML",
 
			"LAST");

		if ( atoi(lr_eval_string("{Mycount}")) ==0 )
		{
			lr_fail_trans_with_error( lr_eval_string("{apiTransactionName} Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_transaction( lr_eval_string("{apiTransactionName}"), 1);
		}
		else
		{
			if (atoi(lr_eval_string("{Mycount}")) > 0 )  {
				lr_end_transaction( lr_eval_string("{apiTransactionName}"), 2);
			}
			else {
				lr_end_transaction( lr_eval_string("{apiTransactionName}"), 1);
			}
		}
 
# 444 "..\\..\\browseFunctions.c"
	}
	return 0;
}

webURL2(char *Url, char *pageName, char *textCheck)
{
	lr_save_string(Url, "Url");
	lr_save_string(pageName, "pageName");
	lr_save_string(textCheck, "textCheck");
	web_reg_find("SaveCount=txtCheckCount", "Text/IC={textCheck}", "LAST");

	web_reg_find("SaveCount=noProducts", "Text/IC=We're sorry, there are no products available for purchase at this time. Please check back later.", "LAST");

	web_url ( lr_eval_string("{pageName}") ,
    	     "URL={Url}" ,
             "Resource=0" ,
             "Mode=HTML" ,
             "LAST" ) ;

	if ( atoi(lr_eval_string("{txtCheckCount}")) > 0 )
		return 0;
	else
		return 1;

} 


void endIteration()
{
	 
}

void homePageCorrelations()
{
	int categoryIndex = 0;

	web_reg_save_param ( "categories" ,
 
				 "LB=<a href=\"{store_home_page}c/" ,   
				 "RB=\" class=\"navigation-level-one-link" ,
				 "ORD=ALL" , "Convert=HTML_TO_TEXT" , "NotFound=Warning", "LAST" ) ;


	if ( strcmp(lr_eval_string("{storeId}"), "10152") == 0 ) {
		index = rand () % 6 + 1 ;
	} else {
		index = rand () % 7 + 1 ;
	}

	switch(index)
	{
		case 1: web_reg_save_param_regexp ("ParamName=subcategory",	"RegExp=navigationTree.*?\"Girl\"(.*?)Toddler Girl\"",  "NotFound=Warning", "SEARCH_FILTERS", "RequestUrl=*", "LAST" ); break;
		case 2: web_reg_save_param_regexp ("ParamName=subcategory", "RegExp=navigationTree.*?\"Boy\"(.*?)\"Toddler Boy\"",  "NotFound=Warning", "SEARCH_FILTERS", "RequestUrl=*", "LAST" ); break;
		case 3: web_reg_save_param_regexp ("ParamName=subcategory", "RegExp=navigationTree.*?\"Toddler Boy\"(.*?)\"Baby\"", "NotFound=Warning", "SEARCH_FILTERS", "RequestUrl=*", "LAST" ); break;
		case 4: web_reg_save_param_regexp ("ParamName=subcategory",	"RegExp=navigationTree.*?\"Baby\"(.*?)\"Shoes\"",       "NotFound=Warning", "SEARCH_FILTERS", "RequestUrl=*", "LAST" ); break;
		case 5: web_reg_save_param_regexp ("ParamName=subcategory",	"RegExp=navigationTree.*?\"Baby\"(.*?)\"Shoes\"",	    "NotFound=Warning", "SEARCH_FILTERS", "RequestUrl=*", "LAST" ); break;
		case 6: web_reg_save_param_regexp ("ParamName=subcategory", "RegExp=navigationTree.*?\"Shoes\"(.*?)\"Accessories\"","NotFound=Warning", "SEARCH_FILTERS", "RequestUrl=*",	"LAST" ); break;
		case 7: web_reg_save_param_regexp ("ParamName=subcategory", "RegExp=navigationTree.*?\"Accessories\"(.*?)}]]}]",    "NotFound=Warning", "SEARCH_FILTERS", "RequestUrl=*",	"LAST" ); break;
		default: break;
	}

}

void homePageCorrelations_OLD()  
{
 






 







	web_reg_save_param ( "categories" ,
				 "LB=\t\t\t\t<a href=\"http://{host}{store_home_page}c/" ,
				 "RB=\"" ,
				 "ORD=ALL" , "Convert=HTML_TO_TEXT" , "NotFound=Warning", "LAST" ) ;

 






	 





 








 





	 

 
 
# 580 "..\\..\\browseFunctions.c"
} 

 
# 595 "..\\..\\browseFunctions.c"

void categoryPageCorrelationsR5()  
{

	web_reg_save_param_regexp( "ParamName=subcategories" ,
								"RegExp=class=\"navigation-level-one\"[^<]*?<a href=\"(.*?)\"" ,
							    
	                            
			                   "Ordinal=ALL" ,
			                   "Notfound=warning" ,
			                   "SEARCH_FILTERS" ,
							   "IgnoreRedirections=Yes",
			                    
							   "RequestUrl=*{host}*",
							  "LAST" ) ;


	web_reg_save_param_regexp( "ParamName=facetsID" ,
	                           "RegExp=facetCheckBox\" id =\"(.*?)\" value=\"(.*?)\"" ,
			                   "Ordinal=ALL" ,
			                   "Notfound=warning" ,
			                   "SEARCH_FILTERS" ,
			                   "Group=2" ,
			                   "LAST" ) ;

	web_reg_save_param ( "sortURL" ,
				  		 "LB=<option value=\"http://{host}" ,
	                   	 "RB=\"" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 "LAST" ) ;

	web_reg_save_param ( "searchSubCategories" ,
				  		 "LB=href='http://{host}" ,
	                   	 "RB='>" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 "LAST" ) ;

	web_reg_save_param ( "paginationNextURL" ,
				  		 "LB=goToResultPage('http://{host}" ,
	                   	 "RB='" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 "LAST" ) ;

	web_reg_save_param ( "paginationPageIndex" ,
				  		 "LB=catalogSearchResultDisplay_Context','" ,
	                   	 "RB='" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 "LAST" ) ;

	web_reg_save_param ( "paginationShowAllURL" ,
				  		 "LB=viewall-right-bottom\">\r\n\t\t\t\t\t                <a href=\"http://{host}" ,
	                   	 "RB=\"" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 "LAST" ) ;

} 

 
# 697 "..\\..\\browseFunctions.c"

void productDisplayPageCorrelations()
{

	web_reg_save_param ( "skuSelectionSwitchProductID" ,
	                     "LB=confirmSwitchProduct(event,'product_" ,
	                     "RB='" ,
	                     "notFound=Warning",
	                     "ORD=ALL" ,
	                     "LAST" ) ;

	web_reg_save_param ( "skuSelectionProductID" ,
	                     "LB=input type=\"hidden\" id=\"prodId\" value=\"" ,
	                     "RB=\"" ,
	                     "notFound=Warning",
	                     "LAST" ) ;

	 
	web_reg_save_param("productId", "LB=\"productId\" : \"", "RB=\",", "ORD=ALL","NotFound=Warning", "LAST");  
	web_reg_save_param("TCPProductInd", "LB=\"TCPProductInd\" : \"", "RB=\"", "ORD=ALL", "NotFound=Warning", "LAST");  
	web_reg_save_param("bopisCatEntryId", "LB=\"catentry_id\" : \"", "RB=\"", "ORD=ALL", "NotFound=Warning", "LAST");  
	web_reg_save_param("bopisQty", "LB=\"qty\" : \"", "RB=\"", "ORD=ALL", "NotFound=Warning", "LAST");  

}  


 
void addToCartCorrelations()
{

	web_reg_save_param_regexp( "ParamName=atc_catentryIds" ,
	                           "RegExp=\"catentry_id\" : \"(.*?)\",\r\n\t\t\t\t\t\t\t\t\t\t\t\"item_sku\" : \"(.*?)\",\r\n\t\t\t\t\t\t\t\t\t\t\t\"qty\" : \"([1-9][0-9]*)\"" ,
			                   "Ordinal=ALL" ,
			                   "Notfound=warning" ,
			                   "SEARCH_FILTERS" ,
			                   "Group=1" ,
			                   "LAST" ) ;

 	web_reg_save_param ( "atc_comment" ,
                    	 "LB=<input type=\"hidden\" name=\"comment\" value=\"" ,
                    	 "RB=\"" ,
                    	 "Notfound=warning" ,
                    	 "LAST" ) ;

}  
 
# 864 "..\\..\\browseFunctions.c"

void viewHomePage()
{
	lr_save_string("home", "callerId");

	homePageCorrelations();

	web_global_verification("Text/IC=h1 role=\"main\">The store has encountered a problem processing the last request",  "Fail=Found","ID=FindStoreProblem", "LAST" );


	lr_param_sprintf ( "homePageUrl" , "http://%s%shome" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}") ) ;

	lr_save_string("T01_Home Page", "mainTransaction");
	if( atoi(lr_eval_string("{akamaiRatio}")) <= (100 - NO_BROWSE_RATIO_HOME) ) {

		lr_save_string("akamai", "callerId2");
		API_SUB_TRANSACTION_SWITCH = 0;
		callAPI();

	} else {
		lr_start_transaction( "T01_Home Page" ) ;

		if ( webURL2(lr_eval_string ( "{homePageUrl}" ), "T01_Home Page" , "Kids Clothes & Baby Clothes | The Children's Place" ) == 0) {
			callAPI();
			lr_end_transaction( "T01_Home Page" , 0 ) ;
		} else {
			lr_fail_trans_with_error( lr_eval_string("T01_Home Page Text Check Failed - {homePageUrl}") ) ;
			lr_end_transaction( "T01_Home Page" , 1 ) ;
		}

	}

	BROWSE_OPTIONS_FLAG = 1;

	lr_save_string("Other", "callerId");

}

int getTopCategories()
{
	web_reg_save_param("aaaaaa", "LB=url\": \"", "RB=\"", "ORD=All", "NotFound=Warning", "LAST");

 

	web_submit_data("TCPLocaleSelection",
	    "Action=http://{host}/shop/{store}/getTopCategories",
		"Method=POST",
		"Resource=0",
		"Mode=HTML",
		"ITEMDATA",
            "Name=storeId", "Value={storeId}", "ENDITEM",
            "Name=catalogId", "Value={catalogId}", "ENDITEM",
		"LAST");

 
 
 
 
	return 0;
}

void getProductviewbyCategory()
{
	addHeader();
	web_add_header( "categoryId", lr_eval_string("{category}") );
	web_add_header( "pageNumber", "1" );
	web_add_header( "pageSize", "18" );
	web_add_header( "searchSource", "E" );
	web_add_header( "searchType", "-1,-1,0" );

 
 

	web_custom_request("getProductviewbyCategory",
		"URL=https://{api_host}/tcpproduct/getProductviewbyCategory",
		"Method=GET",
		"Resource=0",
		"RecContentType=text/html",
		"EncType=application/x-www-form-urlencoded",
		"LAST");
}

void navByBrowse()
{
	int categoryIndex = 0;

	lr_think_time ( LINK_TT ) ;

	if ( strcmp(lr_eval_string("{storeId}"), "10152") == 0 )
	{
		index = rand () % 6 + 1 ;
		switch(index)
		{
			case 1: lr_save_string( "349517", "category"); break;
			case 2: lr_save_string( "349514", "category"); break;
			case 3: lr_save_string( "349515", "category"); break;
			case 4: lr_save_string( "349513", "category"); break;
			case 5: lr_save_string( "349518", "category"); break;
 
 
			default: break;
		}
	}
	else
	{
			index = rand () % 7 + 1 ;
			switch(index)
			{
				case 1: lr_save_string( "girls-clothing", "category"); break;
				case 2: lr_save_string( "toddler-girl-clothes", "category"); break;
				case 3: lr_save_string( "boys-clothing", "category"); break;
				case 4: lr_save_string( "toddler-boy-clothes", "category"); break;
				case 5: lr_save_string( "baby-clothes", "category"); break;
				case 6: lr_save_string( "childrens-shoes-kids-shoes", "category"); break;
				case 7: lr_save_string( "kids-accessories-us", "category"); break;
				default: break;
			}
	}

	lr_param_sprintf ( "cateogryPageUrl" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/") , lr_eval_string( "{category}" ) ) ;

 
 
 

 
	lr_param_sprintf ( "cateogryPageUrl" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/") , lr_eval_string( "{category}" ) ) ;

	web_convert_param("cateogryPageUrl", "SourceEncoding=HTML","TargetEncoding=PLAIN", "LAST" );
	web_reg_save_param( "subCategories", "LB=navigation-level-two-link\" href=\"{store_home_page}c/", "RB=\"", "ORD=All", "NotFound=Warning", "LAST");  


 




	lr_save_string("categoryDisplay", "callerId");
	lr_save_string("T02_Category Display", "mainTransaction");

	if( atoi(lr_eval_string("{akamaiRatio}")) <= (100 - NO_BROWSE_RATIO_CATEGORY) ) {

		lr_save_string("akamai", "callerId2");
		API_SUB_TRANSACTION_SWITCH = 0;
		callAPI();

	} else {
		lr_start_transaction( "T02_Category Display" ) ;
		if (webURL2(lr_eval_string ( "{cateogryPageUrl}" ), "T02_Category Display", "html " ) == 0 ) {
			callAPI();
			lr_end_transaction ( "T02_Category Display" , 0 ) ;
		}
		else if ((lr_paramarr_len("subcategories")) == 0)
		{
			lr_fail_trans_with_error( lr_eval_string("T02_Category Display Failed subcategories==0 for- {cateogryPageUrl}") ) ;
			lr_end_transaction ( "T02_Category Display" , 1 ) ;  
		} else {

			lr_fail_trans_with_error( lr_eval_string("T02_Category Display Text Check Failed for - {cateogryPageUrl}") ) ;
			lr_end_transaction ( "T02_Category Display" , 1 ) ;  
		}
	}

	lr_save_string("", "mainTransaction");
	 




}

void navByBrowseR3()
{
	int categoryCount = atoi( lr_eval_string("{categories_count}"))-1;
	lr_think_time ( LINK_TT ) ;

	 
	index = rand ( ) % categoryCount + 1 ;
	lr_save_string( lr_paramarr_idx ( "categories", index ), "category");

	lr_param_sprintf ( "cateogryPageUrl" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/") , lr_eval_string( "{category}" ) ) ;

	web_convert_param("cateogryPageUrl", "SourceEncoding=HTML","TargetEncoding=PLAIN", "LAST" );

	 

	lr_save_string("T02_Category Display", "mainTransaction");
	lr_start_transaction( "T02_Category Display" ) ;

 
	if (webURL2(lr_eval_string ( "{cateogryPageUrl}" ), "T02_Category Display", "html " ) == 0 )  
	{
		lr_save_string("categoryDisplay", "callerId");
		callAPI();

		lr_end_transaction ( "T02_Category Display" , 0 ) ;
	}
	else if ((lr_paramarr_len("subcategories")) == 0)
	{
			lr_fail_trans_with_error( lr_eval_string("T02_Category Display Failed subcategories==0 for- {cateogryPageUrl}") ) ;
			lr_end_transaction ( "T02_Category Display" , 1 ) ;  
	}
	else {
		lr_fail_trans_with_error( lr_eval_string("T02_Category Display Text Check Failed for - {cateogryPageUrl}") ) ;
		lr_end_transaction ( "T02_Category Display" , 1 ) ;  
	}

	 

}  


void navByClearance()
{
 	lr_think_time ( LINK_TT ) ;
	 
 
 
 
	 

	lr_param_sprintf ( "clearancePageUrl" , "%s" ,  lr_eval_string("{categories_3}") ) ;  
	lr_save_string("categoryDisplay", "callerId");

 
 

	web_reg_find("SaveCount=noProducts", "Text/IC=We're sorry, there are no products available for purchase at this time. Please check back later.", "LAST");
	web_reg_find("Text=FILTER BY", "SaveCount=FilterBy_Count", "LAST" );

	lr_save_string("T02_Clearance Display", "mainTransaction");
	lr_start_transaction ( "T02_Clearance Display" ) ;

	 
	if (webURL2(lr_eval_string ( "{clearancePageUrl}" ), "T02_Clearance Display", "<!-- BEGIN TCPSearchSetup.jspf-->" ) == 0 )
	{
		callAPI();

		if ( atoi(lr_eval_string("{noProducts}")) == 1 )
		{
			lr_param_sprintf ( "failedPageUrl" , "T02_Clearance Display Failure : %s", lr_eval_string("{clearancePageUrl}") ) ;
			lr_fail_trans_with_error( lr_eval_string("T02_Clearance Display Text Check Failed for - {failedPageUrl}") ) ;
			lr_end_transaction ( "T02_Clearance Display" , 1 ) ;

		}
		else
			lr_end_transaction ( "T02_Clearance Display" , 0 ) ;

	}
	else
	{
 

		lr_end_transaction ( "T02_Clearance Display" , 1 ) ;
	}
}  


findNewArrivals()  
{
	int offset = 1;

    char * position;

    char * str = "";

    char * search_str = "Arrivals";  

    while( offset >= 1 ) {

		str = lr_paramarr_random( "clearances" );
		 
	    position = (char *)strstr(str, search_str);

	    offset = (int)(position - str + 1);
    }

    lr_save_string(str, "clearance");

	return 0;
}

void typeAheadSearch()
{
	 

	 
    for (i = 0; i < length; i++)
    {
       	 
       	if (i == 0)
           	lr_param_sprintf ("SEARCH_STRING_PARAM", "%c", searchString[i]);
       	else
           	lr_param_sprintf ("SEARCH_STRING_PARAM", "%s%c", lr_eval_string("{SEARCH_STRING_PARAM}"), searchString[i]);
           	 

		if (i == 1) lr_save_string(lr_eval_string("{SEARCH_STRING_PARAM}"),"forDidYouMean");

 
		if (i == 2 || i == 5 || i == 8 || i == 11 || i == 14 || i == 17 || i == 20 )
       	{
    		lr_think_time ( TYPE_SPEED );

			web_reg_save_param_json(
				"ParamName=autoSelectOptionTerm",
				"QueryString=$.autosuggestions..termsArray..term",
				"SelectAll=Yes",
				"SEARCH_FILTERS",
				"NotFound=Warning",
			"LAST");
			web_reg_save_param_json(
				"ParamName=autoSelectOptionCategory",
				"QueryString=$.category..response..categoryUrl",
				"SelectAll=Yes",
				"SEARCH_FILTERS",
				"NotFound=Warning",
			"LAST");
			web_add_header("isRest","true");
			web_add_header("Content-Type","application/json");
			web_add_header("term","{SEARCH_STRING_PARAM}");
			web_add_header("coreName","MC_10001_CatalogEntry_en_US");
			addHeader();

			lr_start_transaction ( "T02_Search_getAutoSuggestion" ) ;
			web_custom_request("getAutoSuggestions",
						"URL=https://{api_host}/getAutoSuggestions",
						"Method=GET",
						"Resource=0",
						"RecContentType=application/json",
						"LAST");


				lr_end_transaction ( "T02_Search_getAutoSuggestion" , 0 ) ;
			 
 
			if (atoi(lr_eval_string("{autoSelectOptionTerm_count}")) > 0) {
				searchString = lr_paramarr_idx("autoSelectOptionTerm", 1);
				break;
			}
			else if (atoi(lr_eval_string("{autoSelectOptionCategory_count}")) > 0) {
				searchString = lr_paramarr_idx("autoSelectOptionCategory", 1);
				break;
			}
 

 
 
 
 

       	}  
   	 }  
 	 

}  


void submitCompleteSearch()  
{
	lr_think_time (LINK_TT);

	web_reg_save_param("totalProductsCount", "LB=totalProductsCount\":", "RB=,\"breadcrumbs", "NotFound=Warning", "LAST");

	lr_save_string(lr_eval_string("{searchStr}"), "searchTerm");

	lr_start_transaction ( "T02_Search" );

	web_url("search",
		"URL=https://{host}{store_home_page}search/{searchTerm}",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTTP",
		"LAST");

	lr_start_sub_transaction("T02_Search_tcpproduct/searchTermSuggestions", "T02_Search");
	addHeader();
	web_add_header("count", "4");
	web_add_header("resultCount", "4");
	web_add_header("searchTerm", lr_eval_string("{searchTerm}"));
	web_add_header("timeAllowed", "15000");
	web_reg_find("TEXT/IC=autosuggestions", "SaveCount=apiCheck", "LAST");

	web_url("searchTermSuggestions",
		"URL=https://{api_host}/tcpproduct/searchTermSuggestions",
		"Resource=0",
		"Mode=HTTP",
		"LAST");

	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T02_Search_tcpproduct/searchTermSuggestions", 1);
	else
		lr_end_sub_transaction("T02_Search_tcpproduct/searchTermSuggestions", 2);

	lr_save_string("T02_Search", "mainTransaction");
	callAPI();

	lr_end_transaction("T02_Search", 2);
	lr_save_string("search", "paginateBy");
	return;

}  


void submitCompleteSearchDidYouMean()  
{
	 
 	lr_think_time (LINK_TT);
 
 
 

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_SEARCH_SUGGEST )
		lr_param_sprintf ("SEARCH_STRING", "%s", lr_eval_string("{forDidYouMean}"));
	else
		lr_param_sprintf ("SEARCH_STRING", "%s", searchString);

	web_reg_find("Text= 0 matches", "SaveCount=searchResults_Count", "LAST" );
	web_reg_find("Text=The store has encountered a problem", "SaveCount=storeProblem_Count", "LAST" );  
	web_reg_save_param("didYouMeanId", "LB=http://{host}/shop/SearchDisplay?searchTermScope=&searchType=1002&filterTerm=&orderBy=&maxPrice=&showResultsPage=true&langId=-1&departmentId=&sType=",
	"RB=\" class=\"result\">", "ORD=All", "NotFound=Warning", "LAST");

	    
	lr_start_transaction ( "T02_Search" ) ;

	 
	  
	web_url("submitSearch",
		 
		"URL=https://{host}/shop/SearchDisplay?searchTerm={{SEARCH_STRING}}&storeId={storeId}&catalogId={catalogId}&langId=-1&pageSize=100&beginIndex=0&searchSource=Q&sType=SimpleSearch&resultCatEntry=2&showResultsPage=true&pageView=image&custSrch=search"
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		"LAST");

		lr_save_string("T02_Search", "mainTransaction");
 

	if (atoi(lr_eval_string("{searchResults_Count}")) == 0 || atoi(lr_eval_string("{storeProblem_Count}")) == 0 )
		lr_end_transaction ( "T02_Search" , 1 ) ;
	else
		lr_end_transaction ( "T02_Search" , 0 ) ;

}  

writeToFile1()
{
	char *ip;
    char fullpath[1024], * filename1 = "\\e$\\Performance\\LogOutput\\LowInventory_US_v2.dat";
	long file1;
 
    strcpy(fullpath, "\\\\" );
	ip = lr_get_host_name( );
    strcat(fullpath, ip);
	strcat(fullpath, filename1);

    if ((file1 = fopen(fullpath, "a+")) == 0) {
        lr_output_message ("Unable to open %s", fullpath);
        return -1;
    }

	fprintf(file1, "%s\n", lr_eval_string("{logLowCatEntryId}"));

    fclose(file1);
	return 0;
}

writeToFile2()
{
	char *ip;
    char fullpath[1024], * filename1 = "\\e$\\Performance\\LogOutput\\LowInventory_CA_v2.dat";
	long file1;
 
    strcpy(fullpath, "\\\\" );
	ip = lr_get_host_name( );
    strcat(fullpath, ip);
	strcat(fullpath, filename1);

    if ((file1 = fopen(fullpath, "a+")) == 0) {
        lr_output_message ("Unable to open %s", fullpath);
        return -1;
    }

	fprintf(file1, "%s\n", lr_eval_string("{logLowCatEntryId}"));

    fclose(file1);
	return 0;
}

void navBySearch()
{
	int searchSuggest = atoi(lr_eval_string("{RANDOM_PERCENT}"));

	lr_think_time ( TYPE_SPEED );
 
	searchString = lr_eval_string("{searchStr}");  
	lr_save_string(searchString,"originalSearchString");
	 
 




    length = (int)strlen(searchString);

    if (length >= 3)  
    	typeAheadSearch();

	if ( searchSuggest < RATIO_SEARCH_SUGGEST )
		submitCompleteSearchDidYouMean();
	else
		submitCompleteSearch();

	 

}  


void navByPlace()
{
	 
	if ( atoi( lr_eval_string( "{placeShopCategory_count}")) != 0 ) {

		lr_save_string( lr_paramarr_random("placeShopCategory"), "placeShop");

		if ( strcmp( lr_eval_string("{placeShop}") ,"placeShops-pjplace-2014" ) == 0)
			lr_save_string( lr_paramarr_random("placeShopCategory"), "placeShop");

		lr_start_transaction ( "T02_PlaceShop" ) ;

		web_url("placeShop",
			"URL=http://{host}{store_home_page}content/{placeShop}",
			"Resource=0",
			"RecContentType=text/html",
			"LAST");

		lr_end_transaction ( "T02_PlaceShop" , 0 ) ;
	}
 
# 1411 "..\\..\\browseFunctions.c"
}  


 
 
void topNav()
{
 

	randomPercent = atoi(lr_eval_string("{RANDOM_PERCENT}"));

	if ( randomPercent <= NAV_BROWSE )
		navByBrowse();
	else if ( randomPercent <= (NAV_BROWSE + NAV_CLEARANCE) )
		navByClearance();
	else if ( randomPercent <= (NAV_BROWSE + NAV_CLEARANCE + NAV_SEARCH))
		 
		submitCompleteSearch();
	else if (randomPercent <= (NAV_BROWSE + NAV_CLEARANCE + NAV_SEARCH + NAV_PLACE))
		navByPlace();

}  


void paginate()
{

	int itemCounter = 0;

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) < APPLY_PAGINATE ) {

		lr_start_transaction ( "T03_Paginate" ) ;

		addHeader();

		if(strcmp(lr_eval_string("{paginateBy}"), "search") == 0 ) {

			web_reg_save_param("uniqueId", "LB=\"uniqueID\":\"", "RB=\",\"seoURL", "ORD=All", "NotFound=Warning", "LAST");

			lr_start_sub_transaction("T03_Paginate getProductsBySearchTerm", "T03_Paginate");
			web_add_header( "searchTerm", lr_eval_string("{searchTerm}") );
			web_add_header( "pageNumber", lr_eval_string("{pageNumber}") );
			web_add_header( "pageSize", "18" );
			web_add_header( "searchSource", "E" );
			web_add_header( "searchType", "-1,-1,0" );

			web_custom_request("getProductsBySearchTerm",
				"URL=https://{api_host}/tcpproduct/getProductsBySearchTerm",
				"Method=GET",
				"Resource=0",
				"RecContentType=text/html",
				"EncType=application/x-www-form-urlencoded",
				"LAST");
			lr_end_sub_transaction("T03_Paginate getProductsBySearchTerm", 2);

			if ( lr_paramarr_len("uniqueId") != 0 ) {

				lr_param_sprintf ( "body", "%s", "{\"productId\":[\"" );

				for( itemCounter = 1; itemCounter <= lr_paramarr_len("uniqueId"); itemCounter++ ) {

					lr_param_sprintf( "body", "%s%s%s", lr_eval_string("{body}") , lr_paramarr_idx("uniqueId", itemCounter), "\"" );

					if (lr_paramarr_len("uniqueId") != itemCounter )
						lr_param_sprintf( "body", "%s%s", lr_eval_string("{body}"), ",\"" );
					else
						lr_param_sprintf( "body", "%s%s", lr_eval_string("{body}") , "\"]}" );

				}
			}

			 
			addHeader();

			if ( isLoggedIn == 1	) {
			lr_start_sub_transaction("T03_Paginate getListofDefaultWishlist", "T03_Paginate");
			web_custom_request("getListofDefaultWishlist",
				"URL=https://{api_host}/tcpstore/getListofDefaultWishlist",
				"Method=POST",
				"Resource=0",
				"RecContentType=application/json",
				"Mode=HTTP",
				"Body={body}",
				"LAST");
			lr_end_sub_transaction("T03_Paginate getListofDefaultWishlist", 2);
			}


		} else {

			web_reg_save_param("uniqueId", "LB=\"extendedData\": {\n            \"uniqueId\": \"", "RB=\",\n            \"parentIds", "ORD=All", "NotFound=Warning", "LAST");

			lr_start_sub_transaction("T03_Paginate getProductviewbyCategory", "T03_Paginate");
			web_add_header( "categoryId", lr_eval_string("{category}") );   
			web_add_header( "pageNumber", lr_eval_string("{pageNumber}") );
			web_add_header( "pageSize", "18" );
			web_add_header( "searchSource", "E" );
			web_add_header( "searchType", "-1,-1,0" );

			web_custom_request("getProductviewbyCategory",
				"URL=https://{api_host}/tcpproduct/getProductviewbyCategory",
				"Method=GET",
				"Resource=0",
				"RecContentType=text/html",
				"EncType=application/x-www-form-urlencoded",
				"LAST");
			lr_end_sub_transaction("T03_Paginate getProductviewbyCategory", 2);

			if ( lr_paramarr_len("uniqueId") != 0 ) {

				lr_param_sprintf ( "body", "%s", "{\"productId\":[\"" );

				for( itemCounter = 1; itemCounter <= lr_paramarr_len("uniqueId"); itemCounter++ ) {

					lr_param_sprintf( "body", "%s%s%s", lr_eval_string("{body}") , lr_paramarr_idx("uniqueId", itemCounter), "\"" );

					if (lr_paramarr_len("uniqueId") != itemCounter )
						lr_param_sprintf( "body", "%s%s", lr_eval_string("{body}"), ",\"" );
					else
						lr_param_sprintf( "body", "%s%s", lr_eval_string("{body}"), "]}" );

				}
			}

			if ( isLoggedIn == 1	) {
			lr_start_sub_transaction("T03_Paginate getListofDefaultWishlist", "T03_Paginate");
			web_custom_request("getListofDefaultWishlist",
				"URL=https://{api_host}/tcpstore/getListofDefaultWishlist",
				"Method=POST",
				"Resource=0",
				"RecContentType=application/json",
				"Mode=HTTP",
				"Body={body}",
				"LAST");
			lr_end_sub_transaction("T03_Paginate getListofDefaultWishlist", 2);
			}
		}

		lr_end_transaction ( "T03_Paginate" , 0 ) ;

 
# 1581 "..\\..\\browseFunctions.c"
	}

}  


void sortResults()
{
	 
 	lr_think_time ( LINK_TT ) ;
	if (( lr_paramarr_len ( "sortURL" ) > 0 )) {
 
 
 
	 
	 
		lr_param_sprintf ( "drillUrl" , "http://%s%s" , lr_eval_string("{host}") , lr_paramarr_random ( "sortURL" ) ) ;

		lr_start_transaction ( "T03_Sort Results" ) ;
			webURL(lr_eval_string ( "{drillUrl}" ), "T03_Sort Results" );
		lr_end_transaction ( "T03_Sort Results" , 0 ) ;
	}  
}  

void sort()
{
 
 
 

 

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) < APPLY_SORT ){
		 
		sortResults();
		}  
	else {
		 
		return;
		}  

}  

void drillOneFacet()
{
	 
 	lr_think_time ( LINK_TT ) ;

 
 

 
		web_reg_save_param ( "sizesFacetsURL", "LB=\"id\":\"ads_f16501_ntk_cs", "RB=\"},{\"displayName", "ORD=ALL" , "Convert=HTML_TO_URL", "NotFound=warning", "LAST" ); 
		web_reg_save_param ( "colorFacetsURL", "LB=\"id\":\"ads_f12001_ntk_cs", "RB=\"},{\"displayName", "ORD=ALL" , "Convert=HTML_TO_URL", "NotFound=warning", "LAST" ); 
		web_reg_save_param( "pdpURL", "LB=,\"pdpUrl\":\"{store_home_page}p/", "RB=\",\"shortDescription", "ORD=All", "NotFound=Warning", "LAST");  
		web_reg_save_param( "productId", "LB=generalProductId\":\"", "RB=\"", "ORD=All", "NotFound=Warning", "LAST");  
		lr_param_sprintf ( "drillUrl" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ( "{subCategory}" ) ) ;

		lr_save_string("T03_Sub-Category Display", "mainTransaction");
		if( atoi(lr_eval_string("{akamaiRatio}")) <= (100 - NO_BROWSE_RATIO_SUBCATEGORY) ) {

			lr_save_string("akamai", "callerId2");
			API_SUB_TRANSACTION_SWITCH = 0;
			callAPI();
		} else {
			lr_save_string("T03_Sub-Category Display","callerId");
			lr_start_transaction ( "T03_Sub-Category Display" ) ;
			if ( webURL2(lr_eval_string ( "{drillUrl}" ), "T03_Sub-Category Display", "html" ) == 0)
			{
				callAPI();
				lr_end_transaction ( "T03_Sub-Category Display" , 0 ) ;
				lr_save_string("", "mainTransaction");
			}
			else if ( atoi(lr_eval_string("{sizesFacetsURL}")) == 1 )
			{
				lr_fail_trans_with_error( lr_eval_string("T03_Sub-Category Display Text Check Failed - {drillUrl} AND no products found") ) ;
				lr_end_transaction ( "T03_Sub-Category Display" , 1 ) ;
			}
			else {
				lr_fail_trans_with_error( lr_eval_string("T03_Sub-Category Display Text Check Failed - {drillUrl}") ) ;
				lr_end_transaction ( "T03_Sub-Category Display" , 1 ) ;
			}
		}

		lr_save_string("categoryDisplay", "callerId");
	    if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 50 )
		{
			if (( lr_paramarr_len ( "sizesFacetsURL" ) > 0 )) {
				lr_save_string(lr_paramarr_random("sizesFacetsURL"), "facet");
				lr_param_sprintf ( "drillUrlOneFacet" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ( "{subCategory}?sizes=ads_f16501_ntk_cs{facet}" ) ) ;
			}
			else
				return;
		}
		else
		{
			if (( lr_paramarr_len ( "colorFacetsURL" ) > 0 )) {
				lr_save_string(lr_paramarr_random("colorFacetsURL"), "facet");
				lr_param_sprintf ( "drillUrlOneFacet" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ( "{subCategory}?colors=ads_f12001_ntk_cs{facet}" ) ) ;
			}
			else
				return;
		}

 
		web_reg_save_param( "pdpURL", "LB=,\"pdpUrl\":\"{store_home_page}p/", "RB=\",\"shortDescription", "ORD=All", "NotFound=Warning", "LAST");  
		web_reg_save_param( "productId", "LB=generalProductId\":\"", "RB=\"", "ORD=All", "NotFound=Warning", "LAST");  
		lr_start_transaction ( "T03_Facet Display_1facet" ) ;

		webURL(lr_eval_string ( "{drillUrlOneFacet}" ), "T03_Facet Display_1facet" );
		lr_save_string("T03_Facet Display_1facet", "mainTransaction");

		callAPI();

		lr_end_transaction ( "T03_Facet Display_1facet" , 0 ) ;

 
}  


void drillTwoFacets()
{
 
	int nCount = 0;
 	lr_think_time ( LINK_TT ) ;

 
 
 
 
		productDisplayPageCorrelations();

 
		web_reg_save_param ( "sizesFacetsURL", "LB=\"id\":\"ads_f16501_ntk_cs", "RB=\"},{\"displayName", "ORD=ALL" , "Convert=HTML_TO_URL", "NotFound=warning", "LAST" ); 
		web_reg_save_param ( "colorFacetsURL", "LB=\"id\":\"ads_f12001_ntk_cs", "RB=\"},{\"displayName", "ORD=ALL" , "Convert=HTML_TO_URL", "NotFound=warning", "LAST" ); 
		web_reg_save_param( "pdpURL", "LB=,\"pdpUrl\":\"{store_home_page}p/", "RB=\",\"shortDescription", "ORD=All", "NotFound=Warning", "LAST");  
		web_reg_save_param( "productId", "LB=generalProductId\":\"", "RB=\"", "ORD=All", "NotFound=Warning", "LAST");  
		lr_param_sprintf ( "drillUrl" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ( "{subCategory}" ) ) ;

		lr_save_string("T03_Sub-Category Display", "mainTransaction");
		if( atoi(lr_eval_string("{akamaiRatio}")) <= (100 - NO_BROWSE_RATIO_SUBCATEGORY) ) {

			lr_save_string("akamai", "callerId2");
			API_SUB_TRANSACTION_SWITCH = 0;
			callAPI();

		} else {

			lr_start_transaction ( "T03_Sub-Category Display" ) ;
			if ( webURL2(lr_eval_string ( "{drillUrl}" ), "T03_Sub-Category Display", "html" ) == 0)
			{
				callAPI();
				lr_end_transaction ( "T03_Sub-Category Display" , 0 ) ;
				lr_save_string("", "mainTransaction");
			}
			else if ( atoi(lr_eval_string("{sizesFacetsURL}")) == 1 )
			{
					lr_fail_trans_with_error( lr_eval_string("T03_Sub-Category Display Text Check Failed - {drillUrl} AND no products found") ) ;
					lr_end_transaction ( "T03_Sub-Category Display" , 1 ) ;
			}
			else {
					lr_fail_trans_with_error( lr_eval_string("T03_Sub-Category Display Text Check Failed - {drillUrl}") ) ;
					lr_end_transaction ( "T03_Sub-Category Display" , 1 ) ;
			}
		}

		lr_save_string("categoryDisplay", "callerId");
	    if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 80 )
		{
			if (( lr_paramarr_len ( "sizesFacetsURL" ) > 0 )) {
				lr_save_string(lr_paramarr_random("sizesFacetsURL"), "facet1");
				lr_param_sprintf ( "drillUrlTwoFacets" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ("{subCategory}?sizes=ads_f16501_ntk_cs{facet1}" ) ) ;
 
				nCount = lr_paramarr_len("sizesFacetsURL");
				while( nCount >= 2)
				{
					lr_save_string(lr_paramarr_random("sizesFacetsURL"), "facet2");
					if (strcmp(lr_eval_string("{facet2}"), lr_eval_string("{facet1}")) != 0 )
					{	lr_param_sprintf ( "drillUrlTwoFacets" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ("{subCategory}?sizes=ads_f16501_ntk_cs{facet1}%2Csizes=ads_f16501_ntk_cs{facet2}" ) ) ;
 
						break;
					}
				}
			}
			else
				return;
		}
		else
		{
			if (( lr_paramarr_len ( "colorFacetsURL" ) > 0 )) {
				lr_save_string(lr_paramarr_random("colorFacetsURL"), "facet1");
				lr_param_sprintf ( "drillUrlTwoFacets" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ("{subCategory}?colors=ads_f12001_ntk_cs{facet1}" ) );
 
				nCount = lr_paramarr_len("colorFacetsURL");
				while( nCount >= 2)
				{
					lr_save_string(lr_paramarr_random("colorFacetsURL"), "facet2");
					if (strcmp(lr_eval_string("{facet2}"), lr_eval_string("{facet1}")) != 0 )
					{	lr_param_sprintf ( "drillUrlTwoFacets" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ("{subCategory}?colors=ads_f12001_ntk_cs{facet1}%2Cads_f12001_ntk_cs{facet2}" ) ) ;
 
						break;
					}
				}
			}
			else
				return;
		}

		web_reg_save_param( "pdpURL", "LB=,\"pdpUrl\":\"{store_home_page}p/", "RB=\",\"shortDescription", "ORD=All", "NotFound=Warning", "LAST");  
		web_reg_save_param( "productId", "LB=generalProductId\":\"", "RB=\"", "ORD=All", "NotFound=Warning", "LAST");  
		productDisplayPageCorrelations();
		lr_start_transaction ( "T03_Facet Display_2facets" ) ;

			webURL(lr_eval_string ( "{drillUrlTwoFacets}" ), "T03_Facet Display_2facets" );

			lr_save_string("T03_Facet Display_2facets", "mainTransaction");
			callAPI();

		lr_end_transaction ( "T03_Facet Display_2facets" , 0 ) ;

 
}  


void drillThreeFacets()
{
 
	int nCount = 0;
	 
 	lr_think_time ( LINK_TT ) ;
 
 

 
		web_reg_save_param ( "sizesFacetsURL", "LB=\"id\":\"ads_f16501_ntk_cs", "RB=\"},{\"displayName", "ORD=ALL" , "Convert=HTML_TO_URL", "NotFound=warning", "LAST" ); 
		web_reg_save_param ( "colorFacetsURL", "LB=\"id\":\"ads_f12001_ntk_cs", "RB=\"},{\"displayName", "ORD=ALL" , "Convert=HTML_TO_URL", "NotFound=warning", "LAST" ); 
		lr_param_sprintf ( "drillUrl" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ( "{subCategory}" ) ) ;
		web_reg_save_param( "pdpURL", "LB=,\"pdpUrl\":\"{store_home_page}p/", "RB=\",\"shortDescription", "ORD=All", "NotFound=Warning", "LAST");  
		web_reg_save_param( "productId", "LB=generalProductId\":\"", "RB=\"", "ORD=All", "NotFound=Warning", "LAST");  

		lr_save_string("T03_Sub-Category Display", "mainTransaction");
		if( atoi(lr_eval_string("{akamaiRatio}")) <= (100 - NO_BROWSE_RATIO_SUBCATEGORY) ) {

			lr_save_string("akamai", "callerId2");
			API_SUB_TRANSACTION_SWITCH = 0;
			callAPI();
		} else {

			lr_start_transaction ( "T03_Sub-Category Display" ) ;
			if ( webURL2(lr_eval_string ( "{drillUrl}" ), "T03_Sub-Category Display", "html" ) == 0)
			{
				callAPI();
				lr_end_transaction ( "T03_Sub-Category Display" , 0 ) ;
				lr_save_string("", "mainTransaction");
			}
			else if ( atoi(lr_eval_string("{sizesFacetsURL}")) == 1 )
			{
					lr_fail_trans_with_error( lr_eval_string("T03_Sub-Category Display Text Check Failed - {drillUrl} AND no products found") ) ;
					lr_end_transaction ( "T03_Sub-Category Display" , 1 ) ;
			}
			else {
					lr_fail_trans_with_error( lr_eval_string("T03_Sub-Category Display Text Check Failed - {drillUrl}") ) ;
					lr_end_transaction ( "T03_Sub-Category Display" , 1 ) ;
			}
		}

		lr_save_string("categoryDisplay", "callerId");
	    if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 80 )
		{
			if (( lr_paramarr_len ( "sizesFacetsURL" ) > 0 )) {
				lr_save_string(lr_paramarr_random("sizesFacetsURL"), "facet1");
				lr_param_sprintf ( "drillUrlThreeFacets" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ("{subCategory}?sizes=ads_f16501_ntk_cs{facet1}" ) ) ;
 

				nCount = lr_paramarr_len("sizesFacetsURL");
				while( nCount >= 2)
				{
					lr_save_string(lr_paramarr_random("sizesFacetsURL"), "facet2");
					if (strcmp(lr_eval_string("{facet2}"), lr_eval_string("{facet1}")) != 0 )
					{	lr_param_sprintf ( "drillUrlThreeFacets" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ("{subCategory}?sizes=ads_f16501_ntk_cs{facet1}%2Csizes=ads_f16501_ntk_cs{facet2}" ) ) ;
 
						break;
					}
				}
				nCount = lr_paramarr_len("sizesFacetsURL");
				while( nCount >= 3)
				{
					lr_save_string(lr_paramarr_random("sizesFacetsURL"), "facet3");
					if (strcmp(lr_eval_string("{facet3}"), lr_eval_string("{facet1}")) != 0 && strcmp(lr_eval_string("{facet3}"), lr_eval_string("{facet2}")) != 0 )
					{	lr_param_sprintf ( "drillUrlThreeFacets" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ("{subCategory}?sizes=ads_f16501_ntk_cs{facet1}%2Cads_f16501_ntk_cs{facet2}%2Cads_f16501_ntk_cs{facet3}" ) ) ;
						break;
					}
				}
			}
			else
				return;
		}
		else
		{
			if (( lr_paramarr_len ( "colorFacetsURL" ) > 0 )) {
				lr_save_string(lr_paramarr_random("colorFacetsURL"), "facet1");
				lr_param_sprintf ( "drillUrlThreeFacets" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ("{subCategory}?colors=ads_f12001_ntk_cs{facet1}" ) );
 
				nCount = lr_paramarr_len("colorFacetsURL");
				while( nCount >= 2)
				{
					lr_save_string(lr_paramarr_random("colorFacetsURL"), "facet2");
					if (strcmp(lr_eval_string("{facet2}"), lr_eval_string("{facet1}")) != 0 )
					{	lr_param_sprintf ( "drillUrlThreeFacets" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ("{subCategory}?colors=ads_f12001_ntk_cs{facet1}%2Cads_f12001_ntk_cs{facet2}" ) ) ;
 
						break;
					}
				}
				nCount = lr_paramarr_len("colorFacetsURL");
				while( nCount >= 3)
				{
					lr_save_string(lr_paramarr_random("colorFacetsURL"), "facet3");
					if (strcmp(lr_eval_string("{facet3}"), lr_eval_string("{facet1}")) != 0 && strcmp(lr_eval_string("{facet3}"), lr_eval_string("{facet2}")) != 0 )
					{
						lr_param_sprintf ( "drillUrlThreeFacets" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ("{subCategory}?colors=ads_f12001_ntk_cs{facet1}%2Cads_f12001_ntk_cs{facet2}%2Cads_f12001_ntk_cs{facet3}" ) ) ;
 
						break;
					}
				}
			}
			else
				return;
		}

		web_reg_save_param( "pdpURL", "LB=,\"pdpUrl\":\"{store_home_page}p/", "RB=\",\"shortDescription", "ORD=All", "NotFound=Warning", "LAST");  
		web_reg_save_param( "productId", "LB=generalProductId\":\"", "RB=\"", "ORD=All", "NotFound=Warning", "LAST");  
		productDisplayPageCorrelations();

		lr_start_transaction ( "T03_Facet Display_3facets" ) ;
 






			web_custom_request("T03_Facet Display_3facets" ,
				 "URL={drillUrlThreeFacets}" ,
				"Method=GET",
				"Resource=0",
				 "Mode=HTML" ,
				"LAST");

			lr_save_string("T03_Facet Display_3facets", "mainTransaction");
			callAPI();

			lr_save_string("", "drillUrlThreeFacets");

		lr_end_transaction ( "T03_Facet Display_3facets" , 0 ) ;

 
} 

int checkOutfit()
{
    extern char * strtok(char * string, const char * delimiters );
    char path[1000] = "";
    char separators[] = "-";
    char * token;
 
 

 

	lr_save_string(lr_paramarr_random ( "subCategories" ), "subCategory");
    strcpy(path, lr_eval_string("{subCategory}") );

     
    token = (char *)strtok(path, separators);
    if (!token) {
        lr_output_message ("No tokens found in string!");
    }

    while (token != 0 ) {
 
		 
        token = (char *)strtok(0, separators);

        if(token != 0) {
	        if (strcmp(token,"outfits") == 0) {
				return 1;
	        }
        }
    }

	return 0;

}

int getSubCategories()
{
    extern char * strtok(char * string, const char * delimiters );
    char path[9999] = "";
    char separators[] = "\"";
    char * token;
 
    int counterX, counterY, counterZ = 0;
    char *arr[20];  

    strcpy(path, lr_eval_string("{subcategory}") );

     
    token = (char *)strtok(path, separators);
    if (!token) {
        lr_output_message ("No tokens found in string!");
    }

    while (token != 0 ) {
        token = (char *)strtok(0, separators);
 
 

		if(token != 0) {  
	        if (strcmp(token,"url") == 0) {
				token = (char *)strtok(0, separators);
				token = (char *)strtok(0, separators);
				counterX++;

				arr[counterX] = token;
				lr_message("CounterX is %d", counterX);
				if (counterX == 1) {
					lr_param_sprintf("category", "%s", token);
				} else {
					counterZ++;
					lr_param_sprintf("subcategories", "subcategories_%d", counterZ);
					lr_param_sprintf(lr_eval_string("{subcategories}"), "%s", token );
					lr_save_int(counterZ, "subcategories_count");
				}
			}
        }
    }
    lr_message("parameter length is %d", atoi(lr_eval_string("{subcategories_count}")) );
     
    while ( checkOutfit() == 1) {}
	return 0;
}

void drillSubCategory()
{
 	lr_think_time ( LINK_TT ) ;

 

		 

 
		lr_param_sprintf ( "drillUrl" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string("{subCategory}") ) ;
		web_reg_save_param( "pdpURL", "LB=,\"pdpUrl\":\"{store_home_page}p/", "RB=\",\"shortDescription", "ORD=All", "NotFound=Warning", "LAST");  
		web_reg_save_param( "productId", "LB=generalProductId\":\"", "RB=\"", "ORD=All", "NotFound=Warning", "LAST");  
		productDisplayPageCorrelations();
		lr_save_string(lr_eval_string("{subCategory}"), "category");  
		lr_save_string("T03_Sub-Category Display", "mainTransaction");
		if( atoi(lr_eval_string("{akamaiRatio}")) <= (100 - NO_BROWSE_RATIO_SUBCATEGORY) ) {

			lr_save_string("akamai", "callerId2");
			API_SUB_TRANSACTION_SWITCH = 0;
			callAPI();

		} else {

			lr_start_transaction ( "T03_Sub-Category Display" ) ;

	 

			if ( webURL2(lr_eval_string ( "{drillUrl}" ), "T03_Sub-Category Display", "html" ) == 0)
			{

				lr_save_string("categoryDisplay", "callerId");
				callAPI();
				lr_end_transaction ( "T03_Sub-Category Display" , 0 ) ;
			}
			else if ( atoi(lr_eval_string("{noProducts}")) == 1 )
			{
					lr_fail_trans_with_error( lr_eval_string("T03_Sub-Category Display Text Check Failed - {drillUrl} AND no products found") ) ;
					lr_end_transaction ( "T03_Sub-Category Display" , 1 ) ;
			}
			else {
					lr_fail_trans_with_error( lr_eval_string("T03_Sub-Category Display Text Check Failed - {drillUrl}") ) ;
					lr_end_transaction ( "T03_Sub-Category Display" , 1 ) ;
			}
		}

 

	 

} 


void drill()
{
	 
	randomPercent = atoi(lr_eval_string("{RANDOM_PERCENT}"));

	if ( randomPercent <= DRILL_ONE_FACET ) {
		drillOneFacet();
	} else if ( randomPercent <= (DRILL_ONE_FACET + DRILL_TWO_FACET) ) {
		drillTwoFacets();
	} else if ( randomPercent <= (DRILL_ONE_FACET + DRILL_TWO_FACET + DRILL_THREE_FACET)) {
		drillThreeFacets();
	} else if ( randomPercent <= (DRILL_ONE_FACET + DRILL_TWO_FACET + DRILL_THREE_FACET + DRILL_SUB_CATEGORY)) {
		drillSubCategory();
	}

}  

int getValueOf()
{
    extern char * strtok(char * string, const char * delimiters );
    char path[9999] = "";
    char separators[] = "\"";
    char * token;
    strcpy(path, lr_eval_string("{getProductId}") );

     
    token = (char *)strtok(path, separators);
    if (!token) {
        lr_output_message ("No tokens found in string!");
    }

    while (token != 0 ) {
 
 
        token = (char *)strtok(0, separators);
 
        lr_save_string(token, "generalProductId");
        break;
    }
	return 0;

}

void productDetailViewBOPIS()
{
 	lr_think_time ( LINK_TT ) ;

 

 

		 

		lr_param_sprintf ( "drillUrl" , "http://%s%s" , lr_eval_string("{host}") , lr_eval_string("/us/p/{randCatEntryId_BOPIS}") ) ;
 
 
 
		web_reg_save_param_regexp ("ParamName=getProductId",
							"RegExp=navigationTree.*?generalProductId(.*?)name\"",
							"NotFound=Warning",
							"SEARCH_FILTERS",
							"RequestUrl=*",	"LAST" );
 
		lr_start_transaction ( "T04_Product Display Page" ) ;

		if (webURL2(lr_eval_string ( "{drillUrl}" ), "T04_Product Display Page", "satellite.pageBottom") == 0) {

			lr_save_string("T04_Product Display Page", "mainTransaction");
			lr_save_string("productDisplay", "callerId");
			callAPI();

			lr_save_string("PDP","PDP_or_PQV");
			lr_end_transaction ( "T04_Product Display Page" , 0 ) ;

		}
		else {
			lr_fail_trans_with_error( lr_eval_string("T04_Product Display Text Check Failed - {drillUrl} - {storeId}") ) ;
			lr_end_transaction ( "T04_Product Display Page" , 1 ) ;
		}

		 

 

}


void productDetailView()
{
 	lr_think_time ( LINK_TT ) ;

 

 
 
 

		lr_save_string( lr_eval_string("{pdpURLData}") , "pdpURL") ;

		lr_param_sprintf ( "drillUrl" , "http://%s%s%s" , lr_eval_string("{host}") , lr_eval_string("{store_home_page}p/"), lr_eval_string( "{pdpURL}" ) ) ;
		 

		 


		lr_save_string("T04_Product Display Page", "mainTransaction");

		lr_save_string("productDisplay", "callerId");

		if( atoi(lr_eval_string("{akamaiRatio}")) <= (100 - NO_BROWSE_RATIO_PDP) ) {

			lr_save_string("akamai", "callerId2");
			API_SUB_TRANSACTION_SWITCH = 0;
			callAPI();

		} else {
			web_reg_save_param_regexp ("ParamName=getProductId",
					"RegExp=navigationTree.*?generalProductId(.*?)name\"",
					"NotFound=Warning",
					"SEARCH_FILTERS",
					"RequestUrl=*",	"LAST" );

			lr_start_transaction ( "T04_Product Display Page" ) ;

			if (webURL2(lr_eval_string ( "{drillUrl}" ), "T04_Product Display Page", "satellite.pageBottom") == 0) {

				getValueOf();
				lr_save_string("productDisplay", "callerId");
				callAPI();

				if ( isLoggedIn == 1 && atoi(lr_eval_string("{RANDOM_PERCENT}")) <= PDP_to_WL )
				{
					lr_start_sub_transaction("T04_Product Display Page_addOrUpdateWishlist", "T04_Product Display Page");
					addHeader();
					web_add_header("Content-Type","application/json");
					web_add_header("addItem", "true");
					web_custom_request("addOrUpdateWishlist",
						"URL=https://{api_host}/addOrUpdateWishlist",
						"Method=PUT",
						"Resource=0",
						"RecContentType=application/json",
						"Mode=HTTP",
						"Body={\"item\":[{\"productId\":\"{generalProductId}\",\"quantityRequested\":\"1\",\"isProduct\":\"TRUE\"}]}",   
	 
						"LAST");
					lr_end_sub_transaction("T04_Product Display Page_addOrUpdateWishlist", 2);

					lr_start_sub_transaction("T04_Product Display Page_tcpproduct/getSKUInventoryandProductCounterDetails", "T04_Product Display Page");
					addHeader();
					web_add_header("productId", lr_eval_string("{productId}"));
					web_url("getSKUInventoryandProductCounterDetails",
						"URL=https://{api_host}/tcpproduct/getSKUInventoryandProductCounterDetails",
						"Resource=1",
						"RecContentType=application/json",
						"LAST");
					lr_end_sub_transaction("T04_Product Display Page_tcpproduct/getSKUInventoryandProductCounterDetails", 2);
				}

				lr_save_string("PDP","PDP_or_PQV");
				lr_end_transaction ( "T04_Product Display Page" , 0 ) ;

			} else {
				lr_fail_trans_with_error( lr_eval_string("T04_Product Display Text Check Failed - {drillUrl} - {storeId}") ) ;
				lr_end_transaction ( "T04_Product Display Page" , 1 ) ;
			}
		}
		 

 

}  


void productQuickView()
{
 	lr_think_time ( LINK_TT ) ;

	if (( lr_paramarr_len ( "quickviewURL" ) > 0 )) {

		productDisplayPageCorrelations();

		lr_param_sprintf ( "drillUrl" , "http://%s%s" , lr_eval_string("{host}") , lr_paramarr_random ( "quickviewURL" ) ) ;

		lr_start_transaction ( "T04_Product Quickview Page" ) ;

		webURL(lr_eval_string ( "{drillUrl}" ), "T04_Product Quickview Page" );

		lr_start_sub_transaction ("T04_Product Quickview Page - GetSwatchesAndSizeInfo", "T04_Product Quickview Page" );

		web_custom_request("GetSwatchesAndSizeInfo",
			"URL=http://{host}/webapp/wcs/stores/servlet/GetSwatchesAndSizeInfo?langId=-1&storeId={storeId}&catalogId={catalogId}&productId={skuSelectionProductID}",
			"Method=GET",
			"Resource=0",
			"RecContentType=text/html",
			"Snapshot=t18.inf",
			"Mode=HTML",
			"EncType=application/x-www-form-urlencoded",
			"LAST");

		lr_end_sub_transaction ( "T04_Product Quickview Page - GetSwatchesAndSizeInfo", 0 );

 
 
 
		web_reg_save_param_regexp( "ParamName=atc_catentryIds" ,
	                          "RegExp={ \"(.*?)\" : \"([1-9][0-9]*)\"" ,
	                            
			                   "Ordinal=ALL" ,
			                   "Notfound=warning" ,
			                   "SEARCH_FILTERS" ,
			                   "Group=1" ,
			                   "LAST" ) ;

 
 

 
	   lr_start_sub_transaction ("T04_Product Quickview Page - TCPGetSKUDetailsView", "T04_Product Quickview Page" );

		web_custom_request("TCPGetSKUDetailsView",
			"URL=http://{host}/webapp/wcs/stores/servlet/TCPGetSKUDetailsView?langId=-1&storeId={storeId}&catalogId={catalogId}&productId={skuSelectionProductID}",
			"Method=GET",
			"Resource=0",
			"RecContentType=text/html",
			"Snapshot=t18.inf",
			"Mode=HTML",
			"EncType=application/x-www-form-urlencoded",
			"LAST");

		lr_end_sub_transaction ( "T04_Product Quickview Page - TCPGetSKUDetailsView", 0 );


		if ( isLoggedIn == 1 )
			TCPGetWishListForUsersView(); 

		lr_save_string("PQV","PDP_or_PQV");

		lr_end_transaction ( "T04_Product Quickview Page" , 0 ) ;

		 

	}  

}  

void productQuickviewService()  
{
	web_reg_find("SaveCount=swatchesAndSizeInfo", "Text/IC=itemTCPBogo","LAST");

	 

	lr_start_transaction ("T04_Product Quickview Service - GetSwatchesAndSizeInfo" );

		web_custom_request("GetSwatchesAndSizeInfo",
			"URL=http://{host}/webapp/wcs/stores/servlet/GetSwatchesAndSizeInfo?storeId={storeId}&productId={skuSelectionProductID}",
			"Method=GET",
			"Mode=HTML",
			"LAST");

	if(atoi(lr_eval_string("{swatchesAndSizeInfo}")) > 0)
		lr_end_transaction ( "T04_Product Quickview Service - GetSwatchesAndSizeInfo", 0 );
	else {
		lr_param_sprintf ( "failedPageUrl" , "T03_Sub-Category Display Failure : %s", lr_eval_string("http://{host}/webapp/wcs/stores/servlet/GetSwatchesAndSizeInfo?storeId={storeId}&productId={skuSelectionProductID}") ) ;
		lr_fail_trans_with_error( lr_eval_string("T04_Product Display Text Check Failed - {failedPageUrl} - {storeId}") ) ;
		lr_end_transaction ( "T04_Product Quickview Service - GetSwatchesAndSizeInfo", 1 );
	}
	 

	web_reg_find("SaveCount=detailsView", "Text/IC=inventoryFlag","LAST");

	lr_start_transaction ("T04_Product Quickview Service - TCPGetSKUDetailsView" );

		web_custom_request("TCPGetSKUDetailsView",
			"URL=http://{host}/webapp/wcs/stores/servlet/TCPGetSKUDetailsView?storeId={storeId}&catalogId={catalogId}&langId=-1&productId={skuSelectionProductID}",
			"Method=GET",
			"Mode=HTML",
			"LAST");


	if(atoi(lr_eval_string("{detailsView}")) > 0)
		lr_end_transaction ( "T04_Product Quickview Service - TCPGetSKUDetailsView", 0 );
	else
		lr_end_transaction ( "T04_Product Quickview Service - TCPGetSKUDetailsView", 1 );

}

void productDisplay()
{
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= PDP )
	{
 
			productDetailView();
 
 
	}
	else
	{
		if (( lr_paramarr_len ( "quickviewURL" ) > 0 ))
			productQuickView();  
		else
			productDetailView();
	}

}  

void productDisplayBOPIS()
{
	productDetailViewBOPIS();  
}


void switchColor()
{
}  

void emailSignUp()
{
	lr_start_transaction ( "T16_Submit Shipping Address" ) ;
	lr_end_transaction ( "T16_Submit Shipping Address" , 0) ;

}  


void StoreLocator()
{
 	lr_think_time ( LINK_TT ) ;

	lr_start_transaction("T22_StoreLocator");

		lr_save_string("T22_StoreLocator", "mainTransaction" );

		lr_start_sub_transaction("T22_StoreLocator_store-locator", lr_eval_string("{mainTransaction}") );

			web_url("StoreLocator",
			"URL=http://{host}/{country}/store-locator",
			"TargetFrame=",
			"Resource=0",
			"RecContentType=text/html",
			"Mode=HTML",
			"LAST");

 
			tcp_api2("payment/getRegisteredUserDetailsInfo", "GET",  lr_eval_string("{mainTransaction}") );
 
 
			tcp_api2("tcpproduct/getPriceDetails", "GET",  lr_eval_string("{mainTransaction}") );

			web_add_header("locStore", "False");
			web_add_header("pageName", "orderSummary");
			tcp_api2("getOrderDetails", "GET", lr_eval_string("{mainTransaction}") );
 
 
 

		lr_end_sub_transaction("T22_StoreLocator_store-locator", 2);

		lr_start_sub_transaction("T22_StoreLocator_Find", "T22_StoreLocator" );
			addHeader();
			web_add_header("latitude", "40.7879061");
			web_add_header("longitude", "-74.0444976");
			web_add_header("maxItems", "5");
			web_add_header("radius", "75");
 
		web_reg_save_param_json("ParamName=uniqueId", "QueryString=$.PhysicalStore..uniqueId", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
			web_reg_find("TEXT/IC=uniqueId", "SaveCount=apiCheck", "LAST");
			web_url("findStoresbyLatitudeandLongitude",
				"URL=https://{api_host}/tcpproduct/findStoresbyLatitudeandLongitude",
				"Resource=0",
				"RecContentType=application/json",
				"LAST");
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction("T22_StoreLocator_Find", 1);
		else
			lr_end_sub_transaction("T22_StoreLocator_Find", 2);

		lr_save_string("T22_StoreLocator_Set_As_Favorite_Store", "mainTransaction" );
		lr_start_sub_transaction("T22_StoreLocator_Set_As_Favorite_Store", "T22_StoreLocator" );

			addHeader();
 
			web_add_header("fromPage", "StoreLocator");
			web_add_header("action", "add");
			web_add_header("storeLocId", lr_eval_string("{uniqueId}") );
 
			web_add_header("userId", lr_eval_string("{userId}") );
			web_reg_find("TEXT/IC={uniqueId}", "SaveCount=apiCheck", "LAST");
			web_custom_request("addFavouriteStoreLocation",
				"URL=https://{api_host}/tcporder/addFavouriteStoreLocation",
				"Method=POST",
				"Resource=0",
				"RecContentType=application/json",
				"Mode=HTTP",
				"EncType=application/json",
				"Body={}",
				"LAST");
			if (atoi(lr_eval_string("{apiCheck}")) == 0 )
				lr_end_sub_transaction("T22_StoreLocator_Set_As_Favorite_Store", 1);
			else
				lr_end_sub_transaction("T22_StoreLocator_Set_As_Favorite_Store", 2);

 







		lr_save_string("T22_StoreLocator_See_StoreDetails", "mainTransaction" );
		lr_start_sub_transaction("T22_StoreLocator_See_StoreDetails", "T22_StoreLocator" );

			web_url("See Store Details",
				"URL=https://{host}/us/store/secaucusoutlet-nj-secaucus-07094-114298",
				"Resource=0",
				"RecContentType=text/html",
				"Mode=HTTP",
					"LAST");

 
			tcp_api2("payment/getRegisteredUserDetailsInfo", "GET",  lr_eval_string("{mainTransaction}") );
 
 
			tcp_api2("tcpproduct/getPriceDetails", "GET",  lr_eval_string("{mainTransaction}") );
			web_add_header("locStore", "False");
			web_add_header("pageName", "orderSummary");
			tcp_api2("getOrderDetails", "GET", lr_eval_string("{mainTransaction}") );
 
 
 

		lr_end_sub_transaction("T22_StoreLocator_See_StoreDetails", 2);


	lr_end_transaction("T22_StoreLocator", 2);


}

void TCPGetWishListForUsersViewBrowse()
{
	lr_think_time ( LINK_TT ) ;

	lr_start_transaction("T25_Common_S01_TCPGetWishListForUsersView");

	web_custom_request("TCPGetWishListForUsersView",
		"URL=http://{host}/webapp/wcs/stores/servlet/TCPGetWishListForUsersView?tcpCallBack=jQuery1113014590106982485151_1473881708524&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&sortBy=&_=1473881708525",
		"Method=GET",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		"EncType=application/x-www-form-urlencoded",
		"LAST");

	lr_end_sub_transaction("T25_Common_S01_TCPGetWishListForUsersView", 2);

}
void TCPIShippingView()
{
	lr_think_time ( LINK_TT ) ;

	lr_start_transaction("T27_TCPIShippingView");

	web_custom_request("TCPIShippingView",
		"URL=https://{host}/shop/TCPIShippingView?langId=-1&storeId={storeId}&catalogId={catalogId}",
		"Method=GET",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		"EncType=application/x-www-form-urlencoded",
		"LAST");

	lr_end_transaction("T27_TCPIShippingView", 2);

	TCPGetWishListForUsersViewBrowse();

}

# 1 "vuser_init.c" 2

# 1 "..\\..\\checkoutFunctions.c" 1
# 1 "..\\..\\checkoutWorkloadModel.c" 1
int CART_PAGE_DROP, LOGIN_PAGE_DROP, SHIP_PAGE_DROP, BILL_PAGE_DROP, REVIEW_PAGE_DROP;  
 
int RATIO_CHECKOUT_GUEST, RATIO_CHECKOUT_LOGIN, RATIO_CHECKOUT_LOGIN_FIRST;  
int OFFLINE_PLUGIN, VISA, MASTER, AMEX, PLCC, GIFT, DISCOVER, OFFLINE_PLUGIN;  
int RATIO_PROMO_APPLY, RATIO_PROMO_MULTIUSE, RATIO_PROMO_SINGLEUSE, RATIO_PROMO_COUPON_REMOVE, RATIO_REDEEM_LOYALTY_POINTS, RATIO_PROMO_APPLY_ALL;  
int RATIO_REGISTER, RATIO_UPDATE_QUANTITY, RATIO_DELETE_PROMOCODE, RATIO_DELETE_ITEM, RATIO_SELECT_COLOR, RATIO_ORDER_HISTORY, RATIO_POINTS_HISTORY, RATIO_RESERVATION_HISTORY, RATIO_RESERVATION_WISHLIST; 
int RATIO_CART_MERGE, RATIO_DROP_CART, RATIO_WISHLIST, cartMerge, currentCartSize; RATIO_BUILDCART_DRILLDOWN, USE_LOW_INVENTORY;
int	RATIO_WL_VIEW, RATIO_WL_CREATE,	RATIO_WL_DELETE, RATIO_WL_CHANGE, RATIO_WL_DELETE_ITEM, RATIO_WL_ADD_ITEM, RATIO_WL_ADD_TO_CART;
int MOVE_FROM_CART_TO_WISHLIST, MOVE_FROM_WISHLIST_TO_CART, RATIO_BUILDCART_LARGE;
int RATIO_ACCOUNT_OVERVIEW, RATIO_ACCOUNT_REWARDS, RATIO_ORDER_HISTORY, RATIO_RESERVATION_HISTORY, RATIO_ADDRESSBOOK, RATIO_PAYMENT, RATIO_PROFILE, RATIO_PREFERENCES, RATIO_LOGOUT, RATIO_FORGOT_PASSWORD,ONLINE_ORDER_SUBMIT, LOGINFIRSTCHECK; RATIO_ADDBIRTHDAYSAVINGS, RATIO_RTPS, RATIO_WIC, RATIO_ADDTOCART_PLP;
  
	 
	 
 	




	LOGIN_PAGE_DROP = 30;  
	SHIP_PAGE_DROP = 30;  
	BILL_PAGE_DROP = 27;  
	 
	 
	 
	RATIO_CHECKOUT_LOGIN = 90;  
	RATIO_CHECKOUT_GUEST = 10;  

	 
	RATIO_DROP_CART = 60;

     
    RATIO_CHECKOUT_LOGIN_FIRST = 50;

	 
	RATIO_ORDER_STATUS = 0;  

     
    RATIO_BUILDCART_DRILLDOWN = 100;  
	
	USE_LOW_INVENTORY = 5;
	RATIO_BUILDCART_LARGE = 0;  
	
	 
	OFFLINE_PLUGIN = 100;
	VISA = 0;
	MASTER = 0;
	AMEX = 0;
	PLCC = 0;
	GIFT = 0;
	DISCOVER = 0;
	PAYPAL = 0;

	 
 
 
 
	RATIO_PROMO_APPLY = 35;  
	RATIO_PROMO_MULTIUSE = 95;  
	RATIO_PROMO_SINGLEUSE = 5;  
	RATIO_PROMO_APPLY_ALL = 0;  
	RATIO_REDEEM_LOYALTY_POINTS = 15;
	
	 
	RATIO_DELETE_PROMOCODE = 0;

	 
	RATIO_CART_MERGE = 0;  

 
	RATIO_REGISTER = 100;

	 
	RATIO_UPDATE_QUANTITY = 100;

	 
	RATIO_DELETE_ITEM = 100;  

	 
	RATIO_SELECT_COLOR = 50;
	 
	 

	 
	 
 
	 


	 
	RATIO_WISHLIST		 = 50;  
 	







	RATIO_WL_CREATE      = 35;  
	RATIO_WL_DELETE      = 2; 
	RATIO_WL_CHANGE      = 3; 
	RATIO_WL_DELETE_ITEM = 5;
	RATIO_WL_ADD_ITEM    = 1; 
	RATIO_WL_ADD_TO_CART = 25;

	MOVE_FROM_CART_TO_WISHLIST = 25;
	MOVE_FROM_WISHLIST_TO_CART = 30;

	RATIO_ACCOUNT_OVERVIEW 		= 40;  

	RATIO_ACCOUNT_REWARDS 		= 5; 
	RATIO_ORDER_HISTORY			= 80;
	RATIO_RESERVATION_HISTORY	= 0;
	RATIO_ADDRESSBOOK			= 5;
	RATIO_PAYMENT				= 5;
	RATIO_PROFILE				= 5;
	RATIO_PREFERENCES			= 5;

	RATIO_ADDBIRTHDAYSAVINGS = 20;
	RATIO_LOGOUT			= 10;
	RATIO_FORGOT_PASSWORD	= 10;
	ONLINE_ORDER_SUBMIT		= 0;
	LOGINFIRSTCHECK			= 0;
 
	RATIO_RTPS = 0; 
	RATIO_WIC  = 0; 
	RATIO_ADDTOCART_PLP = 28;  
# 1 "..\\..\\checkoutFunctions.c" 2

int index , length , i, target_itemsInCart, index_buildCart, randomPercent, isLoggedIn;
int isLoggedIn=0;
char *searchString ;
char *nav_by ;  
char *drill_by ;  
char *sort_by;  
char *product_by;  
int atc_Stat = 0;  
int HttpRetCode;
int start_time, target_time;

 
int rNum;
unsigned short updateStatus;
char **colnames = 0;
char **rowdata = 0;
PVCI2 pvci = 0;

 
 
 
 
 

 
# 42 "..\\..\\checkoutFunctions.c"


void registerErrorCodeCheck()
{
	web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", "LAST");  
	web_reg_save_param("errorMessage", "LB=\"errorMessage\": \"", "RB=\",\n", "NotFound=Warning", "LAST");  
}

void getCatEntryID() {
 

	lr_save_string(lr_eval_string("{lowQty_SKU}"), "atc_catentryId");

	if ( strcmp(lr_eval_string("{atc_catentryId}"), lr_eval_string("{lastvalue}") ) == 0 ) {
		lr_param_sprintf ( "atc_catentryId" , "%s" , lr_paramarr_random ( "atc_catentryIds" ) ) ;
		USE_LOW_INVENTORY = 0;
	}
	else {
		lr_save_string(lr_eval_string("{atc_catentryId}"), "lastvalue");
		lr_save_string(lr_eval_string("{atc_catentryId}"), "atc_comment");
		lr_start_transaction("T20_Low_QTY_Count");
		lr_end_transaction("T20_Low_QTY_Count", 2);
	}

	return;
}

void addToCartFromPLP()
{
	int k;
	lr_think_time ( FORM_TT ) ;
	atc_Stat = 0;  

	if (CACHE_PRIME_MODE == 1)
		return;

	 
 




		lr_advance_param("pdpURLData");
		lr_save_string(lr_eval_string("{pdpProdID}"), "productId" );

		lr_start_transaction ( "T04_Product List Page Bag" ) ;

		lr_start_sub_transaction("T04_Product List Page Bag_getBundleIdAndBundleDetails" , "T04_Product List Page Bag");

		addHeader();
		web_add_header("productCatentryId", lr_eval_string("{productId}") );
		web_reg_find("TEXT/IC=recordSetTotalMatches", "SaveCount=apiCheck", "LAST");
		web_url("getBundleIdAndBundleDetails",
			"URL=https://{api_host}/tcpproduct/getBundleIdAndBundleDetails",
			"Resource=0",
			"RecContentType=application/json",
			"LAST");
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction ("T04_Product List Page Bag_getBundleIdAndBundleDetails", 1);
		else
			lr_end_sub_transaction ("T04_Product List Page Bag_getBundleIdAndBundleDetails", 2);

		lr_start_sub_transaction("T04_Product List Page Bag_getSKUInventoryandProductCounterDetails" , "T04_Product List Page Bag");
		addHeader();
		web_add_header("productId", lr_eval_string("{productId}"));
		web_reg_save_param("atc_catentryIds", "LB=catentryId\": \"", "RB=\",", "ORD=All", "NotFound=Warning", "LAST");  
		web_reg_save_param("atc_quantity"  ,  "LB=quantity\": \"", "RB=.0\",", "ORD=All", "NotFound=Warning", "LAST");  
		web_reg_find("TEXT/IC=getAllSKUInventoryByProductId", "SaveCount=apiCheck", "LAST");
		web_url("getSKUInventoryandProductCounterDetails",
			"URL=https://{api_host}/tcpproduct/getSKUInventoryandProductCounterDetails",
			"Resource=0",
			"RecContentType=application/json",
			"LAST");
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction ("T04_Product List Page Bag_getSKUInventoryandProductCounterDetails", 1);
		else
			lr_end_sub_transaction ("T04_Product List Page Bag_getSKUInventoryandProductCounterDetails", 2);

		lr_end_transaction ( "T04_Product List Page Bag" , 2 ) ;

		if ( lr_paramarr_len ( "atc_catentryIds" ) > 0 )
		{
			for (k = 1; k<= atoi(lr_eval_string("{atc_catentryIds_count}")); k++ )
			{
				if (atoi( lr_paramarr_idx("atc_quantity", k) ) != 0)
 
				{
					lr_save_string(lr_paramarr_idx("atc_catentryIds", k), "atc_catentryId" ); break;
				}
			}

			lr_start_transaction ( "T05_Add To Cart" ) ;

			lr_start_sub_transaction("T05_Add To Cart_addProductToCart" , "T05_Add To Cart");
			web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", "LAST");  
			web_reg_save_param("errorMessage", "LB=\"errorMessage\": \"", "RB=\",\n", "NotFound=Warning", "LAST");  
			web_reg_save_param_json("ParamName=orderId", "QueryString=$.orderId.*", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
			web_reg_save_param_json("ParamName=orderItemId", "QueryString=$.orderItemId.*", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
			addHeader();
			web_custom_request("addProductToCart",
				"URL=https://{api_host}/tcpproduct/addProductToCart",
				"Method=POST",
				"Resource=0",
				"RecContentType=application/json",
				"Mode=HTTP",
				"EncType=application/json",
				"Body={\"calculationUsage[]\":\"-7\",\"storeId\":\"{storeId}\",\"catalogId\":\"{catalogId}\",\"langId\":\"-1\",\"orderId\":\".\",\"field2\":\"0\",\"requesttype\":\"ajax\",\"catEntryId\":\"{atc_catentryId}\",\"quantity\":1}",
				"LAST");

			if ( strlen(lr_eval_string("{orderId}")) <= 0 ) {
				atc_Stat = 1;  
				lr_fail_trans_with_error( lr_eval_string("T05_Add To Cart_addProductToCart - Failed with  Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
				lr_end_sub_transaction ("T05_Add To Cart_addProductToCart", 1) ;
				lr_end_transaction ( "T05_Add To Cart" , 1 ) ;
				return;
			}  
			lr_end_sub_transaction ("T05_Add To Cart_addProductToCart", 2);

			addHeader();
			web_add_header("calc", "false");
 
 
			 
			web_add_header("locStore", "False");
			web_add_header("pageName", "orderSummary");
			web_reg_save_param("cartCount", "LB=cartCount\": ", "RB=,", "NotFound=Warning", "LAST");			 
			web_url("getOrderDetails",
				"URL=https://{api_host}/getOrderDetails",
				"Resource=1",
				"RecContentType=application/json",
				"LAST");

			lr_save_string("T05_Add To Cart", "mainTransaction");
			tcp_api2("tcporder/getAllCoupons", "GET",  lr_eval_string("{mainTransaction}") );
 

			if ( isLoggedIn == 1	) {
				tcp_api2("payment/getPointsService", "GET",  lr_eval_string("{mainTransaction}") );
			}

 

			lr_end_transaction ( "T05_Add To Cart" , 2 ) ;
			 

		}
		else
		{
			 
			atc_Stat = 1;  
			lr_start_transaction ( "T41_Add To Cart" ) ;
			lr_fail_trans_with_error( lr_eval_string("Romano T41_Add To Cart Failed with URL Code: \"{drillUrl}\"") ) ;
 
			lr_end_transaction ( "T41_Add To Cart" , 2 ) ;
			return;
		}
 







}

void addToCart()
{
	int k = 0, newTime, RANDOM_PERCENT = 0;
	atc_Stat = 0;  

	if (CACHE_PRIME_MODE == 1)
		return;

	 
	lr_think_time ( FORM_TT ) ;
 
# 230 "..\\..\\checkoutFunctions.c"
		lr_save_string(lr_eval_string("{randCatEntryId}"), "atc_catentryId" );
		web_reg_save_param_json("ParamName=orderId", "QueryString=$.orderId.*", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_save_param_json("ParamName=orderItemId", "QueryString=$.orderItemId.*", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_find("Text=the products you wish to purchase are not available", "SaveCount=atcErrorFound");
		web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", "LAST");  
		web_reg_save_param("errorMessage", "LB=\"errorMessage\": \"", "RB=\",\n", "NotFound=Warning", "LAST");  

		lr_start_transaction ( "T05_Add To Cart" ) ;

		lr_start_sub_transaction("T05_Add To Cart_addProductToCart" , "T05_Add To Cart");
		addHeader();
		web_custom_request("addProductToCart",
			"URL=https://{api_host}/tcpproduct/addProductToCart",
			"Method=POST",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTML",
			"EncType=application/json",
			"Body={\"calculationUsage[]\":\"-7\",\"storeId\":\"{storeId}\",\"catalogId\":\"{catalogId}\",\"langId\":\"-1\",\"orderId\":\".\",\"field2\":\"0\",\"requesttype\":\"ajax\",\"catEntryId\":\"{atc_catentryId}\",\"quantity\":1}",
			"LAST");
 







		if ( strlen(lr_eval_string("{orderId}")) <= 0 ) {
			atc_Stat = 1;  
			lr_fail_trans_with_error( lr_eval_string("T05_Add To Cart_addProductToCart - Failed with  Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_sub_transaction ("T05_Add To Cart_addProductToCart", 1) ;
			lr_end_transaction ( "T05_Add To Cart" , 1 ) ;
			return;
		}  

		lr_end_sub_transaction ("T05_Add To Cart_addProductToCart", 2) ;

		lr_start_sub_transaction ("T05_Add To Cart_getOrderDetails","T05_Add To Cart");
						




			addHeader();
 
 
			 
			web_add_header("locStore", "False");
			web_add_header("pageName", "orderSummary");
			web_reg_save_param("cartCount", "LB=cartCount\": ", "RB=,", "NotFound=Warning", "LAST");			 
			web_url("getOrderDetails",
				"URL=https://{api_host}/getOrderDetails",
				"Resource=0",
				"RecContentType=application/json",
				"LAST");
			lr_end_sub_transaction ("T05_Add To Cart_getOrderDetails",2);

 

		lr_save_string("T05_Add To Cart", "mainTransaction");
		tcp_api2("tcporder/getAllCoupons", "GET",  lr_eval_string("{mainTransaction}") );
 

			if ( isLoggedIn == 1	) {
				tcp_api2("payment/getPointsService", "GET",  lr_eval_string("{mainTransaction}") );
			}
 

		lr_end_transaction ( "T05_Add To Cart" , 2 ) ;
		 
 
# 315 "..\\..\\checkoutFunctions.c"
}  

void addToCartNoBrowse()
{
 
	atc_Stat = 0;  
	 
	lr_think_time ( FORM_TT ) ;

	if (CACHE_PRIME_MODE == 1)
		return;


 
		lr_save_string(lr_eval_string("{randCatEntryId}"), "atc_catentryId" );

		web_reg_save_param_json("ParamName=orderId", "QueryString=$.orderId.*", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_save_param_json("ParamName=orderItemId", "QueryString=$.orderItemId.*", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_find("Text=the products you wish to purchase are not available", "SaveCount=atcErrorFound");
		web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", "LAST");  
		web_reg_save_param("errorMessage", "LB=\"errorMessage\": \"", "RB=\",\n", "NotFound=Warning", "LAST");  

		lr_start_transaction ( "T05_Add To Cart" ) ;

		lr_start_sub_transaction("T05_Add To Cart_addProductToCart" , "T05_Add To Cart");
		addHeader();
		web_custom_request("addProductToCart",
			"URL=https://{api_host}/tcpproduct/addProductToCart",
			"Method=POST",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTML",
			"EncType=application/json",
			"Body={\"calculationUsage[]\":\"-7\",\"storeId\":\"{storeId}\",\"catalogId\":\"{catalogId}\",\"langId\":\"-1\",\"orderId\":\".\",\"field2\":\"0\",\"requesttype\":\"ajax\",\"catEntryId\":\"{atc_catentryId}\",\"quantity\":1}",
			"LAST");

		if ( strlen(lr_eval_string("{orderId}")) <= 0 ) {
			atc_Stat = 1;  
			lr_fail_trans_with_error( lr_eval_string("T05_Add To Cart_addProductToCart - Failed with  Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_sub_transaction ("T05_Add To Cart_addProductToCart", 1) ;
			lr_end_transaction ( "T05_Add To Cart" , 1 ) ;
			return;
		}  

		lr_end_sub_transaction ("T05_Add To Cart_addProductToCart", 2) ;
 			lr_start_sub_transaction ("T05_Add To Cart_getOrderDetails","T05_Add To Cart");
# 389 "..\\..\\checkoutFunctions.c"
						




			addHeader();
 
 
			 
			web_add_header("locStore", "False");
			web_add_header("pageName", "orderSummary");
			web_reg_save_param("cartCount", "LB=cartCount\": ", "RB=,", "NotFound=Warning", "LAST");			 
			web_url("getOrderDetails",
				"URL=https://{api_host}/getOrderDetails",
				"Resource=0",
				"RecContentType=application/json",
				"LAST");
			lr_end_sub_transaction ("T05_Add To Cart_getOrderDetails",2);

 

		lr_save_string("T05_Add To Cart", "mainTransaction");
		tcp_api2("tcporder/getAllCoupons", "GET",  lr_eval_string("{mainTransaction}") );
 
			if ( isLoggedIn == 1	) {
				tcp_api2("payment/getPointsService", "GET",  lr_eval_string("{mainTransaction}") );
			}
 

		lr_end_transaction ( "T05_Add To Cart" , 2 ) ;
		 

}  

void addToCartMixed()
{
	int k = 0, newTime, RANDOM_PERCENT = 0;
	atc_Stat = 0;  

	if (CACHE_PRIME_MODE == 1)
		return;


	if ( lr_paramarr_len ( "atc_catentryIds" ) > 0 ) {  

		lr_param_sprintf ( "atc_catentryId" , "%s" , lr_paramarr_random ( "atc_catentryIds" ) ) ;
		web_reg_save_param ( "orderId" , "LB=\"orderId\": [\"" , "RB=\"]" , "NotFound=Warning",  "LAST" ) ;
		web_reg_save_param ( "orderItemId" , "LB=\"orderItemId\": [\"" , "RB=\"]" , "NotFound=Warning", "LAST" ) ;
		web_reg_find("Text=the products you wish to purchase are not available", "SaveCount=atcErrorFound");

		lr_think_time ( FORM_TT ) ;

	    if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= BOPIS_CART_RATIO && strcmp( lr_eval_string("{storeId}") , "10151") == 0)
		{
			if (pickupInStore() == 0)
			{
				if ( addBopisToCart() == 0)
				{
					atc_Stat = 0;  
					lr_save_string("true", "addBopisToCart");
				} else
					atc_Stat = 1;  

			}
		} else {
			if (strcmp( lr_eval_string("{storeId}") , "10151") == 0)
				lr_save_string("true", "addEcommToCart");

			productDisplay();
			addToCart();
		}
	}

}

void parseOrderItemId() {  

    extern char * strtok(char * string, const char * delimiters );
    char path[1000] = "";
    char separators[] = "\"";
    char * token;
    char fullpath[1024];
    int counter = 0;
    strcpy(path, lr_eval_string("{unavailId_1}"));

     

    token = (char *)strtok(path, separators);
    if (!token) {
        lr_output_message ("No tokens found in string!");
    }
 
    while (token != 0 ) {
 
 
        token = (char *)strtok(0, separators);

        if(token != 0) {
	        if (strlen(token) > 10) {
	        	counter++;
	        	lr_param_sprintf ("count", "%d", counter);
	        	lr_save_string(token, lr_eval_string("upcId{count}") );
	        }
        }
    }
	return;
}


void getOutOfStockItemIds(){

	int i;

	if (atoi(lr_eval_string("{unavailId_count}")) > 0) {

 
# 537 "..\\..\\checkoutFunctions.c"
		web_reg_save_param("orderItemId", "LB=id=\"{unavailId_1}\" data-item-id=\"", "RB=\">", "NotFound=Warning", "LAST");

		web_url("OrderCalculate",
		"URL=https://{host}/shop/OrderCalculate?calculationUsageId=-1&updatePrices=1&catalogId={catalogId}&errorViewName=AjaxCheckoutDisplayView&orderId=.&langId=-1&storeId={storeId}&URL=AjaxCheckoutDisplayView",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		"LAST");

		removeOutOfStockItem();

	}
	return;
}


int updateQuantity()
{
	lr_think_time ( FORM_TT ) ;

	if (atoi( lr_eval_string("{itemCatentryId_count}")) > 0 )  
	{
		index = rand ( ) % lr_paramarr_len( "itemCatentryId" ) + 1 ;

		lr_save_string ( lr_paramarr_idx( "itemCatentryId" , index ) , "itemCatentryId" ) ;
		lr_save_string ( lr_paramarr_idx( "productId" , index ) , "productId" ) ;
		lr_save_string ( lr_paramarr_idx( "orderItemIds" , index ) , "orderItemId" ) ;

		 

		lr_start_transaction ( "T09_Update Quantity" ) ;

		lr_start_sub_transaction("T09_Update Quantity_S01_getSwatchesAndSizeInfo", "T09_Update Quantity");

				# 583 "..\\..\\checkoutFunctions.c"



		web_add_header("catalogId", lr_eval_string("{catalogId}") );
		web_add_header("storeId", lr_eval_string("{storeId}") );
		web_add_header("productId", lr_eval_string("{productId}") );
		web_add_header("langId", "-1");
		web_custom_request("getSwatchesAndSizeInfo",
			"URL=https://{api_host}/getSwatchesAndSizeInfo",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			"LAST");
		lr_end_sub_transaction("T09_Update Quantity_S01_getSwatchesAndSizeInfo", 2);

		lr_start_sub_transaction("T09_Update Quantity_S02_getSKUDetails", "T09_Update Quantity");

				# 611 "..\\..\\checkoutFunctions.c"


		web_add_header("catalogId", lr_eval_string("{catalogId}") );
		web_add_header("storeId", lr_eval_string("{storeId}") );
		web_add_header("productId", lr_eval_string("{productId}") );
		web_add_header("langId", "-1");
		web_custom_request("getSKUDetails",
			"URL=https://{api_host}/getSKUDetails",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			"LAST");
		lr_end_sub_transaction("T09_Update Quantity_S02_getSKUDetails", 2);

		lr_start_sub_transaction("T09_Update Quantity_S03_updateOrderItem", "T09_Update Quantity");

				# 638 "..\\..\\checkoutFunctions.c"


		web_add_header("Content-Type", "application/json");
		web_add_header("storeId", lr_eval_string("{storeId}") );
		web_add_header("catalogId", lr_eval_string("{catalogId}") );
		web_add_header("langId", "-1");
		registerErrorCodeCheck();
		web_custom_request("Quantity_S01_updateOrderItem",
			"URL=https://{api_host}/updateOrderItem",
			"Method=PUT",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTML",
			"Body={\"orderItem\":[{\"orderItemId\":\"{orderItemId}\",\"xitem_catEntryId\":\"{itemCatentryId}\",\"quantity\":\"2\"}]}",
			"LAST");
		lr_end_sub_transaction("T09_Update Quantity_S03_updateOrderItem", 2);

		lr_start_sub_transaction ("T09_Update Quantity_S04_getOrderDetails", "T09_Update Quantity" ) ;
				


		addHeader();
		web_add_header("locStore", "True");
		web_add_header("pageName", "fullOrderInfo");
		web_add_header("calc", "true");
		web_reg_save_param("piAmountonUpdateQuantity", "LB=\"piAmount\": \"", "RB=\"", "NotFound=Warning", "LAST");  
		web_custom_request("Quantity_S02_getOrderDetails",
			"URL=https://{api_host}/getOrderDetails",
			"Method=GET",
			"Resource=0",
			"RecContentType=text/html",
			"Snapshot=t28.inf",
			"Mode=HTML",
			"EncType=application/x-www-form-urlencoded",
			"LAST");
		lr_end_sub_transaction ("T09_Update Quantity_S04_getOrderDetails", 2) ;
		 

		if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 )  
		{
			lr_end_transaction ( "T09_Update Quantity" , 0) ;
			return 0;
		}
		else
		{
			lr_fail_trans_with_error( lr_eval_string("T09_Update Quantity Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_transaction ( lr_eval_string ( "T09_Update Quantity" ) , 1 ) ;
			return 1;
		}
	}
	return 0;
}  


void viewCart()
{

	if (CACHE_PRIME_MODE == 1)
		return;

	lr_think_time ( LINK_TT ) ;
	lr_start_transaction ( "T06_View_Cart" ) ;

	 
	 

	lr_start_sub_transaction ( "T06_View_Cart_S01_MyBag", "T06_View_Cart" );
	web_custom_request("bag",
		"URL=https://{host}/{country}/bag",
		"Method=GET",
		"Resource=0",
		"Mode=HTTP",
		"LAST");
	lr_end_sub_transaction("T06_View_Cart_S01_MyBag", 2);

 

	lr_start_sub_transaction ( "T06_View_Cart_S02_GetOrderDetails", "T06_View_Cart" );
		

	web_reg_save_param("itemQuantity", "LB=\"qty\": ", "RB=,", "NotFound=Warning", "Ord=All", "LAST");  
	web_reg_save_param("itemCatentryId", "LB=\"itemCatentryId\": ", "RB=,", "NotFound=Warning", "Ord=All", "LAST");  
	web_reg_save_param("productId", "LB=\"productId\": \"", "RB=\",", "NotFound=Warning", "Ord=All", "LAST");  
	web_reg_save_param("cartCount", "LB=\"cartCount\": ", "RB=,", "NotFound=Warning", "LAST");  
	web_reg_save_param("orderItemIds", "LB=\"orderItemId\": ", "RB=,", "NotFound=Warning", "Ord=All", "LAST");  
	web_reg_save_param("piAmount", "LB=\"piAmount\": \"", "RB=\",", "NotFound=Warning", "LAST");  
	web_reg_save_param("couponExist", "LB=\"code\": \"", "RB=\",", "NotFound=Warning", "LAST");  
	web_reg_save_param("orderId", "LB=\"subOrderId\": \"", "RB=\"", "NotFound=Warning", "LAST");  

	addHeader();
	web_add_header("locStore", "True");
	web_add_header("pageName", "fullOrderInfo");
	web_add_header("calc", "false");
	web_reg_find("TEXT/IC={","SaveCount=apiCheck", "LAST");
	web_custom_request("getOrderDetails",
		"URL=https://{api_host}/getOrderDetails",
		"Method=GET",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		"LAST");
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T06_View_Cart_S02_GetOrderDetails", 1);
	else
		lr_end_sub_transaction("T06_View_Cart_S02_GetOrderDetails", 2);

	lr_start_sub_transaction ( "T06_View_Cart_S03_getRegisteredUserDetailsInfo", "T06_View_Cart" );
		

	addHeader();
	web_reg_save_param ( "addressId" , "LB=\"addressId\": \"" , "RB=\",\n" , "ORD=1", "NotFound=Warning", "LAST" ) ;  
	web_reg_save_param ( "addressIdAll" , "LB=\"addressId\": \"" , "RB=\",\n" , "ORD=ALL", "NotFound=Warning", "LAST" ) ;  
	web_reg_find("TEXT/IC=resourceName", "SaveCount=apiCheck", "LAST");
	web_custom_request("getRegisteredUserDetailsInfo",
		"URL=https://{api_host}/payment/getRegisteredUserDetailsInfo",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T06_View_Cart_S03_getRegisteredUserDetailsInfo", 1);
	else
		lr_end_sub_transaction("T06_View_Cart_S03_getRegisteredUserDetailsInfo", 2);
 
# 784 "..\\..\\checkoutFunctions.c"

	lr_save_string("T06_View_Cart","mainTransaction");
	tcp_api2("tcpproduct/getPriceDetails", "GET",  lr_eval_string("{mainTransaction}") );

	lr_start_sub_transaction ( "T06_View_Cart_S05_getAllCoupons", "T06_View_Cart" );
		


	addHeader();
	web_reg_find("TEXT/IC=appliedCoupons", "SaveCount=apiCheck", "LAST");
	web_custom_request("getAllCoupons",
		"URL=https://{api_host}/tcporder/getAllCoupons",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T06_View_Cart_S05_getAllCoupons", 1);
	else
		lr_end_sub_transaction("T06_View_Cart_S05_getAllCoupons", 2);

	if (ESPOT_FLAG == 1)
	{
		lr_start_sub_transaction ( "T06_View_Cart_S08_getESpot", "T06_View_Cart" );

				


		addHeader();
		web_add_header("espotName","GlobalHeaderBannerAboveHeader,GlobalHeaderBannerWithinHeader,GlobalHeaderLogo,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,create_account_drawer,MinicartLoginBanner,MinicartRegisterBanner,drawer_myaccount_overview_banner,coupon_help,bag_empty_banner,drawer_empty_bag,MinicartLoginFromFavoritesBanner,bag_ledger_promo_banner,gift_services_espot,CheckoutConfirmationBanner,CheckoutConfirmationMPRPromo,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1");
		web_add_header("deviceType","desktop");
	 
		web_reg_find("TEXT/IC=espotName", "SaveCount=apiCheck", "LAST");
		web_custom_request("getESpots",
			"URL=https://{api_host}/getESpot",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			"LAST");
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction("T06_View_Cart_S08_getESpot", 1);
		else
			lr_end_sub_transaction("T06_View_Cart_S08_getESpot", 2);
	}

	if (isLoggedIn == 1)
	{
		api_getESpot_second();  
 
# 868 "..\\..\\checkoutFunctions.c"
	}

	 
	 

	 

	lr_end_transaction ( "T06_View_Cart" , 0 ) ;

}  

void viewCartFromLogin()
{
	 

 

}  

void applyPromoCode(int useRewards)  
{
	int couponCodeCount = 1;
	int couponLoop = 1;
	lr_think_time ( FORM_TT ) ;

	if ( useRewards == 1 ){
		lr_save_string( lr_eval_string ( "{promocodeRewards}" ) , "promocode" ) ;
 
		lr_save_string( "Rewards" , "promotype" ) ;
	}
	else if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_PROMO_APPLY_ALL ) {
		 
		lr_save_string( "Apply All" , "promotype" ) ;
	}
	else if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_PROMO_MULTIUSE ) {
		lr_save_string( lr_eval_string ( "{multiusePromoCode}" ) , "promocode" ) ;
		lr_save_string( "Multi-Use" , "promotype" ) ;
		MULTI_USE_COUPON_FLAG = 1;
	}  
	else if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= (RATIO_PROMO_SINGLEUSE + RATIO_PROMO_MULTIUSE) ) {

		getPromoCode();

		if ( strcmp(lr_eval_string("{promocode}"), "") == 0 )
			lr_save_string( lr_eval_string ( "{multiusePromoCode}" ) , "promocode" ) ;

		lr_save_string( "Single-Use" , "promotype" ) ;
	}
 





	for (couponLoop = 1; couponLoop <= couponCodeCount; couponLoop++)
	{
		if (couponCodeCount > 1)
		{
			lr_save_string( lr_paramarr_idx("couponCodes", couponLoop) , "promocode" ) ;
			lr_save_string( lr_eval_string("Y{promocode}") , "promocode" ) ;
		}

		 

		lr_start_transaction ( lr_eval_string ( "T07_Enter Promo {promotype}" ) ) ;

		lr_start_sub_transaction ( "T07_Enter Promo_S01_Coupons" , lr_eval_string ("T07_Enter Promo {promotype}"));
				# 946 "..\\..\\checkoutFunctions.c"


		registerErrorCodeCheck();
		addHeader();
		web_custom_request("coupons",
			"URL=https://{api_host}/payment/coupons",
			"Method=POST",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTTP",
			"EncType=application/json",
			"Body={\"storeId\":\"{storeId}\",\"langId\":\"-1\",\"catalogId\":\"{catalogId}\",\"URL\":\"\",\"promoCode\":\"{promocode}\",\"requesttype\":\"ajax\",\"fromPage\":\"shoppingCartDisplay\",\"taskType\":\"A\"}",
			"LAST");
		lr_end_sub_transaction ("T07_Enter Promo_S01_Coupons", 2)	;

 
 

		lr_start_sub_transaction ( "T07_Enter Promo_S02_getAllCoupons" , lr_eval_string ("T07_Enter Promo {promotype}"));
		addHeader();
		web_reg_find("TEXT/IC=appliedCoupons", "SaveCount=apiCheck", "LAST");
		web_custom_request("getAllCoupons",
			"URL=https://{api_host}/tcporder/getAllCoupons",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			"LAST");
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction ("T07_Enter Promo_S02_getAllCoupons", 1)	;
		else
			lr_end_sub_transaction ("T07_Enter Promo_S02_getAllCoupons", 2)	;

		lr_start_sub_transaction ( "T07_Enter Promo_S03_getOrderDetails", lr_eval_string ("T07_Enter Promo {promotype}") );
 




		addHeader();
		web_add_header("calc", "true");    
		web_add_header("locStore", "True");
		web_add_header("pageName", "fullOrderInfo");
		web_url("getOrderDetails",
			"URL=https://{api_host}/getOrderDetails",
			"Resource=0",
			"RecContentType=application/json",
			"LAST");
		lr_end_sub_transaction ( "T07_Enter Promo_S03_getOrderDetails", 2 );

		lr_start_sub_transaction ( "T07_Enter Promo_S03_getPointsService", lr_eval_string ("T07_Enter Promo {promotype}") );
 




		addHeader();
		web_url("getPointsService",
			 "URL=https://{api_host}/payment/getPointsService",
			 "Resource=0",
			 "RecContentType=application/json",
			 "LAST");
		lr_end_sub_transaction("T07_Enter Promo_S03_getPointsService", 2);


		 

		if ( strcmp( lr_eval_string("{errorCode}") ,"") != 0 && strcmp(lr_eval_string("{errorMessage}") ,"") != 0 )  
		{
			lr_fail_trans_with_error( lr_eval_string("T07_Enter Promo {promotype} Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_transaction ( lr_eval_string ( "T07_Enter Promo {promotype}" ) , 1 ) ;
		}
		else {
			lr_end_transaction ( lr_eval_string ( "T07_Enter Promo {promotype}" ) , 0 ) ;
			break;
		}
	}

}  


int deletePromoCode()
{
	if ( strcmp( lr_eval_string("{promocode}") , "" ) != 0 )
	{
		 
		lr_start_transaction ( "T07_Delete Promocode" ) ;

		lr_start_sub_transaction ( "T07_Delete Promocode S01_removePromotionCode" , "T07_Delete Promocode" );
				# 1045 "..\\..\\checkoutFunctions.c"


		web_add_header("promoCode", lr_eval_string("{couponExist}") );   
		addHeader();
		registerErrorCodeCheck();
		web_custom_request("removePromotionCode",
			"URL=https://{api_host}/payment/removePromotionCode",
			"Method=POST",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTTP",
			"EncType=",
			"LAST");
		lr_end_sub_transaction ( "T07_Delete Promocode S01_removePromotionCode" , 2 );

	lr_save_string("T07_Delete Promocode","mainTransaction");
	tcp_api2("tcpoproduct/getPriceDetails", "GET",  lr_eval_string("{mainTransaction}") );

 
# 1072 "..\\..\\checkoutFunctions.c"
		lr_start_sub_transaction ( "T07_Delete Promocode S03_getOrderDetails", "T07_Delete Promocode" );
 




		addHeader();
		web_add_header("calc", "true");
		web_add_header("locStore", "True");
		web_add_header("pageName", "fullOrderInfo");
		web_url("getOrderDetails",
			"URL=https://{api_host}/getOrderDetails",
			"Resource=0",
			"RecContentType=application/json",
			"LAST");
		lr_end_sub_transaction ( "T07_Delete Promocode S03_getOrderDetails", 2 );

		 

		if ( strcmp( lr_eval_string("{errorCode}") ,"") != 0 && strcmp(lr_eval_string("{errorMessage}") ,"") != 0 )  
		{
			lr_fail_trans_with_error( lr_eval_string("T07_Delete Promocode Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_transaction ( "T07_Delete Promocode" , 1 ) ;
			return 1;
		}
		else {
			lr_end_transaction ( "T07_Delete Promocode" , 0 ) ;
			return 0;
		}
	}
	return 0;
}  

int deleteItem()    
{

	if ( lr_paramarr_len ( "orderItemIds" ) > 0 ){
		lr_think_time ( LINK_TT ) ;
		 

		lr_start_transaction ("T06_Cart_Item_Remove") ;

		lr_start_sub_transaction ( "T06_Cart_Item_Remove_S01_updateMultiSelectItemsToRemove", "T06_Cart_Item_Remove" );

				# 1127 "..\\..\\checkoutFunctions.c"


		web_add_header("Content-Type", "application/json");
		web_add_header("storeId", lr_eval_string("{storeId}") );
		web_add_header("catalogId", lr_eval_string("{catalogId}") );
		web_add_header("langId", "-1");
		registerErrorCodeCheck();
		lr_save_string ( lr_paramarr_random( "orderItemIds" ) , "orderItemId" ) ;
		web_custom_request("updateMultiSelectItemsToRemove",
			"URL=https://{api_host}/updateMultiSelectItemsToRemove",
			"Method=PUT",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTTP",
			"Body={\"orderItem\":[{\"orderItemId\":\"{orderItemId}\",\"quantity\":\"0\"}]}",
			"LAST");
		lr_end_sub_transaction("T06_Cart_Item_Remove_S01_updateMultiSelectItemsToRemove", 2);

		lr_start_sub_transaction ( "T06_Cart_Item_Remove_S02_getPointsService", "T06_Cart_Item_Remove" );
 




		addHeader();
		web_url("getPointsService",
			 "URL=https://{api_host}/payment/getPointsService",
			 "Resource=0",
			 "RecContentType=application/json",
			 "LAST");
		lr_end_sub_transaction("T06_Cart_Item_Remove_S02_getPointsService", 2);

		lr_start_sub_transaction ( "T06_Cart_Item_Remove_S03_GetOrderDetails", "T06_Cart_Item_Remove" );
 




		addHeader();
		web_add_header("calc", "true");    
		web_add_header("locStore", "True");
		web_add_header("pageName", "fullOrderInfo");

		web_url("getOrderDetails",
			"URL=https://{api_host}/getOrderDetails",
			"Resource=0",
			"RecContentType=application/json",
			"LAST");
		lr_end_sub_transaction("T06_Cart_Item_Remove_S03_GetOrderDetails", 2);

	lr_start_sub_transaction ( "T06_Cart_Item_Remove_S04_getAllCoupons", "T06_Cart_Item_Remove" );
		addHeader();
		web_url("getAllCoupons",
			"URL=https://{api_host}/tcporder/getAllCoupons",
			"Resource=0",
			"RecContentType=application/json",
			"LAST");
		lr_end_sub_transaction ( "T06_Cart_Item_Remove_S04_getAllCoupons" , 2 );

		 

		if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 )  
		{	lr_end_transaction ("T06_Cart_Item_Remove" , 0 ) ;
			return 0;
		}
		else
		{
			lr_fail_trans_with_error( lr_eval_string("T06_Cart_Item_Remove Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_transaction ( lr_eval_string ( "T06_Cart_Item_Remove" ) , 1 ) ;
			return 1;
		}

	}
	return 0;
}  


void Submit_Pickup_Detail()
{

	if (strcmp( lr_eval_string("{addBopisToCart}"), "true") == 0 )
	{
		lr_think_time ( FORM_TT ) ;

		lr_start_transaction ( "T11_Submit_Pickup_Detail") ;

 

		web_reg_save_param ( "addressId" , "LB=\"addressId\": \"" , "RB=\",\n" , "ORD=1", "NotFound=Warning", "LAST" ) ;  

				


		addHeader();

		lr_start_sub_transaction ("T11_Submit_Pickup_Detail S01_addAddress", "T11_Submit_Pickup_Detail" ) ;
		web_custom_request("addAddress",
			"URL=https://{api_host}/payment/addAddress",
			"Method=POST",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTTP",
			"EncType=application/json", "Body={\"contact\":[{\"addressType\":\"Shipping\",\"firstName\":\"Joe\",\"lastName\":\"User\",\"phone2\":\"2014534613\",\"email1\":\"bitrogue@mailinator.com\",\"email2\":\"\"}]}",
			"LAST");
		lr_end_sub_transaction ("T11_Submit_Pickup_Detail S01_addAddress", 2) ;
 

		lr_start_sub_transaction ("T11_Submit_Pickup_Detail S02_getOrderDetails", "T11_Submit_Pickup_Detail" ) ;
 




		addHeader();
		web_add_header("calc", "true");
		web_add_header("locStore", "True");
		web_add_header("pageName", "fullOrderInfo");
		web_reg_find("TEXT/IC={", "SaveCount=apiCheck", "LAST");
		web_url("getOrderDetails",
			"URL=https://{api_host}/getOrderDetails",
			"TargetFrame=",
			"Resource=0",
			"RecContentType=application/json",
			"LAST");
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction ("T11_Submit_Pickup_Detail S02_getOrderDetails", 1) ;
		else
			lr_end_sub_transaction ("T11_Submit_Pickup_Detail S02_getOrderDetails", 2) ;


		lr_start_sub_transaction ("T11_Submit_Pickup_Detail S03_getShipmentMethods", "T11_Submit_Pickup_Detail" ) ;
 
 
# 1270 "..\\..\\checkoutFunctions.c"
		addHeader();
		web_add_header("state", lr_eval_string("{savedState}"));
		web_add_header("zipCode", lr_eval_string("{savedZipCode}"));
		web_add_header("addressField1", lr_eval_string("{savedAddressLine}"));

		web_reg_find("TEXT/IC=jsonArr", "SaveCount=apiCheck", "LAST");
		web_url("getShipmentMethods",
			"URL=https://{api_host}/payment/getShipmentMethods",
			"TargetFrame=",
			"Resource=0",
			"RecContentType=application/json",
			"LAST");
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction ("T11_Submit_Pickup_Detail S03_getShipmentMethods", 1) ;
		else
			lr_end_sub_transaction ("T11_Submit_Pickup_Detail S03_getShipmentMethods", 2) ;

		lr_end_transaction ( "T11_Submit_Pickup_Detail" , 0) ;

	}

}

int proceedToCheckout_ShippingView()  
{
	lr_think_time ( LINK_TT ) ;
	 

	lr_start_transaction ( "T09_Proceed_To_Checkout Registered" ) ;

 

	lr_start_sub_transaction ("T09_Proceed_To_Checkout_getUnqualifiedItems", "T09_Proceed_To_Checkout Registered" ) ;
		# 1314 "..\\..\\checkoutFunctions.c"


	addHeader();
	web_custom_request("getUnqualifiedItems",
		"URL=https://{api_host}/tcporder/getUnqualifiedItems",
		"Method=GET",
		"Resource=0",
		"Mode=HTML",
		"LAST");
	lr_end_sub_transaction ("T09_Proceed_To_Checkout_getUnqualifiedItems", 2) ;

	lr_start_sub_transaction ("T09_Proceed_To_Checkout shipping", "T09_Proceed_To_Checkout Registered" ) ;
	web_custom_request("checkout",
 
		"URL=https://{host}/{country}/checkout/shipping/",
		"Method=GET",
		"Resource=0",
		"Mode=HTML",
		"LAST");
	lr_end_sub_transaction ("T09_Proceed_To_Checkout shipping", 2) ;

	lr_start_sub_transaction ("T09_Proceed_To_Checkout_getOrderDetails", "T09_Proceed_To_Checkout Registered" ) ;
 




	addHeader();
	web_add_header("locStore", "True");
	web_add_header("pageName", "fullOrderInfo");
	web_reg_find("TEXT/IC={", "SaveCount=apiCheck", "LAST");
	web_reg_save_param("cartCount", "LB=cartCount\": ", "RB=,", "NotFound=Warning", "LAST");

	web_custom_request("getOrderDetails",
		"URL=https://{api_host}/getOrderDetails",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction ("T09_Proceed_To_Checkout_getOrderDetails", 1) ;
	else
		lr_end_sub_transaction ("T09_Proceed_To_Checkout_getOrderDetails", 2) ;

	lr_start_sub_transaction ("T09_Proceed_To_Checkout_getRegisteredUserDetailsInfo", "T09_Proceed_To_Checkout Registered" ) ;
 




	addHeader();
	web_reg_find("TEXT/IC=resourceName", "SaveCount=apiCheck", "LAST");
	web_custom_request("getRegisteredUserDetailsInfo",
		"URL=https://{api_host}/payment/getRegisteredUserDetailsInfo",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction ("T09_Proceed_To_Checkout_getRegisteredUserDetailsInfo", 1) ;
	else
		lr_end_sub_transaction ("T09_Proceed_To_Checkout_getRegisteredUserDetailsInfo", 2) ;
 



	lr_start_sub_transaction ("T09_Proceed_To_Checkout_getAllCoupons", "T09_Proceed_To_Checkout Registered" ) ;
	addHeader();
	web_reg_find("TEXT/IC=appliedCoupons", "SaveCount=apiCheck", "LAST");
	web_custom_request("getAllCoupons",
		"URL=https://{api_host}/tcporder/getAllCoupons",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction ("T09_Proceed_To_Checkout_getAllCoupons", 1) ;
	else
		lr_end_sub_transaction ("T09_Proceed_To_Checkout_getAllCoupons", 2) ;

 





 
# 1413 "..\\..\\checkoutFunctions.c"
	 
	 

	lr_start_sub_transaction ("T09_Proceed_To_Checkout_giftOptionsCmd", "T09_Proceed_To_Checkout Registered" ) ;
 




	addHeader();
	web_reg_find("TEXT/IC=giftOptions", "SaveCount=apiCheck", "LAST");
	web_custom_request("giftOptionsCmd",
		"URL=https://{api_host}/payment/giftOptionsCmd",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction ("T09_Proceed_To_Checkout_giftOptionsCmd", 1) ;
	else
		lr_end_sub_transaction ("T09_Proceed_To_Checkout_giftOptionsCmd", 2) ;

	if (ESPOT_FLAG == 1)
	{
		lr_start_sub_transaction ( "T09_Proceed_To_Checkout_getESpots", "T09_Proceed_To_Checkout Registered" );

				


		addHeader();
		web_add_header("espotName","GlobalHeaderBannerAboveHeader,GlobalHeaderBannerWithinHeader,GlobalHeaderLogo,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,create_account_drawer,MinicartLoginBanner,MinicartRegisterBanner,drawer_myaccount_overview_banner,coupon_help,bag_empty_banner,drawer_empty_bag,MinicartLoginFromFavoritesBanner,bag_ledger_promo_banner,gift_services_espot,CheckoutConfirmationBanner,CheckoutConfirmationMPRPromo,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1");
		web_add_header("deviceType","desktop");

	 
		web_reg_find("TEXT/IC=espotName", "SaveCount=apiCheck", "LAST");
		web_custom_request("getESpots",
			"URL=https://{api_host}/getESpot",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			"LAST");
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction("T09_Proceed_To_Checkout_getESpots", 1);
		else
			lr_end_sub_transaction("T09_Proceed_To_Checkout_getESpots", 2);

		api_getESpot_second();
	}

	lr_start_sub_transaction ("T09_Proceed_To_Checkout_getAddressFromBook", "T09_Proceed_To_Checkout Registered" ) ;

	web_reg_save_param("savedaddressId", "LB=addressLine\": [\r\n\"", "RB=\"", "ORD=1","NotFound=Warning", "LAST");
 
	web_reg_save_param("savedState", "LB=state\": \"", "RB=\"", "NotFound=Warning", "LAST");
	web_reg_save_param("savedZipCode", "LB=zipCode\": \"", "RB=\"", "NotFound=Warning", "LAST");
	web_reg_save_param_json( "ParamName=savedAddressLine", "QueryString=$.contact[0]...addressLine[0].", "SelectAll=NO", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
 
 

	addHeader();
	web_custom_request("getAddressFromBook",
		"URL=https://{api_host}/payment/getAddressFromBook",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	lr_end_sub_transaction ("T09_Proceed_To_Checkout_getAddressFromBook", 2) ;
 

	lr_start_sub_transaction ( "T09_Proceed_To_Checkout_getCreditCardDetails", "T09_Proceed_To_Checkout Registered" );

	addHeader();
	web_add_header("isRest", "true" );
	web_custom_request("getCreditCardDetails",
		"URL=https://{api_host}/payment/getCreditCardDetails",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	lr_end_sub_transaction("T09_Proceed_To_Checkout_getCreditCardDetails", 2);

	lr_start_sub_transaction ( "T09_Proceed_To_Checkout_getShipmentMethods", "T09_Proceed_To_Checkout Registered" );
		# 1507 "..\\..\\checkoutFunctions.c"


	addHeader();
	if (strlen(lr_eval_string("{savedZipCode}")) == 5 ) {
		web_add_header("state", lr_eval_string("{savedState}") );
		web_add_header("zipCode", lr_eval_string("{savedZipCode}") );
		web_add_header("addressField1", lr_eval_string("{savedAddressLine}") );
	} else {
		if (strcmp(lr_eval_string("{storeId}"), "10151") == 0) {
			web_add_header("state", "NJ");
			web_add_header("zipCode", "07094");
		} else {
			web_add_header("state", lr_eval_string("{guestState}"));
			web_add_header("zipCode", lr_eval_string("{guestZip}"));
		}
	}
	web_custom_request("getShipmentMethods",
		"URL=https://{api_host}/payment/getShipmentMethods",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	lr_end_sub_transaction("T09_Proceed_To_Checkout_getShipmentMethods", 2);

	lr_end_transaction ( "T09_Proceed_To_Checkout Registered" , 0) ;

	 

	Submit_Pickup_Detail();

	return 0;

}  


int proceedAsGuest()
{
	lr_think_time ( LINK_TT ) ;
	 

	lr_start_transaction ( "T09_Proceed_To_Checkout Guest" ) ;

 

	lr_start_sub_transaction ("T09_Proceed_To_Checkout_getUnqualifiedItems", "T09_Proceed_To_Checkout Guest" ) ;

		# 1564 "..\\..\\checkoutFunctions.c"


	addHeader();
	web_custom_request("getUnqualifiedItems",
		"URL=https://{api_host}/tcporder/getUnqualifiedItems",
		"Method=GET",
		"Resource=0",
		"Mode=HTML",
		"LAST");
	lr_end_sub_transaction ("T09_Proceed_To_Checkout_getUnqualifiedItems", 2) ;

	lr_start_sub_transaction ("T09_Proceed_To_Checkout shipping", "T09_Proceed_To_Checkout Guest" ) ;
	web_custom_request("checkout",
		"URL=https://{host}/{country}/checkout/shipping",
		"Method=GET",
		"Resource=0",
		"Mode=HTML",
		"LAST");
	lr_end_sub_transaction ("T09_Proceed_To_Checkout shipping", 2) ;

	lr_start_sub_transaction ("T09_Proceed_To_Checkout_getOrderDetails", "T09_Proceed_To_Checkout Guest" ) ;
 




	addHeader();
	web_add_header("locStore", "True");
	web_add_header("pageName", "fullOrderInfo");
	web_reg_find("TEXT/IC={", "SaveCount=apiCheck", "LAST");
	web_url("getOrderDetails",
		"URL=https://{api_host}/getOrderDetails",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	if (atoi(lr_eval_string("{apiCheck}")) ==0 )
		lr_end_sub_transaction ("T09_Proceed_To_Checkout_getOrderDetails", 1) ;
	else
		lr_end_sub_transaction ("T09_Proceed_To_Checkout_getOrderDetails", 2) ;

	lr_start_sub_transaction ("T09_Proceed_To_Checkout_getRegisteredUserDetailsInfo", "T09_Proceed_To_Checkout Guest" ) ;
 




	addHeader();
	web_reg_find("TEXT/IC=resourceName", "SaveCount=apiCheck", "LAST");
	web_url("getRegisteredUserDetailsInfo",
		"URL=https://{api_host}/payment/getRegisteredUserDetailsInfo",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction ("T09_Proceed_To_Checkout_getRegisteredUserDetailsInfo", 1) ;
	else
		lr_end_sub_transaction ("T09_Proceed_To_Checkout_getRegisteredUserDetailsInfo", 2) ;
 



	lr_start_sub_transaction ("T09_Proceed_To_Checkout_getAllCoupons", "T09_Proceed_To_Checkout Guest" ) ;
	addHeader();
	web_reg_find("TEXT/IC=appliedCoupons", "SaveCount=apiCheck", "LAST");
	web_url("getAllCoupons",
		"URL=https://{api_host}/tcporder/getAllCoupons",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction ("T09_Proceed_To_Checkout_getAllCoupons", 1) ;
	else
		lr_end_sub_transaction ("T09_Proceed_To_Checkout_getAllCoupons", 2) ;



 




 
# 1662 "..\\..\\checkoutFunctions.c"
	web_reg_save_param("savedState", "LB=state\": \"", "RB=\"", "NotFound=Warning", "LAST");
	web_reg_save_param("savedZipCode", "LB=zipCode\": \"", "RB=\"", "NotFound=Warning", "LAST");
	web_reg_save_param_json( "ParamName=savedAddressLine", "QueryString=$.contact[0]...addressLine[0].", "SelectAll=NO", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");


	lr_start_sub_transaction ("T09_Proceed_To_Checkout_getShipmentMethods", "T09_Proceed_To_Checkout Guest" ) ;
	addHeader();
	web_reg_find("TEXT/IC=jsonArr", "SaveCount=apiCheck", "LAST");
	web_url("getShipmentMethods",
		"URL=https://{api_host}/payment/getShipmentMethods",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction ("T09_Proceed_To_Checkout_getShipmentMethods", 1) ;
	else
		lr_end_sub_transaction ("T09_Proceed_To_Checkout_getShipmentMethods", 2) ;

	lr_start_sub_transaction ("T09_Proceed_To_Checkout_giftOptionsCmd", "T09_Proceed_To_Checkout Guest" ) ;
 




	addHeader();
	web_reg_find("TEXT/IC=giftOptions", "SaveCount=apiCheck", "LAST");
	web_url("giftOptionsCmd",
		"URL=https://{api_host}/payment/giftOptionsCmd",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction ("T09_Proceed_To_Checkout_giftOptionsCmd", 1) ;
	else
		lr_end_sub_transaction ("T09_Proceed_To_Checkout_giftOptionsCmd", 2) ;

	if (ESPOT_FLAG == 1)
	{
		lr_start_sub_transaction ("T09_Proceed_To_Checkout_getESpots", "T09_Proceed_To_Checkout Guest" ) ;

				


		web_add_header("espotName","GlobalHeaderBannerAboveHeader,GlobalHeaderBannerWithinHeader,GlobalHeaderLogo,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,create_account_drawer,MinicartLoginBanner,MinicartRegisterBanner,drawer_myaccount_overview_banner,coupon_help,bag_empty_banner,drawer_empty_bag,MinicartLoginFromFavoritesBanner,bag_ledger_promo_banner,gift_services_espot,CheckoutConfirmationBanner,CheckoutConfirmationMPRPromo,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1");
		web_add_header("deviceType","desktop");

	 
		addHeader();
		web_reg_find("TEXT/IC=espotName", "SaveCount=apiCheck", "LAST");
		web_custom_request("getESpots",
			"URL=https://{api_host}/getESpot",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			"LAST");
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction("T09_Proceed_To_Checkout_getESpots", 1);
		else
			lr_end_sub_transaction("T09_Proceed_To_Checkout_getESpots", 2);

		api_getESpot_second();
	}

	lr_end_transaction ( "T09_Proceed_To_Checkout Guest" , 0) ;

	 

	Submit_Pickup_Detail();

	return 0;

}  

int forgetPassword()
{
	lr_think_time ( FORM_TT ) ;
	web_cleanup_cookies ( ) ;
	web_cache_cleanup();

	 
	lr_start_transaction ( "T29_ForgotPassword_resetpassword" ) ;

		# 1758 "..\\..\\checkoutFunctions.c"


	registerErrorCodeCheck();
	addHeader();
	web_add_header("Content-Type", "application/json");
	lr_save_string( "BITROGUE@MAILINATOR.COM", "userEmail" );


	web_custom_request("resetpassword",
		"URL=https://{api_host}/resetpassword",
		"Method=PUT",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTML",
		"Body={\"storeId\":\"{storeId}\",\"catalogId\":\"{catalogId}\",\"langId\":\"-1\",\"isPasswordReset\":\"true\",\"logonId\":\"{userEmail}\",\"reLogonURL\":\"ChangePassword\",\"formFlag\":\"true\"}",
		"LAST");

	 
	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 )  
	{
		lr_end_transaction ( "T29_ForgotPassword_resetpassword" , 0) ;
		return 0;
	}
	else
	{
		lr_fail_trans_with_error( lr_eval_string("T29_ForgotPassword_resetpassword Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\" User: {userEmail}") ) ;
		lr_end_transaction ( "T29_ForgotPassword_resetpassword" , 1) ;
		return 1;
	}

}

int login()  
{
	lr_think_time ( FORM_TT ) ;

	 

	lr_start_transaction ( "T10_Logon_ShippingView" ) ;

	lr_start_sub_transaction ( "T10_Logon_ShippingView1_logon", "T10_Logon_ShippingView" ) ;
		# 1811 "..\\..\\checkoutFunctions.c"


	web_reg_save_param ( "addressId" , "LB=\"addressId\": \"" , "RB=\",\n" , "ORD=1", "NotFound=Warning", "LAST" ) ;  
	web_reg_save_param ( "addressIdAll" , "LB=\"addressId\": \"" , "RB=\",\n" , "ORD=ALL", "NotFound=Warning", "LAST" ) ;  
	web_reg_save_param ( "responseCode" , "LB=\"responseCode\": \"" , "RB=\"" , "NotFound=Warning", "LAST" ) ;  
	registerErrorCodeCheck();

	addHeader();
	web_custom_request("logon",
		"URL=https://{api_host}/logon",
		"Method=POST",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTML",
		"EncType=application/json",	"Body={\"storeId\":\"{storeId}\",\"logonId1\":\"{userEmail}\",\"logonPassword1\":\"Asdf!234\",\"rememberCheck\":false,\"rememberMe\":false,\"requesttype\":\"ajax\",\"reLogonURL\":\"TCPAjaxLogonErrorView\",\"URL\":\"TCPAjaxLogonSuccessView\",\"registryAccessPreference\":\"Public\",\"calculationUsageId\":-1,\"createIfEmpty\":1,\"deleteIfEmpty\":\"*\",\"fromOrderId\":\"*\",\"toOrderId\":\".\",\"updatePrices\":0}",
		"LAST");
	lr_end_sub_transaction ( "T10_Logon_ShippingView1_logon", 2 ) ;

	lr_start_sub_transaction ( "T10_Logon_ShippingView2_shipping", "T10_Logon_ShippingView" ) ;
		web_url("checkout",
 
		"URL=https://{host}/{country}/checkout",   
		"TargetFrame=",
		"Resource=0",
		"Mode=HTML",
		"LAST");
	lr_end_sub_transaction ( "T10_Logon_ShippingView2_shipping", 2 ) ;

	lr_start_sub_transaction ("T10_Logon_ShippingView_getRegisteredUserDetailsInfo","T10_Logon_ShippingView");
 




	web_reg_save_param ( "addressId" , "LB=\"addressId\": \"" , "RB=\",\n" , "ORD=1", "NotFound=Warning", "LAST" ) ;  
	web_reg_save_param ( "addressIdAll" , "LB=\"addressId\": \"" , "RB=\",\n" , "ORD=ALL", "NotFound=Warning", "LAST" ) ;  
	web_reg_save_param ( "userId" , "LB=\"userId\": \"" , "RB=\",\n" , "NotFound=Warning", "LAST" ) ;  
	addHeader();
	web_reg_find("TEXT/IC=resourceName", "SaveCount=apiCheck", "LAST");
	web_url("getRegisteredUserDetailsInfo",
		"URL=https://{api_host}/payment/getRegisteredUserDetailsInfo",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction ("T10_Logon_ShippingView_getRegisteredUserDetailsInfo",1);
	else
		lr_end_sub_transaction ("T10_Logon_ShippingView_getRegisteredUserDetailsInfo",2);

 
 
 
 

	lr_start_sub_transaction ("T10_Logon_ShippingView_getAllCoupons","T10_Logon_ShippingView");
	addHeader();
	web_reg_find("TEXT/IC=appliedCoupons", "SaveCount=apiCheck", "LAST");
	web_url("getAllCoupons",
		"URL=https://{api_host}/tcporder/getAllCoupons",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction ("T10_Logon_ShippingView_getAllCoupons",1);
	else
		lr_end_sub_transaction ("T10_Logon_ShippingView_getAllCoupons",2);

	lr_start_sub_transaction ("T10_Logon_ShippingView_getOrderDetails","T10_Logon_ShippingView");
 




	addHeader();
	web_add_header("calc", "false");
	web_add_header("locStore", "True");
	web_add_header("pageName", "fullOrderInfo");
	web_reg_save_param("cartCount", "LB=cartCount\": ", "RB=,", "NotFound=Warning", "LAST");		 
	web_reg_find("TEXT/IC={", "SaveCount=apiCheck", "LAST");
	web_url("getOrderDetails",
		"URL=https://{api_host}/getOrderDetails",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction ("T10_Logon_ShippingView_getOrderDetails",1);
	else
		lr_end_sub_transaction ("T10_Logon_ShippingView_getOrderDetails",2);

	lr_start_sub_transaction ("T10_Logon_ShippingView_getCreditCardDetails","T10_Logon_ShippingView");
 




	addHeader();
	web_add_header("isRest", "true" );
	web_url("getCreditCardDetails",
		"URL=https://{api_host}/payment/getCreditCardDetails",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	lr_end_sub_transaction ("T10_Logon_ShippingView_getCreditCardDetails",2);

	lr_start_sub_transaction ("T10_Logon_ShippingView_giftOptionsCmd","T10_Logon_ShippingView");
 




	addHeader();
	web_reg_find("TEXT/IC=giftOptions", "SaveCount=apiCheck", "LAST");
	web_url("giftOptionsCmd",
		"URL=https://{api_host}/payment/giftOptionsCmd",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction ("T10_Logon_ShippingView_giftOptionsCmd",1);
	else
		lr_end_sub_transaction ("T10_Logon_ShippingView_giftOptionsCmd",2);

	lr_start_sub_transaction ("T10_Logon_ShippingView_getAddressFromBook","T10_Logon_ShippingView");
 




	web_reg_save_param("savedState", "LB=state\": \"", "RB=\"", "NotFound=Warning", "LAST");
	web_reg_save_param("savedZipCode", "LB=zipCode\": \"", "RB=\"", "NotFound=Warning", "LAST");
	web_reg_save_param_json( "ParamName=savedAddressLine", "QueryString=$.contact[0]...addressLine[0].", "SelectAll=NO", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
	addHeader();
	web_url("getAddressFromBook",
		"URL=https://{api_host}/payment/getAddressFromBook",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	lr_end_sub_transaction ("T10_Logon_ShippingView_getAddressFromBook",2);

	lr_start_sub_transaction ("T10_Logon_ShippingView_getShipmentMethods","T10_Logon_ShippingView");
	addHeader();
	if (strlen(lr_eval_string("{savedZipCode}")) == 5 ) {
		web_add_header("state", lr_eval_string("{savedState}") );
		web_add_header("zipCode", lr_eval_string("{savedZipCode}") );
		web_add_header("addressField1", lr_eval_string("{savedAddressLine}") );
	} else {
		if (strcmp(lr_eval_string("{storeId}"), "10151") == 0) {
			web_add_header("state", "NJ");
			web_add_header("zipCode", "07094");
		} else {
			web_add_header("state", lr_eval_string("{guestState}"));
			web_add_header("zipCode", lr_eval_string("{guestZip}"));
		}
	}

	web_reg_find("TEXT/IC=jsonArr", "SaveCount=apiCheck", "LAST");
	web_url("getShipmentMethods",
		"URL=https://{api_host}/payment/getShipmentMethods",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction ("T10_Logon_ShippingView_getShipmentMethods",1);
	else
		lr_end_sub_transaction ("T10_Logon_ShippingView_getShipmentMethods",2);

 


	if (ESPOT_FLAG == 1)
	{
		lr_start_sub_transaction ("T10_Logon_ShippingView_getESpots","T10_Logon_ShippingView");

				


		web_add_header("espotName","GlobalHeaderBannerAboveHeader,GlobalHeaderBannerWithinHeader,GlobalHeaderLogo,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,create_account_drawer,MinicartLoginBanner,MinicartRegisterBanner,drawer_myaccount_overview_banner,coupon_help,bag_empty_banner,drawer_empty_bag,MinicartLoginFromFavoritesBanner,bag_ledger_promo_banner,gift_services_espot,CheckoutConfirmationBanner,CheckoutConfirmationMPRPromo,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1");
		web_add_header("deviceType","desktop");

	 
		addHeader();
		web_reg_find("TEXT/IC=espotName", "SaveCount=apiCheck", "LAST");
		web_custom_request("getESpots",
			"URL=https://{api_host}/getESpot",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			"LAST");
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction("T10_Logon_ShippingView_getESpots", 1);
		else
			lr_end_sub_transaction("T10_Logon_ShippingView_getESpots", 2);

		api_getESpot_second();
	}

	 
	 

	 

	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 && strcmp ( lr_eval_string ( "{responseCode}" ), "LoginSuccess" ) == 0)  
	{
		isLoggedIn = 1;
		lr_end_transaction ( "T10_Logon_ShippingView" , 0) ;

		Submit_Pickup_Detail();

		return 0;
	}
	else
	{
		isLoggedIn = 0;
		lr_fail_trans_with_error( lr_eval_string("T10_Logon_ShippingView Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\" User: {userEmail}") ) ;
		lr_end_transaction ( "T10_Logon_ShippingView" , 1) ;
		return 1;
	}
}


int loginFromHomePage()   
{
	lr_think_time ( FORM_TT ) ;
 	 

	lr_start_transaction ( "T10_Logon" ) ;

	lr_start_sub_transaction ("T10_Logon logon","T10_Logon");
		# 2052 "..\\..\\checkoutFunctions.c"


	web_reg_save_param ( "responseCode" , "LB=\"responseCode\": \"" , "RB=\"" , "NotFound=Warning", "LAST" ) ;  
	registerErrorCodeCheck();
	addHeader();
	web_custom_request("logon",
		"URL=https://{api_host}/logon",
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTML",
		"EncType=application/json", 		"Body={\"storeId\":\"{storeId}\",\"logonId1\":\"{userEmail}\",\"logonPassword1\":\"Asdf!234\",\"rememberCheck\":true,\"requesttype\":\"ajax\",\"reLogonURL\":\"TCPAjaxLogonErrorView\",\"URL\":\"TCPAjaxLogonSuccessView\",\"registryAccessPreference\":\"Public\",\"calculationUsageId\":-1,\"createIfEmpty\":1,\"deleteIfEmpty\":\"*\",\"fromOrderId\":\"*\",\"toOrderId\":\".\",\"updatePrices\":0}",
		"LAST");
	lr_end_sub_transaction ("T10_Logon logon",2);

	lr_start_sub_transaction ("T10_Logon_getRegisteredUserDetailsInfo","T10_Logon");
 




	web_reg_save_param ( "addressId" , "LB=\"addressId\": \"" , "RB=\",\n" , "ORD=1", "NotFound=Warning", "LAST" ) ;  
	web_reg_save_param ( "addressIdAll" , "LB=\"addressId\": \"" , "RB=\",\n" , "ORD=ALL", "NotFound=Warning", "LAST" ) ;  
	web_reg_save_param ( "state" , "LB=\"state\": \"" , "RB=\"" , "NotFound=Warning", "LAST" ) ;  
	web_reg_save_param ( "userId" , "LB=\"userId\": \"" , "RB=\",\n" , "NotFound=Warning", "LAST" ) ;  

	addHeader();
		web_custom_request("getRegisteredUserDetailsInfo",
			"URL=https://{api_host}/payment/getRegisteredUserDetailsInfo",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			"LAST");
	lr_end_sub_transaction ("T10_Logon_getRegisteredUserDetailsInfo",2);
 





	lr_start_sub_transaction ("T10_Logon_getOrderDetails","T10_Logon");
 




	 
	addHeader();
	web_add_header("locStore", "True");
	web_add_header("pageName", "fullOrderInfo");
	web_reg_save_param ( "addressIdLoginFirst" , "LB=<option id=\"" , "RB=\" value=" , "NotFound=Warning", "LAST" ) ;
	web_reg_find("TEXT/IC={", "SaveCount=apiCheck", "LAST");
	web_custom_request("getOrderDetails",
		"URL=https://{api_host}/getOrderDetails",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction ("T10_Logon_getOrderDetails",1);
	else
		lr_end_sub_transaction ("T10_Logon_getOrderDetails",2);

	 
	 

	if (ESPOT_FLAG == 1)
	{
		lr_start_sub_transaction ("T10_Logon_getESpots","T10_Logon");

				


		web_add_header("espotName","GlobalHeaderBannerAboveHeader,GlobalHeaderBannerWithinHeader,GlobalHeaderLogo,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,create_account_drawer,MinicartLoginBanner,MinicartRegisterBanner,drawer_myaccount_overview_banner,coupon_help,bag_empty_banner,drawer_empty_bag,MinicartLoginFromFavoritesBanner,bag_ledger_promo_banner,gift_services_espot,CheckoutConfirmationBanner,CheckoutConfirmationMPRPromo,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1");
		web_add_header("deviceType","desktop");

	 
		addHeader();
		web_reg_find("TEXT/IC=espotName", "SaveCount=apiCheck", "LAST");
		web_custom_request("getESpots",
			"URL=https://{api_host}/getESpot",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			"LAST");
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction("T10_Logon_getESpots", 1);
		else
			lr_end_sub_transaction("T10_Logon_getESpots", 2);

		api_getESpot_second();
	}

 	 
 
# 2164 "..\\..\\checkoutFunctions.c"
	lr_start_sub_transaction ( "T10_Logon_getAllCoupons", "T10_Logon" );

	addHeader();
	web_reg_find("TEXT/IC=appliedCoupons", "SaveCount=apiCheck", "LAST");
	web_custom_request("getAllCoupons",
		"URL=https://{api_host}/tcporder/getAllCoupons",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T10_Logon_getAllCoupons", 1);
	else
		lr_end_sub_transaction("T10_Logon_getAllCoupons", 2);


	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0)  
	{
		isLoggedIn = 1;
		lr_end_transaction ( "T10_Logon" , 0) ;
		return 0;
	}
	else
	{
		isLoggedIn = 0;
		lr_fail_trans_with_error( lr_eval_string("T10_Logon Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
		lr_end_transaction ( "T10_Logon" , 1) ;
		return 1;
	}

}


void convertAndApplyPoints()
{
 
 
	lr_think_time ( FORM_TT ) ;

	web_reg_save_param ( "myPlaceId" , "LB=<input type=\"hidden\" value=\"" , "RB=\" name=\"myPlaceId\" />", "Notfound=warning", "LAST" ) ;
	web_reg_save_param ( "myAccountId" , "LB=<input type=\"hidden\" value=\"" , "RB=\" name=\"accountId\" />", "Notfound=warning", "LAST" ) ;

	web_reg_save_param ( "addressIds" , "LB=<option id=\"" , "RB=\" value=" , "Ord=ALL" , "NotFound=Warning", "LAST" ) ;
	web_reg_save_param ( "TCPMyAddressBook" , "LB=edit shipping address\" id=\"TCPMyAddressBook_" , "RB=\"" , "Ord=ALL", "NotFound=Warning", "LAST" ) ;
	web_reg_save_param ( "emailAddress1" , "LB=<input type=\"hidden\" name=\"email1\" value=\"" , "RB=\" id=\"email1\" />" , "NotFound=Warning", "LAST" ) ;
	web_reg_save_param ( "authToken" , "LB=<input type=\"hidden\" name=\"authToken\" value=\"" , "RB=\"", "NotFound=Warning", "LAST" ) ;

	lr_start_transaction ( "T11_Convert Points To Coupon" ) ;

		lr_start_sub_transaction( "T11_Convert Points_S01_Click MyPlace Rewards", "T11_Convert Points To Coupon" );

		web_url("AjaxLogonForm",
			"URL=https://{host}/shop/AjaxLogonForm?catalogId={catalogId}&myAcctMain=1&langId=-1&storeId={storeId}",
			"Mode=HTTP",
			"LAST");

		lr_end_sub_transaction( "T11_Convert Points_S01_Click MyPlace Rewards", 2 );

		web_reg_save_param ( "promocodeRewards" , "LB=\"javascript:applyLoyaltyCode('" , "RB=');", "NotFound=Warning", "LAST" ) ;

		lr_start_sub_transaction("T11_Convert Points_S02_TCPRedeemLoyaltyPoints", "T11_Convert Points To Coupon" );

		web_submit_data("TCPRedeemLoyaltyPoints",
				"Action=https://{host}/shop/TCPRedeemLoyaltyPoints",     
				"Method=POST",
				"Mode=HTTP",
				"ITEMDATA",
				"Name=storeId", "Value={storeId}", "ENDITEM",
				"Name=langId", "Value=-1", "ENDITEM",
				"Name=catalogId", "Value={catalogId}", "ENDITEM",
				"Name=myPlaceId", "Value={myPlaceId}", "ENDITEM",
				"Name=accountId", "Value={myAccountId}", "ENDITEM",
				"Name=visitorId", "Value=[CS]v1|2B2AE4E305078968-600001048004DD05[CE]", "ENDITEM",
				"Name=amountToRedeem", "Value=5", "ENDITEM",
				"Name=cpnWalletValue", "Value=0", "ENDITEM",  
				"LAST");

		lr_end_transaction("T11_Convert Points_S02_TCPRedeemLoyaltyPoints", 2);

	lr_end_transaction ( "T11_Convert Points To Coupon" , 0) ;

 
	if ( strlen(lr_eval_string("{promocodeRewards}")) == 16 ) {

		viewCart();

	    applyPromoCode(1);
	}

 
 
 
 
 
}  

int submitShippingBrowseFirst()  
{
	lr_think_time ( FORM_TT ) ;
	 
	lr_start_transaction ( "T13_Submit_Shipping_Address Registered" );

	lr_save_string( lr_paramarr_idx("addressIdAll", atoi(lr_eval_string("{addressIdAll_count}")) ), "addressId" );

	lr_start_sub_transaction ( "T13_Submit_Shipping_Address_updateShippingMethodSelection1", "T13_Submit_Shipping_Address Registered" ) ;

		# 2281 "..\\..\\checkoutFunctions.c"


	addHeader();
	web_add_header( "Content-Type", "application/json" );
	if (strcmp(lr_eval_string("{storeId}"), "10151") != 0)
		lr_save_string("900103", "shipmode_id");
	else
		lr_save_string("901101", "shipmode_id");

	registerErrorCodeCheck();
	web_reg_find("TEXT/IC=updateShippingMethodSelectionResponse", "SaveCount=apiCheck1", "LAST");
	web_custom_request("updateShippingMethodSelection",
		"URL=https://{api_host}/payment/updateShippingMethodSelection",
		"Method=PUT",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTML",
		"Body={\"shipModeId\":\"{shipmode_id}\",\"addressId\":\"{addressId}\",\"requesttype\":\"ajax\",\"x_calculationUsage\":\"-1,-2,-3,-4,-5,-6,-7\"}",
 
		"LAST");
	lr_end_sub_transaction ( "T13_Submit_Shipping_Address_updateShippingMethodSelection1" , 2 ) ;

	lr_start_sub_transaction ( "T13_Submit_Shipping_Address_getPointsService", "T13_Submit_Shipping_Address Registered" ) ;
 




	addHeader();
	web_reg_find("TEXT/IC=LoyaltyWebsiteInd", "SaveCount=apiCheck", "LAST");
	web_url("getPointsService",
		"URL=https://{api_host}/payment/getPointsService",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction ( "T13_Submit_Shipping_Address_getPointsService" , 1 ) ;
	else
		lr_end_sub_transaction ( "T13_Submit_Shipping_Address_getPointsService" , 2 ) ;

	lr_start_sub_transaction ( "T13_Submit_Shipping_Address_getOrderDetails", "T13_Submit_Shipping_Address Registered" ) ;
 




	addHeader();
	web_add_header("calc", "false");
	web_add_header("locStore", "True");
	web_add_header("pageName", "fullOrderInfo");
	web_reg_find("TEXT/IC={", "SaveCount=apiCheck", "LAST");
	web_url("getOrderDetails",
		"URL=https://{api_host}/getOrderDetails",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction ( "T13_Submit_Shipping_Address_getOrderDetails" , 1 ) ;
	else
		lr_end_sub_transaction ( "T13_Submit_Shipping_Address_getOrderDetails" , 2 ) ;

	lr_start_sub_transaction ( "T13_Submit_Shipping_Address_addSignUpEmail", "T13_Submit_Shipping_Address Registered" ) ;

		# 2356 "..\\..\\checkoutFunctions.c"


	addHeader();
	web_custom_request("addSignUpEmail",
		"URL=https://{api_host}/addSignUpEmail",
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTTP",
		"EncType=application/json",
		"Body={\"storeId\":\"{storeId}\",\"catalogId\":\"{catalogId}\",\"langId\":\"-1\",\"emailaddr\":\"{userEmail}\",\"URL\":\"email-confirmation\",\"response\":\"accept_all::true:false\"}",
		"LAST");
	lr_end_sub_transaction ( "T13_Submit_Shipping_Address_addSignUpEmail" , 2 ) ;

	 

 
	if (strlen(lr_eval_string("{apiCheck1}")) > 0 )
	{
		lr_end_transaction ( "T13_Submit_Shipping_Address Registered" , 0) ;
		return 0;
	}
	else {
		lr_fail_trans_with_error( lr_eval_string("T13_Submit_Shipping_Address Registered Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\" shipModeId:\"{shipmode_id}\", addressId:\"{addressId}\", User: {userEmail}") ) ;
		lr_end_transaction ( "T13_Submit_Shipping_Address Registered", 1 ) ;
		return 1;
	}

}

int submitShippingRegistered()   
{
	lr_think_time ( FORM_TT ) ;
	 


	lr_start_transaction ( "T13_Submit_Shipping_Address Registered" );

	if (strcmp( lr_eval_string("{addBopisToCart}"), "true") == 0 && strcmp( lr_eval_string("{addEcommToCart}"), "true") == 0 )  
	{
		lr_start_sub_transaction ( "T13_Submit_Shipping_Address_addAddress", "T13_Submit_Shipping_Address Registered" ) ;
				


		addHeader();
		web_add_header( "Content-Type", "application/json" );
		web_custom_request("addAddress",
			"URL=https://{api_host}/payment/addAddress",
			"Method=POST",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTTP",
			"EncType=application/json",
			"Body={\"contact\":[{\"addressLine\":[\"{guestAdr1}\",\"\",\"\"],\"attributes\":[{\"key\":\"addressField3\",\"value\":\"{guestZip}\"}],\"addressType\":\"ShippingAndBilling\",\"city\":\"{guestCity}\",\"country\":\"{guestCountry}\",\"firstName\":\"Joe\",\"lastName\":\"User\",\"nickName\":\"{nickName}\",\"phone1\":\"2014531513\",\"email1\":\"BITROGUE@MAILINATOR.COM\",\"phone1Publish\":\"false\",\"primary\":\"false\",\"state\":\"{guestState}\",\"zipCode\":\"{guestZip}\",\"xcont_addressField3\":\"{guestZip}\",\"fromPage\":\"\"}]}",
			 "LAST");
		lr_end_sub_transaction ( "T13_Submit_Shipping_Address_addAddress", 2 ) ;

	}

	lr_save_string( lr_paramarr_idx("addressIdAll", atoi(lr_eval_string("{addressIdAll_count}")) ), "addressId" );

	lr_start_sub_transaction ( "T13_Submit_Shipping_Address_updateShippingMethodSelection2", "T13_Submit_Shipping_Address Registered" ) ;

		# 2431 "..\\..\\checkoutFunctions.c"


	if (strcmp(lr_eval_string("{storeId}"), "10151") != 0)
		lr_save_string("900103", "shipmode_id");
	else
		lr_save_string("901101", "shipmode_id");

	addHeader();
	web_add_header( "Content-Type", "application/json" );
	web_reg_find("TEXT/IC=orderid", "SaveCount=apiCheck1", "LAST");
	registerErrorCodeCheck();
	web_custom_request("updateShippingMethodSelection",
		"URL=https://{api_host}/payment/updateShippingMethodSelection",
		"Method=PUT",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTML",
		"Body={\"shipModeId\":\"{shipmode_id}\",\"addressId\":\"{addressId}\",\"requesttype\":\"ajax\",\"x_calculationUsage\":\"-1,-2,-3,-4,-5,-6,-7\"}",   
 
		"LAST");
	lr_end_sub_transaction ( "T13_Submit_Shipping_Address_updateShippingMethodSelection2", 2 ) ;

	lr_start_sub_transaction ( "T13_Submit_Shipping_Address_getOrderDetails", "T13_Submit_Shipping_Address Registered" ) ;
 




	addHeader();
	web_add_header("locStore", "True");
	web_add_header("pageName", "fullOrderInfo");
 
	web_reg_save_param("piAmount", "LB=\"piAmount\": \"", "RB=\"", "NotFound=Warning", "LAST");  
	web_reg_find("TEXT/IC={", "SaveCount=apiCheck", "LAST");
	web_custom_request("getOrderDetails",
		"URL=https://{api_host}/getOrderDetails",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction ( "T13_Submit_Shipping_Address_getOrderDetails", 1 ) ;
	else
		lr_end_sub_transaction ( "T13_Submit_Shipping_Address_getOrderDetails", 2 ) ;

	lr_start_sub_transaction ( "T13_Submit_Shipping_Address_getPointsService", "T13_Submit_Shipping_Address Registered" );
 




	addHeader();
	web_reg_find("TEXT/IC=LoyaltyWebsiteInd", "SaveCount=apiCheck", "LAST");
	web_custom_request("getPointsService",
		"URL=https://{api_host}/payment/getPointsService",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T13_Submit_Shipping_Address_getPointsService", 1);
	else
		lr_end_sub_transaction("T13_Submit_Shipping_Address_getPointsService", 2);


	lr_start_sub_transaction ( "T13_Submit_Shipping_Address_addSignUpEmail", "T13_Submit_Shipping_Address Registered" );

		# 2510 "..\\..\\checkoutFunctions.c"


	addHeader();
	web_custom_request("addSignUpEmail",
		"URL=https://{api_host}/addSignUpEmail",
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTTP",
		"EncType=application/json",
		"Body={\"storeId\":\"{storeId}\",\"catalogId\":\"{catalogId}\",\"langId\":\"-1\",\"emailaddr\":\"BITROGUE@MAILINATOR.COM\",\"URL\":\"email-confirmation\",\"response\":\"accept_all::true:false\"}",
		"LAST");
	lr_end_sub_transaction("T13_Submit_Shipping_Address_addSignUpEmail", 2);

	 

 
	if ( strlen( lr_eval_string("{apiCheck1}") ) > 0 )
	{
		lr_end_transaction ( "T13_Submit_Shipping_Address Registered" , 0) ;
		return 0;
	}
	else {
		lr_fail_trans_with_error( lr_eval_string("T13_Submit_Shipping_Address  Registered with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
		lr_end_transaction ( "T13_Submit_Shipping_Address Registered", 1 ) ;
		return 1;
	}
}


int submitShipping()  
{
	lr_think_time ( FORM_TT ) ;
	 
	lr_start_transaction ( "T13_Submit_Shipping_Address Guest" ) ;

	lr_start_sub_transaction ( "T13_Submit_Shipping_Address_addAddress", "T13_Submit_Shipping_Address Guest" ) ;
 



	web_reg_save_param("addressId", "LB=\"addressId\": \"", "RB=\",\n", "NotFound=Warning", "LAST");
	web_reg_save_param ( "addressIdAll" , "LB=\"addressId\": \"" , "RB=\",\n" , "ORD=ALL", "NotFound=Warning", "LAST" ) ;  
	registerErrorCodeCheck();

	addHeader();
 
 
# 2567 "..\\..\\checkoutFunctions.c"
	web_custom_request("addAddress",
		"URL=https://{api_host}/payment/addAddress",
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTML",
		"EncType=application/json",
		"Body={\"contact\":[{\"addressLine\":[\"{guestAdr1}\",\"\",\"\"],\"attributes\":[{\"key\":\"addressField3\",\"value\":\"{guestZip}\"}],\"addressType\":\"ShippingAndBilling\",\"city\":\"{guestCity}\",\"country\":\"{guestCountry}\",\"firstName\":\"Joe\",\"lastName\":\"User\",\"nickName\":\"{randomNineDigits}{randomFourDigits}\",\"phone1\":\"201453{randomFourDigits}\",\"email1\":\"bitrogue@mailinator.com\",\"phone1Publish\":\"true\",\"primary\":\"false\",\"state\":\"{guestState}\",\"zipCode\":\"{guestZip}\",\"xcont_addressField2\":\"2\",\"xcont_addressField3\":\""
		"{guestZip}\",\"fromPage\":\"\"}]}",
		"LAST");

	lr_end_sub_transaction ( "T13_Submit_Shipping_Address_addAddress", 2 ) ;

	lr_start_sub_transaction ( "T13_Submit_Shipping_Address_updateShippingMethodSelection3", "T13_Submit_Shipping_Address Guest" ) ;
		# 2592 "..\\..\\checkoutFunctions.c"


	addHeader();
	web_add_header( "Content-Type", "application/json" );
	web_reg_find("TEXT/IC=orderid", "SaveCount=apiCheck1", "LAST");
	web_custom_request("updateShippingMethodSelection",
		"URL=https://{api_host}/payment/updateShippingMethodSelection",
		"Method=PUT",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTML",
		"Body={\"shipModeId\":\"{shipmode_id}\",\"addressId\":\"{addressId}\",\"requesttype\":\"ajax\",\"x_calculationUsage\":\"-1,-2,-3,-4,-5,-6,-7\"}",
 
		"LAST");
	lr_end_sub_transaction ( "T13_Submit_Shipping_Address_updateShippingMethodSelection3", 2 ) ;

	web_reg_save_param("piAmount", "LB=\"piAmount\": \"", "RB=\"", "NotFound=Warning", "LAST");  

	lr_start_sub_transaction ( "T13_Submit_Shipping_Address_getOrderDetails", "T13_Submit_Shipping_Address Guest" ) ;
 




	addHeader();
	web_add_header("locStore", "True");
	web_add_header("pageName", "fullOrderInfo");
	web_reg_find("TEXT/IC={", "SaveCount=apiCheck", "LAST");
	web_custom_request("getOrderDetails",
		"URL=https://{api_host}/getOrderDetails",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction ( "T13_Submit_Shipping_Address_getOrderDetails", 1 ) ;
	else
		lr_end_sub_transaction ( "T13_Submit_Shipping_Address_getOrderDetails", 2 ) ;

	lr_start_sub_transaction ( "T13_Submit_Shipping_Address_getPointsService", "T13_Submit_Shipping_Address Guest" );
 




	addHeader();
	web_reg_find("TEXT/IC=LoyaltyWebsiteInd", "SaveCount=apiCheck", "LAST");
	web_custom_request("getPointsService",
		"URL=https://{api_host}/payment/getPointsService",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T13_Submit_Shipping_Address_getPointsService", 1);
	else
		lr_end_sub_transaction("T13_Submit_Shipping_Address_getPointsService", 2);

	lr_start_sub_transaction ( "T13_Submit_Shipping_Address_addSignUpEmail", "T13_Submit_Shipping_Address Guest" );

		# 2663 "..\\..\\checkoutFunctions.c"


	addHeader();
	web_custom_request("addSignUpEmail",
		"URL=https://{api_host}/addSignUpEmail",
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTTP",
		"EncType=application/json",
		"Body={\"storeId\":\"{storeId}\",\"catalogId\":\"{catalogId}\",\"langId\":\"-1\",\"emailaddr\":\"BITROGUE@MAILINATOR.COM\",\"URL\":\"email-confirmation\",\"response\":\"accept_all::true:false\"}",
		"LAST");
	lr_end_sub_transaction("T13_Submit_Shipping_Address_addSignUpEmail", 2);


	 
 
	web_reg_find("TEXT/IC=updateShippingMethodSelectionResponse", "SaveCount=apiCheck1", "LAST");
	if ( strlen( lr_eval_string("{apiCheck1}") ) >  0 )
	{
		lr_end_transaction ( "T13_Submit_Shipping_Address Guest" , 0) ;
		return 0;
	}
	else {
 
		lr_end_transaction ( "T13_Submit_Shipping_Address Guest", 1 ) ;
		return 1;
	}

}

int BillingAsGuest()  
{
	lr_think_time ( FORM_TT ) ;
	 
	lr_start_transaction ( "T15_Submit Billing Address Guest" ) ;

	lr_start_sub_transaction("T15_Submit_Billing_updateAddress", "T15_Submit Billing Address Guest");
		# 2712 "..\\..\\checkoutFunctions.c"


	registerErrorCodeCheck();
	addHeader();
	web_add_header( "nickName", lr_eval_string("{nickName}") );
	 
	web_custom_request("updateAddress",
		"URL=https://{api_host}/payment/updateAddress",
		"Method=PUT",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTTP",
		"EncType=application/json",
		"Body={\"addressId\":\"{addressId}\",\"fromPage\":\"checkout\"}",
		"LAST");
	lr_end_sub_transaction("T15_Submit_Billing_updateAddress", 2);

	lr_start_sub_transaction("T15_Submit_Billing_addPaymentInstruction", "T15_Submit Billing Address Guest");

		# 2742 "..\\..\\checkoutFunctions.c"


	addHeader();
	web_add_header( "nickName", lr_eval_string("{nickName}") );
	web_add_header( "isRest", "true" );
	web_add_header( "savePayment", "false" );
	web_add_header( "identifier", "true" );
	web_reg_save_param("orderId", "LB=\"orderId\": \"", "RB=\"", "NotFound=Warning", "LAST");
	web_custom_request("addPaymentInstruction",
		"URL=https://{api_host}/payment/addPaymentInstruction",
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTTP",
		"EncType=application/json", "Body={\"paymentInstruction\":[{\"billing_address_id\":\"{addressId}\",\"piAmount\":\"{piAmount}\",\"expire_month\":\"12\",\"payMethodId\":\"VISA\",\"cc_brand\":\"VISA\",\"expire_year\":\"2031\",\"account\":\"2818526303813893\",\"isDefault\":\"false\",\"cc_cvc\":\"111\"}]}", "LAST");

	lr_end_sub_transaction("T15_Submit_Billing_addPaymentInstruction", 2);
 

	lr_start_sub_transaction("T15_Submit_Billing_getOrderDetails", "T15_Submit Billing Address Guest");
 




	addHeader();
	web_add_header("locStore", "True");
	web_add_header("pageName", "fullOrderInfo");
	web_reg_save_param("piAmount", "LB=\"piAmount\": \"", "RB=\",\n", "NotFound=Warning", "LAST");  
	web_reg_find("TEXT/IC={", "SaveCount=apiCheck", "LAST");
	web_custom_request("getOrderDetails",
		"URL=https://{api_host}/getOrderDetails",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T15_Submit_Billing_getOrderDetails", 1);
	else
		lr_end_sub_transaction("T15_Submit_Billing_getOrderDetails", 2);

	lr_start_sub_transaction("T15_Submit_Billing_getPointsService", "T15_Submit Billing Address Guest");

	addHeader();
	web_reg_find("TEXT/IC=LoyaltyWebsiteInd", "SaveCount=apiCheck", "LAST");
	web_custom_request("getPointsService",
		"URL=https://{api_host}/payment/getPointsService",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T15_Submit_Billing_getPointsService", 1);
	else
		lr_end_sub_transaction("T15_Submit_Billing_getPointsService", 2);

	 

	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 )  
	{
		lr_end_transaction ( "T15_Submit Billing Address Guest" , 0) ;
		return 0;
	}
	else {
		lr_fail_trans_with_error( lr_eval_string("T15_Submit Billing Address Guest Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
		lr_end_transaction ( "T15_Submit Billing Address Guest", 1 ) ;
		return 1;
	}

}


int submitBillingRegistered()
{
	lr_think_time ( FORM_TT ) ;
	lr_start_transaction ( "T15_Submit Billing Address Registered" ) ;
	 

	lr_start_sub_transaction("T15_Submit_Billing_updateAddress", "T15_Submit Billing Address Registered");

		# 2833 "..\\..\\checkoutFunctions.c"


	registerErrorCodeCheck();
	web_add_header( "nickName", lr_eval_string("{nickName}") );
	addHeader();

	web_custom_request("updateAddress",
		"URL=https://{api_host}/payment/updateAddress",
		"Method=PUT",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTTP",
		"EncType=application/json",
		"Body={\"addressId\":\"{addressIdAll_2}\",\"fromPage\":\"checkout\"}",
		"LAST");
	lr_end_sub_transaction("T15_Submit_Billing_updateAddress", 2);

	 
	lr_start_sub_transaction ( "T15_Submit_Billing_addPaymentInstruction", "T15_Submit Billing Address Registered" ) ;
		# 2863 "..\\..\\checkoutFunctions.c"


	addHeader();
 
	web_reg_save_param("orderId", "LB=\"orderId\": \"", "RB=\"", "NotFound=Warning", "LAST");  
	web_add_header( "isRest", "true" );   
	web_add_header( "nickName", lr_eval_string("Billing_{storeId}_{randomNineDigits}{randomFourDigits}") );  

	web_custom_request("addPaymentInstruction",
		"URL=https://{api_host}/payment/addPaymentInstruction",
		"Method=POST",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTML",
		"EncType=application/json", 	"Body={\"paymentInstruction\":[{\"billing_address_id\":\"{addressIdAll_2}\",\"piAmount\":\"{piAmount}\",\"payMethodId\":\"VISA\",\"cc_brand\":\"VISA\",\"account\":\"2818526303813893\",\"expire_month\":\"2\",\"expire_year\":\"2022\",\"cc_cvc\":\"111\"}]}",
		"LAST");
	lr_end_sub_transaction ( "T15_Submit_Billing_addPaymentInstruction", 2) ;
 

	lr_start_sub_transaction ( "T15_Submit_Billing_getPointsService", "T15_Submit Billing Address Registered" );

		


	addHeader();
	web_reg_find("TEXT/IC=LoyaltyWebsiteInd", "SaveCount=apiCheck", "LAST");
	web_custom_request("getPointsService",
		"URL=https://{api_host}/payment/getPointsService",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T15_Submit_Billing_getPointsService", 1);
	else
		lr_end_sub_transaction("T15_Submit_Billing_getPointsService", 2);

	lr_start_sub_transaction("T15_Submit_Billing_getOrderDetails", "T15_Submit Billing Address Registered");

		


	addHeader();
	web_add_header("calc", "True");
	web_add_header("locStore", "True");
	web_add_header("pageName", "fullOrderInfo");
	web_reg_save_param("piAmount", "LB=\"piAmount\": \"", "RB=\",\n", "NotFound=Warning", "LAST");  
	web_reg_find("TEXT/IC={", "SaveCount=apiCheck", "LAST");
	web_custom_request("getOrderDetails",
		"URL=https://{api_host}/getOrderDetails",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T15_Submit_Billing_getOrderDetails", 1);
	else
		lr_end_sub_transaction("T15_Submit_Billing_getOrderDetails", 2);

	 
	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 )  
	{
		lr_end_transaction ("T15_Submit Billing Address Registered" , 0 ) ;
		return 0;
	}
	else
	{
		lr_fail_trans_with_error(  lr_eval_string ("T15_Submit Billing Address Registered Failed with Error Code:  \"{errorCode}\", Error Message: {errorMessage}") ) ;
		lr_end_transaction ( lr_eval_string ( "T15_Submit Billing Address Registered" ) , 1 ) ;
		return 1;
	}

}

int submitOrderAsGuest()
{
	int rc;
	lr_think_time ( FORM_TT ) ;
 
	 

	lr_start_transaction ( "T16_Submit_Order" ) ;

	if (strcmp( lr_eval_string("{largeCart}"), "true") == 0 )
		lr_save_string("T16_Submit_Order_Large_Cart", "T16_Submit_Order_Sub");
	else if (strcmp( lr_eval_string("{addBopisToCart}"), "true") == 0 )
		lr_save_string("T16_Submit_Order_Bopis", "T16_Submit_Order_Sub");
	else
		lr_save_string("T16_Submit_Order_Ecom", "T16_Submit_Order_Sub");

	lr_start_sub_transaction ( lr_eval_string("{T16_Submit_Order_Sub}_S01_addCheckout"), "T16_Submit_Order" ) ;

		# 2969 "..\\..\\checkoutFunctions.c"


	registerErrorCodeCheck();
	addHeader();
	web_add_header( "Content-Type", "application/json" );

	web_custom_request("addCheckout",
		"URL=https://{api_host}/addCheckout",
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTTP",
		"EncType=application/json",
		"Body={\"orderId\":\"{orderId}\",\"isRest\":\"true\",\"locStore\":\"True\"}",
		"LAST");

	lr_end_sub_transaction ( lr_eval_string("{T16_Submit_Order_Sub}_S01_addCheckout"), 2 ) ;

	rc = web_get_int_property(1);
	lr_save_int(rc, "httpReturnCode");

	lr_save_string("T16_Submit_Order","mainTransaction");
	tcp_api2("tcpproduct/getPriceDetails", "GET",  lr_eval_string("{mainTransaction}") );

	 
 

	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 )  
	{
		lr_end_transaction ("T16_Submit_Order", 2 ) ;
		return 0;
	}
	else {
		lr_fail_trans_with_error( lr_eval_string ("{T16_Submit_Order} Guest Failed with http Error Code: {httpReturnCode}, api Error Code:  \"{errorCode}\", Error Message: {errorMessage}, OrderId:{orderId}") ) ;
		lr_end_transaction ("T16_Submit_Order", 1 ) ;

		lr_start_transaction ( "T16_Submit_Order" ) ;

		if (strcmp( lr_eval_string("{largeCart}"), "true") == 0 )
			lr_save_string("T16_Submit_Order_Large_Cart", "T16_Submit_Order_Sub");
		else if (strcmp( lr_eval_string("{addBopisToCart}"), "true") == 0 )
			lr_save_string("T16_Submit_Order_Bopis", "T16_Submit_Order_Sub");
		else
			lr_save_string("T16_Submit_Order_Ecom", "T16_Submit_Order_Sub");

		lr_start_sub_transaction ( lr_eval_string("{T16_Submit_Order_Sub}_S01_addCheckout"), "T16_Submit_Order" ) ;

				# 3027 "..\\..\\checkoutFunctions.c"


		registerErrorCodeCheck();
		addHeader();
		web_add_header( "Content-Type", "application/json" );

		web_custom_request("addCheckout",
			"URL=https://{api_host}/addCheckout",
			"Method=POST",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTTP",
			"EncType=application/json",
			"Body={\"orderId\":\"{orderId}\",\"isRest\":\"true\",\"locStore\":\"True\"}",
			"LAST");
		lr_end_sub_transaction( lr_eval_string("{T16_Submit_Order_Sub}_S01_addCheckout"),  2 );

		rc = web_get_int_property(1);
		lr_save_int(rc, "httpReturnCode");

		lr_save_string("T16_Submit_Order","mainTransaction");
		tcp_api2("tcpproduct/getPriceDetails", "GET",  lr_eval_string("{mainTransaction}") );

		if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 )  
		{
			lr_end_transaction ("T16_Submit_Order", 2 ) ;
			return 0;
		}else{
			lr_end_transaction ("T16_Submit_Order", 1 ) ;
			return 1;
		}
	}
}

int submitOrderRegistered()
{
	int rc;
	lr_think_time ( FORM_TT ) ;
 
	 

	lr_start_transaction ( "T16_Submit_Order" ) ;

	if (strcmp( lr_eval_string("{largeCart}"), "true") == 0 )
		lr_save_string("T16_Submit_Order_Large_Cart", "T16_Submit_Order_Sub");
	else if (strcmp( lr_eval_string("{addBopisToCart}"), "true") == 0 )
		lr_save_string("T16_Submit_Order_Bopis", "T16_Submit_Order_Sub");
	else
		lr_save_string("T16_Submit_Order_Ecom", "T16_Submit_Order_Sub");

	lr_start_sub_transaction ( lr_eval_string("{T16_Submit_Order_Sub}_S01_addCheckout"), "T16_Submit_Order" ) ;

		# 3090 "..\\..\\checkoutFunctions.c"


	registerErrorCodeCheck();
	addHeader();
	web_reg_find("TEXT/IC={", "SaveCount=apiCheck", "LAST");
	web_custom_request("addCheckout",
		"URL=https://{api_host}/addCheckout",
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTTP",
		"EncType=application/json",
		"Body={\"orderId\":\"{orderId}\",\"isRest\":\"true\",\"locStore\":\"True\"}",
		"LAST");

	lr_end_sub_transaction(lr_eval_string("{T16_Submit_Order_Sub}_S01_addCheckout"),  2 );

	rc = web_get_int_property(1);
	lr_save_int(rc, "httpReturnCode");

	lr_save_string("T16_Submit_Order","mainTransaction");
	tcp_api2("tcpproduct/getPriceDetails", "GET",  lr_eval_string("{mainTransaction}") );
	 
 

	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 )  
	{
		lr_end_transaction ( "T16_Submit_Order", 0 ) ;
		ONLINE_ORDER_SUBMIT=1;
		return 0;
	}
	else {
		lr_fail_trans_with_error( lr_eval_string ("{T16_Submit_Order} Failed with http Error Code: {httpReturnCode}, api Error Code:  \"{errorCode}\", Error Message: {errorMessage}, Email: {userEmail}, OrderId:{orderId}") ) ;
		lr_end_transaction ( "T16_Submit_Order", 1 ) ;
		ONLINE_ORDER_SUBMIT=0;

		lr_start_transaction ( "T16_Submit_Order" ) ;

		if (strcmp( lr_eval_string("{largeCart}"), "true") == 0 )
			lr_save_string("T16_Submit_Order_Large_Cart", "T16_Submit_Order_Sub");
		else if (strcmp( lr_eval_string("{addBopisToCart}"), "true") == 0 )
			lr_save_string("T16_Submit_Order_Bopis", "T16_Submit_Order_Sub");
		else
			lr_save_string("T16_Submit_Order_Ecom", "T16_Submit_Order_Sub");

		lr_start_sub_transaction ( lr_eval_string("{T16_Submit_Order_Sub}_S01_addCheckout"), "T16_Submit_Order" ) ;

				# 3148 "..\\..\\checkoutFunctions.c"


		registerErrorCodeCheck();
		addHeader();
		web_add_header( "Content-Type", "application/json" );
		web_reg_find("TEXT/IC={", "SaveCount=apiCheck", "LAST");

		web_custom_request("addCheckout",
			"URL=https://{api_host}/addCheckout",
			"Method=POST",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTTP",
			"EncType=application/json",
			"Body={\"orderId\":\"{orderId}\",\"isRest\":\"true\",\"locStore\":\"True\"}",
			"LAST");
		lr_end_sub_transaction(lr_eval_string("{T16_Submit_Order_Sub}_S01_addCheckout"),  2 );

		rc = web_get_int_property(1);
		lr_save_int(rc, "httpReturnCode");

		lr_save_string("T16_Submit_Order","mainTransaction");
		tcp_api2("tcpproduct/getPriceDetails", "GET",  lr_eval_string("{mainTransaction}") );
 
		if ( strlen( lr_eval_string("{apiCheck}") ) > 0 )  
		{
			lr_end_transaction ("T16_Submit_Order", 2 ) ;
			return 0;
		}else{
			lr_end_transaction ("T16_Submit_Order", 1 ) ;
			return 1;
		}
	}

}

void viewReservationHistory()
{
	lr_think_time ( LINK_TT );
	 
	 

	lr_start_transaction("T18_ViewReservationHistory");

	web_url("TCPDOMMyReservationHistoryView",
		"URL=https://{host}/webapp/wcs/stores/servlet/TCPDOMMyReservationHistoryView?storeId={storeId}&catalogId={catalogId}&langId=-1&sortRank=&sortKey=&curentPage=1&pageLength=1000",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		"LAST");

		lr_start_sub_transaction("T18_ViewReservationHistory_S01_getReservationHistory", "T18_ViewReservationHistory" );
	addHeader();
	web_add_header("fromRest", "true");
	web_custom_request("getReservationHistory",
		"URL=https://{api_host}/payment/getReservationHistory",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		"EncType=application/json",
		"LAST");

	lr_end_sub_transaction("T18_ViewReservationHistory_S01_getReservationHistory",2);


	lr_end_transaction("T18_ViewReservationHistory",2);

}

void viewPointsHistory()
{
	lr_think_time ( LINK_TT );

	lr_start_transaction("T18_ViewPointsHistory");

	web_custom_request("TCPMyPointsHistoryView",
		"URL=https://{host}/shop/TCPMyPointsHistoryView?catalogId={catalogId}&langId=-1&storeId={storeId}",
		"Method=POST",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		"EncType=",
		"LAST");

	lr_end_transaction("T18_ViewPointsHistory",2);
}


void viewOrderStatusGuest()
{
	 
	lr_think_time ( LINK_TT );

	lr_save_string("T08_ViewMyAccount_Orders_Details", "mainTransaction");

	lr_start_transaction("T08_ViewMyAccount_Orders_Details");

	lr_start_sub_transaction("T08_ViewMyAccount_Orders_Details_track-order", "T08_ViewMyAccount_Orders_Details" );
	web_reg_find("TEXT/IC={orderId}", "SaveCount=apiCheck", "LAST");
	web_url("T18_ViewOrderStatus",
		"URL=https://{host}/{country}/track-order/{orderId}/bitrogue@mailinator.com",  
		"TargetFrame=_self",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		"LAST");

	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T08_ViewMyAccount_Orders_Details_track-order", 1);
	else
		lr_end_sub_transaction("T08_ViewMyAccount_Orders_Details_track-order", 2);

	 

	if (ESPOT_FLAG == 1)
	{

		lr_start_sub_transaction ("T08_ViewMyAccount_Orders_Details_getESpot","T08_ViewMyAccount_Orders_Details" );

				


		web_add_header("espotName","GlobalHeaderBannerAboveHeader,GlobalHeaderBannerWithinHeader,GlobalHeaderLogo,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,create_account_drawer,MinicartLoginBanner,MinicartRegisterBanner,drawer_myaccount_overview_banner,coupon_help,bag_empty_banner,drawer_empty_bag,MinicartLoginFromFavoritesBanner,bag_ledger_promo_banner,gift_services_espot,CheckoutConfirmationBanner,CheckoutConfirmationMPRPromo,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1");
		web_add_header("deviceType","desktop");

 
		addHeader();
		web_reg_find("TEXT/IC=espotName", "SaveCount=apiCheck", "LAST");
		web_custom_request("getESpots",
			"URL=https://{api_host}/getESpot",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			"LAST");
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction("T08_ViewMyAccount_Orders_Details_getESpot", 1);
		else
			lr_end_sub_transaction("T08_ViewMyAccount_Orders_Details_getESpot", 2);

		api_getESpot_second();
	}
		lr_start_sub_transaction ( "T08_ViewMyAccount_Orders_Details_getRegisteredUserDetailsInfo", "T08_ViewMyAccount_Orders_Details" );
 




		addHeader();
		web_reg_find("TEXT/IC=resourceName", "SaveCount=apiCheck", "LAST");
		web_custom_request("getRegisteredUserDetailsInfo",
			"URL=https://{api_host}/payment/getRegisteredUserDetailsInfo",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			"LAST");
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction("T08_ViewMyAccount_Orders_Details_getRegisteredUserDetailsInfo", 1);
		else
			lr_end_sub_transaction("T08_ViewMyAccount_Orders_Details_getRegisteredUserDetailsInfo", 2);

		lr_start_sub_transaction ( "T08_ViewMyAccount_Orders_Details_getPointsService", "T08_ViewMyAccount_Orders_Details" );
 




		addHeader();
		web_reg_find("TEXT/IC=LoyaltyWebsiteInd", "SaveCount=apiCheck", "LAST");
		web_custom_request("getPointsService",
			"URL=https://{api_host}/payment/getPointsService",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			"LAST");
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction("T08_ViewMyAccount_Orders_Details_getPointsService", 1);
		else
			lr_end_sub_transaction("T08_ViewMyAccount_Orders_Details_getPointsService", 2);

		lr_start_sub_transaction("T08_ViewMyAccount_Orders_Details_getOrderDetails", "T08_ViewMyAccount_Orders_Details" );
 




		addHeader();
		web_add_header("locStore", "True");
		web_add_header("pageName", "fullOrderInfo");
 
		web_reg_find("TEXT/IC={", "SaveCount=apiCheck", "LAST");
		web_custom_request("getOrderDetails",
			"URL=https://{api_host}/getOrderDetails",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			"LAST");
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction("T08_ViewMyAccount_Orders_Details_getOrderDetails", 1);
		else
			lr_end_sub_transaction("T08_ViewMyAccount_Orders_Details_getOrderDetails", 2);


		lr_start_sub_transaction("T08_ViewMyAccount_Orders_Details_orderLookUp", "T08_ViewMyAccount_Orders_Details" );
		addHeader();
		web_add_header("orderId", lr_eval_string("{orderId}"));
		web_add_header("emailId", "bitrogue@mailinator.com");
		web_reg_find("TEXT/IC=orderLookupResponse", "SaveCount=apiCheck", "LAST");
		web_custom_request("tcporder/orderLookUp",
			"URL=https://{api_host}/tcporder/orderLookUp",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			"LAST");
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction("T08_ViewMyAccount_Orders_Details_orderLookUp", 1);
		else
			lr_end_sub_transaction("T08_ViewMyAccount_Orders_Details_orderLookUp", 2);

		 
		 

		 

		lr_end_transaction("T08_ViewMyAccount_Orders_Details", 2);

		 

 
} 

void logoff()
{

	addHeader();
	web_add_header( "Content-Type", "application/json" );
	 
	lr_start_transaction("T21_Logoff");
	web_custom_request("logout",
		"URL=https://{api_host}/logout",
		"Method=DELETE",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTTP",
		"LAST");

	if (ESPOT_FLAG == 1)
	{
		lr_start_sub_transaction ( "T21_Logoff_getESpot", "T21_Logoff" );

				


		web_add_header("espotName","GlobalHeaderBannerAboveHeader,GlobalHeaderBannerWithinHeader,GlobalHeaderLogo,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,create_account_drawer,MinicartLoginBanner,MinicartRegisterBanner,drawer_myaccount_overview_banner,coupon_help,bag_empty_banner,drawer_empty_bag,MinicartLoginFromFavoritesBanner,bag_ledger_promo_banner,gift_services_espot,CheckoutConfirmationBanner,CheckoutConfirmationMPRPromo,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1");
		web_add_header("deviceType","desktop");

	 
		addHeader();
		web_reg_find("TEXT/IC=espotName", "SaveCount=apiCheck", "LAST");
		web_custom_request("getESpots",
			"URL=https://{api_host}/getESpot",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			"LAST");
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction("T21_Logoff_getESpot", 1);
		else
			lr_end_sub_transaction("T21_Logoff_getESpot", 2);

		api_getESpot_second();
	}

	lr_start_sub_transaction ("T21_Logoff_getOrderDetails","T21_Logoff");
 




	addHeader();
	web_add_header("locStore", "False");
	web_add_header("pageName", "orderSummary");
 
	web_url("getOrderDetails",
		"URL=https://{api_host}/getOrderDetails",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	lr_end_sub_transaction ("T21_Logoff_getOrderDetails",2);

	lr_start_sub_transaction ( "T21_Logoff_getRegisteredUserDetailsInfo", "T21_Logoff" );
 




	addHeader();
	 
	 
	web_reg_find("TEXT/IC=resourceName", "SaveCount=apiCheck", "LAST");
	web_custom_request("getRegisteredUserDetailsInfo",
		"URL=https://{api_host}/payment/getRegisteredUserDetailsInfo",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T21_Logoff_getRegisteredUserDetailsInfo", 1);
	else
		lr_end_sub_transaction("T21_Logoff_getRegisteredUserDetailsInfo", 2);

	lr_start_sub_transaction ( "T21_Logoff_getPointsService", "T21_Logoff" );
 




	addHeader();
	web_reg_find("TEXT/IC=LoyaltyWebsiteInd", "SaveCount=apiCheck", "LAST");
	web_custom_request("getPointsService",
		"URL=https://{api_host}/payment/getPointsService",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T21_Logoff_getPointsService", 1);
	else
		lr_end_sub_transaction("T21_Logoff_getPointsService", 2);

	lr_end_transaction("T21_Logoff", 2);
 
# 3491 "..\\..\\checkoutFunctions.c"
	 

}

int createAccount()  
{
	lr_think_time ( FORM_TT ) ;

	 

	lr_start_transaction ( "T17_Register_User" ) ;

	 
	registerErrorCodeCheck();

	addHeader();
	lr_start_sub_transaction ( "T17_Register_User_addCustomerRegistration", "T17_Register_User" );
	web_custom_request("addCustomerRegistration",
		"URL=https://{api_host}/addCustomerRegistration",
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTTP",
		"EncType=application/json",
		"Body={\"firstName\":\"Joe\",\"lastName\":\"User\",\"zipCode\":\"07094\",\"logonId\":\"bitrogue@mailinator.com\",\"logonPassword\":\"Asdf!234\",\"phone1\":\"2014531513\",\"rememberCheck\":true,\"rememberMe\":true,\"catalogId\":\"{catalogId}\",\"langId\":\"-1\",\"storeId\":\"{storeId}\"}",
		"LAST");
	lr_end_sub_transaction("T17_Register_User_addCustomerRegistration", 2);
 

	lr_save_string("T17_Register_User", "mainTransaction");

	tcp_api2("payment/getRegisteredUserDetailsInfo", "GET",  lr_eval_string("{mainTransaction}") );  
	 
	 

	web_add_header("locStore", "True");
	web_add_header("pageName", "fullOrderInfo");
	web_reg_save_param("cartCount", "LB=cartCount\": ", "RB=,", "NotFound=Warning", "LAST");			 
	tcp_api2("getOrderDetails", "GET", lr_eval_string("{mainTransaction}") );                       

	if (ESPOT_FLAG == 1)
	{
		tcp_api2("getESpot", "GET", lr_eval_string("{mainTransaction}") );                              
	}

	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 )  
	{
		lr_end_transaction ( "T17_Register_User", 2 ) ;
		return 0;
	}
	else {
		lr_fail_trans_with_error( lr_eval_string ("T17_Register_User Failed with Error Code:  \"{errorCode}\", Error Message: {errorMessage}") ) ;
		lr_end_transaction ( "T17_Register_User", 1 ) ;
	}

	 

	return 0;
}


void buildCartDrop(int userProfile)
{
	int iLoop = 0;
	lr_think_time ( FORM_TT ) ;

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CART_MERGE ) {  
		target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_MERGE_CART_SIZE}"));
	}
	else {
		target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_DROP_CART_SIZE}"));
	}
	if ( strcmp(strupr(lr_eval_string("{buildCart}")),  "TRUE") == 0) {
		target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_BUILD_CART}"));
	}

	iLoop = target_itemsInCart + 2;

	lr_start_transaction("T20_New_Cart");
	lr_end_transaction("T20_New_Cart", 0);

	if ( userProfile == 0 && atoi(lr_eval_string("{cartCount}")) == 1) {
		orderItemIdscount = 0;
	}
	else {
		if (atoi(lr_eval_string("{cartCount}")) == 1) {
			orderItemIdscount = 0;
		}
		else
			orderItemIdscount = atoi(lr_eval_string("{cartCount}"));
	}

	if (  orderItemIdscount < target_itemsInCart ) {

	    target_itemsInCart = target_itemsInCart - orderItemIdscount ;

		for(index_buildCart=0; index_buildCart < target_itemsInCart ; index_buildCart++)
		{
			topNav();
			if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_BUILDCART_DRILLDOWN )
				drill();

			paginate();

 







			if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ADDTOCART_PLP )
				addToCartFromPLP();
			else {
				productDisplay();
				addToCart();
			}
			iLoop--;

			if (iLoop == 0)
			    break;

			if (atc_Stat == 1)   
				index_buildCart--;

		}  

		viewCart();  
	}
}  

void buildCartDropNoBrowse(int userProfile)
{
	int iLoop = 0;
	lr_think_time ( FORM_TT ) ;

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CART_MERGE ) {  
		target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_MERGE_CART_SIZE}"));
	}
	else {
		target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_DROP_CART_SIZE}"));
	}
	if ( strcmp(strupr(lr_eval_string("{buildCart}")),  "TRUE") == 0) {
		target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_BUILD_CART}"));
	}

	iLoop = target_itemsInCart + 2;

	lr_start_transaction("T20_New_Cart");
	lr_end_transaction("T20_New_Cart", 0);

	if ( userProfile == 0 && atoi(lr_eval_string("{cartCount}")) == 1) {
		orderItemIdscount = 0;
	}
	else {
		if (atoi(lr_eval_string("{cartCount}")) == 1) {
			orderItemIdscount = 0;
		}
		else
			orderItemIdscount = atoi(lr_eval_string("{cartCount}"));
	}

	if (  orderItemIdscount < target_itemsInCart ) {

	    target_itemsInCart = target_itemsInCart - orderItemIdscount ;

		for(index_buildCart=0; index_buildCart < target_itemsInCart ; index_buildCart++)
		{
			topNav();

			if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_BUILDCART_DRILLDOWN )
				drill();

			paginate();

			productDisplay();

			addToCartNoBrowse();
			iLoop--;

			if (iLoop == 0)
			    break;

			if (atc_Stat == 1)   
				index_buildCart--;

		}  

 
	}
}  

void buildCartCheckout(int userProfile)
{
	int iLoop, cartTypeRatio = 0;
	lr_think_time ( FORM_TT ) ;
	lr_save_string("false", "largeCart");

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_BUILDCART_LARGE ) {
		target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_LARGE_CART_SIZE}"));
		lr_save_string("true", "largeCart");
	}
	else if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CART_MERGE ) {
		target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_MERGE_CART_SIZE}"));
	}
	else {
		target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_CART_SIZE}"));
	}

	iLoop = target_itemsInCart + 2;

	lr_start_transaction("T20_New_Cart");
	lr_end_transaction("T20_New_Cart", 0);

	if ( userProfile == 0 && atoi(lr_eval_string("{cartCount}")) == 1) {  
		orderItemIdscount = 0;
	}
	else {
		if (atoi(lr_eval_string("{cartCount}")) == 1) {
			orderItemIdscount = 0;
		}
		else
			orderItemIdscount = atoi(lr_eval_string("{cartCount}")); 
	}

	if (  orderItemIdscount < target_itemsInCart ) {

	    target_itemsInCart = target_itemsInCart - orderItemIdscount ;
 
 

		cartTypeRatio =  atoi(lr_eval_string("{RANDOM_PERCENT}"));  

		for(index_buildCart=0; index_buildCart < target_itemsInCart ; index_buildCart++)
		{
			topNav();

			if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_BUILDCART_DRILLDOWN )
				drill();

			paginate();

 






			if ( cartTypeRatio <= ECOMM_CART_RATIO ) {
				if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ADDTOCART_PLP )
					addToCartFromPLP();
				else {
					productDisplay();
					addToCart();
				}
			}
			else {
				productDisplayBOPIS();
				addToCartMixed();
			}

			iLoop--;

			if (iLoop == 0)
			    break;

			if (atc_Stat == 1) {  
				index_buildCart--;
				lr_start_transaction("T40_Failed_Add_Cart");
				lr_end_transaction("T40_Failed_Add_Cart", 0);

			}

		}  

 

	}

}  

void buildCartCheckoutNoBrowse(int userProfile)
{
	int iLoop, cartTypeRatio = 0;
	lr_think_time ( FORM_TT ) ;
	lr_save_string("false", "largeCart");

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_BUILDCART_LARGE ) {
		target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_LARGE_CART_SIZE}"));
		lr_save_string("true", "largeCart");
	}
	else if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CART_MERGE ) {
		target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_MERGE_CART_SIZE}"));
	}
	else {
		target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_CART_SIZE}"));
	}

	iLoop = target_itemsInCart + 2;

	lr_start_transaction("T20_New_Cart");
	lr_end_transaction("T20_New_Cart", 0);

	if ( userProfile == 0 && atoi(lr_eval_string("{cartCount}")) == 1) {  
		orderItemIdscount = 0;
	}
	else {
		if (atoi(lr_eval_string("{cartCount}")) == 1) {
			orderItemIdscount = 0;
		}
		else
			orderItemIdscount = atoi(lr_eval_string("{cartCount}")); 
	}

	if (  orderItemIdscount < target_itemsInCart ) {

	    target_itemsInCart = target_itemsInCart - orderItemIdscount ;

		cartTypeRatio =  atoi(lr_eval_string("{RANDOM_PERCENT}"));  

		for(index_buildCart=0; index_buildCart < target_itemsInCart ; index_buildCart++)
		{
			topNav();
			if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_BUILDCART_DRILLDOWN )
				drill();

			paginate();

			productDisplay();

			addToCartNoBrowse();

			iLoop--;

			if (iLoop == 0)
			    break;

			if (atc_Stat == 1) {  
				index_buildCart--;
				lr_start_transaction("T40_Failed_Add_Cart");
				lr_end_transaction("T40_Failed_Add_Cart", 0);

			}

		}  

 

	}

}  


void buildCartRopis()
{
	int randomPercent = atoi(lr_eval_string("{RANDOM_PERCENT}"));
	lr_think_time ( FORM_TT ) ;
 
# 3860 "..\\..\\checkoutFunctions.c"
		drill();
		productDisplay();
		if ( randomPercent <= RESERVE_ONLINE && atoi(lr_eval_string("{atc_catentryIds_count}")) != 0) {
			lr_save_string( lr_paramarr_random("atc_catentryIds"), "catentryId");
		}
		else if ( randomPercent <= RESERVE_ONLINE && atoi(lr_eval_string("{bopisCatEntryId_count}")) != 0) {
			lr_save_string( lr_paramarr_random("bopisCatEntryId_count"), "catentryId");
		}

 
}
 
# 3937 "..\\..\\checkoutFunctions.c"

int pickupDetailsGuest()  
{
	lr_think_time ( FORM_TT ) ;

	lr_start_transaction("T05_PickupInStore_Guest");
	web_reg_save_param("errorCode", "LB=\"errorCode\":\"", "RB=\"", "NotFound=Warning", "LAST");  
	web_reg_save_param("errorMessage", "LB=\"errorMessage\":\"", "RB=\"", "NotFound=Warning", "LAST");  

	addHeader();
	lr_start_sub_transaction("T05_PickupInStore_Guest addAddress","T05_PickupInStore_Guest");
	web_custom_request("addAddress",
		"URL=https://{api_host}/payment/addAddress",
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTTP",
		"EncType=application/json",
		"Body={\"contact\":[{\"addressType\":\"Shipping\",\"firstName\":\"Manny\",\"lastName\":\"Yamzon\",\"phone2\":\"2014531236\",\"email1\":\"bitrogue@mailinator.com\",\"email2\":\"\"}]}",
		"LAST");
	lr_end_sub_transaction("T05_PickupInStore_Guest addAddress", 2);
 

	lr_start_sub_transaction("T05_PickupInStore_Guest_getOrderDetails", "T05_PickupInStore_Guest");
 




	addHeader();
	 
	web_add_header("calc", "false");
	web_add_header("locStore", "True");
	web_add_header("pageName", "fullOrderInfo");
	web_reg_find("TEXT/IC={", "SaveCount=apiCheck", "LAST");
	web_url("getOrderDetails",
		"URL=https://{api_host}/getOrderDetails",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T05_PickupInStore_Guest_getOrderDetails", 1);
	else
		lr_end_sub_transaction("T05_PickupInStore_Guest_getOrderDetails", 2);
	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 )  
	{
		lr_end_transaction("T05_PickupInStore_Guest", 2);
		return 0;
	}
	else
	{
		lr_fail_trans_with_error( lr_eval_string("T05_PickupInStore_Guest getUserBopisStore Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
		lr_end_transaction("T05_PickupInStore_Guest", 1);
		return 1;
	}

}

int pickupInStore()
{
	lr_think_time ( FORM_TT ) ;
	lr_start_transaction ( "T05_PickupInStore" ) ;

	lr_save_string("T05_PickupInStore", "mainTransaction");

	lr_start_sub_transaction("T05_PickupInStore_getUserBopisStores", lr_eval_string("{mainTransaction}"));
	addHeader();
	web_reg_find("TEXT/IC={", "SaveCount=apiCheck", "LAST");
	web_url("getUserBopisStores",
		"URL=https://{api_host}/tcporder/getUserBopisStores",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");

	if (atoi(lr_eval_string("{apiCheck}")) == 0 ) {
		lr_end_sub_transaction("T05_PickupInStore_getUserBopisStores", 1);
		lr_end_transaction("T05_PickupInStore", 1);
		return 1;
	} else {
		lr_end_transaction("T05_PickupInStore_getUserBopisStores", 2);

 
 
		tcp_api2("payment/getPointsService", "GET",  lr_eval_string("{mainTransaction}") );
		 
		 

	 
	 
	 
	 
	web_add_header("sType","BOPIS");
	web_add_header("latitude","40.4333407");
	web_add_header("longitude","-74.4115246");
	
	 
		lr_start_sub_transaction("T05_PickupInStore_getStoreandProductInventoryInfo", lr_eval_string("{mainTransaction}"));
		web_reg_save_param("storeLocId","LB=storeUniqueID\": \"", "RB=\"", "Notfound=Warning", "LAST");
		web_reg_find("TEXT/IC=numStoresHavingItem", "SaveCount=apiCheck", "LAST");
		addHeader();
		web_url("getStoreandProductInventoryInfo",
 
 
            "URL=https://{api_host}/tcpstore/getStoreandProductInventoryInfo?latitude=40.4333407&longitude=-74.4115246&catentryId={atc_catentryId}&distance=75&country=US&sType=BOPIS",
			"Resource=0",
			"RecContentType=application/json",
			"LAST");
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction("T05_PickupInStore_getStoreandProductInventoryInfo", 1);
		else
			lr_end_sub_transaction("T05_PickupInStore_getStoreandProductInventoryInfo", 2);

		lr_end_transaction("T05_PickupInStore", 2);
	}

	return 0;

}

int pickupInStore_R3()
{
	lr_think_time ( FORM_TT ) ;
	lr_start_transaction ( "T05_PickupInStore" ) ;
	 
	 
	lr_start_sub_transaction("T05_PickupInStore_S01 TCPAutoPopulateAddressControllerCmd", "T05_PickupInStore" );
	web_custom_request("TCPAutoPopulateAddressControllerCmd",
		"URL=https://{host}/webapp/wcs/stores/servlet/TCPAutoPopulateAddressControllerCmd?storeId={storeId}&catalogId={catalogId}&langId=-1",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		"EncType=application/json",
		"LAST");
	lr_end_sub_transaction("T05_PickupInStore_S01 TCPAutoPopulateAddressControllerCmd",2);

	 
	 
		 
		 
		 
		 
		 
		 
	 

	lr_start_sub_transaction("T05_PickupInStore_S02 getUserBopisStores", "T05_PickupInStore" );
	web_custom_request("getUserBopisStores",
		"URL=https://{host}/wcs/resources/store/{storeId}/getUserBopisStores?storeId={storeId}&catalogId={catalogId}&langId=-1",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		"EncType=application/json",
		"LAST");
	lr_end_sub_transaction("T05_PickupInStore_S02 getUserBopisStores",2);

	lr_save_string(lr_paramarr_random( "productId"), "bopisProductId" );

	web_reg_save_param("errorCode", "LB=\"errorCode\":\"", "RB=\"", "NotFound=Warning", "LAST");  
	web_reg_save_param("errorMessage", "LB=\"errorMessage\":\"", "RB=\"", "NotFound=Warning", "LAST");  

	lr_start_sub_transaction("T05_PickupInStore_S03 GetSwatchesAndSizeInfo", "T05_PickupInStore" );
	web_custom_request("GetSwatchesAndSizeInfo",
 
		"URL=https://{host}/webapp/wcs/stores/servlet/GetSwatchesAndSizeInfo?storeId={storeId}&catalogId={catalogId}&langId=-1&productId={bopisProductId}",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		"EncType=application/json",
		"LAST");
	lr_end_sub_transaction("T05_PickupInStore_S03 GetSwatchesAndSizeInfo",2);

	lr_start_sub_transaction("T05_PickupInStore_S04 BOPIS_Search_Modal", "T05_PickupInStore" );
	web_custom_request("BOPIS_Search_Modal",
		"URL=https://{host}/wcs/resources/store/{storeId}/espots/BOPIS_Search_Modal?responseFormat=json&storeId={storeId}&catalogId={catalogId}&langId=-1",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		"EncType=application/json",
		"LAST");
	lr_end_sub_transaction("T05_PickupInStore_S04 BOPIS_Search_Modal",2);


	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 )  
	{
		lr_end_transaction ( "T05_PickupInStore" , 0) ;
		return 0;
	}
	else
	{
		lr_fail_trans_with_error( lr_eval_string("T05_PickupInStore_S02 getUserBopisStore Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
		lr_end_transaction ( "T05_PickupInStore", 1 ) ;
		return 1;
	}

}

int addBopisToCart()
{
	lr_think_time ( LINK_TT ) ;
	lr_start_transaction("T05_Add_To_Cart_Bopis");

	web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", "LAST");  
	web_reg_save_param("errorMessage", "LB=\"errorMessage\": \"", "RB=\",\n", "NotFound=Warning", "LAST");  
	web_reg_save_param("orderId", "LB=\"orderId\": \"", "RB=\"", "NotFound=Warning", "LAST");  
	web_reg_save_param("orderItemDescription", "LB=\"orderItemDescription\": \"", "RB=\"", "NotFound=Warning", "LAST");  

	addHeader();
	web_custom_request("createBopisOrder",
		"URL=https://{api_host}/tcporder/createBopisOrder",
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTTP",
		"EncType=application/json",
		"Body={\"storeLocId\":\"110005\",\"quantity\":\"1\",\"catEntryId\":\"{randCatEntryId_BOPIS}\",\"isRest\":\"false\"}",
		"LAST");
 
 

	addHeader();
	web_add_header("locStore", "True");
	web_add_header("pageName", "fullOrderInfo");
	web_url("getOrderDetails",
		"URL=https://{api_host}/getOrderDetails",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");

 
 
	tcp_api2("payment/getPointsService", "GET",  "T05_Add_To_Cart_Bopis" );
	 
	 

	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 )  
	{	lr_end_transaction("T05_Add_To_Cart_Bopis", 0);
		return 0;
	}
	else
	{
		lr_fail_trans_with_error( lr_eval_string("T05_Add_To_Cart_Bopis S02_createOrUpdateBOPISOrder Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
		lr_end_transaction("T05_Add_To_Cart_Bopis", 1);
		 
		return 1;
	}
}




int addBopisToCartR3()
{
	int i, itemIsAvailableInStore = 0;

	if ( strcmp( lr_eval_string("{storeId}") , "10151") == 0 )
	{
 
 
			lr_think_time ( LINK_TT ) ;
			lr_start_transaction("T05_Add_To_Cart_Bopis");

			lr_save_string( lr_eval_string("{atc_catentryId}"), "catentryId");
			lr_save_string( "40.8136765", "storeLocLatitude");
			lr_save_string( "-74.08288390000001", "storeLocLongitude");
			web_add_header( "X-Requested-With", "XMLHttpRequest");
			 
			 
			 

			web_reg_save_param("storeLocId", "LB=\"storeUniqueID\":\"", "RB=\"", "NotFound=Warning", "ORD=ALL","LAST");  
			web_reg_save_param("itemStatus", "LB=\"itemStatus\":\"", "RB=\"", "NotFound=Warning", "ORD=ALL","LAST");  
			web_reg_save_param("errorCode", "LB=\"errorCode\":\"", "RB=\"", "NotFound=Warning", "LAST");  
			web_reg_save_param("errorMessage", "LB=\"errorMessage\":\"", "RB=\"", "NotFound=Warning", "LAST");  

			 
			lr_start_sub_transaction("T05_Add_To_Cart_Bopis S01_GetStoreAndProductInventoryInfo", "T05_Add_To_Cart_Bopis" );
			web_custom_request("GetStoreAndProductInventoryInfo",
 
 
				"URL=https://{host}/webapp/wcs/stores/servlet/GetStoreAndProductInventoryInfo?storeId={storeId}&catalogId={catalogId}&langId=-1&catentryId={catentryId}&latitude={storeLocLatitude}&longitude={storeLocLongitude}&sType=BOPIS",
				"Method=GET",
				"TargetFrame=",
				"Resource=0",
				"RecContentType=text/html",
				"Mode=HTML",
				"EncType=application/json",
				"LAST");
			lr_end_sub_transaction("T05_Add_To_Cart_Bopis S01_GetStoreAndProductInventoryInfo",2);


			for (i=1; i <= atoi(lr_eval_string("{itemStatus_count}")); i++)
			{
				if (strcmp(lr_paramarr_idx("itemStatus",i),"AVAILABLE") == 0)
				{
					lr_save_string( lr_paramarr_idx("storeLocId", i ), "storeLocId");
					itemIsAvailableInStore = 1;
					break;
				}
			}

 
 
 
				lr_save_string( "787752", "catentryId");
				lr_save_string( "114298", "storeLocId");
 
 
 
 

			web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", "LAST");  
			web_reg_save_param("errorMessage", "LB=\"errorMsg\": \"", "RB=\"", "NotFound=Warning", "LAST");  

			lr_start_sub_transaction("T05_Add_To_Cart_Bopis S02_createOrUpdateBOPISOrder", "T05_Add_To_Cart_Bopis" );
			web_custom_request("createOrUpdateBOPISOrder",
				"URL=https://{host}/wcs/resources/store/{storeId}/bopisOrder/createOrUpdateBOPISOrder?storeId={storeId}&catalogId={catalogId}&langId=-1&storeLocId={storeLocId}&quantity=1&requesttype=ajax&isRest=true&catEntryId={catentryId}",
				"Method=POST",
				"TargetFrame=",
				"Resource=0",
				"RecContentType=application/json",
				"Mode=HTML",
				"EncType=application/json; charset=UTF-8",
				"Body={\"storeId\":\"{storeId}\",\"catalogId\":\"{catalogId}\",\"langId\":\"-1\",\"storeLocId\":\"{storeLocId}\",\"quantity\":\"1\",\"requesttype\":\"ajax\",\"isRest\":true,\"catEntryId\":\"{catentryId}\"}",
				"LAST");
			lr_end_sub_transaction("T05_Add_To_Cart_Bopis S02_createOrUpdateBOPISOrder",2);

			lr_start_sub_transaction("T05_Add_To_Cart_Bopis S03_TCPAdd2CartQuickView", "T05_Add_To_Cart_Bopis" );
			web_custom_request("TCPAdd2CartQuickView",
				"URL=https://{host}/webapp/wcs/stores/servlet/TCPAdd2CartQuickView?langId=-1&storeId={storeId}&catalogId={catalogId}&storeId={storeId}&catalogId={catalogId}&langId=-1",
				"Method=GET",
				"TargetFrame=",
				"Resource=0",
				"RecContentType=text/html",
				"Mode=HTML",
				"EncType=application/json",
				"LAST");
			lr_end_sub_transaction("T05_Add_To_Cart_Bopis S03_TCPAdd2CartQuickView",2);

			lr_start_sub_transaction("T05_Add_To_Cart_Bopis S04_CreateCookieCmd", "T05_Add_To_Cart_Bopis" );
			web_custom_request("CreateCookieCmd",
				"URL=https://{host}/webapp/wcs/stores/servlet/CreateCookieCmd?langId=-1&storeId={storeId}&goToPage=NoURL&catalogId={catalogId}&storeId={storeId}&catalogId={catalogId}&langId=-1",
				"Method=GET",
				"TargetFrame=",
				"Resource=0",
				"RecContentType=text/html",
				"Mode=HTML",
				"EncType=application/json",
				"LAST");
			lr_end_sub_transaction("T05_Add_To_Cart_Bopis S04_CreateCookieCmd",2);

			if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 )  
			{	lr_end_transaction("T05_Add_To_Cart_Bopis", 0);
				return 0;
			}
			else
			{
				lr_fail_trans_with_error( lr_eval_string("T05_Add_To_Cart_Bopis S02_createOrUpdateBOPISOrder Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
				lr_end_transaction("T05_Add_To_Cart_Bopis", 1);
				return 1;
			}
 
	}
	return 0;
}

int reserveOnline()
{
 	lr_think_time ( LINK_TT ) ;

	lr_start_transaction("T04_Product_Reserve_Online_FIND_IT");

	web_url("GetStoreAndProductInventoryInfo",
		"URL=https://{host}/webapp/wcs/stores/servlet/GetStoreAndProductInventoryInfo?storeId=10151&catalogId=10551&langId=-1&catentryId={catentryId}&latitude=33.9697897&longitude=-118.24681479999998",
 
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		"LAST");

	lr_end_transaction("T04_Product_Reserve_Online_FIND_IT",2);

 	lr_think_time ( LINK_TT ) ;

	web_reg_save_param("errorCode", "LB=\"errorCode\":\"", "RB=\"", "NotFound=Warning", "LAST");  
	web_reg_save_param("errorMessage", "LB=\"errorMessage\":\"", "RB=\"", "NotFound=Warning", "LAST");  


	lr_start_transaction("T04_Product_Reserve_Online_Submit");
	web_submit_data("TCPReservationSubmitCmd",
		"Action=https://{host}/webapp/wcs/stores/servlet/TCPReservationSubmitCmd",
		"Method=POST",
		"EncType=multipart/form-data",
		"RecContentType=text/html",
		"Mode=HTTP",
		"ITEMDATA",
		"Name=quantity", "Value=1", "ENDITEM",
		"Name=catEntryId", "Value={catentryId}", "ENDITEM",
		"Name=stlocId", "Value=111966", "ENDITEM",
		"Name=firstname", "Value=manny", "ENDITEM",
		"Name=lastname", "Value=paquiao", "ENDITEM",
		"Name=phone", "Value=2014531236", "ENDITEM",
		"Name=email", "Value={userEmail}", "ENDITEM",
		"Name=marketingEmail", "Value=1", "ENDITEM",
		"Name=mobileSignup", "Value=0", "ENDITEM",
		"Name=storeId", "Value={storeId}", "ENDITEM",
		"Name=catalogId", "Value={catalogId}", "ENDITEM",
		"Name=langId", "Value=-1", "ENDITEM",
		"LAST");
 
# 4361 "..\\..\\checkoutFunctions.c"
	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 )  
	{	lr_end_transaction("T04_Product_Reserve_Online_Submit", 0);
		return 0;
	}
	else
	{
		lr_fail_trans_with_error( lr_eval_string("T04_Product_Reserve_Online_Submit Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
		lr_end_transaction("T04_Product_Reserve_Online_Submit", 1);
		return 1;
	}

}

void inCartEdits()
{
	viewCart();

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_UPDATE_QUANTITY )
		updateQuantity();   

 
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_PROMO_APPLY && (strcmp( lr_eval_string("{couponExist}"), "FORTYOFF") != 0 && strcmp( lr_eval_string("{couponExist}"), "TESTCODE20") != 0) )
			applyPromoCode(0);

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_DELETE_PROMOCODE )
		deletePromoCode();

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_DELETE_ITEM )
			deleteItem();  

	if (isLoggedIn == 1) {

		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= MOVE_FROM_CART_TO_WISHLIST  ) {
			moveFromCartToWishlist();

 
 
		}
	}

	viewCart();

	return;
}


void wlGetProduct()
{
	drill();

	productDisplay();
}

void wlFavorites()
{
	int j;
 	lr_think_time ( LINK_TT ) ;

	lr_start_transaction("T19_Favorites");

	web_url("favorites",
		"URL=https://{host}{store_home_page}favorites",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		"LAST");

	lr_save_string("T19_Favorites", "mainTransaction");
 
 

	if (ESPOT_FLAG == 1)
	{
		web_add_header("deviceType", "desktop");
		web_add_header("espotName", "GlobalHeaderBannerAboveHeader,GlobalHeaderBannerWithinHeader,GlobalHeaderLogo,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,create_account_drawer,MinicartLoginBanner,MinicartRegisterBanner,drawer_myaccount_overview_banner,coupon_help,bag_empty_banner,drawer_empty_bag,MinicartLoginFromFavoritesBanner,bag_ledger_promo_banner,gift_services_espot,CheckoutConfirmationBanner,CheckoutConfirmationMPRPromo,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1");
		tcp_api2("getESpot", "GET", lr_eval_string("{mainTransaction}") );

		web_add_header("deviceType", "desktop");
		web_add_header("espotName", "DTEmptyFavorites");
		tcp_api2("getESpot", "GET", lr_eval_string("{mainTransaction}") );

		web_add_header("deviceType", "desktop");
		web_add_header("espotName", "GlobalHeaderBannerAboveHeader,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1");
		tcp_api2("getESpot", "GET", lr_eval_string("{mainTransaction}") );
	}
	 
	 
	web_add_header("locStore", "False");
	web_add_header("pageName", "orderSummary");
	web_reg_save_param("cartCount", "LB=cartCount\": ", "RB=,", "NotFound=Warning", "LAST");
	tcp_api2("getOrderDetails", "GET", lr_eval_string("{mainTransaction}") );
	tcp_api2("payment/getRegisteredUserDetailsInfo", "GET",  lr_eval_string("{mainTransaction}") );
	 

	web_reg_save_param("nameIdentifier", "LB=\"nameIdentifier\": \"", "RB=\"", "ORD=All", "NotFound=Warning", "LAST");  
	web_reg_save_param("status", "LB=\"status\": \"", "RB=\"", "ORD=All", "NotFound=Warning", "LAST");  
	web_reg_save_param("giftListExternalIdentifier", "LB=\"giftListExternalIdentifier\": ", "RB=,", "ORD=All", "NotFound=Warning", "LAST");  
	web_reg_save_param("itemCount", "LB=\"itemCount\": ", "RB=,", "NotFound=Warning", "LAST");  
	tcp_api2("tcpstore/getListofWishList", "GET",  lr_eval_string("{mainTransaction}") );

 
 
		web_reg_save_param("giftListItemID", "LB=\"giftListItemID\": \"", "RB=\"", "ORD=All", "NotFound=Warning", "LAST");  
		web_reg_save_param("productCatentryId", "LB=\"parentProductId\": \"", "RB=\"", "ORD=All", "NotFound=Warning", "LAST");  



 
	lr_end_transaction("T19_Favorites", 2);

}

void wlGetAll()
{
	if (lr_paramarr_len("WishlistIDs") == 0)  { 
	 
		web_reg_save_param("WishlistIDs", "LB=\"giftListExternalIdentifier\": ", "RB=,", "ORD=All", "NotFound=Warning", "LAST");
	 
	 
	 

		 
		 
		 
		lr_start_transaction("T19_Wishlist Get List");

			TCPGetWishListForUsersView();

			if (lr_paramarr_len("WishlistIDs") == 0)  { 

			web_reg_save_param("WishlistIDs", "LB=\"giftListId\": ", "RB=,", "ORD=All", "NotFound=Warning", "LAST");
		 
			web_url("AjaxGiftListServiceCreate",
				"URL=http://{host}/webapp/wcs/stores/servlet/AjaxGiftListServiceCreate?tcpCallBack=jQuery111309054115856997669_1446156866892&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&name=Joe%27s+Wishlist&_=1446156866894",
				"TargetFrame=",
				"Resource=0",
				"RecContentType=text/html",
				"Referer=",
				"Snapshot=t21.inf",
				"Mode=HTML",
				"LAST");

			if (lr_paramarr_len("WishlistIDs") == 0)   
				lr_end_transaction("T19_Wishlist Get List", 1);
			else
				lr_end_transaction("T19_Wishlist Get List", 0);
		}
		else
			lr_end_transaction("T19_Wishlist Get List", 0);

		if (lr_paramarr_len("WishlistIDs") > 0)
			lr_save_string(lr_paramarr_random("WishlistIDs"), "WishlistID");
	}

} 


void wlCreate()
{
	wlFavorites();

 	lr_think_time ( LINK_TT ) ;

	if (lr_paramarr_len("giftListExternalIdentifier") < 5)  {
	 
	 
	 
		lr_start_transaction("T19_Wishlist Create");

			addHeader();
			web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", "LAST");  
			web_reg_save_param("errorMessage", "LB=\"errorMessage\": \"", "RB=\",\n", "NotFound=Warning", "LAST");  
			web_reg_find("TEXT/IC=descriptionName","Fail=NotFound", "SaveCount=Mycount","LAST");
			web_custom_request("createWishListForUser",
				"URL=https://{api_host}/tcpproduct/createWishListForUser",
				"Method=POST",
				"Resource=0",
				"RecContentType=application/json",
				"Mode=HTTP",
				"EncType=application/json",
				"Body={\"descriptionName\":\"Perf_{WL_Random_Number}_WL\",\"state\":\"Active\"}",   
				"LAST");

			addHeader();
			web_add_header("externalId", lr_paramarr_idx("giftListExternalIdentifier",1) );
			web_url("getWishListbyId",
				"URL=https://{api_host}/tcpproduct/getWishListbyId",
				"Resource=0",
				"RecContentType=application/json",
				"LAST");

			addHeader();
			web_url("getListofWishList",
				"URL=https://{api_host}/tcpstore/getListofWishList",
				"Resource=0",
				"RecContentType=application/json",
				"LAST");

		if ( strcmp( lr_eval_string("{errorCode}") ,"") != 0 && strcmp(lr_eval_string("{errorMessage}") ,"") != 0  && (atoi(lr_eval_string("{Mycount}"))==0) )
		{
			lr_fail_trans_with_error( lr_eval_string("T19_Wishlist Create Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_transaction("T19_Wishlist Create", 1);
		}
		else
		{
			lr_end_transaction("T19_Wishlist Create", 2);
		}
	}

}

void wlDelete()
{
	wlFavorites();

 	lr_think_time ( LINK_TT ) ;
	if (lr_paramarr_len("giftListExternalIdentifier") > 1)  {
	 
	 
	 
		lr_start_transaction("T19_Wishlist Delete");

			addHeader();
			web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", "LAST");  
			web_reg_save_param("errorMessage", "LB=\"errorMessage\": \"", "RB=\",\n", "NotFound=Warning", "LAST");  
			web_reg_find("TEXT/IC=uniqueID","Fail=NotFound", "SaveCount=Mycount","LAST");
			web_add_header("externalId", lr_paramarr_idx("giftListExternalIdentifier",1) );
			web_custom_request("deleteWishListForUser",
				"URL=https://{api_host}/tcpproduct/deleteWishListForUser",
				"Method=DELETE",
				"Resource=0",
				"RecContentType=application/json",
				"Mode=HTTP",
				"LAST");

			addHeader();
			web_url("getListofWishList",
				"URL=https://{api_host}/tcpstore/getListofWishList",
				"Resource=0",
				"RecContentType=application/json",
				"LAST");

			addHeader();
			web_add_header("externalId", lr_paramarr_idx("giftListExternalIdentifier",2) );
			web_url("getWishListbyId",
				"URL=https://{api_host}/tcpproduct/getWishListbyId",
				"Resource=0",
				"RecContentType=application/json",
				"LAST");

		if ( strcmp( lr_eval_string("{errorCode}") ,"") != 0 && strcmp(lr_eval_string("{errorMessage}") ,"") != 0  && (atoi(lr_eval_string("{Mycount}"))==0) )
		{
			lr_fail_trans_with_error( lr_eval_string("T19_Wishlist Delete Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_transaction("T19_Wishlist Delete", 1);
		}
		else
		{
			lr_end_transaction("T19_Wishlist Delete", 2);
		}
	}
}

void wlChange()
{
	wlFavorites();

 	lr_think_time ( LINK_TT ) ;
	 
	 
	 
	if (lr_paramarr_len("giftListExternalIdentifier") > 0)  {

		lr_start_transaction("T19_Wishlist Change Name");

			web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", "LAST");  
			web_reg_save_param("errorMessage", "LB=\"errorMessage\": \"", "RB=\",\n", "NotFound=Warning", "LAST");  

			addHeader();
			 
			 
			web_add_header("Content-Type", "application/json" );
			lr_save_string( lr_paramarr_idx("status",1), "Status" );
			web_reg_find("TEXT/IC=descriptionName", "SaveCount=Mycount","LAST");

			web_custom_request("updateWishListForUser",
				"URL=https://{api_host}/tcpproduct/updateWishListForUser",
				"Method=PUT",
				"Resource=0",
				"RecContentType=application/json",
				"Mode=HTTP",
				"EncType=application/json",
				"Body={\"descriptionName\":\"Perf_{WL_Random_Number}_Change\",\"state\":\"{Status}\"}",   
				"LAST");

			addHeader();
			web_url("getListofWishList",
				"URL=https://{api_host}/tcpstore/getListofWishList",
				"Resource=0",
				"RecContentType=application/json",
				"LAST");

			addHeader();
			web_add_header("externalId", lr_paramarr_idx("giftListExternalIdentifier",1) );
			web_url("getWishListbyId",
				"URL=https://{api_host}/tcpproduct/getWishListbyId",
				"Resource=0",
				"RecContentType=application/json",
				"LAST");

 
		if ( atoi(lr_eval_string("{Mycount}")) == 0 )
		{
			lr_fail_trans_with_error( lr_eval_string("T19_Wishlist Change Name Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_transaction("T19_Wishlist Change Name", 1);
		}
		else
		{
			lr_end_transaction("T19_Wishlist Change Name", 2);
		}
	}
}



void wlAddItem()   
{
	lr_think_time ( LINK_TT ) ;
	wlGetAll();  

	target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_CART_SIZE}"));

	 

 
 
		wlGetProduct();

		if (lr_paramarr_len("atc_catentryIds") > 0) {

			lr_save_string( lr_paramarr_random( "atc_catentryIds" ), "atc_catentryId");

			 
			 
			 

			lr_start_transaction("T19_Wishlist Add Item");

			web_url("AjaxGiftListServiceAddItem",  
			"URL=http://{host}/webapp/wcs/stores/servlet/AjaxGiftListServiceAddItem?&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&tcpCallBack=jQuery111301397454545367509_1440779623033&giftListId={WishlistID}&catEntryId_1={atc_catentryId}&quantity_1=1&_=1440779623035",
				"TargetFrame=",
				"Resource=0",
				"RecContentType=text/html",
				"Referer=",
				"Snapshot=t21.inf",
				"Mode=HTML",
				"LAST");

			lr_end_transaction("T19_Wishlist Add Item", 2);

		}  

 
 
 
}

void wlGetItems()
{
 

 	lr_think_time ( LINK_TT ) ;

	if (atoi( lr_eval_string("{WishlistIDs_count}")) > 0 )
	{
		web_reg_save_param("catEntryIds", "LB=\"itemId\": \"", "RB=\"", "NotFound=Warning", "Ord=All", "LAST");
		web_reg_save_param("wishListItemIds", "LB=\"wishListItemId\": \"", "RB=\"", "NotFound=Warning", "Ord=All", "LAST");
		lr_save_string ( lr_paramarr_random( "WishlistIDs") , "WishlistID" ) ;
		 
		 
		 

		lr_start_transaction("T19_Wishlist Get Items");

		web_url("TCPGetWishListItemsForSelectedListView",  
			"URL=http://{host}/webapp/wcs/stores/servlet/TCPGetWishListItemsForSelectedListView?&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&wishListId={WishlistID}&tcpCallBack=jQuery111308157584751024842_1440519437271&_=1440519437287",
			"TargetFrame=",
			"Resource=0",
			"RecContentType=text/html",
			"Referer=",
			"Snapshot=t21.inf",
			"Mode=HTML",
			"LAST");

		lr_end_transaction("T19_Wishlist Get Items", 2);
	}
}

void wlAddToCart()
{
	int k, productCount = 0;
	wlFavorites();

 	lr_think_time ( LINK_TT ) ;
	 
	if ( lr_paramarr_len ( "productCatentryId" ) > 0 )
	{
		if ( lr_paramarr_len ( "productCatentryId" ) > 1 )
		{	productCount = atoi( lr_eval_string("{productCatentryId_count}"))-1;
			index = rand ( ) % productCount + 1 ;
			lr_save_string(lr_paramarr_idx("productCatentryId", index), "productCatentryId");
			lr_save_string(lr_paramarr_idx("giftListItemID", index), "giftListItemID");
		}
		else {
			lr_save_string(lr_paramarr_random("productCatentryId"), "productCatentryId");
			lr_save_string(lr_paramarr_random("giftListItemID"), "giftListItemID");
		}

		lr_start_transaction ( "T05_Add To Cart_From Wishlist" ) ;

		lr_start_sub_transaction("T05_Add To Cart_From WishList_getBundleIdAndBundleDetails" , "T05_Add To Cart_From Wishlist");
		addHeader();
		web_add_header("productCatentryId", lr_eval_string("{productCatentryId}") );
		web_reg_find("TEXT/IC=recordSetTotalMatches", "SaveCount=apiCheck", "LAST");
		web_url("getBundleIdAndBundleDetails",
			"URL=https://{api_host}/tcpproduct/getBundleIdAndBundleDetails",
			"Resource=0",
			"RecContentType=application/json",
			"LAST");
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction ("T05_Add To Cart_From WishList_getBundleIdAndBundleDetails", 1);
		else
			lr_end_sub_transaction ("T05_Add To Cart_From WishList_getBundleIdAndBundleDetails", 2);

		lr_start_sub_transaction("T05_Add To Cart_From WishList_getSKUInventoryandProductCounterDetails" , "T05_Add To Cart_From Wishlist");
		addHeader();
		web_add_header("productId", lr_eval_string("{productCatentryId}"));  
		web_reg_save_param("atc_catentryIds", "LB=catentryId\": \"", "RB=\",", "ORD=All", "NotFound=Warning", "LAST");  
		web_reg_save_param("atc_quantity"  ,  "LB=quantity\": \"", "RB=.0\",", "ORD=All", "NotFound=Warning", "LAST");  
		web_reg_find("TEXT/IC=getAllSKUInventoryByProductId", "SaveCount=apiCheck", "LAST");
		web_url("getSKUInventoryandProductCounterDetails",
			"URL=https://{api_host}/tcpproduct/getSKUInventoryandProductCounterDetails",
			"Resource=0",
			"RecContentType=application/json",
			"LAST");

		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction ("T05_Add To Cart_From WishList_getSKUInventoryandProductCounterDetails", 1);
		else
			lr_end_sub_transaction ("T05_Add To Cart_From WishList_getSKUInventoryandProductCounterDetails", 2);

		if ( lr_paramarr_len ( "atc_catentryIds" ) > 0 )
		{
			for (k = 1; k<= atoi(lr_eval_string("{atc_catentryIds_count}")); k++ )
			{
				if (atoi( lr_paramarr_idx("atc_quantity", k) ) != 0)
 
				{
					lr_save_string(lr_paramarr_idx("atc_catentryIds", k), "atc_catentryId" ); break;
				}
			}
			lr_start_sub_transaction ( "T05_Add To Cart_From Wishlist_addProductToCart", "T05_Add To Cart_From Wishlist");
			web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", "LAST");  
			web_reg_save_param("errorMessage", "LB=\"errorMessage\": \"", "RB=\",\n", "NotFound=Warning", "LAST");  
			web_reg_save_param_json("ParamName=orderId", "QueryString=$.orderId.*", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
			web_reg_save_param_json("ParamName=orderItemId", "QueryString=$.orderItemId.*", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
			addHeader();
			web_custom_request("addProductToCart",
				"URL=https://{api_host}/tcpproduct/addProductToCart",
				"Method=POST",
				"Resource=0",
				"RecContentType=application/json",
				"Mode=HTTP",
				"EncType=application/json",
	 
				"Body={\"calculationUsage[]\":\"-7\",\"storeId\":\"{storeId}\",\"catalogId\":\"{catalogId}\",\"langId\":\"-1\",\"orderId\":\".\",\"field2\":\"0\",\"requesttype\":\"ajax\",\"catEntryId\":\"{atc_catentryId}\",\"quantity\":1,\"externalId\":\"{giftListItemId}\"}",
				"LAST");

			if ( strlen(lr_eval_string("{orderId}")) <= 0 )
			{
				 
				lr_fail_trans_with_error( lr_eval_string("T05_Add To Cart_From Wishlist_addProductToCart - Failed with  Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
				lr_end_sub_transaction ("T05_Add To Cart_From Wishlist_addProductToCart", 1) ;
				lr_end_transaction ( "T05_Add To Cart_From Wishlist" , 1 ) ;
				return;
			}  

			lr_end_sub_transaction ("T05_Add To Cart_From Wishlist_addProductToCart", 2);

			lr_start_sub_transaction("T05_Add To Cart_From Wishlist_getOrderDetails" , "T05_Add To Cart_From Wishlist");
			addHeader();
			web_add_header("calc", "false");
			web_add_header("locStore", "True");
			web_add_header("pageName", "fullOrderInfo");
			web_reg_save_param("cartCount", "LB=cartCount\": ", "RB=,", "NotFound=Warning", "LAST");			 
			web_url("getOrderDetails",
				"URL=https://{api_host}/getOrderDetails",
				"Resource=1",
				"RecContentType=application/json",
				"LAST");
			lr_end_sub_transaction ("T05_Add To Cart_From Wishlist_getOrderDetails", 2);

			lr_save_string("T05_Add To Cart", "mainTransaction");
			 
			 
			tcp_api2("payment/getPointsService", "GET",  "T05_Add To Cart_From Wishlist" );

			lr_end_transaction ( "T05_Add To Cart_From Wishlist" , 2 ) ;
		}
	}
	else
	{
		 
		atc_Stat = 1;  
		lr_start_transaction ( "T41_Add To Cart" ) ;
		lr_end_transaction ( "T41_Add To Cart" , 1 ) ;
		return;
	}

}

void wlDeleteItem()
{
 

 	lr_think_time ( LINK_TT ) ;

	 
	 
	 
 
	if ( atoi( lr_eval_string("{itemCount}") ) != 0 ) {  

 

		lr_start_transaction("T19_Wishlist Delete Item");

			addHeader();
			web_add_header("externalId", lr_paramarr_idx("giftListExternalIdentifier",1) );
			web_url("getWishListbyId",
				"URL=https://{api_host}/tcpproduct/getWishListbyId",
				"Resource=0",
				"RecContentType=application/json",
				"LAST");


			addHeader();
			web_url("getListofWishList",
				"URL=https://{api_host}/tcpstore/getListofWishList",
				"Resource=0",
				"RecContentType=application/json",
				"LAST");

			addHeader();
			web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", "LAST");  
			web_reg_save_param("errorMessage", "LB=\"errorMessage\": \"", "RB=\",\n", "NotFound=Warning", "LAST");  
			web_reg_find("TEXT/IC=uniqueID","Fail=NotFound", "SaveCount=Mycount","LAST");
			web_add_header("externalId", lr_paramarr_idx("giftListExternalIdentifier",1) );
			web_add_header("itemId", lr_paramarr_idx("giftListItemID",1) );
			web_custom_request("deleteItemFromWishList",
				"URL=https://{api_host}/tcpproduct/deleteItemFromWishList",
				"Method=DELETE",
				"Resource=0",
				"RecContentType=application/json",
				"Mode=HTTP",
				"LAST");



		if ( strcmp( lr_eval_string("{errorCode}") ,"") != 0 && strcmp(lr_eval_string("{errorMessage}") ,"") != 0  && (atoi(lr_eval_string("{Mycount}"))==0) )
		{
			lr_fail_trans_with_error( lr_eval_string("T19_Wishlist Delete Item - Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_transaction("T19_Wishlist Delete Item", 1);
		}
		else
		{
			lr_end_transaction("T19_Wishlist Delete Item", 2);
		}
	}

}

void wlGetCatEntryId()
{
	wlFavorites();
	wlGetAll();  
	wlGetItems();

 	lr_think_time ( LINK_TT ) ;
}



void wishList()
{
	int iRandomTask = 0;

	iRandomTask = atoi(lr_eval_string("{RANDOM_PERCENT}"));

	if (iRandomTask <= RATIO_WL_CREATE)
		wlCreate();
	else if (iRandomTask <= RATIO_WL_DELETE + RATIO_WL_CREATE)
		wlDelete();
	else if (iRandomTask <= RATIO_WL_CHANGE + RATIO_WL_CREATE + RATIO_WL_DELETE)
		wlChange();
	else if (iRandomTask <= RATIO_WL_DELETE_ITEM + RATIO_WL_CREATE + RATIO_WL_DELETE + RATIO_WL_CHANGE)
		wlDeleteItem();
 
 
	else if (iRandomTask <= RATIO_WL_ADD_ITEM + RATIO_WL_CREATE + RATIO_WL_DELETE + RATIO_WL_CHANGE + RATIO_WL_DELETE_ITEM + RATIO_WL_ADD_TO_CART)
		wlAddToCart();

}


void dropCartTransaction()
{
	lr_start_transaction("T20_Abandon_Cart");
	lr_end_transaction("T20_Abandon_Cart", 0);

	if (atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 60)
		viewCart();  
	 
}

void getDataPoints()
{

	lr_param_sprintf("LOGIN_PAGE_DROP", "01. LOGIN_PAGE_DROP = %d", LOGIN_PAGE_DROP);
	lr_user_data_point(lr_eval_string("{LOGIN_PAGE_DROP}"), 0);
	lr_param_sprintf("SHIP_PAGE_DROP", "01. SHIP_PAGE_DROP = %d", SHIP_PAGE_DROP);
	lr_user_data_point(lr_eval_string("{SHIP_PAGE_DROP}"), 0);
	lr_param_sprintf("BILL_PAGE_DROP", "01. BILL_PAGE_DROP = %d", BILL_PAGE_DROP);
	lr_user_data_point(lr_eval_string("{BILL_PAGE_DROP}"), 0);

	lr_param_sprintf("RATIO_CHECKOUT_LOGIN", "02. RATIO_CHECKOUT_LOGIN = %d", RATIO_CHECKOUT_LOGIN);
	lr_user_data_point(lr_eval_string("{RATIO_CHECKOUT_LOGIN}"), 0);
	lr_param_sprintf("RATIO_CHECKOUT_GUEST", "02. RATIO_CHECKOUT_GUEST = %d", RATIO_CHECKOUT_GUEST);
	lr_user_data_point(lr_eval_string("{RATIO_CHECKOUT_GUEST}"), 0);


	lr_param_sprintf("RATIO_DROP_CART", "03. RATIO_DROP_CART = %d", RATIO_DROP_CART);
	lr_user_data_point(lr_eval_string("{RATIO_DROP_CART}"), 0);

	lr_param_sprintf("RATIO_CHECKOUT_LOGIN_FIRST", "04. RATIO_CHECKOUT_LOGIN_FIRST = %d", RATIO_CHECKOUT_LOGIN_FIRST);
	lr_user_data_point(lr_eval_string("{RATIO_CHECKOUT_LOGIN_FIRST}"), 0);

	lr_param_sprintf("RATIO_BUILDCART_DRILLDOWN", "04. RATIO_BUILDCART_DRILLDOWN = %d", RATIO_BUILDCART_DRILLDOWN);
	lr_user_data_point(lr_eval_string("{RATIO_BUILDCART_DRILLDOWN}"), 0);

	lr_param_sprintf("USE_LOW_INVENTORY", "05. USE_LOW_INVENTORY = %d", USE_LOW_INVENTORY);
	lr_user_data_point(lr_eval_string("{USE_LOW_INVENTORY}"), 0);

	lr_param_sprintf("OFFLINE_PLUGIN", "06. OFFLINE_PLUGIN = %d", OFFLINE_PLUGIN);
	lr_user_data_point(lr_eval_string("{OFFLINE_PLUGIN}"), 0);

	lr_param_sprintf("RATIO_PROMO_APPLY", "07. RATIO_PROMO_APPLY = %d", RATIO_PROMO_APPLY);
	lr_user_data_point(lr_eval_string("{RATIO_PROMO_APPLY}"), 0);
	lr_param_sprintf("RATIO_PROMO_MULTIUSE", "07. RATIO_PROMO_MULTIUSE = %d", RATIO_PROMO_MULTIUSE);
	lr_user_data_point(lr_eval_string("{RATIO_PROMO_MULTIUSE}"), 0);
	lr_param_sprintf("RATIO_PROMO_SINGLEUSE", "07. RATIO_PROMO_SINGLEUSE = %d", RATIO_PROMO_SINGLEUSE);
	lr_user_data_point(lr_eval_string("{RATIO_PROMO_SINGLEUSE}"), 0);
	lr_param_sprintf("RATIO_REDEEM_LOYALTY_POINTS", "07. RATIO_REDEEM_LOYALTY_POINTS = %d", RATIO_REDEEM_LOYALTY_POINTS);
	lr_user_data_point(lr_eval_string("{RATIO_REDEEM_LOYALTY_POINTS}"), 0);

	lr_param_sprintf("RATIO_CART_MERGE", "08. RATIO_CART_MERGE = %d", RATIO_CART_MERGE);
	lr_user_data_point(lr_eval_string("{RATIO_CART_MERGE}"), 0);

	lr_param_sprintf("RATIO_REGISTER", "09. RATIO_REGISTER = %d", RATIO_REGISTER);
	lr_user_data_point(lr_eval_string("{RATIO_REGISTER}"), 0);

	lr_param_sprintf("RATIO_UPDATE_QUANTITY", "10. RATIO_UPDATE_QUANTITY = %d", RATIO_UPDATE_QUANTITY);
	lr_user_data_point(lr_eval_string("{RATIO_UPDATE_QUANTITY}"), 0);

	lr_param_sprintf("RATIO_DELETE_ITEM", "11. RATIO_DELETE_ITEM = %d", RATIO_DELETE_ITEM);
	lr_user_data_point(lr_eval_string("{RATIO_DELETE_ITEM}"), 0);

	lr_param_sprintf("RATIO_SELECT_COLOR", "12. RATIO_SELECT_COLOR = %d", RATIO_SELECT_COLOR);
	lr_user_data_point(lr_eval_string("{RATIO_SELECT_COLOR}"), 0);

	lr_param_sprintf("RATIO_RESERVATION_HISTORY", "13. RATIO_RESERVATION_HISTORY = %d", RATIO_RESERVATION_HISTORY);
	lr_user_data_point(lr_eval_string("{RATIO_RESERVATION_HISTORY}"), 0);
	lr_param_sprintf("RATIO_POINTS_HISTORY", "13. RATIO_POINTS_HISTORY = %d", RATIO_POINTS_HISTORY);
	lr_user_data_point(lr_eval_string("{RATIO_POINTS_HISTORY}"), 0);
	lr_param_sprintf("RATIO_ORDER_HISTORY", "13. RATIO_ORDER_HISTORY = %d", RATIO_ORDER_HISTORY);
	lr_user_data_point(lr_eval_string("{RATIO_ORDER_HISTORY}"), 0);
	lr_param_sprintf("RATIO_ORDER_STATUS", "13. RATIO_ORDER_STATUS = %d", RATIO_ORDER_STATUS);
	lr_user_data_point(lr_eval_string("{RATIO_ORDER_STATUS}"), 0);

	lr_param_sprintf("RATIO_WISHLIST", "14. RATIO_WISHLIST = %d", RATIO_WISHLIST);
	lr_user_data_point(lr_eval_string("{RATIO_WISHLIST}"), 0);
	lr_param_sprintf("RATIO_WL_CREATE", "14. RATIO_WL_CREATE = %d", RATIO_WL_CREATE);
	lr_user_data_point(lr_eval_string("{RATIO_WL_CREATE}"), 0);
	lr_param_sprintf("RATIO_WL_DELETE", "14. RATIO_WL_DELETE = %d", RATIO_WL_DELETE);
	lr_user_data_point(lr_eval_string("{RATIO_WL_DELETE}"), 0);
	lr_param_sprintf("RATIO_WL_CHANGE", "14. RATIO_WL_CHANGE = %d", RATIO_WL_CHANGE);
	lr_user_data_point(lr_eval_string("{RATIO_WL_CHANGE}"), 0);
	lr_param_sprintf("RATIO_WL_DELETE_ITEM", "14. RATIO_WL_DELETE_ITEM = %d", RATIO_WL_DELETE_ITEM);
	lr_user_data_point(lr_eval_string("{RATIO_WL_DELETE_ITEM}"), 0);
	lr_param_sprintf("RATIO_WL_ADD_ITEM", "14. RATIO_WL_ADD_ITEM = %d", RATIO_WL_ADD_ITEM);
	lr_user_data_point(lr_eval_string("{RATIO_WL_ADD_ITEM}"), 0);

	lr_param_sprintf("NAV_BROWSE", "15. NAV_BROWSE = %d", NAV_BROWSE);
	lr_user_data_point(lr_eval_string("{NAV_BROWSE}"), 0);
	lr_param_sprintf("NAV_SEARCH", "15. NAV_SEARCH = %d", NAV_SEARCH);
	lr_user_data_point(lr_eval_string("{NAV_SEARCH}"), 0);
	lr_param_sprintf("NAV_CLEARANCE", "15. NAV_CLEARANCE = %d", NAV_CLEARANCE);
	lr_user_data_point(lr_eval_string("{NAV_CLEARANCE}"), 0);
	lr_param_sprintf("NAV_PLACE", "15. NAV_PLACE = %d", NAV_PLACE);
	lr_user_data_point(lr_eval_string("{NAV_PLACE}"), 0);

	lr_param_sprintf("DRILL_ONE_FACET", "16. DRILL_ONE_FACET = %d", DRILL_ONE_FACET);
	lr_user_data_point(lr_eval_string("{DRILL_ONE_FACET}"), 0);
	lr_param_sprintf("DRILL_TWO_FACET", "16. DRILL_TWO_FACET = %d", DRILL_TWO_FACET);
	lr_user_data_point(lr_eval_string("{DRILL_TWO_FACET}"), 0);
	lr_param_sprintf("DRILL_THREE_FACET", "16. DRILL_THREE_FACET = %d", DRILL_THREE_FACET);
	lr_user_data_point(lr_eval_string("{DRILL_THREE_FACET}"), 0);
	lr_param_sprintf("DRILL_SUB_CATEGORY", "16. DRILL_SUB_CATEGORY = %d", DRILL_SUB_CATEGORY);
	lr_user_data_point(lr_eval_string("{DRILL_SUB_CATEGORY}"), 0);

	lr_param_sprintf("APPLY_SORT", "17. APPLY_SORT = %d", APPLY_SORT);
	lr_user_data_point(lr_eval_string("{APPLY_SORT}"), 0);

	lr_param_sprintf("APPLY_PAGINATE", "17. APPLY_PAGINATE = %d", APPLY_PAGINATE);
	lr_user_data_point(lr_eval_string("{APPLY_PAGINATE}"), 0);

	lr_param_sprintf("PDP", "18. PDP = %d", PDP);
	lr_user_data_point(lr_eval_string("{PDP}"), 0);
	lr_param_sprintf("QUICKVIEW", "18. QUICKVIEW = %d", QUICKVIEW);
	lr_user_data_point(lr_eval_string("{QUICKVIEW}"), 0);

	lr_param_sprintf("RATIO_STORE_LOCATOR", "19. RATIO_STORE_LOCATOR = %d", RATIO_STORE_LOCATOR);
	lr_user_data_point(lr_eval_string("{RATIO_STORE_LOCATOR}"), 0);
	lr_param_sprintf("RATIO_SEARCH_SUGGEST", "19. RATIO_SEARCH_SUGGEST = %d", RATIO_SEARCH_SUGGEST);
	lr_user_data_point(lr_eval_string("{RATIO_SEARCH_SUGGEST}"), 0);

	lr_param_sprintf("PRODUCT_QUICKVIEW_SERVICE", "20. PRODUCT_QUICKVIEW_SERVICE = %d", PRODUCT_QUICKVIEW_SERVICE);
	lr_user_data_point(lr_eval_string("{PRODUCT_QUICKVIEW_SERVICE}"), 0);
	lr_param_sprintf("RESERVE_ONLINE", "21. RESERVE_ONLINE = %d", RESERVE_ONLINE);
	lr_user_data_point(lr_eval_string("{RESERVE_ONLINE}"), 0);

	lr_param_sprintf("MOVE_FROM_CART_TO_WISHLIST", "22. MOVE_FROM_CART_TO_WISHLIST = %d", MOVE_FROM_CART_TO_WISHLIST);
	lr_user_data_point(lr_eval_string("{MOVE_FROM_CART_TO_WISHLIST}"), 0);

	lr_param_sprintf("MOVE_FROM_WISHLIST_TO_CART", "22. MOVE_FROM_WISHLIST_TO_CART = %d", MOVE_FROM_WISHLIST_TO_CART);
	lr_user_data_point(lr_eval_string("{MOVE_FROM_WISHLIST_TO_CART}"), 0);

}


int moveFromWishlistToCart()   
{
	wlFavorites();
	wlGetItems();

	lr_think_time ( LINK_TT );

	if (atoi( lr_eval_string("{wishListItemIds_count}")) > 0 )
	{
		index = rand ( ) % lr_paramarr_len( "wishListItemIds" ) + 1 ;

		lr_save_string ( lr_paramarr_idx( "wishListItemIds" , index ) , "wishListIdToCart" ) ;
		lr_save_string ( lr_paramarr_idx( "catEntryIds" , index ) , "catEntryIdWLToCart" ) ;

		registerErrorCodeCheck();
		 

		lr_start_transaction("T24_MoveFromWishlistToCart");

		lr_start_sub_transaction("T24_MoveFromWishlistToCart_S01_AjaxOrderChangeServiceItemAdd", "T24_MoveFromWishlistToCart");

		web_custom_request("T24_S01_AjaxOrderChangeServiceItemAdd",
			"URL=http://{host}/webapp/wcs/stores/servlet/AjaxOrderChangeServiceItemAdd?tcpCallBack=jQuery111300307750510271938_1473867270738&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&orderId=.&calculationUsage=-1%2C-2%2C-5%2C-6%2C-7&catEntryId={catEntryIdWLToCart}&quantity=1&externalId={wishListIdToCart}&_=1473867270742",
			"Method=GET",
			"Resource=0",
			"RecContentType=text/html",
			"Mode=HTML",
			"LAST");

		lr_end_sub_transaction("T24_MoveFromWishlistToCart_S01_AjaxOrderChangeServiceItemAdd", 2);

		lr_start_sub_transaction("T24_MoveFromWishlistToCart_S02_CreateCookieCmd", "T24_MoveFromWishlistToCart");

		web_custom_request("T24_S02_CreateCookieCmd",
			"URL=http://{host}/webapp/wcs/stores/servlet/CreateCookieCmd",
			"Method=HEAD",
			"Resource=0",
			"RecContentType=text/html",
			"Mode=HTML",
			"LAST");

		lr_end_sub_transaction("T24_MoveFromWishlistToCart_S02_CreateCookieCmd", 2);

		lr_start_sub_transaction("T24_MoveFromWishlistToCart_S03_getOrderDetails", "T24_MoveFromWishlistToCart");
 




		addHeader();
		web_add_header("calc", "true");    
		web_add_header("locStore", "True");
		web_add_header("pageName", "fullOrderInfo");
		web_reg_find("TEXT/IC={", "SaveCount=apiCheck", "LAST");
		web_url("getOrderDetails",
			"URL=https://{api_host}/getOrderDetails",
			"Resource=0",
			"RecContentType=application/json",
			"LAST");
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_transaction("T24_MoveFromWishlistToCart_S03_getOrderDetails", 1);
		else
			lr_end_transaction("T24_MoveFromWishlistToCart_S03_getOrderDetails", 2);

		 

		if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 )  
		{
			lr_end_transaction ( "T24_MoveFromWishlistToCart", 2 ) ;
			return 0;
		}
		else {
			lr_fail_trans_with_error( lr_eval_string ("T24_MoveFromWishlistToCart Failed with Error Code:  \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_transaction ( "T24_MoveFromWishlistToCart", 1 ) ;
			return 1;
		}
	}
	return 0;
}


int moveFromCartToWishlist()
{
	lr_think_time ( LINK_TT );

	viewCart();
 
 
	if (atoi( lr_eval_string("{itemCatentryId_count}")) > 0 )
	{

		index = rand ( ) % lr_paramarr_len( "orderItemIds" ) + 1 ;

		lr_save_string ( lr_paramarr_idx( "orderItemIds" , index ) , "orderItemId" ) ;
		lr_save_string ( lr_paramarr_idx( "itemCatentryId" , index ) , "productId" ) ;
		lr_save_string ( lr_paramarr_idx( "itemQuantity" , index ) , "quantity" ) ;

		addHeader();
		web_add_header("Content-Type", "application/json");

		registerErrorCodeCheck();
		 

		lr_start_transaction("T23_MoveFromCartToWishlist");

		lr_start_sub_transaction("T23_MoveFromCartToWishlist_S01_addOrUpdateWishlist", "T23_MoveFromCartToWishlist");

				


		web_reg_find("TEXT/IC=item", "SaveCount=apiCheck", "LAST");
		web_custom_request("addOrUpdateWishlist",
			"URL=https://{api_host}/addOrUpdateWishlist",
			"Method=PUT",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTML",
			"Body={\"item\":[{\"productId\":\"{productId}\",\"quantityRequested\":\"1\"}]}",
			"LAST");
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction("T23_MoveFromCartToWishlist_S01_addOrUpdateWishlist", 1);
		else
			lr_end_sub_transaction("T23_MoveFromCartToWishlist_S01_addOrUpdateWishlist", 2);

				


		lr_start_sub_transaction("T23_MoveFromCartToWishlist_S02_updateMultiSelectItemsToRemove", "T23_MoveFromCartToWishlist");
		addHeader();
		web_add_header("Content-Type", "application/json");
		web_reg_find("TEXT/IC=orderId", "SaveCount=apiCheck", "LAST");
		web_custom_request("updateMultiSelectItemsToRemove",
			"URL=https://{api_host}/updateMultiSelectItemsToRemove",
			"Method=PUT",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTML",
			"Body={\"orderItem\":[{\"orderItemId\":\"{orderItemId}\",\"quantity\":\"0\"}]}",
		"LAST");
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction("T23_MoveFromCartToWishlist_S02_updateMultiSelectItemsToRemove", 1);
		else
			lr_end_sub_transaction("T23_MoveFromCartToWishlist_S02_updateMultiSelectItemsToRemove", 2);

		lr_start_sub_transaction("T23_MoveFromCartToWishlist_S03_getOrderDetails", "T23_MoveFromCartToWishlist");
 




		addHeader();
		web_add_header("calc", "True");
		web_add_header("locStore", "True");
		web_add_header("pageName", "fullOrderInfo");
		web_reg_find("TEXT/IC={", "SaveCount=apiCheck", "LAST");
		web_url("getOrderDetails",
			"URL=https://{api_host}/getOrderDetails",
			"Resource=0",
			"RecContentType=application/json",
			"LAST");
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_transaction("T23_MoveFromCartToWishlist_S03_getOrderDetails", 1);
		else
			lr_end_transaction("T23_MoveFromCartToWishlist_S03_getOrderDetails", 2);

 
 
# 5293 "..\\..\\checkoutFunctions.c"
		lr_start_sub_transaction ( "T23_MoveFromCartToWishlist_S05_getPointsService", "T23_MoveFromCartToWishlist" );
 




		addHeader();
		web_reg_find("TEXT/IC=LoyaltyWebsiteInd", "SaveCount=apiCheck", "LAST");
		web_custom_request("getPointsService",
			"URL=https://{api_host}/payment/getPointsService",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			"LAST");
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction("T23_MoveFromCartToWishlist_S05_getPointsService", 1);
		else
			lr_end_sub_transaction("T23_MoveFromCartToWishlist_S05_getPointsService", 2);

		 

		if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 )  
		{
			lr_end_transaction ( "T23_MoveFromCartToWishlist", 2 ) ;
			return 0;
		}
		else {
			lr_fail_trans_with_error( lr_eval_string ("T23_MoveFromCartToWishlist Failed with Error Code:  \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_transaction ( "T23_MoveFromCartToWishlist", 1 ) ;
			return 1;
		}

	}

 
 
# 5434 "..\\..\\checkoutFunctions.c"
	return 0;
}

int getPromoCode() {  
int rNum;
unsigned short updateStatus;
char **colnames = 0;
char **rowdata = 0;
PVCI2 pvci = 0;

    int row, rc, loopCtr = 0;
    char FieldValue[50];
    int totalCoupon = lr_get_attrib_long("SinglePromoCodeCount");
	char  *VtsServer = "10.56.29.36";
	int   nPort = 4000;
    pvci = vtc_connect(VtsServer,nPort,0x01);
	lr_save_string("", "promocode");
    if (strcmp(lr_eval_string("{storeId}"), "10151") == 0 ) {
		while (pvci != 0) {

	        rNum = rand() % totalCoupon + 1;
	        loopCtr++;
	        vtc_query_row(pvci, rNum, &colnames, &rowdata);
			lr_output_message("Query Row Names : %s", colnames[0]);
			lr_output_message("Query Row Data  : %s", rowdata[0]);

	        if ( strcmp(rowdata[0], "") != 0) {  

	        	lr_save_string(rowdata[0], "promocode");  
	 
	        	 
				if (strcmp (lr_eval_string("{currentFlow}"), "checkoutFlow") == 0 )
					vtc_update_row1(pvci,"US_COUPON_CODE", rNum, "",",", &updateStatus);

	            break;
	        }
			if (loopCtr == 5)
				break;
	    }

	    if ( strcmp(lr_eval_string("{promocode}"), "") == 0) {
	        lr_error_message("Single-Use promo code out of data.");
	 
	    }

	    vtc_free_list(colnames);
	    vtc_free_list(rowdata);
    }
    else {
		while (pvci != 0) {

	        rNum = rand() % totalCoupon + 1;
	        loopCtr++;
	        vtc_query_row(pvci, rNum, &colnames, &rowdata);
			lr_output_message("Query Row Names : %s", colnames[1]);
			lr_output_message("Query Row Data  : %s", rowdata[1]);

	        if ( strcmp(rowdata[1], "") != 0) {  

	        	lr_save_string(rowdata[1], "promocode");  
	 
	 
				if (strcmp (lr_eval_string("{currentFlow}"), "checkoutFlow") == 0 )
					vtc_update_row1(pvci,"CA_COUPON_CODE", rNum, "",",", &updateStatus);

	            break;
	        }
			if (loopCtr == 5)
				break;
	    }

	    if ( strcmp(lr_eval_string("{promocode}"), "") == 0) {
	        lr_error_message("Single-Use promo code out of data.");
	 
	    }

	    vtc_free_list(colnames);
	    vtc_free_list(rowdata);

    }

    rc = vtc_disconnect( pvci );

    return 0;

}

void accountRewards()
{
	lr_start_transaction("T08_ViewMyAccount_Rewards");

	web_reg_find("TEXT/IC=My Place Rewards", "SaveCount=rewardsCheck", "LAST");
	 
	web_url("account/rewards/",
		"URL=https://{host}/{country}/account/rewards/",
		"Resource=0",
		"RecContentType=text/html",
		"Referer=",
		"Mode=HTML",
		"LAST");

	lr_start_sub_transaction ("T08_ViewMyAccount_Rewards_getMyPointHistory","T08_ViewMyAccount_Rewards");

	addHeader();
	web_url("payment/getMyPointHistory",
		"URL=https://{api_host}/payment/getMyPointHistory",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");

	lr_end_sub_transaction ("T08_ViewMyAccount_Rewards_getMyPointHistory",2);
 
# 5581 "..\\..\\checkoutFunctions.c"
	if (atoi(lr_eval_string("{rewardsCheck}")) == 0 )
		lr_end_transaction("T08_ViewMyAccount_Rewards", 1);
	else
		lr_end_transaction("T08_ViewMyAccount_Rewards", 2);

 

	lr_start_transaction("T08_ViewMyAccount_Rewards_PointsHistory");
	addHeader();
	web_reg_find("TEXT/IC=My Place Rewards", "SaveCount=rewardsCheck1", "LAST");
	web_url("account/rewards/points-history",
		"URL=https://{host}/{country}/account/rewards/points-history",
		"Resource=0",
		"RecContentType=text/html",
		"Referer=",
		"Mode=HTML",
		"LAST");
 
# 5642 "..\\..\\checkoutFunctions.c"
	lr_start_sub_transaction ( "T08_ViewMyAccount_Rewards_PointsHistory_getMyPointHistory", "T08_ViewMyAccount_Rewards_PointsHistory" );
	addHeader();
	web_reg_find("TEXT/IC=pointsHistoryList", "SaveCount=apiCheck", "LAST");
	web_custom_request("payment/getMyPointHistory",
		"URL=https://{api_host}/payment/getMyPointHistory",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T08_ViewMyAccount_Rewards_PointsHistory_getMyPointHistory", 1);
	else
		lr_end_sub_transaction("T08_ViewMyAccount_Rewards_PointsHistory_getMyPointHistory", 2);

	if (atoi(lr_eval_string("{rewardsCheck1}")) == 0 )
		lr_end_transaction("T08_ViewMyAccount_Rewards_PointsHistory", 1);
	else
		lr_end_transaction("T08_ViewMyAccount_Rewards_PointsHistory", 2);

 

	lr_start_transaction("T08_ViewMyAccount_Rewards_Offers&Coupons");
	web_reg_find("TEXT/IC=My Place Rewards", "SaveCount=rewardsCheck1", "LAST");
	web_url("account/rewards/offers",
		"URL=https://{host}/{country}/account/rewards/offers",
		"Resource=0",
		"RecContentType=text/html",
		"Referer=",
		"Mode=HTML",
		"LAST");

	lr_start_sub_transaction ("T08_ViewMyAccount_Rewards_Offers&Coupons_getMyPointHistory","T08_ViewMyAccount_Rewards_Offers&Coupons");

	addHeader();
	web_reg_save_param("cartCount", "LB=cartCount\": ", "RB=,", "NotFound=Warning", "LAST");			 

	web_url("payment/getMyPointHistory",
		"URL=https://{api_host}/payment/getMyPointHistory",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");

	lr_end_sub_transaction ("T08_ViewMyAccount_Rewards_Offers&Coupons_getMyPointHistory",2);
 
# 5732 "..\\..\\checkoutFunctions.c"
	if (atoi(lr_eval_string("{rewardsCheck1}")) == 0 )
		lr_end_transaction("T08_ViewMyAccount_Rewards_Offers&Coupons", 1);
	else
		lr_end_transaction("T08_ViewMyAccount_Rewards_Offers&Coupons", 2);

 


	if (MULTI_USE_COUPON_FLAG == 1)
	{
		lr_start_transaction ( "T08_ViewMyAccount_Rewards_Offers&Coupons_ApplyToBag" ) ;

			lr_start_sub_transaction ( "T08_ViewMyAccount_Rewards_Offers&Coupons_ApplyToBag_Coupons" , "T08_ViewMyAccount_Rewards_Offers&Coupons_ApplyToBag") ;
						# 5756 "..\\..\\checkoutFunctions.c"


			registerErrorCodeCheck();
			addHeader();
			web_custom_request("coupons",
				"URL=https://{api_host}/payment/coupons",
				"Method=POST",
				"Resource=0",
				"RecContentType=application/json",
				"Mode=HTTP",
				"EncType=application/json",
				"Body={\"storeId\":\"{storeId}\",\"langId\":\"-1\",\"catalogId\":\"{catalogId}\",\"URL\":\"\",\"promoCode\":\"FORTYOFF\",\"requesttype\":\"ajax\",\"fromPage\":\"shoppingCartDisplay\",\"taskType\":\"A\"}",
				"LAST");
			lr_end_sub_transaction ("T08_ViewMyAccount_Rewards_Offers&Coupons_ApplyToBag_Coupons", 2)	;

 

 
# 5788 "..\\..\\checkoutFunctions.c"
			lr_start_sub_transaction ( "T08_ViewMyAccount_Rewards_Offers&Coupons_ApplyToBag_getOrderDetails", "T08_ViewMyAccount_Rewards_Offers&Coupons_ApplyToBag" );
	 




			addHeader();
			web_add_header("locStore", "False");
			web_add_header("pageName", "orderSummary");
			web_url("getOrderDetails",
				"URL=https://{api_host}/getOrderDetails",
				"Resource=0",
				"RecContentType=application/json",
				"LAST");
			lr_end_sub_transaction ( "T08_ViewMyAccount_Rewards_Offers&Coupons_ApplyToBag_getOrderDetails", 2 );

			lr_start_sub_transaction ( "T08_ViewMyAccount_Rewards_Offers&Coupons_ApplyToBag_getPointsService", "T08_ViewMyAccount_Rewards_Offers&Coupons_ApplyToBag" );
	 




			addHeader();
			web_url("getPointsService",
				 "URL=https://{api_host}/payment/getPointsService",
				 "Resource=0",
				 "RecContentType=application/json",
				 "LAST");
			lr_end_sub_transaction("T08_ViewMyAccount_Rewards_Offers&Coupons_ApplyToBag_getPointsService", 2);

		lr_end_transaction ("T08_ViewMyAccount_Rewards_Offers&Coupons_ApplyToBag", 2)	;
	}
}

void viewOrderStatus()
{
	lr_think_time ( LINK_TT );
	 

	if ( atoi( lr_eval_string("{s_OrderLookup_count}")) > 0 ) {   

		if (ONLINE_ORDER_SUBMIT==0){
			lr_save_string( lr_paramarr_random("s_OrderLookup"),"orderId");
					lr_save_string("T08_ViewMyAccount_Orders_Details", "orderStatusTransaction");
		}else if (ONLINE_ORDER_SUBMIT==1){
			 
			 
				if (strcmp( lr_eval_string("{addBopisToCart}"), "true") == 0 ){
					lr_save_string("T08_ViewMyAccount_Orders_Details_BOPIS", "orderStatusTransaction");
				}else{
					lr_save_string("T08_ViewMyAccount_Orders_Details_ECOM", "orderStatusTransaction");
				}
		}

		lr_start_transaction(lr_eval_string("{orderStatusTransaction}"));

		web_custom_request("T08_ViewMyAccount_S03_OrdersDetails",
		"URL=https://{host}/{country}/account/orders/order-details/{orderId}",
		"Method=GET",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		"EncType=",
		"LAST");

		web_reg_find("TEXT/IC=\"orderId\": \"{orderId}\"", "SaveCount=apiCheck", "LAST");
		addHeader();
		web_add_header("Origin", "https://{host}");
		web_add_header("orderId", "{orderId}");
		web_add_header("emailId", "{userEmail}");

		web_custom_request("orderLookup",
			"URL=https://{api_host}/tcporder/orderLookUp",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			"LAST");
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_transaction(lr_eval_string("{orderStatusTransaction}"), 1);
		else
			lr_end_transaction(lr_eval_string("{orderStatusTransaction}"), 2);


	}

	 

} 

void viewOrderHistory()
{
	lr_think_time ( LINK_TT );

 
 
	web_reg_find("Text/IC=My Place Rewards", "SaveCount=checkOrderhistoryHistory");

	lr_start_transaction ( "T08_ViewMyAccount_Orders" ) ;

	web_custom_request("T08_ViewMyAccount_Orders",
 
 
	"URL=https://{host}/{country}/account/orders",  
		"Method=GET",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		"EncType=",
		"LAST");
 
# 5945 "..\\..\\checkoutFunctions.c"

	if ( atoi ( lr_eval_string ( "{checkOrderhistoryHistory}" ) ) > 0 )
		lr_end_transaction ( "T08_ViewMyAccount_Orders" , 0) ;
	else
		lr_end_transaction ( "T08_ViewMyAccount_Orders" , 1) ;

	if (atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 50 )
		viewOrderStatus();

} 



void reservationHistory()  
{
	lr_think_time ( LINK_TT );
	lr_start_transaction("T08_ViewMyAccount_ReservationHistory");
		web_reg_find("TEXT/IC=My Place Rewards", "SaveCount=reservationsCheck", "LAST");
		web_url("Reservations",
			"URL=https://{host}/{country}/account/reservations/",
			"Resource=0",
			"RecContentType=text/html",
			"Referer=",
			"Mode=HTML",
		"LAST");

	lr_end_transaction("T08_ViewMyAccount_ReservationHistory", 2);
}

void addressBook()
{
	lr_think_time ( LINK_TT );
	lr_start_transaction("T08_ViewMyAccount_AddressBook");
	web_reg_find("TEXT/IC=My Place Rewards", "SaveCount=addressBookCheck", "LAST");
	web_url("Address-book",
		"URL=https://{host}/{country}/account/address-book/",
		"Resource=0",
		"RecContentType=text/html",
		"Referer=",
		"Mode=HTML",
		"LAST");


	if (atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 100 )
	{
		lr_start_sub_transaction("T08_ViewMyAccount_AddressBook_AddNewAddress", "T08_ViewMyAccount_AddressBook");
		web_url("Address-book/add-new-address",
			"URL=https://{host}/{country}/account/address-book/add-new-address",
			"Resource=0",
			"RecContentType=text/html",
			"Referer=",
			"Mode=HTML",
			"LAST");
		lr_end_sub_transaction("T08_ViewMyAccount_AddressBook_AddNewAddress", 2);

		lr_start_sub_transaction ("T08_ViewMyAccount_AddressBook_addAddress","T08_ViewMyAccount_AddressBook");
		web_reg_save_param("newNickName", "LB=nickName\": \"", "RB=\",", "NotFound=Warning", "LAST");
		addHeader();
		web_custom_request("addAddress",
			"URL=https://{api_host}/payment/addAddress",
			"Method=POST",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTML",
			"EncType=application/json",
			"Body={\"contact\":[{\"addressLine\":[\"{guestAdr1}\",\"\",\"\"],\"attributes\":[{\"key\":\"addressField3\",\"value\":\"{guestState}\"}],\"addressType\":\"ShippingAndBilling\",\"city\":\"{guestCity}\",\"country\":\"{guestCountry}\",\"firstName\":\"Joe\",\"lastName\":\"User\",\"nickName\":\"1498060089577\",\"phone1\":\"2014564545\",\"email1\":\"{userEmail}\",\"phone1Publish\":\"false\",\"primary\":\"false\",\"state\":\"{guestState}\",\"zipCode\":\"{guestZip}\",\"xcont_addressField2\":\"1\",\"xcont_addressField3\""
			":\"{guestZip}\",\"fromPage\":\"\"}]}",
			"LAST");

		lr_end_sub_transaction ("T08_ViewMyAccount_AddressBook_addAddress", 2) ;

		if (strlen(lr_eval_string("{newNickName}")) != 0 )
		{
			lr_start_sub_transaction ("T08_ViewMyAccount_AddressBook_deleteAddressDetails","T08_ViewMyAccount_AddressBook");
			addHeader();
			web_add_header("nickname", lr_eval_string("{newNickName}"));
			web_add_header("Content-Type", "application/json");
			web_custom_request("deleteAddressDetails",
				"URL=https://{api_host}/payment/deleteAddressDetails",
				"Method=DELETE",
				"Resource=0",
				"RecContentType=application/json",
				"Mode=HTTP",
				"LAST");
			lr_end_sub_transaction ("T08_ViewMyAccount_AddressBook_deleteAddressDetails", 2) ;

			lr_start_sub_transaction ( "T08_ViewMyAccount_AddressBook_getCreditCardDetails", "T08_ViewMyAccount_AddressBook");
 




			addHeader();
			web_add_header("isRest", "true" );
			web_custom_request("getCreditCardDetails",
				"URL=https://{api_host}/payment/getCreditCardDetails",
				"Method=GET",
				"Resource=0",
				"RecContentType=application/json",
				"LAST");
			lr_end_sub_transaction("T08_ViewMyAccount_AddressBook_getCreditCardDetails", 2);

		}

	}
	if (atoi(lr_eval_string("{addressBookCheck}")) == 0 )
		lr_end_transaction("T08_ViewMyAccount_AddressBook", 1);
	else
		lr_end_transaction("T08_ViewMyAccount_AddressBook", 2);

}

void payment()
{
	lr_think_time ( LINK_TT );
	lr_start_transaction("T08_ViewMyAccount_Payment");
	web_reg_find("TEXT/IC=My Place Rewards", "SaveCount=paymentCheck", "LAST");
	web_url("Payment",
		"URL=https://{host}/{country}/account/payment/",
		"Resource=0",
		"RecContentType=text/html",
		"Referer=",
		"Mode=HTML",
		"LAST");

	if (atoi(lr_eval_string("{paymentCheck}")) == 0 )
		lr_end_transaction("T08_ViewMyAccount_Payment", 1);
	else
		lr_end_transaction("T08_ViewMyAccount_Payment", 2);
}

void addDeleteBirthdaySavings()
{
	lr_think_time ( LINK_TT );

	lr_start_transaction("T08_ViewMyAccount_Profile_addBirthdaySavings");
		# 6091 "..\\..\\checkoutFunctions.c"


	web_reg_save_param("timeStamp", "LB=timeStamp\": \"", "RB=\"", "NotFound=Warning", "LAST");
	web_reg_save_param("childId", "LB=childId\": \"", "RB=\"", "NotFound=Warning", "LAST");
	addHeader();
    web_custom_request("addBirthdaySavings",
        "URL=https://{api_host}/tcporder/addBirthdaySavings",
        "Method=POST",
        "Resource=0",
        "RecContentType=application/json",
        "Mode=HTML",
        "EncType=application/json",
        "Body={\"firstName\":\"JoeJunior{profileEdit}\",\"lastName\":\"User\",\"timestamp\":\"{tsBirthdaySavings}\",\"childDetails\":[{\"childName\":\"JoeBaby{profileEdit}\",\"birthYear\":\"2017\",\"birthMonth\":\"2\",\"gender\":\"01\"}]}",
 
        "LAST");

	lr_end_transaction("T08_ViewMyAccount_Profile_addBirthdaySavings", 2);

	lr_think_time ( LINK_TT );

	lr_start_transaction("T08_ViewMyAccount_Profile_deleteBirthdaySavings");
		# 6122 "..\\..\\checkoutFunctions.c"


	addHeader();
	web_custom_request("deleteBirthdaySavings_2",
		"URL=https://{api_host}/tcporder/deleteBirthdaySavings",
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTML",
		"EncType=application/json",
		"Body={\"timestamp\":\"{timeStamp}\",\"childDetails\":[{\"childId\":\"{childId}\"}]}",
 
		"LAST");
	lr_end_transaction("T08_ViewMyAccount_Profile_deleteBirthdaySavings", 2);
}


void profile()
{
	lr_think_time ( LINK_TT );
	lr_start_transaction("T08_ViewMyAccount_Profile");
	web_reg_find("TEXT/IC=My Place Rewards", "SaveCount=profileCheck", "LAST");
	web_url("Profile",
		"URL=https://{host}/{country}/account/profile/",
		"Resource=0",
		"RecContentType=text/html",
		"Referer=",
		"Mode=HTML",
		"LAST");

	if (atoi(lr_eval_string("{profileCheck}")) == 0 )
		lr_end_transaction("T08_ViewMyAccount_Profile", 1);
	else
		lr_end_transaction("T08_ViewMyAccount_Profile", 2);

	lr_start_transaction ( "T08_ViewMyAccount_Profile_getBirthdaySavings" );

		


	addHeader();
	web_custom_request("tcporder/getBirthdaySavings",
		"URL=https://{api_host}/tcporder/getBirthdaySavings",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");

	lr_end_transaction("T08_ViewMyAccount_Profile_getBirthdaySavings", 2);

	lr_start_transaction("T08_ViewMyAccount_Profile_Edit");
		# 6184 "..\\..\\checkoutFunctions.c"

	addHeader();
	web_add_header("Content-Type", "application/json");
	web_custom_request("updatesAccountDataForRegisteredUser",
		"URL=https://{api_host}/payment/updatesAccountDataForRegisteredUser",
		"Method=PUT",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTML",
		"Body={\"firstName\":\"Joe{profileEdit}\",\"lastName\":\"User\",\"associateId\":\"\",\"email1\":\"{userEmail}\",\"phone1\":\"2014531236\",\"operation\":\"\"}",
		"LAST");
	lr_end_transaction("T08_ViewMyAccount_Profile_Edit", 2);

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ADDBIRTHDAYSAVINGS )
		addDeleteBirthdaySavings();

}

void preferences()
{
	lr_think_time ( LINK_TT );
	lr_start_transaction("T08_ViewMyAccount_Preferences");
	web_reg_find("TEXT/IC=My Place Rewards", "SaveCount=preferencesCheck", "LAST");
	web_url("Preferences",
		"URL=https://{host}/{country}/account/preferences/",
		"Resource=0",
		"RecContentType=text/html",
		"Referer=",
		"Mode=HTML",
		"LAST");

	if (atoi(lr_eval_string("{preferencesCheck}")) == 0 )
		lr_end_transaction("T08_ViewMyAccount_Preferences", 1);
	else
		lr_end_transaction("T08_ViewMyAccount_Preferences", 2);

}

void myAccountOthers()
{
	int randomNumber = atoi(lr_eval_string("{RANDOM_PERCENT}"));

	if ( randomNumber <= RATIO_ACCOUNT_REWARDS )
		accountRewards();
	else if ( randomNumber <= RATIO_ACCOUNT_REWARDS + RATIO_ORDER_HISTORY )
		viewOrderHistory();
	else if ( randomNumber <= RATIO_ACCOUNT_REWARDS + RATIO_ORDER_HISTORY + RATIO_RESERVATION_HISTORY )
		reservationHistory();
	else if ( randomNumber <= RATIO_ACCOUNT_REWARDS + RATIO_ORDER_HISTORY + RATIO_RESERVATION_HISTORY  + RATIO_ADDRESSBOOK )
		addressBook();
	else if ( randomNumber <= RATIO_ACCOUNT_REWARDS + RATIO_ORDER_HISTORY + RATIO_RESERVATION_HISTORY  + RATIO_ADDRESSBOOK + RATIO_PAYMENT )
		payment();
	else if ( randomNumber <= RATIO_ACCOUNT_REWARDS + RATIO_ORDER_HISTORY + RATIO_RESERVATION_HISTORY  + RATIO_ADDRESSBOOK + RATIO_PAYMENT + RATIO_PROFILE )
		profile();
	else if ( randomNumber <= RATIO_ACCOUNT_REWARDS + RATIO_ORDER_HISTORY + RATIO_RESERVATION_HISTORY  + RATIO_ADDRESSBOOK + RATIO_PAYMENT + RATIO_PROFILE + RATIO_PREFERENCES )
		preferences();

	 

	return;
}



int viewMyAccount()
{
	int randomNumber = atoi(lr_eval_string("{RANDOM_PERCENT}"));
	lr_think_time ( LINK_TT ) ;
	 

	 
	 
	 
	 
	lr_start_transaction("T08_ViewMyAccount");

	lr_start_sub_transaction("T08_ViewMyAccount ViewMyAccount", "T08_ViewMyAccount" );

		web_reg_find("TEXT/IC=window.__PRELOADED_STATE","SaveCount=MyACC_openCount", "LAST");

		web_url("Account Overview",
			"URL=https://{host}/{country}/account/",
			"Resource=0",
			"RecContentType=text/html",
			"Referer=",
			"Mode=HTML",
			"LAST");
	if (atoi(lr_eval_string("{MyACC_openCount}"))>0) {
		lr_end_sub_transaction("T08_ViewMyAccount ViewMyAccount",2);
	} else {
		lr_end_sub_transaction("T08_ViewMyAccount ViewMyAccount",1);
	}

	 

	lr_start_sub_transaction ( "T08_ViewMyAccount_getRegisteredUserDetailsInfo", "T08_ViewMyAccount" );
 




	addHeader();
	web_reg_save_param ( "addressId" , "LB=\"addressId\": \"" , "RB=\",\n" , "ORD=1", "NotFound=Warning", "LAST" ) ;  
	web_reg_save_param ( "addressIdAll" , "LB=\"addressId\": \"" , "RB=\",\n" , "ORD=ALL", "NotFound=Warning", "LAST" ) ;  
	web_reg_find("TEXT/IC=resourceName", "SaveCount=apiCheck", "LAST");
	web_custom_request("getRegisteredUserDetailsInfo",
		"URL=https://{api_host}/payment/getRegisteredUserDetailsInfo",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T08_ViewMyAccount_getRegisteredUserDetailsInfo", 1);
	else
		lr_end_sub_transaction("T08_ViewMyAccount_getRegisteredUserDetailsInfo", 2);

	 

	lr_start_sub_transaction ( "T08_ViewMyAccount_getPointsAndOrderHistory", "T08_ViewMyAccount" );
	web_reg_save_param("s_OrderLookup","LB=\"orderNumber\": \"","RB=\"","NotFound=Warning","Ord=All","LAST");
	addHeader();
	web_add_header("fromRest", "true");
	web_reg_find("TEXT/IC=getOrderHistoryResponse", "SaveCount=apiCheck", "LAST");
	web_custom_request("getPointsAndOrderHistory",
		"URL=https://{api_host}/tcporder/getPointsAndOrderHistory",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T08_ViewMyAccount_getPointsAndOrderHistory", 1);
	else
		lr_end_sub_transaction("T08_ViewMyAccount_getPointsAndOrderHistory", 2);

	lr_start_sub_transaction ("T08_ViewMyAccount_getOrderDetails","T08_ViewMyAccount");
 




 
	addHeader();
	web_add_header("locStore", "False");
	web_add_header("pageName", "orderSummary");
	web_reg_save_param("cartCount", "LB=cartCount\": ", "RB=,", "NotFound=Warning", "LAST");			 
	web_url("getOrderDetails",
		"URL=https://{api_host}/getOrderDetails",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	lr_end_sub_transaction ("T08_ViewMyAccount_getOrderDetails",2);

	if (ESPOT_FLAG == 1)
	{
		lr_start_sub_transaction ( "T08_ViewMyAccount_getESpot", "T08_ViewMyAccount" );  

				


		addHeader();
		web_add_header("espotName", "GlobalHeaderBannerAboveHeader,GlobalHeaderBannerWithinHeader,GlobalHeaderLogo,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,create_account_drawer,MinicartLoginBanner,MinicartRegisterBanner,drawer_myaccount_overview_banner,coupon_help,bag_empty_banner,drawer_empty_bag,MinicartLoginFromFavoritesBanner,bag_ledger_promo_banner,gift_services_espot,CheckoutConfirmationBanner,CheckoutConfirmationMPRPromo,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1" );
		web_add_header("deviceType","desktop");

	 
		web_reg_find("TEXT/IC=espotName", "SaveCount=apiCheck", "LAST");
		web_custom_request("getESpots",
			"URL=https://{api_host}/getESpot",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			"LAST");
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction("T08_ViewMyAccount_getESpot", 1);
		else
			lr_end_sub_transaction("T08_ViewMyAccount_getESpot", 2);

		api_getESpot_second();
	}

	lr_start_sub_transaction ( "T08_ViewMyAccount_getCreditCardDetails", "T08_ViewMyAccount" );
 




 
	addHeader();
	web_add_header("isRest", "true" );
	web_custom_request("getCreditCardDetails",
		"URL=https://{api_host}/payment/getCreditCardDetails",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	lr_end_sub_transaction("T08_ViewMyAccount_getCreditCardDetails", 2);

	tcp_api2("tcpproduct/getPriceDetails", "GET",  "T08_ViewMyAccount" );
 
# 6397 "..\\..\\checkoutFunctions.c"
	lr_start_sub_transaction ( "T08_ViewMyAccount_getAddressFromBook", "T08_ViewMyAccount" );
 





	addHeader();
	web_custom_request("getAddressFromBook",
		"URL=https://{api_host}/payment/getAddressFromBook",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	lr_end_sub_transaction("T08_ViewMyAccount_getAddressFromBook", 2);

	lr_end_transaction("T08_ViewMyAccount", 2);

 
 
 
 
 
 
 
 

	if (ONLINE_ORDER_SUBMIT==1){    
		if ( randomNumber <= RATIO_ACCOUNT_REWARDS + RATIO_ORDER_HISTORY )
 
	    return 0;							 
	}

	if (randomNumber <= 15)
		myAccountOthers();

	 return 0;
}


void newsLetterSignup()
{
	lr_think_time ( LINK_TT ) ;

	web_set_sockets_option("SSL_VERSION", "TLS1.1");

	lr_start_transaction("T26_NewsLetterSignup");

		lr_start_sub_transaction("T26_NewsLetterSignup_S01_email-subscribe", "T26_NewsLetterSignup");

		web_url("email-subscribe",
			"URL=https://{host}/webapp/wcs/stores/servlet/TCPContentDisplay?catalogId={catalogId}&langId=-1&storeId={storeId}&categoryId=341001",
			"Resource=0",
			"RecContentType=text/html",
			"Referer=",
			"Mode=HTML",
			"LAST");

		lr_end_sub_transaction("T26_NewsLetterSignup_S01_email-subscribe", 2);

		lr_start_sub_transaction("T26_NewsLetterSignup_S02_TCPAjaxEmailVerificationCmd", "T26_NewsLetterSignup");

		web_submit_data("TCPAjaxEmailVerificationCmd",
			"Action=https://{host}/webapp/wcs/stores/servlet/TCPAjaxEmailVerificationCmd",
			"Method=POST",
			"RecContentType=application/json",
			"Mode=HTML",
			"EncodeAtSign=YES",
			"ITEMDATA",
			"Name=email", "Value={userEmail}", "ENDITEM",
 
			"Name=page", "Value=newsletterSignUp", "ENDITEM",
			"Name=requesttype", "Value=ajax", "ENDITEM",
			"LAST");

		lr_end_sub_transaction("T26_NewsLetterSignup_S02_TCPAjaxEmailVerificationCmd", 2);

 

		lr_start_sub_transaction("T26_NewsLetterSignup_S03_addSignUpEmail", "T26_NewsLetterSignup");

		addHeader();
		web_add_header("Content-Type", "application/json");
		web_custom_request("addSignUpEmail",
			"URL=https://{api_host}/addSignUpEmail",
			"Method=POST",
			"Resource=0",
			"RecContentType=application/json",
			"Body={\"storeId\":\"{storeId}\",\"langId\":\"-1\",\"catalogId\":\"{catalogId}\",\"emailaddr\":\"{userEmail}\",\"URL\":\"email-confirmation\",\"response\":\"accept_all::true:false\"}",
		"LAST");

		lr_end_sub_transaction("T26_NewsLetterSignup_S03_addSignUpEmail", 2);

	lr_end_transaction("T26_NewsLetterSignup", 2);
}

void createCookieCmd()
{
	lr_start_transaction("T28_CreateCookieCmd");

	web_custom_request("CreateCookieCmd",
		"URL=https://{host}/webapp/wcs/stores/servlet/CreateCookieCmd?langId=-1&storeId={storeId}&goToPage=NoURL&catalogId={catalogId}",
		"Method=GET",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		"EncType=application/x-www-form-urlencoded",
		"LAST");

	lr_end_transaction("T28_CreateCookieCmd", 2);

}


int tempATC()
{

				web_custom_request("TCPProductQuickView",
				 
					"URL=http://{host}/shop/TCPProductQuickView?catalogId=10551&parent_category_rn=&top_category=&categoryId=489202&langId=-1&productId=801561&storeId=10151",
					"Method=GET",
					"Resource=0",
					"RecContentType=text/html",
					"Snapshot=t18.inf",
					"Mode=HTML",
					"EncType=application/x-www-form-urlencoded",
					"LAST");

				lr_start_sub_transaction ("T04_Product Quickview Page - GetSwatchesAndSizeInfo", "T04_Product Quickview Page" );

				web_custom_request("GetSwatchesAndSizeInfo",
				 
					"URL=http://{host}/webapp/wcs/stores/servlet/GetSwatchesAndSizeInfo?langId=-1&storeId=10151&catalogId=10551&productId=801561",
					"Method=GET",
					"Resource=0",
					"RecContentType=text/html",
					"Snapshot=t18.inf",
					"Mode=HTML",
					"EncType=application/x-www-form-urlencoded",
					"LAST");

				lr_end_sub_transaction ( "T04_Product Quickview Page - GetSwatchesAndSizeInfo", 0 );

				addToCartCorrelations();

				lr_start_sub_transaction ("T04_Product Quickview Page - TCPGetSKUDetailsView", "T04_Product Quickview Page" );

				web_custom_request("TCPGetSKUDetailsView",
				 
					"URL=http://{host}/webapp/wcs/stores/servlet/TCPGetSKUDetailsView?langId=-1&storeId=10151&catalogId=10551&productId=801561",
					"Method=GET",
					"Resource=0",
					"RecContentType=text/html",
					"Snapshot=t18.inf",
					"Mode=HTML",
					"EncType=application/x-www-form-urlencoded",
					"LAST");

				lr_end_sub_transaction ( "T04_Product Quickview Page - TCPGetSKUDetailsView", 0 );
		web_reg_save_param ( "orderId" , "LB=\"orderId\": [\"" , "RB=\"]" , "NotFound=Warning",  "LAST" ) ;
		web_reg_save_param ( "orderItemId" , "LB=\"orderItemId\": [\"" , "RB=\"]" , "NotFound=Warning", "LAST" ) ;
	web_reg_save_param_regexp ( "ParamName=authTokens" , "RegExp=WC_AUTHENTICATION_[0-9]+=([^D][^;]+);" , "SEARCH_FILTERS" , "Scope=Headers" , "NotFound=Warning", "Ordinal=All", "LAST" ) ;
				web_submit_data("addtocart_AjaxOrderChangeServiceItemAdd",
				"Action=https://{host}/webapp/wcs/stores/servlet/AjaxOrderChangeServiceItemAdd",
				"Method=POST",
				"RecContentType=text/html",
				"Snapshot=t25.inf",
				"Mode=HTML",
				"ITEMDATA",
				"Name=storeId", "Value={storeId}", "ENDITEM",
				"Name=catalogId", "Value={catalogId}", "ENDITEM",
				"Name=langId", "Value=-1", "ENDITEM",
				"Name=orderId", "Value=.", "ENDITEM",
				"Name=field2", "Value=0", "ENDITEM",
				"Name=comment", "Value=828661", "ENDITEM",
				"Name=calculationUsage", "Value=-1,-2,-5,-6,-7", "ENDITEM",
				"Name=catEntryId", "Value=828661", "ENDITEM",
				"Name=quantity", "Value=1", "ENDITEM",
				"Name=requesttype", "Value=ajax", "ENDITEM",
				"Name=visitorId", "Value=[CS]v1|2B0B56810507A725-40000116E00E1B28[CE]", "ENDITEM",  
				"LAST");
 
# 6644 "..\\..\\checkoutFunctions.c"
	return 0;
}



int webInstantCredit()   
{
	lr_think_time ( LINK_TT ) ;

	web_set_sockets_option("SSL_VERSION", "TLS");
	web_url("place-card",
		"URL=http://{host}/us/place-card/",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		"LAST");

	web_url("application",
		"URL=https://{host}/us/place-card/application",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		"LAST");

	lr_start_transaction ( "T18_WebInstantCredit_tcpstore/processWIC") ;
	web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", "LAST");  
	web_reg_save_param("errorMessage", "LB=\"errorMessage\": \"", "RB=\",\n", "NotFound=Warning", "LAST");  

	addHeader();
	web_custom_request("processWIC",
 		"URL=https://{api_host}/tcpstore/processWIC",
 		"Method=POST",
 		"Resource=0",
 		"RecContentType=application/json",
		"Mode=HTML",
		"Body=firstName=always&"
		"lastName=approve&"
		"middleInitial=&"
		"address1=500%20Plaza%20Dr&"
		"address2=&"
		"city=Secaucus&"
 		"state=NJ&"
 		"zipCode=07094&"
 		"country=US&"
		"ssn=9713&"
 		"alternatePhoneNumber=20145379531"
 		"emailAddress=bitrogue@mailinator.COM&"
 		"birthdayDate=1966{randomTwoDigits}25&"
 		"BF_ioBlackBox=0400mTR4Z7%2BBQcwNf94lis1ztosU8peJTCdUy7uGpRvabX7zVQjDWlYxYTXfueurujcaykJk9fR9lXIBdODzAYZvxp%2FyAGc0u%2F5UPzPPrHsRu27u2MJCSi12NBBoiZbfxP1Whlz5wlRFwWJi0FRulruXQQGCQaJkXU7GIIQMZuRs6N3qtP6CELnsH2G%2FcdkIMFbm6Yf0%2FpTJUUz1vNp0X2Zw8QydKgnOIDKXq4HnEqNOos0%2FB5zvrPJclqTOWG8pq1WO6yzJ1GNmMuMWZBamlGXoG%2FimnjwHY9HQtQzpGfcm0cR8X2Fd1ngNFGLDGZlWOX0jWbA6sSW4rBr%2FNrWzf5gYDruvNvEfRkJupBy3Z8hSEMEK7ZWd2T2HOoqcaV9FaOImRYT%2FgKodv2ev8lCwCfn1kaxBMac8RE9JQyxpl%2FFlRdEUxU0rlIm%2Bh0IdvEt%2BMbJEO8xzyZZyces4cd3X0jzFPr%2BN"
 		"op3TdJMFv9eOvsyvTBVVGK66EXEuaD0YLXuOWeFzcFTbpKHLiQrKf2hhwqstJ%2BrhYKIcxurY7kpCzwwdNW9e0%2F8IpHNM6NXh35GdB6Cc0rdKBra3m4ZITAN%2BPMPl1UvBE7jWZcVftxOczszofnt%2B%2Fu06EOo%2FUEYn9EFMRfvxVguGdCj5lheavfwPVmg%2BGk4OuHG49T1M%2BsnP9%2BSWhwxVxvHWf%2FuK445lNi%2BcovQlymqz1P3vzg9Oq87auE3RWwyPd429Hc3XdHevRMsQLV99Js2o8vmPAEOCbnRzpX9SwTbkqlmBR5N%2BYk9XXjom4y0AH2WgO40osBx6w1tx0mMZz7HtmSQrKuHt39bvxE86eVaYJEF00M2dmwpFJMf4g%2FnVivVfWl%2FGUEnvBpGzF9Rzh%2BjIcWSpp1SAFu7g0I1zyeSmTmPvDtmeZWysG5GNVi4ixCeMzU55NYBu%2FnSSujAF%2"
 		"FImMsuY70u6HOYdWG7EoZqeIeCHbDJ27mIZGZKEKh7xUN0sYF8r08f8uCcC0xOD%2BDPTu5VIg0CDMnwUlr%2F77l1ahRLPHCJ2nyP5hDtcTkIWM0MsrZYk6VspTTT4YfknEFHaD16ysmtjwcmmsgU0YREDCPutzcZTzmdVsVWccBDUhUs7HwJBLr5Keb%2BrR4RRGASpJ6ETmLbtOxKYkBFXNCg%2BO9YxO2R4GpUeravdrC26q6G5qP8J%2FK36i8%2FSM0OKZFYlvAevMmALkiiRCA6evWpL5xAYexDg4i8EgSau8uDTmXK3W95ikc26%2FuyCFY4R6KghlGeYLRb6hyFDOLPt%2B1kkJorDKbFLXB%2BZnOmejza7dnYdB3dXjB8n%2FNyT5AQKok7PtgJahH1UlSIs%2FQYMqtxvWZOLhAalWhbY%2B5yP8WdvLrD%2BP%2B1qaTpIvVu%2BuyQsaMh%2Bu9Xlp9HpZMEwvM9eWpJ"
		"75rIHe6%2FoPDSN%2Fxbk%2F43Sqb%2BGNnr6gpDIP0CxSx7kjSYP3C%2B3xVRrwyLRJ5XaZyrxo5J2FcY2%2BNO9P6IY2f%2BxktCOaw7AMCoqGG0O%2B3VHdw0mDejZrO1Umoq3MFE1OOL9Bn8XCTDfLXCixdWzqMvL5OMQ%2FiYXaPnuM20kjENkpO1N%2FdgoDKMciqJTUWwy1e%2FKq%2BVlO6QfGcU5hfv6m5A%3D%3D"
		"&prescreenId=",
 		"LAST");
 

		if ( strcmp( lr_eval_string("{errorCode}") ,"") != 0 && strcmp(lr_eval_string("{errorMessage}") ,"") != 0 )
		{
			lr_fail_trans_with_error( lr_eval_string("T18_WebInstantCredit_tcpstore/processWIC - Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_transaction( "T18_WebInstantCredit_tcpstore/processWIC", 1);
			return 1;
		}
		else
		{
			lr_end_transaction ( "T18_WebInstantCredit_tcpstore/processWIC" , 0 ) ;
			return 0;
		}
}

int submitRTPS()
{
	lr_save_string( lr_eval_string("bitrogue@mailinator.com"), "emailAddr");
 
	lr_think_time ( FORM_TT ) ;
	 

	lr_start_transaction("T14_Submit_RTPS");

	lr_start_sub_transaction("T14_Submit_RTPS_addSignUpEmail", "T14_Submit_RTPS" );
		# 6734 "..\\..\\checkoutFunctions.c"


	addHeader();
	web_custom_request("addSignUpEmail",
		"URL=https://{api_host}/addSignUpEmail",
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTML",
		"EncType=application/json",
		"Body={\"storeId\":\"{storeId}\",\"catalogId\":\"{catalogId}\",\"langId\":\"-1\",\"emailaddr\":\"{emailAddr}\",\"URL\":\"email-confirmation\",\"response\":\"accept_all::true:false\"}",
		"LAST");
 
	lr_end_sub_transaction("T14_Submit_RTPS_addSignUpEmail", 2);

	lr_start_sub_transaction("T14_Submit_RTPS_getShipmentMethods", "T14_Submit_RTPS" );

		


	addHeader();
	web_add_header("state", lr_eval_string("{savedState}"));
	web_add_header("zipCode", lr_eval_string("{savedZipCode}"));
	web_add_header("addressField1", lr_eval_string("{savedAddressLine}"));

	web_url("getShipmentMethods",
		"URL=https://{api_host}/payment/getShipmentMethods",
		"Resource=0",
		"RecContentType=application/json",
		"LAST");
	lr_end_sub_transaction("T14_Submit_RTPS_getShipmentMethods", 2);

	lr_start_sub_transaction("T14_Submit_RTPS_addAddress", "T14_Submit_RTPS" );
		


	addHeader();
	web_add_header("Content-Type", "application/json");
	web_reg_save_param ( "addressId" , "LB=\"addressId\": \"" , "RB=\",\n" , "ORD=1", "NotFound=Warning", "LAST" ) ;  
	web_custom_request("addAddress",
		"URL=https://{api_host}/payment/addAddress",
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTML",
		"EncType=application/json",
		"Body={\"contact\":[{\"addressLine\":[\"500 Plaza Dr\",\"\",\"\"],\"attributes\":[{\"key\":\"addressField3\",\"value\":\"07094\"}],\"addressType\":\"ShippingAndBilling\",\"city\":\"Secaucus\",\"country\":\"US\",\"firstName\":\"always\",\"lastName\":\"approve\",\"nickName\":\"{randomNineDigits}{randomFourDigits}\",\"phone1\":\"201456{randomFourDigits}\",\"email1\":\"{emailAddr}\",\"phone1Publish\":\"true\",\"primary\":\"false\",\"state\":\"NJ\",\"zipCode\":\"07094\",\"xcont_addressField2\":\"2\",\"xcont_addressField3\":\"07094\",\"fromPage\":\"\"}]}",
		"LAST");
	lr_end_sub_transaction("T14_Submit_RTPS_addAddress", 2);

	lr_start_sub_transaction("T14_Submit_RTPS_updateShippingMethodSelection", "T14_Submit_RTPS" );
		


	addHeader();
	web_add_header( "Content-Type", "application/json" );
	if (strcmp(lr_eval_string("{storeId}"), "10151") != 0)
		lr_save_string("900103", "shipmode_id");
	else
		lr_save_string("901101", "shipmode_id");

	web_reg_save_param ( "preScreenId" , "LB=\"prescreenId\": \"" , "RB=\"," , "ORD=1", "NotFound=Warning", "LAST" ) ;  
	web_custom_request("updateShippingMethodSelection",
		"URL=https://{api_host}/payment/updateShippingMethodSelection",
		"Method=PUT",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTML",
		"Body={\"shipModeId\":\"{shipmode_id}\",\"addressId\":\"{addressId}\",\"requesttype\":\"ajax\",\"prescreen\":true,\"x_calculationUsage\":\"-1,-2,-3,-4,-5,-6,-7\"}",
		"LAST");
	lr_end_sub_transaction("T14_Submit_RTPS_updateShippingMethodSelection", 2);

	lr_start_sub_transaction("T14_Submit_RTPS_processMadeOffer", "T14_Submit_RTPS" );
	 
		


	addHeader();
	web_custom_request("processMadeOffer",
		"URL=https://{api_host}/tcpstore/processMadeOffer",
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTML",
		"EncType=application/json",
		"Body={\"preScreenId\":\"{preScreenId}\",\"madeOffer\":\"true\"}",
		"LAST");
	lr_end_sub_transaction("T14_Submit_RTPS_processMadeOffer", 2);

	lr_start_sub_transaction("T14_Submit_RTPS_processPreScreenAcceptance", "T14_Submit_RTPS" );
 
		

 
# 6844 "..\\..\\checkoutFunctions.c"
	addHeader();
  	registerErrorCodeCheck();
	web_reg_find("TEXT/IC=APPROVE", "SaveCount=apiCheck", "LAST");  

	web_custom_request("processPreScreenAcceptance",
		"URL=https://{api_host}/tcpstore/processPreScreenAcceptance",
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTML",
		"Body=firstName=always&lastName=approve&middleInitial=&address1=500%20Plaza%20Dr&address2=&city=Secaucus&state=NJ&zipCode=07094&country=US&ssn={randomFourDigits}&alternatePhoneNumber=908310{randomFourDigits}&emailAddress=manny{randomFourDigits}29%40gmail.com&birthdayDate={randomTwoDigits}031965&mobilePhoneNumber=908210{randomFourDigits}&BF_ioBlackBox="
		"0400nyhffR%2BvPCUNf94lis1zthjxeWKvEU0ry7uGpRvabX4dRxDSNQ9jsDXfueurujcaykJk9fR9lXIfdF254%2BODaNBcbj4gkm0misVi%2BOF1dNzwR%2BKf0PyWLIssn9qQ3uRPSdu3DZ2D0kFMgGebJUzGieKM40UsrTpCWhRAl1%2B3J5VDdd59r5gHspEIHZVXg4ncI8sel0fu1jEKOVTTL2g136yNzGkmK4M7jmU2L5yi9CUENPETrpKU6cYn8q04UaLxEp48DVRl0oBkPCJz7Ln%2Bv5KTIBvOIMzuTzEBdNLiXsPV6scyeMvquA%2Bwb5jRBwwIKAfKGWdG7VQns1b77hdqlZYwJVsFx3E%2FoYDerSQOKdZwPpPV0ZSMv6L5iCyHj4Tx50%2FEUPmGH7XaKK8QfyIp%2FMdwRBr0%2BK4w4SDfZTdHW4zG%2FjemUUwMNsQPcd6kSm4zm2UbrU22b3Pd6Wf1rrmlEKIGit"
		"1Q7ozZo2Ji61Ck5rdiHsRUrA6QSxbBfNyqZtyt8rblBKVJPt0pQs%2FEGzmZLxeqL4ckMPgBd6qI6vrqvBV2LM2Vog9QTv6a%2B9aA%2Beta6HPyNaJmTDaetuP1fPKCNMjlC7JHcnY4hCiebBE66IIElXD0hEMtvQl2olrNgUNPlLThLcwBzhuMYUNJo2ptcamjSPPHBXoKQX9Xfe%2BAojbIB9QxnksoWkMUrfnoDmSfSwVX8xgj%2FS5PLkXahPQKIlx0QxPOeeN5%2FXQXiHK5P5lKj%2BqHrqGCPLI%2BvS3zO1UWrlN%2BAwf0o9eusa2jjh0Vn%2BTQHjy7H5XhLHCHAGY3J0jpLqVrSVzsI1LB6M5fl2EoMEAoEitEttFUo1DRkUtPGG4lkbMj5hta0%2F%2F2N3v%2BZJl4VShbY%2BEVBDdML6ZhV5yEfobiOhcONJPNnBg89k56rNOHjs7HO5CL%2F%2Fli8nYbsqKV1%2F"
		"B3exlsY%2Bb8dIdPEb9km9ceHDKRCiLCgbgXPmizjHlWmCRBdNDNsuaxAwAEbRdjjZSkBTBViHxgPVj4eN%2FNGYWc8AmavPB%2F61YuvcgmcIodaHxf7ueiL3FWf8R3Mrc9Gx%2FanjFOBbiQbgKbAKMAiPU19Qu8SWX1NGAoXBpuw7czrCX1iPd6mFeGBU7NS06tiLOJVmOicOfVERHb8wLsDKThVP1hAyBTju8Izs6%2FKBQ1h4tNGHTh8CNjY0flnyuelCnt2ypgfW%2Fm3Zqxt3w1Bn6LxoWOnZu64rr07LwRGXZpyc7oQIV63xadNE2jqb6o3IAEdbB%2BxBDlc5ibQ6opsC2ODMcwYCSmkgFp7tSI9InUPcpWx6106%2BW3d%2FkBnCTVaSp6iBnY1zDKHwzk7mbtMSKFVCnQfPeHmfuwjlXY6eIIskGTT3HNIi7SoUevORp5jP%2BsHNo6TrJdXfRHmiPApB3Az0M%2FchYUNz"
		"iou1mCyrDeeEOCGK6BBwGGLYs5gZAFc%2BWqw46CU7ezeK9%2FVLHcAFA3D%2B68Xvc8dSjswabztj23W%2BiA%2FA1fuHGCj1LxYzacIivT1%2BIaXp2sv44JH8G9&prescreenId={preScreenId}",
		"LAST");
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
	{
		lr_fail_trans_with_error( lr_eval_string("T14_Submit_RTPS_processPreScreenAcceptance - Failed with  Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
		lr_end_sub_transaction("T14_Submit_RTPS_processPreScreenAcceptance", 1);
		lr_end_transaction ( "T14_Submit_RTPS" , 1 ) ;
		 
		return 1;
	}
	else
	{
		lr_end_sub_transaction("T14_Submit_RTPS_processPreScreenAcceptance", 2);

		tcp_api2("payment/getPointsService", "GET",  "T14_Submit_RTPS"  );
		web_add_header("locStore", "True");
		web_add_header("pageName", "fullOrderInfo");
		web_reg_save_param("cartCount", "LB=cartCount\": ", "RB=,", "NotFound=Warning", "LAST");
		tcp_api2("getOrderDetails", "GET", "T14_Submit_RTPS" );

		lr_end_transaction ( "T14_Submit_RTPS" , 2 ) ;
		 
		return 0;
	}
}
# 2 "vuser_init.c" 2




vuser_init()
{	
	
	lr_save_string ( lr_get_attrib_string ( "TestEnvironment") , "host" ) ;
	lr_save_string ( lr_get_attrib_string ( "APIEnvironment") , "api_host" ) ; 
	lr_save_string(lr_get_attrib_string ("BuildCart"), "buildCart");
	lr_save_string ( "0" , "lastvalue");
	lr_save_string ( "0" , "startATC");
 
 
 
	lr_save_string("US","scriptOrigin");
      
	return 0;
}
# 4 "e:\\performance\\scripts\\2017_r5\\03_perf\\ecommerce\\us_checkout\\\\combined_US_Checkout.c" 2

# 1 "Action.c" 1
Action()
{
	web_set_sockets_option("SSL_VERSION", "TLS"); 
	
	 
	web_set_max_html_param_len ( "3072" ) ;
	web_cleanup_cookies () ;
	web_cache_cleanup();
	isLoggedIn=0;
	ONLINE_ORDER_SUBMIT=0;
	API_SUB_TRANSACTION_SWITCH = 1;

	if (CACHE_PRIME_MODE == 1)
		RATIO_CHECKOUT_LOGIN_FIRST = 0;
	
 
 
 
 
	RATIO_DROP_CART = 0;  
	RATIO_CHECKOUT_GUEST = 100; 
 
 
 
 
	lr_save_string("10151", "storeId");
	lr_save_string("10551", "catalogId");
	lr_save_string("us", "country");
	lr_save_string("false", "addBopisToCart");
	lr_save_string("false", "addEcommToCart");
	lr_save_string(lr_eval_string("{RANDOM_PERCENT}"), "akamaiRatio" );
 
 
 
 
 
 
# 46 "Action.c"
 
# 64 "Action.c"
 




	viewHomePage();
	
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_DROP_CART ) {
		dropCart();
	} else {
		checkOut(); 
	}

	return 0;
}
# 5 "e:\\performance\\scripts\\2017_r5\\03_perf\\ecommerce\\us_checkout\\\\combined_US_Checkout.c" 2

# 1 "dropCart.c" 1
dropCart()
{
	lr_user_data_point("Abandon Cart Rate", 1);
	lr_user_data_point("Checkout Rate", 0);
	lr_save_string("dropCartFlow", "currentFlow");
	
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CHECKOUT_GUEST ) {

		dropCartGuest();
	} 
	else {
		lr_save_string( lr_eval_string("{loginid_drop}"), "userEmail" );
 
		
		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CHECKOUT_LOGIN_FIRST ) {  

			dropCartLoginFirst();
 
        }
        else {  

			dropCartBuildCartFirst();
            
        }
		
	} 

	return 0;
}

dropCartGuest()
{
	int WIC_Submit = 0;
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_WIC )
	{	if (webInstantCredit() != 1 )
			WIC_Submit = 1;
	}
	
	if( atoi(lr_eval_string("{akamaiRatio}")) <= NO_BROWSE_RATIO )
    	buildCartDropNoBrowse(0); 
	else {
		buildCartDrop(0);
	}

	if (CACHE_PRIME_MODE == 1)
		return; 

	inCartEdits();
	
	if (proceedAsGuest() == 0)
	{
		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= SHIP_PAGE_DROP )
		{		dropCartTransaction();
				return 0;
		}
		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_RTPS && WIC_Submit == 0 )
		{	if (submitRTPS() == 1)
				return 0;
		}
		else
		{	if ( submitShipping() == 1)   
			  return 0;
		}
		
		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= BILL_PAGE_DROP )
		{		dropCartTransaction();
				return 0;
		}
		if (BillingAsGuest() == 0)  
		{
			dropCartTransaction();
			
		}
	}
	
	return 0;
}

dropCartLoginFirst()
{
	int viewHistory = atoi(lr_eval_string("{RANDOM_PERCENT}"));
	isLoggedIn=1;	
	if (loginFromHomePage() == 0)
	{
        if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ACCOUNT_OVERVIEW )
        	viewMyAccount();
         
	    viewCart();   
		 
		if( atoi(lr_eval_string("{akamaiRatio}")) <= NO_BROWSE_RATIO )
	    	buildCartDropNoBrowse(1); 
		else {
			buildCartDrop(1);
		}

		if (CACHE_PRIME_MODE == 1)
			return; 

        if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ACCOUNT_OVERVIEW )
        	viewMyAccount();
        
		if ( strcmp(strupr(lr_eval_string("{buildCart}")),  "TRUE") != 0) 
			inCartEdits();

        if ( atoi(lr_eval_string("{RANDOM_WL_PERCENT}")) <= RATIO_WISHLIST )
            wishList();

		if (proceedToCheckout_ShippingView() == 0) 
		{
    	
	        if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= SHIP_PAGE_DROP )
			{		dropCartTransaction();
					return 0;
			}

			if (submitShippingRegistered() == 0)
			{
	            if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= BILL_PAGE_DROP )
				{		dropCartTransaction();
						return 0;
				}

				if (submitBillingRegistered() == 0)
					dropCartTransaction();

            }
        }

	}
	
	return 0;
}

dropCartBuildCartFirst()
{
	int viewHistory = atoi(lr_eval_string("{RANDOM_PERCENT}"));
	int WIC_Submit = 0;
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_WIC )
	{	if (webInstantCredit() != 1 )
			WIC_Submit = 1;
	}
	
	if( atoi(lr_eval_string("{akamaiRatio}")) <= NO_BROWSE_RATIO )
    	buildCartDropNoBrowse(0); 
	else {
		buildCartDrop(0);
	}

	if (CACHE_PRIME_MODE == 1)
		return; 

	inCartEdits();
	
    if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= LOGIN_PAGE_DROP )
	{		dropCartTransaction();
			return 0;
	}
	isLoggedIn=1;
	if (login() == 0) 
	{
        if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ACCOUNT_OVERVIEW )
        	viewMyAccount();
        
	    if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= SHIP_PAGE_DROP )
		{		dropCartTransaction();
				return 0;
		}
		if (submitShippingBrowseFirst() == 0) 
		{
	        if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= BILL_PAGE_DROP )
			{		dropCartTransaction();
					return 0;
			}
		     if (submitBillingRegistered() == 0 ) {
				dropCartTransaction();
			}
		}
		            
	}

 	return 0;
}
# 6 "e:\\performance\\scripts\\2017_r5\\03_perf\\ecommerce\\us_checkout\\\\combined_US_Checkout.c" 2

# 1 "checkOut.c" 1
checkOut()
{
	lr_user_data_point("Abandon Cart Rate", 0);
	lr_user_data_point("Checkout Rate", 1);
	lr_save_string("checkoutFlow", "currentFlow");
	
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CHECKOUT_GUEST ) {
		
		checkoutGuest();
	} 
	else {
		lr_save_string( lr_eval_string("{logonid}"), "userEmail" );
 
 
 
  		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CHECKOUT_LOGIN_FIRST ) {  
			
			checkoutLoginFirst();
        }
        else { 
			
			checkoutBuildCartFirst();
        } 
	}

	return 0;
}

checkoutGuest() 
{
	if( atoi(lr_eval_string("{akamaiRatio}")) <= NO_BROWSE_RATIO )
    	buildCartCheckoutNoBrowse(0); 
	else {
		buildCartCheckout(0);
	}

	if (CACHE_PRIME_MODE == 1)
		return; 

	if (strcmp( lr_eval_string("{addBopisToCart}"), "true") != 0 ) 
		inCartEdits();
		
	if (proceedAsGuest() == 0)
	{
		if (strcmp( lr_eval_string("{addBopisToCart}"), "true") == 0 && strcmp( lr_eval_string("{addEcommToCart}"), "false") == 0 )  
		{
			if (BillingAsGuest() == 0)  
			{
				if( submitOrderAsGuest() == 0)
				{
					if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_REGISTER ) {
						createAccount();  
					}
					if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_STORE_LOCATOR ) {
						StoreLocator();
					}
					
 
				}
			}
		}
		else {
			if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_RTPS )
			{	if (submitRTPS() == 1)
					return 0;
			}
			else
			{	if ( submitShipping() == 1)   
				  return 0;
			}

			
			if (BillingAsGuest() == 0)  
			{
				if( submitOrderAsGuest() == 0)
				{
					if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ORDER_STATUS ) {  
            			viewOrderStatusGuest();
					}
					if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_REGISTER ) {
						createAccount();  
					}
					if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_STORE_LOCATOR ) {
						StoreLocator();
					}
					
				    if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_FORGOT_PASSWORD ) 
						forgetPassword();	
				}
			}
			
		}
	}

	
	return 0;
}

checkoutLoginFirst()
{
	int viewHistory = atoi(lr_eval_string("{RANDOM_PERCENT}"));
	isLoggedIn=1;	
	if (loginFromHomePage() == 0)
	{
	    viewCart();

		if( atoi(lr_eval_string("{akamaiRatio}")) <= NO_BROWSE_RATIO )
	    	buildCartCheckoutNoBrowse(1); 
		else {
			buildCartCheckout(1);
		}

		if (CACHE_PRIME_MODE == 1)
			return; 

		if (strcmp( lr_eval_string("{addBopisToCart}"), "true") != 0 ) 
			inCartEdits();
		
	    if ( atoi(lr_eval_string("{RANDOM_WL_PERCENT}")) <= RATIO_WISHLIST )
	        wishList();
	    
	    if (proceedToCheckout_ShippingView() == 0) 
	    {
			if (strcmp( lr_eval_string("{addBopisToCart}"), "true") == 0 && strcmp( lr_eval_string("{addEcommToCart}"), "false") == 0 )  
			{
				if (submitBillingRegistered() == 0)
					submitOrderRegistered();
			}
			else
			{
 			
# 140 "checkOut.c"
				if (submitShippingRegistered() == 0)
				{
					if (submitBillingRegistered() == 0) 
					{
						if (submitOrderRegistered() == 0) 
						{
							if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 5 ) 
				        		viewMyAccount();
						}
					}
				}
			}
	    }
	    
	    if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_LOGOUT ) 
	    	logoff();
	    
	    if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_FORGOT_PASSWORD ) 
			forgetPassword();	
	    
	    if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_STORE_LOCATOR ) {
			StoreLocator();
		}
	    
	    if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 50 ) 
	   		newsLetterSignup();
	    
	}

	return 0;
}

checkoutBuildCartFirst()
{
	int viewHistory = atoi(lr_eval_string("{RANDOM_PERCENT}"));

	if( atoi(lr_eval_string("{akamaiRatio}")) <= NO_BROWSE_RATIO )
    	buildCartCheckoutNoBrowse(0);
	else {
		buildCartCheckout(0);
	}

	if (CACHE_PRIME_MODE == 1)
		return; 

	inCartEdits(); 
 
 
	isLoggedIn=1;		
	if (login() == 0)  
	{
		if (strcmp( lr_eval_string("{addBopisToCart}"), "true") == 0 && strcmp( lr_eval_string("{addEcommToCart}"), "false") == 0 )  
		{
			if (submitBillingRegistered() == 0)
				submitOrderRegistered();
		}
		else if (submitShippingBrowseFirst() == 0)
		{
			if (submitBillingRegistered() == 0)
				submitOrderRegistered();
		}
		
	    if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_LOGOUT ) 
	    	logoff();
	    
	    if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_FORGOT_PASSWORD ) 
			forgetPassword();	
	    
	}
    if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_STORE_LOCATOR ) {
		StoreLocator();
	}
    
    if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 50 ) 
   		newsLetterSignup();
 
	return 0;
	
}  
# 7 "e:\\performance\\scripts\\2017_r5\\03_perf\\ecommerce\\us_checkout\\\\combined_US_Checkout.c" 2

# 1 "vuser_end.c" 1
vuser_end()
{
	return 0;
}
# 8 "e:\\performance\\scripts\\2017_r5\\03_perf\\ecommerce\\us_checkout\\\\combined_US_Checkout.c" 2

