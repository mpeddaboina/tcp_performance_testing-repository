#include "..\..\browseFunctions.c"
//#include "..\..\checkoutFunctions.c"
//#include "e:\\performance\\scripts\\2017_r3\\03_perf\\browseFunctions.c"

vuser_init()
{	
/*	
	lr_save_string ( lr_get_attrib_string ( "TestEnvironment") , "host" ) ; 

	if ( atoi( lr_eval_string("{RANDOM_PERCENT}")) <= lr_get_attrib_long("US_Traffic_Percentage") )
		lr_save_string( "/shop/us/", "store_home_page" );
	else
		lr_save_string( "/shop/ca/", "store_home_page" );
	
	viewHomePage();	
*/	

	lr_param_sprintf("NAV_BROWSE", "1. NAV_BROWSE = %d", NAV_BROWSE);
	lr_user_data_point(lr_eval_string("{NAV_BROWSE}"), 0);
	lr_param_sprintf("NAV_SEARCH", "2. NAV_SEARCH = %d", NAV_SEARCH);
	lr_user_data_point(lr_eval_string("{NAV_SEARCH}"), 0);
	lr_param_sprintf("NAV_CLEARANCE", "3. NAV_CLEARANCE = %d", NAV_CLEARANCE);
	lr_user_data_point(lr_eval_string("{NAV_CLEARANCE}"), 0);
	lr_param_sprintf("NAV_PLACE", "4. NAV_PLACE = %d", NAV_PLACE);
	lr_user_data_point(lr_eval_string("{NAV_PLACE}"), 0);

	lr_param_sprintf("DRILL_ONE_FACET", "5. DRILL_ONE_FACET = %d", DRILL_ONE_FACET);
	lr_user_data_point(lr_eval_string("{DRILL_ONE_FACET}"), 0);
	lr_param_sprintf("DRILL_TWO_FACET", "6. DRILL_TWO_FACET = %d", DRILL_TWO_FACET);
	lr_user_data_point(lr_eval_string("{DRILL_TWO_FACET}"), 0);
	lr_param_sprintf("DRILL_THREE_FACET", "7. DRILL_THREE_FACET = %d", DRILL_THREE_FACET);
	lr_user_data_point(lr_eval_string("{DRILL_THREE_FACET}"), 0);
	lr_param_sprintf("DRILL_SUB_CATEGORY", "8. DRILL_SUB_CATEGORY = %d", DRILL_SUB_CATEGORY);
	lr_user_data_point(lr_eval_string("{DRILL_SUB_CATEGORY}"), 0);

	return 0;
}
