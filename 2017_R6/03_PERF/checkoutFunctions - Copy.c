#include "..\..\checkoutWorkloadModel.c"
int index , length , i, target_itemsInCart, index_buildCart, randomPercent, isLoggedIn;
int isLoggedIn=0;
char *searchString ;
char *nav_by ; // T02 - Category Navigation selection
char *drill_by ; // T03 - Facet / SubCategory drill selection
char *sort_by; // T03 - Sort selection
char *product_by; // T04 - Product Display Method
int atc_Stat = 0; //0-Pass 1-Fail
int HttpRetCode;
int start_time, target_time;

//VTS variables
int rNum;
unsigned short updateStatus;
char **colnames = NULL;
char **rowdata = NULL;
PVCI2 pvci = 0;

//int form_TT, link_TT, typeSpeed_TT;
//typedef long time_t;
//time_t t;
//int authcookielen , authtokenlen ;
//authcookielen = 0;

/*void api_getESpot_second(){
	addHeader();
	web_add_header("espotName","GlobalHeaderBannerAboveHeader,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1");
	web_reg_find("TEXT/IC=espotName", "SaveCount=apiCheck", LAST);
	lr_start_transaction("T99_API_Second_getESpot");
	web_custom_request("getESpots",
		"URL=https://{api_host}/getESpot",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T99_API_Second_getESpot", LR_FAIL);
	else
		lr_end_sub_transaction("T99_API_Second_getESpot", LR_AUTO);
}*/


void registerErrorCodeCheck()
{
	web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", LAST); //"errorCode": "",\n
	web_reg_save_param("errorMessage", "LB=\"errorMessage\": \"", "RB=\",\n", "NotFound=Warning", LAST); //"errorMessage": "",\n
}

void getCatEntryID() {
//	lr_error_message("Should not come here.");

	lr_save_string(lr_eval_string("{lowQty_SKU}"), "atc_catentryId");

	if ( strcmp(lr_eval_string("{atc_catentryId}"), lr_eval_string("{lastvalue}") ) == 0 ) {
		lr_param_sprintf ( "atc_catentryId" , "%s" , lr_paramarr_random ( "atc_catentryIds" ) ) ;
		USE_LOW_INVENTORY = 0;
	}
	else {
		lr_save_string(lr_eval_string("{atc_catentryId}"), "lastvalue");
		lr_save_string(lr_eval_string("{atc_catentryId}"), "atc_comment");
		lr_start_transaction("T20_Low_QTY_Count");
		lr_end_transaction("T20_Low_QTY_Count", LR_AUTO);
	}

	return;
}


int addToCartFromPLP()
{
	int k;
	atc_Stat = 0; //0-Pass 1-Fail

	if (CACHE_PRIME_MODE == 1)
		return 0;

	lr_think_time ( FORM_TT ) ;
	//lr_continue_on_error(0);

	if ( lr_paramarr_len ( "productId" ) > 0 )
	{
		lr_save_string(lr_paramarr_random("productId"), "productId" );//03212018

		//lr_advance_param("pdpURLData");
//		lr_save_string(lr_eval_string("{pdpProdID}"), "productId" );

		lr_start_transaction ( "T04_Product List Page Bag" ) ;

		lr_start_sub_transaction("T04_Product List Page Bag_getBundleIdAndBundleDetails" , "T04_Product List Page Bag");

		addHeader();
		web_add_header("productCatentryId", lr_eval_string("{productId}") );
		web_reg_find("TEXT/IC=recordSetTotalMatches", "SaveCount=apiCheck", LAST);
		web_url("getBundleIdAndBundleDetails",
			"URL=https://{api_host}/tcpproduct/getBundleIdAndBundleDetails",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction ("T04_Product List Page Bag_getBundleIdAndBundleDetails", LR_FAIL);
		else
			lr_end_sub_transaction ("T04_Product List Page Bag_getBundleIdAndBundleDetails", LR_AUTO);

		lr_start_sub_transaction("T04_Product List Page Bag_getSKUInventoryandProductCounterDetails" , "T04_Product List Page Bag");
		addHeader();
		web_add_header("productId", lr_eval_string("{productId}"));
		web_reg_save_param("atc_catentryIds", "LB=catentryId\": \"", "RB=\",", "ORD=All", "NotFound=Warning", LAST); //"catentryId": "735396",
		web_reg_save_param("atc_quantity"  ,  "LB=quantity\": \"", "RB=.0\",", "ORD=All", "NotFound=Warning", LAST); //"quantity": "99999.0",
		web_reg_find("TEXT/IC=getAllSKUInventoryByProductId", "SaveCount=apiCheck", LAST);
		web_url("getSKUInventoryandProductCounterDetails",
			"URL=https://{api_host}/tcpproduct/getSKUInventoryandProductCounterDetails",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction ("T04_Product List Page Bag_getSKUInventoryandProductCounterDetails", LR_FAIL);
		else
			lr_end_sub_transaction ("T04_Product List Page Bag_getSKUInventoryandProductCounterDetails", LR_AUTO);

		lr_end_transaction ( "T04_Product List Page Bag" , LR_AUTO ) ;

		if ( lr_paramarr_len ( "atc_catentryIds" ) > 0 )
		{
			for (k = 1; k<= atoi(lr_eval_string("{atc_catentryIds_count}")); k++ )
			{
				if (atoi( lr_paramarr_idx("atc_quantity", k) ) != 0)
//				if (atoi( lr_paramarr_idx("atc_quantity", k) ) != 0 && strlen(lr_paramarr_idx("atc_catentryIds", k)) == 6 )
				{
					lr_save_string(lr_paramarr_idx("atc_catentryIds", k), "atc_catentryId" ); break;
				}
			}

			lr_start_transaction ( "T05_Add To Cart" ) ;

			lr_start_sub_transaction("T05_Add To Cart_addProductToCart" , "T05_Add To Cart");
			web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", LAST); //"errorCode": "",\n
			web_reg_save_param("errorMessage", "LB=\"errorMessage\": \"", "RB=\",\n", "NotFound=Warning", LAST); //"errorMessage": "",\n
			web_reg_save_param_json("ParamName=orderId", "QueryString=$.orderId.*", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
			web_reg_save_param_json("ParamName=orderItemId", "QueryString=$.orderItemId.*", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
			addHeader();
			web_custom_request("addProductToCart",
				"URL=https://{api_host}/tcpproduct/addProductToCart",
				"Method=POST",
				"Resource=0",
				"RecContentType=application/json",
				"Mode=HTTP",
				"EncType=application/json",
				"Body={\"calculationUsage[]\":\"-7\",\"storeId\":\"{storeId}\",\"catalogId\":\"{catalogId}\",\"langId\":\"-1\",\"orderId\":\".\",\"field2\":\"0\",\"requesttype\":\"ajax\",\"catEntryId\":\"{atc_catentryId}\",\"quantity\":1}",
				LAST);

			if ( strlen(lr_eval_string("{orderId}")) <= 0 ) {
				atc_Stat = 1; //0-Pass 1-Fail
				lr_fail_trans_with_error( lr_eval_string("T05_Add To Cart_addProductToCart - Failed with  Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
				lr_end_sub_transaction ("T05_Add To Cart_addProductToCart", LR_FAIL) ;
				lr_end_transaction ( "T05_Add To Cart" , LR_FAIL ) ;
				return LR_FAIL;
			} // end if
			lr_end_sub_transaction ("T05_Add To Cart_addProductToCart", LR_AUTO);

			lr_save_string("T05_Add To Cart", "mainTransaction");
			tcp_api2("tcporder/getAllCoupons", "GET",  lr_eval_string("{mainTransaction}") );

			lr_start_sub_transaction("T05_Add To Cart_getOrderDetails" , "T05_Add To Cart");
			addHeader();
			web_add_header("calc", "false");
			web_add_header("locStore", "False");
			web_add_header("pageName", "orderSummary");
//			web_reg_save_param("cartCount", "LB=cartCount\": ", "RB=,", "NotFound=Warning", LAST);			//"CartCount": 1,\n
			web_reg_save_param("cartCount", "LB=artCount\": ", "RB=,", "NotFound=Warning", LAST);		//03202018 - took the "C" out, this way no matter they changed it back to lower case again, were fine
			web_url("getOrderDetails",
				"URL=https://{api_host}/getOrderDetails",
				"Resource=0",
				"RecContentType=application/json",
				LAST);
			lr_end_sub_transaction ("T05_Add To Cart_getOrderDetails", LR_AUTO);


			if ( isLoggedIn == 1	) {
				tcp_api2("payment/getPointsService", "GET",  lr_eval_string("{mainTransaction}") );
			}

			web_add_header("action", "get");
			web_add_header("Accept", "application/json");
			tcp_api2("tcporder/getFavouriteStoreLocation", "GET",  lr_eval_string("{mainTransaction}") );
//			lr_output_message ("%s", lr_eval_string("Passed : \"{drillUrl}\"")  );

			lr_end_transaction ( "T05_Add To Cart" , LR_AUTO ) ;
			//lr_continue_on_error(1);

		}
		else
		{
			//lr_continue_on_error(1);
			atc_Stat = 1; //0-Pass 1-Fail
			lr_start_transaction ( "T41_Add To Cart" ) ;
			lr_fail_trans_with_error( lr_eval_string("Romano T41_Add To Cart Failed with URL Code: \"{drillUrl}\"") ) ;
//			lr_output_message ("%s", lr_eval_string("Failed : \"{drillUrl}\"")  );
			lr_end_transaction ( "T41_Add To Cart" , LR_AUTO ) ;
			return LR_FAIL;
		}
	}
	else
	{
		//lr_continue_on_error(1);
		atc_Stat = 1; //0-Pass 1-Fail
		return LR_FAIL;
	}

	return 0;
}

void addToCart()
{
	int k = 0, newTime, RANDOM_PERCENT = 0;
	atc_Stat = 0; //0-Pass 1-Fail

	if (CACHE_PRIME_MODE == 1)
		return;

	//lr_continue_on_error(1);
	lr_think_time ( FORM_TT ) ;
/*
	if ( lr_paramarr_len ( "atc_catentryIds" ) > 0 ) { // at least 1 product with positive quantity check

		for (k = 1; k<= atoi(lr_eval_string("{atc_catentryIds_count}")); k++ )
		{
			if (atoi( lr_paramarr_idx("atc_quantity", k) ) != 0 )
//			if (atoi( lr_paramarr_idx("atc_quantity", k) ) != 0 && strlen(lr_paramarr_idx("atc_catentryIds", k)) == 6 )
			{
				lr_save_string(lr_paramarr_idx("atc_catentryIds", k), "atc_catentryId" ); break;
			}
		}
*/
		lr_save_string(lr_eval_string("{randCatEntryId}"), "atc_catentryId" );
		web_reg_save_param_json("ParamName=orderId", "QueryString=$.orderId.*", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_save_param_json("ParamName=orderItemId", "QueryString=$.orderItemId.*", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_find("Text=the products you wish to purchase are not available", "SaveCount=atcErrorFound");
		web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", LAST); //"errorCode": "",\n
		web_reg_save_param("errorMessage", "LB=\"errorMessage\": \"", "RB=\",\n", "NotFound=Warning", LAST); //"errorMessage": "",\n

		lr_start_transaction ( "T05_Add To Cart" ) ;

		lr_start_sub_transaction("T05_Add To Cart_addProductToCart" , "T05_Add To Cart");
		addHeader();
		web_custom_request("addProductToCart",
			"URL=https://{api_host}/tcpproduct/addProductToCart",
			"Method=POST",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTML",
			"EncType=application/json",
			"Body={\"calculationUsage[]\":\"-7\",\"storeId\":\"{storeId}\",\"catalogId\":\"{catalogId}\",\"langId\":\"-1\",\"orderId\":\".\",\"field2\":\"0\",\"requesttype\":\"ajax\",\"catEntryId\":\"{atc_catentryId}\",\"quantity\":1}",
			LAST);
/*
		if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 ) //if no error code and message
		{	atc_Stat = 1; //0-Pass 1-Fail
			lr_end_sub_transaction ("T05_Add To Cart_addProductToCart", LR_FAIL) ;
			lr_end_transaction ( "T05_Add To Cart" , LR_FAIL ) ;
			return;
		} // end if
*/
		if ( strlen(lr_eval_string("{orderId}")) <= 0 ) {
			atc_Stat = 1; //0-Pass 1-Fail
			lr_fail_trans_with_error( lr_eval_string("T05_Add To Cart_addProductToCart - Failed with  Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_sub_transaction ("T05_Add To Cart_addProductToCart", LR_FAIL) ;
			lr_end_transaction ( "T05_Add To Cart" , LR_FAIL ) ;
			return;
		} // end if

		lr_end_sub_transaction ("T05_Add To Cart_addProductToCart", LR_AUTO) ;

		lr_start_sub_transaction ("T05_Add To Cart_getOrderDetails","T05_Add To Cart");
			#if OPTIONSENABLED
				if (isLoggedIn == 0)
				{
					call_OPTIONS("getOrderDetails");
				}
			#endif
			addHeader();
			web_add_header("calc", "false");
			web_add_header("locStore", "False");
			web_add_header("pageName", "orderSummary");
			web_reg_save_param("cartCount", "LB=artCount\": ", "RB=,", "NotFound=Warning", LAST);			//"CartCount": 1,\n
			web_url("getOrderDetails",
				"URL=https://{api_host}/getOrderDetails",
				"Resource=0",
				"RecContentType=application/json",
				LAST);
			lr_end_sub_transaction ("T05_Add To Cart_getOrderDetails",LR_AUTO);

//		}

		lr_save_string("T05_Add To Cart", "mainTransaction");
		tcp_api2("tcporder/getAllCoupons", "GET",  lr_eval_string("{mainTransaction}") );

		if ( isLoggedIn == 1	) {
			tcp_api2("payment/getPointsService", "GET",  lr_eval_string("{mainTransaction}") );
		}

		web_add_header("action", "get");
		web_add_header("Accept", "application/json");
		tcp_api2("tcporder/getFavouriteStoreLocation", "GET",  lr_eval_string("{mainTransaction}") );
//		lr_output_message ("%s", lr_eval_string("Passed : \"{drillUrl}\"")  );

		lr_end_transaction ( "T05_Add To Cart" , LR_AUTO ) ;
		//lr_continue_on_error(1);
/*
	}
	else
	{
		//lr_continue_on_error(1);
		atc_Stat = 1; //0-Pass 1-Fail
		lr_start_transaction ( "T41_Add To Cart" ) ;
//		lr_fail_trans_with_error( lr_eval_string("Romano T41_Add To Cart Failed with URL Code: \"{drillUrl}\"") ) ;
//		lr_output_message ("%s", lr_eval_string("Failed : \"{drillUrl}\"")  );
		lr_end_transaction ( "T41_Add To Cart" , LR_FAIL ) ;
		return;
	}
*/
} // end addToCart

void addToCartNoBrowse()
{
//	int k = 0, newTime, RANDOM_PERCENT = 0;
	atc_Stat = 0; //0-Pass 1-Fail
	//lr_continue_on_error(1);
	lr_think_time ( FORM_TT ) ;

	if (CACHE_PRIME_MODE == 1)
		return;


//	if ( lr_paramarr_len ( "atc_catentryIds" ) > 0 ) { // at least 1 product with positive quantity check
		lr_save_string(lr_eval_string("{randCatEntryId}"), "atc_catentryId" );

		web_reg_save_param_json("ParamName=orderId", "QueryString=$.orderId.*", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_save_param_json("ParamName=orderItemId", "QueryString=$.orderItemId.*", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_find("Text=the products you wish to purchase are not available", "SaveCount=atcErrorFound");
		web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", LAST); //"errorCode": "",\n
		web_reg_save_param("errorMessage", "LB=\"errorMessage\": \"", "RB=\",\n", "NotFound=Warning", LAST); //"errorMessage": "",\n

		lr_start_transaction ( "T05_Add To Cart" ) ;

		lr_start_sub_transaction("T05_Add To Cart_addProductToCart" , "T05_Add To Cart");
		addHeader();
		web_custom_request("addProductToCart",
			"URL=https://{api_host}/tcpproduct/addProductToCart",
			"Method=POST",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTML",
			"EncType=application/json",
			"Body={\"calculationUsage[]\":\"-7\",\"storeId\":\"{storeId}\",\"catalogId\":\"{catalogId}\",\"langId\":\"-1\",\"orderId\":\".\",\"field2\":\"0\",\"requesttype\":\"ajax\",\"catEntryId\":\"{atc_catentryId}\",\"quantity\":1}",
			LAST);

		if ( strlen(lr_eval_string("{orderId}")) <= 0 ) {
			atc_Stat = 1; //0-Pass 1-Fail
			lr_fail_trans_with_error( lr_eval_string("T05_Add To Cart_addProductToCart - Failed with  Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_sub_transaction ("T05_Add To Cart_addProductToCart", LR_FAIL) ;
			lr_end_transaction ( "T05_Add To Cart" , LR_FAIL ) ;
			return;
		} // end if

		lr_end_sub_transaction ("T05_Add To Cart_addProductToCart", LR_AUTO) ;
/*
		if (isLoggedIn == 1)
		{
			lr_start_sub_transaction ("T05_Add To Cart_getOrderDetails","T05_Add To Cart");

			#if OPTIONSENABLED
				if (isLoggedIn == 0)
				{
					call_OPTIONS("getOrderDetails");
				}
			#endif

			addHeader();
//			web_add_header("locStore", "False");
//			web_add_header("pageName", "orderSummary");
			//new 11/19/2017
			web_add_header("locStore", "False");
			web_add_header("pageName", "orderSummary");
			web_reg_save_param("cartCount", "LB=cartCount\": ", "RB=,", "NotFound=Warning", LAST);			//"CartCount": 1,\n
			web_url("getOrderDetails",
				"URL=https://{api_host}/getOrderDetails",
				"Resource=0",
				"RecContentType=application/json",
				LAST);
			lr_end_sub_transaction ("T05_Add To Cart_getOrderDetails",LR_AUTO);

		}
		else {
*/			lr_start_sub_transaction ("T05_Add To Cart_getOrderDetails","T05_Add To Cart");
			#if OPTIONSENABLED
				if (isLoggedIn == 0)
				{
					call_OPTIONS("getOrderDetails");
				}
			#endif
			addHeader();
//			web_add_header("locStore", "True");
//			web_add_header("pageName", "fullOrderInfo");
			//new 11/19/2017
			web_add_header("locStore", "False");
			web_add_header("pageName", "orderSummary");
			web_reg_save_param("cartCount", "LB=artCount\": ", "RB=,", "NotFound=Warning", LAST);			//"CartCount": 1,\n
			web_url("getOrderDetails",
				"URL=https://{api_host}/getOrderDetails",
				"Resource=0",
				"RecContentType=application/json",
				LAST);
			lr_end_sub_transaction ("T05_Add To Cart_getOrderDetails",LR_AUTO);

//		}

		lr_save_string("T05_Add To Cart", "mainTransaction");
		tcp_api2("tcporder/getAllCoupons", "GET",  lr_eval_string("{mainTransaction}") );

		if ( isLoggedIn == 1	) {
			tcp_api2("payment/getPointsService", "GET",  lr_eval_string("{mainTransaction}") );
		}

		lr_end_transaction ( "T05_Add To Cart" , LR_AUTO ) ;
		//lr_continue_on_error(1);

} // end addToCartNoBrowse

void addToCartMixed()
{
	int k = 0, newTime, RANDOM_PERCENT = 0;
	atc_Stat = 0; //0-Pass 1-Fail

	if (CACHE_PRIME_MODE == 1)
		return;


	if ( lr_paramarr_len ( "atc_catentryIds" ) > 0 ) { // at least 1 product with positive quantity check

		lr_param_sprintf ( "atc_catentryId" , "%s" , lr_paramarr_random ( "atc_catentryIds" ) ) ;
		web_reg_save_param ( "orderId" , "LB=\"orderId\": [\"" , "RB=\"]" , "NotFound=Warning",  LAST ) ;
		web_reg_save_param ( "orderItemId" , "LB=\"orderItemId\": [\"" , "RB=\"]" , "NotFound=Warning", LAST ) ;
		web_reg_find("Text=the products you wish to purchase are not available", "SaveCount=atcErrorFound");

		lr_think_time ( FORM_TT ) ;

	    if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= BOPIS_CART_RATIO && strcmp( lr_eval_string("{storeId}") , "10151") == 0)
		{
			if (pickupInStore() == LR_PASS)
			{
				if ( addBopisToCart() == LR_PASS)
				{
					atc_Stat = 0; //0-Pass 1-Fail
					lr_save_string("true", "addBopisToCart");
				} else
					atc_Stat = 1; //0-Pass 1-Fail

			}
		} else {
			if (strcmp( lr_eval_string("{storeId}") , "10151") == 0)
				lr_save_string("true", "addEcommToCart");

			productDisplay();
			addToCart();
		}
	}

}

void parseOrderItemId() { //This is to grab the upcIDs and this can only be called  after TCPAjaxCheckInventoryAvail(viewCart) when OutOfStock_Count > 0

    extern char * strtok(char * string, const char * delimiters );
    char path[1000] = "";
    char separators[] = "\"";
    char * token;
    char fullpath[1024];
    int counter = 0;
    strcpy(path, lr_eval_string("{unavailId_1}"));

    // Get the first token

    token = (char *)strtok(path, separators);
    if (!token) {
        lr_output_message ("No tokens found in string!");
    }
// While valid tokens are returned
    while (token != NULL ) {
//        lr_output_message ("%s", token );
// Get the next token
        token = (char *)strtok(NULL, separators);

        if(token != NULL) {
	        if (strlen(token) > 10) {
	        	counter++;
	        	lr_param_sprintf ("count", "%d", counter);
	        	lr_save_string(token, lr_eval_string("upcId{count}") );
	        }
        }
    }
	return;
}


void getOutOfStockItemIds(){

	int i;

	if (atoi(lr_eval_string("{unavailId_count}")) > 0) {

/*		parseOrderItemId();

		if (atoi(lr_eval_string("{count}")) > 0) {
			for (i = 1; i <= atoi(lr_eval_string("{count}")); i++) {

		if (atoi(lr_eval_string("{unavailId_count}")) > 0) {
			for (i = 1; i <= atoi(lr_eval_string("{unavailId_count}")); i++) {
				lr_eval_string("{unavailId_1}")
				lr_save_string(token, lr_eval_string("upcId{count}") );
				switch(i) {
					case 1:
						web_reg_save_param("orderItemId_1", "LB=id=\"{upcId1}\" data-item-id=\"", "RB=\">", "NotFound=Warning", LAST); break;
					case 2:
						web_reg_save_param("orderItemId_2", "LB=id=\"{upcId2}\" data-item-id=\"", "RB=\">", "NotFound=Warning", LAST); break;
					case 3:
						web_reg_save_param("orderItemId_3", "LB=id=\"{upcId3}\" data-item-id=\"", "RB=\">", "NotFound=Warning", LAST); break;
					case 4:
						web_reg_save_param("orderItemId_4", "LB=id=\"{upcId4}\" data-item-id=\"", "RB=\">", "NotFound=Warning", LAST); break;
					case 5:
						web_reg_save_param("orderItemId_5", "LB=id=\"{upcId5}\" data-item-id=\"", "RB=\">", "NotFound=Warning", LAST); break;
					case 6:
						web_reg_save_param("orderItemId_6", "LB=id=\"{upcId6}\" data-item-id=\"", "RB=\">", "NotFound=Warning", LAST); break;
					case 7:
						web_reg_save_param("orderItemId_7", "LB=id=\"{upcId7}\" data-item-id=\"", "RB=\">", "NotFound=Warning", LAST); break;
					case 8:
						web_reg_save_param("orderItemId_8", "LB=id=\"{upcId8}\" data-item-id=\"", "RB=\">", "NotFound=Warning", LAST); break;
					default: break;
				}
			}

		}
*/
		web_reg_save_param("orderItemId", "LB=id=\"{unavailId_1}\" data-item-id=\"", "RB=\">", "NotFound=Warning", LAST);

		web_url("OrderCalculate",
		"URL=https://{host}/shop/OrderCalculate?calculationUsageId=-1&updatePrices=1&catalogId={catalogId}&errorViewName=AjaxCheckoutDisplayView&orderId=.&langId=-1&storeId={storeId}&URL=AjaxCheckoutDisplayView",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		LAST);

		removeOutOfStockItem();

	}
	return;
}


int updateQuantity()
{
	lr_think_time ( FORM_TT ) ;

	if (atoi( lr_eval_string("{itemCatentryId_count}")) > 0 ) // && atoi( lr_eval_string("{orderItemIds_count}")) > 0 )
	{
		index = rand ( ) % lr_paramarr_len( "itemCatentryId" ) + 1 ;

		lr_save_string ( lr_paramarr_idx( "itemCatentryId" , index ) , "itemCatentryId" ) ;
		lr_save_string ( lr_paramarr_idx( "productId" , index ) , "productId" ) ;
		lr_save_string ( lr_paramarr_idx( "orderItemIds" , index ) , "orderItemId" ) ;

		//lr_continue_on_error(1);

		lr_start_transaction ( "T09_Update Quantity" ) ;

		lr_start_sub_transaction("T09_Update Quantity_S01_getSwatchesAndSizeInfo", "T09_Update Quantity");

		#if OPTIONSENABLED
			web_add_header("Accept", "*/*");
			web_add_header("Access-Control-Request-Headers", "Origin, Host, User-Agent, Accept, Accept-Encoding, Accept-Language, Referer, Request, Content-Type, Transfer-Encoding, Cache-Control, Connection, Content-Length, Content-Language, Date, Expires, Keep-Alive, Location, pragma, client_id, client_secret, grant_type, scope, Authorization, Set-Cookie, Cookie, storeId, nickName, catalogId, langId, orderId, callingPage, requestType, paymentInstructionId, customerId, act, com, a1, city, state, postal, cols, opt, fromPage, promoCode, addressField1, addressField2, zipCode, pageName, locStore, responseFormat, coreName, term, count, isRest, espotName, orderItemId, productId, oldOrderItemId, memberId, emailId, fromRest, companyId, storeLocId, quantity, catEntryId, firstname, lastname, phone, email, action, taskType, stlocId, posStores, isBOPIS, xAppConfigAttrNames, tcpOrderId, PaRes, MD, externalId, pageNumber, pageSize, categoryId, primary, addressId, identifier, corenamecategory, published, rows, searchterm, start, timeallowed, devicetype, savePayment, calc");
			web_add_header("Access-Control-Request-Method", "CONNECT, DELETE, GET, OPTIONS, PATCH, POST, PUT, TRACE");
			web_add_header("Origin", "https://{host}");

			web_custom_request("getSwatchesAndSizeInfo",
				"URL=https://{api_host}/getSwatchesAndSizeInfo",
			   "Method=OPTIONS",
			   "Resource=0",
			LAST);
		#endif

		web_add_header("catalogId", lr_eval_string("{catalogId}") );
		web_add_header("storeId", lr_eval_string("{storeId}") );
		web_add_header("productId", lr_eval_string("{productId}") );
		web_add_header("langId", "-1");
		web_custom_request("getSwatchesAndSizeInfo",
			"URL=https://{api_host}/getSwatchesAndSizeInfo",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		lr_end_sub_transaction("T09_Update Quantity_S01_getSwatchesAndSizeInfo", LR_AUTO);

		lr_start_sub_transaction("T09_Update Quantity_S02_getSKUInventoryandProductCounterDetails", "T09_Update Quantity");

		#if OPTIONSENABLED
			web_add_header("Accept", "*/*");
			web_add_header("Access-Control-Request-Headers", "Origin, Host, User-Agent, Accept, Accept-Encoding, Accept-Language, Referer, Request, Content-Type, Transfer-Encoding, Cache-Control, Connection, Content-Length, Content-Language, Date, Expires, Keep-Alive, Location, pragma, client_id, client_secret, grant_type, scope, Authorization, Set-Cookie, Cookie, storeId, nickName, catalogId, langId, orderId, callingPage, requestType, paymentInstructionId, customerId, act, com, a1, city, state, postal, cols, opt, fromPage, promoCode, addressField1, addressField2, zipCode, pageName, locStore, responseFormat, coreName, term, count, isRest, espotName, orderItemId, productId, oldOrderItemId, memberId, emailId, fromRest, companyId, storeLocId, quantity, catEntryId, firstname, lastname, phone, email, action, taskType, stlocId, posStores, isBOPIS, xAppConfigAttrNames, tcpOrderId, PaRes, MD, externalId, pageNumber, pageSize, categoryId, primary, addressId, identifier, corenamecategory, published, rows, searchterm, start, timeallowed, devicetype, savePayment, calc");
			web_add_header("Access-Control-Request-Method", "CONNECT, DELETE, GET, OPTIONS, PATCH, POST, PUT, TRACE");
			web_add_header("Origin", "https://{host}");

			web_custom_request("getSKUInventoryandProductCounterDetails",
				"URL=https://{api_host}/tcpproduct/getSKUInventoryandProductCounterDetails",
			   "Method=OPTIONS",
			   "Resource=0",
			LAST);
		#endif

		web_add_header("catalogId", lr_eval_string("{catalogId}") );
		web_add_header("storeId", lr_eval_string("{storeId}") );
		web_add_header("productId", lr_eval_string("{productId}") );
		web_add_header("langId", "-1");
		web_custom_request("getSKUInventoryandProductCounterDetails",
			"URL=https://{api_host}/tcpproduct/getSKUInventoryandProductCounterDetails",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		lr_end_sub_transaction("T09_Update Quantity_S02_getSKUInventoryandProductCounterDetails", LR_AUTO);

		lr_start_sub_transaction("T09_Update Quantity_S03_updateOrderItem", "T09_Update Quantity");

		#if OPTIONSENABLED
			web_add_header("Accept", "*/*");
			web_add_header("Access-Control-Request-Headers", "Origin, Host, User-Agent, Accept, Accept-Encoding, Accept-Language, Referer, Request, Content-Type, Transfer-Encoding, Cache-Control, Connection, Content-Length, Content-Language, Date, Expires, Keep-Alive, Location, pragma, client_id, client_secret, grant_type, scope, Authorization, Set-Cookie, Cookie, storeId, nickName, catalogId, langId, orderId, callingPage, requestType, paymentInstructionId, customerId, act, com, a1, city, state, postal, cols, opt, fromPage, promoCode, addressField1, addressField2, zipCode, pageName, locStore, responseFormat, coreName, term, count, isRest, espotName, orderItemId, productId, oldOrderItemId, memberId, emailId, fromRest, companyId, storeLocId, quantity, catEntryId, firstname, lastname, phone, email, action, taskType, stlocId, posStores, isBOPIS, xAppConfigAttrNames, tcpOrderId, PaRes, MD, externalId, pageNumber, pageSize, categoryId, primary, addressId, identifier, corenamecategory, published, rows, searchterm, start, timeallowed, devicetype, savePayment, calc");
			web_add_header("Access-Control-Request-Method", "CONNECT, DELETE, GET, OPTIONS, PATCH, POST, PUT, TRACE");
			web_add_header("Origin", "https://{host}");

			web_custom_request("Quantity_S01_updateOrderItem",
				"URL=https://{api_host}/updateOrderItem",
			   "Method=OPTIONS",
			   "Resource=0",
			LAST);
		#endif

		web_add_header("Content-Type", "application/json");
		web_add_header("storeId", lr_eval_string("{storeId}") );
		web_add_header("catalogId", lr_eval_string("{catalogId}") );
		web_add_header("langId", "-1");
		registerErrorCodeCheck();
		web_custom_request("Quantity_S01_updateOrderItem",
			"URL=https://{api_host}/updateOrderItem",
			"Method=PUT",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTML",
			"Body={\"orderItem\":[{\"orderItemId\":\"{orderItemId}\",\"xitem_catEntryId\":\"{itemCatentryId}\",\"quantity\":\"2\"}]}",
			LAST);
		lr_end_sub_transaction("T09_Update Quantity_S03_updateOrderItem", LR_AUTO);

		lr_start_sub_transaction ("T09_Update Quantity_S04_getOrderDetails", "T09_Update Quantity" ) ;
		#if OPTIONSENABLED
			call_OPTIONS("getOrderDetails");
		#endif

		addHeader();
		web_add_header("locStore", "True");
		web_add_header("pageName", "fullOrderInfo");
		web_add_header("calc", "true");
		web_reg_save_param("piAmountonUpdateQuantity", "LB=\"piAmount\": \"", "RB=\"", "NotFound=Warning", LAST); //"piAmount": "28.85",\n
		web_custom_request("Quantity_S04_getOrderDetails",
			"URL=https://{api_host}/getOrderDetails",
			"Method=GET",
			"Resource=0",
			"RecContentType=text/html",
			"Snapshot=t28.inf",
			"Mode=HTML",
			"EncType=application/x-www-form-urlencoded",
			LAST);
		lr_end_sub_transaction ("T09_Update Quantity_S04_getOrderDetails", LR_AUTO) ;
		//lr_continue_on_error(0);
		lr_save_string("T09_Update Quantity", "mainTransaction");
		tcp_api2("tcporder/getAllCoupons", "GET",  lr_eval_string("{mainTransaction}") );
		web_add_header("action", "get");
		web_add_header("Accept", "application/json");
		tcp_api2("tcporder/getFavouriteStoreLocation", "GET",  lr_eval_string("{mainTransaction}") );

		if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 ) //if no error code and message
		{
			lr_end_transaction ( "T09_Update Quantity" , LR_PASS) ;
			return LR_PASS;
		}
		else
		{
			lr_fail_trans_with_error( lr_eval_string("T09_Update Quantity Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_transaction ( lr_eval_string ( "T09_Update Quantity" ) , LR_FAIL ) ;
			return LR_FAIL;
		}
	}
	return 0;
} // end updateQuantity()


void viewCart()
{

	if (CACHE_PRIME_MODE == 1)
		return;

	lr_think_time ( LINK_TT ) ;
	lr_start_transaction ( "T06_View_Cart" ) ;

	//lr_continue_on_error(1);
	//	lr_set_debug_message(16|8|4|2,1);

	lr_start_sub_transaction ( "T06_View_Cart_S01_MyBag", "T06_View_Cart" );
	web_custom_request("bag",
		"URL=https://{host}/{country}/bag",
		"Method=GET",
		"Resource=0",
		"Mode=HTTP",
		LAST);
	lr_end_sub_transaction("T06_View_Cart_S01_MyBag", LR_AUTO);

//	tcp_api2("tcporder/getXAppConfigValues", "GET", "T06_View_Cart" );

	lr_start_sub_transaction ( "T06_View_Cart_S02_GetOrderDetails", "T06_View_Cart" );
	#if OPTIONSENABLED
		call_OPTIONS("getOrderDetails");
	#endif
	web_reg_save_param("itemQuantity", "LB=\"qty\": ", "RB=,", "NotFound=Warning", "Ord=All", LAST); //\t\t\t"qty": 2,\n
	web_reg_save_param("itemCatentryId", "LB=\"itemCatentryId\": ", "RB=,", "NotFound=Warning", "Ord=All", LAST); //\t\t\t"itemCatentryId": 781088,\n		  -->  "itemCatentryId": 787085,
	web_reg_save_param("productId", "LB=\"productId\": \"", "RB=\",", "NotFound=Warning", "Ord=All", LAST); //  "productId": "426632",\n
	web_reg_save_param("cartCount", "LB=\"cartCount\": ", "RB=,", "NotFound=Warning", LAST); //"cartCount": 44,\n
	web_reg_save_param("orderItemIds", "LB=\"orderItemId\": ", "RB=,", "NotFound=Warning", "Ord=All", LAST); //\t\t\t"orderItemId": 8007970018,\n// 03/01/17
	web_reg_save_param("piAmount", "LB=\"piAmount\": \"", "RB=\",", "NotFound=Warning", LAST); //"piAmount": "7.95",\n  "piAmount": "22.30",\n
	web_reg_save_param("couponExist", "LB=\"code\": \"", "RB=\",", "NotFound=Warning", LAST); //"code": "FORTYOFF",
	web_reg_save_param("orderId", "LB=\"subOrderId\": \"", "RB=\"", "NotFound=Warning", LAST); //parentOrderId": 660240482\n

	addHeader();
	web_add_header("locStore", "True");
	web_add_header("pageName", "fullOrderInfo");
	web_add_header("calc", "true");	//02132018
//	web_add_header("calc", "false"); //02132018
	web_reg_find("TEXT/IC={","SaveCount=apiCheck", LAST);
	web_custom_request("getOrderDetails",
		"URL=https://{api_host}/getOrderDetails",
		"Method=GET",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T06_View_Cart_S02_GetOrderDetails", LR_FAIL);
	else
		lr_end_sub_transaction("T06_View_Cart_S02_GetOrderDetails", LR_AUTO);

	lr_start_sub_transaction ( "T06_View_Cart_S03_getRegisteredUserDetailsInfo", "T06_View_Cart" );
	#if OPTIONSENABLED
		call_OPTIONS("payment/getRegisteredUserDetailsInfo");
	#endif
	addHeader();

	web_reg_save_param ( "isExpress" , "LB=\"x_isExpress\": \"" , "RB=\"" , "NotFound=Warning", LAST ) ; //"x_isExpress": "true",
	web_reg_save_param ( "addressId" , "LB=\"addressId\": \"" , "RB=\",\n" , "ORD=1", "NotFound=Warning", LAST ) ; //"addressId": "1398789",\n
	web_reg_save_param ( "addressIdAll" , "LB=\"addressId\": \"" , "RB=\",\n" , "ORD=ALL", "NotFound=Warning", LAST ) ; //"addressId": "1398789",\n
	web_reg_find("TEXT/IC=resourceName", "SaveCount=apiCheck", LAST);
	web_custom_request("getRegisteredUserDetailsInfo",
		"URL=https://{api_host}/payment/getRegisteredUserDetailsInfo",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T06_View_Cart_S03_getRegisteredUserDetailsInfo", LR_FAIL);
	else
		lr_end_sub_transaction("T06_View_Cart_S03_getRegisteredUserDetailsInfo", LR_AUTO);

	lr_save_string("T06_View_Cart","mainTransaction");
	web_add_header("action", "get");
	tcp_api2("tcporder/getFavouriteStoreLocation", "GET", lr_eval_string("{mainTransaction}") );

	tcp_api2("tcpproduct/getPriceDetails", "GET",  lr_eval_string("{mainTransaction}") );

	lr_start_sub_transaction ( "T06_View_Cart_S05_getAllCoupons", "T06_View_Cart" );

	addHeader();
	web_reg_find("TEXT/IC=appliedCoupons", "SaveCount=apiCheck", LAST);
	web_reg_find("TEXT/IC=\"couponCode\": \"FORTYOFF\",", "SaveCount=couponFortyOffApplied", LAST);
	web_custom_request("getAllCoupons",
		"URL=https://{api_host}/tcporder/getAllCoupons",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T06_View_Cart_S05_getAllCoupons", LR_FAIL);
	else
		lr_end_sub_transaction("T06_View_Cart_S05_getAllCoupons", LR_AUTO);

	if (ESPOT_FLAG == 1)
	{
		lr_start_sub_transaction ( "T06_View_Cart_S08_getESpot1", "T06_View_Cart" );

		#if OPTIONSENABLED
			call_OPTIONS("getESpot");
		#endif

		addHeader();
		web_add_header("espotName","GlobalHeaderBannerAboveHeader,GlobalHeaderBannerWithinHeader,GlobalHeaderLogo,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,create_account_drawer,MinicartLoginBanner,MinicartRegisterBanner,drawer_myaccount_overview_banner,coupon_help,bag_empty_banner,drawer_empty_bag,MinicartLoginFromFavoritesBanner,bag_ledger_promo_banner,gift_services_espot,CheckoutConfirmationBanner,CheckoutConfirmationMPRPromo,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1");
		web_add_header("deviceType","desktop");
	//	web_add_header("espotName","GlobalHeaderBannerAboveHeader,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1");
		web_reg_find("TEXT/IC=espotName", "SaveCount=apiCheck", LAST);
		web_custom_request("getESpots",
			"URL=https://{api_host}/getESpot",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			LAST);

		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction("T06_View_Cart_S08_getESpot1", LR_FAIL);
		else
			lr_end_sub_transaction("T06_View_Cart_S08_getESpot1", LR_AUTO);

		lr_start_sub_transaction ( "T06_View_Cart_S08_getESpot2", "T06_View_Cart" );

		#if OPTIONSENABLED
			call_OPTIONS("getESpot");
		#endif

		addHeader();
		web_add_header("espotName","GlobalHeaderBannerAboveHeader,GlobalHeaderBannerWithinHeader,GlobalHeaderLogo,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,create_account_drawer,MinicartLoginBanner,MinicartRegisterBanner,drawer_myaccount_overview_banner,coupon_help,bag_empty_banner,drawer_empty_bag,MinicartLoginFromFavoritesBanner,bag_ledger_promo_banner,gift_services_espot,CheckoutConfirmationBanner,CheckoutConfirmationMPRPromo,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1");
		web_add_header("deviceType","desktop");
	//	web_add_header("espotName","GlobalHeaderBannerAboveHeader,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1");
		web_reg_find("TEXT/IC=espotName", "SaveCount=apiCheck", LAST);
		web_custom_request("getESpots",
			"URL=https://{api_host}/getESpot",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			LAST);

		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction("T06_View_Cart_S08_getESpot2", LR_FAIL);
		else
			lr_end_sub_transaction("T06_View_Cart_S08_getESpot2", LR_AUTO);

	}

	if (isLoggedIn == 1)
	{
		api_getESpot_second(); //IMP CHANGE BY PAVAN DUSI 06-27
	}

	//	web_add_header("action", "get");    //Removed by PavanDusi 111617
	//	tcp_api2("tcporder/getFavouriteStoreLocation", "GET",  "T06_View_Cart" );  //Removed by PavanDusi 111617

	//lr_continue_on_error(0);

	lr_end_transaction ( "T06_View_Cart" , LR_PASS ) ;

} // end viewCart

void viewCartFromLogin()
{
	/// not being called anymore, just a reminder to put a functionality to identify out of stock item

//	getOutOfStockItemIds();

} // end viewCart

void applyPromoCode(int useRewards) //1 to use rewards points, 0 to use either single or multi-use promo
{
	int couponCodeCount = 1;
	int couponLoop = 1;
	lr_think_time ( FORM_TT ) ;

	if ( useRewards == 1 ){
		lr_save_string( lr_eval_string ( "{promocodeRewards}" ) , "promocode" ) ;
//		lr_save_string( lr_eval_string ( "Y022053295EB53F6" ) , "promocode" ) ;
		lr_save_string( "Rewards" , "promotype" ) ;
	}
	else if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_PROMO_APPLY_ALL ) {
		//couponCodeCount = lr_paramarr_len("couponCodes");
		lr_save_string( "Apply All" , "promotype" ) ;
	}
	else if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_PROMO_MULTIUSE ) {
		lr_save_string( lr_eval_string ( "{multiusePromoCode}" ) , "promocode" ) ;
		lr_save_string( "Multi-Use" , "promotype" ) ;
		MULTI_USE_COUPON_FLAG = 1;
	} // end if
	else if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= (RATIO_PROMO_SINGLEUSE + RATIO_PROMO_MULTIUSE) ) {

		getPromoCode();

		if ( strcmp(lr_eval_string("{promocode}"), "") == 0 )
			lr_save_string( lr_eval_string ( "{multiusePromoCode}" ) , "promocode" ) ;

		lr_save_string( "Single-Use" , "promotype" ) ;
	}
/*	else if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= (RATIO_PROMO_SINGLEUSE + RATIO_PROMO_MULTIUSE + RATIO_ACCOUNT_REWARDS) ) {
		if (isLoggedIn == 1)
			accountRewards();
		return 0;
	}
*/
	for (couponLoop = 1; couponLoop <= couponCodeCount; couponLoop++)
	{
		if (couponCodeCount > 1)
		{
			lr_save_string( lr_paramarr_idx("couponCodes", couponLoop) , "promocode" ) ;
			lr_save_string( lr_eval_string("Y{promocode}") , "promocode" ) ;
		}

		//lr_continue_on_error(1);

		lr_start_transaction ( lr_eval_string ( "T07_Enter Promo {promotype}" ) ) ;

		lr_start_sub_transaction ( "T07_Enter Promo_S01_Coupons" , lr_eval_string ("T07_Enter Promo {promotype}"));
		#if OPTIONSENABLED
			web_add_header("Accept", "*/*");
			web_add_header("Access-Control-Request-Headers", "Origin, Host, User-Agent, Accept, Accept-Encoding, Accept-Language, Referer, Request, Content-Type, Transfer-Encoding, Cache-Control, Connection, Content-Length, Content-Language, Date, Expires, Keep-Alive, Location, pragma, client_id, client_secret, grant_type, scope, Authorization, Set-Cookie, Cookie, storeId, nickName, catalogId, langId, orderId, callingPage, requestType, paymentInstructionId, customerId, act, com, a1, city, state, postal, cols, opt, fromPage, promoCode, addressField1, addressField2, zipCode, pageName, locStore, responseFormat, coreName, term, count, isRest, espotName, orderItemId, productId, oldOrderItemId, memberId, emailId, fromRest, companyId, storeLocId, quantity, catEntryId, firstname, lastname, phone, email, action, taskType, stlocId, posStores, isBOPIS, xAppConfigAttrNames, tcpOrderId, PaRes, MD, externalId, pageNumber, pageSize, categoryId, primary, addressId, identifier, corenamecategory, published, rows, searchterm, start, timeallowed, devicetype, savePayment, calc");
			web_add_header("Access-Control-Request-Method", "CONNECT, DELETE, GET, OPTIONS, PATCH, POST, PUT, TRACE");
			web_add_header("Origin", "https://{host}");

			web_custom_request("coupons",
				"URL=https://{api_host}/payment/coupons",
			   "Method=OPTIONS",
			   "Resource=0",
			LAST);
		#endif

		registerErrorCodeCheck();
		addHeader();
		web_custom_request("coupons",
			"URL=https://{api_host}/payment/coupons",
			"Method=POST",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTTP",
			"EncType=application/json",
			"Body={\"storeId\":\"{storeId}\",\"langId\":\"-1\",\"catalogId\":\"{catalogId}\",\"URL\":\"\",\"promoCode\":\"{promocode}\",\"requesttype\":\"ajax\",\"fromPage\":\"shoppingCartDisplay\",\"taskType\":\"A\"}",
			LAST);
		lr_end_sub_transaction ("T07_Enter Promo_S01_Coupons", LR_AUTO)	;

//	lr_save_string(lr_eval_string ("T07_Enter Promo {promotype}"),"mainTransaction");

		lr_start_sub_transaction ( "T07_Enter Promo_S02_getAllCoupons" , lr_eval_string ("T07_Enter Promo {promotype}"));
		addHeader();
		web_reg_find("TEXT/IC=appliedCoupons", "SaveCount=apiCheck", LAST);
		web_custom_request("getAllCoupons",
			"URL=https://{api_host}/tcporder/getAllCoupons",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction ("T07_Enter Promo_S02_getAllCoupons", LR_FAIL)	;
		else
			lr_end_sub_transaction ("T07_Enter Promo_S02_getAllCoupons", LR_AUTO)	;

		lr_start_sub_transaction ( "T07_Enter Promo_S03_getOrderDetails", lr_eval_string ("T07_Enter Promo {promotype}") );
/*
		#if OPTIONSENABLED
			call_OPTIONS("getOrderDetails");
		#endif
*/
		addHeader();
		web_add_header("calc", "true");   //0802-4pm
		web_add_header("locStore", "True");
		web_add_header("pageName", "fullOrderInfo");
		web_url("getOrderDetails",
			"URL=https://{api_host}/getOrderDetails",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		lr_end_sub_transaction ( "T07_Enter Promo_S03_getOrderDetails", LR_AUTO );

		if ( isLoggedIn == 1) {
			lr_start_sub_transaction ( "T07_Enter Promo_S03_getPointsService", lr_eval_string ("T07_Enter Promo {promotype}") );
			addHeader();
			web_url("getPointsService",
				 "URL=https://{api_host}/payment/getPointsService",
				 "Resource=0",
				 "RecContentType=application/json",
				 LAST);
			lr_end_sub_transaction("T07_Enter Promo_S03_getPointsService", LR_AUTO);
		}

		//lr_continue_on_error(0);

		if ( strcmp( lr_eval_string("{errorCode}") ,"") != 0 && strcmp(lr_eval_string("{errorMessage}") ,"") != 0 ) //if no error code and message
		{
			lr_fail_trans_with_error( lr_eval_string("T07_Enter Promo {promotype} Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_transaction ( lr_eval_string ( "T07_Enter Promo {promotype}" ) , LR_FAIL ) ;
		}
		else {
			lr_end_transaction ( lr_eval_string ( "T07_Enter Promo {promotype}" ) , LR_PASS ) ;
			break;
		}
	}

} // end applyPromoCode


int deletePromoCode()
{
	if ( strcmp( lr_eval_string("{promocode}") , "" ) != 0 )
	{
		//lr_continue_on_error(1);
		lr_start_transaction ( "T07_Delete Promocode" ) ;

		lr_start_sub_transaction ( "T07_Delete Promocode S01_removePromotionCode" , "T07_Delete Promocode" );
		#if OPTIONSENABLED
			web_add_header("Accept", "*/*");
			web_add_header("Access-Control-Request-Headers", "Origin, Host, User-Agent, Accept, Accept-Encoding, Accept-Language, Referer, Request, Content-Type, Transfer-Encoding, Cache-Control, Connection, Content-Length, Content-Language, Date, Expires, Keep-Alive, Location, pragma, client_id, client_secret, grant_type, scope, Authorization, Set-Cookie, Cookie, storeId, nickName, catalogId, langId, orderId, callingPage, requestType, paymentInstructionId, customerId, act, com, a1, city, state, postal, cols, opt, fromPage, promoCode, addressField1, addressField2, zipCode, pageName, locStore, responseFormat, coreName, term, count, isRest, espotName, orderItemId, productId, oldOrderItemId, memberId, emailId, fromRest, companyId, storeLocId, quantity, catEntryId, firstname, lastname, phone, email, action, taskType, stlocId, posStores, isBOPIS, xAppConfigAttrNames, tcpOrderId, PaRes, MD, externalId, pageNumber, pageSize, categoryId, primary, addressId, identifier, corenamecategory, published, rows, searchterm, start, timeallowed, devicetype, savePayment, calc");
			web_add_header("Access-Control-Request-Method", "CONNECT, DELETE, GET, OPTIONS, PATCH, POST, PUT, TRACE");
			web_add_header("Origin", "https://{host}");

			web_custom_request("removePromotionCode",
				"URL=https://{api_host}/payment/removePromotionCode",
			   "Method=OPTIONS",
			   "Resource=0",
			LAST);
		#endif

		web_add_header("promoCode", lr_eval_string("{couponExist}") );  //make sure that Viewcart was called before calling deletePromoCode(),
		addHeader();
		registerErrorCodeCheck();
		web_custom_request("removePromotionCode",
			"URL=https://{api_host}/payment/removePromotionCode",
			"Method=POST",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTTP",
			"EncType=",
			LAST);
		lr_end_sub_transaction ( "T07_Delete Promocode S01_removePromotionCode" , LR_AUTO );

	lr_save_string("T07_Delete Promocode","mainTransaction");

	lr_start_sub_transaction ( lr_eval_string ( "T07_Delete Promocode S02_getAllCoupons" ), lr_eval_string ( "T07_Delete Promocode" ) );
		addHeader();
		web_url("getAllCoupons",
			"URL=https://{api_host}/tcporder/getAllCoupons",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		lr_end_sub_transaction ( "T07_Delete Promocode S02_getAllCoupons" , LR_AUTO );

		lr_start_sub_transaction ( "T07_Delete Promocode S03_getOrderDetails", "T07_Delete Promocode" );
/*
		#if OPTIONSENABLED
			call_OPTIONS("getOrderDetails");
		#endif
*/
		addHeader();
		web_add_header("calc", "true");
		web_add_header("locStore", "True");
		web_add_header("pageName", "fullOrderInfo");
		web_url("getOrderDetails",
			"URL=https://{api_host}/getOrderDetails",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		lr_end_sub_transaction ( "T07_Delete Promocode S03_getOrderDetails", LR_AUTO );

		if ( isLoggedIn == 1) {
			lr_start_sub_transaction ( "T07_Delete Promo_S04_getPointsService", lr_eval_string ("T07_Delete PromoCode") );
			addHeader();
			web_url("getPointsService",
				 "URL=https://{api_host}/payment/getPointsService",
				 "Resource=0",
				 "RecContentType=application/json",
				 LAST);
			lr_end_sub_transaction("T07_Delete Promo_S04_getPointsService", LR_AUTO);
		}

		if ( strcmp( lr_eval_string("{errorCode}") ,"") != 0 && strcmp(lr_eval_string("{errorMessage}") ,"") != 0 ) //if no error code and message
		{
			lr_fail_trans_with_error( lr_eval_string("T07_Delete Promocode Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_transaction ( "T07_Delete Promocode" , LR_FAIL ) ;
			return LR_FAIL;
		}
		else {
			lr_end_transaction ( "T07_Delete Promocode" , LR_PASS ) ;
			return LR_PASS;
		}
	}
	return 0;
} // end deletePromoCode

int deleteItem()   //Remove from Bag
{

	if ( lr_paramarr_len ( "orderItemIds" ) > 0 ){
		lr_think_time ( LINK_TT ) ;
		//lr_continue_on_error(1);

		lr_start_transaction ("T06_Cart_Item_Remove") ;

		lr_start_sub_transaction ( "T06_Cart_Item_Remove_S01_updateMultiSelectItemsToRemove", "T06_Cart_Item_Remove" );

		#if OPTIONSENABLED
			web_add_header("Accept", "*/*");
			web_add_header("Access-Control-Request-Headers", "Origin, Host, User-Agent, Accept, Accept-Encoding, Accept-Language, Referer, Request, Content-Type, Transfer-Encoding, Cache-Control, Connection, Content-Length, Content-Language, Date, Expires, Keep-Alive, Location, pragma, client_id, client_secret, grant_type, scope, Authorization, Set-Cookie, Cookie, storeId, nickName, catalogId, langId, orderId, callingPage, requestType, paymentInstructionId, customerId, act, com, a1, city, state, postal, cols, opt, fromPage, promoCode, addressField1, addressField2, zipCode, pageName, locStore, responseFormat, coreName, term, count, isRest, espotName, orderItemId, productId, oldOrderItemId, memberId, emailId, fromRest, companyId, storeLocId, quantity, catEntryId, firstname, lastname, phone, email, action, taskType, stlocId, posStores, isBOPIS, xAppConfigAttrNames, tcpOrderId, PaRes, MD, externalId, pageNumber, pageSize, categoryId, primary, addressId, identifier, corenamecategory, published, rows, searchterm, start, timeallowed, devicetype, savePayment, calc");
			web_add_header("Access-Control-Request-Method", "CONNECT, DELETE, GET, OPTIONS, PATCH, POST, PUT, TRACE");
			web_add_header("Origin", "https://{host}");

			web_custom_request("updateMultiSelectItemsToRemove",
				"URL=https://{api_host}/updateMultiSelectItemsToRemove",
			   "Method=OPTIONS",
			   "Resource=0",
			LAST);
		#endif

		web_add_header("Content-Type", "application/json");
		web_add_header("storeId", lr_eval_string("{storeId}") );
		web_add_header("catalogId", lr_eval_string("{catalogId}") );
		web_add_header("langId", "-1");
		registerErrorCodeCheck();
		lr_save_string ( lr_paramarr_random( "orderItemIds" ) , "orderItemId" ) ;
		web_custom_request("updateMultiSelectItemsToRemove",
			"URL=https://{api_host}/updateMultiSelectItemsToRemove",
			"Method=PUT",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTTP",
			"Body={\"orderItem\":[{\"orderItemId\":\"{orderItemId}\",\"quantity\":\"0\"}]}",
			LAST);
		lr_end_sub_transaction("T06_Cart_Item_Remove_S01_updateMultiSelectItemsToRemove", LR_AUTO);
/*
		lr_start_sub_transaction ( "T06_Cart_Item_Remove_S02_getPointsService", "T06_Cart_Item_Remove" );

		addHeader();
		web_url("getPointsService",
			 "URL=https://{api_host}/payment/getPointsService",
			 "Resource=0",
			 "RecContentType=application/json",
			 LAST);
		lr_end_sub_transaction("T06_Cart_Item_Remove_S02_getPointsService", LR_AUTO);
*/
		lr_start_sub_transaction ( "T06_Cart_Item_Remove_S03_getOrderDetails", "T06_Cart_Item_Remove" );
		addHeader();
		web_add_header("calc", "true");   //0802-4pm
		web_add_header("locStore", "True");
		web_add_header("pageName", "fullOrderInfo");

		web_url("getOrderDetails",
			"URL=https://{api_host}/getOrderDetails",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		lr_end_sub_transaction("T06_Cart_Item_Remove_S03_getOrderDetails", LR_AUTO);

		lr_start_sub_transaction ( "T06_Cart_Item_Remove_S04_getAllCoupons", "T06_Cart_Item_Remove" );
		addHeader();
		web_url("getAllCoupons",
			"URL=https://{api_host}/tcporder/getAllCoupons",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		lr_end_sub_transaction ( "T06_Cart_Item_Remove_S04_getAllCoupons" , LR_AUTO );

		lr_start_sub_transaction ( "T06_Cart_Item_Remove_S05_getFavouriteStoreLocation", "T06_Cart_Item_Remove" );
		addHeader();
		web_add_header("action", "get");
		web_add_header("Accept", "application/json");
		web_custom_request("getFavouriteStoreLocation",
			"URL=https://{api_host}/tcporder/getFavouriteStoreLocation",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTTP",
			LAST);

		lr_end_sub_transaction ( "T06_Cart_Item_Remove_S05_getFavouriteStoreLocation" , LR_AUTO );

		if ( isLoggedIn == 1) {
			lr_start_sub_transaction ( "T06_Cart_Item_Remove_S06_getPointsService", lr_eval_string ("T06_Cart_Item_Remove") );
			addHeader();
			web_url("getPointsService",
				 "URL=https://{api_host}/payment/getPointsService",
				 "Resource=0",
				 "RecContentType=application/json",
				 LAST);
			lr_end_sub_transaction("T06_Cart_Item_Remove_S06_getPointsService", LR_AUTO);
		}

		//lr_continue_on_error(0);

		if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 ) //if no error code and message
		{	lr_end_transaction ("T06_Cart_Item_Remove" , LR_PASS ) ;
			return LR_PASS;
		}
		else
		{
			lr_fail_trans_with_error( lr_eval_string("T06_Cart_Item_Remove Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_transaction ( lr_eval_string ( "T06_Cart_Item_Remove" ) , LR_FAIL ) ;
			return LR_FAIL;
		}

	}
	return 0;
} // end deleteItem


void Submit_Pickup_Detail()
{

	if (strcmp( lr_eval_string("{addBopisToCart}"), "true") == 0 )
	{
		lr_think_time ( FORM_TT ) ;

		lr_start_transaction ( "T11_Submit_Pickup_Detail") ;

//		tcp_api2("tcporder/getXAppConfigValues", "GET", "T11_Submit_Pickup_Detail" );

		web_reg_save_param ( "addressId" , "LB=\"addressId\": \"" , "RB=\",\n" , "ORD=1", "NotFound=Warning", LAST ) ; //"addressId": "1398789",\n

		#if OPTIONSENABLED
			call_OPTIONS("payment/addAddress");
		#endif

		addHeader();

		lr_start_sub_transaction ("T11_Submit_Pickup_Detail S01_addAddress", "T11_Submit_Pickup_Detail" ) ;
		web_custom_request("addAddress",
			"URL=https://{api_host}/payment/addAddress",
			"Method=POST",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTTP",
			"EncType=application/json", "Body={\"contact\":[{\"addressType\":\"Shipping\",\"firstName\":\"Joe\",\"lastName\":\"User\",\"phone2\":\"2014534613\",\"email1\":\"bitrogue@mailinator.com\",\"email2\":\"\"}]}",
			LAST);
		lr_end_sub_transaction ("T11_Submit_Pickup_Detail S01_addAddress", LR_AUTO) ;
//			"EncType=application/json", "Body={\"contact\":[{\"addressType\":\"Shipping\",\"firstName\":\"Joe\",\"lastName\":\"User\",\"phone2\":\"2014534613\",\"email1\":\"mannyy{emailVerification}@gmail.com\",\"email2\":\"\"}]}",

		lr_start_sub_transaction ("T11_Submit_Pickup_Detail S02_getOrderDetails", "T11_Submit_Pickup_Detail" ) ;
/*
		#if OPTIONSENABLED
			call_OPTIONS("getOrderDetails");
		#endif
*/
		addHeader();
		web_add_header("calc", "true");
		web_add_header("locStore", "True");
		web_add_header("pageName", "fullOrderInfo");
		web_reg_find("TEXT/IC={", "SaveCount=apiCheck", LAST);
		web_url("getOrderDetails",
			"URL=https://{api_host}/getOrderDetails",
			"TargetFrame=",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction ("T11_Submit_Pickup_Detail S02_getOrderDetails", LR_FAIL) ;
		else
			lr_end_sub_transaction ("T11_Submit_Pickup_Detail S02_getOrderDetails", LR_AUTO) ;


		lr_start_sub_transaction ("T11_Submit_Pickup_Detail S03_getShipmentMethods", "T11_Submit_Pickup_Detail" ) ;
//		web_add_header("Accept", "*/*");
/*		web_add_header("Access-Control-Request-Headers", "Origin, Host, User-Agent, Accept, Accept-Encoding, Accept-Language, Referer, Request, Content-Type, Transfer-Encoding, Cache-Control, Connection, Content-Length, Content-Language, Date, Expires, Keep-Alive, Location, pragma, client_id, client_secret, grant_type, scope, Authorization, Set-Cookie, Cookie, storeId, nickName, catalogId, langId, orderId, callingPage, requestType, paymentInstructionId, customerId, act, com, a1, city, state, postal, cols, opt, fromPage, promoCode, addressField1, addressField2, zipCode, pageName, locStore, responseFormat, coreName, term, count, isRest, espotName, orderItemId, productId, oldOrderItemId, memberId, emailId, fromRest, companyId, storeLocId, quantity, catEntryId, firstname, lastname, phone, email, action, taskType, stlocId, posStores, isBOPIS, xAppConfigAttrNames, tcpOrderId, PaRes, MD, externalId, pageNumber, pageSize, categoryId, primary, addressId, identifier, corenamecategory, published, rows, searchterm, start, timeallowed, devicetype, savePayment, calc");
		web_add_header("Access-Control-Request-Method", "CONNECT, DELETE, GET, OPTIONS, PATCH, POST, PUT, TRACE");
		web_add_header("Origin", "https://{host}");

		web_url("getShipmentMethods",
			"URL=https://{api_host}/payment/getShipmentMethods",
		   "Method=OPTIONS",
		   "Resource=0",
		LAST);

		addHeader();
		web_add_header("state", lr_eval_string("{savedState}"));
		web_add_header("zipCode", lr_eval_string("{savedZipCode}"));
		web_add_header("addressField1", lr_eval_string("{savedAddressLine}"));

		web_reg_find("TEXT/IC=jsonArr", "SaveCount=apiCheck", LAST);
		web_url("getShipmentMethods",
			"URL=https://{api_host}/payment/getShipmentMethods",
			"TargetFrame=",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction ("T11_Submit_Pickup_Detail S03_getShipmentMethods", LR_FAIL) ;
		else
			lr_end_sub_transaction ("T11_Submit_Pickup_Detail S03_getShipmentMethods", LR_AUTO) ;

		lr_end_transaction ( "T11_Submit_Pickup_Detail" , LR_PASS) ;

	}

}

int proceedToCheckout_ShippingView() //Login First Scenario
{
	lr_think_time ( LINK_TT ) ;
	//lr_continue_on_error(1);

	lr_start_transaction ( "T09_Proceed_To_Checkout Registered" ) ;

//	tcp_api2("tcporder/getXAppConfigValues", "GET", "T09_Proceed_To_Checkout Registered" );

	lr_start_sub_transaction ("T09_Proceed_To_Checkout_getUnqualifiedItems", "T09_Proceed_To_Checkout Registered" ) ;
	#if OPTIONSENABLED
		web_add_header("Accept", "* / *");
		web_add_header("Access-Control-Request-Headers", "Origin, Host, User-Agent, Accept, Accept-Encoding, Accept-Language, Referer, Request, Content-Type, Transfer-Encoding, Cache-Control, Connection, Content-Length, Content-Language, Date, Expires, Keep-Alive, Location, pragma, client_id, client_secret, grant_type, scope, Authorization, Set-Cookie, Cookie, storeId, nickName, catalogId, langId, orderId, callingPage, requestType, paymentInstructionId, customerId, act, com, a1, city, state, postal, cols, opt, fromPage, promoCode, addressField1, addressField2, zipCode, pageName, locStore, responseFormat, coreName, term, count, isRest, espotName, orderItemId, productId, oldOrderItemId, memberId, emailId, fromRest, companyId, storeLocId, quantity, catEntryId, firstname, lastname, phone, email, action, taskType, stlocId, posStores, isBOPIS, xAppConfigAttrNames, tcpOrderId, PaRes, MD, externalId, pageNumber, pageSize, categoryId, primary, addressId, identifier, corenamecategory, published, rows, searchterm, start, timeallowed, devicetype, savePayment, calc");
		web_add_header("Access-Control-Request-Method", "CONNECT, DELETE, GET, OPTIONS, PATCH, POST, PUT, TRACE");
		web_add_header("Origin", "https://{host}");

		web_custom_request("getUnqualifiedItems",
			"URL=https://{api_host}/tcporder/getUnqualifiedItems",
		   "Method=OPTIONS",
		   "Resource=0",
		LAST);
	#endif

	addHeader();
	web_custom_request("getUnqualifiedItems",
		"URL=https://{api_host}/tcporder/getUnqualifiedItems",
		"Method=GET",
		"Resource=0",
		"Mode=HTML",
		LAST);
	lr_end_sub_transaction ("T09_Proceed_To_Checkout_getUnqualifiedItems", LR_AUTO) ;

	lr_start_sub_transaction ("T09_Proceed_To_Checkout shipping", "T09_Proceed_To_Checkout Registered" ) ;
	web_custom_request("checkout",
//		"URL=https://{host}/{country}/checkout",
		"URL=https://{host}/{country}/checkout/shipping/",
		"Method=GET",
		"Resource=0",
		"Mode=HTML",
		LAST);
	lr_end_sub_transaction ("T09_Proceed_To_Checkout shipping", LR_AUTO) ;

	if( strcmp(lr_eval_string("{isExpress}"), "true") == 0 ) {

		lr_start_sub_transaction ("T09_Proceed_To_Checkout expressCheckout", "T09_Proceed_To_Checkout Registered" ) ;
		addHeader();
		web_add_header("prescreen", "true");
		web_custom_request("expressCheckout",
			"URL=https://{api_host}/tcporder/expressCheckout",
			"Method=POST",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		lr_end_sub_transaction ("T09_Proceed_To_Checkout expressCheckout", LR_AUTO) ;
	}

	lr_start_sub_transaction ("T09_Proceed_To_Checkout_getOrderDetails", "T09_Proceed_To_Checkout Registered" ) ;
/*
	#if OPTIONSENABLED
		call_OPTIONS("getOrderDetails");
	#endif
*/
	addHeader();
	web_add_header("locStore", "True");
	web_add_header("pageName", "fullOrderInfo");
	web_reg_find("TEXT/IC={", "SaveCount=apiCheck", LAST);
	web_reg_save_param("cartCount", "LB=artCount\": ", "RB=,", "NotFound=Warning", LAST);
	web_reg_save_param("piId", "LB=\"piId\": \"", "RB=\",", "NotFound=Warning", LAST); //"piId": "7.95",\n

	web_custom_request("getOrderDetails",
		"URL=https://{api_host}/getOrderDetails",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction ("T09_Proceed_To_Checkout_getOrderDetails", LR_FAIL) ;
	else
		lr_end_sub_transaction ("T09_Proceed_To_Checkout_getOrderDetails", LR_AUTO) ;

	if( strcmp(lr_eval_string("{isExpress}"), "true") == 0 ) {
		lr_start_sub_transaction ("T09_Proceed_To_Checkout getOrderDetails_2","T09_Proceed_To_Checkout Registered");
		addHeader();
		web_add_header("locStore", "True");
		web_add_header("pageName", "fullOrderInfo");
		web_custom_request("getOrderDetails",
			"URL=https://{api_host}/getOrderDetails",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		lr_end_sub_transaction ("T09_Proceed_To_Checkout getOrderDetails_2",LR_AUTO);
	}


	lr_start_sub_transaction ("T09_Proceed_To_Checkout_getRegisteredUserDetailsInfo", "T09_Proceed_To_Checkout Registered" ) ;
/*
	#if OPTIONSENABLED
		call_OPTIONS("payment/getRegisteredUserDetailsInfo");
	#endif
*/
	addHeader();
	web_reg_find("TEXT/IC=resourceName", "SaveCount=apiCheck", LAST);
	web_custom_request("getRegisteredUserDetailsInfo",
		"URL=https://{api_host}/payment/getRegisteredUserDetailsInfo",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction ("T09_Proceed_To_Checkout_getRegisteredUserDetailsInfo", LR_FAIL) ;
	else
		lr_end_sub_transaction ("T09_Proceed_To_Checkout_getRegisteredUserDetailsInfo", LR_AUTO) ;

	lr_start_sub_transaction ("T09_Proceed_To_Checkout_getAllCoupons", "T09_Proceed_To_Checkout Registered" ) ;
	addHeader();
	web_reg_find("TEXT/IC=appliedCoupons", "SaveCount=apiCheck", LAST);
	web_custom_request("getAllCoupons",
		"URL=https://{api_host}/tcporder/getAllCoupons",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction ("T09_Proceed_To_Checkout_getAllCoupons", LR_FAIL) ;
	else
		lr_end_sub_transaction ("T09_Proceed_To_Checkout_getAllCoupons", LR_AUTO) ;

	if( strcmp(lr_eval_string("{isExpress}"), "true") == 0 ) {
		lr_start_sub_transaction ("T09_Proceed_To_Checkout_getAllCoupons_2","T09_Proceed_To_Checkout Registered");
		addHeader();
		web_url("getAllCoupons",
			"URL=https://{api_host}/tcporder/getAllCoupons",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		lr_end_sub_transaction ("T09_Proceed_To_Checkout_getAllCoupons_2",LR_AUTO);
	}

/*	lr_start_sub_transaction ("T09_Proceed_To_Checkout_getPointsService", "T09_Proceed_To_Checkout Registered" ) ;

	#if OPTIONSENABLED
		call_OPTIONS("payment/getPointsService");
	#endif
*/
/*	addHeader();
	web_reg_find("TEXT/IC=LoyaltyWebsiteInd", "SaveCount=apiCheck", LAST);
	web_custom_request("getPointsService",
		"URL=https://{api_host}/payment/getPointsService",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction ("T09_Proceed_To_Checkout_getPointsService", LR_FAIL) ;
	else
		lr_end_sub_transaction ("T09_Proceed_To_Checkout_getPointsService", LR_AUTO) ;
*/
	web_add_header("action", "get"); //Removed by PavanDusi 111617, 03212018 brought back --romano
	tcp_api2("tcporder/getFavouriteStoreLocation", "GET",  "T09_Proceed_To_Checkout Registered" ); //Removed by PavanDusi 111617, 03212018 brought back --romano

	lr_start_sub_transaction ("T09_Proceed_To_Checkout_giftOptionsCmd", "T09_Proceed_To_Checkout Registered" ) ;
/*
	#if OPTIONSENABLED
		call_OPTIONS("payment/giftOptionsCmd");
	#endif
*/
	addHeader();
	web_reg_find("TEXT/IC=giftOptions", "SaveCount=apiCheck", LAST);
	web_custom_request("giftOptionsCmd",
		"URL=https://{api_host}/payment/giftOptionsCmd",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction ("T09_Proceed_To_Checkout_giftOptionsCmd", LR_FAIL) ;
	else
		lr_end_sub_transaction ("T09_Proceed_To_Checkout_giftOptionsCmd", LR_AUTO) ;

	if (ESPOT_FLAG == 1)
	{
		lr_start_sub_transaction ( "T09_Proceed_To_Checkout_getESpots", "T09_Proceed_To_Checkout Registered" );

		#if OPTIONSENABLED
			call_OPTIONS("getESpot");
		#endif

		addHeader();
		web_add_header("espotName","GlobalHeaderBannerAboveHeader,GlobalHeaderBannerWithinHeader,GlobalHeaderLogo,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,create_account_drawer,MinicartLoginBanner,MinicartRegisterBanner,drawer_myaccount_overview_banner,coupon_help,bag_empty_banner,drawer_empty_bag,MinicartLoginFromFavoritesBanner,bag_ledger_promo_banner,gift_services_espot,CheckoutConfirmationBanner,CheckoutConfirmationMPRPromo,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1");
		web_add_header("deviceType","desktop");

	//	web_add_header("espotName","GlobalHeaderBannerAboveHeader,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1");
		web_reg_find("TEXT/IC=espotName", "SaveCount=apiCheck", LAST);
		web_custom_request("getESpots",
			"URL=https://{api_host}/getESpot",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction("T09_Proceed_To_Checkout_getESpots", LR_FAIL);
		else
			lr_end_sub_transaction("T09_Proceed_To_Checkout_getESpots", LR_AUTO);

		api_getESpot_second();
	}

	lr_start_sub_transaction ("T09_Proceed_To_Checkout_getAddressFromBook", "T09_Proceed_To_Checkout Registered" ) ;

	web_reg_save_param("savedaddressId", "LB=addressLine\": [\r\n\"", "RB=\"", "ORD=1","NotFound=Warning", LAST);
//	web_reg_save_param("addressLine2", "LB=addressLine\": [\r\n\"", "RB=\"", "ORD=1","NotFound=Warning", LAST);
	web_reg_save_param("savedState", "LB=state\": \"", "RB=\"", "NotFound=Warning", LAST);
	web_reg_save_param("savedZipCode", "LB=zipCode\": \"", "RB=\"", "NotFound=Warning", LAST);
	web_reg_save_param_json( "ParamName=savedAddressLine", "QueryString=$.contact[0]...addressLine[0].", "SelectAll=NO", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
//	web_reg_save_param_json( "ParamName=state", "QueryString=$.contact[0]...state", "SelectAll=NO", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
//	web_reg_save_param_json( "ParamName=zipCode", "QueryString=$.contact[0]...zipCode", "SelectAll=NO", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);

	addHeader();
	web_custom_request("getAddressFromBook",
		"URL=https://{api_host}/payment/getAddressFromBook",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	lr_end_sub_transaction ("T09_Proceed_To_Checkout_getAddressFromBook", LR_AUTO) ;
//lr_message(lr_eval_string("{savedAddressLine}"));

	lr_start_sub_transaction ( "T09_Proceed_To_Checkout_getCreditCardDetails", "T09_Proceed_To_Checkout Registered" );

	web_reg_save_param("expMonth", "LB=expMonth\": \"", "RB=\"", "NotFound=Warning", LAST);
	web_reg_save_param("expYear", "LB=expYear\": \"", "RB=\"", "NotFound=Warning", LAST);

	addHeader();
	web_add_header("isRest", "true" );
	web_custom_request("getCreditCardDetails",
		"URL=https://{api_host}/payment/getCreditCardDetails",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	lr_end_sub_transaction("T09_Proceed_To_Checkout_getCreditCardDetails", LR_AUTO);

	lr_start_sub_transaction ( "T09_Proceed_To_Checkout_getShipmentMethods", "T09_Proceed_To_Checkout Registered" );
	#if OPTIONSENABLED
		web_add_header("Accept", "*/*");
		web_add_header("Access-Control-Request-Headers", "Origin, Host, User-Agent, Accept, Accept-Encoding, Accept-Language, Referer, Request, Content-Type, Transfer-Encoding, Cache-Control, Connection, Content-Length, Content-Language, Date, Expires, Keep-Alive, Location, pragma, client_id, client_secret, grant_type, scope, Authorization, Set-Cookie, Cookie, storeId, nickName, catalogId, langId, orderId, callingPage, requestType, paymentInstructionId, customerId, act, com, a1, city, state, postal, cols, opt, fromPage, promoCode, addressField1, addressField2, zipCode, pageName, locStore, responseFormat, coreName, term, count, isRest, espotName, orderItemId, productId, oldOrderItemId, memberId, emailId, fromRest, companyId, storeLocId, quantity, catEntryId, firstname, lastname, phone, email, action, taskType, stlocId, posStores, isBOPIS, xAppConfigAttrNames, tcpOrderId, PaRes, MD, externalId, pageNumber, pageSize, categoryId, primary, addressId, identifier, corenamecategory, published, rows, searchterm, start, timeallowed, devicetype, savePayment, calc");
		web_add_header("Access-Control-Request-Method", "CONNECT, DELETE, GET, OPTIONS, PATCH, POST, PUT, TRACE");
		web_add_header("Origin", "https://{host}");

		web_custom_request("getShipmentMethods",
			"URL=https://{api_host}/payment/getShipmentMethods",
		   "Method=OPTIONS",
		   "Resource=0",
		LAST);
	#endif

	addHeader();
	if (strlen(lr_eval_string("{savedZipCode}")) == 5 ) {
		web_add_header("state", lr_eval_string("{savedState}") );
		web_add_header("zipCode", lr_eval_string("{savedZipCode}") );
		web_add_header("addressField1", lr_eval_string("{savedAddressLine}") );
	} else {
		if (strcmp(lr_eval_string("{storeId}"), "10151") == 0) {
			web_add_header("state", "NJ");
			web_add_header("zipCode", "07094");
		} else {
			web_add_header("state", lr_eval_string("{guestState}"));
			web_add_header("zipCode", lr_eval_string("{guestZip}"));
		}
	}
	web_custom_request("getShipmentMethods",
		"URL=https://{api_host}/payment/getShipmentMethods",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	lr_end_sub_transaction("T09_Proceed_To_Checkout_getShipmentMethods", LR_AUTO);

	lr_end_transaction ( "T09_Proceed_To_Checkout Registered" , LR_PASS) ;

	//lr_continue_on_error(0);

	Submit_Pickup_Detail();

	return LR_PASS;

} // end proceedToCheckout_ShippingView


int proceedAsGuest()
{
	lr_think_time ( LINK_TT ) ;
	//lr_continue_on_error(1);

	lr_start_transaction ( "T09_Proceed_To_Checkout Guest" ) ;

//	tcp_api2("tcporder/getXAppConfigValues", "GET", "T09_Proceed_To_Checkout Guest" );

	lr_start_sub_transaction ("T09_Proceed_To_Checkout_getUnqualifiedItems", "T09_Proceed_To_Checkout Guest" ) ;

	#if OPTIONSENABLED
		web_add_header("Accept", "*/*");
		web_add_header("Access-Control-Request-Headers", "Origin, Host, User-Agent, Accept, Accept-Encoding, Accept-Language, Referer, Request, Content-Type, Transfer-Encoding, Cache-Control, Connection, Content-Length, Content-Language, Date, Expires, Keep-Alive, Location, pragma, client_id, client_secret, grant_type, scope, Authorization, Set-Cookie, Cookie, storeId, nickName, catalogId, langId, orderId, callingPage, requestType, paymentInstructionId, customerId, act, com, a1, city, state, postal, cols, opt, fromPage, promoCode, addressField1, addressField2, zipCode, pageName, locStore, responseFormat, coreName, term, count, isRest, espotName, orderItemId, productId, oldOrderItemId, memberId, emailId, fromRest, companyId, storeLocId, quantity, catEntryId, firstname, lastname, phone, email, action, taskType, stlocId, posStores, isBOPIS, xAppConfigAttrNames, tcpOrderId, PaRes, MD, externalId, pageNumber, pageSize, categoryId, primary, addressId, identifier, corenamecategory, published, rows, searchterm, start, timeallowed, devicetype, savePayment, calc");
		web_add_header("Access-Control-Request-Method", "CONNECT, DELETE, GET, OPTIONS, PATCH, POST, PUT, TRACE");
		web_add_header("Origin", "https://{host}");

		web_custom_request("getUnqualifiedItems",
			"URL=https://{api_host}/tcporder/getUnqualifiedItems",
		   "Method=OPTIONS",
		   "Resource=0",
		LAST);
	#endif

	addHeader();
	web_custom_request("getUnqualifiedItems",
		"URL=https://{api_host}/tcporder/getUnqualifiedItems",
		"Method=GET",
		"Resource=0",
		"Mode=HTML",
		LAST);
	lr_end_sub_transaction ("T09_Proceed_To_Checkout_getUnqualifiedItems", LR_AUTO) ;

	lr_start_sub_transaction ("T09_Proceed_To_Checkout shipping", "T09_Proceed_To_Checkout Guest" ) ;
	web_custom_request("checkout",
		"URL=https://{host}/{country}/checkout/shipping",
		"Method=GET",
		"Resource=0",
		"Mode=HTML",
		LAST);
	lr_end_sub_transaction ("T09_Proceed_To_Checkout shipping", LR_AUTO) ;

	lr_start_sub_transaction ("T09_Proceed_To_Checkout_getOrderDetails", "T09_Proceed_To_Checkout Guest" ) ;
/*
	#if OPTIONSENABLED
		call_OPTIONS("getOrderDetails");
	#endif
*/
	addHeader();
	web_add_header("locStore", "True");
	web_add_header("pageName", "fullOrderInfo");
	web_reg_find("TEXT/IC={", "SaveCount=apiCheck", LAST);
	web_url("getOrderDetails",
		"URL=https://{api_host}/getOrderDetails",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) ==0 )
		lr_end_sub_transaction ("T09_Proceed_To_Checkout_getOrderDetails", LR_FAIL) ;
	else
		lr_end_sub_transaction ("T09_Proceed_To_Checkout_getOrderDetails", LR_AUTO) ;

	lr_start_sub_transaction ("T09_Proceed_To_Checkout_getRegisteredUserDetailsInfo", "T09_Proceed_To_Checkout Guest" ) ;
/*
	#if OPTIONSENABLED
		call_OPTIONS("payment/getRegisteredUserDetailsInfo");
	#endif
*/
	addHeader();
	web_reg_find("TEXT/IC=resourceName", "SaveCount=apiCheck", LAST);
	web_url("getRegisteredUserDetailsInfo",
		"URL=https://{api_host}/payment/getRegisteredUserDetailsInfo",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction ("T09_Proceed_To_Checkout_getRegisteredUserDetailsInfo", LR_FAIL) ;
	else
		lr_end_sub_transaction ("T09_Proceed_To_Checkout_getRegisteredUserDetailsInfo", LR_AUTO) ;
/*
	lr_save_string("T09_Proceed_To_Checkout Guest","mainTransaction");
	tcp_api2("tcpproduct/getPriceDetails", "GET",  lr_eval_string("{mainTransaction}") );
*/
	lr_start_sub_transaction ("T09_Proceed_To_Checkout_getAllCoupons", "T09_Proceed_To_Checkout Guest" ) ;
	addHeader();
	web_reg_find("TEXT/IC=appliedCoupons", "SaveCount=apiCheck", LAST);
	web_url("getAllCoupons",
		"URL=https://{api_host}/tcporder/getAllCoupons",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction ("T09_Proceed_To_Checkout_getAllCoupons", LR_FAIL) ;
	else
		lr_end_sub_transaction ("T09_Proceed_To_Checkout_getAllCoupons", LR_AUTO) ;

	web_reg_save_param("savedState", "LB=state\": \"", "RB=\"", "NotFound=Warning", LAST);
	web_reg_save_param("savedZipCode", "LB=zipCode\": \"", "RB=\"", "NotFound=Warning", LAST);
	web_reg_save_param_json( "ParamName=savedAddressLine", "QueryString=$.contact[0]...addressLine[0].", "SelectAll=NO", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);

	lr_start_sub_transaction ("T09_Proceed_To_Checkout_getShipmentMethods", "T09_Proceed_To_Checkout Guest" ) ;
	addHeader();
	web_add_header("state",lr_eval_string("{guestState}"));
//	web_add_header("zipCode",lr_eval_string("{guestZip}"));
	web_add_header("addressField1",lr_eval_string("{guestAdr1}"));
	web_add_header("Accept","application/json");
	web_add_header("Content-Type","application/json");

	web_reg_find("TEXT/IC=jsonArr", "SaveCount=apiCheck", LAST);
	web_custom_request("getShipmentMethods",
		"URL=https://{api_host}/payment/getShipmentMethods",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction ("T09_Proceed_To_Checkout_getShipmentMethods", LR_FAIL) ;
	else
		lr_end_sub_transaction ("T09_Proceed_To_Checkout_getShipmentMethods", LR_AUTO) ;

	lr_start_sub_transaction ("T09_Proceed_To_Checkout_giftOptionsCmd", "T09_Proceed_To_Checkout Guest" ) ;
/*
	#if OPTIONSENABLED
		call_OPTIONS("payment/giftOptionsCmd");
	#endif
*/
	addHeader();
	web_reg_find("TEXT/IC=giftOptions", "SaveCount=apiCheck", LAST);
	web_url("giftOptionsCmd",
		"URL=https://{api_host}/payment/giftOptionsCmd",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction ("T09_Proceed_To_Checkout_giftOptionsCmd", LR_FAIL) ;
	else
		lr_end_sub_transaction ("T09_Proceed_To_Checkout_giftOptionsCmd", LR_AUTO) ;

	lr_save_string("T09_Proceed_To_Checkout Guest", "mainTransaction");
	web_add_header("action", "get");
	tcp_api2("tcporder/getFavouriteStoreLocation", "GET", lr_eval_string("{mainTransaction}") );

	if (ESPOT_FLAG == 1)
	{
		lr_start_sub_transaction ("T09_Proceed_To_Checkout_getESpots", "T09_Proceed_To_Checkout Guest" ) ;

		#if OPTIONSENABLED
			call_OPTIONS("getESpot");
		#endif

		web_add_header("espotName","GlobalHeaderBannerAboveHeader,GlobalHeaderBannerWithinHeader,GlobalHeaderLogo,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,create_account_drawer,MinicartLoginBanner,MinicartRegisterBanner,drawer_myaccount_overview_banner,coupon_help,bag_empty_banner,drawer_empty_bag,MinicartLoginFromFavoritesBanner,bag_ledger_promo_banner,gift_services_espot,CheckoutConfirmationBanner,CheckoutConfirmationMPRPromo,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1");
		web_add_header("deviceType","desktop");

	//	web_add_header("espotName","GlobalHeaderBannerAboveHeader,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1");
		addHeader();
		web_reg_find("TEXT/IC=espotName", "SaveCount=apiCheck", LAST);
		web_custom_request("getESpots",
			"URL=https://{api_host}/getESpot",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction("T09_Proceed_To_Checkout_getESpots", LR_FAIL);
		else
			lr_end_sub_transaction("T09_Proceed_To_Checkout_getESpots", LR_AUTO);

		api_getESpot_second();
	}

	lr_end_transaction ( "T09_Proceed_To_Checkout Guest" , LR_PASS) ;

	//lr_continue_on_error(0);

	Submit_Pickup_Detail();

	return 0;

} // end proceedAsGuest

int forgetPassword()
{
	lr_think_time ( FORM_TT ) ;
	web_cleanup_cookies ( ) ;
	web_cache_cleanup();

	//lr_continue_on_error(1);
	lr_start_transaction ( "T29_ForgotPassword_resetpassword" ) ;

	#if OPTIONSENABLED
		web_add_header("Accept", "*/*");
		web_add_header("Access-Control-Request-Headers", "Origin, Host, User-Agent, Accept, Accept-Encoding, Accept-Language, Referer, Request, Content-Type, Transfer-Encoding, Cache-Control, Connection, Content-Length, Content-Language, Date, Expires, Keep-Alive, Location, pragma, client_id, client_secret, grant_type, scope, Authorization, Set-Cookie, Cookie, storeId, nickName, catalogId, langId, orderId, callingPage, requestType, paymentInstructionId, customerId, act, com, a1, city, state, postal, cols, opt, fromPage, promoCode, addressField1, addressField2, zipCode, pageName, locStore, responseFormat, coreName, term, count, isRest, espotName, orderItemId, productId, oldOrderItemId, memberId, emailId, fromRest, companyId, storeLocId, quantity, catEntryId, firstname, lastname, phone, email, action, taskType, stlocId, posStores, isBOPIS, xAppConfigAttrNames, tcpOrderId, PaRes, MD, externalId, pageNumber, pageSize, categoryId, primary, addressId, identifier, corenamecategory, published, rows, searchterm, start, timeallowed, devicetype, savePayment, calc");
		web_add_header("Access-Control-Request-Method", "CONNECT, DELETE, GET, OPTIONS, PATCH, POST, PUT, TRACE");
		web_add_header("Origin", "https://{host}");

		web_custom_request("resetpassword",
			"URL=https://{api_host}/resetpassword",
		   "Method=OPTIONS",
		   "Resource=0",
		LAST);
	#endif

	registerErrorCodeCheck();
	addHeader();
	web_add_header("Content-Type", "application/json");
	lr_save_string( "BITROGUE@MAILINATOR.COM", "userEmail" );


	web_custom_request("resetpassword",
		"URL=https://{api_host}/resetpassword",
		"Method=PUT",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTML",
		"Body={\"storeId\":\"{storeId}\",\"catalogId\":\"{catalogId}\",\"langId\":\"-1\",\"isPasswordReset\":\"true\",\"logonId\":\"{userEmail}\",\"reLogonURL\":\"ChangePassword\",\"formFlag\":\"true\"}",
		LAST);

	//lr_continue_on_error(0);
	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 ) //if no error code and message
	{
		lr_end_transaction ( "T29_ForgotPassword_resetpassword" , LR_PASS) ;
		return LR_PASS;
	}
	else
	{
		lr_fail_trans_with_error( lr_eval_string("T29_ForgotPassword_resetpassword Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\" User: {userEmail}") ) ;
		lr_end_transaction ( "T29_ForgotPassword_resetpassword" , LR_FAIL) ;
		return LR_FAIL;
	}

}

int login() //Logon then proceed to checkout
{
	lr_think_time ( FORM_TT ) ;

	//lr_continue_on_error(1);

	lr_start_transaction ( "T10_Logon_ShippingView" ) ;

	lr_start_sub_transaction ( "T10_Logon_ShippingView1_logon", "T10_Logon_ShippingView" ) ;
	#if OPTIONSENABLED
		web_add_header("Accept", "*/*");
		web_add_header("Access-Control-Request-Headers", "catalogid,content-type,langid,storeid");
		web_add_header("Access-Control-Request-Method", "POST");
		web_add_header("Origin", "https://{host}");

		web_custom_request("logon",
		   "URL=https://{api_host}/logon",
		   "Method=OPTIONS",
		   "Resource=0",
		LAST);
	#endif

	web_reg_save_param ( "addressId" , "LB=\"addressId\": \"" , "RB=\",\n" , "ORD=1", "NotFound=Warning", LAST ) ; //"addressId": "1398789",\n
	web_reg_save_param ( "addressIdAll" , "LB=\"addressId\": \"" , "RB=\",\n" , "ORD=ALL", "NotFound=Warning", LAST ) ; //"addressId": "1398789",\n
	web_reg_save_param ( "responseCode" , "LB=\"responseCode\": \"" , "RB=\"" , "NotFound=Warning", LAST ) ; //"responseCode": "LoginSuccess"\n
	registerErrorCodeCheck();

	addHeader();
	web_custom_request("logon",
		"URL=https://{api_host}/logon",
		"Method=POST",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTML",
		"EncType=application/json",	"Body={\"storeId\":\"{storeId}\",\"logonId1\":\"{userEmail}\",\"logonPassword1\":\"Asdf!234\",\"rememberCheck\":false,\"rememberMe\":false,\"requesttype\":\"ajax\",\"reLogonURL\":\"TCPAjaxLogonErrorView\",\"URL\":\"TCPAjaxLogonSuccessView\",\"registryAccessPreference\":\"Public\",\"calculationUsageId\":-1,\"createIfEmpty\":1,\"deleteIfEmpty\":\"*\",\"fromOrderId\":\"*\",\"toOrderId\":\".\",\"updatePrices\":0}",
		LAST);
	lr_end_sub_transaction ( "T10_Logon_ShippingView1_logon", LR_AUTO ) ;

	lr_start_sub_transaction ( "T10_Logon_ShippingView2_shipping", "T10_Logon_ShippingView" ) ;
		web_url("checkout",
//		"URL=https://{host}/{country}/checkout/shipping",  //R3
		"URL=https://{host}/{country}/checkout",  //R5
		"TargetFrame=",
		"Resource=0",
		"Mode=HTML",
		LAST);
	lr_end_sub_transaction ( "T10_Logon_ShippingView2_shipping", LR_AUTO ) ;

	lr_start_sub_transaction ("T10_Logon_ShippingView_getRegisteredUserDetailsInfo","T10_Logon_ShippingView");
/*
	#if OPTIONSENABLED
		call_OPTIONS("payment/getRegisteredUserDetailsInfo");
	#endif
*/
	web_reg_save_param ( "addressId" , "LB=\"addressId\": \"" , "RB=\",\n" , "ORD=1", "NotFound=Warning", LAST ) ; //"addressId": "1398789",\n
	web_reg_save_param ( "addressIdAll" , "LB=\"addressId\": \"" , "RB=\",\n" , "ORD=ALL", "NotFound=Warning", LAST ) ; //"addressId": "1398789",\n
	web_reg_save_param ( "userId" , "LB=\"userId\": \"" , "RB=\",\n" , "NotFound=Warning", LAST ) ; // "userId": "202684824"
	addHeader();
	web_reg_find("TEXT/IC=resourceName", "SaveCount=apiCheck", LAST);
	web_url("getRegisteredUserDetailsInfo",
		"URL=https://{api_host}/payment/getRegisteredUserDetailsInfo",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction ("T10_Logon_ShippingView_getRegisteredUserDetailsInfo",LR_FAIL);
	else
		lr_end_sub_transaction ("T10_Logon_ShippingView_getRegisteredUserDetailsInfo",LR_AUTO);

//	if (atoi( lr_eval_string("{addressIdAll_count}")) ==0 )
//	{
//		lr_error_message(lr_eval_string("Used ID: {userEmail}, did not return an address id."));
//	}

	lr_start_sub_transaction ("T10_Logon_ShippingView_getAllCoupons","T10_Logon_ShippingView");
	addHeader();
	web_reg_find("TEXT/IC=appliedCoupons", "SaveCount=apiCheck", LAST);
	web_url("getAllCoupons",
		"URL=https://{api_host}/tcporder/getAllCoupons",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction ("T10_Logon_ShippingView_getAllCoupons",LR_FAIL);
	else
		lr_end_sub_transaction ("T10_Logon_ShippingView_getAllCoupons",LR_AUTO);

	if( strcmp(lr_eval_string("{isExpress}"), "true") == 0 ) {

		lr_start_sub_transaction ("T10_Logon_ShippingView_getAllCoupons_2","T10_Logon_ShippingView");
		addHeader();
		web_reg_find("TEXT/IC=appliedCoupons", "SaveCount=apiCheck", LAST);
		web_url("getAllCoupons",
			"URL=https://{api_host}/tcporder/getAllCoupons",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		lr_end_sub_transaction ("T10_Logon_ShippingView_getAllCoupons_2",LR_AUTO);
	}
	lr_start_sub_transaction ("T10_Logon_ShippingView_getOrderDetails","T10_Logon_ShippingView");
/*
	#if OPTIONSENABLED
		call_OPTIONS("getOrderDetails");
	#endif
*/
	addHeader();
	web_add_header("calc", "false");
	web_add_header("locStore", "True");
	web_add_header("pageName", "fullOrderInfo");
	web_reg_save_param("cartCount", "LB=artCount\": ", "RB=,", "NotFound=Warning", LAST);		//0429
	web_reg_save_param("piId", "LB=\"piId\": \"", "RB=\",", "NotFound=Warning", LAST); //"piId": "7.95",\n
	web_reg_find("TEXT/IC={", "SaveCount=apiCheck", LAST);
	web_url("getOrderDetails",
		"URL=https://{api_host}/getOrderDetails",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction ("T10_Logon_ShippingView_getOrderDetails",LR_FAIL);
	else
		lr_end_sub_transaction ("T10_Logon_ShippingView_getOrderDetails",LR_AUTO);

	if( strcmp(lr_eval_string("{isExpress}"), "true") == 0 ) {
		lr_start_sub_transaction ("T10_Logon_ShippingView_getOrderDetails_2","T10_Logon_ShippingView");
		web_add_header("calc", "false");
		web_add_header("locStore", "True");
		web_add_header("pageName", "fullOrderInfo");
		web_url("getOrderDetails",
			"URL=https://{api_host}/getOrderDetails",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		lr_end_sub_transaction ("T10_Logon_ShippingView_getOrderDetails_2",LR_AUTO);
	}

	lr_start_sub_transaction ("T10_Logon_ShippingView_getCreditCardDetails","T10_Logon_ShippingView");
/*
	#if OPTIONSENABLED
		call_OPTIONS("payment/getCreditCardDetails");
	#endif
*/
	web_reg_save_param("expMonth", "LB=expMonth\": \"", "RB=\"", "NotFound=Warning", LAST);
	web_reg_save_param("expYear", "LB=expYear\": \"", "RB=\"", "NotFound=Warning", LAST);

	addHeader();
	web_add_header("isRest", "true" );
	web_url("getCreditCardDetails",
		"URL=https://{api_host}/payment/getCreditCardDetails",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	lr_end_sub_transaction ("T10_Logon_ShippingView_getCreditCardDetails",LR_AUTO);

	lr_start_sub_transaction ("T10_Logon_ShippingView_giftOptionsCmd","T10_Logon_ShippingView");
/*
	#if OPTIONSENABLED
		call_OPTIONS("payment/giftOptionsCmd");
	#endif
*/
	addHeader();
	web_reg_find("TEXT/IC=giftOptions", "SaveCount=apiCheck", LAST);
	web_url("giftOptionsCmd",
		"URL=https://{api_host}/payment/giftOptionsCmd",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction ("T10_Logon_ShippingView_giftOptionsCmd",LR_FAIL);
	else
		lr_end_sub_transaction ("T10_Logon_ShippingView_giftOptionsCmd",LR_AUTO);

	lr_start_sub_transaction ("T10_Logon_ShippingView_getAddressFromBook","T10_Logon_ShippingView");
/*
	#if OPTIONSENABLED
		call_OPTIONS("payment/getAddressFromBook");
	#endif
*/
	web_reg_save_param("savedState", "LB=state\": \"", "RB=\"", "NotFound=Warning", LAST);
	web_reg_save_param("savedZipCode", "LB=zipCode\": \"", "RB=\"", "NotFound=Warning", LAST);
	web_reg_save_param_json( "ParamName=savedAddressLine", "QueryString=$.contact[0]...addressLine[0].", "SelectAll=NO", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	addHeader();
	web_url("getAddressFromBook",
		"URL=https://{api_host}/payment/getAddressFromBook",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	lr_end_sub_transaction ("T10_Logon_ShippingView_getAddressFromBook",LR_AUTO);

	lr_start_sub_transaction ("T10_Logon_ShippingView_getShipmentMethods","T10_Logon_ShippingView");
	addHeader();
	if (strlen(lr_eval_string("{savedZipCode}")) == 5 ) {
		web_add_header("state", lr_eval_string("{savedState}") );
		web_add_header("zipCode", lr_eval_string("{savedZipCode}") );
		web_add_header("addressField1", lr_eval_string("{savedAddressLine}") );
	} else {
		if (strcmp(lr_eval_string("{storeId}"), "10151") == 0) {
			web_add_header("state", "NJ");
			web_add_header("zipCode", "07094");
		} else {
			web_add_header("state", lr_eval_string("{guestState}"));
			web_add_header("zipCode", lr_eval_string("{guestZip}"));
		}
	}

	web_reg_find("TEXT/IC=jsonArr", "SaveCount=apiCheck", LAST);
	web_url("getShipmentMethods",
		"URL=https://{api_host}/payment/getShipmentMethods",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction ("T10_Logon_ShippingView_getShipmentMethods",LR_FAIL);
	else
		lr_end_sub_transaction ("T10_Logon_ShippingView_getShipmentMethods",LR_AUTO);

//	lr_start_sub_transaction ("T10_Logon_ShippingView_getCreditCardDetails","T10_Logon_ShippingView");


	if (ESPOT_FLAG == 1)
	{
		lr_start_sub_transaction ("T10_Logon_ShippingView_getESpots","T10_Logon_ShippingView");

		#if OPTIONSENABLED
			call_OPTIONS("getESpot");
		#endif

		web_add_header("espotName","GlobalHeaderBannerAboveHeader,GlobalHeaderBannerWithinHeader,GlobalHeaderLogo,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,create_account_drawer,MinicartLoginBanner,MinicartRegisterBanner,drawer_myaccount_overview_banner,coupon_help,bag_empty_banner,drawer_empty_bag,MinicartLoginFromFavoritesBanner,bag_ledger_promo_banner,gift_services_espot,CheckoutConfirmationBanner,CheckoutConfirmationMPRPromo,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1");
		web_add_header("deviceType","desktop");

	//	web_add_header("espotName","GlobalHeaderBannerAboveHeader,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1");
		addHeader();
		web_reg_find("TEXT/IC=espotName", "SaveCount=apiCheck", LAST);
		web_custom_request("getESpots",
			"URL=https://{api_host}/getESpot",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction("T10_Logon_ShippingView_getESpots", LR_FAIL);
		else
			lr_end_sub_transaction("T10_Logon_ShippingView_getESpots", LR_AUTO);

		api_getESpot_second();
	}

	//web_add_header("action", "get");  //Removed by PavanDusi 111617
	//tcp_api2("tcporder/getFavouriteStoreLocation", "GET",  "T10_Logon_ShippingView" );	//Removed by PavanDusi 111617

	//lr_continue_on_error(0);

	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 && strcmp ( lr_eval_string ( "{responseCode}" ), "LoginSuccess" ) == 0) //if no error code and message
	{
		isLoggedIn = 1;
		lr_end_transaction ( "T10_Logon_ShippingView" , LR_PASS) ;

		Submit_Pickup_Detail();

		return LR_PASS;
	}
	else
	{
		isLoggedIn = 0;
		lr_fail_trans_with_error( lr_eval_string("T10_Logon_ShippingView Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\" User: {userEmail}") ) ;
		lr_end_transaction ( "T10_Logon_ShippingView" , LR_FAIL) ;
		return LR_FAIL;
	}
}


int loginFromHomePage()  //loginFirst
{
	lr_think_time ( FORM_TT ) ;
 	//lr_continue_on_error(1);

	lr_start_transaction ( "T10_Logon" ) ;

	lr_start_sub_transaction ("T10_Logon logon","T10_Logon");
	#if OPTIONSENABLED
		web_add_header("Accept", "*/*");
		web_add_header("Access-Control-Request-Headers", "catalogid,content-type,langid,storeid");
		web_add_header("Access-Control-Request-Method", "POST");
		web_add_header("Origin", "https://{host}");

		web_custom_request("logon",
		   "URL=https://{api_host}/logon",
		   "Method=OPTIONS",
		   "Resource=0",
		LAST);
	#endif

	web_reg_save_param ( "responseCode" , "LB=\"responseCode\": \"" , "RB=\"" , "NotFound=Warning", LAST ) ; //"responseCode": "LoginSuccess"\n
	registerErrorCodeCheck();
	addHeader();
	web_custom_request("logon",
		"URL=https://{api_host}/logon",
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTML",
		"EncType=application/json",
//03212018		"Body={\"storeId\":\"{storeId}\",\"logonId1\":\"{userEmail}\",\"logonPassword1\":\"Asdf!234\",\"rememberCheck\":true,\"requesttype\":\"ajax\",\"reLogonURL\":\"TCPAjaxLogonErrorView\",\"URL\":\"TCPAjaxLogonSuccessView\",\"registryAccessPreference\":\"Public\",\"calculationUsageId\":-1,\"createIfEmpty\":1,\"deleteIfEmpty\":\"*\",\"fromOrderId\":\"*\",\"toOrderId\":\".\",\"updatePrices\":0}",
		"Body={\"storeId\":\"{storeId}\",\"logonId1\":\"{userEmail}\",\"logonPassword1\":\"Asdf!234\",\"rememberCheck\":true,\"requesttype\":\"ajax\",\"reLogonURL\":\"TCPAjaxLogonErrorView\",\"URL\":\"TCPAjaxLogonSuccessView\",\"registryAccessPreference\":\"Public\",\"calculationUsageId\":-1,\"createIfEmpty\":1,\"deleteIfEmpty\":\"*\",\"fromOrderId\":\"*\",\"toOrderId\":\".\",\"updatePrices\":0,\"xCreditCardId\":\"\"}",
		LAST);
	lr_end_sub_transaction ("T10_Logon logon",LR_AUTO);

	lr_start_sub_transaction ("T10_Logon_getRegisteredUserDetailsInfo","T10_Logon");
/*
	#if OPTIONSENABLED
		call_OPTIONS("payment/getRegisteredUserDetailsInfo");
	#endif
*/
	web_reg_save_param ( "addressId" , "LB=\"addressId\": \"" , "RB=\",\n" , "ORD=1", "NotFound=Warning", LAST ) ; //"addressId": "1398789",\n
	web_reg_save_param ( "addressIdAll" , "LB=\"addressId\": \"" , "RB=\",\n" , "ORD=ALL", "NotFound=Warning", LAST ) ; //"addressId": "1398789",\n
	web_reg_save_param ( "state" , "LB=\"state\": \"" , "RB=\"" , "NotFound=Warning", LAST ) ; //"state": "NJ"
	web_reg_save_param ( "userId" , "LB=\"userId\": \"" , "RB=\",\n" , "NotFound=Warning", LAST ) ; // "userId": "202684824"

	addHeader();
		web_custom_request("getRegisteredUserDetailsInfo",
			"URL=https://{api_host}/payment/getRegisteredUserDetailsInfo",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
	lr_end_sub_transaction ("T10_Logon_getRegisteredUserDetailsInfo",LR_AUTO);
/*
	if (atoi( lr_eval_string("{addressIdAll_count}")) ==0 )
	{
		lr_error_message(lr_eval_string("Used ID: {userEmail}, did not return an address id."));
	}
*/
	lr_start_sub_transaction ("T10_Logon_getOrderDetails","T10_Logon");
/*
	#if OPTIONSENABLED
		call_OPTIONS("getOrderDetails");
	#endif
*/
	//Pavan Dusi Fixed This 0626 to prevent 500 errors upon login DT-23031 .. Changed the locStore and pagename value params in headers
	addHeader();
	web_add_header("locStore", "False");
	web_add_header("pageName", "orderSummary");
	web_reg_save_param ( "addressIdLoginFirst" , "LB=<option id=\"" , "RB=\" value=" , "NotFound=Warning", LAST ) ;
	web_reg_find("TEXT/IC={", "SaveCount=apiCheck", LAST);
	web_custom_request("getOrderDetails",
		"URL=https://{api_host}/getOrderDetails",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction ("T10_Logon_getOrderDetails",LR_FAIL);
	else
		lr_end_sub_transaction ("T10_Logon_getOrderDetails",LR_AUTO);

	//web_add_header("action", "get");  //Removed by PavanDusi 111617
	//tcp_api2("tcporder/getFavouriteStoreLocation", "GET",  "T10_Logon" ); //Removed by PavanDusi 111617

	if (ESPOT_FLAG == 1)
	{
		lr_start_sub_transaction ("T10_Logon_getESpots","T10_Logon");

		#if OPTIONSENABLED
			call_OPTIONS("getESpot");
		#endif

		web_add_header("espotName","GlobalHeaderBannerAboveHeader,GlobalHeaderBannerWithinHeader,GlobalHeaderLogo,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,create_account_drawer,MinicartLoginBanner,MinicartRegisterBanner,drawer_myaccount_overview_banner,coupon_help,bag_empty_banner,drawer_empty_bag,MinicartLoginFromFavoritesBanner,bag_ledger_promo_banner,gift_services_espot,CheckoutConfirmationBanner,CheckoutConfirmationMPRPromo,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1");
		web_add_header("deviceType","desktop");

	//    web_add_header("espotName","GlobalHeaderBannerAboveHeader,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1");
		addHeader();
		web_reg_find("TEXT/IC=espotName", "SaveCount=apiCheck", LAST);
		web_custom_request("getESpots",
			"URL=https://{api_host}/getESpot",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction("T10_Logon_getESpots", LR_FAIL);
		else
			lr_end_sub_transaction("T10_Logon_getESpots", LR_AUTO);

		api_getESpot_second();
	}

 	//lr_continue_on_error(0);

	lr_start_sub_transaction ( "T10_Logon_getPointsAndOrderHistory", "T10_Logon" );
	//web_reg_save_param("s_OrderLookup","LB=\"orderNumber\": \"","RB=\"","NotFound=Warning","Ord=All",LAST);
	addHeader();
	web_add_header("fromRest", "true");
	web_reg_find("TEXT/IC=getOrderHistoryResponse", "SaveCount=apiCheck", LAST);
	web_custom_request("getPointsAndOrderHistory",
		"URL=https://{api_host}/tcporder/getPointsAndOrderHistory",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T10_Logon_getPointsAndOrderHistory", LR_FAIL);
	else
		lr_end_sub_transaction("T10_Logon_getPointsAndOrderHistory", LR_AUTO);

	lr_start_sub_transaction ( "T10_Logon_getAllCoupons", "T10_Logon" );

	addHeader();
	web_reg_find("TEXT/IC=appliedCoupons", "SaveCount=apiCheck", LAST);
	web_custom_request("getAllCoupons",
		"URL=https://{api_host}/tcporder/getAllCoupons",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T10_Logon_getAllCoupons", LR_FAIL);
	else
		lr_end_sub_transaction("T10_Logon_getAllCoupons", LR_AUTO);

		lr_save_string("T10_Logon", "mainTransaction");
		web_add_header("action", "get");
		web_add_header("Accept", "application/json");
		tcp_api2("tcporder/getFavouriteStoreLocation", "GET",  lr_eval_string("{mainTransaction}") );

	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0) // && strcmp ( lr_eval_string ( "{responseCode}" ), "LoginSuccess" ) == 0) //if no error code and message
	{
		isLoggedIn = 1;
		lr_end_transaction ( "T10_Logon" , LR_PASS) ;
		return LR_PASS;
	}
	else
	{
		isLoggedIn = 0;
		lr_fail_trans_with_error( lr_eval_string("T10_Logon Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
		lr_end_transaction ( "T10_Logon" , LR_FAIL) ;
		return LR_FAIL;
	}

}


void convertAndApplyPoints()
{
//	if (pointsBalance > 5000) and (converPoints random selector is positive)
//	{
	lr_think_time ( FORM_TT ) ;

	web_reg_save_param ( "myPlaceId" , "LB=<input type=\"hidden\" value=\"" , "RB=\" name=\"myPlaceId\" />", "Notfound=warning", LAST ) ;
	web_reg_save_param ( "myAccountId" , "LB=<input type=\"hidden\" value=\"" , "RB=\" name=\"accountId\" />", "Notfound=warning", LAST ) ;

	web_reg_save_param ( "addressIds" , "LB=<option id=\"" , "RB=\" value=" , "Ord=ALL" , "NotFound=Warning", LAST ) ;
	web_reg_save_param ( "TCPMyAddressBook" , "LB=edit shipping address\" id=\"TCPMyAddressBook_" , "RB=\"" , "Ord=ALL", "NotFound=Warning", LAST ) ;
	web_reg_save_param ( "emailAddress1" , "LB=<input type=\"hidden\" name=\"email1\" value=\"" , "RB=\" id=\"email1\" />" , "NotFound=Warning", LAST ) ;
	web_reg_save_param ( "authToken" , "LB=<input type=\"hidden\" name=\"authToken\" value=\"" , "RB=\"", "NotFound=Warning", LAST ) ;

	lr_start_transaction ( "T11_Convert Points To Coupon" ) ;

		lr_start_sub_transaction( "T11_Convert Points_S01_Click MyPlace Rewards", "T11_Convert Points To Coupon" );

		web_url("AjaxLogonForm",
			"URL=https://{host}/shop/AjaxLogonForm?catalogId={catalogId}&myAcctMain=1&langId=-1&storeId={storeId}",
			"Mode=HTTP",
			LAST);

		lr_end_sub_transaction( "T11_Convert Points_S01_Click MyPlace Rewards", LR_AUTO );

		web_reg_save_param ( "promocodeRewards" , "LB=\"javascript:applyLoyaltyCode('" , "RB=');", "NotFound=Warning", LAST ) ;

		lr_start_sub_transaction("T11_Convert Points_S02_TCPRedeemLoyaltyPoints", "T11_Convert Points To Coupon" );

		web_submit_data("TCPRedeemLoyaltyPoints",
				"Action=https://{host}/shop/TCPRedeemLoyaltyPoints",    //rhy 06/02/2016
				"Method=POST",
				"Mode=HTTP",
				ITEMDATA,
				"Name=storeId", "Value={storeId}", ENDITEM,
				"Name=langId", "Value=-1", ENDITEM,
				"Name=catalogId", "Value={catalogId}", ENDITEM,
				"Name=myPlaceId", "Value={myPlaceId}", ENDITEM,
				"Name=accountId", "Value={myAccountId}", ENDITEM,
				"Name=visitorId", "Value=[CS]v1|2B2AE4E305078968-600001048004DD05[CE]", ENDITEM,
				"Name=amountToRedeem", "Value=5", ENDITEM,
				"Name=cpnWalletValue", "Value=0", ENDITEM, //this was zero in the recording
				LAST);

		lr_end_transaction("T11_Convert Points_S02_TCPRedeemLoyaltyPoints", LR_AUTO);

	lr_end_transaction ( "T11_Convert Points To Coupon" , LR_PASS) ;

// 	promocodeRewards - Y022053295A08021
	if ( strlen(lr_eval_string("{promocodeRewards}")) == 16 ) {

		viewCart();

	    applyPromoCode(1);
	}

//	else if
//	{
//		//lr_message("No points convereted");
//		return;
//	}
} //end convertAndApplyPoints

int submitShippingBrowseFirst() //buildcart first
{
	lr_think_time ( FORM_TT ) ;
	//lr_continue_on_error(1);
	lr_start_transaction ( "T13_Submit_Shipping_Address Registered" );

	lr_save_string( lr_paramarr_idx("addressIdAll", atoi(lr_eval_string("{addressIdAll_count}")) ), "addressId" );

	lr_start_sub_transaction ( "T13_Submit_Shipping_Address_updateShippingMethodSelection", "T13_Submit_Shipping_Address Registered" ) ;

	addHeader();
	web_add_header( "Content-Type", "application/json" );
	if (strcmp(lr_eval_string("{storeId}"), "10151") != 0)
		lr_save_string("900103", "shipmode_id");
	else
		lr_save_string("901101", "shipmode_id");

	registerErrorCodeCheck();
	web_reg_find("TEXT/IC=updateShippingMethodSelectionResponse", "SaveCount=apiCheck1", LAST);
	web_custom_request("updateShippingMethodSelection",
		"URL=https://{api_host}/payment/updateShippingMethodSelection",
		"Method=PUT",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTML",
		"Body={\"shipModeId\":\"{shipmode_id}\",\"addressId\":\"{addressId}\",\"requesttype\":\"ajax\",\"x_calculationUsage\":\"-1,-2,-3,-4,-5,-6,-7\"}",
//		"Body={\"shipModeId\":\"{shipmode_id}\",\"addressId\":\"{addressIdAll_2}\",\"requesttype\":\"ajax\",\"prescreen\":false,\"x_calculationUsage\":\"-1,-2,-3,-4,-5,-6,-7\"}",  //R5
		LAST);
	lr_end_sub_transaction ( "T13_Submit_Shipping_Address_updateShippingMethodSelection" , LR_AUTO ) ;

	lr_start_sub_transaction ( "T13_Submit_Shipping_Address_getPointsService", "T13_Submit_Shipping_Address Registered" ) ;
/*
	#if OPTIONSENABLED
		call_OPTIONS("payment/getPointsService");
	#endif
*/
	addHeader();
	web_reg_find("TEXT/IC=LoyaltyWebsiteInd", "SaveCount=apiCheck", LAST);
	web_url("getPointsService",
		"URL=https://{api_host}/payment/getPointsService",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction ( "T13_Submit_Shipping_Address_getPointsService" , LR_FAIL ) ;
	else
		lr_end_sub_transaction ( "T13_Submit_Shipping_Address_getPointsService" , LR_AUTO ) ;

	lr_start_sub_transaction ( "T13_Submit_Shipping_Address_getOrderDetails", "T13_Submit_Shipping_Address Registered" ) ;
/*
	#if OPTIONSENABLED
		call_OPTIONS("getOrderDetails");
	#endif
*/
	addHeader();
	web_add_header("calc", "false");
	web_add_header("locStore", "True");
	web_add_header("pageName", "fullOrderInfo");
	web_reg_find("TEXT/IC={", "SaveCount=apiCheck", LAST);
	web_url("getOrderDetails",
		"URL=https://{api_host}/getOrderDetails",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction ( "T13_Submit_Shipping_Address_getOrderDetails" , LR_FAIL ) ;
	else
		lr_end_sub_transaction ( "T13_Submit_Shipping_Address_getOrderDetails" , LR_AUTO ) ;

	lr_start_sub_transaction ( "T13_Submit_Shipping_Address_addSignUpEmail", "T13_Submit_Shipping_Address Registered" ) ;

	#if OPTIONSENABLED
		web_add_header("Accept", "*/*");
		web_add_header("Access-Control-Request-Headers", "Origin, Host, User-Agent, Accept, Accept-Encoding, Accept-Language, Referer, Request, Content-Type, Transfer-Encoding, Cache-Control, Connection, Content-Length, Content-Language, Date, Expires, Keep-Alive, Location, pragma, client_id, client_secret, grant_type, scope, Authorization, Set-Cookie, Cookie, storeId, nickName, catalogId, langId, orderId, callingPage, requestType, paymentInstructionId, customerId, act, com, a1, city, state, postal, cols, opt, fromPage, promoCode, addressField1, addressField2, zipCode, pageName, locStore, responseFormat, coreName, term, count, isRest, espotName, orderItemId, productId, oldOrderItemId, memberId, emailId, fromRest, companyId, storeLocId, quantity, catEntryId, firstname, lastname, phone, email, action, taskType, stlocId, posStores, isBOPIS, xAppConfigAttrNames, tcpOrderId, PaRes, MD, externalId, pageNumber, pageSize, categoryId, primary, addressId, identifier, corenamecategory, published, rows, searchterm, start, timeallowed, devicetype, savePayment, calc");
		web_add_header("Access-Control-Request-Method", "CONNECT, DELETE, GET, OPTIONS, PATCH, POST, PUT, TRACE");
		web_add_header("Origin", "https://{host}");

		web_custom_request("addSignUpEmail",
			"URL=https://{api_host}/addSignUpEmail",
			"Method=OPTIONS",
			"Resource=0",
		LAST);
	#endif

	addHeader();
	web_custom_request("addSignUpEmail",
		"URL=https://{api_host}/addSignUpEmail",
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTTP",
		"EncType=application/json",
		"Body={\"storeId\":\"{storeId}\",\"catalogId\":\"{catalogId}\",\"langId\":\"-1\",\"emailaddr\":\"{userEmail}\",\"URL\":\"email-confirmation\",\"response\":\"accept_all::true:false\"}",
		LAST);
	lr_end_sub_transaction ( "T13_Submit_Shipping_Address_addSignUpEmail" , LR_AUTO ) ;

	//lr_continue_on_error(0);

//	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 ) //if no error code and message
	if (strlen(lr_eval_string("{apiCheck1}")) > 0 )
	{
		lr_end_transaction ( "T13_Submit_Shipping_Address Registered" , LR_PASS) ;
		return LR_PASS;
	}
	else {
		lr_fail_trans_with_error( lr_eval_string("T13_Submit_Shipping_Address Registered Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\" shipModeId:\"{shipmode_id}\", addressId:\"{addressId}\", User: {userEmail}") ) ;
		lr_end_transaction ( "T13_Submit_Shipping_Address Registered", LR_FAIL ) ;
		return LR_FAIL;
	}

}

int submitShippingRegisteredIsExpressPrep()
//int submitShippingRegistered()
{
	web_add_header( "nickName", lr_eval_string("{userEmail}") );
	web_add_header( "Accept", "application/json" );
	web_add_header( "Content-Type", "application/json" );
	web_reg_save_param ( "addressId" , "LB=\"addressId\": \"" , "RB=\"," , "NotFound=Warning", LAST ) ;
/*
	lr_start_transaction ( "T13_Submit_Shipping_UpdateAddress Registered" );

	web_custom_request("updateAddress",
		"URL=https://{api_host}/payment/updateAddress",
		"Method=PUT",
		"Resource=0",
		"Mode=HTTP",
		"Body={\"firstName\":\"Joe\",\"lastName\":\"User\",\"addressLine\":[\"500 Plaza Dr\",\"\",\"\"],\"attributes\":[{\"key\":\"addressField3\",\"value\":\"07094\"}],\"addressType\":\"ShippingAndBilling\",\"zipCode\":\"07094\",\"city\":\"Secaucus\",\"state\":\"NJ\",\"country\":\"US\",\"email1\":\"{userEmail}\",\"phone1\":\"2014531236\",\"xcont_addressField3\":\"07094\",\"phone1Publish\":false,\"primary\":\"true\",\"xcont_addressField2\":\"2\"}",
		LAST);
	lr_end_transaction ( "T13_Submit_Shipping_UpdateAddress Registered", LR_FAIL ) ;
*/
	lr_start_transaction ( "T13_Submit_Shipping_AddAddress Registered" );

		addHeader();
		web_custom_request("addAddress",
			"URL=https://{api_host}/payment/addAddress",
			"Method=POST",
			"Resource=0",
			"Mode=HTTP",
			"Body={\"contact\":[{\"addressLine\":[\"{guestAdr1}\",\"\",\"\"],\"attributes\":[{\"key\":\"addressField3\",\"value\":\"{guestZip}\"}],\"addressType\":\"ShippingAndBilling\",\"city\":\"{guestCity}\",\"country\":\"{guestCountry}\",\"firstName\":\"Joe\",\"lastName\":\"User\",\"nickName\":\"{nickName}\",\"phone1\":\"2014531513\",\"email1\":\"{userEmail}\",\"phone1Publish\":\"false\",\"primary\":\"true\",\"state\":\"{guestState}\",\"zipCode\":\"{guestZip}\",\"xcont_addressField3\":\"{guestZip}\",\"fromPage\":\"\"}]}",
			 LAST);
	lr_end_sub_transaction ( "T13_Submit_Shipping_AddAddress Registered", LR_AUTO ) ;

	addHeader();
	web_add_header("calc", "true");
	web_add_header("locStore", "True");
	web_add_header("pageName", "fullOrderInfo");
	web_reg_save_param("piAmount", "LB=\"piAmount\": \"", "RB=\",\n", "NotFound=Warning", LAST); //"piAmount": "7.95",\n
	web_reg_save_param("piId", "LB=\"piId\": \"", "RB=\",", "NotFound=Warning", LAST); //"piId": "7.95",\n
	web_reg_find("TEXT/IC={", "SaveCount=apiCheck", LAST);
	web_custom_request("getOrderDetails",
		"URL=https://{api_host}/getOrderDetails",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);

	if (strcmp(lr_eval_string("{storeId}"), "10151") != 0)
		lr_save_string("900103", "shipmode_id");
	else
		lr_save_string("901101", "shipmode_id");

	addHeader();
	web_add_header( "Content-Type", "application/json" );
	web_reg_find("TEXT/IC=orderid", "SaveCount=apiCheck1", LAST);
	registerErrorCodeCheck();
	web_custom_request("updateShippingMethodSelection",
		"URL=https://{api_host}/payment/updateShippingMethodSelection",
		"Method=PUT",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTML",
		"Body={\"shipModeId\":\"{shipmode_id}\",\"addressId\":\"{addressId}\",\"requesttype\":\"ajax\",\"x_calculationUsage\":\"-1,-2,-3,-4,-5,-6,-7\"}",  //R3
//		"Body={\"shipModeId\":\"{shipmode_id}\",\"addressId\":\"{addressIdAll_2}\",\"requesttype\":\"ajax\",\"prescreen\":false,\"x_calculationUsage\":\"-1,-2,-3,-4,-5,-6,-7\"}",  //R5
		LAST);

	addHeader();
	web_reg_find("TEXT/IC=LoyaltyWebsiteInd", "SaveCount=apiCheck", LAST);
	web_custom_request("getPointsService",
		"URL=https://{api_host}/payment/getPointsService",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);

	addHeader();
	web_reg_find("TEXT/IC=appliedCoupons", "SaveCount=apiCheck", LAST);
	web_custom_request("getAllCoupons",
		"URL=https://{api_host}/tcporder/getAllCoupons",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	return 0;
}

int submitShippingRegistered()  //login first
{
	lr_think_time ( FORM_TT ) ;
	//lr_continue_on_error(1);


	lr_start_transaction ( "T13_Submit_Shipping_Address Registered" );

	if (strcmp( lr_eval_string("{addBopisToCart}"), "true") == 0 && strcmp( lr_eval_string("{addEcommToCart}"), "true") == 0 ) //if mixed cart, do addAddress
	{
		lr_start_sub_transaction ( "T13_Submit_Shipping_Address_addAddress", "T13_Submit_Shipping_Address Registered" ) ;
		#if OPTIONSENABLED
			call_OPTIONS("payment/addAddress");
		#endif

		addHeader();
		web_add_header( "Content-Type", "application/json" );
		web_custom_request("addAddress",
			"URL=https://{api_host}/payment/addAddress",
			"Method=POST",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTTP",
			"EncType=application/json",
			"Body={\"contact\":[{\"addressLine\":[\"{guestAdr1}\",\"\",\"\"],\"attributes\":[{\"key\":\"addressField3\",\"value\":\"{guestZip}\"}],\"addressType\":\"ShippingAndBilling\",\"city\":\"{guestCity}\",\"country\":\"{guestCountry}\",\"firstName\":\"Joe\",\"lastName\":\"User\",\"nickName\":\"{nickName}\",\"phone1\":\"2014531513\",\"email1\":\"BITROGUE@MAILINATOR.COM\",\"phone1Publish\":\"false\",\"primary\":\"true\",\"state\":\"{guestState}\",\"zipCode\":\"{guestZip}\",\"xcont_addressField3\":\"{guestZip}\",\"fromPage\":\"\"}]}",
//			"Body={\"contact\":[{\"addressLine\":[\"{guestAdr1}\",\"\",\"\"],\"attributes\":[{\"key\":\"addressField3\",\"value\":\"{guestZip}\"}],\"addressType\":\"ShippingAndBilling\",\"city\":\"{guestCity}\",\"country\":\"{guestCountry}\",\"firstName\":\"Joe\",\"lastName\":\"User\",\"nickName\":\"{nickName}\",\"phone1\":\"2014531513\",\"email1\":\"BITROGUE@MAILINATOR.COM\",\"phone1Publish\":\"false\",\"primary\":\"false\",\"state\":\"{guestState}\",\"zipCode\":\"{guestZip}\",\"xcont_addressField3\":\"{guestZip}\",\"fromPage\":\"\"}]}",
			 LAST);
		lr_end_sub_transaction ( "T13_Submit_Shipping_Address_addAddress", LR_AUTO ) ;

	}

	lr_save_string( lr_paramarr_idx("addressIdAll", atoi(lr_eval_string("{addressIdAll_count}")) ), "addressId" );

	lr_start_sub_transaction ( "T13_Submit_Shipping_Address_updateShippingMethodSelection", "T13_Submit_Shipping_Address Registered" ) ;

	#if OPTIONSENABLED
		web_add_header("Accept", "*/*");
		web_add_header("Access-Control-Request-Headers", "Origin, Host, User-Agent, Accept, Accept-Encoding, Accept-Language, Referer, Request, Content-Type, Transfer-Encoding, Cache-Control, Connection, Content-Length, Content-Language, Date, Expires, Keep-Alive, Location, pragma, client_id, client_secret, grant_type, scope, Authorization, Set-Cookie, Cookie, storeId, nickName, catalogId, langId, orderId, callingPage, requestType, paymentInstructionId, customerId, act, com, a1, city, state, postal, cols, opt, fromPage, promoCode, addressField1, addressField2, zipCode, pageName, locStore, responseFormat, coreName, term, count, isRest, espotName, orderItemId, productId, oldOrderItemId, memberId, emailId, fromRest, companyId, storeLocId, quantity, catEntryId, firstname, lastname, phone, email, action, taskType, stlocId, posStores, isBOPIS, xAppConfigAttrNames, tcpOrderId, PaRes, MD, externalId, pageNumber, pageSize, categoryId, primary, addressId, identifier, corenamecategory, published, rows, searchterm, start, timeallowed, devicetype, savePayment, calc");
		web_add_header("Access-Control-Request-Method", "CONNECT, DELETE, GET, OPTIONS, PATCH, POST, PUT, TRACE");
		web_add_header("Origin", "https://{host}");

		web_custom_request("payment/updateShippingMethodSelection",
			"URL=https://{api_host}/payment/updateShippingMethodSelection",
			"Method=OPTIONS",
			"Resource=0",
		LAST);
	#endif

	if (strcmp(lr_eval_string("{storeId}"), "10151") != 0)
		lr_save_string("900103", "shipmode_id");
	else
		lr_save_string("901101", "shipmode_id");

	addHeader();
	web_add_header( "Content-Type", "application/json" );
	web_reg_find("TEXT/IC=orderid", "SaveCount=apiCheck1", LAST);
	registerErrorCodeCheck();
	web_custom_request("updateShippingMethodSelection",
		"URL=https://{api_host}/payment/updateShippingMethodSelection",
		"Method=PUT",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTML",
//03212018		"Body={\"shipModeId\":\"{shipmode_id}\",\"addressId\":\"{addressId}\",\"requesttype\":\"ajax\",\"x_calculationUsage\":\"-1,-2,-3,-4,-5,-6,-7\"}",  //R3
		"Body={\"shipModeId\":\"{shipmode_id}\",\"addressId\":\"{addressIdAll_2}\",\"requesttype\":\"ajax\",\"prescreen\":false,\"x_calculationUsage\":\"-1,-2,-3,-4,-5,-6,-7\"}",  //R5
		LAST);
	lr_end_sub_transaction ( "T13_Submit_Shipping_Address_updateShippingMethodSelection", LR_AUTO ) ;

	lr_start_sub_transaction ( "T13_Submit_Shipping_Address_getOrderDetails", "T13_Submit_Shipping_Address Registered" ) ;
/*
	#if OPTIONSENABLED
		call_OPTIONS("getOrderDetails");
	#endif
*/
	addHeader();
	web_add_header("locStore", "True");
	web_add_header("pageName", "excludeCartItems");
// insert an if statement for piAmountonUpdateQuantity	here
	web_reg_save_param("piAmount", "LB=\"piAmount\": \"", "RB=\"", "NotFound=Warning", LAST); //"piAmount": "28.85",\n
	web_reg_find("TEXT/IC={", "SaveCount=apiCheck", LAST);
	web_custom_request("getOrderDetails",
		"URL=https://{api_host}/getOrderDetails",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction ( "T13_Submit_Shipping_Address_getOrderDetails", LR_FAIL ) ;
	else
		lr_end_sub_transaction ( "T13_Submit_Shipping_Address_getOrderDetails", LR_AUTO ) ;

	lr_start_sub_transaction ( "T13_Submit_Shipping_Address_getPointsService", "T13_Submit_Shipping_Address Registered" );
/*
	#if OPTIONSENABLED
		call_OPTIONS("payment/getPointsService");
	#endif
*/
	addHeader();
	web_reg_find("TEXT/IC=LoyaltyWebsiteInd", "SaveCount=apiCheck", LAST);
	web_custom_request("getPointsService",
		"URL=https://{api_host}/payment/getPointsService",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T13_Submit_Shipping_Address_getPointsService", LR_FAIL);
	else
		lr_end_sub_transaction("T13_Submit_Shipping_Address_getPointsService", LR_AUTO);


	lr_start_sub_transaction ( "T13_Submit_Shipping_Address_addSignUpEmail", "T13_Submit_Shipping_Address Registered" );

	#if OPTIONSENABLED
		web_add_header("Accept", "*/*");
		web_add_header("Access-Control-Request-Headers", "Origin, Host, User-Agent, Accept, Accept-Encoding, Accept-Language, Referer, Request, Content-Type, Transfer-Encoding, Cache-Control, Connection, Content-Length, Content-Language, Date, Expires, Keep-Alive, Location, pragma, client_id, client_secret, grant_type, scope, Authorization, Set-Cookie, Cookie, storeId, nickName, catalogId, langId, orderId, callingPage, requestType, paymentInstructionId, customerId, act, com, a1, city, state, postal, cols, opt, fromPage, promoCode, addressField1, addressField2, zipCode, pageName, locStore, responseFormat, coreName, term, count, isRest, espotName, orderItemId, productId, oldOrderItemId, memberId, emailId, fromRest, companyId, storeLocId, quantity, catEntryId, firstname, lastname, phone, email, action, taskType, stlocId, posStores, isBOPIS, xAppConfigAttrNames, tcpOrderId, PaRes, MD, externalId, pageNumber, pageSize, categoryId, primary, addressId, identifier, corenamecategory, published, rows, searchterm, start, timeallowed, devicetype, savePayment, calc");
		web_add_header("Access-Control-Request-Method", "CONNECT, DELETE, GET, OPTIONS, PATCH, POST, PUT, TRACE");
		web_add_header("Origin", "https://{host}");

		web_custom_request("addSignUpEmail",
			"URL=https://{api_host}/addSignUpEmail",
			"Method=OPTIONS",
			"Resource=0",
		LAST);
	#endif

	addHeader();
	web_custom_request("addSignUpEmail",
		"URL=https://{api_host}/addSignUpEmail",
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTTP",
		"EncType=application/json",
		"Body={\"storeId\":\"{storeId}\",\"catalogId\":\"{catalogId}\",\"langId\":\"-1\",\"emailaddr\":\"BITROGUE@MAILINATOR.COM\",\"URL\":\"email-confirmation\",\"response\":\"accept_all::true:false\"}",
		LAST);
	lr_end_sub_transaction("T13_Submit_Shipping_Address_addSignUpEmail", LR_AUTO);

	//lr_continue_on_error(0);

//	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 ) //if no error code and message
	if ( strlen( lr_eval_string("{apiCheck1}") ) > 0 )
	{
		lr_end_transaction ( "T13_Submit_Shipping_Address Registered" , LR_PASS) ;
		return LR_PASS;
	}
	else {
		lr_fail_trans_with_error( lr_eval_string("T13_Submit_Shipping_Address  Registered with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
		lr_end_transaction ( "T13_Submit_Shipping_Address Registered", LR_FAIL ) ;
		return LR_FAIL;
	}
}


int submitShipping() //former submitShippingAddressAsGuest()
{
	lr_think_time ( FORM_TT ) ;
	//lr_continue_on_error(1);
	lr_start_transaction ( "T13_Submit_Shipping_Address Guest" ) ;

	lr_start_sub_transaction ( "T13_Submit_Shipping_Address_addAddress", "T13_Submit_Shipping_Address Guest" ) ;
/*	#if OPTIONSENABLED
		call_OPTIONS("payment/addAddress");
	#endif
*/
	web_reg_save_param("addressId", "LB=\"addressId\": \"", "RB=\",\n", "NotFound=Warning", LAST);
	web_reg_save_param ( "addressIdAll" , "LB=\"addressId\": \"" , "RB=\",\n" , "ORD=ALL", "NotFound=Warning", LAST ) ; //"addressId": "1398789",\n
	registerErrorCodeCheck();

	addHeader();
	web_add_header("Accept", "application/json");
	web_add_header("Content-Type", "application/json");
	web_custom_request("addAddress",
		"URL=https://{api_host}/payment/addAddress",
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTML",
		"EncType=application/json",
		"Body={\"contact\":[{\"addressLine\":[\"{guestAdr1}\",\"\",\"\"],\"attributes\":[{\"key\":\"addressField3\",\"value\":\"{guestZip}\"}],\"addressType\":\"ShippingAndBilling\",\"city\":\"{guestCity}\",\"country\":\"{guestCountry}\",\"firstName\":\"Joe\",\"lastName\":\"User\",\"nickName\":\"1521580882451\",\"phone1\":\"2014537894\",\"email1\":\"bitrogue@mailinator.com\",\"phone1Publish\":\"true\",\"primary\":\"false\",\"state\":\"{guestState}\",\"zipCode\":\"{guestZip}\",\"xcont_addressField2\":\"2\",\"xcont_addressField3\":\"{guestZip}\",\"fromPage\":\"\"}]}",
//		"Body={\"contact\":[{\"addressLine\":[\"{guestAdr1}\",\"\",\"\"],\"attributes\":[{\"key\":\"addressField3\",\"value\":\"{guestZip}\"}\"}],\"addressType\":\"ShippingAndBilling\",\"city\":\"{guestCity}\",\"country\":\"{guestCountry}\",\"firstName\":\"Joe\",\"lastName\":\"User\",\"nickName\":\"{randomNineDigits}{randomFourDigits}\",\"phone1\":\"201453{randomFourDigits}\",\"email1\":\"bitrogue@mailinator.com\",\"phone1Publish\":\"true\",\"primary\":\"false\",\"state\":\"{guestState}\",\"zipCode\":\"{guestZip}\",\"xcont_addressField2\":\"2\",\"xcont_addressField3\":\"{guestZip}\",\"fromPage\":\"\"}]}",
		LAST);
		//OLD 02202018		"Body={\"contact\":[{\"addressLine\":[\"{guestAdr1}\",\"\",\"\"],\"attributes\":[{\"key\":\"addressField3\",\"value\":\"{guestZip}\"}],\"addressType\":\"ShippingAndBilling\",\"city\":\"{guestCity}\",\"country\":\"
		//		{guestCountry}\",\"firstName\":\"Joe\",\"lastName\":\"User\",\"nickName\":\"{randomNineDigits}{randomFourDigits}\",\"phone1\":\"201453{randomFourDigits}\",\"email1\":\"bitrogue@mailinator.com\",\"
		//		phone1Publish\":\"true\",\"primary\":\"false\",\"state\":\"{guestState}\",\"zipCode\":\"{guestZip}\",\"xcont_addressField2\":\"2\",\"xcont_addressField3\":\""
		//		"{guestZip}\",\"fromPage\":\"\"}]}",

//{\"contact\":[{\"addressLine\":[\"500 Plaza Dr\",\"\",\"\"],\"attributes\":[{\"key\":\"addressField3\",\"value\":\"07094\"}],\"addressType\":\"ShippingAndBilling\",\"city\":\"Secaucus\",\"country\":\"US\",\"firstName\":\"Joe\",\"lastName\":\"User\",\"nickName\":\"1521580882451\",\"phone1\":\"2014537894\",\"email1\":\"josiersdf@gmail.com\",\"phone1Publish\":\"true\",\"primary\":\"false\",\"state\":\"NJ\",\"zipCode\":\"07094\",\"xcont_addressField2\":\"2\",\"xcont_addressField3\":\"07094\",\"fromPage\":\"\"}]}

	lr_end_sub_transaction ( "T13_Submit_Shipping_Address_addAddress", LR_AUTO ) ;

	lr_start_sub_transaction ( "T13_Submit_Shipping_Address_updateShippingMethodSelection", "T13_Submit_Shipping_Address Guest" ) ;
	#if OPTIONSENABLED
		web_add_header("Accept", "*/*");
		web_add_header("Access-Control-Request-Headers", "Origin, Host, User-Agent, Accept, Accept-Encoding, Accept-Language, Referer, Request, Content-Type, Transfer-Encoding, Cache-Control, Connection, Content-Length, Content-Language, Date, Expires, Keep-Alive, Location, pragma, client_id, client_secret, grant_type, scope, Authorization, Set-Cookie, Cookie, storeId, nickName, catalogId, langId, orderId, callingPage, requestType, paymentInstructionId, customerId, act, com, a1, city, state, postal, cols, opt, fromPage, promoCode, addressField1, addressField2, zipCode, pageName, locStore, responseFormat, coreName, term, count, isRest, espotName, orderItemId, productId, oldOrderItemId, memberId, emailId, fromRest, companyId, storeLocId, quantity, catEntryId, firstname, lastname, phone, email, action, taskType, stlocId, posStores, isBOPIS, xAppConfigAttrNames, tcpOrderId, PaRes, MD, externalId, pageNumber, pageSize, categoryId, primary, addressId, identifier, corenamecategory, published, rows, searchterm, start, timeallowed, devicetype, savePayment, calc");
		web_add_header("Access-Control-Request-Method", "CONNECT, DELETE, GET, OPTIONS, PATCH, POST, PUT, TRACE");
		web_add_header("Origin", "https://{host}");

		web_custom_request("payment/updateShippingMethodSelection",
			"URL=https://{api_host}/payment/updateShippingMethodSelection",
			"Method=OPTIONS",
			"Resource=0",
		LAST);
	#endif

	addHeader();
	web_add_header( "Content-Type", "application/json" );
	web_reg_find("TEXT/IC=orderid", "SaveCount=apiCheck1", LAST);
	web_custom_request("updateShippingMethodSelection",
		"URL=https://{api_host}/payment/updateShippingMethodSelection",
		"Method=PUT",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTML",
//		"Body={\"shipModeId\":\"{shipmode_id}\",\"addressId\":\"{addressId}\",\"requesttype\":\"ajax\",\"x_calculationUsage\":\"-1,-2,-3,-4,-5,-6,-7\"}",
		"Body={\"shipModeId\":\"{shipmode_id}\",\"addressId\":\"{addressId}\",\"requesttype\":\"ajax\",\"prescreen\":true,\"x_calculationUsage\":\"-1,-2,-3,-4,-5,-6,-7\"}",  //R5
		LAST);
	lr_end_sub_transaction ( "T13_Submit_Shipping_Address_updateShippingMethodSelection", LR_AUTO ) ;

	web_reg_save_param("piAmount", "LB=\"piAmount\": \"", "RB=\"", "NotFound=Warning", LAST); //"piAmount": "28.85",\n

	lr_start_sub_transaction ( "T13_Submit_Shipping_Address_getOrderDetails", "T13_Submit_Shipping_Address Guest" ) ;
/*
	#if OPTIONSENABLED
		call_OPTIONS("getOrderDetails");
	#endif
*/
	addHeader();
	web_add_header("locStore", "True");
	web_add_header("pageName", "excludeCartItems");
	web_reg_find("TEXT/IC={", "SaveCount=apiCheck", LAST);
	web_custom_request("getOrderDetails",
		"URL=https://{api_host}/getOrderDetails",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction ( "T13_Submit_Shipping_Address_getOrderDetails", LR_FAIL ) ;
	else
		lr_end_sub_transaction ( "T13_Submit_Shipping_Address_getOrderDetails", LR_AUTO ) ;
/*
	lr_start_sub_transaction ( "T13_Submit_Shipping_Address_getPointsService", "T13_Submit_Shipping_Address Guest" );

	#if OPTIONSENABLED
		call_OPTIONS("payment/getPointsService");
	#endif

	addHeader();
	web_reg_find("TEXT/IC=LoyaltyWebsiteInd", "SaveCount=apiCheck", LAST);
	web_custom_request("getPointsService",
		"URL=https://{api_host}/payment/getPointsService",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T13_Submit_Shipping_Address_getPointsService", LR_FAIL);
	else
		lr_end_sub_transaction("T13_Submit_Shipping_Address_getPointsService", LR_AUTO);
*/
	lr_start_sub_transaction ( "T13_Submit_Shipping_Address_addSignUpEmail", "T13_Submit_Shipping_Address Guest" );

	#if OPTIONSENABLED
		web_add_header("Accept", "*/*");
		web_add_header("Access-Control-Request-Headers", "Origin, Host, User-Agent, Accept, Accept-Encoding, Accept-Language, Referer, Request, Content-Type, Transfer-Encoding, Cache-Control, Connection, Content-Length, Content-Language, Date, Expires, Keep-Alive, Location, pragma, client_id, client_secret, grant_type, scope, Authorization, Set-Cookie, Cookie, storeId, nickName, catalogId, langId, orderId, callingPage, requestType, paymentInstructionId, customerId, act, com, a1, city, state, postal, cols, opt, fromPage, promoCode, addressField1, addressField2, zipCode, pageName, locStore, responseFormat, coreName, term, count, isRest, espotName, orderItemId, productId, oldOrderItemId, memberId, emailId, fromRest, companyId, storeLocId, quantity, catEntryId, firstname, lastname, phone, email, action, taskType, stlocId, posStores, isBOPIS, xAppConfigAttrNames, tcpOrderId, PaRes, MD, externalId, pageNumber, pageSize, categoryId, primary, addressId, identifier, corenamecategory, published, rows, searchterm, start, timeallowed, devicetype, savePayment, calc");
		web_add_header("Access-Control-Request-Method", "CONNECT, DELETE, GET, OPTIONS, PATCH, POST, PUT, TRACE");
		web_add_header("Origin", "https://{host}");

		web_custom_request("addSignUpEmail",
			"URL=https://{api_host}/addSignUpEmail",
			"Method=OPTIONS",
			"Resource=0",
		LAST);
	#endif

	addHeader();
	web_custom_request("addSignUpEmail",
		"URL=https://{api_host}/addSignUpEmail",
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTTP",
		"EncType=application/json",
		"Body={\"storeId\":\"{storeId}\",\"catalogId\":\"{catalogId}\",\"langId\":\"-1\",\"emailaddr\":\"BITROGUE@MAILINATOR.COM\",\"URL\":\"email-confirmation\",\"response\":\"valid::false:false\"}",
		LAST);
	lr_end_sub_transaction("T13_Submit_Shipping_Address_addSignUpEmail", LR_AUTO);

	lr_save_string("T13_Submit_Shipping_Address Guest", "mainTransaction");
	tcp_api2("tcporder/getAllCoupons", "GET",  lr_eval_string("{mainTransaction}") );

//	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 ) //if no error code and message
	web_reg_find("TEXT/IC=updateShippingMethodSelectionResponse", "SaveCount=apiCheck1", LAST);
	if ( strlen( lr_eval_string("{apiCheck1}") ) >  0 )
	{
		lr_end_transaction ( "T13_Submit_Shipping_Address Guest" , LR_PASS) ;
		return LR_PASS;
	}
	else {
//		lr_fail_trans_with_error( lr_eval_string("T13_Submit_Shipping_Address Guest Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\" ) ) ;
		lr_end_transaction ( "T13_Submit_Shipping_Address Guest", LR_FAIL ) ;
		return LR_FAIL;
	}

}

int BillingAsGuest() //former submitBillingAddressAsGuest()
{
	lr_think_time ( FORM_TT ) ;
	//lr_continue_on_error(1);
	lr_start_transaction ( "T15_Submit Billing Address Guest" ) ;

	lr_start_sub_transaction("T15_Submit_Billing_updateAddress", "T15_Submit Billing Address Guest");
	#if OPTIONSENABLED
		web_add_header("Accept", "*/*");
		web_add_header("Access-Control-Request-Headers", "Origin, Host, User-Agent, Accept, Accept-Encoding, Accept-Language, Referer, Request, Content-Type, Transfer-Encoding, Cache-Control, Connection, Content-Length, Content-Language, Date, Expires, Keep-Alive, Location, pragma, client_id, client_secret, grant_type, scope, Authorization, Set-Cookie, Cookie, storeId, nickName, catalogId, langId, orderId, callingPage, requestType, paymentInstructionId, customerId, act, com, a1, city, state, postal, cols, opt, fromPage, promoCode, addressField1, addressField2, zipCode, pageName, locStore, responseFormat, coreName, term, count, isRest, espotName, orderItemId, productId, oldOrderItemId, memberId, emailId, fromRest, companyId, storeLocId, quantity, catEntryId, firstname, lastname, phone, email, action, taskType, stlocId, posStores, isBOPIS, xAppConfigAttrNames, tcpOrderId, PaRes, MD, externalId, pageNumber, pageSize, categoryId, primary, addressId, identifier, corenamecategory, published, rows, searchterm, start, timeallowed, devicetype, savePayment, calc");
		web_add_header("Access-Control-Request-Method", "CONNECT, DELETE, GET, OPTIONS, PATCH, POST, PUT, TRACE");
		web_add_header("Origin", "https://{host}");

		web_custom_request("payment/updateAddress",
		   "URL=https://{api_host}/payment/updateAddress",
		   "Method=OPTIONS",
		   "Resource=0",
		LAST);
	#endif

	registerErrorCodeCheck();
	addHeader();
	web_add_header( "nickName", lr_eval_string("{nickName}") );
	//get piAmount, billing_address_id
	web_custom_request("updateAddress",
		"URL=https://{api_host}/payment/updateAddress",
		"Method=PUT",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTTP",
		"EncType=application/json",
		"Body={\"addressId\":\"{addressId}\",\"fromPage\":\"checkout\"}",
		LAST);
	lr_end_sub_transaction("T15_Submit_Billing_updateAddress", LR_AUTO);

	lr_start_sub_transaction("T15_Submit_Billing_addPaymentInstruction", "T15_Submit Billing Address Guest");

	#if OPTIONSENABLED
		web_add_header("Accept", "*/*");
		web_add_header("Access-Control-Request-Headers", "Origin, Host, User-Agent, Accept, Accept-Encoding, Accept-Language, Referer, Request, Content-Type, Transfer-Encoding, Cache-Control, Connection, Content-Length, Content-Language, Date, Expires, Keep-Alive, Location, pragma, client_id, client_secret, grant_type, scope, Authorization, Set-Cookie, Cookie, storeId, nickName, catalogId, langId, orderId, callingPage, requestType, paymentInstructionId, customerId, act, com, a1, city, state, postal, cols, opt, fromPage, promoCode, addressField1, addressField2, zipCode, pageName, locStore, responseFormat, coreName, term, count, isRest, espotName, orderItemId, productId, oldOrderItemId, memberId, emailId, fromRest, companyId, storeLocId, quantity, catEntryId, firstname, lastname, phone, email, action, taskType, stlocId, posStores, isBOPIS, xAppConfigAttrNames, tcpOrderId, PaRes, MD, externalId, pageNumber, pageSize, categoryId, primary, addressId, identifier, corenamecategory, published, rows, searchterm, start, timeallowed, devicetype, savePayment, calc");
		web_add_header("Access-Control-Request-Method", "CONNECT, DELETE, GET, OPTIONS, PATCH, POST, PUT, TRACE");
		web_add_header("Origin", "https://{host}");

		web_custom_request("payment/addPaymentInstruction",
			"URL=https://{api_host}/payment/addPaymentInstruction",
		   "Method=OPTIONS",
		   "Resource=0",
		LAST);
	#endif

	addHeader();
	web_add_header( "nickName", lr_eval_string("{nickName}") );
	web_add_header( "isRest", "true" );
	web_add_header( "savePayment", "false" );
	web_add_header( "identifier", "true" );
	//web_reg_save_param("orderId", "LB=\"orderId\": \"", "RB=\"", "NotFound=Warning", LAST);//Changed by Pavan Dusi on 1/30/2018
	web_reg_save_param("orderId", "LB=\"orderId\":\"", "RB=\"", "NotFound=Warning", LAST);//Changed by Pavan Dusi on 1/30/2018
	web_custom_request("addPaymentInstruction",
		"URL=https://{api_host}/payment/addPaymentInstruction",
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTTP",
		"EncType=application/json", "Body={\"paymentInstruction\":[{\"billing_address_id\":\"{addressId}\",\"piAmount\":\"{piAmount}\",\"expire_month\":\"12\",\"payMethodId\":\"COMPASSVISA\",\"cc_brand\":\"VISA\",\"expire_year\":\"2031\",\"account\":\"4111111111111111\",\"isDefault\":\"false\",\"cc_cvc\":\"111\"}]}", LAST);
//		"EncType=application/json", "Body={\"paymentInstruction\":[{\"billing_address_id\":\"{addressId}\",\"piAmount\":\"{piAmount}\",\"expire_month\":\"12\",\"payMethodId\":\"VISA\",\"cc_brand\":\"VISA\",\"expire_year\":\"2031\",\"account\":\"2818526303813893\",\"isDefault\":\"false\",\"cc_cvc\":\"111\"}]}", LAST);

	lr_end_sub_transaction("T15_Submit_Billing_addPaymentInstruction", LR_AUTO);
//		"EncType=application/json", "Body={\"paymentInstruction\":[{\"billing_address_id\":\"{addressId}\",\"piAmount\":\"{piAmount}\",\"expire_month\":\"12\",\"payMethodId\":\"VISA\",\"cc_brand\":\"VISA\",\"expire_year\":\"2031\",\"account\":\"4111111111111111\",\"isDefault\":\"false\",\"cc_cvc\":\"111\"}]}", LAST);

	lr_start_sub_transaction("T15_Submit_Billing_getOrderDetails", "T15_Submit Billing Address Guest");
/*
	#if OPTIONSENABLED
		call_OPTIONS("getOrderDetails");
	#endif
*/
	addHeader();
	web_add_header("calc", "true");	//02132018
//	web_add_header("calc", "false"); //02132018
	web_add_header("locStore", "True");
	web_add_header("pageName", "excludeCartItems");
	web_reg_save_param("piAmount", "LB=\"piAmount\": \"", "RB=\",\n", "NotFound=Warning", LAST); //"piAmount": "7.95",\n
	web_reg_find("TEXT/IC={", "SaveCount=apiCheck", LAST);
	web_custom_request("getOrderDetails",
		"URL=https://{api_host}/getOrderDetails",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T15_Submit_Billing_getOrderDetails", LR_FAIL);
	else
		lr_end_sub_transaction("T15_Submit_Billing_getOrderDetails", LR_AUTO);

	lr_start_sub_transaction("T15_Submit_Billing_getAllCoupons", "T15_Submit Billing Address Guest");
	addHeader();
	web_reg_find("TEXT/IC=appliedCoupons", "SaveCount=apiCheck", LAST);
	web_custom_request("getAllCoupons",
		"URL=https://{api_host}/tcporder/getAllCoupons",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T15_Submit_Billing_getAllCoupons", LR_FAIL);
	else
		lr_end_sub_transaction("T15_Submit_Billing_getAllCoupons", LR_AUTO);

	//lr_continue_on_error(0);

	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 ) //if no error code and message
	{
		lr_end_transaction ( "T15_Submit Billing Address Guest" , LR_PASS) ;
		return LR_PASS;
	}
	else {
		lr_fail_trans_with_error( lr_eval_string("T15_Submit Billing Address Guest Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
		lr_end_transaction ( "T15_Submit Billing Address Guest", LR_FAIL ) ;
		return LR_FAIL;
	}

}

//int submitBillingRegisteredIsExpressPrep()
int submitBillingRegistered()
{
	lr_think_time ( FORM_TT ) ;
	lr_start_transaction ( "T15_Submit Billing Address Registered" ) ;
	//lr_continue_on_error(1);

	lr_start_sub_transaction("T15_Submit_Billing_updateAddress", "T15_Submit Billing Address Registered");

	#if OPTIONSENABLED
		web_add_header("Accept", "*/*");
		web_add_header("Access-Control-Request-Headers", "Origin, Host, User-Agent, Accept, Accept-Encoding, Accept-Language, Referer, Request, Content-Type, Transfer-Encoding, Cache-Control, Connection, Content-Length, Content-Language, Date, Expires, Keep-Alive, Location, pragma, client_id, client_secret, grant_type, scope, Authorization, Set-Cookie, Cookie, storeId, nickName, catalogId, langId, orderId, callingPage, requestType, paymentInstructionId, customerId, act, com, a1, city, state, postal, cols, opt, fromPage, promoCode, addressField1, addressField2, zipCode, pageName, locStore, responseFormat, coreName, term, count, isRest, espotName, orderItemId, productId, oldOrderItemId, memberId, emailId, fromRest, companyId, storeLocId, quantity, catEntryId, firstname, lastname, phone, email, action, taskType, stlocId, posStores, isBOPIS, xAppConfigAttrNames, tcpOrderId, PaRes, MD, externalId, pageNumber, pageSize, categoryId, primary, addressId, identifier, corenamecategory, published, rows, searchterm, start, timeallowed, devicetype, savePayment, calc");
		web_add_header("Access-Control-Request-Method", "CONNECT, DELETE, GET, OPTIONS, PATCH, POST, PUT, TRACE");
		web_add_header("Origin", "https://{host}");

		web_custom_request("payment/updateAddress",
		   "URL=https://{api_host}/payment/updateAddress",
		   "Method=OPTIONS",
		   "Resource=0",
		LAST);
	#endif

	registerErrorCodeCheck();
	web_add_header( "nickName", lr_eval_string("{nickName}") );
	addHeader();

	web_custom_request("updateAddress",
		"URL=https://{api_host}/payment/updateAddress",
		"Method=PUT",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTTP",
		"EncType=application/json",
		"Body={\"addressId\":\"{addressId}\",\"fromPage\":\"checkout\"}",
		LAST);
	lr_end_sub_transaction("T15_Submit_Billing_updateAddress", LR_AUTO);

	//browsefirst path
	lr_start_sub_transaction ( "T15_Submit_Billing_addPaymentInstruction", "T15_Submit Billing Address Registered" ) ;

	addHeader();
//	web_reg_save_param("orderId", "LB=\"orderId\": \"", "RB=\"", "NotFound=Warning", LAST); //Changed by Pavan Dusi on 01/30/2018
	web_reg_save_param_regexp ("ParamName=orderId",	"RegExp=\"orderId\":[ ]*\"(.*?)\"",  "NotFound=Warning", SEARCH_FILTERS, "RequestUrl=*", LAST );
	web_add_header( "savePayment", "false" );

	web_custom_request("addPaymentInstruction",
		"URL=https://{api_host}/payment/updatePaymentInstruction",
		"Method=PUT",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTML",
		"EncType=application/json",
		"Body={\"prescreen\":true,\"paymentInstruction\":[{\"expire_month\":\"2\",\"piAmount\":\"{piAmount}\",\"payMethodId\":\"VISA\",\"cc_brand\":\"VISA\",\"expire_year\":\"2022\",\"account\":\"************1111\",\"piId\":\"{piId}\",\"cc_cvc\":\"111\",\"billing_address_id\":\"{addressId}\",\"isDefault\":\"true\"}]}",
		LAST);
	lr_end_sub_transaction ( "T15_Submit_Billing_addPaymentInstruction", LR_AUTO) ;

	lr_start_sub_transaction ( "T15_Submit_Billing_getPointsService", "T15_Submit Billing Address Registered" );

	addHeader();
	web_reg_find("TEXT/IC=LoyaltyWebsiteInd", "SaveCount=apiCheck", LAST);
	web_custom_request("getPointsService",
		"URL=https://{api_host}/payment/getPointsService",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T15_Submit_Billing_getPointsService", LR_FAIL);
	else
		lr_end_sub_transaction("T15_Submit_Billing_getPointsService", LR_AUTO);

	lr_start_sub_transaction ( "T15_Submit_Billing_getAllCoupons", "T15_Submit Billing Address Registered" );

	addHeader();
	web_reg_find("TEXT/IC=appliedCoupons", "SaveCount=apiCheck", LAST);
	web_custom_request("getAllCoupons",
		"URL=https://{api_host}/tcporder/getAllCoupons",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T15_Submit_Billing_getAllCoupons", LR_FAIL);
	else
		lr_end_sub_transaction("T15_Submit_Billing_getAllCoupons", LR_AUTO);

	lr_start_sub_transaction("T15_Submit_Billing_getOrderDetails", "T15_Submit Billing Address Registered");

	#if OPTIONSENABLED
		call_OPTIONS("getOrderDetails");
	#endif

	addHeader();
	web_add_header("calc", "true");
	web_add_header("locStore", "True");
	web_add_header("pageName", "excludeCartItems");
	web_reg_save_param("piAmount", "LB=\"piAmount\": \"", "RB=\",\n", "NotFound=Warning", LAST); //"piAmount": "7.95",\n
	web_reg_find("TEXT/IC={", "SaveCount=apiCheck", LAST);
	web_custom_request("getOrderDetails",
		"URL=https://{api_host}/getOrderDetails",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T15_Submit_Billing_getOrderDetails", LR_FAIL);
	else
		lr_end_sub_transaction("T15_Submit_Billing_getOrderDetails", LR_AUTO);

	//lr_continue_on_error(0);
	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 ) //if no error code and message
	{
		lr_end_transaction ("T15_Submit Billing Address Registered" , LR_PASS ) ;
		return LR_PASS;
	}
	else
	{
		lr_fail_trans_with_error(  lr_eval_string ("T15_Submit Billing Address Registered Failed with Error Code:  \"{errorCode}\", Error Message: {errorMessage}") ) ;
		lr_end_transaction ( lr_eval_string ( "T15_Submit Billing Address Registered" ) , LR_FAIL ) ;
		return LR_FAIL;
	}
}



int submitBillingRegisteredOriginal()
//int submitBillingRegistered()
{
	lr_think_time ( FORM_TT ) ;
	lr_start_transaction ( "T15_Submit Billing Address Registered" ) ;
	//lr_continue_on_error(1);

	lr_start_sub_transaction("T15_Submit_Billing_updateAddress", "T15_Submit Billing Address Registered");

	#if OPTIONSENABLED
		web_add_header("Accept", "*/*");
		web_add_header("Access-Control-Request-Headers", "Origin, Host, User-Agent, Accept, Accept-Encoding, Accept-Language, Referer, Request, Content-Type, Transfer-Encoding, Cache-Control, Connection, Content-Length, Content-Language, Date, Expires, Keep-Alive, Location, pragma, client_id, client_secret, grant_type, scope, Authorization, Set-Cookie, Cookie, storeId, nickName, catalogId, langId, orderId, callingPage, requestType, paymentInstructionId, customerId, act, com, a1, city, state, postal, cols, opt, fromPage, promoCode, addressField1, addressField2, zipCode, pageName, locStore, responseFormat, coreName, term, count, isRest, espotName, orderItemId, productId, oldOrderItemId, memberId, emailId, fromRest, companyId, storeLocId, quantity, catEntryId, firstname, lastname, phone, email, action, taskType, stlocId, posStores, isBOPIS, xAppConfigAttrNames, tcpOrderId, PaRes, MD, externalId, pageNumber, pageSize, categoryId, primary, addressId, identifier, corenamecategory, published, rows, searchterm, start, timeallowed, devicetype, savePayment, calc");
		web_add_header("Access-Control-Request-Method", "CONNECT, DELETE, GET, OPTIONS, PATCH, POST, PUT, TRACE");
		web_add_header("Origin", "https://{host}");

		web_custom_request("payment/updateAddress",
		   "URL=https://{api_host}/payment/updateAddress",
		   "Method=OPTIONS",
		   "Resource=0",
		LAST);
	#endif

	registerErrorCodeCheck();
	web_add_header( "nickName", lr_eval_string("{nickName}") );
	addHeader();

	web_custom_request("updateAddress",
		"URL=https://{api_host}/payment/updateAddress",
		"Method=PUT",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTTP",
		"EncType=application/json",
		"Body={\"addressId\":\"{addressIdAll_2}\",\"fromPage\":\"checkout\"}",
		LAST);
	lr_end_sub_transaction("T15_Submit_Billing_updateAddress", LR_AUTO);

	//browsefirst path
	lr_start_sub_transaction ( "T15_Submit_Billing_addPaymentInstruction", "T15_Submit Billing Address Registered" ) ;

	addHeader();
//	web_reg_save_param("orderId", "LB=\"orderId\":\"", "RB=\"", "NotFound=Warning", LAST); //0418
//	web_reg_save_param("orderId", "LB=\"orderId\": \"", "RB=\"", "NotFound=Warning", LAST); //0718 release  "orderId": "650651124",\n //Changed by Pavan Dusi on 01/30/2018
	web_reg_save_param("orderId", "LB=\"orderId\": \"", "RB=\"", "NotFound=Warning", LAST); //Changed by Pavan Dusi on 01/30/2018
	web_add_header( "isRest", "true" );
	web_add_header( "identifier", "true" );
	web_add_header( "savePayment", "true" );
	web_add_header( "nickName", lr_eval_string("Billing_{storeId}_{randomNineDigits}{randomFourDigits}") ); //nickName:Billing_10151_1502895072662

	web_custom_request("addPaymentInstruction",
		"URL=https://{api_host}/payment/addPaymentInstruction",
		"Method=POST",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTML",
		"EncType=application/json",
		"Body={\"paymentInstruction\":[{\"billing_address_id\":\"{addressIdAll_2}\",\"piAmount\":\"{piAmount}\",\"payMethodId\":\"VISA\",\"cc_brand\":\"VISA\",\"account\":\"4111111111111111\",\"expire_month\":\"4\",\"expire_year\":\"2021\",\"isDefault\":\"true\",\"cc_cvc\":\"111\"}]}",
//ORIGINAL	03122018	"Body={\"paymentInstruction\":[{\"billing_address_id\":\"{addressIdAll_2}\",\"piAmount\":\"{piAmount}\",\"payMethodId\":\"VISA\",\"cc_brand\":\"VISA\",\"account\":\"4111111111111111\",\"expire_month\":\"2\",\"expire_year\":\"2022\",\"cc_cvc\":\"111\"}]}",
		LAST);
	lr_end_sub_transaction ( "T15_Submit_Billing_addPaymentInstruction", LR_AUTO) ;
///		"EncType=application/json", 	"Body={\"paymentInstruction\":[{\"billing_address_id\":\"{addressId}\",\"piAmount\":\"{piAmount}\",\"payMethodId\":\"VISA\",\"cc_brand\":\"VISA\",\"account\":\"4111111111111111\",\"expire_month\":\"2\",\"expire_year\":\"2022\",\"cc_cvc\":\"111\"}]}",

	lr_start_sub_transaction ( "T15_Submit_Billing_getPointsService", "T15_Submit Billing Address Registered" );

	addHeader();
	web_reg_find("TEXT/IC=LoyaltyWebsiteInd", "SaveCount=apiCheck", LAST);
	web_custom_request("getPointsService",
		"URL=https://{api_host}/payment/getPointsService",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T15_Submit_Billing_getPointsService", LR_FAIL);
	else
		lr_end_sub_transaction("T15_Submit_Billing_getPointsService", LR_AUTO);

	lr_start_sub_transaction ( "T15_Submit_Billing_getAllCoupons", "T15_Submit Billing Address Registered" );

	addHeader();
	web_reg_find("TEXT/IC=appliedCoupons", "SaveCount=apiCheck", LAST);
	web_custom_request("getAllCoupons",
		"URL=https://{api_host}/tcporder/getAllCoupons",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T15_Submit_Billing_getAllCoupons", LR_FAIL);
	else
		lr_end_sub_transaction("T15_Submit_Billing_getAllCoupons", LR_AUTO);

	lr_start_sub_transaction("T15_Submit_Billing_getOrderDetails", "T15_Submit Billing Address Registered");

	#if OPTIONSENABLED
		call_OPTIONS("getOrderDetails");
	#endif

	addHeader();
	web_add_header("calc", "true");
	web_add_header("locStore", "True");
	web_add_header("pageName", "fullOrderInfo");
	web_reg_save_param("piAmount", "LB=\"piAmount\": \"", "RB=\",\n", "NotFound=Warning", LAST); //"piAmount": "7.95",\n
	web_reg_find("TEXT/IC={", "SaveCount=apiCheck", LAST);
	web_custom_request("getOrderDetails",
		"URL=https://{api_host}/getOrderDetails",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T15_Submit_Billing_getOrderDetails", LR_FAIL);
	else
		lr_end_sub_transaction("T15_Submit_Billing_getOrderDetails", LR_AUTO);

	//lr_continue_on_error(0);
	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 ) //if no error code and message
	{
		lr_end_transaction ("T15_Submit Billing Address Registered" , LR_PASS ) ;
		return LR_PASS;
	}
	else
	{
		lr_fail_trans_with_error(  lr_eval_string ("T15_Submit Billing Address Registered Failed with Error Code:  \"{errorCode}\", Error Message: {errorMessage}") ) ;
		lr_end_transaction ( lr_eval_string ( "T15_Submit Billing Address Registered" ) , LR_FAIL ) ;
		return LR_FAIL;
	}
}

int submitOrderAsGuest()
{
	int rc;
	lr_think_time ( FORM_TT ) ;
//	lr_set_debug_message(16|8|4|2,1);
	//lr_continue_on_error(1);

	lr_start_transaction ( "T16_Submit_Order" ) ;

	if (strcmp( lr_eval_string("{largeCart}"), "true") == 0 )
		lr_save_string("T16_Submit_Order_Large_Cart", "T16_Submit_Order_Sub");
	else if (strcmp( lr_eval_string("{addBopisToCart}"), "true") == 0 )
		lr_save_string("T16_Submit_Order_Bopis", "T16_Submit_Order_Sub");
	else
		lr_save_string("T16_Submit_Order_Ecom", "T16_Submit_Order_Sub");

	lr_start_sub_transaction ( lr_eval_string("{T16_Submit_Order_Sub}_S01_addCheckout"), "T16_Submit_Order" ) ;

	#if OPTIONSENABLED
		web_add_header("Accept", "*/*");
		web_add_header("Access-Control-Request-Headers", "Origin, Host, User-Agent, Accept, Accept-Encoding, Accept-Language, Referer, Request, Content-Type, Transfer-Encoding, Cache-Control, Connection, Content-Length, Content-Language, Date, Expires, Keep-Alive, Location, pragma, client_id, client_secret, grant_type, scope, Authorization, Set-Cookie, Cookie, storeId, nickName, catalogId, langId, orderId, callingPage, requestType, paymentInstructionId, customerId, act, com, a1, city, state, postal, cols, opt, fromPage, promoCode, addressField1, addressField2, zipCode, pageName, locStore, responseFormat, coreName, term, count, isRest, espotName, orderItemId, productId, oldOrderItemId, memberId, emailId, fromRest, companyId, storeLocId, quantity, catEntryId, firstname, lastname, phone, email, action, taskType, stlocId, posStores, isBOPIS, xAppConfigAttrNames, tcpOrderId, PaRes, MD, externalId, pageNumber, pageSize, categoryId, primary, addressId, identifier, corenamecategory, published, rows, searchterm, start, timeallowed, devicetype, savePayment, calc");
		web_add_header("Access-Control-Request-Method", "CONNECT, DELETE, GET, OPTIONS, PATCH, POST, PUT, TRACE");
		web_add_header("Origin", "https://{host}");

		web_custom_request("addCheckout",
			"URL=https://{api_host}/addCheckout",
			"Method=OPTIONS",
			"Resource=0",
		LAST);
	#endif

	registerErrorCodeCheck();
	addHeader();
	web_add_header( "Content-Type", "application/json" );

	web_custom_request("addCheckout",
		"URL=https://{api_host}/addCheckout",
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTTP",
		"EncType=application/json",
		"Body={\"orderId\":\"{orderId}\",\"isRest\":\"true\",\"locStore\":\"True\"}",
		LAST);

	lr_end_sub_transaction ( lr_eval_string("{T16_Submit_Order_Sub}_S01_addCheckout"), LR_AUTO ) ;

	rc = web_get_int_property(HTTP_INFO_RETURN_CODE);
	lr_save_int(rc, "httpReturnCode");

	lr_save_string("T16_Submit_Order","mainTransaction");
	tcp_api2("tcpproduct/getPriceDetails", "GET",  lr_eval_string("{mainTransaction}") );

	//lr_continue_on_error(0);
//	lr_set_debug_message(16|8|4|2,0);

	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 ) //if no error code and message
	{
		lr_end_transaction ("T16_Submit_Order", LR_AUTO ) ;
		return LR_PASS;
	}
	else {
		lr_fail_trans_with_error( lr_eval_string ("{T16_Submit_Order} Guest Failed with http Error Code: {httpReturnCode}, api Error Code:  \"{errorCode}\", Error Message: {errorMessage}, OrderId:{orderId}") ) ;
		lr_end_transaction ("T16_Submit_Order", LR_FAIL ) ;

		lr_start_transaction ( "T16_Submit_Order" ) ;

		if (strcmp( lr_eval_string("{largeCart}"), "true") == 0 )
			lr_save_string("T16_Submit_Order_Large_Cart", "T16_Submit_Order_Sub");
		else if (strcmp( lr_eval_string("{addBopisToCart}"), "true") == 0 )
			lr_save_string("T16_Submit_Order_Bopis", "T16_Submit_Order_Sub");
		else
			lr_save_string("T16_Submit_Order_Ecom", "T16_Submit_Order_Sub");

		lr_start_sub_transaction ( lr_eval_string("{T16_Submit_Order_Sub}_S01_addCheckout"), "T16_Submit_Order" ) ;

		#if OPTIONSENABLED
			web_add_header("Accept", "*/*");
			web_add_header("Access-Control-Request-Headers", "Origin, Host, User-Agent, Accept, Accept-Encoding, Accept-Language, Referer, Request, Content-Type, Transfer-Encoding, Cache-Control, Connection, Content-Length, Content-Language, Date, Expires, Keep-Alive, Location, pragma, client_id, client_secret, grant_type, scope, Authorization, Set-Cookie, Cookie, storeId, nickName, catalogId, langId, orderId, callingPage, requestType, paymentInstructionId, customerId, act, com, a1, city, state, postal, cols, opt, fromPage, promoCode, addressField1, addressField2, zipCode, pageName, locStore, responseFormat, coreName, term, count, isRest, espotName, orderItemId, productId, oldOrderItemId, memberId, emailId, fromRest, companyId, storeLocId, quantity, catEntryId, firstname, lastname, phone, email, action, taskType, stlocId, posStores, isBOPIS, xAppConfigAttrNames, tcpOrderId, PaRes, MD, externalId, pageNumber, pageSize, categoryId, primary, addressId, identifier, corenamecategory, published, rows, searchterm, start, timeallowed, devicetype, savePayment, calc");
			web_add_header("Access-Control-Request-Method", "CONNECT, DELETE, GET, OPTIONS, PATCH, POST, PUT, TRACE");
			web_add_header("Origin", "https://{host}");

			web_custom_request("payment/addCheckout",
				"URL=https://{api_host}/addCheckout",
				"Method=OPTIONS",
				"Resource=0",
			LAST);
		#endif

		registerErrorCodeCheck();
		addHeader();
		web_add_header( "Content-Type", "application/json" );

		web_custom_request("addCheckout",
			"URL=https://{api_host}/addCheckout",
			"Method=POST",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTTP",
			"EncType=application/json",
			"Body={\"orderId\":\"{orderId}\",\"isRest\":\"true\",\"locStore\":\"True\"}",
			LAST);
		lr_end_sub_transaction( lr_eval_string("{T16_Submit_Order_Sub}_S01_addCheckout"),  LR_AUTO );

		rc = web_get_int_property(HTTP_INFO_RETURN_CODE);
		lr_save_int(rc, "httpReturnCode");

		lr_save_string("T16_Submit_Order","mainTransaction");
		tcp_api2("tcpproduct/getPriceDetails", "GET",  lr_eval_string("{mainTransaction}") );

		if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 ) //if no error code and message
		{
			lr_end_transaction ("T16_Submit_Order", LR_AUTO ) ;
			return LR_PASS;
		}else{
			lr_end_transaction ("T16_Submit_Order", LR_FAIL ) ;
			return LR_FAIL;
		}
	}
}

int truncSpace()
{
    extern char * strtok(char * string, const char * delimiters );
    char path[1000] = "";
    char separators[] = " ";
    char * token;
    strcpy(path, lr_eval_string("{expMonth}"));
    token = (char *)strtok(path, separators);
    if (!token) {
        lr_output_message ("No tokens found in string!");
    }
    else {
    	lr_save_string(token, lr_eval_string("expMonth") );
    }

	return 0;
}
int submitOrderRegistered()
{
	int rc;
	lr_think_time ( FORM_TT ) ;
//	lr_set_debug_message(16|8|4|2,1);
	//lr_continue_on_error(1);

	lr_start_transaction ( "T16_Submit_Order" ) ;

	if (strcmp( lr_eval_string("{largeCart}"), "true") == 0 )
		lr_save_string("T16_Submit_Order_Large_Cart", "T16_Submit_Order_Sub");
	else if (strcmp( lr_eval_string("{addBopisToCart}"), "true") == 0 )
		lr_save_string("T16_Submit_Order_Bopis", "T16_Submit_Order_Sub");
	else
		lr_save_string("T16_Submit_Order_Ecom", "T16_Submit_Order_Sub");

	if( strcmp(lr_eval_string("{isExpress}"), "true") == 0 ) {

		truncSpace(); //truncate space from expMonth
		lr_start_sub_transaction ( lr_eval_string("{T16_Submit_Order_Sub}_updatePaymentInstruction"), "T16_Submit_Order" ) ;
		addHeader();
		web_add_header( "savePayment", "false" );
		web_add_header( "Accept", "application/json" );
		web_add_header( "Content-Type", "application/json" );
		web_custom_request("addPaymentInstruction",
			"URL=https://{api_host}/payment/updatePaymentInstruction",
			"Method=PUT",
			"TargetFrame=",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTML",
			"EncType=application/json",

	  //            {\"prescreen\":true,\"paymentInstruction\":[{\"expire_month\":\"12\",\"piAmount\":\"34.53\",\"payMethodId\":\"COMPASSVISA\",\"cc_brand\":\"VISA\",\"expire_year\":\"2028\",\"account\":\"************1111\",\"piId\":\"87767626\",\"cc_cvc\":\"111\",\"isDefault\":\"false\"}]}
      //            {\"prescreen\":true,\"paymentInstruction\":[{\"expire_month\":\"12\",\"piAmount\":\"34.53\",\"payMethodId\":\"VISA\",\"cc_brand\":\"VISA\",\"expire_year\":\"2028\",\"account\":\"************1111\",\"piId\":\"87765417\",\"cc_cvc\":\"111\",\"isDefault\":\"false\"}]}
			"Body={\"prescreen\":true,\"paymentInstruction\":[{\"expire_month\":\"{expMonth}\",\"piAmount\":\"{piAmount}\",\"payMethodId\":\"COMPASSVISA\",\"cc_brand\":\"VISA\",\"expire_year\":\"{expYear}\",\"account\":\"************1111\",\"piId\":\"{piId}\",\"cc_cvc\":\"111\",\"isDefault\":\"false\"}]}",
			LAST);
		lr_end_sub_transaction(lr_eval_string("{T16_Submit_Order_Sub}_updatePaymentInstruction"),  LR_AUTO );
	}

	lr_start_sub_transaction ( lr_eval_string("{T16_Submit_Order_Sub}_S01_addCheckout"), "T16_Submit_Order" ) ;

	registerErrorCodeCheck();
	addHeader();
	web_add_header( "Accept", "application/json" );
	web_add_header( "Content-Type", "application/json" );
	web_reg_find("TEXT/IC={", "SaveCount=apiCheck", LAST);
	web_custom_request("addCheckout",
		"URL=https://{api_host}/addCheckout",
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTTP",
		"EncType=application/json",
		"Body={\"orderId\":\"{orderId}\",\"isRest\":\"true\",\"locStore\":\"True\"}",
		LAST);

	lr_end_sub_transaction(lr_eval_string("{T16_Submit_Order_Sub}_S01_addCheckout"),  LR_AUTO );

//	rc = web_get_int_property(HTTP_INFO_RETURN_CODE);
//	lr_save_int(rc, "httpReturnCode");

	lr_save_string("T16_Submit_Order","mainTransaction");
	tcp_api2("tcpproduct/getPriceDetails", "GET",  lr_eval_string("{mainTransaction}") );
	//lr_continue_on_error(0);
//	lr_set_debug_message(16|8|4|2,0);

	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 ) //if no error code and message
	{
		lr_end_transaction ( "T16_Submit_Order", LR_PASS ) ;
		ONLINE_ORDER_SUBMIT=1;
		return LR_PASS;
	}
	else {
		lr_fail_trans_with_error( lr_eval_string ("{T16_Submit_Order} Failed with http Error Code: {httpReturnCode}, api Error Code:  \"{errorCode}\", Error Message: {errorMessage}, Email: {userEmail}, OrderId:{orderId}") ) ;
		lr_end_transaction ( "T16_Submit_Order", LR_FAIL ) ;
		ONLINE_ORDER_SUBMIT=0;

		lr_start_transaction ( "T16_Submit_Order" ) ;

		if (strcmp( lr_eval_string("{largeCart}"), "true") == 0 )
			lr_save_string("T16_Submit_Order_Large_Cart", "T16_Submit_Order_Sub");
		else if (strcmp( lr_eval_string("{addBopisToCart}"), "true") == 0 )
			lr_save_string("T16_Submit_Order_Bopis", "T16_Submit_Order_Sub");
		else
			lr_save_string("T16_Submit_Order_Ecom", "T16_Submit_Order_Sub");

		if( strcmp(lr_eval_string("{isExpress}"), "true") == 0 ) {
			truncSpace(); //truncate space from expMonth
			lr_start_sub_transaction ( lr_eval_string("{T16_Submit_Order_Sub}_updatePaymentInstruction"), "T16_Submit_Order" ) ;
			addHeader();
			web_add_header( "savePayment", "false" );
			web_add_header( "Accept", "application/json" );
			web_add_header( "Content-Type", "application/json" );
			web_custom_request("addPaymentInstruction",
				"URL=https://{api_host}/payment/updatePaymentInstruction",
				"Method=PUT",
				"TargetFrame=",
				"Resource=0",
				"RecContentType=application/json",
				"Mode=HTML",
				"EncType=application/json",
//				"Body={\"prescreen\":true,\"paymentInstruction\":[{\"expire_month\":\"2\",\"piAmount\":\"{piAmount}\",\"payMethodId\":\"VISA\",\"cc_brand\":\"VISA\",\"expire_year\":\"2022\",\"account\":\"************1111\",\"piId\":\"{piId}\",\"cc_cvc\":\"111\",\"isDefault\":\"false\"}]}",
				"Body={\"prescreen\":true,\"paymentInstruction\":[{\"expire_month\":\"{expMonth}\",\"piAmount\":\"{piAmount}\",\"payMethodId\":\"VISA\",\"cc_brand\":\"VISA\",\"expire_year\":\"{expYear}\",\"account\":\"************1111\",\"piId\":\"{piId}\",\"cc_cvc\":\"111\",\"isDefault\":\"false\"}]}",
				LAST);
			lr_end_sub_transaction(lr_eval_string("{T16_Submit_Order_Sub}_updatePaymentInstruction"),  LR_AUTO );
		}

		lr_start_sub_transaction ( lr_eval_string("{T16_Submit_Order_Sub}_S01_addCheckout"), "T16_Submit_Order" ) ;

		#if OPTIONSENABLED
			web_add_header("Accept", "*/*");
			web_add_header("Access-Control-Request-Headers", "Origin, Host, User-Agent, Accept, Accept-Encoding, Accept-Language, Referer, Request, Content-Type, Transfer-Encoding, Cache-Control, Connection, Content-Length, Content-Language, Date, Expires, Keep-Alive, Location, pragma, client_id, client_secret, grant_type, scope, Authorization, Set-Cookie, Cookie, storeId, nickName, catalogId, langId, orderId, callingPage, requestType, paymentInstructionId, customerId, act, com, a1, city, state, postal, cols, opt, fromPage, promoCode, addressField1, addressField2, zipCode, pageName, locStore, responseFormat, coreName, term, count, isRest, espotName, orderItemId, productId, oldOrderItemId, memberId, emailId, fromRest, companyId, storeLocId, quantity, catEntryId, firstname, lastname, phone, email, action, taskType, stlocId, posStores, isBOPIS, xAppConfigAttrNames, tcpOrderId, PaRes, MD, externalId, pageNumber, pageSize, categoryId, primary, addressId, identifier, corenamecategory, published, rows, searchterm, start, timeallowed, devicetype, savePayment, calc");
			web_add_header("Access-Control-Request-Method", "CONNECT, DELETE, GET, OPTIONS, PATCH, POST, PUT, TRACE");
			web_add_header("Origin", "https://{host}");

			web_custom_request("payment/addCheckout",
				"URL=https://{api_host}/addCheckout",
				"Method=OPTIONS",
				"Resource=0",
			LAST);
		#endif

		registerErrorCodeCheck();
		addHeader();
		web_add_header( "Accept", "application/json" );
		web_add_header( "Content-Type", "application/json" );
		web_reg_find("TEXT/IC={", "SaveCount=apiCheck", LAST);

		web_custom_request("addCheckout",
			"URL=https://{api_host}/addCheckout",
			"Method=POST",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTTP",
			"EncType=application/json",
			"Body={\"orderId\":\"{orderId}\",\"isRest\":\"true\",\"locStore\":\"True\"}",
			LAST);
		lr_end_sub_transaction(lr_eval_string("{T16_Submit_Order_Sub}_S01_addCheckout"),  LR_AUTO );

		rc = web_get_int_property(HTTP_INFO_RETURN_CODE);
		lr_save_int(rc, "httpReturnCode");

		lr_save_string("T16_Submit_Order","mainTransaction");
		tcp_api2("tcpproduct/getPriceDetails", "GET",  lr_eval_string("{mainTransaction}") );
//		if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 ) //if no error code and message
		if ( strlen( lr_eval_string("{apiCheck}") ) > 0 ) //&& strcmp(lr_eval_string("{errorMessage}") ,"") == 0 ) //if no error code and message
		{
			lr_end_transaction ("T16_Submit_Order", LR_AUTO ) ;
			return LR_PASS;
		}else{
			lr_end_transaction ("T16_Submit_Order", LR_FAIL ) ;
			return LR_FAIL;
		}
	}

}

void viewReservationHistory()
{
	lr_think_time ( LINK_TT );
	//web_reg_save_param("reservationHistory", "LB=Re", "RB=ation History", "NotFound=Warning", LAST);
	//web_reg_find("Text=Reservation History", "SaveCount=reservationHistory");

	lr_start_transaction("T18_ViewReservationHistory");

	web_url("TCPDOMMyReservationHistoryView",
		"URL=https://{host}/webapp/wcs/stores/servlet/TCPDOMMyReservationHistoryView?storeId={storeId}&catalogId={catalogId}&langId=-1&sortRank=&sortKey=&curentPage=1&pageLength=1000",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		LAST);

		lr_start_sub_transaction("T18_ViewReservationHistory_S01_getReservationHistory", "T18_ViewReservationHistory" );
	addHeader();
	web_add_header("fromRest", "true");
	web_custom_request("getReservationHistory",
		"URL=https://{api_host}/payment/getReservationHistory",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		"EncType=application/json",
		LAST);

	lr_end_sub_transaction("T18_ViewReservationHistory_S01_getReservationHistory",LR_AUTO);


	lr_end_transaction("T18_ViewReservationHistory",LR_AUTO);

}

void viewPointsHistory()
{
	lr_think_time ( LINK_TT );

	lr_start_transaction("T18_ViewPointsHistory");

	web_custom_request("TCPMyPointsHistoryView",
		"URL=https://{host}/shop/TCPMyPointsHistoryView?catalogId={catalogId}&langId=-1&storeId={storeId}",
		"Method=POST",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		"EncType=",
		LAST);

	lr_end_transaction("T18_ViewPointsHistory",LR_AUTO);
}


void viewOrderStatusGuest()
{
	//lr_continue_on_error(1);
	lr_think_time ( LINK_TT );

	lr_save_string("T08_ViewMyAccount_Orders_Details", "mainTransaction");

	lr_start_transaction("T08_ViewMyAccount_Orders_Details");

	lr_start_sub_transaction("T08_ViewMyAccount_Orders_Details_track-order", "T08_ViewMyAccount_Orders_Details" );
	web_reg_find("TEXT/IC={orderId}", "SaveCount=apiCheck", LAST);
	web_url("T18_ViewOrderStatus",
		"URL=https://{host}/{country}/track-order/{orderId}/bitrogue@mailinator.com", //06.21.17
		"TargetFrame=_self",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		LAST);

	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T08_ViewMyAccount_Orders_Details_track-order", LR_FAIL);
	else
		lr_end_sub_transaction("T08_ViewMyAccount_Orders_Details_track-order", LR_AUTO);

	//tcp_api2("tcporder/getXAppConfigValues", "GET", "T08_ViewMyAccount_Orders_Details" );

	if (ESPOT_FLAG == 1)
	{

		lr_start_sub_transaction ("T08_ViewMyAccount_Orders_Details_getESpot","T08_ViewMyAccount_Orders_Details" );

		#if OPTIONSENABLED
			call_OPTIONS("getESpot");
		#endif

		web_add_header("espotName","GlobalHeaderBannerAboveHeader,GlobalHeaderBannerWithinHeader,GlobalHeaderLogo,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,create_account_drawer,MinicartLoginBanner,MinicartRegisterBanner,drawer_myaccount_overview_banner,coupon_help,bag_empty_banner,drawer_empty_bag,MinicartLoginFromFavoritesBanner,bag_ledger_promo_banner,gift_services_espot,CheckoutConfirmationBanner,CheckoutConfirmationMPRPromo,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1");
		web_add_header("deviceType","desktop");

//		web_add_header("espotName","GlobalHeaderBannerAboveHeader,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1");
		addHeader();
		web_reg_find("TEXT/IC=espotName", "SaveCount=apiCheck", LAST);
		web_custom_request("getESpots",
			"URL=https://{api_host}/getESpot",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction("T08_ViewMyAccount_Orders_Details_getESpot", LR_FAIL);
		else
			lr_end_sub_transaction("T08_ViewMyAccount_Orders_Details_getESpot", LR_AUTO);

		api_getESpot_second();
	}
		lr_start_sub_transaction ( "T08_ViewMyAccount_Orders_Details_getRegisteredUserDetailsInfo", "T08_ViewMyAccount_Orders_Details" );
/*
		#if OPTIONSENABLED
			call_OPTIONS("payment/getRegisteredUserDetailsInfo");
		#endif
*/
		addHeader();
		web_reg_find("TEXT/IC=resourceName", "SaveCount=apiCheck", LAST);
		web_custom_request("getRegisteredUserDetailsInfo",
			"URL=https://{api_host}/payment/getRegisteredUserDetailsInfo",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction("T08_ViewMyAccount_Orders_Details_getRegisteredUserDetailsInfo", LR_FAIL);
		else
			lr_end_sub_transaction("T08_ViewMyAccount_Orders_Details_getRegisteredUserDetailsInfo", LR_AUTO);

		lr_start_sub_transaction ( "T08_ViewMyAccount_Orders_Details_getPointsService", "T08_ViewMyAccount_Orders_Details" );
/*
		#if OPTIONSENABLED
			call_OPTIONS("payment/getPointsService");
		#endif
*/
		addHeader();
		web_reg_find("TEXT/IC=LoyaltyWebsiteInd", "SaveCount=apiCheck", LAST);
		web_custom_request("getPointsService",
			"URL=https://{api_host}/payment/getPointsService",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction("T08_ViewMyAccount_Orders_Details_getPointsService", LR_FAIL);
		else
			lr_end_sub_transaction("T08_ViewMyAccount_Orders_Details_getPointsService", LR_AUTO);

		lr_start_sub_transaction("T08_ViewMyAccount_Orders_Details_getOrderDetails", "T08_ViewMyAccount_Orders_Details" );
/*
		#if OPTIONSENABLED
			call_OPTIONS("getOrderDetails");
		#endif
*/
		addHeader();
		web_add_header("locStore", "True");
		web_add_header("pageName", "fullOrderInfo");
//		web_reg_save_param("piAmount", "LB=\"piAmount\": \"", "RB=\",\n", "NotFound=Warning", LAST); //"piAmount": "7.95",\n
		web_reg_find("TEXT/IC={", "SaveCount=apiCheck", LAST);
		web_custom_request("getOrderDetails",
			"URL=https://{api_host}/getOrderDetails",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction("T08_ViewMyAccount_Orders_Details_getOrderDetails", LR_FAIL);
		else
			lr_end_sub_transaction("T08_ViewMyAccount_Orders_Details_getOrderDetails", LR_AUTO);


		lr_start_sub_transaction("T08_ViewMyAccount_Orders_Details_orderLookUp", "T08_ViewMyAccount_Orders_Details" );
		addHeader();
		web_add_header("orderId", lr_eval_string("{orderId}"));
		web_add_header("emailId", "bitrogue@mailinator.com");
		web_reg_find("TEXT/IC=orderLookupResponse", "SaveCount=apiCheck", LAST);
		web_custom_request("tcporder/orderLookUp",
			"URL=https://{api_host}/tcporder/orderLookUp",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction("T08_ViewMyAccount_Orders_Details_orderLookUp", LR_FAIL);
		else
			lr_end_sub_transaction("T08_ViewMyAccount_Orders_Details_orderLookUp", LR_AUTO);

		//web_add_header("action", "get");   //Removed by PavanDusi 111617
		//tcp_api2("tcporder/getFavouriteStoreLocation", "GET", "T08_ViewMyAccount_Orders_Details" );    //Removed by PavanDusi 111617

		//tcp_api2("tcporder/getXAppConfigValues", "GET", "T08_ViewMyAccount_Orders_Details" );

		lr_end_transaction("T08_ViewMyAccount_Orders_Details", LR_AUTO);

		//lr_continue_on_error(0);

//	}
}// end viewOrderStatusGuest()

void logoff()
{

	addHeader();
	web_add_header( "Content-Type", "application/json" );
	//web_reg_save_param("orderId",  "LB=\"orderId\": \"", "RB=\"", "NotFound=Warning", LAST);
	lr_start_transaction("T21_Logoff");
	web_custom_request("logout",
		"URL=https://{api_host}/logout",
		"Method=DELETE",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTTP",
		LAST);

	if (ESPOT_FLAG == 1)
	{
		lr_start_sub_transaction ( "T21_Logoff_getESpot", "T21_Logoff" );

		#if OPTIONSENABLED
			call_OPTIONS("getESpot");
		#endif

		web_add_header("espotName","GlobalHeaderBannerAboveHeader,GlobalHeaderBannerWithinHeader,GlobalHeaderLogo,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,create_account_drawer,MinicartLoginBanner,MinicartRegisterBanner,drawer_myaccount_overview_banner,coupon_help,bag_empty_banner,drawer_empty_bag,MinicartLoginFromFavoritesBanner,bag_ledger_promo_banner,gift_services_espot,CheckoutConfirmationBanner,CheckoutConfirmationMPRPromo,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1");
		web_add_header("deviceType","desktop");

	//    web_add_header("espotName","GlobalHeaderBannerAboveHeader,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1");
		addHeader();
		web_reg_find("TEXT/IC=espotName", "SaveCount=apiCheck", LAST);
		web_custom_request("getESpots",
			"URL=https://{api_host}/getESpot",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction("T21_Logoff_getESpot", LR_FAIL);
		else
			lr_end_sub_transaction("T21_Logoff_getESpot", LR_AUTO);

		api_getESpot_second();
	}

	lr_start_sub_transaction ("T21_Logoff_getOrderDetails","T21_Logoff");
/*
	#if OPTIONSENABLED
		call_OPTIONS("getOrderDetails");
	#endif
*/
	addHeader();
	web_add_header("locStore", "False");
	web_add_header("pageName", "orderSummary");
//	web_reg_save_param("cartCount", "LB=cartCount\": ", "RB=,", "NotFound=Warning", LAST);			//"CartCount": 1,\n
	web_url("getOrderDetails",
		"URL=https://{api_host}/getOrderDetails",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	lr_end_sub_transaction ("T21_Logoff_getOrderDetails",LR_AUTO);

	lr_start_sub_transaction ( "T21_Logoff_getRegisteredUserDetailsInfo", "T21_Logoff" );
/*
	#if OPTIONSENABLED
		call_OPTIONS("payment/getRegisteredUserDetailsInfo");
	#endif
*/
	addHeader();
	//web_reg_save_param ( "addressId" , "LB=\"addressId\": \"" , "RB=\",\n" , "ORD=1", "NotFound=Warning", LAST ) ; //"addressId": "1398789",\n
	//web_reg_save_param ( "addressIdAll" , "LB=\"addressId\": \"" , "RB=\",\n" , "ORD=ALL", "NotFound=Warning", LAST ) ; //"addressId": "1398789",\n
	web_reg_find("TEXT/IC=resourceName", "SaveCount=apiCheck", LAST);
	web_custom_request("getRegisteredUserDetailsInfo",
		"URL=https://{api_host}/payment/getRegisteredUserDetailsInfo",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T21_Logoff_getRegisteredUserDetailsInfo", LR_FAIL);
	else
		lr_end_sub_transaction("T21_Logoff_getRegisteredUserDetailsInfo", LR_AUTO);

	lr_start_sub_transaction ( "T21_Logoff_getPointsService", "T21_Logoff" );
/*
	#if OPTIONSENABLED
		call_OPTIONS("payment/getPointsService");
	#endif
*/
	addHeader();
	web_reg_find("TEXT/IC=LoyaltyWebsiteInd", "SaveCount=apiCheck", LAST);
	web_custom_request("getPointsService",
		"URL=https://{api_host}/payment/getPointsService",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T21_Logoff_getPointsService", LR_FAIL);
	else
		lr_end_sub_transaction("T21_Logoff_getPointsService", LR_AUTO);

	lr_end_transaction("T21_Logoff", LR_AUTO);
/*
	web_url("T21_Logoff",
		"URL=https://{host}/shop/Logoff?catalogId={catalogId}&rememberMe=false&myAcctMain=1&langId=-1&storeId={storeId}&URL=LogonForm",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Snapshot=t62.inf",
		"Mode=HTML",
		LAST);
*/
	//lr_end_transaction("T21_Logoff", LR_AUTO);

}

int createAccount() //former registerUser()
{
	lr_think_time ( FORM_TT ) ;

	//lr_continue_on_error(1);

	lr_start_transaction ( "T17_Register_User" ) ;

	//web_reg_save_param("userId", "LB=\"userId\": \"", "RB=\"", "NotFound=Warning", LAST); //"userId": "202607181"\n // when it goes thru
	registerErrorCodeCheck();

	addHeader();
	lr_start_sub_transaction ( "T17_Register_User_addCustomerRegistration", "T17_Register_User" );
	web_custom_request("addCustomerRegistration",
		"URL=https://{api_host}/addCustomerRegistration",
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTTP",
		"EncType=application/json",
		"Body={\"firstName\":\"Joe\",\"lastName\":\"User\",\"zipCode\":\"07094\",\"logonId\":\"bitrogue@mailinator.com\",\"logonPassword\":\"Asdf!234\",\"phone1\":\"2014531513\",\"rememberCheck\":true,\"rememberMe\":true,\"catalogId\":\"{catalogId}\",\"langId\":\"-1\",\"storeId\":\"{storeId}\"}",
		LAST);
	lr_end_sub_transaction("T17_Register_User_addCustomerRegistration", LR_AUTO);
//		"Body={\"firstName\":\"Joe\",\"lastName\":\"User\",\"zipCode\":\"07094\",\"logonId\":\"TCPPERF_{storeId}_Guest_{emailVerification}@gmail.com\",\"logonPassword\":\"Asdf!234\",\"phone1\":\"2014531513\",\"rememberCheck\":true,\"rememberMe\":true,\"catalogId\":\"{catalogId}\",\"langId\":\"-1\",\"storeId\":\"{storeId}\"}",

	lr_save_string("T17_Register_User", "mainTransaction");

	tcp_api2("payment/getRegisteredUserDetailsInfo", "GET",  lr_eval_string("{mainTransaction}") ); // api_getRegisteredUserDetailsInfo();
	//web_add_header("action", "get"); //Removed by PavanDusi 111617
	//tcp_api2("tcporder/getFavouriteStoreLocation", "GET",  lr_eval_string("{mainTransaction}") );             //Removed by PavanDusi 111617

	web_add_header("locStore", "True");
	web_add_header("pageName", "fullOrderInfo");
	web_reg_save_param("cartCount", "LB=artCount\": ", "RB=,", "NotFound=Warning", LAST);			//"CartCount": 1,\n
	tcp_api2("getOrderDetails", "GET", lr_eval_string("{mainTransaction}") );                      // api_getOrderDetails();

	if (ESPOT_FLAG == 1)
	{
		tcp_api2("getESpot", "GET", lr_eval_string("{mainTransaction}") );                             // turned off for akamai caching
	}

	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 ) //if no error code and message
	{
		lr_end_transaction ( "T17_Register_User", LR_AUTO ) ;
		return LR_PASS;
	}
	else {
		lr_fail_trans_with_error( lr_eval_string ("T17_Register_User Failed with Error Code:  \"{errorCode}\", Error Message: {errorMessage}") ) ;
		lr_end_transaction ( "T17_Register_User", LR_FAIL ) ;
	}

	//lr_continue_on_error(0);

	return 0;
}


void buildCartDrop(int userProfile)
{
	int iLoop, cartTypeRatio = 0;
	lr_think_time ( FORM_TT ) ;

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CART_MERGE ) { //50%
		target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_MERGE_CART_SIZE}"));
	}
	else {
		target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_DROP_CART_SIZE}"));
	}
	if ( strcmp(strupr(lr_eval_string("{buildCart}")),  "TRUE") == 0) {
		target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_BUILD_CART}"));
	}

	iLoop = target_itemsInCart + 2;

	lr_start_transaction("T20_New_Cart");
	lr_end_transaction("T20_New_Cart", LR_PASS);

	if ( userProfile == 0 && atoi(lr_eval_string("{cartCount}")) == 1) {
		orderItemIdscount = 0;
	}
	else {
		if (atoi(lr_eval_string("{cartCount}")) == 1) {
			orderItemIdscount = 0;
		}
		else
			orderItemIdscount = atoi(lr_eval_string("{cartCount}"));
	}

	if (  orderItemIdscount < target_itemsInCart ) {

	    target_itemsInCart = target_itemsInCart - orderItemIdscount ;

			cartTypeRatio =  atoi(lr_eval_string("{RANDOM_PERCENT}")); ///determine the type of cart to build

		for(index_buildCart=0; index_buildCart < target_itemsInCart ; index_buildCart++)
		{
			topNav();
			if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_BUILDCART_DRILLDOWN )
				drill();

			paginate();

/*			rhy - this is disabled as we are picking the pdpurl now from the datafile
			if (( lr_paramarr_len ( "pdpURL" ) == 0 ) ) //&& lr_paramarr_len ( "quickviewURL" ) == 0 )
			{	index_buildCart--;

				continue;
			}
*/
		if ( cartTypeRatio <= ECOMM_CART_RATIO ) {
			if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ADDTOCART_PLP )
				if (addToCartFromPLP() == LR_FAIL) {
					atc_Stat = 1;
				}
			else {
				if (productDisplay() != LR_FAIL) {
					addToCart();
				}
				else {
					atc_Stat = 1;
				}
			}
		}
		else {
			productDisplayBOPIS();
			addToCartMixed();
		}
			iLoop--;

			if (iLoop == 0)
			    break;

			if (atc_Stat == 1)  //0-Pass 1-Fail //if the addtocart failed, set the index_buildCart to original number
				index_buildCart--;

		} // end for loop to add o cart

		viewCart(); //0425 disabled
	}
} // end buildCartDrop

void buildCartDropNoBrowse(int userProfile)
{
	int iLoop, cartTypeRatio = 0;
	lr_think_time ( FORM_TT ) ;

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CART_MERGE ) { //50%
		target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_MERGE_CART_SIZE}"));
	}
	else {
		target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_DROP_CART_SIZE}"));
	}
	if ( strcmp(strupr(lr_eval_string("{buildCart}")),  "TRUE") == 0) {
		target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_BUILD_CART}"));
	}

	iLoop = target_itemsInCart + 2;

	lr_start_transaction("T20_New_Cart");
	lr_end_transaction("T20_New_Cart", LR_PASS);

	if ( userProfile == 0 && atoi(lr_eval_string("{cartCount}")) == 1) {
		orderItemIdscount = 0;
	}
	else {
		if (atoi(lr_eval_string("{cartCount}")) == 1) {
			orderItemIdscount = 0;
		}
		else
			orderItemIdscount = atoi(lr_eval_string("{cartCount}"));
	}

	if (  orderItemIdscount < target_itemsInCart ) {

	    target_itemsInCart = target_itemsInCart - orderItemIdscount ;

			cartTypeRatio =  atoi(lr_eval_string("{RANDOM_PERCENT}")); ///determine the type of cart to build

		for(index_buildCart=0; index_buildCart < target_itemsInCart ; index_buildCart++)
		{
			topNav();

			if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_BUILDCART_DRILLDOWN )
				drill();

			paginate();

			if ( cartTypeRatio <= ECOMM_CART_RATIO ) {
				if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ADDTOCART_PLP )
					if (addToCartFromPLP() == LR_FAIL) {
						atc_Stat = 1;
					}
				else {
					if (productDisplay() != LR_FAIL) {
						addToCart();
					}
					else {
						atc_Stat = 1;
					}
				}
			}
			else {
				productDisplayBOPIS();
				addToCartMixed();
			}
//			productDisplay();

//			addToCartNoBrowse();
			iLoop--;

			if (iLoop == 0)
			    break;

			if (atc_Stat == 1)  //0-Pass 1-Fail //if the addtocart failed, set the index_buildCart to original number
				index_buildCart--;

		} // end for loop to add o cart

//		viewCart(); //0425 disabled
	}
} // end buildCartDropNoBrowse

void buildCartCheckout(int userProfile)
{
	int iLoop, cartTypeRatio = 0;
	lr_think_time ( FORM_TT ) ;
	lr_save_string("false", "largeCart");

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_BUILDCART_LARGE ) {
		target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_LARGE_CART_SIZE}"));
		lr_save_string("true", "largeCart");
	}
	else if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CART_MERGE ) {
		target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_MERGE_CART_SIZE}"));
	}
	else {
		target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_CART_SIZE}"));
	}

	iLoop = target_itemsInCart + 2;

	lr_start_transaction("T20_New_Cart");
	lr_end_transaction("T20_New_Cart", LR_PASS);

	if ( userProfile == 0 && atoi(lr_eval_string("{cartCount}")) == 1) { //	userProfile == 0 is buildCartFirst before login, userProfile == 1 is loginFirst before building the cart
		orderItemIdscount = 0;
	}
	else {
		if (atoi(lr_eval_string("{cartCount}")) == 1) {
			orderItemIdscount = 0;
		}
		else
			orderItemIdscount = atoi(lr_eval_string("{cartCount}"));//cartCount
	}

	if (  orderItemIdscount < target_itemsInCart ) {

	    target_itemsInCart = target_itemsInCart - orderItemIdscount ;
//	    target_itemsInCart = 1 ;
//		lr_message("target_itemsInCart = %d", target_itemsInCart );

		cartTypeRatio =  atoi(lr_eval_string("{RANDOM_PERCENT}")); ///determine the type of cart to build

		for(index_buildCart=0; index_buildCart < target_itemsInCart ; index_buildCart++)
		{
			topNav();

			if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_BUILDCART_DRILLDOWN )
				drill();

			paginate();

/*			rhy - this is disabled as we are picking the pdpurl now from the datafile
			if (( lr_paramarr_len ( "pdpURL" ) == 0 ) ) //&& lr_paramarr_len ( "quickviewURL" ) == 0 )
			{	index_buildCart--;

				continue;
			}
*/
			if ( cartTypeRatio <= ECOMM_CART_RATIO ) {
				if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ADDTOCART_PLP )
					if (addToCartFromPLP() == LR_FAIL) {
						atc_Stat = 1;
					}
				else {
					if (productDisplay() != LR_FAIL) {
						addToCart();
					}
					else {
						atc_Stat = 1;
					}
				}
			}
			else {
				productDisplayBOPIS();
				addToCartMixed();
			}

			iLoop--;

			if (iLoop == 0)
			    break;

			if (atc_Stat == 1) { //0-Pass 1-Fail //if the addtocart failed, set the index_buildCart to original number
				index_buildCart--;
				lr_start_transaction("T40_Failed_Add_Cart");
				lr_end_transaction("T40_Failed_Add_Cart", LR_PASS);

			}

		} // end for loop to add o cart

//		viewCart(); //0425 diabled

	}

} // end buildCartCheckout

void buildCartCheckoutNoBrowse(int userProfile)
{
	int iLoop, cartTypeRatio = 0;
	lr_think_time ( FORM_TT ) ;
	lr_save_string("false", "largeCart");

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_BUILDCART_LARGE ) {
		target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_LARGE_CART_SIZE}"));
		lr_save_string("true", "largeCart");
	}
	else if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CART_MERGE ) {
		target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_MERGE_CART_SIZE}"));
	}
	else {
		target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_CART_SIZE}"));
	}

	iLoop = target_itemsInCart + 2;

	lr_start_transaction("T20_New_Cart");
	lr_end_transaction("T20_New_Cart", LR_PASS);

	if ( userProfile == 0 && atoi(lr_eval_string("{cartCount}")) == 1) { //	userProfile == 0 is buildCartFirst before login, userProfile == 1 is loginFirst before building the cart
		orderItemIdscount = 0;
	}
	else {
		if (atoi(lr_eval_string("{cartCount}")) == 1) {
			orderItemIdscount = 0;
		}
		else
			orderItemIdscount = atoi(lr_eval_string("{cartCount}"));//cartCount
	}

	if (  orderItemIdscount < target_itemsInCart ) {

	    target_itemsInCart = target_itemsInCart - orderItemIdscount ;

		cartTypeRatio =  atoi(lr_eval_string("{RANDOM_PERCENT}")); ///determine the type of cart to build

		for(index_buildCart=0; index_buildCart < target_itemsInCart ; index_buildCart++)
		{
			topNav();
			if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_BUILDCART_DRILLDOWN )
				drill();

			paginate();

			if ( cartTypeRatio <= ECOMM_CART_RATIO ) {
				if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ADDTOCART_PLP )
					if (addToCartFromPLP() == LR_FAIL) {
						atc_Stat = 1;
					}
				else {
					if (productDisplay() != LR_FAIL) {
						addToCart();
					}
					else {
						atc_Stat = 1;
					}
				}
			}
			else {
				productDisplayBOPIS();
				addToCartMixed();
			}
//			productDisplay();

//			addToCartNoBrowse();

			iLoop--;

			if (iLoop == 0)
			    break;

			if (atc_Stat == 1) { //0-Pass 1-Fail //if the addtocart failed, set the index_buildCart to original number
				index_buildCart--;
				lr_start_transaction("T40_Failed_Add_Cart");
				lr_end_transaction("T40_Failed_Add_Cart", LR_PASS);

			}

		} // end for loop to add o cart

//		viewCart(); //0425 diabled

	}

} // end buildCartCheckoutNoBrowse


void buildCartRopis()
{
	int randomPercent = atoi(lr_eval_string("{RANDOM_PERCENT}"));
	lr_think_time ( FORM_TT ) ;
/*
//	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_RESERVATION_WISHLIST) {
		wlGetCatEntryId();
		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RESERVE_ONLINE && atoi(lr_eval_string("{catEntryIds_count}")) != 0) {
			lr_save_string( lr_paramarr_random("catEntryIds"), "catentryId");
			reserveOnline();
		}
	}
	else {
*/
		drill();
		productDisplay();
		if ( randomPercent <= RESERVE_ONLINE && atoi(lr_eval_string("{atc_catentryIds_count}")) != 0) {
			lr_save_string( lr_paramarr_random("atc_catentryIds"), "catentryId");
		}
		else if ( randomPercent <= RESERVE_ONLINE && atoi(lr_eval_string("{bopisCatEntryId_count}")) != 0) {
			lr_save_string( lr_paramarr_random("bopisCatEntryId_count"), "catentryId");
		}

//		reserveOnline();
}
/*
int pickupInStoreGuest() //NOT MIXED  // this is not being called anywhere
{
	lr_think_time ( FORM_TT ) ;
	lr_save_string(lr_paramarr_random( "productId"), "bopisProductId" );

	web_reg_save_param("errorCode", "LB=\"errorCode\":\"", "RB=\"", "NotFound=Warning", LAST); //"errorCode":"0"
	web_reg_save_param("errorMessage", "LB=\"errorMessage\":\"", "RB=\"", "NotFound=Warning", LAST); //"errorMessage":"Invalid Parameters"

	addHeader();
	web_add_header("X-Requested-With", "XMLHttpRequest" );
	web_add_cookie("tcpState=NJ; DOMAIN=tcp-perf.childrensplace.com");

	lr_start_transaction("T05_PickupInStore_Guest getUserBopisStores");
		web_custom_request("getUserBopisStores",
		"URL=https://{host}/wcs/resources/store/{storeId}/getUserBopisStores?storeId={storeId}&catalogId={catalogId}&langId=-1",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		"EncType=application/json",
		LAST);
	lr_end_transaction("T05_PickupInStore_Guest getUserBopisStores", LR_AUTO);

	addHeader();
	lr_start_transaction("T05_PickupInStore_Guest GetSwatchesAndSizeInfo");
	web_custom_request("GetSwatchesAndSizeInfo",
		"URL=https://{host}/webapp/wcs/stores/servlet/GetSwatchesAndSizeInfo?storeId={storeId}&catalogId={catalogId}&langId=-1&productId=62141",
//		"URL=https://{host}/webapp/wcs/stores/servlet/GetSwatchesAndSizeInfo?storeId={storeId}&catalogId={catalogId}&langId=-1&productId={bopisProductId}",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		"EncType=application/json",
		LAST);
	lr_end_transaction("T05_PickupInStore_Guest GetSwatchesAndSizeInfo", LR_AUTO);

	lr_start_transaction("T05_PickupInStore_Guest BOPIS_Search_Modal");
	web_custom_request("BOPIS_Search_Modal",
		"URL=https://{host}/wcs/resources/store/10151/espots/BOPIS_Search_Modal?responseFormat=json&storeId={storeId}&catalogId={catalogId}&langId=-1",
		"Method=GET",
		"Resource=1",
		"RecContentType=application/json",
		"EncType=application/json",
		LAST);
	lr_end_transaction("T05_PickupInStore_Guest BOPIS_Search_Modal", LR_AUTO);



	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 ) //if no error code and message
	{
//		if (addBopisToCart() == LR_PASS)
//		{	if ( pickupDetailsGuest() == LR_PASS)
//				return LR_PASS;
//			else
				return LR_FAIL;
//		}
//		else
//			return LR_FAIL;
	}
	else
	{
		lr_fail_trans_with_error( lr_eval_string("T05_PickupInStore_Guest getUserBopisStore Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
		return LR_FAIL;
	}

}
*/

int pickupDetailsGuest() //NOT MIXED
{
	lr_think_time ( FORM_TT ) ;

	lr_start_transaction("T05_PickupInStore_Guest");
	web_reg_save_param("errorCode", "LB=\"errorCode\":\"", "RB=\"", "NotFound=Warning", LAST); //"errorCode":"0"
	web_reg_save_param("errorMessage", "LB=\"errorMessage\":\"", "RB=\"", "NotFound=Warning", LAST); //"errorMessage":"Invalid Parameters"

	addHeader();
	lr_start_sub_transaction("T05_PickupInStore_Guest addAddress","T05_PickupInStore_Guest");
	web_custom_request("addAddress",
		"URL=https://{api_host}/payment/addAddress",
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTTP",
		"EncType=application/json",
		"Body={\"contact\":[{\"addressType\":\"Shipping\",\"firstName\":\"Manny\",\"lastName\":\"Yamzon\",\"phone2\":\"2014531236\",\"email1\":\"bitrogue@mailinator.com\",\"email2\":\"\"}]}",
		LAST);
	lr_end_sub_transaction("T05_PickupInStore_Guest addAddress", LR_AUTO);
//		"Body={\"contact\":[{\"addressType\":\"Shipping\",\"firstName\":\"Manny\",\"lastName\":\"Yamzon\",\"phone2\":\"2014531236\",\"email1\":\"mannyyamzon@gmail.com\",\"email2\":\"\"}]}",

	lr_start_sub_transaction("T05_PickupInStore_Guest_getOrderDetails", "T05_PickupInStore_Guest");
/*
	#if OPTIONSENABLED
		call_OPTIONS("getOrderDetails");
	#endif
*/
	addHeader();
	//R3 hypercare change 08102017
	web_add_header("calc", "false");
	web_add_header("locStore", "True");
	web_add_header("pageName", "fullOrderInfo");
	web_reg_find("TEXT/IC={", "SaveCount=apiCheck", LAST);
	web_url("getOrderDetails",
		"URL=https://{api_host}/getOrderDetails",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T05_PickupInStore_Guest_getOrderDetails", LR_FAIL);
	else
		lr_end_sub_transaction("T05_PickupInStore_Guest_getOrderDetails", LR_AUTO);
	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 ) //if no error code and message
	{
		lr_end_transaction("T05_PickupInStore_Guest", LR_AUTO);
		return LR_PASS;
	}
	else
	{
		lr_fail_trans_with_error( lr_eval_string("T05_PickupInStore_Guest getUserBopisStore Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
		lr_end_transaction("T05_PickupInStore_Guest", LR_FAIL);
		return LR_FAIL;
	}

}

int pickupInStore()
{
	lr_think_time ( FORM_TT ) ;
	lr_start_transaction ( "T05_PickupInStore" ) ;

	lr_save_string("T05_PickupInStore", "mainTransaction");

	lr_start_sub_transaction("T05_PickupInStore_getUserBopisStores", lr_eval_string("{mainTransaction}"));
	addHeader();
	web_reg_find("TEXT/IC={", "SaveCount=apiCheck", LAST);
	web_url("getUserBopisStores",
		"URL=https://{api_host}/tcporder/getUserBopisStores",
		"Resource=0",
		"RecContentType=application/json",
		LAST);

	if (atoi(lr_eval_string("{apiCheck}")) == 0 ) {
		lr_end_sub_transaction("T05_PickupInStore_getUserBopisStores", LR_FAIL);
		lr_end_transaction("T05_PickupInStore", LR_FAIL);
		return LR_FAIL;
	} else {
		lr_end_transaction("T05_PickupInStore_getUserBopisStores", LR_AUTO);

//		tcp_api2("tcporder/getAllCoupons", "GET",  lr_eval_string("{mainTransaction}") );
//		tcp_api2("tcpproduct/getPriceDetails", "GET",  lr_eval_string("{mainTransaction}") );
		tcp_api2("payment/getPointsService", "GET",  lr_eval_string("{mainTransaction}") );
		//web_add_header("action", "get");  //Removed by PavanDusi 111617
		//tcp_api2("tcporder/getFavouriteStoreLocation", "GET",  lr_eval_string("{mainTransaction}") ); //Removed by PavanDusi 111617

	//	country:US
	//	latitude:40.9411487
	//	longitude:-74.25314650000001
	//	sType:BOPIS
	web_add_header("sType","BOPIS");
	web_add_header("latitude","40.4333407");
	web_add_header("longitude","-74.4115246");

	//"numStoresHavingItem": 2,
		lr_start_sub_transaction("T05_PickupInStore_getStoreandProductInventoryInfo", lr_eval_string("{mainTransaction}"));
		web_reg_save_param("storeLocId","LB=storeUniqueID\": \"", "RB=\"", "Notfound=Warning", LAST);
		web_reg_find("TEXT/IC=numStoresHavingItem", "SaveCount=apiCheck", LAST);
		addHeader();
		web_url("getStoreandProductInventoryInfo",
//			"URL=https://{api_host}/tcpstore/getStoreandProductInventoryInfo?latitude=40.9411487&longitude=-74.25314650000001&catentryId=569196&country=US&sType=BOPIS",
//            "URL=https://{api_host}/tcpstore/getStoreandProductInventoryInfo?latitude=40.4333407&longitude=-74.4115246&catentryId=473513&distance=75&country=US&sType=BOPIS",
            "URL=https://{api_host}/tcpstore/getStoreandProductInventoryInfo?latitude=40.4333407&longitude=-74.4115246&catentryId={atc_catentryId}&distance=75&country=US&sType=BOPIS",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction("T05_PickupInStore_getStoreandProductInventoryInfo", LR_FAIL);
		else
			lr_end_sub_transaction("T05_PickupInStore_getStoreandProductInventoryInfo", LR_AUTO);

		lr_end_transaction("T05_PickupInStore", LR_AUTO);
	}

	return LR_PASS;

}

int pickupInStore_R3()
{
	lr_think_time ( FORM_TT ) ;
	lr_start_transaction ( "T05_PickupInStore" ) ;
	//get store location availability here
	//All these call is just for clicking the "PICK UP IN STORE" button
	lr_start_sub_transaction("T05_PickupInStore_S01 TCPAutoPopulateAddressControllerCmd", "T05_PickupInStore" );
	web_custom_request("TCPAutoPopulateAddressControllerCmd",
		"URL=https://{host}/webapp/wcs/stores/servlet/TCPAutoPopulateAddressControllerCmd?storeId={storeId}&catalogId={catalogId}&langId=-1",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		"EncType=application/json",
		LAST);
	lr_end_sub_transaction("T05_PickupInStore_S01 TCPAutoPopulateAddressControllerCmd",LR_AUTO);

	// lr_start_sub_transaction("T05_PickupInStore_S02 getXAppConfigValues", "T05_PickupInStore" );
	// web_custom_request("getXAppConfigValues",
		// "URL=https://{host}/wcs/resources/store/{storeId}/getXAppConfigValues?storeId={storeId}&catalogId={catalogId}&langId=-1",
		// "Method=GET",
		// "Resource=0",
		// "RecContentType=application/json",
		// "EncType=application/json",
		// LAST);
	// lr_end_sub_transaction("T05_PickupInStore_S02 getXAppConfigValues",LR_AUTO);

	lr_start_sub_transaction("T05_PickupInStore_S02 getUserBopisStores", "T05_PickupInStore" );
	web_custom_request("getUserBopisStores",
		"URL=https://{host}/wcs/resources/store/{storeId}/getUserBopisStores?storeId={storeId}&catalogId={catalogId}&langId=-1",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		"EncType=application/json",
		LAST);
	lr_end_sub_transaction("T05_PickupInStore_S02 getUserBopisStores",LR_AUTO);

	lr_save_string(lr_paramarr_random( "productId"), "bopisProductId" );

	web_reg_save_param("errorCode", "LB=\"errorCode\":\"", "RB=\"", "NotFound=Warning", LAST); //"errorCode":"0"
	web_reg_save_param("errorMessage", "LB=\"errorMessage\":\"", "RB=\"", "NotFound=Warning", LAST); //"errorMessage":"Invalid Parameters"

	lr_start_sub_transaction("T05_PickupInStore_S03 GetSwatchesAndSizeInfo", "T05_PickupInStore" );
	web_custom_request("GetSwatchesAndSizeInfo",
//		"URL=https://{host}/webapp/wcs/stores/servlet/GetSwatchesAndSizeInfo?storeId={storeId}&catalogId={catalogId}&langId=-1&productId=62141",
		"URL=https://{host}/webapp/wcs/stores/servlet/GetSwatchesAndSizeInfo?storeId={storeId}&catalogId={catalogId}&langId=-1&productId={bopisProductId}",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		"EncType=application/json",
		LAST);
	lr_end_sub_transaction("T05_PickupInStore_S03 GetSwatchesAndSizeInfo",LR_AUTO);

	lr_start_sub_transaction("T05_PickupInStore_S04 BOPIS_Search_Modal", "T05_PickupInStore" );
	web_custom_request("BOPIS_Search_Modal",
		"URL=https://{host}/wcs/resources/store/{storeId}/espots/BOPIS_Search_Modal?responseFormat=json&storeId={storeId}&catalogId={catalogId}&langId=-1",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		"EncType=application/json",
		LAST);
	lr_end_sub_transaction("T05_PickupInStore_S04 BOPIS_Search_Modal",LR_AUTO);


	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 ) //if no error code and message
	{
		lr_end_transaction ( "T05_PickupInStore" , LR_PASS) ;
		return LR_PASS;
	}
	else
	{
		lr_fail_trans_with_error( lr_eval_string("T05_PickupInStore_S02 getUserBopisStore Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
		lr_end_transaction ( "T05_PickupInStore", LR_FAIL ) ;
		return LR_FAIL;
	}

}

int addBopisToCart()
{
	lr_think_time ( LINK_TT ) ;
	lr_start_transaction("T05_Add_To_Cart_Bopis");

	web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", LAST); //"errorCode": "",\n
	web_reg_save_param("errorMessage", "LB=\"errorMessage\": \"", "RB=\",\n", "NotFound=Warning", LAST); //"errorMessage": "",\n
	web_reg_save_param("orderId", "LB=\"orderId\": \"", "RB=\"", "NotFound=Warning", LAST); //"orderId": "8006336024"
	web_reg_save_param("orderItemDescription", "LB=\"orderItemDescription\": \"", "RB=\"", "NotFound=Warning", LAST); //"orderItemDescription": "8006336025"

	addHeader();
	web_custom_request("createBopisOrder",
		"URL=https://{api_host}/tcporder/createBopisOrder",
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTTP",
		"EncType=application/json",
		"Body={\"storeLocId\":\"110005\",\"quantity\":\"1\",\"catEntryId\":\"{randCatEntryId_BOPIS}\",\"isRest\":\"false\"}",
		LAST);
//		"Body={\"storeLocId\":\"110005\",\"quantity\":\"1\",\"catEntryId\":\"473513\",\"isRest\":\"false\"}",  // old tcp-perf 11/06
//		"Body={\"storeLocId\":\"110002\",\"quantity\":\"1\",\"catEntryId\":\"569196\",\"isRest\":\"false\"}",  //uatlive1 - todo - replace this later with a parameter when it gets to perflive environment

	addHeader();
	web_add_header("locStore", "True");
	web_add_header("pageName", "fullOrderInfo");
	web_url("getOrderDetails",
		"URL=https://{api_host}/getOrderDetails",
		"Resource=0",
		"RecContentType=application/json",
		LAST);

//	tcp_api2("tcporder/getAllCoupons", "GET",  "T05_Add_To_Cart_Bopis" );
//	tcp_api2("tcpproduct/getPriceDetails", "GET",  "T05_Add_To_Cart_Bopis" );
	tcp_api2("payment/getPointsService", "GET",  "T05_Add_To_Cart_Bopis" );
	//web_add_header("action", "get");   //Removed by PavanDusi 111617
	//tcp_api2("tcporder/getFavouriteStoreLocation", "GET",  "T05_Add_To_Cart_Bopis" );  //Removed by PavanDusi 111617

	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 ) //if no error code and message
	{	lr_end_transaction("T05_Add_To_Cart_Bopis", LR_PASS);
		return LR_PASS;
	}
	else
	{
		lr_fail_trans_with_error( lr_eval_string("T05_Add_To_Cart_Bopis S02_createOrUpdateBOPISOrder Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
		lr_end_transaction("T05_Add_To_Cart_Bopis", LR_FAIL);
		//lr_abort();
		return LR_FAIL;
	}
}




int addBopisToCartR3()
{
	int i, itemIsAvailableInStore = 0;

	if ( strcmp( lr_eval_string("{storeId}") , "10151") == 0 )
	{
//		if ( strcmp( lr_eval_string("{TCPProductInd_1}") , "Clearance") != 0 && strcmp( lr_eval_string("{TCPProductInd_count}") , "1") == 0 && strcmp( lr_eval_string("{TCPWebOnlyFlag_count}") , "1") == 0 )
//		{
			lr_think_time ( LINK_TT ) ;
			lr_start_transaction("T05_Add_To_Cart_Bopis");

			lr_save_string( lr_eval_string("{atc_catentryId}"), "catentryId");
			lr_save_string( "40.8136765", "storeLocLatitude");
			lr_save_string( "-74.08288390000001", "storeLocLongitude");
			web_add_header( "X-Requested-With", "XMLHttpRequest");
			//{"errorCode":"GENERIC","exceptionData":"detailedErrorInfo:ERROR: EXCEPTION OCCURED WHILE GETTING CLOSEST STORES AND PRODUCT INVENTORY INFO (TCPStoreInfoAndOneItemDOMInventoryDataBean) : GETTING CLOSEST STORES AND PRODUCT INVENTORY INFO : requestIdentifier = distance = 10  latitude = 40.9541697  longitude = -74.06526159999999  maxStores = 3  storeId = 10151  catentryId = 779909 : requestProperties = catalogId=10551\nviewTaskName=GetStoreAndProductInventoryInfo\ncatentryId=779909\nlangId=-1\nstoreId=10151\nlatitude=40.9541697\nsType=BOPIS\nlongitude=-74.06526159999999\n\nstoreInfoServiceResponseJSON = {\"errorCode\":\"GENERIC_ERROR\",\"exceptionData\":\"Got error while calling inventory service.\"
			//,\"errorMessage\":\"Sorry, we're unable to complete your reservation at this time. Please try again later.\"}\navailabilityInfoJSON = {\"errorCode\":\"GENERIC_ERROR\",\"exceptionData\":\"Got error while calling inventory service.\",\"errorMessage\":\"Sorry, we're unable to complete your reservation at this time. Please try again later.\"}\n -- The value for key: [result] was null.  Object required. -- org.apache.commons.json.JSONException: The value for key: [result] was null.  Object required. -- Request Properties:catalogId=10551\nviewTaskName=GetStoreAndProductInventoryInfo\ncatentryId=779909\nlangId=-1\nstoreId=10151\nlatitude=40.9541697\nsType=BOPIS\nlongitude=-74.06526159999999\n"
			//,"errorMessage":"Sorry, we're unable to complete your reservation at this time. Please try again later."}

			web_reg_save_param("storeLocId", "LB=\"storeUniqueID\":\"", "RB=\"", "NotFound=Warning", "ORD=ALL",LAST); //"storeUniqueID":"
			web_reg_save_param("itemStatus", "LB=\"itemStatus\":\"", "RB=\"", "NotFound=Warning", "ORD=ALL",LAST); //"itemStatus":"
			web_reg_save_param("errorCode", "LB=\"errorCode\":\"", "RB=\"", "NotFound=Warning", LAST); //"errorCode":"0"
			web_reg_save_param("errorMessage", "LB=\"errorMessage\":\"", "RB=\"", "NotFound=Warning", LAST); //"errorMessage":"Invalid Parameters"

			//Search Store, this should get us the storeLocId
			lr_start_sub_transaction("T05_Add_To_Cart_Bopis S01_GetStoreAndProductInventoryInfo", "T05_Add_To_Cart_Bopis" );
			web_custom_request("GetStoreAndProductInventoryInfo",
//"URL=https://tcp-perf.childrensplace.com/webapp/wcs/stores/servlet/GetStoreAndProductInventoryInfo?storeId=10151&catalogId=10551&langId=-1&catentryId=671505&latitude=40.8136765&longitude=-74.08288390000001&sType=BOPIS
//"URL=https://uatlive3.childrensplace.com/webapp/wcs/stores/servlet/GetStoreAndProductInventoryInfo?storeId=10151&catalogId=10551&langId=-1&catentryId=120561&latitude=40.782392&longitude=-74.0711359&sType=BOPIS",
				"URL=https://{host}/webapp/wcs/stores/servlet/GetStoreAndProductInventoryInfo?storeId={storeId}&catalogId={catalogId}&langId=-1&catentryId={catentryId}&latitude={storeLocLatitude}&longitude={storeLocLongitude}&sType=BOPIS",
				"Method=GET",
				"TargetFrame=",
				"Resource=0",
				"RecContentType=text/html",
				"Mode=HTML",
				"EncType=application/json",
				LAST);
			lr_end_sub_transaction("T05_Add_To_Cart_Bopis S01_GetStoreAndProductInventoryInfo",LR_AUTO);


			for (i=1; i <= atoi(lr_eval_string("{itemStatus_count}")); i++)
			{
				if (strcmp(lr_paramarr_idx("itemStatus",i),"AVAILABLE") == 0)
				{
					lr_save_string( lr_paramarr_idx("storeLocId", i ), "storeLocId");
					itemIsAvailableInStore = 1;
					break;
				}
			}

//			if (itemIsAvailableInStore == 0)
//			{
//			lr_start_sub_transaction("T05_Add_To_Cart_Bopis FixedStoreLocAndItem", "T05_Add_To_Cart_Bopis" );
				lr_save_string( "787752", "catentryId");
				lr_save_string( "114298", "storeLocId");
//				lr_save_string( "671505", "catentryId");
//				lr_save_string( "110600", "storeLocId");
//			lr_end_sub_transaction("T05_Add_To_Cart_Bopis FixedStoreLocAndItem",LR_AUTO);
//			}

			web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", LAST); //"errorCode": "DEFAULT_ERROR",\n
			web_reg_save_param("errorMessage", "LB=\"errorMsg\": \"", "RB=\"", "NotFound=Warning", LAST); //"errorMsg": "We're sorry we 're unable to complete your order at this time.Plea

			lr_start_sub_transaction("T05_Add_To_Cart_Bopis S02_createOrUpdateBOPISOrder", "T05_Add_To_Cart_Bopis" );
			web_custom_request("createOrUpdateBOPISOrder",
				"URL=https://{host}/wcs/resources/store/{storeId}/bopisOrder/createOrUpdateBOPISOrder?storeId={storeId}&catalogId={catalogId}&langId=-1&storeLocId={storeLocId}&quantity=1&requesttype=ajax&isRest=true&catEntryId={catentryId}",
				"Method=POST",
				"TargetFrame=",
				"Resource=0",
				"RecContentType=application/json",
				"Mode=HTML",
				"EncType=application/json; charset=UTF-8",
				"Body={\"storeId\":\"{storeId}\",\"catalogId\":\"{catalogId}\",\"langId\":\"-1\",\"storeLocId\":\"{storeLocId}\",\"quantity\":\"1\",\"requesttype\":\"ajax\",\"isRest\":true,\"catEntryId\":\"{catentryId}\"}",
				LAST);
			lr_end_sub_transaction("T05_Add_To_Cart_Bopis S02_createOrUpdateBOPISOrder",LR_AUTO);

			lr_start_sub_transaction("T05_Add_To_Cart_Bopis S03_TCPAdd2CartQuickView", "T05_Add_To_Cart_Bopis" );
			web_custom_request("TCPAdd2CartQuickView",
				"URL=https://{host}/webapp/wcs/stores/servlet/TCPAdd2CartQuickView?langId=-1&storeId={storeId}&catalogId={catalogId}&storeId={storeId}&catalogId={catalogId}&langId=-1",
				"Method=GET",
				"TargetFrame=",
				"Resource=0",
				"RecContentType=text/html",
				"Mode=HTML",
				"EncType=application/json",
				LAST);
			lr_end_sub_transaction("T05_Add_To_Cart_Bopis S03_TCPAdd2CartQuickView",LR_AUTO);

			lr_start_sub_transaction("T05_Add_To_Cart_Bopis S04_CreateCookieCmd", "T05_Add_To_Cart_Bopis" );
			web_custom_request("CreateCookieCmd",
				"URL=https://{host}/webapp/wcs/stores/servlet/CreateCookieCmd?langId=-1&storeId={storeId}&goToPage=NoURL&catalogId={catalogId}&storeId={storeId}&catalogId={catalogId}&langId=-1",
				"Method=GET",
				"TargetFrame=",
				"Resource=0",
				"RecContentType=text/html",
				"Mode=HTML",
				"EncType=application/json",
				LAST);
			lr_end_sub_transaction("T05_Add_To_Cart_Bopis S04_CreateCookieCmd",LR_AUTO);

			if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 ) //if no error code and message
			{	lr_end_transaction("T05_Add_To_Cart_Bopis", LR_PASS);
				return LR_PASS;
			}
			else
			{
				lr_fail_trans_with_error( lr_eval_string("T05_Add_To_Cart_Bopis S02_createOrUpdateBOPISOrder Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
				lr_end_transaction("T05_Add_To_Cart_Bopis", LR_FAIL);
				return LR_FAIL;
			}
//		}
	}
	return 0;
}

int reserveOnline()
{
 	lr_think_time ( LINK_TT ) ;

	lr_start_transaction("T04_Product_Reserve_Online_FIND_IT");

	web_url("GetStoreAndProductInventoryInfo",
		"URL=https://{host}/webapp/wcs/stores/servlet/GetStoreAndProductInventoryInfo?storeId=10151&catalogId=10551&langId=-1&catentryId={catentryId}&latitude=33.9697897&longitude=-118.24681479999998",
//		"URL=http://{host}/webapp/wcs/stores/servlet/GetStoreAndProductInventoryInfo?storeId={storeId}&catalogId={catalogId}&langId=-1&catentryId={catentryId}&latitude={storeLocatorLatitude}&longitude={storeLocatorLongitude}",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		LAST);

	lr_end_transaction("T04_Product_Reserve_Online_FIND_IT",LR_AUTO);

 	lr_think_time ( LINK_TT ) ;

	web_reg_save_param("errorCode", "LB=\"errorCode\":\"", "RB=\"", "NotFound=Warning", LAST); //"errorCode":"0"
	web_reg_save_param("errorMessage", "LB=\"errorMessage\":\"", "RB=\"", "NotFound=Warning", LAST); //"errorMessage":"Invalid Parameters"


	lr_start_transaction("T04_Product_Reserve_Online_Submit");
	web_submit_data("TCPReservationSubmitCmd",
		"Action=https://{host}/webapp/wcs/stores/servlet/TCPReservationSubmitCmd",
		"Method=POST",
		"EncType=multipart/form-data",
		"RecContentType=text/html",
		"Mode=HTTP",
		ITEMDATA,
		"Name=quantity", "Value=1", ENDITEM,
		"Name=catEntryId", "Value={catentryId}", ENDITEM,
		"Name=stlocId", "Value=111966", ENDITEM,
		"Name=firstname", "Value=manny", ENDITEM,
		"Name=lastname", "Value=paquiao", ENDITEM,
		"Name=phone", "Value=2014531236", ENDITEM,
		"Name=email", "Value={userEmail}", ENDITEM,
		"Name=marketingEmail", "Value=1", ENDITEM,
		"Name=mobileSignup", "Value=0", ENDITEM,
		"Name=storeId", "Value={storeId}", ENDITEM,
		"Name=catalogId", "Value={catalogId}", ENDITEM,
		"Name=langId", "Value=-1", ENDITEM,
		LAST);
/*
	web_reg_save_param("reservationOrderID", "LB=reservationOrderId\": \"", "RB=\",", "NotFound=Warning", LAST);
	web_url("TCPReservationSubmitCmd",  //OLD
		"URL=http://{host}/webapp/wcs/stores/servlet/TCPReservationSubmitCmd?storeId={storeId}&catalogId={catalogId}&langId=-1&quantity=1&catEntryId={catentryId}&stlocId={storeUniqueID}&firstname=manny&lastname=Paquiao&phone=2014537616&email={userEmail}&optInEmail&optInEmail=true",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		LAST);

	if(strlen(lr_eval_string("{reservationOrderID}")) == 9)
		lr_end_transaction("T04_Product_Reserve_Online_Submit",LR_PASS);
	else
		lr_end_transaction("T04_Product_Reserve_Online_Submit",LR_FAIL);
*/
	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 ) //if no error code and message
	{	lr_end_transaction("T04_Product_Reserve_Online_Submit", LR_PASS);
		return LR_PASS;
	}
	else
	{
		lr_fail_trans_with_error( lr_eval_string("T04_Product_Reserve_Online_Submit Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
		lr_end_transaction("T04_Product_Reserve_Online_Submit", LR_FAIL);
		return LR_FAIL;
	}

}

void inCartEdits()
{
	viewCart();

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_UPDATE_QUANTITY )
		updateQuantity();  //if ( atoi( lr_eval_string("{orderItemIds_count}") ) != 0 ) {

//	if ( strcmp( lr_eval_string("{couponExist}"), "FORTYOFF") != 0 && strcmp( lr_eval_string("{couponExist}"), "TESTCODE20") != 0 )
//	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_PROMO_APPLY && (strcmp( lr_eval_string("{couponExist}"), "FORTYOFF") != 0 && strcmp( lr_eval_string("{couponExist}"), "TESTCODE20") != 0) )
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_PROMO_APPLY && (atoi( lr_eval_string("{couponFortyOffApplied}")) == 0 ) )
			applyPromoCode(0);

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_DELETE_PROMOCODE )
		deletePromoCode();
/*
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_DELETE_ITEM )
			deleteItem(); //delete item from cart
*/
	if (isLoggedIn == 1) {

		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= MOVE_FROM_CART_TO_WISHLIST  ) {
			moveFromCartToWishlist();

//			if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= MOVE_FROM_WISHLIST_TO_CART )
//				moveFromWishlistToCart();
		}
	}

	viewCart();

	return;
}


void wlGetProduct()
{
	drill();

	productDisplay();
}

void wlFavorites()
{
	int j;
 	lr_think_time ( LINK_TT ) ;

	lr_start_transaction("T19_Favorites");

	web_url("favorites",
		"URL=https://{host}{store_home_page}favorites",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		LAST);

	lr_save_string("T19_Favorites", "mainTransaction");
//	tcp_api2("tcporder/getAllCoupons", "GET",  lr_eval_string("{mainTransaction}") );
//	tcp_api2("tcpproduct/getPriceDetails", "GET",  lr_eval_string("{mainTransaction}") );

	if (ESPOT_FLAG == 1)
	{
		web_add_header("deviceType", "desktop");
		web_add_header("espotName", "GlobalHeaderBannerAboveHeader,GlobalHeaderBannerWithinHeader,GlobalHeaderLogo,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,create_account_drawer,MinicartLoginBanner,MinicartRegisterBanner,drawer_myaccount_overview_banner,coupon_help,bag_empty_banner,drawer_empty_bag,MinicartLoginFromFavoritesBanner,bag_ledger_promo_banner,gift_services_espot,CheckoutConfirmationBanner,CheckoutConfirmationMPRPromo,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1");
		tcp_api2("getESpot", "GET", lr_eval_string("{mainTransaction}") );

		web_add_header("deviceType", "desktop");
		web_add_header("espotName", "DTEmptyFavorites");
		tcp_api2("getESpot", "GET", lr_eval_string("{mainTransaction}") );

		web_add_header("deviceType", "desktop");
		web_add_header("espotName", "GlobalHeaderBannerAboveHeader,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1");
		tcp_api2("getESpot", "GET", lr_eval_string("{mainTransaction}") );
	}
	//web_add_header("action", "get"); //Removed by PavanDusi 111617
	//tcp_api2("tcporder/getFavouriteStoreLocation", "GET",  lr_eval_string("{mainTransaction}") );              //Removed by PavanDusi 111617
	web_add_header("locStore", "False");
	web_add_header("pageName", "orderSummary");
	web_reg_save_param("cartCount", "LB=artCount\": ", "RB=,", "NotFound=Warning", LAST);
	tcp_api2("getOrderDetails", "GET", lr_eval_string("{mainTransaction}") );
	tcp_api2("payment/getRegisteredUserDetailsInfo", "GET",  lr_eval_string("{mainTransaction}") );
	//tcp_api2("tcporder/getXAppConfigValues", "GET", lr_eval_string("{mainTransaction}") );

	web_reg_save_param("nameIdentifier", "LB=\"nameIdentifier\": \"", "RB=\"", "ORD=All", "NotFound=Warning", LAST); //"nameIdentifier": "Perf_955128650_WL",
	web_reg_save_param("status", "LB=\"status\": \"", "RB=\"", "ORD=All", "NotFound=Warning", LAST); //"status": "Active",
	web_reg_save_param("giftListExternalIdentifier", "LB=\"giftListExternalIdentifier\": ", "RB=,", "ORD=All", "NotFound=Warning", LAST); //"giftListExternalIdentifier": 210001,
	web_reg_save_param("itemCount", "LB=\"itemCount\": ", "RB=,", "NotFound=Warning", LAST); //"giftListExternalIdentifier": 210001,
	tcp_api2("tcpstore/getListofWishList", "GET",  lr_eval_string("{mainTransaction}") );

//	if ( atoi( lr_eval_string("{itemCount}") ) != 0 ) {
//		web_add_header("externalId", lr_paramarr_idx("giftListExternalIdentifier", 1) );
		web_reg_save_param("giftListItemID", "LB=\"giftListItemID\": \"", "RB=\"", "ORD=All", "NotFound=Warning", LAST); //"giftListItemID": "232010",
		web_reg_save_param("productCatentryId", "LB=\"parentProductId\": \"", "RB=\"", "ORD=All", "NotFound=Warning", LAST); //parentProductId": "614085",\
		web_reg_save_param("productId", "LB=\"productId\": \"", "RB=\"", "ORD=All", "NotFound=Warning", LAST); //productId": "806929",\
		tcp_api2("tcpproduct/getWishListbyId", "GET", lr_eval_string("{mainTransaction}") );

//	}
	lr_end_transaction("T19_Favorites", LR_AUTO);

}

void wlGetAll()
{
	if (lr_paramarr_len("WishlistIDs") == 0)  {//no need to call this again if it already has values
	//  giftListExternalIdentifier\": 2001,
		web_reg_save_param("WishlistIDs", "LB=\"giftListExternalIdentifier\": ", "RB=,", "ORD=All", "NotFound=Warning", LAST);
	//	web_reg_save_param("WishlistName", "LB=\"nameIdentifier\": \"", "RB=,", "ORD=All", LAST);
	//	web_reg_save_param("WishlistItemCount", "LB=\"itemCount\": ", "RB=,", "ORD=All", LAST);
	// 	lr_think_time (1) ;

		//================================================
		//Get all wish-lists for users
		//================================================
		lr_start_transaction("T19_Wishlist Get List");

			TCPGetWishListForUsersView();

			if (lr_paramarr_len("WishlistIDs") == 0)  {//confirmed NOT login, giftListExternalIdentifier is NOT found in the response

			web_reg_save_param("WishlistIDs", "LB=\"giftListId\": ", "RB=,", "ORD=All", "NotFound=Warning", LAST);
		//http://tcp-perf.childrensplace.com/webapp/wcs/stores/servlet/AjaxGiftListServiceCreate?tcpCallBack=jQuery111309054115856997669_1446156866892&registryAccessPreference=Public&requesttype=ajax&storeId=10151&catalogId=10551&langId=-1&name=Joe%27s+Wishlist&_=1446156866894
			web_url("AjaxGiftListServiceCreate",
				"URL=http://{host}/webapp/wcs/stores/servlet/AjaxGiftListServiceCreate?tcpCallBack=jQuery111309054115856997669_1446156866892&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&name=Joe%27s+Wishlist&_=1446156866894",
				"TargetFrame=",
				"Resource=0",
				"RecContentType=text/html",
				"Referer=",
				"Snapshot=t21.inf",
				"Mode=HTML",
				LAST);

			if (lr_paramarr_len("WishlistIDs") == 0)  //confirmed NOT login, giftListExternalIdentifier is NOT found in the response
				lr_end_transaction("T19_Wishlist Get List", LR_FAIL);
			else
				lr_end_transaction("T19_Wishlist Get List", LR_PASS);
		}
		else
			lr_end_transaction("T19_Wishlist Get List", LR_PASS);

		if (lr_paramarr_len("WishlistIDs") > 0)
			lr_save_string(lr_paramarr_random("WishlistIDs"), "WishlistID");
	}

}//wlGetAll()


void wlCreate()
{
	wlFavorites();

 	lr_think_time ( LINK_TT ) ;

	if (lr_paramarr_len("giftListExternalIdentifier") < 5)  {
	//================================================
	//Create a wish-list -
	//===============================================
		lr_start_transaction("T19_Wishlist Create");

			addHeader();
			web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", LAST); //"errorCode": "",\n
			web_reg_save_param("errorMessage", "LB=\"errorMessage\": \"", "RB=\",\n", "NotFound=Warning", LAST); //"errorMessage": "",\n
			web_reg_find("TEXT/IC=descriptionName","Fail=NotFound", "SaveCount=Mycount",LAST);
			web_custom_request("createWishListForUser",
				"URL=https://{api_host}/tcpproduct/createWishListForUser",
				"Method=POST",
				"Resource=0",
				"RecContentType=application/json",
				"Mode=HTTP",
				"EncType=application/json",
				"Body={\"descriptionName\":\"Perf_{WL_Random_Number}_WL\",\"state\":\"Active\"}",  //take nameIdentifier as the descriptionName and status as state from getListofWishList api
				LAST);

			addHeader();
			web_add_header("externalId", lr_paramarr_idx("giftListExternalIdentifier",1) );
			web_url("getWishListbyId",
				"URL=https://{api_host}/tcpproduct/getWishListbyId",
				"Resource=0",
				"RecContentType=application/json",
				LAST);

			addHeader();
			web_url("getListofWishList",
				"URL=https://{api_host}/tcpstore/getListofWishList",
				"Resource=0",
				"RecContentType=application/json",
				LAST);

		if ( strcmp( lr_eval_string("{errorCode}") ,"") != 0 && strcmp(lr_eval_string("{errorMessage}") ,"") != 0  && (atoi(lr_eval_string("{Mycount}"))==0) )
		{
			lr_fail_trans_with_error( lr_eval_string("T19_Wishlist Create Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_transaction("T19_Wishlist Create", LR_FAIL);
		}
		else
		{
			lr_end_transaction("T19_Wishlist Create", LR_AUTO);
		}
	}

}

void wlDelete()
{
	wlFavorites();

 	lr_think_time ( LINK_TT ) ;
	if (lr_paramarr_len("giftListExternalIdentifier") > 1)  {
	//================================================
	//Delete a wish-list
	//================================================
		lr_start_transaction("T19_Wishlist Delete");

			addHeader();
			web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", LAST); //"errorCode": "",\n
			web_reg_save_param("errorMessage", "LB=\"errorMessage\": \"", "RB=\",\n", "NotFound=Warning", LAST); //"errorMessage": "",\n
			web_reg_find("TEXT/IC=uniqueID","Fail=NotFound", "SaveCount=Mycount",LAST);
			web_add_header("externalId", lr_paramarr_idx("giftListExternalIdentifier",1) );
			web_custom_request("deleteWishListForUser",
				"URL=https://{api_host}/tcpproduct/deleteWishListForUser",
				"Method=DELETE",
				"Resource=0",
				"RecContentType=application/json",
				"Mode=HTTP",
				LAST);

			addHeader();
			web_url("getListofWishList",
				"URL=https://{api_host}/tcpstore/getListofWishList",
				"Resource=0",
				"RecContentType=application/json",
				LAST);

			addHeader();
			web_add_header("externalId", lr_paramarr_idx("giftListExternalIdentifier",2) );
			web_url("getWishListbyId",
				"URL=https://{api_host}/tcpproduct/getWishListbyId",
				"Resource=0",
				"RecContentType=application/json",
				LAST);

		if ( strcmp( lr_eval_string("{errorCode}") ,"") != 0 && strcmp(lr_eval_string("{errorMessage}") ,"") != 0  && (atoi(lr_eval_string("{Mycount}"))==0) )
		{
			lr_fail_trans_with_error( lr_eval_string("T19_Wishlist Delete Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_transaction("T19_Wishlist Delete", LR_FAIL);
		}
		else
		{
			lr_end_transaction("T19_Wishlist Delete", LR_AUTO);
		}
	}
}

void wlChange()
{
	wlFavorites();

 	lr_think_time ( LINK_TT ) ;
	//================================================
	//Change wish-list name
	//================================================
	if (lr_paramarr_len("giftListExternalIdentifier") > 0)  {

		lr_start_transaction("T19_Wishlist Change Name");

			web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", LAST); //"errorCode": "",\n
			web_reg_save_param("errorMessage", "LB=\"errorMessage\": \"", "RB=\",\n", "NotFound=Warning", LAST); //"errorMessage": "",\n

			addHeader();
			web_add_header("externalId", lr_paramarr_idx("giftListExternalIdentifier",1) );
			//web_add_header("deviceType", "desktop" );
			web_add_header("Content-Type", "application/json" );
			//lr_save_string( lr_paramarr_idx("status",1), "Status" );
			web_reg_find("TEXT/IC=descriptionName", "SaveCount=Mycount",LAST);

			web_custom_request("updateWishListForUser",
				"URL=https://{api_host}/tcpproduct/updateWishListForUser",
				"Method=PUT",
				"Resource=0",
				"RecContentType=application/json",
				"Mode=HTTP",
				"EncType=application/json",
				"Body={\"descriptionName\":\"Perf_{WL_Random_Number}_Change\",\"state\":\"Default\"}",  //take nameIdentifier as the descriptionName and status as state from getListofWishList api
				LAST);

			addHeader();
			web_url("getListofWishList",
				"URL=https://{api_host}/tcpstore/getListofWishList",
				"Resource=0",
				"RecContentType=application/json",
				LAST);

			addHeader();
			web_add_header("externalId", lr_paramarr_idx("giftListExternalIdentifier",1) );
			web_url("getWishListbyId",
				"URL=https://{api_host}/tcpproduct/getWishListbyId",
				"Resource=0",
				"RecContentType=application/json",
				LAST);

//		if ( strcmp( lr_eval_string("{errorCode}") ,"") != 0 && strcmp(lr_eval_string("{errorMessage}") ,"") != 0  && (atoi(lr_eval_string("{Mycount}"))==0) )
		if ( atoi(lr_eval_string("{Mycount}")) == 0 )
		{
			lr_fail_trans_with_error( lr_eval_string("T19_Wishlist Change Name Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_transaction("T19_Wishlist Change Name", LR_FAIL);
		}
		else
		{
			lr_end_transaction("T19_Wishlist Change Name", LR_AUTO);
		}
	}
}



void wlAddItem()  // This is already covered by PDP_to_WL
{
	lr_think_time ( LINK_TT ) ;
	wlGetAll(); //Get all wish-lists for users

	target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_CART_SIZE}"));

	//lr_message("target_itemsInCart = %d", target_itemsInCart );

//	for(index_buildCart=0; index_buildCart < target_itemsInCart ; index_buildCart++)
//	{
		wlGetProduct();

		if (lr_paramarr_len("atc_catentryIds") > 0) {

			lr_save_string( lr_paramarr_random( "atc_catentryIds" ), "atc_catentryId");

			//================================================
			//Add items to a wish-list
			//================================================

			lr_start_transaction("T19_Wishlist Add Item");

			web_url("AjaxGiftListServiceAddItem", //Add item to wish-lists
			"URL=http://{host}/webapp/wcs/stores/servlet/AjaxGiftListServiceAddItem?&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&tcpCallBack=jQuery111301397454545367509_1440779623033&giftListId={WishlistID}&catEntryId_1={atc_catentryId}&quantity_1=1&_=1440779623035",
				"TargetFrame=",
				"Resource=0",
				"RecContentType=text/html",
				"Referer=",
				"Snapshot=t21.inf",
				"Mode=HTML",
				LAST);

			lr_end_transaction("T19_Wishlist Add Item", LR_AUTO);

		} // end for loop to add o cart

//	}
//Request URL:https://uatlive1.childrensplace.com/api/addOrUpdateWishlist
//Request Method:PUT
}

void wlGetItems()
{
//	wlFavorites();

 	lr_think_time ( LINK_TT ) ;

	if (atoi( lr_eval_string("{WishlistIDs_count}")) > 0 )
	{
		web_reg_save_param("catEntryIds", "LB=\"itemId\": \"", "RB=\"", "NotFound=Warning", "Ord=All", LAST);
		web_reg_save_param("wishListItemIds", "LB=\"wishListItemId\": \"", "RB=\"", "NotFound=Warning", "Ord=All", LAST);
		lr_save_string ( lr_paramarr_random( "WishlistIDs") , "WishlistID" ) ;
		//================================================
		//Get wish-list items for a selected wish-list
		//================================================

		lr_start_transaction("T19_Wishlist Get Items");

		web_url("TCPGetWishListItemsForSelectedListView", //Get all wish-list Items - This works for changing WL, jquery value does not matter, need to give the ListId
			"URL=http://{host}/webapp/wcs/stores/servlet/TCPGetWishListItemsForSelectedListView?&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&wishListId={WishlistID}&tcpCallBack=jQuery111308157584751024842_1440519437271&_=1440519437287",
			"TargetFrame=",
			"Resource=0",
			"RecContentType=text/html",
			"Referer=",
			"Snapshot=t21.inf",
			"Mode=HTML",
			LAST);

		lr_end_transaction("T19_Wishlist Get Items", LR_AUTO);
	}
}

void wlAddToCart()
{
	int k, productCount = 0;
	wlFavorites();

 	lr_think_time ( LINK_TT ) ;
	//lr_continue_on_error(0);
	if ( lr_paramarr_len ( "productCatentryId" ) > 0 )
	{
		if ( lr_paramarr_len ( "productCatentryId" ) > 1 )
		{	productCount = atoi( lr_eval_string("{productCatentryId_count}"))-1;
			index = rand ( ) % productCount + 1 ;
			lr_save_string(lr_paramarr_idx("productCatentryId", index), "productCatentryId");
			lr_save_string(lr_paramarr_idx("giftListItemID", index), "giftListItemID");
		}
		else {
			lr_save_string(lr_paramarr_random("productCatentryId"), "productCatentryId");
			lr_save_string(lr_paramarr_random("giftListItemID"), "giftListItemID");
		}

		lr_start_transaction ( "T05_Add To Cart_From Wishlist" ) ;

		lr_start_sub_transaction("T05_Add To Cart_From WishList_getBundleIdAndBundleDetails" , "T05_Add To Cart_From Wishlist");
		addHeader();
		web_add_header("productCatentryId", lr_eval_string("{productCatentryId}") );
		web_reg_find("TEXT/IC=recordSetTotalMatches", "SaveCount=apiCheck", LAST);
		web_url("getBundleIdAndBundleDetails",
			"URL=https://{api_host}/tcpproduct/getBundleIdAndBundleDetails",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction ("T05_Add To Cart_From WishList_getBundleIdAndBundleDetails", LR_FAIL);
		else
			lr_end_sub_transaction ("T05_Add To Cart_From WishList_getBundleIdAndBundleDetails", LR_AUTO);

		lr_start_sub_transaction("T05_Add To Cart_From WishList_getSKUInventoryandProductCounterDetails" , "T05_Add To Cart_From Wishlist");
		addHeader();
		web_add_header("productId", lr_eval_string("{productCatentryId}")); //"689891" );
		web_reg_save_param("atc_catentryIds", "LB=catentryId\": \"", "RB=\",", "ORD=All", "NotFound=Warning", LAST); //"catentryId": "735396",
		web_reg_save_param("atc_quantity"  ,  "LB=quantity\": \"", "RB=.0\",", "ORD=All", "NotFound=Warning", LAST); //"quantity": "99999.0",
		web_reg_find("TEXT/IC=getAllSKUInventoryByProductId", "SaveCount=apiCheck", LAST);
		web_url("getSKUInventoryandProductCounterDetails",
			"URL=https://{api_host}/tcpproduct/getSKUInventoryandProductCounterDetails",
			"Resource=0",
			"RecContentType=application/json",
			LAST);

		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction ("T05_Add To Cart_From WishList_getSKUInventoryandProductCounterDetails", LR_FAIL);
		else
			lr_end_sub_transaction ("T05_Add To Cart_From WishList_getSKUInventoryandProductCounterDetails", LR_AUTO);

		if ( lr_paramarr_len ( "atc_catentryIds" ) > 0 )
		{
			for (k = 1; k<= atoi(lr_eval_string("{atc_catentryIds_count}")); k++ )
			{
				if (atoi( lr_paramarr_idx("atc_quantity", k) ) != 0)
//				if (atoi( lr_paramarr_idx("atc_quantity", k) ) != 0 && strlen(lr_paramarr_idx("atc_catentryIds", k)) == 6 )
				{
					lr_save_string(lr_paramarr_idx("atc_catentryIds", k), "atc_catentryId" ); break;
				}
			}
			lr_start_sub_transaction ( "T05_Add To Cart_From Wishlist_addProductToCart", "T05_Add To Cart_From Wishlist");
			web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", LAST); //"errorCode": "",\n
			web_reg_save_param("errorMessage", "LB=\"errorMessage\": \"", "RB=\",\n", "NotFound=Warning", LAST); //"errorMessage": "",\n
			web_reg_save_param_json("ParamName=orderId", "QueryString=$.orderId.*", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
			web_reg_save_param_json("ParamName=orderItemId", "QueryString=$.orderItemId.*", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
			addHeader();
			web_custom_request("addProductToCart",
				"URL=https://{api_host}/tcpproduct/addProductToCart",
				"Method=POST",
				"Resource=0",
				"RecContentType=application/json",
				"Mode=HTTP",
				"EncType=application/json",
	//			"Body={\"calculationUsage[]\":\"-7\",\"storeId\":\"{storeId}\",\"catalogId\":\"{catalogId}\",\"langId\":\"-1\",\"orderId\":\".\",\"field2\":\"0\",\"requesttype\":\"ajax\",\"catEntryId\":\"{atc_catentryId}\",\"quantity\":1}",
				"Body={\"calculationUsage[]\":\"-7\",\"storeId\":\"{storeId}\",\"catalogId\":\"{catalogId}\",\"langId\":\"-1\",\"orderId\":\".\",\"field2\":\"0\",\"requesttype\":\"ajax\",\"catEntryId\":\"{atc_catentryId}\",\"quantity\":1,\"externalId\":\"{giftListItemId}\"}",
				LAST);

			if ( strlen(lr_eval_string("{orderId}")) <= 0 )
			{
				//atc_Stat = 1; //0-Pass 1-Fail
				lr_fail_trans_with_error( lr_eval_string("T05_Add To Cart_From Wishlist_addProductToCart - Failed with  Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
				lr_end_sub_transaction ("T05_Add To Cart_From Wishlist_addProductToCart", LR_FAIL) ;
				lr_end_transaction ( "T05_Add To Cart_From Wishlist" , LR_FAIL ) ;
				return;
			} // end if

			lr_end_sub_transaction ("T05_Add To Cart_From Wishlist_addProductToCart", LR_AUTO);

			lr_start_sub_transaction("T05_Add To Cart_From Wishlist_getOrderDetails" , "T05_Add To Cart_From Wishlist");
			addHeader();
			web_add_header("calc", "false");
			web_add_header("locStore", "True");
			web_add_header("pageName", "fullOrderInfo");
			web_reg_save_param("cartCount", "LB=artCount\": ", "RB=,", "NotFound=Warning", LAST);			//"CartCount": 1,\n
			web_url("getOrderDetails",
				"URL=https://{api_host}/getOrderDetails",
				"Resource=0",
				"RecContentType=application/json",
				LAST);
			lr_end_sub_transaction ("T05_Add To Cart_From Wishlist_getOrderDetails", LR_AUTO);

			lr_save_string("T05_Add To Cart", "mainTransaction");
			//tcp_api2("tcporder/getAllCoupons", "GET",  "T05_Add To Cart_From Wishlist" );
			//tcp_api2("tcpproduct/getPriceDetails", "GET",  "T05_Add To Cart_From Wishlist" );
			tcp_api2("payment/getPointsService", "GET",  "T05_Add To Cart_From Wishlist" );

			lr_end_transaction ( "T05_Add To Cart_From Wishlist" , LR_AUTO ) ;
		}
	}
	else
	{
		//lr_continue_on_error(1);
		atc_Stat = 1; //0-Pass 1-Fail
		lr_start_transaction ( "T41_Add To Cart" ) ;
		lr_end_transaction ( "T41_Add To Cart" , LR_FAIL ) ;
		return;
	}

}

void wlDeleteItem()
{
//	wlFavorites();

 	lr_think_time ( LINK_TT ) ;

	//================================================
	//Delete items from wish-list
	//================================================
//	if ( lr_paramarr_len("giftListItemID") > 0 ) { //if there is any item to delete
	if ( atoi( lr_eval_string("{itemCount}") ) != 0 ) { //if there is any item to delete

//		lr_save_string(lr_paramarr_random("wishListItemIds"), "wishListItemId");

		lr_start_transaction("T19_Wishlist Delete Item");

			addHeader();
			web_add_header("externalId", lr_paramarr_idx("giftListExternalIdentifier",1) );
			web_url("getWishListbyId",
				"URL=https://{api_host}/tcpproduct/getWishListbyId",
				"Resource=0",
				"RecContentType=application/json",
				LAST);


			addHeader();
			web_url("getListofWishList",
				"URL=https://{api_host}/tcpstore/getListofWishList",
				"Resource=0",
				"RecContentType=application/json",
				LAST);

			addHeader();
			web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", LAST); //"errorCode": "",\n
			web_reg_save_param("errorMessage", "LB=\"errorMessage\": \"", "RB=\",\n", "NotFound=Warning", LAST); //"errorMessage": "",\n
			//web_reg_find("TEXT/IC=uniqueID","Fail=NotFound", "SaveCount=Mycount",LAST);
			web_add_header("externalId", lr_paramarr_idx("giftListExternalIdentifier",1) );
			web_add_header("itemId", lr_paramarr_idx("giftListItemID",1) );
			web_custom_request("deleteItemFromWishList",
				"URL=https://{api_host}/tcpproduct/deleteItemFromWishList",
				"Method=DELETE",
				"Resource=0",
				"RecContentType=application/json",
				"Mode=HTTP",
				LAST);



		if ( strcmp( lr_eval_string("{errorCode}") ,"") != 0 && strcmp(lr_eval_string("{errorMessage}") ,"") != 0  && (atoi(lr_eval_string("{Mycount}"))==0) )
		{
			lr_fail_trans_with_error( lr_eval_string("T19_Wishlist Delete Item - Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_transaction("T19_Wishlist Delete Item", LR_FAIL);
		}
		else
		{
			lr_end_transaction("T19_Wishlist Delete Item", LR_AUTO);
		}
	}

}

void wlGetCatEntryId()
{
	wlFavorites();
	wlGetAll(); //Get all wish-lists for users
	wlGetItems();

 	lr_think_time ( LINK_TT ) ;
}



void wishList()
{
	int iRandomTask = 0;

	iRandomTask = atoi(lr_eval_string("{RANDOM_PERCENT}"));

	if (iRandomTask <= RATIO_WL_CREATE)
		wlCreate();
	else if (iRandomTask <= RATIO_WL_DELETE + RATIO_WL_CREATE)
		wlDelete();
	else if (iRandomTask <= RATIO_WL_CHANGE + RATIO_WL_CREATE + RATIO_WL_DELETE)
		wlChange();
	else if (iRandomTask <= RATIO_WL_DELETE_ITEM + RATIO_WL_CREATE + RATIO_WL_DELETE + RATIO_WL_CHANGE)
		wlDeleteItem();
//	else if (iRandomTask <= RATIO_WL_ADD_ITEM + RATIO_WL_CREATE + RATIO_WL_DELETE + RATIO_WL_CHANGE + RATIO_WL_DELETE_ITEM)
//		wlAddItem(); //covered from pdp
	else if (iRandomTask <= RATIO_WL_ADD_ITEM + RATIO_WL_CREATE + RATIO_WL_DELETE + RATIO_WL_CHANGE + RATIO_WL_DELETE_ITEM + RATIO_WL_ADD_TO_CART)
		wlAddToCart();

}


void dropCartTransaction()
{
	lr_start_transaction("T20_Abandon_Cart");
	lr_end_transaction("T20_Abandon_Cart", LR_PASS);

	if (atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 60)
		viewCart(); //06/15/17 rhy, to add extra 20k viewcart count
	//return LR_PASS;
}

void getDataPoints()
{

	lr_param_sprintf("LOGIN_PAGE_DROP", "01. LOGIN_PAGE_DROP = %d", LOGIN_PAGE_DROP);
	lr_user_data_point(lr_eval_string("{LOGIN_PAGE_DROP}"), 0);
	lr_param_sprintf("SHIP_PAGE_DROP", "01. SHIP_PAGE_DROP = %d", SHIP_PAGE_DROP);
	lr_user_data_point(lr_eval_string("{SHIP_PAGE_DROP}"), 0);
	lr_param_sprintf("BILL_PAGE_DROP", "01. BILL_PAGE_DROP = %d", BILL_PAGE_DROP);
	lr_user_data_point(lr_eval_string("{BILL_PAGE_DROP}"), 0);

	lr_param_sprintf("RATIO_CHECKOUT_LOGIN", "02. RATIO_CHECKOUT_LOGIN = %d", RATIO_CHECKOUT_LOGIN);
	lr_user_data_point(lr_eval_string("{RATIO_CHECKOUT_LOGIN}"), 0);
	lr_param_sprintf("RATIO_CHECKOUT_GUEST", "02. RATIO_CHECKOUT_GUEST = %d", RATIO_CHECKOUT_GUEST);
	lr_user_data_point(lr_eval_string("{RATIO_CHECKOUT_GUEST}"), 0);


	lr_param_sprintf("RATIO_DROP_CART", "03. RATIO_DROP_CART = %d", RATIO_DROP_CART);
	lr_user_data_point(lr_eval_string("{RATIO_DROP_CART}"), 0);

	lr_param_sprintf("RATIO_CHECKOUT_LOGIN_FIRST", "04. RATIO_CHECKOUT_LOGIN_FIRST = %d", RATIO_CHECKOUT_LOGIN_FIRST);
	lr_user_data_point(lr_eval_string("{RATIO_CHECKOUT_LOGIN_FIRST}"), 0);

	lr_param_sprintf("RATIO_BUILDCART_DRILLDOWN", "04. RATIO_BUILDCART_DRILLDOWN = %d", RATIO_BUILDCART_DRILLDOWN);
	lr_user_data_point(lr_eval_string("{RATIO_BUILDCART_DRILLDOWN}"), 0);

	lr_param_sprintf("USE_LOW_INVENTORY", "05. USE_LOW_INVENTORY = %d", USE_LOW_INVENTORY);
	lr_user_data_point(lr_eval_string("{USE_LOW_INVENTORY}"), 0);

	lr_param_sprintf("OFFLINE_PLUGIN", "06. OFFLINE_PLUGIN = %d", OFFLINE_PLUGIN);
	lr_user_data_point(lr_eval_string("{OFFLINE_PLUGIN}"), 0);

	lr_param_sprintf("RATIO_PROMO_APPLY", "07. RATIO_PROMO_APPLY = %d", RATIO_PROMO_APPLY);
	lr_user_data_point(lr_eval_string("{RATIO_PROMO_APPLY}"), 0);
	lr_param_sprintf("RATIO_PROMO_MULTIUSE", "07. RATIO_PROMO_MULTIUSE = %d", RATIO_PROMO_MULTIUSE);
	lr_user_data_point(lr_eval_string("{RATIO_PROMO_MULTIUSE}"), 0);
	lr_param_sprintf("RATIO_PROMO_SINGLEUSE", "07. RATIO_PROMO_SINGLEUSE = %d", RATIO_PROMO_SINGLEUSE);
	lr_user_data_point(lr_eval_string("{RATIO_PROMO_SINGLEUSE}"), 0);
	lr_param_sprintf("RATIO_REDEEM_LOYALTY_POINTS", "07. RATIO_REDEEM_LOYALTY_POINTS = %d", RATIO_REDEEM_LOYALTY_POINTS);
	lr_user_data_point(lr_eval_string("{RATIO_REDEEM_LOYALTY_POINTS}"), 0);

	lr_param_sprintf("RATIO_CART_MERGE", "08. RATIO_CART_MERGE = %d", RATIO_CART_MERGE);
	lr_user_data_point(lr_eval_string("{RATIO_CART_MERGE}"), 0);

	lr_param_sprintf("RATIO_REGISTER", "09. RATIO_REGISTER = %d", RATIO_REGISTER);
	lr_user_data_point(lr_eval_string("{RATIO_REGISTER}"), 0);

	lr_param_sprintf("RATIO_UPDATE_QUANTITY", "10. RATIO_UPDATE_QUANTITY = %d", RATIO_UPDATE_QUANTITY);
	lr_user_data_point(lr_eval_string("{RATIO_UPDATE_QUANTITY}"), 0);

	lr_param_sprintf("RATIO_DELETE_ITEM", "11. RATIO_DELETE_ITEM = %d", RATIO_DELETE_ITEM);
	lr_user_data_point(lr_eval_string("{RATIO_DELETE_ITEM}"), 0);

	lr_param_sprintf("RATIO_SELECT_COLOR", "12. RATIO_SELECT_COLOR = %d", RATIO_SELECT_COLOR);
	lr_user_data_point(lr_eval_string("{RATIO_SELECT_COLOR}"), 0);

	lr_param_sprintf("RATIO_RESERVATION_HISTORY", "13. RATIO_RESERVATION_HISTORY = %d", RATIO_RESERVATION_HISTORY);
	lr_user_data_point(lr_eval_string("{RATIO_RESERVATION_HISTORY}"), 0);
	lr_param_sprintf("RATIO_POINTS_HISTORY", "13. RATIO_POINTS_HISTORY = %d", RATIO_POINTS_HISTORY);
	lr_user_data_point(lr_eval_string("{RATIO_POINTS_HISTORY}"), 0);
	lr_param_sprintf("RATIO_ORDER_HISTORY", "13. RATIO_ORDER_HISTORY = %d", RATIO_ORDER_HISTORY);
	lr_user_data_point(lr_eval_string("{RATIO_ORDER_HISTORY}"), 0);
	lr_param_sprintf("RATIO_ORDER_STATUS", "13. RATIO_ORDER_STATUS = %d", RATIO_ORDER_STATUS);
	lr_user_data_point(lr_eval_string("{RATIO_ORDER_STATUS}"), 0);

	lr_param_sprintf("RATIO_WISHLIST", "14. RATIO_WISHLIST = %d", RATIO_WISHLIST);
	lr_user_data_point(lr_eval_string("{RATIO_WISHLIST}"), 0);
	lr_param_sprintf("RATIO_WL_CREATE", "14. RATIO_WL_CREATE = %d", RATIO_WL_CREATE);
	lr_user_data_point(lr_eval_string("{RATIO_WL_CREATE}"), 0);
	lr_param_sprintf("RATIO_WL_DELETE", "14. RATIO_WL_DELETE = %d", RATIO_WL_DELETE);
	lr_user_data_point(lr_eval_string("{RATIO_WL_DELETE}"), 0);
	lr_param_sprintf("RATIO_WL_CHANGE", "14. RATIO_WL_CHANGE = %d", RATIO_WL_CHANGE);
	lr_user_data_point(lr_eval_string("{RATIO_WL_CHANGE}"), 0);
	lr_param_sprintf("RATIO_WL_DELETE_ITEM", "14. RATIO_WL_DELETE_ITEM = %d", RATIO_WL_DELETE_ITEM);
	lr_user_data_point(lr_eval_string("{RATIO_WL_DELETE_ITEM}"), 0);
	lr_param_sprintf("RATIO_WL_ADD_ITEM", "14. RATIO_WL_ADD_ITEM = %d", RATIO_WL_ADD_ITEM);
	lr_user_data_point(lr_eval_string("{RATIO_WL_ADD_ITEM}"), 0);

	lr_param_sprintf("NAV_BROWSE", "15. NAV_BROWSE = %d", NAV_BROWSE);
	lr_user_data_point(lr_eval_string("{NAV_BROWSE}"), 0);
	lr_param_sprintf("NAV_SEARCH", "15. NAV_SEARCH = %d", NAV_SEARCH);
	lr_user_data_point(lr_eval_string("{NAV_SEARCH}"), 0);
	lr_param_sprintf("NAV_CLEARANCE", "15. NAV_CLEARANCE = %d", NAV_CLEARANCE);
	lr_user_data_point(lr_eval_string("{NAV_CLEARANCE}"), 0);
	lr_param_sprintf("NAV_PLACE", "15. NAV_PLACE = %d", NAV_PLACE);
	lr_user_data_point(lr_eval_string("{NAV_PLACE}"), 0);

	lr_param_sprintf("DRILL_ONE_FACET", "16. DRILL_ONE_FACET = %d", DRILL_ONE_FACET);
	lr_user_data_point(lr_eval_string("{DRILL_ONE_FACET}"), 0);
	lr_param_sprintf("DRILL_TWO_FACET", "16. DRILL_TWO_FACET = %d", DRILL_TWO_FACET);
	lr_user_data_point(lr_eval_string("{DRILL_TWO_FACET}"), 0);
	lr_param_sprintf("DRILL_THREE_FACET", "16. DRILL_THREE_FACET = %d", DRILL_THREE_FACET);
	lr_user_data_point(lr_eval_string("{DRILL_THREE_FACET}"), 0);
	lr_param_sprintf("DRILL_SUB_CATEGORY", "16. DRILL_SUB_CATEGORY = %d", DRILL_SUB_CATEGORY);
	lr_user_data_point(lr_eval_string("{DRILL_SUB_CATEGORY}"), 0);

	lr_param_sprintf("APPLY_SORT", "17. APPLY_SORT = %d", APPLY_SORT);
	lr_user_data_point(lr_eval_string("{APPLY_SORT}"), 0);

	lr_param_sprintf("APPLY_PAGINATE", "17. APPLY_PAGINATE = %d", APPLY_PAGINATE);
	lr_user_data_point(lr_eval_string("{APPLY_PAGINATE}"), 0);

	lr_param_sprintf("PDP", "18. PDP = %d", PDP);
	lr_user_data_point(lr_eval_string("{PDP}"), 0);
	lr_param_sprintf("QUICKVIEW", "18. QUICKVIEW = %d", QUICKVIEW);
	lr_user_data_point(lr_eval_string("{QUICKVIEW}"), 0);

	lr_param_sprintf("RATIO_STORE_LOCATOR", "19. RATIO_STORE_LOCATOR = %d", RATIO_STORE_LOCATOR);
	lr_user_data_point(lr_eval_string("{RATIO_STORE_LOCATOR}"), 0);
	lr_param_sprintf("RATIO_SEARCH_SUGGEST", "19. RATIO_SEARCH_SUGGEST = %d", RATIO_SEARCH_SUGGEST);
	lr_user_data_point(lr_eval_string("{RATIO_SEARCH_SUGGEST}"), 0);

	lr_param_sprintf("PRODUCT_QUICKVIEW_SERVICE", "20. PRODUCT_QUICKVIEW_SERVICE = %d", PRODUCT_QUICKVIEW_SERVICE);
	lr_user_data_point(lr_eval_string("{PRODUCT_QUICKVIEW_SERVICE}"), 0);
	lr_param_sprintf("RESERVE_ONLINE", "21. RESERVE_ONLINE = %d", RESERVE_ONLINE);
	lr_user_data_point(lr_eval_string("{RESERVE_ONLINE}"), 0);

	lr_param_sprintf("MOVE_FROM_CART_TO_WISHLIST", "22. MOVE_FROM_CART_TO_WISHLIST = %d", MOVE_FROM_CART_TO_WISHLIST);
	lr_user_data_point(lr_eval_string("{MOVE_FROM_CART_TO_WISHLIST}"), 0);

	lr_param_sprintf("MOVE_FROM_WISHLIST_TO_CART", "22. MOVE_FROM_WISHLIST_TO_CART = %d", MOVE_FROM_WISHLIST_TO_CART);
	lr_user_data_point(lr_eval_string("{MOVE_FROM_WISHLIST_TO_CART}"), 0);

}


int moveFromWishlistToCart()  //this does not exist anymore and was replaced by tcpproduct/addProductToCart
{
	wlFavorites();
	wlGetItems();

	lr_think_time ( LINK_TT );

	if (atoi( lr_eval_string("{wishListItemIds_count}")) > 0 )
	{
		index = rand ( ) % lr_paramarr_len( "wishListItemIds" ) + 1 ;

		lr_save_string ( lr_paramarr_idx( "wishListItemIds" , index ) , "wishListIdToCart" ) ;
		lr_save_string ( lr_paramarr_idx( "catEntryIds" , index ) , "catEntryIdWLToCart" ) ;

		registerErrorCodeCheck();
		//lr_continue_on_error(1);

		lr_start_transaction("T24_MoveFromWishlistToCart");

		lr_start_sub_transaction("T24_MoveFromWishlistToCart_S01_AjaxOrderChangeServiceItemAdd", "T24_MoveFromWishlistToCart");

		web_custom_request("T24_S01_AjaxOrderChangeServiceItemAdd",
			"URL=http://{host}/webapp/wcs/stores/servlet/AjaxOrderChangeServiceItemAdd?tcpCallBack=jQuery111300307750510271938_1473867270738&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&orderId=.&calculationUsage=-1%2C-2%2C-5%2C-6%2C-7&catEntryId={catEntryIdWLToCart}&quantity=1&externalId={wishListIdToCart}&_=1473867270742",
			"Method=GET",
			"Resource=0",
			"RecContentType=text/html",
			"Mode=HTML",
			LAST);

		lr_end_sub_transaction("T24_MoveFromWishlistToCart_S01_AjaxOrderChangeServiceItemAdd", LR_AUTO);

		lr_start_sub_transaction("T24_MoveFromWishlistToCart_S02_CreateCookieCmd", "T24_MoveFromWishlistToCart");

		web_custom_request("T24_S02_CreateCookieCmd",
			"URL=http://{host}/webapp/wcs/stores/servlet/CreateCookieCmd",
			"Method=HEAD",
			"Resource=0",
			"RecContentType=text/html",
			"Mode=HTML",
			LAST);

		lr_end_sub_transaction("T24_MoveFromWishlistToCart_S02_CreateCookieCmd", LR_AUTO);

		lr_start_sub_transaction("T24_MoveFromWishlistToCart_S03_getOrderDetails", "T24_MoveFromWishlistToCart");
/*
		#if OPTIONSENABLED
			call_OPTIONS("getOrderDetails");
		#endif
*/
		addHeader();
		web_add_header("calc", "true");   //0802-4pm
		web_add_header("locStore", "True");
		web_add_header("pageName", "fullOrderInfo");
		web_reg_find("TEXT/IC={", "SaveCount=apiCheck", LAST);
		web_url("getOrderDetails",
			"URL=https://{api_host}/getOrderDetails",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_transaction("T24_MoveFromWishlistToCart_S03_getOrderDetails", LR_FAIL);
		else
			lr_end_transaction("T24_MoveFromWishlistToCart_S03_getOrderDetails", LR_AUTO);

		//lr_continue_on_error(0);

		if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 ) //if no error code and message
		{
			lr_end_transaction ( "T24_MoveFromWishlistToCart", LR_AUTO ) ;
			return LR_PASS;
		}
		else {
			lr_fail_trans_with_error( lr_eval_string ("T24_MoveFromWishlistToCart Failed with Error Code:  \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_transaction ( "T24_MoveFromWishlistToCart", LR_FAIL ) ;
			return LR_FAIL;
		}
	}
	return 0;
}


int moveFromCartToWishlist()
{
	if ( isLoggedIn == 1 ) {
		lr_think_time ( LINK_TT );

		viewCart();
	//
	//	if (atoi( lr_eval_string("{orderItemIds_count}")) > 0 )
		if (atoi( lr_eval_string("{itemCatentryId_count}")) > 0 )
		{

			index = rand ( ) % lr_paramarr_len( "orderItemIds" ) + 1 ;

			lr_save_string ( lr_paramarr_idx( "orderItemIds" , index ) , "orderItemId" ) ;
			lr_save_string ( lr_paramarr_idx( "itemCatentryId" , index ) , "productId" ) ;
			lr_save_string ( lr_paramarr_idx( "itemQuantity" , index ) , "quantity" ) ;

			addHeader();
			web_add_header("Content-Type", "application/json");

			registerErrorCodeCheck();
			//lr_continue_on_error(1);

			lr_start_transaction("T23_MoveFromCartToWishlist");

			lr_start_sub_transaction("T23_MoveFromCartToWishlist_S01_addOrUpdateWishlist", "T23_MoveFromCartToWishlist");

			#if OPTIONSENABLED
				call_OPTIONS("addOrUpdateWishlist");
			#endif

			web_reg_find("TEXT/IC=item", "SaveCount=apiCheck", LAST);
			web_custom_request("addOrUpdateWishlist",
				"URL=https://{api_host}/addOrUpdateWishlist",
				"Method=PUT",
				"Resource=0",
				"RecContentType=application/json",
				"Mode=HTML",
				"Body={\"item\":[{\"productId\":\"{productId}\",\"quantityRequested\":\"1\"}]}",
				LAST);
			if (atoi(lr_eval_string("{apiCheck}")) == 0 )
				lr_end_sub_transaction("T23_MoveFromCartToWishlist_S01_addOrUpdateWishlist", LR_FAIL);
			else
				lr_end_sub_transaction("T23_MoveFromCartToWishlist_S01_addOrUpdateWishlist", LR_AUTO);

			#if OPTIONSENABLED
				call_OPTIONS("updateMultiSelectItemsToRemove");
			#endif

			lr_start_sub_transaction("T23_MoveFromCartToWishlist_S02_updateMultiSelectItemsToRemove", "T23_MoveFromCartToWishlist");
			addHeader();
			web_add_header("Content-Type", "application/json");
			web_reg_find("TEXT/IC=orderId", "SaveCount=apiCheck", LAST);
			web_custom_request("updateMultiSelectItemsToRemove",
				"URL=https://{api_host}/updateMultiSelectItemsToRemove",
				"Method=PUT",
				"Resource=0",
				"RecContentType=application/json",
				"Mode=HTML",
				"Body={\"orderItem\":[{\"orderItemId\":\"{orderItemId}\",\"quantity\":\"0\"}]}",
			LAST);
			if (atoi(lr_eval_string("{apiCheck}")) == 0 )
				lr_end_sub_transaction("T23_MoveFromCartToWishlist_S02_updateMultiSelectItemsToRemove", LR_FAIL);
			else
				lr_end_sub_transaction("T23_MoveFromCartToWishlist_S02_updateMultiSelectItemsToRemove", LR_AUTO);

			lr_start_sub_transaction("T23_MoveFromCartToWishlist_S03_getOrderDetails", "T23_MoveFromCartToWishlist");
	/*
			#if OPTIONSENABLED
				call_OPTIONS("getOrderDetails");
			#endif
	*/
			addHeader();
			web_add_header("calc", "true");
			web_add_header("locStore", "True");
			web_add_header("pageName", "fullOrderInfo");
			web_reg_find("TEXT/IC={", "SaveCount=apiCheck", LAST);
			web_url("getOrderDetails",
				"URL=https://{api_host}/getOrderDetails",
				"Resource=0",
				"RecContentType=application/json",
				LAST);
			if (atoi(lr_eval_string("{apiCheck}")) == 0 )
				lr_end_transaction("T23_MoveFromCartToWishlist_S03_getOrderDetails", LR_FAIL);
			else
				lr_end_transaction("T23_MoveFromCartToWishlist_S03_getOrderDetails", LR_AUTO);

	//		tcp_api2("tcpproduct/getPriceDetails", "GET",  "T23_MoveFromCartToWishlist" );

			lr_start_sub_transaction ( "T23_MoveFromCartToWishlist_S04_getAllCoupons", "T23_MoveFromCartToWishlist" );
			addHeader();
			web_reg_find("TEXT/IC=appliedCoupons", "SaveCount=apiCheck", LAST);
			web_custom_request("getAllCoupons",
				"URL=https://{api_host}/tcporder/getAllCoupons",
				"Method=GET",
				"Resource=0",
				"RecContentType=application/json",
				LAST);
			if (atoi(lr_eval_string("{apiCheck}")) == 0 )
				lr_end_sub_transaction("T23_MoveFromCartToWishlist_S04_getAllCoupons", LR_FAIL);
			else
				lr_end_sub_transaction("T23_MoveFromCartToWishlist_S04_getAllCoupons", LR_AUTO);

			lr_start_sub_transaction ( "T23_MoveFromCartToWishlist_S05_getPointsService", "T23_MoveFromCartToWishlist" );

			addHeader();
			web_reg_find("TEXT/IC=LoyaltyWebsiteInd", "SaveCount=apiCheck", LAST);
			web_custom_request("getPointsService",
				"URL=https://{api_host}/payment/getPointsService",
				"Method=GET",
				"Resource=0",
				"RecContentType=application/json",
				LAST);
			if (atoi(lr_eval_string("{apiCheck}")) == 0 )
				lr_end_sub_transaction("T23_MoveFromCartToWishlist_S05_getPointsService", LR_FAIL);
			else
				lr_end_sub_transaction("T23_MoveFromCartToWishlist_S05_getPointsService", LR_AUTO);

			//lr_continue_on_error(0);

			if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 ) //if no error code and message
			{
				lr_end_transaction ( "T23_MoveFromCartToWishlist", LR_AUTO ) ;
				return LR_PASS;
			}
			else {
				lr_fail_trans_with_error( lr_eval_string ("T23_MoveFromCartToWishlist Failed with Error Code:  \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
				lr_end_transaction ( "T23_MoveFromCartToWishlist", LR_FAIL ) ;
				return LR_FAIL;
			}
		}
	}
	return 0;
}

int getPromoCode() { //action is either "get" or "delete"
int rNum;
unsigned short updateStatus;
char **colnames = NULL;
char **rowdata = NULL;
PVCI2 pvci = 0;

    int row, rc, loopCtr = 0;
    char FieldValue[50];
    int totalCoupon = lr_get_attrib_long("SinglePromoCodeCount");
	char  *VtsServer = "10.56.29.45";
	int   nPort = 8888;
//	char  *VtsServer = "10.56.29.36";
//	int   nPort = 4000;
    pvci = vtc_connect(VtsServer,nPort,VTOPT_KEEP_ALIVE);
	lr_save_string("", "promocode");
    if (strcmp(lr_eval_string("{storeId}"), "10151") == 0 ) {
		while (pvci != 0) {

	        rNum = rand() % totalCoupon + 1;
	        loopCtr++;
	        vtc_query_row(pvci, rNum, &colnames, &rowdata);
			lr_output_message("Query Row Names : %s", colnames[0]);
			lr_output_message("Query Row Data  : %s", rowdata[0]);

	        if ( strcmp(rowdata[0], "") != 0) { //if row is not blank, save it to a parameter then delete it from the table

	        	lr_save_string(rowdata[0], "promocode"); //save the code
	//        	lr_output_message("Promo Code  : %s", lr_eval_string("{promocode}") );
	        	//vtc_clear_row(pvci, rNum, &updateStatus); //delete the code from the datafile
				if (strcmp (lr_eval_string("{currentFlow}"), "checkoutFlow") == 0 )
					vtc_update_row1(pvci,"US_COUPON_CODE", rNum, "",",", &updateStatus);

	            break;
	        }
			if (loopCtr == 5)
				break;
	    }

	    if ( strcmp(lr_eval_string("{promocode}"), "") == 0) {
	        lr_error_message("Single-Use promo code out of data.");
	//        return LR_FAIL;
	    }

	    vtc_free_list(colnames);
	    vtc_free_list(rowdata);
    }
    else {
		while (pvci != 0) {

	        rNum = rand() % totalCoupon + 1;
	        loopCtr++;
	        vtc_query_row(pvci, rNum, &colnames, &rowdata);
			lr_output_message("Query Row Names : %s", colnames[1]);
			lr_output_message("Query Row Data  : %s", rowdata[1]);

	        if ( strcmp(rowdata[1], "") != 0) { //if row is not blank, save it to a parameter then delete it from the table

	        	lr_save_string(rowdata[1], "promocode"); //save the code
	//        	lr_output_message("Promo Code  : %s", lr_eval_string("{promocode}") );
	//        	vtc_clear_row(pvci, rNum, &updateStatus); //delete the code from the datafile
				if (strcmp (lr_eval_string("{currentFlow}"), "checkoutFlow") == 0 )
					vtc_update_row1(pvci,"CA_COUPON_CODE", rNum, "",",", &updateStatus);

	            break;
	        }
			if (loopCtr == 5)
				break;
	    }

	    if ( strcmp(lr_eval_string("{promocode}"), "") == 0) {
	        lr_error_message("Single-Use promo code out of data.");
	//        return LR_FAIL;
	    }

	    vtc_free_list(colnames);
	    vtc_free_list(rowdata);

    }

    rc = vtc_disconnect( pvci );

    return LR_PASS;

}

void accountRewards()
{
	lr_start_transaction("T08_ViewMyAccount_Rewards");

	web_reg_find("TEXT/IC=My Place Rewards", "SaveCount=rewardsCheck", LAST);
	//web_reg_save_param("xxx", "LB=You can redeem", "RB= online at checkout.", "NotFound=Warning", LAST);
	web_url("account/rewards/",
		"URL=https://{host}/{country}/account/rewards/",
		"Resource=0",
		"RecContentType=text/html",
		"Referer=",
		"Mode=HTML",
		LAST);

	lr_start_sub_transaction ("T08_ViewMyAccount_Rewards_getMyPointHistory","T08_ViewMyAccount_Rewards");

	addHeader();
	web_url("payment/getMyPointHistory",
		"URL=https://{api_host}/payment/getMyPointHistory",
		"Resource=0",
		"RecContentType=application/json",
		LAST);

	lr_end_sub_transaction ("T08_ViewMyAccount_Rewards_getMyPointHistory",LR_AUTO);
/*
	lr_start_sub_transaction ( "T08_ViewMyAccount_Rewards_getPointsAndOrderHistory", "T08_ViewMyAccount_Rewards" );
	//web_reg_save_param("s_OrderLookup","LB=\"orderNumber\": \"","RB=\"","NotFound=Warning","Ord=All",LAST);
	addHeader();
	web_add_header("fromRest", "true");
	web_reg_find("TEXT/IC=getOrderHistoryResponse", "SaveCount=apiCheck", LAST);
	web_custom_request("getPointsAndOrderHistory",
		"URL=https://{api_host}/tcporder/getPointsAndOrderHistory",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T08_ViewMyAccount_Rewards_getPointsAndOrderHistory", LR_FAIL);
	else
		lr_end_sub_transaction("T08_ViewMyAccount_Rewards_getPointsAndOrderHistory", LR_AUTO);

	lr_save_string("T08_ViewMyAccount_Rewards","mainTransaction");
	tcp_api2("tcporder/getCreditCardDetails", "GET", "T08_ViewMyAccount_Rewards" );
	tcp_api2("payment/getRegisteredUserDetailsInfo", "GET",  "T08_ViewMyAccount_Rewards" ); // api_getRegisteredUserDetailsInfo();
//	tcp_api2("tcporder/getAllCoupons", "GET",  "T08_ViewMyAccount_Rewards" ); // api_getRegisteredUserDetailsInfo();
//	tcp_api2("tcpproduct/getPriceDetails", "GET",  "T08_ViewMyAccount_Rewards" );

	//web_add_header("action", "get"); //Removed by PavanDusi 111617
	//tcp_api2("tcporder/getFavouriteStoreLocation", "GET",  "T08_ViewMyAccount_Rewards" );              //Removed by PavanDusi 111617
	web_add_header("locStore", "False");
	web_add_header("pageName", "orderSummary");
	web_reg_save_param("cartCount", "LB=cartCount\": ", "RB=,", "NotFound=Warning", LAST);			//"CartCount": 1,\n
	tcp_api2("getOrderDetails", "GET", "T08_ViewMyAccount_Rewards" );                      // api_getOrderDetails();

	if (ESPOT_FLAG == 1)
	{
		tcp_api2("getESpot", "GET", "T08_ViewMyAccount_Rewards" );                             // turned off for akamai caching
		api_getESpot_second();
	}
*/
	if (atoi(lr_eval_string("{rewardsCheck}")) == 0 )
		lr_end_transaction("T08_ViewMyAccount_Rewards", LR_FAIL);
	else
		lr_end_transaction("T08_ViewMyAccount_Rewards", LR_AUTO);

//=================================================================================================================================

	lr_start_transaction("T08_ViewMyAccount_Rewards_PointsHistory");
	addHeader();
	web_reg_find("TEXT/IC=My Place Rewards", "SaveCount=rewardsCheck1", LAST);
	web_url("account/rewards/points-history",
		"URL=https://{host}/{country}/account/rewards/points-history",
		"Resource=0",
		"RecContentType=text/html",
		"Referer=",
		"Mode=HTML",
		LAST);
/*
	//tcp_api2("tcporder/getXAppConfigValues", "GET", "T08_ViewMyAccount_Rewards_PointsHistory" );
	tcp_api2("payment/getRegisteredUserDetailsInfo", "GET",  "T08_ViewMyAccount_Rewards_PointsHistory" ); // api_getRegisteredUserDetailsInfo();
	//web_add_header("action", "get"); ////Removed by PavanDusi 111617
	//tcp_api2("tcporder/getFavouriteStoreLocation", "GET",  "T08_ViewMyAccount_Rewards_PointsHistory" );             ////Removed by PavanDusi 111617
	web_add_header("locStore", "False");
	web_add_header("pageName", "orderSummary");
	web_reg_save_param("cartCount", "LB=cartCount\": ", "RB=,", "NotFound=Warning", LAST);			//"CartCount": 1,\n
	tcp_api2("getOrderDetails", "GET", "T08_ViewMyAccount_Rewards_PointsHistory" );                      // api_getOrderDetails();
	if (ESPOT_FLAG == 1)
	{
		tcp_api2("getESpot", "GET", "T08_ViewMyAccount_Rewards_PointsHistory" );                             // turned off for akamai caching
		api_getESpot_second();
	}

	lr_start_sub_transaction ( "T08_ViewMyAccount_Rewards_PointsHistory_getPointsAndOrderHistory", "T08_ViewMyAccount_Rewards_PointsHistory" );
	addHeader();
	web_add_header("fromRest", "true");
	web_reg_find("TEXT/IC=getOrderHistoryResponse", "SaveCount=apiCheck", LAST);
	web_custom_request("getPointsAndOrderHistory",
		"URL=https://{api_host}/tcporder/getPointsAndOrderHistory",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T08_ViewMyAccount_Rewards_PointsHistory_getPointsAndOrderHistory", LR_FAIL);
	else
		lr_end_sub_transaction("T08_ViewMyAccount_Rewards_PointsHistory_getPointsAndOrderHistory", LR_AUTO);

	lr_start_sub_transaction ( "T08_ViewMyAccount_Rewards_PointsHistory_getCountryListAndHeaderInfo", "T08_ViewMyAccount_Rewards_PointsHistory" );
	addHeader();
	web_reg_find("TEXT/IC=headerInfo", "SaveCount=apiCheck", LAST);
	web_custom_request("getCountryListAndHeaderInfo",
		"URL=https://{api_host}/getCountryListAndHeaderInfo",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T08_ViewMyAccount_Rewards_PointsHistory_getCountryListAndHeaderInfo", LR_FAIL);
	else
		lr_end_sub_transaction("T08_ViewMyAccount_Rewards_PointsHistory_getCountryListAndHeaderInfo", LR_AUTO);
*/
	lr_start_sub_transaction ( "T08_ViewMyAccount_Rewards_PointsHistory_getMyPointHistory", "T08_ViewMyAccount_Rewards_PointsHistory" );
	addHeader();
	web_reg_find("TEXT/IC=pointsHistoryList", "SaveCount=apiCheck", LAST);
	web_custom_request("payment/getMyPointHistory",
		"URL=https://{api_host}/payment/getMyPointHistory",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T08_ViewMyAccount_Rewards_PointsHistory_getMyPointHistory", LR_FAIL);
	else
		lr_end_sub_transaction("T08_ViewMyAccount_Rewards_PointsHistory_getMyPointHistory", LR_AUTO);

	if (atoi(lr_eval_string("{rewardsCheck1}")) == 0 )
		lr_end_transaction("T08_ViewMyAccount_Rewards_PointsHistory", LR_FAIL);
	else
		lr_end_transaction("T08_ViewMyAccount_Rewards_PointsHistory", LR_AUTO);

//=================================================================================================================================

	lr_start_transaction("T08_ViewMyAccount_Rewards_Offers&Coupons");
	web_reg_find("TEXT/IC=My Place Rewards", "SaveCount=rewardsCheck1", LAST);
	web_url("account/rewards/offers",
		"URL=https://{host}/{country}/account/rewards/offers",
		"Resource=0",
		"RecContentType=text/html",
		"Referer=",
		"Mode=HTML",
		LAST);

	lr_start_sub_transaction ("T08_ViewMyAccount_Rewards_Offers&Coupons_getMyPointHistory","T08_ViewMyAccount_Rewards_Offers&Coupons");

	addHeader();
	web_reg_save_param("cartCount", "LB=artCount\": ", "RB=,", "NotFound=Warning", LAST);			//"CartCount": 1,\n

	web_url("payment/getMyPointHistory",
		"URL=https://{api_host}/payment/getMyPointHistory",
		"Resource=0",
		"RecContentType=application/json",
		LAST);

	lr_end_sub_transaction ("T08_ViewMyAccount_Rewards_Offers&Coupons_getMyPointHistory",LR_AUTO);
/*
	lr_start_sub_transaction ( "T08_ViewMyAccount_Rewards_Offers&Coupons_getCreditCardDetails", "T08_ViewMyAccount_Rewards_Offers&Coupons" );

	#if OPTIONSENABLED
		call_OPTIONS("payment/getCreditCardDetails");
	#endif

	addHeader();
	web_add_header("isRest", "true" );
	web_custom_request("getCreditCardDetails",
		"URL=https://{api_host}/payment/getCreditCardDetails",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	lr_end_transaction("T08_ViewMyAccount_Rewards_Offers&Coupons_getCreditCardDetails", LR_AUTO);

	//tcp_api2("tcporder/getXAppConfigValues", "GET", "T08_ViewMyAccount_Rewards_Offers&Coupons" );
	tcp_api2("payment/getRegisteredUserDetailsInfo", "GET",  "T08_ViewMyAccount_Rewards_Offers&Coupons" ); // api_getRegisteredUserDetailsInfo();
	//web_add_header("action", "get"); //Removed by PavanDusi 111617
	//tcp_api2("tcporder/getFavouriteStoreLocation", "GET",  "T08_ViewMyAccount_Rewards_Offers&Coupons" );              //Removed by PavanDusi 111617
	web_add_header("locStore", "False");
	web_add_header("pageName", "orderSummary");
	web_reg_save_param("cartCount", "LB=cartCount\": ", "RB=,", "NotFound=Warning", LAST);			//"CartCount": 1,\n
	tcp_api2("getOrderDetails", "GET", "T08_ViewMyAccount_Rewards_Offers&Coupons" );                      // api_getOrderDetails();
	if (ESPOT_FLAG == 1)
	{
		tcp_api2("getESpot", "GET", "T08_ViewMyAccount_Rewards_Offers&Coupons" );                             // turned off for akamai caching
		api_getESpot_second();
	}

	lr_start_sub_transaction ( "T08_ViewMyAccount_Rewards_Offers&Coupons_getPointsAndOrderHistory", "T08_ViewMyAccount_Rewards_Offers&Coupons" );
	//web_reg_save_param("s_OrderLookup","LB=\"orderNumber\": \"","RB=\"","NotFound=Warning","Ord=All",LAST);
	addHeader();
	web_add_header("fromRest", "true");
	web_reg_find("TEXT/IC=getOrderHistoryResponse", "SaveCount=apiCheck", LAST);
	web_custom_request("getPointsAndOrderHistory",
		"URL=https://{api_host}/tcporder/getPointsAndOrderHistory",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T08_ViewMyAccount_Rewards_Offers&Coupons_getPointsAndOrderHistory", LR_FAIL);
	else
		lr_end_sub_transaction("T08_ViewMyAccount_Rewards_Offers&Coupons_getPointsAndOrderHistory", LR_AUTO);
*/
	if (atoi(lr_eval_string("{rewardsCheck1}")) == 0 )
		lr_end_transaction("T08_ViewMyAccount_Rewards_Offers&Coupons", LR_FAIL);
	else
		lr_end_transaction("T08_ViewMyAccount_Rewards_Offers&Coupons", LR_AUTO);

//==============================================================================================================================================


	if (MULTI_USE_COUPON_FLAG == 1)
	{
		lr_start_transaction ( "T08_ViewMyAccount_Rewards_Offers&Coupons_ApplyToBag" ) ;

			lr_start_sub_transaction ( "T08_ViewMyAccount_Rewards_Offers&Coupons_ApplyToBag_Coupons" , "T08_ViewMyAccount_Rewards_Offers&Coupons_ApplyToBag") ;
			#if OPTIONSENABLED
				web_add_header("Accept", "*/*");
				web_add_header("Access-Control-Request-Headers", "Origin, Host, User-Agent, Accept, Accept-Encoding, Accept-Language, Referer, Request, Content-Type, Transfer-Encoding, Cache-Control, Connection, Content-Length, Content-Language, Date, Expires, Keep-Alive, Location, pragma, client_id, client_secret, grant_type, scope, Authorization, Set-Cookie, Cookie, storeId, nickName, catalogId, langId, orderId, callingPage, requestType, paymentInstructionId, customerId, act, com, a1, city, state, postal, cols, opt, fromPage, promoCode, addressField1, addressField2, zipCode, pageName, locStore, responseFormat, coreName, term, count, isRest, espotName, orderItemId, productId, oldOrderItemId, memberId, emailId, fromRest, companyId, storeLocId, quantity, catEntryId, firstname, lastname, phone, email, action, taskType, stlocId, posStores, isBOPIS, xAppConfigAttrNames, tcpOrderId, PaRes, MD, externalId, pageNumber, pageSize, categoryId, primary, addressId, identifier, corenamecategory, published, rows, searchterm, start, timeallowed, devicetype, savePayment, calc");
				web_add_header("Access-Control-Request-Method", "CONNECT, DELETE, GET, OPTIONS, PATCH, POST, PUT, TRACE");
				web_add_header("Origin", "https://{host}");

				web_custom_request("coupons",
					"URL=https://{api_host}/payment/coupons",
				   "Method=OPTIONS",
				   "Resource=0",
				LAST);
			#endif

			registerErrorCodeCheck();
			addHeader();
			web_custom_request("coupons",
				"URL=https://{api_host}/payment/coupons",
				"Method=POST",
				"Resource=0",
				"RecContentType=application/json",
				"Mode=HTTP",
				"EncType=application/json",
				"Body={\"storeId\":\"{storeId}\",\"langId\":\"-1\",\"catalogId\":\"{catalogId}\",\"URL\":\"\",\"promoCode\":\"FORTYOFF\",\"requesttype\":\"ajax\",\"fromPage\":\"shoppingCartDisplay\",\"taskType\":\"A\"}",
				LAST);
			lr_end_sub_transaction ("T08_ViewMyAccount_Rewards_Offers&Coupons_ApplyToBag_Coupons", LR_AUTO)	;

//	tcp_api2("tcpproduct/getPriceDetails", "GET",  "T08_ViewMyAccount_Rewards_Offers&Coupons_ApplyToBag" );

/*
	lr_start_sub_transaction ( "T08_ViewMyAccount_Rewards_Offers&Coupons_ApplyToBag_getAllCoupons" , "T08_ViewMyAccount_Rewards_Offers&Coupons_ApplyToBag" );
			addHeader();
			web_reg_find("TEXT/IC=appliedCoupons", "SaveCount=apiCheck", LAST);
			web_custom_request("getAllCoupons",
				"URL=https://{api_host}/tcporder/getAllCoupons",
				"Method=GET",
				"Resource=0",
				"RecContentType=application/json",
				LAST);
			if (atoi(lr_eval_string("{apiCheck}")) == 0 )
				lr_end_sub_transaction ("T08_ViewMyAccount_Rewards_Offers&Coupons_ApplyToBag_getAllCoupons", LR_FAIL)	;
			else
				lr_end_sub_transaction ("T08_ViewMyAccount_Rewards_Offers&Coupons_ApplyToBag_getAllCoupons", LR_AUTO)	;
*/
			lr_start_sub_transaction ( "T08_ViewMyAccount_Rewards_Offers&Coupons_ApplyToBag_getOrderDetails", "T08_ViewMyAccount_Rewards_Offers&Coupons_ApplyToBag" );
	/*
			#if OPTIONSENABLED
				call_OPTIONS("getOrderDetails");
			#endif
	*/
			addHeader();
			web_add_header("locStore", "False");
			web_add_header("pageName", "orderSummary");
			web_url("getOrderDetails",
				"URL=https://{api_host}/getOrderDetails",
				"Resource=0",
				"RecContentType=application/json",
				LAST);
			lr_end_sub_transaction ( "T08_ViewMyAccount_Rewards_Offers&Coupons_ApplyToBag_getOrderDetails", LR_AUTO );

			lr_start_sub_transaction ( "T08_ViewMyAccount_Rewards_Offers&Coupons_ApplyToBag_getPointsService", "T08_ViewMyAccount_Rewards_Offers&Coupons_ApplyToBag" );
	/*
			#if OPTIONSENABLED
				call_OPTIONS("payment/getPointsService");
			#endif
	*/
			addHeader();
			web_url("getPointsService",
				 "URL=https://{api_host}/payment/getPointsService",
				 "Resource=0",
				 "RecContentType=application/json",
				 LAST);
			lr_end_sub_transaction("T08_ViewMyAccount_Rewards_Offers&Coupons_ApplyToBag_getPointsService", LR_AUTO);

		lr_end_transaction ("T08_ViewMyAccount_Rewards_Offers&Coupons_ApplyToBag", LR_AUTO)	;
	}
}

void viewOrderStatus()
{
	lr_think_time ( LINK_TT );
	//lr_continue_on_error(1);

	if ( atoi( lr_eval_string("{s_OrderLookup_count}")) > 0 ) {  //Modified by Pavan 06/20/2017

		if (ONLINE_ORDER_SUBMIT==0){
			lr_save_string( lr_paramarr_random("s_OrderLookup"),"orderId");
					lr_save_string("T08_ViewMyAccount_Orders_Details", "orderStatusTransaction");
		}else if (ONLINE_ORDER_SUBMIT==1){
			//////PAVAN DUSI FILLED LOGIC 0626
			//OrderId is already populated
				if (strcmp( lr_eval_string("{addBopisToCart}"), "true") == 0 ){
					lr_save_string("T08_ViewMyAccount_Orders_Details_BOPIS", "orderStatusTransaction");
				}else{
					lr_save_string("T08_ViewMyAccount_Orders_Details_ECOM", "orderStatusTransaction");
				}
		}

		lr_start_transaction(lr_eval_string("{orderStatusTransaction}"));

		web_custom_request("T08_ViewMyAccount_S03_OrdersDetails",
		"URL=https://{host}/{country}/account/orders/order-details/{orderId}",
		"Method=GET",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		"EncType=",
		LAST);

		web_reg_find("TEXT/IC=\"orderId\": \"{orderId}\"", "SaveCount=apiCheck", LAST);
		addHeader();
		web_add_header("Origin", "https://{host}");
		web_add_header("orderId", "{orderId}");
		web_add_header("emailId", "{userEmail}");

		web_custom_request("orderLookup",
			"URL=https://{api_host}/tcporder/orderLookUp",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_transaction(lr_eval_string("{orderStatusTransaction}"), LR_FAIL);
		else
			lr_end_transaction(lr_eval_string("{orderStatusTransaction}"), LR_AUTO);


	}

	//lr_continue_on_error(0);

}// end viewOrderStatus()

void viewOrderHistory()
{
	lr_think_time ( LINK_TT );

//	web_reg_save_param("orderIDs", "LB=&orderId=", "RB=&langId=-1&storeId=", "NotFound=Warning", "ORD=ALL", LAST);
//	web_reg_save_param("fromOrderDetail", "LB=/shop/", "RB=OrderDetail?catalogId=", "NotFound=Warning", LAST);
	web_reg_find("Text/IC=My Place Rewards", "SaveCount=checkOrderhistoryHistory");

	lr_start_transaction ( "T08_ViewMyAccount_Orders" ) ;

	web_custom_request("T08_ViewMyAccount_Orders",
//	"URL=https://{host}/shop/TCPDOMMyOrderHistoryDisplayContent?catalogId={catalogId}&langId=-1&storeId={storeId}", //Pre-R1 Change
//	"URL=https://{host}/shop/TCPDOMMyOrderHistoryDisplayContent?npe=TRUE&catalogId={catalogId}&langId=-1&storeId={storeId}", //R2 Change
	"URL=https://{host}/{country}/account/orders", //R3 Change
		"Method=GET",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		"EncType=",
		LAST);
/*
	lr_start_sub_transaction ( "T08_ViewMyAccount_Orders_getCreditCardDetails", "T08_ViewMyAccount_Orders" );

	#if OPTIONSENABLED
		call_OPTIONS("payment/getCreditCardDetails");
	#endif

	addHeader();
	web_add_header("isRest", "true" );
	web_custom_request("getCreditCardDetails",
		"URL=https://{api_host}/payment/getCreditCardDetails",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	lr_end_transaction("T08_ViewMyAccount_Orders_getCreditCardDetails", LR_AUTO);

	//tcp_api2("tcporder/getXAppConfigValues", "GET", "T08_ViewMyAccount_Orders" );
	tcp_api2("payment/getRegisteredUserDetailsInfo", "GET",  "T08_ViewMyAccount_Orders" ); // api_getRegisteredUserDetailsInfo();
	//web_add_header("action", "get"); ////Removed by PavanDusi 111617
	//tcp_api2("tcporder/getFavouriteStoreLocation", "GET",  "T08_ViewMyAccount_Orders" );             ////Removed by PavanDusi 111617
	web_add_header("locStore", "False");
	web_add_header("pageName", "orderSummary");
	web_reg_save_param("cartCount", "LB=cartCount\": ", "RB=,", "NotFound=Warning", LAST);			//"CartCount": 1,\n
	tcp_api2("getOrderDetails", "GET", "T08_ViewMyAccount_Orders" );                      // api_getOrderDetails();
	if (ESPOT_FLAG == 1)
	{
		tcp_api2("getESpot", "GET", "T08_ViewMyAccount_Orders" );                             // turned off for akamai caching
		api_getESpot_second();
	}
	lr_start_sub_transaction ( "T08_ViewMyAccount_Orders_getPointsAndOrderHistory", "T08_ViewMyAccount_Orders" );
	web_reg_save_param("s_OrderLookup","LB=\"orderNumber\": \"","RB=\"","NotFound=Warning","Ord=All",LAST);
	addHeader();
	web_add_header("fromRest", "true");
	web_reg_find("TEXT/IC=getOrderHistoryResponse", "SaveCount=apiCheck", LAST);
	web_custom_request("getPointsAndOrderHistory",
		"URL=https://{api_host}/tcporder/getPointsAndOrderHistory",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T08_ViewMyAccount_Orders_getPointsAndOrderHistory", LR_FAIL);
	else
		lr_end_sub_transaction("T08_ViewMyAccount_Orders_getPointsAndOrderHistory", LR_AUTO);
*/

	if ( atoi ( lr_eval_string ( "{checkOrderhistoryHistory}" ) ) > 0 )
		lr_end_transaction ( "T08_ViewMyAccount_Orders" , LR_PASS) ;
	else
		lr_end_transaction ( "T08_ViewMyAccount_Orders" , LR_FAIL) ;

	if (atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 50 )
		viewOrderStatus();

}// end viewOrderHistory()



void reservationHistory() //gone???
{
	lr_think_time ( LINK_TT );
	lr_start_transaction("T08_ViewMyAccount_ReservationHistory");
		web_reg_find("TEXT/IC=My Place Rewards", "SaveCount=reservationsCheck", LAST);
		web_url("Reservations",
			"URL=https://{host}/{country}/account/reservations/",
			"Resource=0",
			"RecContentType=text/html",
			"Referer=",
			"Mode=HTML",
		LAST);

	lr_end_transaction("T08_ViewMyAccount_ReservationHistory", LR_AUTO);
}

void addressBook()
{
	lr_think_time ( LINK_TT );
	lr_start_transaction("T08_ViewMyAccount_AddressBook");
	web_reg_find("TEXT/IC=My Place Rewards", "SaveCount=addressBookCheck", LAST);
	web_url("Address-book",
		"URL=https://{host}/{country}/account/address-book/",
		"Resource=0",
		"RecContentType=text/html",
		"Referer=",
		"Mode=HTML",
		LAST);


	if (atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 100 )
	{
		lr_start_sub_transaction("T08_ViewMyAccount_AddressBook_AddNewAddress", "T08_ViewMyAccount_AddressBook");
		web_url("Address-book/add-new-address",
			"URL=https://{host}/{country}/account/address-book/add-new-address",
			"Resource=0",
			"RecContentType=text/html",
			"Referer=",
			"Mode=HTML",
			LAST);
		lr_end_sub_transaction("T08_ViewMyAccount_AddressBook_AddNewAddress", LR_AUTO);

		lr_start_sub_transaction ("T08_ViewMyAccount_AddressBook_addAddress","T08_ViewMyAccount_AddressBook");
		web_reg_save_param("newNickName", "LB=nickName\": \"", "RB=\",", "NotFound=Warning", LAST);
		addHeader();
		web_custom_request("addAddress",
			"URL=https://{api_host}/payment/addAddress",
			"Method=POST",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTML",
			"EncType=application/json",
			"Body={\"contact\":[{\"addressLine\":[\"{guestAdr1}\",\"\",\"\"],\"attributes\":[{\"key\":\"addressField3\",\"value\":\"{guestState}\"}],\"addressType\":\"ShippingAndBilling\",\"city\":\"{guestCity}\",\"country\":\"{guestCountry}\",\"firstName\":\"Joe\",\"lastName\":\"User\",\"nickName\":\"1498060089577\",\"phone1\":\"2014564545\",\"email1\":\"{userEmail}\",\"phone1Publish\":\"false\",\"primary\":\"false\",\"state\":\"{guestState}\",\"zipCode\":\"{guestZip}\",\"xcont_addressField2\":\"1\",\"xcont_addressField3\""
			":\"{guestZip}\",\"fromPage\":\"\"}]}",
			LAST);

		lr_end_sub_transaction ("T08_ViewMyAccount_AddressBook_addAddress", LR_AUTO) ;

		if (strlen(lr_eval_string("{newNickName}")) != 0 )
		{
			lr_start_sub_transaction ("T08_ViewMyAccount_AddressBook_deleteAddressDetails","T08_ViewMyAccount_AddressBook");
			addHeader();
			web_add_header("nickname", lr_eval_string("{newNickName}"));
			web_add_header("Content-Type", "application/json");
			web_custom_request("deleteAddressDetails",
				"URL=https://{api_host}/payment/deleteAddressDetails",
				"Method=DELETE",
				"Resource=0",
				"RecContentType=application/json",
				"Mode=HTTP",
				LAST);
			lr_end_sub_transaction ("T08_ViewMyAccount_AddressBook_deleteAddressDetails", LR_AUTO) ;

			lr_start_sub_transaction ( "T08_ViewMyAccount_AddressBook_getCreditCardDetails", "T08_ViewMyAccount_AddressBook");
/*
			#if OPTIONSENABLED
				call_OPTIONS("payment/getCreditCardDetails");
			#endif
*/
			addHeader();
			web_add_header("isRest", "true" );
			web_custom_request("getCreditCardDetails",
				"URL=https://{api_host}/payment/getCreditCardDetails",
				"Method=GET",
				"Resource=0",
				"RecContentType=application/json",
				LAST);
			lr_end_sub_transaction("T08_ViewMyAccount_AddressBook_getCreditCardDetails", LR_AUTO);

		}

	}
	if (atoi(lr_eval_string("{addressBookCheck}")) == 0 )
		lr_end_transaction("T08_ViewMyAccount_AddressBook", LR_FAIL);
	else
		lr_end_transaction("T08_ViewMyAccount_AddressBook", LR_AUTO);

}

void payment()
{
	lr_think_time ( LINK_TT );
	lr_start_transaction("T08_ViewMyAccount_Payment");
	web_reg_find("TEXT/IC=My Place Rewards", "SaveCount=paymentCheck", LAST);
	web_url("Payment",
		"URL=https://{host}/{country}/account/payment/",
		"Resource=0",
		"RecContentType=text/html",
		"Referer=",
		"Mode=HTML",
		LAST);

	if (atoi(lr_eval_string("{paymentCheck}")) == 0 )
		lr_end_transaction("T08_ViewMyAccount_Payment", LR_FAIL);
	else
		lr_end_transaction("T08_ViewMyAccount_Payment", LR_AUTO);
}

void addDeleteBirthdaySavings()
{
	lr_think_time ( LINK_TT );

	lr_start_transaction("T08_ViewMyAccount_Profile_addBirthdaySavings");
	#if OPTIONSENABLED
		web_add_header("Accept", "*/*");
		web_add_header("Access-Control-Request-Method", "GET");
		web_add_header("Origin", "https://{host}");
		web_custom_request("addBirthdaySavings",
			"URL=https://{api_host}/tcporder/addBirthdaySavings",
			"Method=OPTIONS",
			"Resource=0",
			"Mode=HTML",
			LAST);
	#endif

	web_reg_save_param("timeStamp", "LB=timeStamp\": \"", "RB=\"", "NotFound=Warning", LAST);
	web_reg_save_param("childId", "LB=childId\": \"", "RB=\"", "NotFound=Warning", LAST);
	addHeader();
    web_custom_request("addBirthdaySavings",
        "URL=https://{api_host}/tcporder/addBirthdaySavings",
        "Method=POST",
        "Resource=0",
        "RecContentType=application/json",
        "Mode=HTML",
        "EncType=application/json",
        "Body={\"firstName\":\"JoeJunior{profileEdit}\",\"lastName\":\"User\",\"timestamp\":\"{tsBirthdaySavings}\",\"childDetails\":[{\"childName\":\"JoeBaby{profileEdit}\",\"birthYear\":\"2017\",\"birthMonth\":\"2\",\"gender\":\"01\"}]}",
//		"Body={\"firstName\":\"JoeJunior\",\"lastName\":\"User\",\"timestamp\":\"2017-07-13T15:59:24.159Z\",\"childDetails\":[{\"childName\":\"JoeBaby\",\"birthYear\":\"2017\",\"birthMonth\":\"2\",\"gender\":\"01\"}]}",
        LAST);

	lr_end_transaction("T08_ViewMyAccount_Profile_addBirthdaySavings", LR_AUTO);

	lr_think_time ( LINK_TT );

	lr_start_transaction("T08_ViewMyAccount_Profile_deleteBirthdaySavings");
	#if OPTIONSENABLED
		web_add_header("Accept", "*/*");
		web_add_header("Access-Control-Request-Method", "GET");
		web_add_header("Origin", "https://{host}");
		web_custom_request("deleteBirthdaySavings",
			"URL=https://{api_host}/tcporder/deleteBirthdaySavings",
			"Method=OPTIONS",
			"Resource=0",
			"Mode=HTML",
			LAST);
	#endif

	addHeader();
	web_custom_request("deleteBirthdaySavings_2",
		"URL=https://{api_host}/tcporder/deleteBirthdaySavings",
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTML",
		"EncType=application/json",
		"Body={\"timestamp\":\"{timeStamp}\",\"childDetails\":[{\"childId\":\"{childId}\"}]}",
//		"Body={\"timestamp\":\"2017-6-13 14:58:46.466\",\"childDetails\":[{\"childId\":\"306510\"}]}",
		LAST);
	lr_end_transaction("T08_ViewMyAccount_Profile_deleteBirthdaySavings", LR_AUTO);
}


void profile()
{
	lr_think_time ( LINK_TT );
	lr_start_transaction("T08_ViewMyAccount_Profile");
	web_reg_find("TEXT/IC=My Place Rewards", "SaveCount=profileCheck", LAST);
	web_url("Profile",
		"URL=https://{host}/{country}/account/profile/",
		"Resource=0",
		"RecContentType=text/html",
		"Referer=",
		"Mode=HTML",
		LAST);

	if (atoi(lr_eval_string("{profileCheck}")) == 0 )
		lr_end_transaction("T08_ViewMyAccount_Profile", LR_FAIL);
	else
		lr_end_transaction("T08_ViewMyAccount_Profile", LR_AUTO);

	lr_start_transaction ( "T08_ViewMyAccount_Profile_getBirthdaySavings" );

	#if OPTIONSENABLED
		call_OPTIONS("tcporder/getBirthdaySavings");
	#endif

	addHeader();
	web_custom_request("tcporder/getBirthdaySavings",
		"URL=https://{api_host}/tcporder/getBirthdaySavings",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);

	lr_end_transaction("T08_ViewMyAccount_Profile_getBirthdaySavings", LR_AUTO);

	lr_start_transaction("T08_ViewMyAccount_Profile_Edit");
	#if OPTIONSENABLED
		web_add_header("Accept", "*/*");
		web_add_header("Access-Control-Request-Method", "GET");
		web_add_header("Origin", "https://{host}");
		web_custom_request("payment/updatesAccountDataForRegisteredUser",
			"URL=https://{api_host}/payment/updatesAccountDataForRegisteredUser",
			"Method=OPTIONS",
			"Resource=0",
			"Mode=HTML",
			LAST);
	#endif
	addHeader();
	web_add_header("Content-Type", "application/json");
	web_custom_request("updatesAccountDataForRegisteredUser",
		"URL=https://{api_host}/payment/updatesAccountDataForRegisteredUser",
		"Method=PUT",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTML",
		"Body={\"firstName\":\"Joe{profileEdit}\",\"lastName\":\"User\",\"associateId\":\"\",\"email1\":\"{userEmail}\",\"phone1\":\"2014531236\",\"operation\":\"\"}",
		LAST);
	lr_end_transaction("T08_ViewMyAccount_Profile_Edit", LR_AUTO);

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ADDBIRTHDAYSAVINGS )
		addDeleteBirthdaySavings();

}

void preferences()
{
	lr_think_time ( LINK_TT );
	lr_start_transaction("T08_ViewMyAccount_Preferences");
	web_reg_find("TEXT/IC=My Place Rewards", "SaveCount=preferencesCheck", LAST);
	web_url("Preferences",
		"URL=https://{host}/{country}/account/preferences/",
		"Resource=0",
		"RecContentType=text/html",
		"Referer=",
		"Mode=HTML",
		LAST);

	if (atoi(lr_eval_string("{preferencesCheck}")) == 0 )
		lr_end_transaction("T08_ViewMyAccount_Preferences", LR_FAIL);
	else
		lr_end_transaction("T08_ViewMyAccount_Preferences", LR_AUTO);

}

void myAccountOthers()
{
	int randomNumber = atoi(lr_eval_string("{RANDOM_PERCENT}"));

	if ( randomNumber <= RATIO_ACCOUNT_REWARDS )
		accountRewards();
	else if ( randomNumber <= RATIO_ACCOUNT_REWARDS + RATIO_ORDER_HISTORY )
		viewOrderHistory();
	else if ( randomNumber <= RATIO_ACCOUNT_REWARDS + RATIO_ORDER_HISTORY + RATIO_RESERVATION_HISTORY )
		reservationHistory();
	else if ( randomNumber <= RATIO_ACCOUNT_REWARDS + RATIO_ORDER_HISTORY + RATIO_RESERVATION_HISTORY  + RATIO_ADDRESSBOOK )
		addressBook();
	else if ( randomNumber <= RATIO_ACCOUNT_REWARDS + RATIO_ORDER_HISTORY + RATIO_RESERVATION_HISTORY  + RATIO_ADDRESSBOOK + RATIO_PAYMENT )
		payment();
	else if ( randomNumber <= RATIO_ACCOUNT_REWARDS + RATIO_ORDER_HISTORY + RATIO_RESERVATION_HISTORY  + RATIO_ADDRESSBOOK + RATIO_PAYMENT + RATIO_PROFILE )
		profile();
	else if ( randomNumber <= RATIO_ACCOUNT_REWARDS + RATIO_ORDER_HISTORY + RATIO_RESERVATION_HISTORY  + RATIO_ADDRESSBOOK + RATIO_PAYMENT + RATIO_PROFILE + RATIO_PREFERENCES )
		preferences();

	//lr_continue_on_error(0);

	return;
}



int viewMyAccount()
{
	int randomNumber = atoi(lr_eval_string("{RANDOM_PERCENT}"));
	lr_think_time ( LINK_TT ) ;
	//lr_continue_on_error(1);

	//web_set_sockets_option("SSL_VERSION", "3");
	//web_set_sockets_option("MAX_CONNECTIONS_PER_HOST","1");
	//web_set_sockets_option("SSL_VERSION", "TLS1.2");
	//Unknown SSL version TLS1: use one of '2', '3', '2&3', 'TLS', 'TLS1.1', 'TLS1.2'
	lr_start_transaction("T08_ViewMyAccount");

	lr_start_sub_transaction("T08_ViewMyAccount ViewMyAccount", "T08_ViewMyAccount" );

		web_reg_find("TEXT/IC=window.__PRELOADED_STATE","SaveCount=MyACC_openCount", LAST);

		web_url("Account Overview",
			"URL=https://{host}/{country}/account/",
			"Resource=0",
			"RecContentType=text/html",
			"Referer=",
			"Mode=HTML",
			LAST);
	if (atoi(lr_eval_string("{MyACC_openCount}"))>0) {
		lr_end_sub_transaction("T08_ViewMyAccount ViewMyAccount",LR_AUTO);
	} else {
		lr_end_sub_transaction("T08_ViewMyAccount ViewMyAccount",LR_FAIL);
	}

	//tcp_api2("tcporder/getXAppConfigValues", "GET", "T08_ViewMyAccount" );

	lr_start_sub_transaction ( "T08_ViewMyAccount_getRegisteredUserDetailsInfo", "T08_ViewMyAccount" );
/*
	#if OPTIONSENABLED
		call_OPTIONS("payment/getRegisteredUserDetailsInfo");
	#endif
*/
	addHeader();
	web_reg_save_param ( "addressId" , "LB=\"addressId\": \"" , "RB=\",\n" , "ORD=1", "NotFound=Warning", LAST ) ; //"addressId": "1398789",\n
	web_reg_save_param ( "addressIdAll" , "LB=\"addressId\": \"" , "RB=\",\n" , "ORD=ALL", "NotFound=Warning", LAST ) ; //"addressId": "1398789",\n
	web_reg_find("TEXT/IC=resourceName", "SaveCount=apiCheck", LAST);
	web_custom_request("getRegisteredUserDetailsInfo",
		"URL=https://{api_host}/payment/getRegisteredUserDetailsInfo",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T08_ViewMyAccount_getRegisteredUserDetailsInfo", LR_FAIL);
	else
		lr_end_sub_transaction("T08_ViewMyAccount_getRegisteredUserDetailsInfo", LR_AUTO);

	//tcp_api2("tcporder/getXAppConfigValues", "GET", "T08_ViewMyAccount" );

	lr_start_sub_transaction ( "T08_ViewMyAccount_getPointsAndOrderHistory", "T08_ViewMyAccount" );
	web_reg_save_param("s_OrderLookup","LB=\"orderNumber\": \"","RB=\"","NotFound=Warning","Ord=All",LAST);
	addHeader();
	web_add_header("fromRest", "true");
	web_reg_find("TEXT/IC=getOrderHistoryResponse", "SaveCount=apiCheck", LAST);
	web_custom_request("getPointsAndOrderHistory",
		"URL=https://{api_host}/tcporder/getPointsAndOrderHistory",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T08_ViewMyAccount_getPointsAndOrderHistory", LR_FAIL);
	else
		lr_end_sub_transaction("T08_ViewMyAccount_getPointsAndOrderHistory", LR_AUTO);

	lr_start_sub_transaction ("T08_ViewMyAccount_getOrderDetails","T08_ViewMyAccount");
/*
	#if OPTIONSENABLED
		call_OPTIONS("getOrderDetails");
	#endif
*/
//          "url": "https://api-perf.childrensplace.com/v3/getOrderDetails",
	addHeader();
	web_add_header("locStore", "False");
	web_add_header("pageName", "orderSummary");
	web_reg_save_param("cartCount", "LB=artCount\": ", "RB=,", "NotFound=Warning", LAST);			//"CartCount": 1,\n
	web_url("getOrderDetails",
		"URL=https://{api_host}/getOrderDetails",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	lr_end_sub_transaction ("T08_ViewMyAccount_getOrderDetails",LR_AUTO);

	if (ESPOT_FLAG == 1)
	{
		lr_start_sub_transaction ( "T08_ViewMyAccount_getESpot", "T08_ViewMyAccount" ); // This is called 2x in uatlive2

		#if OPTIONSENABLED
			call_OPTIONS("getESpot");
		#endif

		addHeader();
		web_add_header("espotName", "GlobalHeaderBannerAboveHeader,GlobalHeaderBannerWithinHeader,GlobalHeaderLogo,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,create_account_drawer,MinicartLoginBanner,MinicartRegisterBanner,drawer_myaccount_overview_banner,coupon_help,bag_empty_banner,drawer_empty_bag,MinicartLoginFromFavoritesBanner,bag_ledger_promo_banner,gift_services_espot,CheckoutConfirmationBanner,CheckoutConfirmationMPRPromo,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1" );
		web_add_header("deviceType","desktop");

	//    web_add_header("espotName","GlobalHeaderBannerAboveHeader,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1");
		web_reg_find("TEXT/IC=espotName", "SaveCount=apiCheck", LAST);
		web_custom_request("getESpots",
			"URL=https://{api_host}/getESpot",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction("T08_ViewMyAccount_getESpot", LR_FAIL);
		else
			lr_end_sub_transaction("T08_ViewMyAccount_getESpot", LR_AUTO);

		api_getESpot_second();
	}

	lr_start_sub_transaction ( "T08_ViewMyAccount_getCreditCardDetails", "T08_ViewMyAccount" );
/*
	#if OPTIONSENABLED
		call_OPTIONS("payment/getCreditCardDetails");
	#endif
*/
//          "url": "https://api-perf.childrensplace.com/v3/payment/getCreditCardDetails",
	addHeader();
	web_add_header("isRest", "true" );
	web_custom_request("getCreditCardDetails",
		"URL=https://{api_host}/payment/getCreditCardDetails",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	lr_end_sub_transaction("T08_ViewMyAccount_getCreditCardDetails", LR_AUTO);

	tcp_api2("tcpproduct/getPriceDetails", "GET",  "T08_ViewMyAccount" );
/*
	lr_start_sub_transaction ( "T08_ViewMyAccount_getAllCoupons", "T08_ViewMyAccount" );
	addHeader();
	web_reg_find("TEXT/IC=appliedCoupons", "SaveCount=apiCheck", LAST);
	web_custom_request("getAllCoupons",
		"URL=https://{api_host}/tcporder/getAllCoupons",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T08_ViewMyAccount_getAllCoupons", LR_FAIL);
	else
		lr_end_sub_transaction("T08_ViewMyAccount_getAllCoupons", LR_AUTO);
*/
	lr_start_sub_transaction ( "T08_ViewMyAccount_getAddressFromBook", "T08_ViewMyAccount" );
/*
	#if OPTIONSENABLED
		call_OPTIONS("payment/getAddressFromBook");
	#endif
*/

	addHeader();
	web_custom_request("getAddressFromBook",
		"URL=https://{api_host}/payment/getAddressFromBook",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	lr_end_sub_transaction("T08_ViewMyAccount_getAddressFromBook", LR_AUTO);

	lr_end_transaction("T08_ViewMyAccount", LR_AUTO);

//	RATIO_ACCOUNT_REWARDS 		= 10;
//	RATIO_ORDER_HISTORY			= 40;
//	RATIO_RESERVATION_HISTORY	= 10;
//	RATIO_ADDRESSBOOK			= 10;
//	RATIO_PAYMENT				= 10;
//	RATIO_PROFILE				= 10;
//	RATIO_PREFERENCES			= 10;
//	ONLINE_ORDER_SUBMIT=1;

	if (ONLINE_ORDER_SUBMIT==1){   //For the case when user places an order and wants to lookup order status directly
		if ( randomNumber <= RATIO_ACCOUNT_REWARDS + RATIO_ORDER_HISTORY )
//		viewOrderHistory();
	    return LR_PASS;							//-- not interested in doing anything else.- the rest cases are covered in DROP User.
	}

	if (randomNumber <= 15)
		myAccountOthers();

	 return LR_PASS;
}


void newsLetterSignup()
{
	lr_think_time ( LINK_TT ) ;

	web_set_sockets_option("SSL_VERSION", "TLS1.1");

	lr_start_transaction("T26_NewsLetterSignup");

		lr_start_sub_transaction("T26_NewsLetterSignup_S01_email-subscribe", "T26_NewsLetterSignup");

		web_url("email-subscribe",
			"URL=https://{host}/webapp/wcs/stores/servlet/TCPContentDisplay?catalogId={catalogId}&langId=-1&storeId={storeId}&categoryId=341001",
			"Resource=0",
			"RecContentType=text/html",
			"Referer=",
			"Mode=HTML",
			LAST);

		lr_end_sub_transaction("T26_NewsLetterSignup_S01_email-subscribe", LR_AUTO);

		lr_start_sub_transaction("T26_NewsLetterSignup_S02_TCPAjaxEmailVerificationCmd", "T26_NewsLetterSignup");

		web_submit_data("TCPAjaxEmailVerificationCmd",
			"Action=https://{host}/webapp/wcs/stores/servlet/TCPAjaxEmailVerificationCmd",
			"Method=POST",
			"RecContentType=application/json",
			"Mode=HTML",
			"EncodeAtSign=YES",
			ITEMDATA,
			"Name=email", "Value={userEmail}", ENDITEM,
//			"Name=email", "Value={emailVerification}@gmail.com", ENDITEM,
			"Name=page", "Value=newsletterSignUp", ENDITEM,
			"Name=requesttype", "Value=ajax", ENDITEM,
			LAST);

		lr_end_sub_transaction("T26_NewsLetterSignup_S02_TCPAjaxEmailVerificationCmd", LR_AUTO);

//		web_reg_find("Text=You will receive your first email from us shortly.", LAST);

		lr_start_sub_transaction("T26_NewsLetterSignup_S03_addSignUpEmail", "T26_NewsLetterSignup");

		addHeader();
		web_add_header("Content-Type", "application/json");
		web_custom_request("addSignUpEmail",
			"URL=https://{api_host}/addSignUpEmail",
			"Method=POST",
			"Resource=0",
			"RecContentType=application/json",
			"Body={\"storeId\":\"{storeId}\",\"langId\":\"-1\",\"catalogId\":\"{catalogId}\",\"emailaddr\":\"{userEmail}\",\"URL\":\"email-confirmation\",\"response\":\"accept_all::true:false\"}",
		LAST);

		lr_end_sub_transaction("T26_NewsLetterSignup_S03_addSignUpEmail", LR_AUTO);

	lr_end_transaction("T26_NewsLetterSignup", LR_AUTO);
}

void createCookieCmd()
{
	lr_start_transaction("T28_CreateCookieCmd");

	web_custom_request("CreateCookieCmd",
		"URL=https://{host}/webapp/wcs/stores/servlet/CreateCookieCmd?langId=-1&storeId={storeId}&goToPage=NoURL&catalogId={catalogId}",
		"Method=GET",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		"EncType=application/x-www-form-urlencoded",
		LAST);

	lr_end_transaction("T28_CreateCookieCmd", LR_AUTO);

}


int tempATC()
{

				web_custom_request("TCPProductQuickView",
				//https://{host}/webapp/wcs/stores/servlet/?storeId=10151&catalogId=10551&langId=-1&productId=800358
					"URL=http://{host}/shop/TCPProductQuickView?catalogId=10551&parent_category_rn=&top_category=&categoryId=489202&langId=-1&productId=801561&storeId=10151",
					"Method=GET",
					"Resource=0",
					"RecContentType=text/html",
					"Snapshot=t18.inf",
					"Mode=HTML",
					"EncType=application/x-www-form-urlencoded",
					LAST);

				lr_start_sub_transaction ("T04_Product Quickview Page - GetSwatchesAndSizeInfo", "T04_Product Quickview Page" );

				web_custom_request("GetSwatchesAndSizeInfo",
				//https://{host}/webapp/wcs/stores/servlet/?storeId=10151&catalogId=10551&langId=-1&productId=800358
					"URL=http://{host}/webapp/wcs/stores/servlet/GetSwatchesAndSizeInfo?langId=-1&storeId=10151&catalogId=10551&productId=801561",
					"Method=GET",
					"Resource=0",
					"RecContentType=text/html",
					"Snapshot=t18.inf",
					"Mode=HTML",
					"EncType=application/x-www-form-urlencoded",
					LAST);

				lr_end_sub_transaction ( "T04_Product Quickview Page - GetSwatchesAndSizeInfo", LR_PASS );

				addToCartCorrelations();

				lr_start_sub_transaction ("T04_Product Quickview Page - TCPGetSKUDetailsView", "T04_Product Quickview Page" );

				web_custom_request("TCPGetSKUDetailsView",
				//https://{host}/webapp/wcs/stores/servlet/TCPGetSKUDetailsView?storeId=10151&catalogId=10551&langId=-1&productId=800358
					"URL=http://{host}/webapp/wcs/stores/servlet/TCPGetSKUDetailsView?langId=-1&storeId=10151&catalogId=10551&productId=801561",
					"Method=GET",
					"Resource=0",
					"RecContentType=text/html",
					"Snapshot=t18.inf",
					"Mode=HTML",
					"EncType=application/x-www-form-urlencoded",
					LAST);

				lr_end_sub_transaction ( "T04_Product Quickview Page - TCPGetSKUDetailsView", LR_PASS );
		web_reg_save_param ( "orderId" , "LB=\"orderId\": [\"" , "RB=\"]" , "NotFound=Warning",  LAST ) ;
		web_reg_save_param ( "orderItemId" , "LB=\"orderItemId\": [\"" , "RB=\"]" , "NotFound=Warning", LAST ) ;
	web_reg_save_param_regexp ( "ParamName=authTokens" , "RegExp=WC_AUTHENTICATION_[0-9]+=([^D][^;]+);" , SEARCH_FILTERS , "Scope=Headers" , "NotFound=Warning", "Ordinal=All", LAST ) ;
				web_submit_data("addtocart_AjaxOrderChangeServiceItemAdd",
				"Action=https://{host}/webapp/wcs/stores/servlet/AjaxOrderChangeServiceItemAdd",
				"Method=POST",
				"RecContentType=text/html",
				"Snapshot=t25.inf",
				"Mode=HTML",
				ITEMDATA,
				"Name=storeId", "Value={storeId}", ENDITEM,
				"Name=catalogId", "Value={catalogId}", ENDITEM,
				"Name=langId", "Value=-1", ENDITEM,
				"Name=orderId", "Value=.", ENDITEM,
				"Name=field2", "Value=0", ENDITEM,
				"Name=comment", "Value=828661", ENDITEM,
				"Name=calculationUsage", "Value=-1,-2,-5,-6,-7", ENDITEM,
				"Name=catEntryId", "Value=828661", ENDITEM,
				"Name=quantity", "Value=1", ENDITEM,
				"Name=requesttype", "Value=ajax", ENDITEM,
				"Name=visitorId", "Value=[CS]v1|2B0B56810507A725-40000116E00E1B28[CE]", ENDITEM, //0802
				LAST);
/*
				web_custom_request("TCPProductQuickView",
				//https://{host}/webapp/wcs/stores/servlet/?storeId=10151&catalogId=10551&langId=-1&productId=800358
					"URL=http://{host}/shop/TCPProductQuickView?catalogId=10551&parent_category_rn=&top_category=&categoryId=489202&langId=-1&productId=280617&storeId=10151",
					"Method=GET",
					"Resource=0",
					"RecContentType=text/html",
					"Snapshot=t18.inf",
					"Mode=HTML",
					"EncType=application/x-www-form-urlencoded",
					LAST);

				lr_start_sub_transaction ("T04_Product Quickview Page - GetSwatchesAndSizeInfo", "T04_Product Quickview Page" );

				web_custom_request("GetSwatchesAndSizeInfo",
				//https://{host}/webapp/wcs/stores/servlet/?storeId=10151&catalogId=10551&langId=-1&productId=800358
					"URL=http://{host}/webapp/wcs/stores/servlet/GetSwatchesAndSizeInfo?langId=-1&storeId=10151&catalogId=10551&productId=280617",
					"Method=GET",
					"Resource=0",
					"RecContentType=text/html",
					"Snapshot=t18.inf",
					"Mode=HTML",
					"EncType=application/x-www-form-urlencoded",
					LAST);

				lr_end_sub_transaction ( "T04_Product Quickview Page - GetSwatchesAndSizeInfo", LR_PASS );

				addToCartCorrelations();

				lr_start_sub_transaction ("T04_Product Quickview Page - TCPGetSKUDetailsView", "T04_Product Quickview Page" );

				web_custom_request("TCPGetSKUDetailsView",
				//https://{host}/webapp/wcs/stores/servlet/TCPGetSKUDetailsView?storeId=10151&catalogId=10551&langId=-1&productId=800358
					"URL=http://{host}/webapp/wcs/stores/servlet/TCPGetSKUDetailsView?langId=-1&storeId=10151&catalogId=10551&productId=280617",
					"Method=GET",
					"Resource=0",
					"RecContentType=text/html",
					"Snapshot=t18.inf",
					"Mode=HTML",
					"EncType=application/x-www-form-urlencoded",
					LAST);

				lr_end_sub_transaction ( "T04_Product Quickview Page - TCPGetSKUDetailsView", LR_PASS );
		web_reg_save_param ( "orderId" , "LB=\"orderId\": [\"" , "RB=\"]" , "NotFound=Warning",  LAST ) ;
		web_reg_save_param ( "orderItemId" , "LB=\"orderItemId\": [\"" , "RB=\"]" , "NotFound=Warning", LAST ) ;
	web_reg_save_param_regexp ( "ParamName=authTokens" , "RegExp=WC_AUTHENTICATION_[0-9]+=([^D][^;]+);" , SEARCH_FILTERS , "Scope=Headers" , "NotFound=Warning", "Ordinal=All", LAST ) ;
				web_submit_data("addtocart_AjaxOrderChangeServiceItemAdd",
				"Action=https://{host}/webapp/wcs/stores/servlet/AjaxOrderChangeServiceItemAdd",
				"Method=POST",
				"RecContentType=text/html",
				"Snapshot=t25.inf",
				"Mode=HTML",
				ITEMDATA,
				"Name=storeId", "Value={storeId}", ENDITEM,
				"Name=catalogId", "Value={catalogId}", ENDITEM,
				"Name=langId", "Value=-1", ENDITEM,
				"Name=orderId", "Value=.", ENDITEM,
				"Name=field2", "Value=0", ENDITEM,
				"Name=comment", "Value=280617", ENDITEM,
				"Name=calculationUsage", "Value=-1,-2,-5,-6,-7", ENDITEM,
				"Name=catEntryId", "Value=280617", ENDITEM,
				"Name=quantity", "Value=1", ENDITEM,
				"Name=requesttype", "Value=ajax", ENDITEM,
				"Name=visitorId", "Value=[CS]v1|2B0B56810507A725-40000116E00E1B28[CE]", ENDITEM, //0802
				LAST);
*/
	return 0;
}



int webInstantCredit()  // need to decide where to insert this, most likely on drop scenario
{
	lr_think_time ( LINK_TT ) ;

	web_set_sockets_option("SSL_VERSION", "TLS");
	web_url("place-card",
		"URL=http://{host}/us/place-card/",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		LAST);

	web_url("application",
		"URL=https://{host}/us/place-card/application",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		LAST);

	lr_start_transaction ( "T18_WebInstantCredit_tcpstore/processWIC") ;
	web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", LAST); //"errorCode": "",\n
	web_reg_save_param("errorMessage", "LB=\"errorMessage\": \"", "RB=\",\n", "NotFound=Warning", LAST); //"errorMessage": "",\n

	addHeader();
	web_custom_request("processWIC",
 		"URL=https://{api_host}/tcpstore/processWIC",
 		"Method=POST",
 		"Resource=0",
 		"RecContentType=application/json",
		"Mode=HTML",
		"Body=firstName=always&"
		"lastName=approve&"
		"middleInitial=&"
		"address1=500%20Plaza%20Dr&"
		"address2=&"
		"city=Secaucus&"
 		"state=NJ&"
 		"zipCode=07094&"
 		"country=US&"
		"ssn=9713&"
 		"alternatePhoneNumber=20145379531"
 		"emailAddress=bitrogue@mailinator.COM&"
 		"birthdayDate=1966{randomTwoDigits}25&"
 		"BF_ioBlackBox=0400mTR4Z7%2BBQcwNf94lis1ztosU8peJTCdUy7uGpRvabX7zVQjDWlYxYTXfueurujcaykJk9fR9lXIBdODzAYZvxp%2FyAGc0u%2F5UPzPPrHsRu27u2MJCSi12NBBoiZbfxP1Whlz5wlRFwWJi0FRulruXQQGCQaJkXU7GIIQMZuRs6N3qtP6CELnsH2G%2FcdkIMFbm6Yf0%2FpTJUUz1vNp0X2Zw8QydKgnOIDKXq4HnEqNOos0%2FB5zvrPJclqTOWG8pq1WO6yzJ1GNmMuMWZBamlGXoG%2FimnjwHY9HQtQzpGfcm0cR8X2Fd1ngNFGLDGZlWOX0jWbA6sSW4rBr%2FNrWzf5gYDruvNvEfRkJupBy3Z8hSEMEK7ZWd2T2HOoqcaV9FaOImRYT%2FgKodv2ev8lCwCfn1kaxBMac8RE9JQyxpl%2FFlRdEUxU0rlIm%2Bh0IdvEt%2BMbJEO8xzyZZyces4cd3X0jzFPr%2BN"
 		"op3TdJMFv9eOvsyvTBVVGK66EXEuaD0YLXuOWeFzcFTbpKHLiQrKf2hhwqstJ%2BrhYKIcxurY7kpCzwwdNW9e0%2F8IpHNM6NXh35GdB6Cc0rdKBra3m4ZITAN%2BPMPl1UvBE7jWZcVftxOczszofnt%2B%2Fu06EOo%2FUEYn9EFMRfvxVguGdCj5lheavfwPVmg%2BGk4OuHG49T1M%2BsnP9%2BSWhwxVxvHWf%2FuK445lNi%2BcovQlymqz1P3vzg9Oq87auE3RWwyPd429Hc3XdHevRMsQLV99Js2o8vmPAEOCbnRzpX9SwTbkqlmBR5N%2BYk9XXjom4y0AH2WgO40osBx6w1tx0mMZz7HtmSQrKuHt39bvxE86eVaYJEF00M2dmwpFJMf4g%2FnVivVfWl%2FGUEnvBpGzF9Rzh%2BjIcWSpp1SAFu7g0I1zyeSmTmPvDtmeZWysG5GNVi4ixCeMzU55NYBu%2FnSSujAF%2"
 		"FImMsuY70u6HOYdWG7EoZqeIeCHbDJ27mIZGZKEKh7xUN0sYF8r08f8uCcC0xOD%2BDPTu5VIg0CDMnwUlr%2F77l1ahRLPHCJ2nyP5hDtcTkIWM0MsrZYk6VspTTT4YfknEFHaD16ysmtjwcmmsgU0YREDCPutzcZTzmdVsVWccBDUhUs7HwJBLr5Keb%2BrR4RRGASpJ6ETmLbtOxKYkBFXNCg%2BO9YxO2R4GpUeravdrC26q6G5qP8J%2FK36i8%2FSM0OKZFYlvAevMmALkiiRCA6evWpL5xAYexDg4i8EgSau8uDTmXK3W95ikc26%2FuyCFY4R6KghlGeYLRb6hyFDOLPt%2B1kkJorDKbFLXB%2BZnOmejza7dnYdB3dXjB8n%2FNyT5AQKok7PtgJahH1UlSIs%2FQYMqtxvWZOLhAalWhbY%2B5yP8WdvLrD%2BP%2B1qaTpIvVu%2BuyQsaMh%2Bu9Xlp9HpZMEwvM9eWpJ"
		"75rIHe6%2FoPDSN%2Fxbk%2F43Sqb%2BGNnr6gpDIP0CxSx7kjSYP3C%2B3xVRrwyLRJ5XaZyrxo5J2FcY2%2BNO9P6IY2f%2BxktCOaw7AMCoqGG0O%2B3VHdw0mDejZrO1Umoq3MFE1OOL9Bn8XCTDfLXCixdWzqMvL5OMQ%2FiYXaPnuM20kjENkpO1N%2FdgoDKMciqJTUWwy1e%2FKq%2BVlO6QfGcU5hfv6m5A%3D%3D"
		"&prescreenId=",
 		LAST);
// 		"emailAddress=MANNY{randomFourDigits}@GMAIL.COM&"

		if ( strcmp( lr_eval_string("{errorCode}") ,"") != 0 && strcmp(lr_eval_string("{errorMessage}") ,"") != 0 )
		{
			lr_fail_trans_with_error( lr_eval_string("T18_WebInstantCredit_tcpstore/processWIC - Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_transaction( "T18_WebInstantCredit_tcpstore/processWIC", LR_FAIL);
			return LR_FAIL;
		}
		else
		{
			lr_end_transaction ( "T18_WebInstantCredit_tcpstore/processWIC" , LR_PASS ) ;
			return LR_PASS;
		}
}

int submitRTPS()
{
	lr_save_string( lr_eval_string("bitrogue@mailinator.com"), "emailAddr");
//	lr_save_string( lr_eval_string("manny{randomFourDigits}@gmail.com"), "emailAddr");
	lr_think_time ( FORM_TT ) ;
	//lr_continue_on_error(0);

	lr_start_transaction("T14_Submit_RTPS");

	lr_start_sub_transaction("T14_Submit_RTPS_addSignUpEmail", "T14_Submit_RTPS" );
	#if OPTIONSENABLED
		web_add_header("Accept", "*/*");
		web_add_header("Access-Control-Request-Headers", "Origin, Host, User-Agent, Accept, Accept-Encoding, Accept-Language, Referer, Request, Content-Type, Transfer-Encoding, Cache-Control, Connection, Content-Length, Content-Language, Date, Expires, Keep-Alive, Location, pragma, client_id, client_secret, grant_type, scope, Authorization, Set-Cookie, Cookie, storeId, nickName, catalogId, langId, orderId, callingPage, requestType, paymentInstructionId, customerId, act, com, a1, city, state, postal, cols, opt, fromPage, promoCode, addressField1, addressField2, zipCode, pageName, locStore, responseFormat, coreName, term, count, isRest, espotName, orderItemId, productId, oldOrderItemId, memberId, emailId, fromRest, companyId, storeLocId, quantity, catEntryId, firstname, lastname, phone, email, action, taskType, stlocId, posStores, isBOPIS, xAppConfigAttrNames, tcpOrderId, PaRes, MD, externalId, pageNumber, pageSize, categoryId, primary, addressId, identifier, corenamecategory, published, rows, searchterm, start, timeallowed, devicetype, savePayment, calc");
		web_add_header("Access-Control-Request-Method", "CONNECT, DELETE, GET, OPTIONS, PATCH, POST, PUT, TRACE");
		web_add_header("Origin", "https://{host}");

		web_custom_request("addSignUpEmail",
			"URL=https://{api_host}/addSignUpEmail",
			"Method=OPTIONS",
			"Resource=0",
		LAST);
	#endif

	addHeader();
	web_custom_request("addSignUpEmail",
		"URL=https://{api_host}/addSignUpEmail",
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTML",
		"EncType=application/json",
		"Body={\"storeId\":\"{storeId}\",\"catalogId\":\"{catalogId}\",\"langId\":\"-1\",\"emailaddr\":\"{emailAddr}\",\"URL\":\"email-confirmation\",\"response\":\"accept_all::true:false\"}",
		LAST);
//		"Body={\"storeId\":\"10151\",\"catalogId\":\"10551\",\"langId\":\"-1\",\"emailaddr\":\"{emailAddr}\",\"URL\":\"email-confirmation\",\"response\":\"accept_all::true:false\"}",
	lr_end_sub_transaction("T14_Submit_RTPS_addSignUpEmail", LR_AUTO);

	lr_start_sub_transaction("T14_Submit_RTPS_getShipmentMethods", "T14_Submit_RTPS" );

	#if OPTIONSENABLED
		call_OPTIONS("payment/getShipmentMethods");
	#endif

	addHeader();
	web_add_header("state", lr_eval_string("{savedState}"));
	web_add_header("zipCode", lr_eval_string("{savedZipCode}"));
	web_add_header("addressField1", lr_eval_string("{savedAddressLine}"));

	web_url("getShipmentMethods",
		"URL=https://{api_host}/payment/getShipmentMethods",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	lr_end_sub_transaction("T14_Submit_RTPS_getShipmentMethods", LR_AUTO);

	lr_start_sub_transaction("T14_Submit_RTPS_addAddress", "T14_Submit_RTPS" );
	#if OPTIONSENABLED
		call_OPTIONS("payment/addAddress");
	#endif

	addHeader();
	web_add_header("Content-Type", "application/json");
	web_reg_save_param ( "addressId" , "LB=\"addressId\": \"" , "RB=\",\n" , "ORD=1", "NotFound=Warning", LAST ) ; //"addressId": "1398789",\n
	web_custom_request("addAddress",
		"URL=https://{api_host}/payment/addAddress",
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTML",
		"EncType=application/json",
		"Body={\"contact\":[{\"addressLine\":[\"500 Plaza Dr\",\"\",\"\"],\"attributes\":[{\"key\":\"addressField3\",\"value\":\"07094\"}],\"addressType\":\"ShippingAndBilling\",\"city\":\"Secaucus\",\"country\":\"US\",\"firstName\":\"always\",\"lastName\":\"approve\",\"nickName\":\"{randomNineDigits}{randomFourDigits}\",\"phone1\":\"201456{randomFourDigits}\",\"email1\":\"{emailAddr}\",\"phone1Publish\":\"true\",\"primary\":\"false\",\"state\":\"NJ\",\"zipCode\":\"07094\",\"xcont_addressField2\":\"2\",\"xcont_addressField3\":\"07094\",\"fromPage\":\"\"}]}",
		LAST);
	lr_end_sub_transaction("T14_Submit_RTPS_addAddress", LR_AUTO);

	lr_start_sub_transaction("T14_Submit_RTPS_updateShippingMethodSelection", "T14_Submit_RTPS" );
	#if OPTIONSENABLED
		call_OPTIONS("payment/updateShippingMethodSelection");
	#endif

	addHeader();
	web_add_header( "Content-Type", "application/json" );
	if (strcmp(lr_eval_string("{storeId}"), "10151") != 0)
		lr_save_string("900103", "shipmode_id");
	else
		lr_save_string("901101", "shipmode_id");

	web_reg_save_param ( "preScreenId" , "LB=\"prescreenId\": \"" , "RB=\"," , "ORD=1", "NotFound=Warning", LAST ) ; //"addressId": "1398789",\n
	web_custom_request("updateShippingMethodSelection",
		"URL=https://{api_host}/payment/updateShippingMethodSelection",
		"Method=PUT",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTML",
		"Body={\"shipModeId\":\"{shipmode_id}\",\"addressId\":\"{addressId}\",\"requesttype\":\"ajax\",\"prescreen\":true,\"x_calculationUsage\":\"-1,-2,-3,-4,-5,-6,-7\"}",
		LAST);
	lr_end_sub_transaction("T14_Submit_RTPS_updateShippingMethodSelection", LR_AUTO);

	lr_start_sub_transaction("T14_Submit_RTPS_processMadeOffer", "T14_Submit_RTPS" );
	//Yes I'm Interested
	#if OPTIONSENABLED
		call_OPTIONS("tcpstore/processMadeOffer");
	#endif
// add an if statement here for preScreenId not blank then continue with rest of the steps --03092018
	addHeader();
	web_custom_request("processMadeOffer",
		"URL=https://{api_host}/tcpstore/processMadeOffer",
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTML",
		"EncType=application/json",
		"Body={\"preScreenId\":\"{preScreenId}\",\"madeOffer\":\"true\"}",
		LAST);
	lr_end_sub_transaction("T14_Submit_RTPS_processMadeOffer", LR_AUTO);

	lr_start_sub_transaction("T14_Submit_RTPS_processPreScreenAcceptance", "T14_Submit_RTPS" );
//Submit to Open an Account
	#if OPTIONSENABLED
		call_OPTIONS("tcpstore/processPreScreenAcceptance");
	#endif
/*
	web_custom_request("processPreScreenAcceptance",
		"URL=https://{api_host}/tcpstore/processPreScreenAcceptance",
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTML",
		"EncType=application/json",
		"Body={\"firstName\":\"always\",\"lastName\":\"approve\",\"middleInitial\":\"\",\"address1\":\"500 Plaza Dr\",\"address2\":\"\",\"city\":\"Secaucus\",\"state\":\"NJ\",\"zipCode\":\"07094\",\"country\":\"US\",\"ssn\":\"{randomFourDigits}\",\"alternatePhoneNumber\":\"908510{randomFourDigits}\",\"emailAddress\":\"{emailAddr}\",\"birthdayDate\":\"{randomTwoDigits}031972\",\"mobilePhoneNumber\":\"201456{randomFourDigits}\",\"BF_ioBlackBox\":\""
		"{preScreenId}\"}",
		LAST);
*/
	addHeader();
  	registerErrorCodeCheck();
	web_reg_find("TEXT/IC=APPROVE", "SaveCount=apiCheck", LAST); //"returnCodeDescription": "APPROVE"

	web_custom_request("processPreScreenAcceptance",
		"URL=https://{api_host}/tcpstore/processPreScreenAcceptance",
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTML",
		"Body=firstName=always&lastName=approve&middleInitial=&address1=500%20Plaza%20Dr&address2=&city=Secaucus&state=NJ&zipCode=07094&country=US&ssn={randomFourDigits}&alternatePhoneNumber=908310{randomFourDigits}&emailAddress=manny{randomFourDigits}29%40gmail.com&birthdayDate={randomTwoDigits}031965&mobilePhoneNumber=908210{randomFourDigits}&BF_ioBlackBox="
		"0400nyhffR%2BvPCUNf94lis1zthjxeWKvEU0ry7uGpRvabX4dRxDSNQ9jsDXfueurujcaykJk9fR9lXIfdF254%2BODaNBcbj4gkm0misVi%2BOF1dNzwR%2BKf0PyWLIssn9qQ3uRPSdu3DZ2D0kFMgGebJUzGieKM40UsrTpCWhRAl1%2B3J5VDdd59r5gHspEIHZVXg4ncI8sel0fu1jEKOVTTL2g136yNzGkmK4M7jmU2L5yi9CUENPETrpKU6cYn8q04UaLxEp48DVRl0oBkPCJz7Ln%2Bv5KTIBvOIMzuTzEBdNLiXsPV6scyeMvquA%2Bwb5jRBwwIKAfKGWdG7VQns1b77hdqlZYwJVsFx3E%2FoYDerSQOKdZwPpPV0ZSMv6L5iCyHj4Tx50%2FEUPmGH7XaKK8QfyIp%2FMdwRBr0%2BK4w4SDfZTdHW4zG%2FjemUUwMNsQPcd6kSm4zm2UbrU22b3Pd6Wf1rrmlEKIGit"
		"1Q7ozZo2Ji61Ck5rdiHsRUrA6QSxbBfNyqZtyt8rblBKVJPt0pQs%2FEGzmZLxeqL4ckMPgBd6qI6vrqvBV2LM2Vog9QTv6a%2B9aA%2Beta6HPyNaJmTDaetuP1fPKCNMjlC7JHcnY4hCiebBE66IIElXD0hEMtvQl2olrNgUNPlLThLcwBzhuMYUNJo2ptcamjSPPHBXoKQX9Xfe%2BAojbIB9QxnksoWkMUrfnoDmSfSwVX8xgj%2FS5PLkXahPQKIlx0QxPOeeN5%2FXQXiHK5P5lKj%2BqHrqGCPLI%2BvS3zO1UWrlN%2BAwf0o9eusa2jjh0Vn%2BTQHjy7H5XhLHCHAGY3J0jpLqVrSVzsI1LB6M5fl2EoMEAoEitEttFUo1DRkUtPGG4lkbMj5hta0%2F%2F2N3v%2BZJl4VShbY%2BEVBDdML6ZhV5yEfobiOhcONJPNnBg89k56rNOHjs7HO5CL%2F%2Fli8nYbsqKV1%2F"
		"B3exlsY%2Bb8dIdPEb9km9ceHDKRCiLCgbgXPmizjHlWmCRBdNDNsuaxAwAEbRdjjZSkBTBViHxgPVj4eN%2FNGYWc8AmavPB%2F61YuvcgmcIodaHxf7ueiL3FWf8R3Mrc9Gx%2FanjFOBbiQbgKbAKMAiPU19Qu8SWX1NGAoXBpuw7czrCX1iPd6mFeGBU7NS06tiLOJVmOicOfVERHb8wLsDKThVP1hAyBTju8Izs6%2FKBQ1h4tNGHTh8CNjY0flnyuelCnt2ypgfW%2Fm3Zqxt3w1Bn6LxoWOnZu64rr07LwRGXZpyc7oQIV63xadNE2jqb6o3IAEdbB%2BxBDlc5ibQ6opsC2ODMcwYCSmkgFp7tSI9InUPcpWx6106%2BW3d%2FkBnCTVaSp6iBnY1zDKHwzk7mbtMSKFVCnQfPeHmfuwjlXY6eIIskGTT3HNIi7SoUevORp5jP%2BsHNo6TrJdXfRHmiPApB3Az0M%2FchYUNz"
		"iou1mCyrDeeEOCGK6BBwGGLYs5gZAFc%2BWqw46CU7ezeK9%2FVLHcAFA3D%2B68Xvc8dSjswabztj23W%2BiA%2FA1fuHGCj1LxYzacIivT1%2BIaXp2sv44JH8G9&prescreenId={preScreenId}",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
	{
		lr_fail_trans_with_error( lr_eval_string("T14_Submit_RTPS_processPreScreenAcceptance - Failed with  Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
		lr_end_sub_transaction("T14_Submit_RTPS_processPreScreenAcceptance", LR_FAIL);
		lr_end_transaction ( "T14_Submit_RTPS" , LR_FAIL ) ;
		//lr_continue_on_error(1);
		return LR_FAIL;
	}
	else
	{
		lr_end_sub_transaction("T14_Submit_RTPS_processPreScreenAcceptance", LR_AUTO);

		tcp_api2("payment/getPointsService", "GET",  "T14_Submit_RTPS"  );
		web_add_header("locStore", "True");
		web_add_header("pageName", "fullOrderInfo");
		web_reg_save_param("cartCount", "LB=artCount\": ", "RB=,", "NotFound=Warning", LAST);
		tcp_api2("getOrderDetails", "GET", "T14_Submit_RTPS" );

		lr_end_transaction ( "T14_Submit_RTPS" , LR_AUTO ) ;
		//lr_continue_on_error(1);
		return LR_PASS;
	}
}
