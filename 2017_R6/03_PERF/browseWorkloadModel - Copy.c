int NAV_BROWSE, NAV_SEARCH, NAV_CLEARANCE, NAV_PLACE; //2nd level (T02) NAV_BY ratio
int DRILL_ONE_FACET, DRILL_TWO_FACET, DRILL_THREE_FACET, DRILL_SUB_CATEGORY; //3rd level (T03) DRILL_BY ratio
int PDP, QUICKVIEW, PRODUCT_QUICKVIEW_SERVICE, RESERVE_ONLINE, MIXED_CART_RATIO, ECOMM_CART_RATIO, NO_BROWSE_RATIO_HOME, NO_BROWSE_RATIO_CATEGORY, NO_BROWSE_RATIO_SUBCATEGORY, NO_BROWSE_RATIO_PDP; //Product Display vs Product QuickView ratio (T04)
int APPLY_SORT, RATIO_TCPSkuSelectionView; 	PDP_to_WL;  //Sort ratio
int APPLY_PAGINATE, RATIO_STORE_LOCATOR, RATIO_SEARCH_SUGGEST, API_SUB_TRANSACTION_SWITCH, ESPOT_FLAG, GETPOINTSSERVICE_FLAG, CACHE_PRIME_MODE; //Pagination ratio //USE_LOW_INVENTORY,

	CACHE_PRIME_MODE = 0; //1 - cacheprime only, 0 - full scenario mode

	//set 2nd level navigation ratio for T02 transaction. Sum must be 100. Valid value is 0 to 100.

	NAV_BROWSE = 88;  //95;
	NAV_SEARCH = 12; //5;
	NAV_CLEARANCE = 0;
	NAV_PLACE = 0;

	//set drill navigation ratio for T03 transaction. Sum must be 100. Valid value is 0 to 100.
	DRILL_ONE_FACET = 11; //35; //25;
	DRILL_TWO_FACET = 3; //10; //15;
	DRILL_THREE_FACET = 1; //10;
	DRILL_SUB_CATEGORY = 85; //50;
/*
	DRILL_ONE_FACET = 100; //25;
	DRILL_TWO_FACET = 0; //15;
	DRILL_THREE_FACET = 0; //10;
	DRILL_SUB_CATEGORY = 100;
*/
	//set the % for SORT during browse activity. Valid value is 0 to 100. Used only in Browse scripts,
	APPLY_SORT = 25;

	//set the % for PAGINATION during browse activity, this will apply pagination only if additional pages are available. Valid value is 0 to 100. Used only in Browse scripts.
	APPLY_PAGINATE = 25; //75; //50; //100;

	//set PDP vs PQV view ratio (T04). Sum must be 100. Valid value is 0 to 100.
	//82/18 as per the 11/30/2015 Omniture data
//	PDP = 72;
//	QUICKVIEW = 28;
	PDP = 72;
	QUICKVIEW = 0; //no more quickview?

	RATIO_STORE_LOCATOR  = 5;
	RATIO_SEARCH_SUGGEST = 0; //50; //Did You Mean? descoped on 06/15
	RATIO_TCPSkuSelectionView = 20;

	PRODUCT_QUICKVIEW_SERVICE = 15; //30; //50;
	RESERVE_ONLINE = 100; //ropis
	ECOMM_CART_RATIO = 100;//60; //60; //80;
	BOPIS_CART_RATIO = 0;//40; //40; //20;


	API_SUB_TRANSACTION_SWITCH = 1; // 1 = ON; 0 = OFF
	BROWSE_OPTIONS_FLAG = 0; // 1 = ON; 0 = OFF
	ESPOT_FLAG = 1; //this is back on Drogonfly
	MULTI_USE_COUPON_FLAG = 0;
	GETPOINTSSERVICE_FLAG = 0;
	PDP_to_WL = 20; //20;  //T23_MoveFromCartToWishlist


	NO_BROWSE_RATIO				= 100; //80; //70;
	NO_BROWSE_RATIO_HOME		= 100; //17;
	NO_BROWSE_RATIO_CATEGORY	= 100; //3;
	NO_BROWSE_RATIO_SUBCATEGORY	= 100; //17;
	NO_BROWSE_RATIO_PDP			= 100; //24;

//with akamai
/*
 	NO_BROWSE_RATIO = 80; //70;
	NO_BROWSE_RATIO_HOME		= 25;
	NO_BROWSE_RATIO_CATEGORY	= 25;
	NO_BROWSE_RATIO_SUBCATEGORY	= 25;
	NO_BROWSE_RATIO_PDP			= 30;
*/

//03162018
/*
 	NO_BROWSE_RATIO = 0; //70;
	NO_BROWSE_RATIO_HOME		= 0;
	NO_BROWSE_RATIO_CATEGORY	= 0;
	NO_BROWSE_RATIO_SUBCATEGORY	= 0;
	NO_BROWSE_RATIO_PDP			= 0;
*/
