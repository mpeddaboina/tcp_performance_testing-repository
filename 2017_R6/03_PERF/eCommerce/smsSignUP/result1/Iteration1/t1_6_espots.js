/**
 * @authors Constantin Bigu, Carlos Henriquez, Nicole Phillips, Carla Crandall
 * @description This houses espots controllers. The file is loaded in the footer across the app.
 * @restrictions Use vanilla JS
 */
(function() {
    // Email signup modal functionality - Only displayed for first visit
    var emailSignupModal = setInterval(function() {
        var modal = document.querySelector('#email-signup-modal');

        if (modal) {
            clearInterval(emailSignupModal);

            // Add event listeners
            var form = modal.querySelector('form');
            form && form.addEventListener('submit', EmailSignupFunctions.submitForm);

            var closeBtn = modal.querySelector('.email-signup-modal-header button');
            closeBtn && closeBtn.addEventListener('click', function() {
                modal.style.display = 'none';
            });

            var termsLink = modal.querySelector('.email-signup-terms');
            termsLink && termsLink.addEventListener('click', function() {
                modal.querySelector('.email-signup-full-terms').style.display = 'block';
            })

            // If cookie doesn't exist...
            // Show the modal
            // Create the cookie so modal doesn't show next time
            var cookieName = 'tcp_email_signup_modal_persistent';
            if (!CookieUtils.readCookie(cookieName)) {
                modal.style.display = 'block';
                CookieUtils.setCookie(cookieName, new Date().toGMTString(), '.childrensplace.com');
            }
        }
    }, 300);

    /* MPR Promo Banner Above Header - cookies */
    var MPRinterval = setInterval(function() {
        var bannerContainer = document.getElementById('header-global-banner-container');

        if (bannerContainer) {
            clearInterval(MPRinterval);

            // Run scripts when the page first loads
            runBannerScripts();

            // Use mutation observer to watch React root element (div#tcp)
            // Have to use the root because sometimes the entire app is re-rendered (DT-27336)
            var observer = new MutationObserver(function(mutations) {
                // Loop through each mutation and check the target
                // If target is the banner or the React root element, trigger the scripts
                for (var i = 0; i < mutations.length; i++) {
                    var target = mutations[i].target;
                    if (target.className === 'header-global-banner' || target.id === 'tcp') {
                        runBannerScripts();
                    }
                }
            });
            observer.observe(document.getElementById('tcp'), {
                childList: true,
                subtree: true
            });
        }

        function runBannerScripts() {
            var cookieName = 'mprAboveHead_' + CookieUtils.getUUID('WC_USERACTIVITY_');
            var banner = document.getElementById('header-global-banner-container');
            var closeBtn = document.getElementById('myPlace-rewards-close');

            if (banner && closeBtn) {
                banner.style.display = (!CookieUtils.readCookie(cookieName)) ? 'block' : 'none';

                // Hide banner and set cookie when user clicks "close"
                closeBtn.addEventListener('click', function() {
                    banner.style.display = 'none';
                    var currentDate = new Date();
                    CookieUtils.setCookie(cookieName, currentDate.toGMTString(), '.childrensplace.com');
                });
            }
        }
    }, 300);

    /* Free Shipping Global Header banner Modal Functionality */
    var detailsModal = setInterval(function() {
        var seeDetails = document.querySelector('.openModal-FS');
        var modalContent = document.querySelector('.fs-global-modal');
        var closeModalButton = document.querySelector('.button-overlay-close');

        if (seeDetails) {
            function openModal(e) {
                e.preventDefault();
                modalContent.style.display = 'block';
            }

            function closeModal(e) {
                e.preventDefault();
                modalContent.style.display = 'none';
            }

            closeModalButton && closeModalButton.addEventListener('click', closeModal);
            seeDetails.addEventListener('click', openModal);
            clearInterval(detailsModal);
        }
    }, 300);

    /* Free Shipping Bag/Cart Modal Functionality */
    var bagDetailsModal = setInterval(function() {
        var seeDetailsBag = document.querySelector('.openModal-bag-FS');
        var modalContentBag = document.querySelector('.bag-modal');
        var closeModalButtonBag = document.querySelector('.close-bag-modal');

        if (seeDetailsBag) {
            function openModal(e) {
                e.preventDefault();
                modalContentBag.style.display = 'block';
            }

            function closeModal(e) {
                e.preventDefault();
                modalContentBag.style.display = 'none';
            }

            closeModalButtonBag.addEventListener('click', closeModal);
            seeDetailsBag.addEventListener('click', openModal);
            clearInterval(bagDetailsModal);
        }
    }, 300);

    /* Simple slider for the global header within header, using setInteval. */
    var simpleSlider = setInterval(function() {
        var slideContainer = document.getElementById('slide-container');
        var currentSlide = 0;
        var playing = true;
        var slideInterval;

        if (slideContainer) {
            clearInterval(simpleSlider);
            var slides = document.querySelectorAll('#slides .slide__item');
            var pauseBtn = slideContainer.querySelector('.pause-slideshow-btn');

            function nextSlide() {
                // Use the remainder to set the current slide and return to the beginning when you reach the end
                var prevSlide = currentSlide;
                currentSlide = (currentSlide + 1) % slides.length;

                slides[currentSlide].style.visibility = 'visible';
                slides[prevSlide].className = 'slide__item';
                slides[currentSlide].className = 'slide__item slide--active';

                // Visibility needed to prevent screen readers from reading hidden slides
                // Wait for fade animation to finish before updating visibility
                setTimeout(function() {
                    slides[prevSlide].style.visibility = 'hidden';
                }, 1000);
            }

            function startSlideshow() {
                // Make sure screen readers won't read hidden slides
                var inactiveSlides = document.querySelectorAll('#slides .slide__item:not(.slide--active)');
                for (var i = 0; i < inactiveSlides.length; i++) {
                    inactiveSlides[i].style.visibility = 'hidden';
                }

                var activeSlide = document.querySelector('#slides .slide__item.slide--active');
                activeSlide.style.visibility = 'visible';

                playSlideshow();
            }

            function playSlideshow() {
                playing = true;

                if (pauseBtn) {
                    pauseBtn.innerHTML = 'Pause Slideshow';
                }

                slideInterval = setInterval(nextSlide, 3000);
            }

            function pauseSlideshow() {
                playing = false;

                if (pauseBtn) {
                    pauseBtn.innerHTML = 'Resume Slideshow';
                }

                clearInterval(slideInterval);
            }

            function checkForSlideshowBlur() {
                setTimeout(function() {
                    var slideshow = document.getElementById('slide-container');
                    var focusIsInSlideshow = slideshow.contains(document.activeElement);

                    // If focus has moved outside of the container div
                    // Restart the slideshow
                    if (!focusIsInSlideshow) {
                        playSlideshow();
                    }
                });
            }

            startSlideshow();

            // Pause on hover
            slideContainer.addEventListener('mouseover', pauseSlideshow);
            slideContainer.addEventListener('mouseleave', playSlideshow);

            // Pause when button receives focus, Restart when focus is outside of slideshow
            pauseBtn && pauseBtn.addEventListener('focus', pauseSlideshow);
            pauseBtn && pauseBtn.addEventListener('blur', checkForSlideshowBlur);

            // Add event listeners for the slides
            var links = document.querySelectorAll('#slides .slide__item > *');
            for (var i = 0; i < links.length; i++) {
                links[i].addEventListener('focus', pauseSlideshow);
                links[i].addEventListener('blur', checkForSlideshowBlur);
            }
        }
    }, 300);

    /* ---------------- COOKIE UTILS -----------------*/
    var CookieUtils = {
        getUUID: function(uuidCookieString) {
            var _UUID = CookieUtils.findCookieStartingWithString(uuidCookieString);
            return (_UUID) ? _UUID.split(',')[0] : '-1002';
        },

        setCookie: function(cookieName, cookieValue, cookieDomain) {
            var d = new Date();
            d.setTime(d.getTime() + 10 * 24 * 60 * 60 * 1000);
            document.cookie = cookieName + '=' + cookieValue + '; path=/' + '; domain=' + cookieDomain + '; expires=' + d.toGMTString();
        },

        readCookie: function(cookieName) {
            var a_all_cookies = document.cookie.split(';');
            var a_temp_cookie = '';
            var cookie_name = '';
            var cookie_value = '';
            var b_cookie_found = false;

            for (i = 0; i < a_all_cookies.length; i++) {
                a_temp_cookie = a_all_cookies[i].split('=');
                cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g, '');

                if (cookie_name == cookieName) {
                    b_cookie_found = true;

                    if (a_temp_cookie.length > 1) {
                        cookie_value = unescape(a_temp_cookie[1].replace(/^\s+|\s+$/g, ''));
                    }

                    return cookie_value;
                    break;
                }

                a_temp_cookie = null;
                cookie_name = '';
            }

            if (!b_cookie_found) {
                return null;
            }
        },

        findCookieStartingWithString: function(keyword) {
            var cookieArr = document.cookie.split(';'),
                cookies = {},
                name_value,
                equals_pos,
                name,
                value;

            for (var i = 0; i < cookieArr.length; i++) {
                name_value = cookieArr[i];
                equals_pos = name_value.indexOf('=');
                name = unescape(name_value.slice(0, equals_pos)).trim();
                value = unescape(name_value.slice(equals_pos + 1));
                cookies[name] = value;
            }

            for (name in cookies) {
                var value = cookies[name];

                if (name.indexOf(keyword) == 0) {
                    return value;
                }
            }
        }
    };

    /* ---------------- SIGNUP FORM UTILS -----------------*/
    // TODO: Consolidate with emailSubscribe.js
    var EmailSignupFunctions = {
        submitForm: function(e) {
            e.preventDefault();
            var email = e.target.querySelector('input[name="email"]').value;
            EmailSignupFunctions.brightVerify(email);
        },

        brightVerify: function(email) {
            $.ajax({
                url: 'https://bpi.briteverify.com/emails.json',
                type: 'GET',
                contentType: 'application/json',
                dataType: 'jsonp',
                data: {
                    apikey: 'e50ab0a9-ac0b-436b-9932-2a74b9486436',
                    address: email
                },
                success: function(res) {
                    var status = res.status;

                    if (status === 'valid' || status === 'accept_all') {
                        EmailSignupFunctions.toggleError();
                        EmailSignupFunctions.signupUser(email);
                    } else {
                        EmailSignupFunctions.toggleError('Oops! We need a valid email address to send you updates.');
                    }
                },
                error: function(err) {
                    EmailSignupFunctions.toggleError('Oops! We need a valid email address to send you updates.');
                }
            });
        },

        signupUser: function(email) {
            var storeId = (window.utag_data && window.utag_data.store_id) || '10151';

            $.ajax({
                url: '/api/addSignUpEmail',
                type: 'post',
                contentType: 'application/json',
                headers: {
                    storeId: storeId
                },
                data: JSON.stringify({
                    storeId: storeId,
                    catalogId: (window.utag_data && window.utag_data.catalog_id) || '10051',
                    langId: '-1',
                    emailaddr: email,
                    URL: 'email-confirmation',
                    response: 'valid::false:false'
                }),
                success: function(res) {
                    var isMobile = $('.email-signup-submit.hide-desktop').css('display') !== 'none';

                    if (isMobile) {
                        // Mobile success stays on same page with thank you message
                        EmailSignupFunctions.signupSuccessMobile();
                    } else {
                        // Desktop success redirects to new URL
                        EmailSignupFunctions.signupSuccessDesktop(res);
                    }
                },
                error: function(err) {
                    console.warn(err);
                    EmailSignupFunctions.toggleError('Something went wrong. Please try again.');
                }
            });
        },

        signupSuccessMobile: function() {
            $('.email-signup-mobile-heading').html('Thanks for signing up!');
            $('#signupFormPage').hide();
            $('.email-signup-thanks').show();

            // Wait 5 seconds, then hide the banner
            setTimeout(function() {
                $('#email-signup-modal').hide()
            }, 5000);
        },

        signupSuccessDesktop: function(res) {
            if (res && res.redirecturl) {
                window.location.href = res.redirecturl;
            }
        },

        toggleError: function(message) {
            var error = document.querySelector('#signupFormPage .email-signup-error');
            error.innerHTML = message ? message : '';
        }
    };
})();

/* =========== JAVASCRIPT FUNCTIONALITY FOR CONTENT PAGES ========= */
/*
*
*
This is for content pages due to character/line limit issue in CMC */
(function() {
    var LoadScriptUtils = {
        formatPathName: function() {
            // Remove the trailing slash
            var pathname = window.location.pathname.replace(/\/$/, "");
            var split = pathname.split('/');

            // Stip out country from the path
            if (split[1] === 'us' || split[1] === 'ca') {
                pathname = pathname.substring(3);
            }

            return pathname;
        },

        // Wait for jQuery to load
        waitForJquery: function(method) {
            if (window.jQuery) {
                method();
            } else {
                setTimeout(function() {
                    LoadScriptUtils.waitForJquery(method)
                }, 50);
            }
        },

        // Wait for jQuery UI to load
        waitForJqueryUI: function(method) {
            if (window.jQuery.ui) {
                method();
            } else {
                setTimeout(function() {
                    LoadScriptUtils.waitForJqueryUI(method)
                }, 50);
            }
        },

        // Wait for rwdImageMaps to load
        waitForRwdImageMap: function(method) {
            if ($.fn.rwdImageMaps) {
                method();
            } else {
                setTimeout(function() {
                    LoadScriptUtils.waitForRwdImageMap(method)
                }, 50);
            }
        },

        // Wait for bxSlider to load
        waitForBxSlider: function(method) {
            if ($.fn.bxSlider) {
                method();
            } else {
                setTimeout(function() {
                    LoadScriptUtils.waitForBxSlider(method)
                }, 50);
            }
        },

        // General use dependency waiting helper function
        waitForDependency: function(dependency, method, timeoutLength) {
            var timeout = (timeoutLength || 50) + 1;

            // base case: if call has been called to many time due to timeoutLength being ++ in recursive call
            if (timeout > 100) {
                return;
            }
            window[dependency] ?
                method() :
                setTimeout(function() {
                    LoadScriptUtils.waitForDependency(dependency, method, timeout)
                }, timeout);
        },

        loadScript: function(url) {
            var scriptTag = document.createElement('script');
            scriptTag.type = 'text/javascript';
            scriptTag.src = url;
            document.body.appendChild(scriptTag);
        },

        loadCSS: function(url) {
            var cssTag = document.createElement('link');
            cssTag.rel = 'stylesheet';
            cssTag.type = 'text/css';
            cssTag.href = url;
            document.body.appendChild(cssTag);
        }
    };

    var INIT_FUNCTIONS = {

        /*
         * PAGE INIT FUNCTIONS
         */

        global: function() {
            // Load files that need to be available globally in here
            // WIC modal could be launched from any page due to global header banner
            INIT_FUNCTIONS.wicModal();
            INIT_FUNCTIONS.rotateBanner();

        },

        homePage: function(pathname) {
            // Only load scripts if on home page
            if (pathname.indexOf('/home') > -1) {
                INIT_FUNCTIONS.commonFunctionality();
            }
        },

        contentPage: function(pathname) {
            // Only load scripts if on content page URL
            if (pathname.indexOf('/content/') > -1) {
                INIT_FUNCTIONS.commonFunctionality();
                // Some pages have extra functionality
                CONTENT_PAGES[pathname] && CONTENT_PAGES[pathname]();
            }
        },

        departmentPage: function(pathname) {
            // Only load scripts if on department page URL
            if (DEPARTMENT_PAGES.indexOf(pathname) > -1) {
                INIT_FUNCTIONS.commonFunctionality()
            }
        },

        pdp: function(pathname) {
            // Only load scripts if on PDP url
            if (pathname.indexOf('/p/') > -1) {
                INIT_FUNCTIONS.sizeChart();
            }
        },

        outfit: function(pathname) {
            // Only load scripts if on outfit url
            if (pathname.indexOf('/outfit/') > -1) {
                INIT_FUNCTIONS.sizeChart();
            }
        },

        // Functionality that is commonly used throughout the site
        // For home page, content pages, and department pages
        commonFunctionality: function() {
            INIT_FUNCTIONS.accordion();
            INIT_FUNCTIONS.getcandid();
            INIT_FUNCTIONS.contentModal();
            INIT_FUNCTIONS.contentTabs();
            INIT_FUNCTIONS.dropDown();
            INIT_FUNCTIONS.hoverImageReplacement();
            INIT_FUNCTIONS.imageMap();
            INIT_FUNCTIONS.imageMapAreaEvents();
            INIT_FUNCTIONS.imageSlider();
        },

        /*
         * FUNCTIONALITY INIT FUNCTIONS
         */

        /*------------ JQUERY Rotating Banner ------------*/
        rotateBanner: function() {
            var bannerInterval;

            function setHeight() {
                $("#sliderBanner > div:gt(0)").hide();
                $("#sliderBanner").height($("#sliderBanner > div > .imgbanner").height());
            }

            function rotateBanner() {
                bannerInterval = setInterval(function() {
                    $('#sliderBanner > div:first')
                        .fadeOut(1000)
                        .next()
                        .fadeIn(1000)
                        .end()
                        .appendTo('#sliderBanner');
                }, 3000);
            }

            function pauseBanner() {
                clearInterval(bannerInterval);
            }

            // When page loads...
            setHeight();
            rotateBanner();

            $(document).on('focus', '.master-pause .rotate-pause', pauseBanner);
            $(document).on('blur', '.master-pause .rotate-pause', rotateBanner);
            $(document).on('mouseenter', '#sliderBanner', pauseBanner);
            $(document).on('mouseleave', '#sliderBanner', rotateBanner);
        },

        accordion: function() {
            LoadScriptUtils.loadScript('/wcsstore/GlobalSAS/javascript/tcp/content_pages/accordion.js');
        },

        // DOCS: https://support.getcandid.com/support/solutions/articles/5000524031-widget-properties
        getcandid: function() {
            var carouselId = 'tcp-get-candid-container';
            var wallId = 'tcp-get-candid-wall-container';
            var apiKey = '070167ca-8287-4d41-a9bb-6b3850cae9b1';
            var isCarousel = document.getElementById(carouselId);
            var isWall = document.getElementById(wallId);
            var isContainerOnPage = isCarousel || isWall;

            if (isContainerOnPage) {
                LoadScriptUtils.loadScript('https://api.getcandid.com/scripts/widget.js');
                LoadScriptUtils.waitForDependency('candid', function() {
                    isCarousel && candid.init({
                        id: apiKey,
                        containerId: carouselId,
                        cluster: 'prod-2',
                        tag: 'homepage'
                    });
                    if (isWall) {
                        LoadScriptUtils.loadScript('https://api.getcandid.com/scripts/wall-isotope.js');
                        LoadScriptUtils.waitForDependency('Isotope', function() {

                            candid.wall('#' + wallId, {
                                id: apiKey,
                                cluster: 'prod-2',
                                layoutMode: 'packery',
                                layout: 'isotope-packery',
                                tag: 'gallery'
                            });
                        });
                    }
                });
            }
        },

        contentModal: function() {
            LoadScriptUtils.loadCSS('/wcsstore/GlobalSAS/css/tcp/jquery_ui/jquery-ui.min.css');
            LoadScriptUtils.loadScript('/wcsstore/GlobalSAS/javascript/tcp/libs/jquery-ui-1.11.3.min.js');

            LoadScriptUtils.waitForJqueryUI(function() {
                LoadScriptUtils.loadScript('/wcsstore/GlobalSAS/javascript/tcp/content_pages/contentModal.js');
            });
        },

        contentTabs: function() {
            LoadScriptUtils.loadScript('/wcsstore/GlobalSAS/javascript/tcp/content_pages/contentTabs.js');
        },

        dropDown: function() {
            LoadScriptUtils.loadScript('/wcsstore/GlobalSAS/javascript/tcp/content_pages/dropDown.js');
        },

        emailSubscribe: function() {
            LoadScriptUtils.loadScript('/wcsstore/GlobalSAS/javascript/tcp/content_pages/emailSubscribe.js');
        },

        hoverImageReplacement: function() {
            LoadScriptUtils.loadScript('/wcsstore/GlobalSAS/javascript/tcp/content_pages/hoverImageReplacement.js');
        },

        imageMap: function() {
            LoadScriptUtils.loadScript('/wcsstore/GlobalSAS/javascript/tcp/jquery.rwdImageMaps.js');

            // Wait for rwdImageMaps to load before running script
            LoadScriptUtils.waitForRwdImageMap(function() {
                LoadScriptUtils.loadScript('/wcsstore/GlobalSAS/javascript/tcp/content_pages/imageMap.js');
            });
        },

        imageMapAreaEvents: function() {
            LoadScriptUtils.loadScript('/wcsstore/GlobalSAS/javascript/tcp/content_pages/imageMapAreaEvents.js');
        },

        imageSlider: function() {
            LoadScriptUtils.loadScript('/wcsstore/GlobalSAS/javascript/tcp/jquery.bxslider.min.js');

            // Wait for bxSlider to load before running script
            LoadScriptUtils.waitForBxSlider(function() {
                LoadScriptUtils.loadScript('/wcsstore/GlobalSAS/javascript/tcp/content_pages/imageSliderSimple.js');
                LoadScriptUtils.loadScript('/wcsstore/GlobalSAS/javascript/tcp/content_pages/imageSliderWithPager.js');
            });
        },

        sizeChart: function() {
            LoadScriptUtils.loadScript('/wcsstore/GlobalSAS/javascript/tcp/content_pages/sizeChart.js');
        },

        storeScroll: function() {
            LoadScriptUtils.loadScript('/wcsstore/GlobalSAS/javascript/tcp/content_pages/scrollToStores.js');
        },

        toggleDetails: function() {
            LoadScriptUtils.loadScript('/wcsstore/GlobalSAS/javascript/tcp/content_pages/toggleCouponDetails.js');
        },

        wicModal: function() {
            LoadScriptUtils.loadScript('/wcsstore/GlobalSAS/javascript/tcp/content_pages/wicModal.js');
        },

        smsLanding: function() {
            LoadScriptUtils.waitForJquery(function() {
                LoadScriptUtils.loadScript('/wcsstore/GlobalSAS/javascript/tcp/content_pages/smsLanding.js');
            });
        },
        kidstylesquad: function() {
            LoadScriptUtils.waitForJquery(function() {
                LoadScriptUtils.loadScript('/wcsstore/GlobalSAS/javascript/tcp/content_pages/styleSquadForm.js');
            });
        },
        kidstylesquadoutfit: function() {
            LoadScriptUtils.waitForJquery(function() {
                LoadScriptUtils.loadScript('/wcsstore/GlobalSAS/javascript/tcp/content_pages/kidsstylesquad.js');
            });
        }
    };

    // All content pages load the "common functionality"
    // If a page needs some specific functionality / file, include it in the list below
    var CONTENT_PAGES = {
        '/content/size-chart': INIT_FUNCTIONS.sizeChart,
        '/content/international-stores': INIT_FUNCTIONS.storeScroll,
        '/content/childrens-place-coupons': INIT_FUNCTIONS.toggleDetails,
        '/content/black-friday': INIT_FUNCTIONS.emailSubscribe,
        '/content/cyber-monday': INIT_FUNCTIONS.emailSubscribe,
        '/content/email-subscribe': INIT_FUNCTIONS.emailSubscribe,
        '/content/mobile': INIT_FUNCTIONS.emailSubscribe,
        '/content/sms': INIT_FUNCTIONS.smsLanding,
        '/content/style-squad-form': INIT_FUNCTIONS.kidstylesquad,
        '/content/girls-style-squad-outfits': INIT_FUNCTIONS.kidstylesquadoutfit,
        '/content/boys-style-squad-outfits': INIT_FUNCTIONS.kidstylesquadoutfit
    };

    // All department pages load the "common functionality"
    var DEPARTMENT_PAGES = [
        '/c/girls-clothing', '/c/47511', '/c/14501',
        '/c/boys-clothing', '/c/47503', '/c/14503',
        '/c/toddler-girl-clothes', '/c/47502', '/c/14502',
        '/c/toddler-boy-clothes', '/c/47501', '/c/14504',
        '/c/baby-clothes', '/c/47504', '/c/14505',
        '/c/childrens-shoes-kids-shoes', '/c/childrens-shoes-kids-shoes-canada', '/c/61534', '/c/61531',
        '/c/kids-accessories-us', '/c/kids-accessories-canada', '/c/61533', '/c/61530',
        '/c/girls-outfit', '/c/toddler-girl-outfit', '/c/boys-outfit', '/c/toddler-boy-outfit', '/c/girls-outfits', '/c/toddler-girl-outfits', '/c/boys-outfits', '/c/toddler-boy-outfits'
    ];

    // Load jQuery, Then load all other scripts
    LoadScriptUtils.loadScript('/wcsstore/GlobalSAS/javascript/tcp/libs/jquery-1.11.3.min.js');
    LoadScriptUtils.waitForJquery(function() {
        var pathname = LoadScriptUtils.formatPathName();

        INIT_FUNCTIONS.global(); // scripts for all pages
        INIT_FUNCTIONS.homePage(pathname); // scripts for home page
        INIT_FUNCTIONS.pdp(pathname); // scripts for PDP pages
        INIT_FUNCTIONS.outfit(pathname); // scripts for outfit pages
        INIT_FUNCTIONS.contentPage(pathname); // scripts for content pages
        INIT_FUNCTIONS.departmentPage(pathname); // scripts for department pages
    });
})();