Action()
{
	web_set_sockets_option("SSL_VERSION", "TLS1.1");  //02232018

	lr_start_transaction( "T01_Home Page" ) ;
	
	web_custom_request("smsSignUpHOME",
		"URL=https://tcp-perf.childrensplace.com/us/home/",
		"Method=GET",
		"Resource=0",
		"RecContentType=text/html",
		"EncType=application/x-www-form-urlencoded",
		LAST);

	lr_end_transaction("T01_Home Page", LR_AUTO);

	web_reg_find("TEXT/IC=participation_date", "SaveCount=apiCheck", LAST);
	
	lr_start_transaction("T12_SMS_SignUp");
	
	//addHeader();
	web_add_header( "Content-Type", "application/json" );
	web_custom_request("smsSignUp",
		"URL=https://tcp-perf.childrensplace.com/api/tcpproduct/smsSignUp",
		"Method=POST",
		"Resource=0",
		"Body={\"mobile_phone\":{\"mdn\":\"299555{randomFourDigit}\"},\"custom_fields\":{\"zip_pos\":\"19702\",\"src_cd\":\"1\",\"sub_src_cd\":\"sms_landing_page\"}}",
		LAST);
	
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T12_SMS_SignUp", LR_FAIL);
	else
		lr_end_sub_transaction("T12_SMS_SignUp", LR_AUTO);
	
	return 0;
}
