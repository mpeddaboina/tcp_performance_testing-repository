dropCart()
{
	lr_user_data_point("Abandon Cart Rate", 1);
	lr_user_data_point("Checkout Rate", 0);
	lr_save_string("dropCartFlow", "currentFlow");
	
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CHECKOUT_GUEST ) {
		dropCartGuest();
	} 
	else {
		lr_save_string( lr_eval_string("{logonid_drop}"), "userEmail" );

		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CHECKOUT_LOGIN_FIRST ) 
		{
			dropCartLoginFirst();
        }
        else { 
			dropCartBuildCartFirst();
		} 
	}
	return 0;
}

dropCartGuest()
{
	lr_save_string("1", "totalNumberOfItems_count");
	if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= NO_BROWSE_RATIO )
    	buildCartDropNoBrowse(0);//
	else {
		buildCartDrop(0);
	}

	if (CACHE_PRIME_MODE == 1)
		return 0; 

	inCartEdits();

	if (proceedAsGuest() == LR_PASS)
	{
		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= LOGIN_PAGE_DROP )
		{		dropCartTransaction();
				return 0;
		}

		if ( submitShipping() == LR_PASS)
		{
			if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= BILL_PAGE_DROP )
			{	dropCartTransaction();
				return 0;
			}

			if (BillingAsGuest() == LR_PASS)
				dropCartTransaction();
		}
	}
	
	return 0;		
}

dropCartLoginFirst()
{
	int viewHistory = atoi(lr_eval_string("{RANDOM_PERCENT}"));
	isLoggedIn=1;	
	if (loginFromHomePage() == LR_PASS)
	{
        if ( atoi(lr_eval_string("{RANDOM_WL_PERCENT}")) <= RATIO_ACCOUNT_OVERVIEW )
        	viewMyAccount();
        
	    viewCart();  //0712TookOut, 0713PutBack

		if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= NO_BROWSE_RATIO )
	    	buildCartDropNoBrowse(1);//
		else {
			buildCartDrop(1);
		}

		if (CACHE_PRIME_MODE == 1)
			return 0; 

        if ( atoi(lr_eval_string("{RANDOM_WL_PERCENT}")) <= RATIO_ACCOUNT_OVERVIEW )
        	viewMyAccount();
        
		if ( strcmp(strupr(lr_eval_string("{buildCart}")),  "TRUE") != 0) 
			inCartEdits();
        
		if (proceedToCheckout_ShippingView() == LR_PASS) //proceedToCheckout_ShippingView(); //proceedAsRegistered();
		{

	        if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= SHIP_PAGE_DROP )
			{	dropCartTransaction();
				return 0;
			}
	        
			if (submitShippingRegistered() == LR_PASS)
			{
    
	            if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= BILL_PAGE_DROP )
				{		dropCartTransaction();
						return 0;
				}

				if (submitBillingRegistered() == LR_PASS)
					dropCartTransaction();

            }
        }
	}
	
	return 0;            
	
}

dropCartBuildCartFirst()
{
	lr_save_string("1", "totalNumberOfItems_count");
	
	if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= NO_BROWSE_RATIO )
    	buildCartDropNoBrowse(0);//
//		buildCartDrop(0);
	else {
		buildCartDrop(0);
	}

	if (CACHE_PRIME_MODE == 1)
		return 0; 

	inCartEdits();
    
//    proceedToCheckout();
 
    if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= LOGIN_PAGE_DROP )
	{	dropCartTransaction();
		return 0;
	}

	isLoggedIn=1;	
	if (login() == LR_PASS) 
	{

        if ( atoi(lr_eval_string("{RANDOM_WL_PERCENT}")) <= RATIO_ACCOUNT_OVERVIEW )
        	viewMyAccount();
        
	    if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= SHIP_PAGE_DROP )
		{	dropCartTransaction();
			return 0;
		}

		if (submitShippingBrowseFirst() == LR_PASS) 
		{
	        if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= BILL_PAGE_DROP )
			{		dropCartTransaction();
					return 0;
			}

		     if (submitBillingRegistered() == LR_PASS ) 
				dropCartTransaction();
        }
    }
	
	return 0;
}