Action()
{
//	web_set_sockets_option("SSL_VERSION", "TLS"); 
	web_set_sockets_option("SSL_VERSION", "TLS1.1");  //02232018
	
	//This is R5 version E:\Performance\Scripts\2017_R5
	web_set_max_html_param_len ( "3072" ) ;
	web_cleanup_cookies () ;
	web_cache_cleanup();
	isLoggedIn=0;
	ONLINE_ORDER_SUBMIT=0;
	API_SUB_TRANSACTION_SWITCH = 1;

	if (CACHE_PRIME_MODE == 1)
		RATIO_CHECKOUT_LOGIN_FIRST = 0;
//	CACHE_PRIME_MODE = 0;
	
//	Temp Debugging Parameters
//	RATIO_PROMO_MULTIUSE = 0;
//	CACHE_PRIME_MODE = 0; //1 - cacheprime only, 0 - full scenario mode
//	RATIO_PROMO_SINGLEUSE = 100;
//	RATIO_DROP_CART = 0; //45;
//	RATIO_CHECKOUT_GUEST = 0; 
//	RATIO_CHECKOUT_LOGIN_FIRST = 0;
//	RATIO_POINTS_HISTORY = 0;
//	RATIO_ORDER_HISTORY = 0;
//	RATIO_REDEEM_LOYALTY_POINTS = 0;
	lr_save_string("10151", "storeId");
	lr_save_string("10551", "catalogId");
	lr_save_string("us", "country");
	lr_save_string("false", "addBopisToCart");
	lr_save_string("false", "addEcommToCart");
	lr_save_string(lr_eval_string("{RANDOM_PERCENT}"), "akamaiRatio" );
	lr_save_string("false", "isExpress" );
	
//	lr_save_string( lr_eval_string("{logonid}"), "userEmail" );
//	lr_save_string( "manny123456@gmail.com", "userEmail" ); //uatlive3
//	lr_save_string( "manny987654@gmail.com", "userEmail" ); //uatlive2
//	lr_save_string( "manny456789@gmail.com", "userEmail" ); //uatlive1
//	lr_save_string( "manny654321@gmail.com", "userEmail" ); //perflive
/*	
loginFirst	
loginFromHomePage();	
viewCart();
proceedToCheckout_ShippingView();
submitShippingRegistered();
submitBillingRegistered();
submitOrderRegistered();
*/
/*
//browse first
//buildCartCheckout(0);
login();
accountRewards();	
lr_abort();
submitShippingBrowseFirst();
submitBillingRegistered();
submitOrderRegistered();
lr_abort();

//	StoreLocator();
//	profile();
//	storeLocator();
//	viewMyAccount();
//	webInstantCredit();
//buildCartCheckout(0);	
*/
/*			web_reg_save_param_json("ParamName=uniqueId", "QueryString=$..catalogEntryView..[?(@.resourceId)].uniqueID", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
			web_reg_save_param_json("ParamName=productId", "QueryString=$..catalogEntryView..[?(@.resourceId)].uniqueID", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
			web_reg_save_param_json("ParamName=pdpURL", "QueryString=$..catalogEntryView..[?(@.resourceId)].uniqueID", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);

			addHeader();
			web_add_header( "categoryId", "girls-leggings" );
			web_add_header( "pageNumber", "2" );
			web_add_header( "pageSize", "20" );
			web_add_header( "searchSource", "E" );
			web_add_header( "searchType", "-1,-1,0" );

			web_custom_request("getProductsBySearchTerm",
				"URL=https://{api_host}/tcpproduct/getProductviewbyCategory?departments=",
				"Method=GET",
				"Resource=0",
				"RecContentType=text/html",
				"EncType=application/x-www-form-urlencoded",
				LAST);
			lr_abort();
*/			
//navByBrowse();
//drillOneFacet();

//lr_abort();
/*	//viewHomePage();
			topNav();
//https://tcp-perf.childrensplace.com/us/c/girls-leggings
			drill();
//submitCompleteSearch();
			paginate();

//			productDisplay();
			addToCartFromPLP();
			

*/

//Ecom_addCheckout" ended with a "Fail
			
			
	viewHomePage();

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_DROP_CART ) {
		dropCart();
	} else {
		checkOut(); 
	}

	return 0;
}


void buildCartCheckoutNoBrowse_1(int userProfile)
{
	int iLoop, cartTypeRatio = 0;
	lr_think_time ( FORM_TT ) ;
	lr_save_string("false", "largeCart");

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_BUILDCART_LARGE ) {
		target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_LARGE_CART_SIZE}"));
		lr_save_string("true", "largeCart");
	}
	else if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CART_MERGE ) {
		target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_MERGE_CART_SIZE}"));
	}
	else {
		target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_CART_SIZE}"));
	}

	iLoop = target_itemsInCart + 2;

	lr_start_transaction("T20_New_Cart");
	lr_end_transaction("T20_New_Cart", LR_PASS);

	if ( userProfile == 0 && atoi(lr_eval_string("{cartCount}")) == 1) { //	userProfile == 0 is buildCartFirst before login, userProfile == 1 is loginFirst before building the cart
		orderItemIdscount = 0;
	}
	else {
		if (atoi(lr_eval_string("{cartCount}")) == 1) {
			orderItemIdscount = 0;
		}
		else
			orderItemIdscount = atoi(lr_eval_string("{cartCount}"));//cartCount
	}
//	lr_message("orderItemIdscount %d", orderItemIdscount);
	if (  orderItemIdscount < target_itemsInCart ) {

	    target_itemsInCart = target_itemsInCart - orderItemIdscount ;

		cartTypeRatio =  atoi(lr_eval_string("{RANDOM_PERCENT}")); ///determine the type of cart to build

		for(index_buildCart=0; index_buildCart < target_itemsInCart ; index_buildCart++)
		{
			topNav();
//			if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_BUILDCART_DRILLDOWN ) // moved to navbybrowse 03222018
//				drill();

//			paginate();

			//rhy - this is disabled as we are picking the pdpurl now from the datafile
//			if (( lr_paramarr_len ( "pdpURL" ) == 0 ) ) //&& lr_paramarr_len ( "quickviewURL" ) == 0 )
//			if (( lr_paramarr_len ( "productId" ) == 0 ) ) 
			if ( atoi( lr_eval_string( "{productId_count}" )) ==0 )
			{	index_buildCart--;

				continue;
			}

			if ( cartTypeRatio <= ECOMM_CART_RATIO ) {
				if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ADDTOCART_PLP )
				{
					if (addToCartFromPLP() == LR_FAIL) {
						atc_Stat = 1;
					}
					
				} else {
					if (productDisplay() != LR_FAIL) {
						addToCart();
					} else {
						atc_Stat = 1;
					}
				}
			}
			else {
				productDisplayBOPIS();
				addToCartMixed();
			}
//			productDisplay();

//			addToCartNoBrowse();

			iLoop--;

			if (iLoop == 0)
			    break;

			if (atc_Stat == 1) { //0-Pass 1-Fail //if the addtocart failed, set the index_buildCart to original number
				index_buildCart--;
				lr_start_transaction("T40_Failed_Add_Cart");
				lr_end_transaction("T40_Failed_Add_Cart", LR_PASS);

			}

		} // end for loop to add o cart

//		viewCart(); //0425 diabled

	}

} // end buildCartCheckoutNoBrowse

