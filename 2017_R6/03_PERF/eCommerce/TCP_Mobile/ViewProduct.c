ViewProduct()
{
//	lr_set_debug_message(16|8|4|2,1);
	lr_think_time(TT);
	if (atoi(lr_eval_string("{s_products_count}"))>0){
	lr_save_string(lr_paramarr_random("s_products"), "s_product");
	}else{
				lr_exit(LR_EXIT_ITERATION_AND_CONTINUE,LR_AUTO);
				return 0;
	}
		web_add_auto_header("Accept", 
		"application/json, text/plain, */*");

	web_add_auto_header("Accept-Encoding", 
		"gzip,deflate");

	web_add_auto_header("Accept-Language", 
		"en-US");

	web_add_auto_header("User-Agent", 
		"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_auto_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");

	web_add_auto_header("catalogId", 
		"10551");

	web_add_auto_header("deviceType", 
		"App");

	web_add_auto_header("langId", "-1");

	web_add_header("productCatentryId", "{s_product}");

	web_add_auto_header("storeId","10151");
	web_reg_find("TEXT/IC=recordSetTotalMatches","SaveCount=recordSetTotalMatches_p",LAST);
	lr_start_transaction("T03_MobileProductViewPage");
	
	lr_start_sub_transaction("T03_MobileProductViewPage_getBundleIdAndBundleDetails","T03_MobileProductViewPage");
	
	web_custom_request("getBundleIdAndBundleDetails", 
		"URL=https://{p_hostname}/api/tcpproduct/getBundleIdAndBundleDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		LAST);
	
	if (atoi(lr_eval_string("{recordSetTotalMatches_p}"))>0){
		lr_end_sub_transaction("T03_MobileProductViewPage_getBundleIdAndBundleDetails", LR_AUTO); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}else{
		lr_end_sub_transaction("T03_MobileProductViewPage_getBundleIdAndBundleDetails", LR_FAIL); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}
	
	web_add_header("productId", "{s_product}");
	web_reg_find("TEXT/IC=getAllSKUInventoryByProductId","SaveCount=getAllSKUInventoryByProductId",LAST);
	web_reg_save_param_json( "ParamName=s_catentryIDs", "QueryString=$.getAllSKUInventoryByProductId..response..catentryId", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json( "ParamName=s_quantities", "QueryString=$.getAllSKUInventoryByProductId..response..quantity", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	lr_start_sub_transaction("T03_MobileProductViewPage_getSKUInventoryandProductCounterDetails","T03_MobileProductViewPage");
	
	web_custom_request("getSKUInventoryandProductCounterDetails", 
		"URL=https://{p_hostname}/api/tcpproduct/getSKUInventoryandProductCounterDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t2.inf", 
		LAST);
	
	if (atoi(lr_eval_string("{getAllSKUInventoryByProductId}"))>0){
		lr_end_sub_transaction("T03_MobileProductViewPage_getSKUInventoryandProductCounterDetails", LR_AUTO); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}else{
		lr_end_sub_transaction("T03_MobileProductViewPage_getSKUInventoryandProductCounterDetails", LR_FAIL); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}	
	lr_end_transaction("T03_MobileProductViewPage", LR_AUTO);
	web_cleanup_auto_headers();	
	return 0;
}
