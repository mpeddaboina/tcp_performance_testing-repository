# 1 "e:\\performance\\scripts\\2017_r6\\03_perf\\ecommerce\\tcp_mobile\\\\combined_TCP_Mobile.c"
# 1 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h" 1
 
 












 











# 103 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h"





















































		


		typedef unsigned size_t;
	
	
        
	

















	

 



















 
 
 
 
 


 
 
 
 
 
 














int     lr_start_transaction   (char * transaction_name);
int lr_start_sub_transaction          (char * transaction_name, char * trans_parent);
long lr_start_transaction_instance    (char * transaction_name, long parent_handle);
int   lr_start_cross_vuser_transaction		(char * transaction_name, char * trans_id_param); 



int     lr_end_transaction     (char * transaction_name, int status);
int lr_end_sub_transaction            (char * transaction_name, int status);
int lr_end_transaction_instance       (long transaction, int status);
int   lr_end_cross_vuser_transaction	(char * transaction_name, char * trans_id_param, int status);


 
typedef char* lr_uuid_t;
 



lr_uuid_t lr_generate_uuid();

 


int lr_generate_uuid_free(lr_uuid_t uuid);

 



int lr_generate_uuid_on_buf(lr_uuid_t buf);

   
# 273 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h"
int lr_start_distributed_transaction  (char * transaction_name, lr_uuid_t correlator, long timeout  );

   







int lr_end_distributed_transaction  (lr_uuid_t correlator, int status);


double lr_stop_transaction            (char * transaction_name);
double lr_stop_transaction_instance   (long parent_handle);


void lr_resume_transaction           (char * trans_name);
void lr_resume_transaction_instance  (long trans_handle);


int lr_update_transaction            (const char *trans_name);


 
void lr_wasted_time(long time);


 
int lr_set_transaction(const char *name, double duration, int status);
 
long lr_set_transaction_instance(const char *name, double duration, int status, long parent_handle);


int   lr_user_data_point                      (char *, double);
long lr_user_data_point_instance                   (char *, double, long);
 



int lr_user_data_point_ex(const char *dp_name, double value, int log_flag);
long lr_user_data_point_instance_ex(const char *dp_name, double value, long parent_handle, int log_flag);


int lr_transaction_add_info      (const char *trans_name, char *info);
int lr_transaction_instance_add_info   (long trans_handle, char *info);
int lr_dpoint_add_info           (const char *dpoint_name, char *info);
int lr_dpoint_instance_add_info        (long dpoint_handle, char *info);


double lr_get_transaction_duration       (char * trans_name);
double lr_get_trans_instance_duration    (long trans_handle);
double lr_get_transaction_think_time     (char * trans_name);
double lr_get_trans_instance_think_time  (long trans_handle);
double lr_get_transaction_wasted_time    (char * trans_name);
double lr_get_trans_instance_wasted_time (long trans_handle);
int    lr_get_transaction_status		 (char * trans_name);
int	   lr_get_trans_instance_status		 (long trans_handle);

 



int lr_set_transaction_status(int status);

 



int lr_set_transaction_status_by_name(int status, const char *trans_name);
int lr_set_transaction_instance_status(int status, long trans_handle);


typedef void* merc_timer_handle_t;
 

merc_timer_handle_t lr_start_timer();
double lr_end_timer(merc_timer_handle_t timer_handle);


 
 
 
 
 
 











 



int   lr_rendezvous  (char * rendezvous_name);
 




int   lr_rendezvous_ex (char * rendezvous_name);



 
 
 
 
 
char *lr_get_vuser_ip (void);
void   lr_whoami (int *vuser_id, char ** sgroup, int *scid);
char *	  lr_get_host_name (void);
char *	  lr_get_master_host_name (void);

 
long     lr_get_attrib_long	(char * attr_name);
char *   lr_get_attrib_string	(char * attr_name);
double   lr_get_attrib_double      (char * attr_name);

char * lr_paramarr_idx(const char * paramArrayName, unsigned int index);
char * lr_paramarr_random(const char * paramArrayName);
int    lr_paramarr_len(const char * paramArrayName);

int	lr_param_unique(const char * paramName);
int lr_param_sprintf(const char * paramName, const char * format, ...);


 
 
static void *ci_this_context = 0;






 








void lr_continue_on_error (int lr_continue);
char *   lr_unmask (const char *EncodedString);
char *   lr_decrypt (const char *EncodedString);


 
 
 
 
 
 



 







 















void   lr_abort (void);
void lr_exit(int exit_option, int exit_status);
void lr_abort_ex (unsigned long flags);

void   lr_peek_events (void);


 
 
 
 
 


void   lr_think_time (double secs);

 


void lr_force_think_time (double secs);


 
 
 
 
 



















int   lr_msg (char * fmt, ...);
int   lr_debug_message (unsigned int msg_class,
									    char * format,
										...);
# 513 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h"
void   lr_new_prefix (int type,
                                 char * filename,
                                 int line);
# 516 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h"
int   lr_log_message (char * fmt, ...);
int   lr_message (char * fmt, ...);
int   lr_error_message (char * fmt, ...);
int   lr_output_message (char * fmt, ...);
int   lr_vuser_status_message (char * fmt, ...);
int   lr_error_message_without_fileline (char * fmt, ...);
int   lr_fail_trans_with_error (char * fmt, ...);

 
 
 
 
 
# 540 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h"

 
 
 
 
 





int   lr_next_row ( char * table);
int lr_advance_param ( char * param);



														  
														  

														  
														  

													      
 


char *   lr_eval_string (char * str);
int   lr_eval_string_ext (const char *in_str,
                                     unsigned long const in_len,
                                     char ** const out_str,
                                     unsigned long * const out_len,
                                     unsigned long const options,
                                     const char *file,
								     long const line);
# 574 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h"
void   lr_eval_string_ext_free (char * * pstr);

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
int lr_param_increment (char * dst_name,
                              char * src_name);
# 597 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h"













											  
											  

											  
											  
											  

int	  lr_save_var (char *              param_val,
							  unsigned long const param_val_len,
							  unsigned long const options,
							  char *			  param_name);
# 621 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h"
int   lr_save_string (const char * param_val, const char * param_name);



int   lr_set_custom_error_message (const char * param_val, ...);

int   lr_remove_custom_error_message ();


int   lr_free_parameter (const char * param_name);
int   lr_save_int (const int param_val, const char * param_name);
int   lr_save_timestamp (const char * tmstampParam, ...);
int   lr_save_param_regexp (const char *bufferToScan, unsigned int bufSize, ...);

int   lr_convert_double_to_integer (const char *source_param_name, const char * target_param_name);
int   lr_convert_double_to_double (const char *source_param_name, const char *format_string, const char * target_param_name);

 
 
 
 
 
 
# 700 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h"
void   lr_save_datetime (const char *format, int offset, const char *name);









 











 
 
 
 
 






 



char * lr_error_context_get_entry (char * key);

 



long   lr_error_context_get_error_id (void);


 
 
 

int lr_table_get_rows_num (char * param_name);

int lr_table_get_cols_num (char * param_name);

char * lr_table_get_cell_by_col_index (char * param_name, int row, int col);

char * lr_table_get_cell_by_col_name (char * param_name, int row, const char* col_name);

int lr_table_get_column_name_by_index (char * param_name, int col, 
											char * * const col_name,
											size_t * col_name_len);
# 761 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h"

int lr_table_get_column_name_by_index_free (char * col_name);

 
 
 
 
# 776 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h"
int   lr_zip (const char* param1, const char* param2);
int   lr_unzip (const char* param1, const char* param2);

 
 
 
 
 
 
 
 

 
 
 
 
 
 
int   lr_param_substit (char * file,
                                   int const line,
                                   char * in_str,
                                   size_t const in_len,
                                   char * * const out_str,
                                   size_t * const out_len);
# 800 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h"
void   lr_param_substit_free (char * * pstr);


 
# 812 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h"





char *   lrfnc_eval_string (char * str,
                                      char * file_name,
                                      long const line_num);
# 820 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h"


int   lrfnc_save_string ( const char * param_val,
                                     const char * param_name,
                                     const char * file_name,
                                     long const line_num);
# 826 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h"

int   lrfnc_free_parameter (const char * param_name );







typedef struct _lr_timestamp_param
{
	int iDigits;
}lr_timestamp_param;

extern const lr_timestamp_param default_timestamp_param;

int   lrfnc_save_timestamp (const char * param_name, const lr_timestamp_param* time_param);

int lr_save_searched_string(char * buffer, long buf_size, unsigned int occurrence,
			    char * search_string, int offset, unsigned int param_val_len, 
			    char * param_name);

 
char *   lr_string (char * str);

 
# 929 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h"

int   lr_save_value (char * param_val,
                                unsigned long const param_val_len,
                                unsigned long const options,
                                char * param_name,
                                char * file_name,
                                long const line_num);
# 936 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h"


 
 
 
 
 











int   lr_printf (char * fmt, ...);
 
int   lr_set_debug_message (unsigned int msg_class,
                                       unsigned int swtch);
# 958 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h"
unsigned int   lr_get_debug_message (void);


 
 
 
 
 

void   lr_double_think_time ( double secs);
void   lr_usleep (long);


 
 
 
 
 
 




int *   lr_localtime (long offset);


int   lr_send_port (long port);


# 1034 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h"



struct _lr_declare_identifier{
	char signature[24];
	char value[128];
};

int   lr_pt_abort (void);

void vuser_declaration (void);






# 1063 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h"


# 1075 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrun.h"
















 
 
 
 
 







int    _lr_declare_transaction   (char * transaction_name);


 
 
 
 
 







int   _lr_declare_rendezvous  (char * rendezvous_name);

 
 
 
 
 


typedef int PVCI;






typedef int VTCERR;









PVCI   vtc_connect(char * servername, int portnum, int options);
VTCERR   vtc_disconnect(PVCI pvci);
VTCERR   vtc_get_last_error(PVCI pvci);
VTCERR   vtc_query_column(PVCI pvci, char * columnName, int columnIndex, char * *outvalue);
VTCERR   vtc_query_row(PVCI pvci, int rowIndex, char * **outcolumns, char * **outvalues);
VTCERR   vtc_send_message(PVCI pvci, char * column, char * message, unsigned short *outRc);
VTCERR   vtc_send_if_unique(PVCI pvci, char * column, char * message, unsigned short *outRc);
VTCERR   vtc_send_row1(PVCI pvci, char * columnNames, char * messages, char * delimiter, unsigned char sendflag, unsigned short *outUpdates);
VTCERR   vtc_update_message(PVCI pvci, char * column, int index , char * message, unsigned short *outRc);
VTCERR   vtc_update_message_ifequals(PVCI pvci, char * columnName, int index,	char * message, char * ifmessage, unsigned short 	*outRc);
VTCERR   vtc_update_row1(PVCI pvci, char * columnNames, int index , char * messages, char * delimiter, unsigned short *outUpdates);
VTCERR   vtc_retrieve_message(PVCI pvci, char * column, char * *outvalue);
VTCERR   vtc_retrieve_messages1(PVCI pvci, char * columnNames, char * delimiter, char * **outvalues);
VTCERR   vtc_retrieve_row(PVCI pvci, char * **outcolumns, char * **outvalues);
VTCERR   vtc_rotate_message(PVCI pvci, char * column, char * *outvalue, unsigned char sendflag);
VTCERR   vtc_rotate_messages1(PVCI pvci, char * columnNames, char * delimiter, char * **outvalues, unsigned char sendflag);
VTCERR   vtc_rotate_row(PVCI pvci, char * **outcolumns, char * **outvalues, unsigned char sendflag);
VTCERR   vtc_increment(PVCI pvci, char * column, int index , int incrValue, int *outValue);
VTCERR   vtc_clear_message(PVCI pvci, char * column, int index , unsigned short *outRc);
VTCERR   vtc_clear_column(PVCI pvci, char * column, unsigned short *outRc);
VTCERR   vtc_ensure_index(PVCI pvci, char * column, unsigned short *outRc);
VTCERR   vtc_drop_index(PVCI pvci, char * column, unsigned short *outRc);
VTCERR   vtc_clear_row(PVCI pvci, int rowIndex, unsigned short *outRc);
VTCERR   vtc_create_column(PVCI pvci, char * column,unsigned short *outRc);
VTCERR   vtc_column_size(PVCI pvci, char * column, int *size);
void   vtc_free(char * msg);
void   vtc_free_list(char * *msglist);

VTCERR   lrvtc_connect(char * servername, int portnum, int options);
VTCERR   lrvtc_disconnect();
VTCERR   lrvtc_query_column(char * columnName, int columnIndex);
VTCERR   lrvtc_query_row(int columnIndex);
VTCERR   lrvtc_send_message(char * columnName, char * message);
VTCERR   lrvtc_send_if_unique(char * columnName, char * message);
VTCERR   lrvtc_send_row1(char * columnNames, char * messages, char * delimiter, unsigned char sendflag);
VTCERR   lrvtc_update_message(char * columnName, int index , char * message);
VTCERR   lrvtc_update_message_ifequals(char * columnName, int index, char * message, char * ifmessage);
VTCERR   lrvtc_update_row1(char * columnNames, int index , char * messages, char * delimiter);
VTCERR   lrvtc_retrieve_message(char * columnName);
VTCERR   lrvtc_retrieve_messages1(char * columnNames, char * delimiter);
VTCERR   lrvtc_retrieve_row();
VTCERR   lrvtc_rotate_message(char * columnName, unsigned char sendflag);
VTCERR   lrvtc_rotate_messages1(char * columnNames, char * delimiter, unsigned char sendflag);
VTCERR   lrvtc_rotate_row(unsigned char sendflag);
VTCERR   lrvtc_increment(char * columnName, int index , int incrValue);
VTCERR   lrvtc_noop();
VTCERR   lrvtc_clear_message(char * columnName, int index);
VTCERR   lrvtc_clear_column(char * columnName); 
VTCERR   lrvtc_ensure_index(char * columnName); 
VTCERR   lrvtc_drop_index(char * columnName); 
VTCERR   lrvtc_clear_row(int rowIndex);
VTCERR   lrvtc_create_column(char * columnName);
VTCERR   lrvtc_column_size(char * columnName);



 
 
 
 
 

 
int lr_enable_ip_spoofing();
int lr_disable_ip_spoofing();


 




int lr_convert_string_encoding(char * sourceString, char * fromEncoding, char * toEncoding, char * paramName);
int lr_read_file(const char *filename, const char *outputParam, int continueOnError);

int lr_get_char_count(const char * string);


 
int lr_db_connect (char * pFirstArg, ...);
int lr_db_disconnect (char * pFirstArg,	...);
int lr_db_executeSQLStatement (char * pFirstArg, ...);
int lr_db_dataset_action(char * pFirstArg, ...);
int lr_checkpoint(char * pFirstArg,	...);
int lr_db_getvalue(char * pFirstArg, ...);







 
 



















# 1 "e:\\performance\\scripts\\2017_r6\\03_perf\\ecommerce\\tcp_mobile\\\\combined_TCP_Mobile.c" 2

# 1 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/SharedParameter.h" 1



 
 
 
 
# 100 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/SharedParameter.h"






typedef int PVCI2;






typedef int VTCERR2;


 
 
 

 
extern PVCI2    vtc_connect(char *servername, int portnum, int options);
extern VTCERR2  vtc_disconnect(PVCI2 pvci);
extern VTCERR2  vtc_get_last_error(PVCI2 pvci);

 
extern VTCERR2  vtc_query_column(PVCI2 pvci, char *columnName, int columnIndex, char **outvalue);
extern VTCERR2  vtc_query_row(PVCI2 pvci, int columnIndex, char ***outcolumns, char ***outvalues);
extern VTCERR2  vtc_send_message(PVCI2 pvci, char *column, char *message, unsigned short *outRc);
extern VTCERR2  vtc_send_if_unique(PVCI2 pvci, char *column, char *message, unsigned short *outRc);
extern VTCERR2  vtc_send_row1(PVCI2 pvci, char *columnNames, char *messages, char *delimiter,  unsigned char sendflag, unsigned short *outUpdates);
extern VTCERR2  vtc_update_message(PVCI2 pvci, char *column, int index , char *message, unsigned short *outRc);
extern VTCERR2  vtc_update_message_ifequals(PVCI2 pvci, char	*columnName, int index,	char *message, char	*ifmessage,	unsigned short 	*outRc);
extern VTCERR2  vtc_update_row1(PVCI2 pvci, char *columnNames, int index , char *messages, char *delimiter, unsigned short *outUpdates);
extern VTCERR2  vtc_retrieve_message(PVCI2 pvci, char *column, char **outvalue);
extern VTCERR2  vtc_retrieve_messages1(PVCI2 pvci, char *columnNames, char *delimiter, char ***outvalues);
extern VTCERR2  vtc_retrieve_row(PVCI2 pvci, char ***outcolumns, char ***outvalues);
extern VTCERR2  vtc_rotate_message(PVCI2 pvci, char *column, char **outvalue, unsigned char sendflag);
extern VTCERR2  vtc_rotate_messages1(PVCI2 pvci, char *columnNames, char *delimiter, char ***outvalues, unsigned char sendflag);
extern VTCERR2  vtc_rotate_row(PVCI2 pvci, char ***outcolumns, char ***outvalues, unsigned char sendflag);
extern VTCERR2	vtc_increment(PVCI2 pvci, char *column, int index , int incrValue, int *outValue);
extern VTCERR2  vtc_clear_message(PVCI2 pvci, char *column, int index , unsigned short *outRc);
extern VTCERR2  vtc_clear_column(PVCI2 pvci, char *column, unsigned short *outRc);

extern VTCERR2  vtc_clear_row(PVCI2 pvci, int rowIndex, unsigned short *outRc);

extern VTCERR2  vtc_create_column(PVCI2 pvci, char *column,unsigned short *outRc);
extern VTCERR2  vtc_column_size(PVCI2 pvci, char *column, int *size);
extern VTCERR2  vtc_ensure_index(PVCI2 pvci, char *column, unsigned short *outRc);
extern VTCERR2  vtc_drop_index(PVCI2 pvci, char *column, unsigned short *outRc);

extern VTCERR2  vtc_noop(PVCI2 pvci);

 
extern void vtc_free(char *msg);
extern void vtc_free_list(char **msglist);

 


 




 




















 




 
 
 

extern VTCERR2  lrvtc_connect(char *servername, int portnum, int options);
extern VTCERR2  lrvtc_disconnect();
extern VTCERR2  lrvtc_query_column(char *columnName, int columnIndex);
extern VTCERR2  lrvtc_query_row(int columnIndex);
extern VTCERR2  lrvtc_send_message(char *columnName, char *message);
extern VTCERR2  lrvtc_send_if_unique(char *columnName, char *message);
extern VTCERR2  lrvtc_send_row1(char *columnNames, char *messages, char *delimiter,  unsigned char sendflag);
extern VTCERR2  lrvtc_update_message(char *columnName, int index , char *message);
extern VTCERR2  lrvtc_update_message_ifequals(char *columnName, int index, char 	*message, char *ifmessage);
extern VTCERR2  lrvtc_update_row1(char *columnNames, int index , char *messages, char *delimiter);
extern VTCERR2  lrvtc_retrieve_message(char *columnName);
extern VTCERR2  lrvtc_retrieve_messages1(char *columnNames, char *delimiter);
extern VTCERR2  lrvtc_retrieve_row();
extern VTCERR2  lrvtc_rotate_message(char *columnName, unsigned char sendflag);
extern VTCERR2  lrvtc_rotate_messages1(char *columnNames, char *delimiter, unsigned char sendflag);
extern VTCERR2  lrvtc_rotate_row(unsigned char sendflag);
extern VTCERR2  lrvtc_increment(char *columnName, int index , int incrValue);
extern VTCERR2  lrvtc_clear_message(char *columnName, int index);
extern VTCERR2  lrvtc_clear_column(char *columnName);
extern VTCERR2  lrvtc_clear_row(int rowIndex);
extern VTCERR2  lrvtc_create_column(char *columnName);
extern VTCERR2  lrvtc_column_size(char *columnName);
extern VTCERR2  lrvtc_ensure_index(char *columnName);
extern VTCERR2  lrvtc_drop_index(char *columnName);

extern VTCERR2  lrvtc_noop();

 
 
 

                               


 
 
 





















# 2 "e:\\performance\\scripts\\2017_r6\\03_perf\\ecommerce\\tcp_mobile\\\\combined_TCP_Mobile.c" 2

# 1 "globals.h" 1



 
 

# 1 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/web_api.h" 1







# 1 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/as_web.h" 1



























































 




 



 











 





















 
 
 

  int
	web_add_filter(
		const char *		mpszArg,
		...
	);									 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_add_auto_filter(
		const char *		mpszArg,
		...
	);									 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
	
  int
	web_add_auto_header(
		const char *		mpszHeader,
		const char *		mpszValue);

  int
	web_add_header(
		const char *		mpszHeader,
		const char *		mpszValue);
  int
	web_add_cookie(
		const char *		mpszCookie);
  int
	web_cleanup_auto_headers(void);
  int
	web_cleanup_cookies(void);
  int
	web_concurrent_end(
		const char * const	mpszReserved,
										 
		...								 
	);
  int
	web_concurrent_start(
		const char * const	mpszConcurrentGroupName,
										 
										 
		...								 
										 
	);
  int
	web_create_html_param(
		const char *		mpszParamName,
		const char *		mpszLeftDelim,
		const char *		mpszRightDelim);
  int
	web_create_html_param_ex(
		const char *		mpszParamName,
		const char *		mpszLeftDelim,
		const char *		mpszRightDelim,
		const char *		mpszNum);
  int
	web_custom_request(
		const char *		mpszReqestName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	spdy_custom_request(
		const char *		mpszReqestName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_disable_keep_alive(void);
  int
	web_enable_keep_alive(void);
  int
	web_find(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_get_int_property(
		const int			miHttpInfoType);
  int
	web_image(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_image_check(
		const char *		mpszName,
		...);
  int
	web_java_check(
		const char *		mpszName,
		...);
  int
	web_link(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

	
  int
	web_global_verification(
		const char *		mpszArg1,
		...);							 
										 
										 
										 
										 
										 
  int
	web_reg_find(
		const char *		mpszArg1,
		...);							 
										 
										 
										 
										 
										 
										 
										 
				
  int
	web_reg_save_param(
		const char *		mpszParamName,
		...);							 
										 
										 
										 
										 
										 
										 

  int
	web_convert_param(
		const char * 		mpszParamName, 
										 
		...);							 
										 
										 


										 

										 
  int
	web_remove_auto_filter(
		const char *		mpszArg,
		...
	);									 
										 
				
  int
	web_remove_auto_header(
		const char *		mpszHeaderName,
		...);							 
										 



  int
	web_remove_cookie(
		const char *		mpszCookie);

  int
	web_save_header(
		const char *		mpszType,	 
		const char *		mpszName);	 
  int
	web_set_certificate(
		const char *		mpszIndex);
  int
	web_set_certificate_ex(
		const char *		mpszArg1,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_set_connections_limit(
		const char *		mpszLimit);
  int
	web_set_max_html_param_len(
		const char *		mpszLen);
  int
	web_set_max_retries(
		const char *		mpszMaxRetries);
  int
	web_set_proxy(
		const char *		mpszProxyHost);
  int
	web_set_pac(
		const char *		mpszPacUrl);
  int
	web_set_proxy_bypass(
		const char *		mpszBypass);
  int
	web_set_secure_proxy(
		const char *		mpszProxyHost);
  int
	web_set_sockets_option(
		const char *		mpszOptionID,
		const char *		mpszOptionValue
	);
  int
	web_set_option(
		const char *		mpszOptionID,
		const char *		mpszOptionValue,
		...								 
	);
  int
	web_set_timeout(
		const char *		mpszWhat,
		const char *		mpszTimeout);
  int
	web_set_user(
		const char *		mpszUserName,
		const char *		mpszPwd,
		const char *		mpszHost);

  int
	web_sjis_to_euc_param(
		const char *		mpszParamName,
										 
		const char *		mpszParamValSjis);
										 

  int
	web_submit_data(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	spdy_submit_data(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_submit_form(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										  
										 
										 
										 
										 
										 
										  
										 
										 
										 
										 
										 
										 
										 
										  
										 
										 
										 
										 
										 
										  
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_url(
		const char *		mpszUrlName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	spdy_url(
		const char *		mpszUrlName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int 
	web_set_proxy_bypass_local(
		const char * mpszNoLocal
		);

  int 
	web_cache_cleanup(void);

  int
	web_create_html_query(
		const char* mpszStartQuery,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int 
	web_create_radio_button_param(
		const char *NameFiled,
		const char *NameAndVal,
		const char *ParamName
		);

  int
	web_convert_from_formatted(
		const char * mpszArg1,
		...);							 
										 
										 
										 
										 
										 
										
  int
	web_convert_to_formatted(
		const char * mpszArg1,
		...);							 
										 
										 
										 
										 
										 

  int
	web_reg_save_param_ex(
		const char * mpszParamName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_reg_save_param_xpath(
		const char * mpszParamName,
		...);							
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_reg_save_param_json(
		const char * mpszParamName,
		...);							
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_reg_save_param_regexp(
		 const char * mpszParamName,
		 ...);							
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_reg_save_param_attrib(
		const char * mpszParamName,
		...);
										 
										 
										 
										 
										 
										 
										 		
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_js_run(
		const char * mpszCode,
		...);							
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_js_reset(void);

  int
	web_convert_date_param(
		const char * 		mpszParamName,
		...);










# 789 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/as_web.h"


# 802 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/as_web.h"



























# 840 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/as_web.h"

 
 
 


  int
	FormSubmit(
		const char *		mpszFormName,
		...);
  int
	InitWebVuser(void);
  int
	SetUser(
		const char *		mpszUserName,
		const char *		mpszPwd,
		const char *		mpszHost);
  int
	TerminateWebVuser(void);
  int
	URL(
		const char *		mpszUrlName);
























# 908 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/as_web.h"


  int
	web_rest(
		const char *		mpszReqestName,
		...);							 
										 
										 
										 
										 

  int
web_stream_open(
	const char *		mpszArg1,
	...
);
  int
	web_stream_wait(
		const char *		mpszArg1,
		...
	);

  int
	web_stream_close(
		const char *		mpszArg1,
		...
	);

  int
web_stream_play(
	const char *		mpszArg1,
	...
	);

  int
web_stream_pause(
	const char *		mpszArg1,
	...
	);

  int
web_stream_seek(
	const char *		mpszArg1,
	...
	);

  int
web_stream_get_param_int(
	const char*			mpszStreamID,
	const int			miStateType
	);

  double
web_stream_get_param_double(
	const char*			mpszStreamID,
	const int			miStateType
	);

  int
web_stream_get_param_string(
	const char*			mpszStreamID,
	const int			miStateType,
	const char*			mpszParameterName
	);

  int
web_stream_set_param_int(
	const char*			mpszStreamID,
	const int			miStateType,
	const int			miStateValue
	);

  int
web_stream_set_param_double(
	const char*			mpszStreamID,
	const int			miStateType,
	const double		mdfStateValue
	);

  int
web_stream_set_custom_mpd(
	const char*			mpszStreamID,
	const char*			aMpdBuf
	);

 
 
 






# 9 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/web_api.h" 2

















 







 















  int
	web_reg_add_cookie(
		const char *		mpszCookie,
		...);							 
										 

  int
	web_report_data_point(
		const char *		mpszEventType,
		const char *		mpszEventName,
		const char *		mpszDataPointName,
		const char *		mpszLAST);	 
										 
										 
										 

  int
	web_text_link(
		const char *		mpszStepName,
		...);

  int
	web_element(
		const char *		mpszStepName,
		...);

  int
	web_image_link(
		const char *		mpszStepName,
		...);

  int
	web_static_image(
		const char *		mpszStepName,
		...);

  int
	web_image_submit(
		const char *		mpszStepName,
		...);

  int
	web_button(
		const char *		mpszStepName,
		...);

  int
	web_edit_field(
		const char *		mpszStepName,
		...);

  int
	web_radio_group(
		const char *		mpszStepName,
		...);

  int
	web_check_box(
		const char *		mpszStepName,
		...);

  int
	web_list(
		const char *		mpszStepName,
		...);

  int
	web_text_area(
		const char *		mpszStepName,
		...);

  int
	web_map_area(
		const char *		mpszStepName,
		...);

  int
	web_eval_java_script(
		const char *		mpszStepName,
		...);

  int
	web_reg_dialog(
		const char *		mpszArg1,
		...);

  int
	web_reg_cross_step_download(
		const char *		mpszArg1,
		...);

  int
	web_browser(
		const char *		mpszStepName,
		...);

  int
	web_control(
		const char *		mpszStepName,
		...);

  int
	web_set_rts_key(
		const char *		mpszArg1,
		...);

  int
	web_save_param_length(
		const char * 		mpszParamName,
		...);

  int
	web_save_timestamp_param(
		const char * 		mpszParamName,
		...);

  int
	web_load_cache(
		const char *		mpszStepName,
		...);							 
										 

  int
	web_dump_cache(
		const char *		mpszStepName,
		...);							 
										 
										 

  int
	web_reg_find_in_log(
		const char *		mpszArg1,
		...);							 
										 
										 

  int
	web_get_sockets_info(
		const char *		mpszArg1,
		...);							 
										 
										 
										 
										 

  int
	web_add_cookie_ex(
		const char *		mpszArg1,
		...);							 
										 
										 
										 

  int
	web_hook_java_script(
		const char *		mpszArg1,
		...);							 
										 
										 
										 

 
 
 
 
 
 
 
 
 
 
 
 
  int
	web_reg_async_attributes(
		const char *		mpszArg,
		...
	);

 
 
 
 
 
 
  int
	web_sync(
		 const char *		mpszArg1,
		 ...
	);

 
 
 
 
  int
	web_stop_async(
		const char *		mpszArg1,
		...
	);

 
 
 
 
 

 
 
 

typedef enum WEB_ASYNC_CB_RC_ENUM_T
{
	WEB_ASYNC_CB_RC_OK,				 

	WEB_ASYNC_CB_RC_ABORT_ASYNC_NOT_ERROR,
	WEB_ASYNC_CB_RC_ABORT_ASYNC_ERROR,
										 
										 
										 
										 
	WEB_ASYNC_CB_RC_ENUM_COUNT
} WEB_ASYNC_CB_RC_ENUM;

 
 
 

typedef enum WEB_CONVERS_CB_CALL_REASON_ENUM_T
{
	WEB_CONVERS_CB_CALL_REASON_BUFFER_RECEIVED,
	WEB_CONVERS_CB_CALL_REASON_END_OF_TASK,

	WEB_CONVERS_CB_CALL_REASON_ENUM_COUNT
} WEB_CONVERS_CB_CALL_REASON_ENUM;

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

typedef
int														 
	(*RequestCB_t)();

typedef
int														 
	(*ResponseBodyBufferCB_t)(
		  const char *		aLastBufferStr,
		  int				aLastBufferLen,
		  const char *		aAccumulatedStr,
		  int				aAccumulatedLen,
		  int				aHttpStatusCode);

typedef
int														 
	(*ResponseCB_t)(
		  const char *		aResponseHeadersStr,
		  int				aResponseHeadersLen,
		  const char *		aResponseBodyStr,
		  int				aResponseBodyLen,
		  int				aHttpStatusCode);

typedef
int														 
	(*ResponseHeadersCB_t)(
		  int				aHttpStatusCode,
		  const char *		aAccumulatedHeadersStr,
		  int				aAccumulatedHeadersLen);



 
 
 

typedef enum WEB_CONVERS_UTIL_RC_ENUM_T
{
	WEB_CONVERS_UTIL_RC_OK,
	WEB_CONVERS_UTIL_RC_CONVERS_NOT_FOUND,
	WEB_CONVERS_UTIL_RC_TASK_NOT_FOUND,
	WEB_CONVERS_UTIL_RC_INFO_NOT_FOUND,
	WEB_CONVERS_UTIL_RC_INFO_UNAVIALABLE,
	WEB_CONVERS_UTIL_RC_INVALID_ARGUMENT,

	WEB_CONVERS_UTIL_RC_ENUM_COUNT
} WEB_CONVERS_UTIL_RC_ENUM;

 
 
 

  int					 
	web_util_set_request_url(
		  const char *		aUrlStr);

  int					 
	web_util_set_request_body(
		  const char *		aRequestBodyStr);

  int					 
	web_util_set_formatted_request_body(
		  const char *		aRequestBodyStr);


 
 
 
 
 

 
 
 
 
 

 
 
 
 
 
 
 
 

 
 
  int
web_websocket_connect(
		 const char *	mpszArg1,
		 ...
		 );


 
 
 
 
 																						
  int
web_websocket_send(
	   const char *		mpszArg1,
		...
	   );

 
 
 
 
 
 
  int
web_websocket_close(
		const char *	mpszArg1,
		...
		);

 
typedef
void														
(*OnOpen_t)(
			  const char* connectionID,  
			  const char * responseHeader,  
			  int length  
);

typedef
void														
(*OnMessage_t)(
	  const char* connectionID,  
	  int isbinary,  
	  const char * data,  
	  int length  
	);

typedef
void														
(*OnError_t)(
	  const char* connectionID,  
	  const char * message,  
	  int length  
	);

typedef
void														
(*OnClose_t)(
	  const char* connectionID,  
	  int isClosedByClient,  
	  int code,  
	  const char* reason,  
	  int length  
	 );
 
 
 
 
 





# 7 "globals.h" 2

# 1 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/lrw_custom_body.h" 1
 





# 8 "globals.h" 2

# 1 "E:\\Program Files (x86)\\HPE\\LoadRunner\\include/Mobile.h" 1










# 9 "globals.h" 2


 
 
int TT=10;
int isLoggedin=0;    
int p_MobileRand=0;
int DropCartRatio=0,LoginDropRatio=0,ShipPageRatio=0,BillPageRatio=0;
 

char *EncodePlainToURL(const char *sIn, char *sOut)
{
    int i;
    char cCurChar;
    char sCurStr[4] = {0};

    sOut[0] = '\0';

    for (i = 0; cCurChar = sIn[i]; i++)
    {
         
        if (isdigit(cCurChar) || isalpha(cCurChar))
        {
             
            sprintf(sCurStr, "%c", cCurChar);
        }
        else
        {
             
            sprintf(sCurStr, "%%%X", cCurChar);
        }

         
        strcat(sOut, sCurStr);
    }

    return sOut;
}


# 3 "e:\\performance\\scripts\\2017_r6\\03_perf\\ecommerce\\tcp_mobile\\\\combined_TCP_Mobile.c" 2

# 1 "vuser_init.c" 1
vuser_init()
{
	return 0;
}
# 4 "e:\\performance\\scripts\\2017_r6\\03_perf\\ecommerce\\tcp_mobile\\\\combined_TCP_Mobile.c" 2

# 1 "HomePage.c" 1
HomePage()
{	

 
	web_set_sockets_option("SSL_VERSION", "TLS1.1");   
		lr_think_time(TT);
	web_set_max_html_param_len ( "3072" ) ;
	web_cleanup_cookies () ;
	web_cache_cleanup();
	
		p_MobileRand=atoi(lr_eval_string("{p_Random}"));
		
	lr_start_transaction("T01_MobileHomePage");

	web_add_auto_header("Accept", 
		"application/json, text/plain, */*");

	web_add_auto_header("Accept-Encoding", 
		"gzip,deflate");

	web_add_auto_header("Accept-Language", 
		"en-US");

	web_add_auto_header("User-Agent", 
		"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_auto_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");

	web_add_auto_header("catalogId", 
		"10551");

	web_add_auto_header("deviceType", 
		"App");

	web_add_auto_header("langId", 
		"-1");

	web_add_auto_header("storeId", 
		"10151");
	web_reg_find("TEXT/IC=resourceName","SaveCount=reguserdetails","LAST");
	lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	web_custom_request("getRegisteredUserDetailsInfo", 
		"URL=https://{p_hostname}/api/payment/getRegisteredUserDetailsInfo", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"LAST");
	
	if (atoi(lr_eval_string("{reguserdetails}"))>0){
		lr_end_sub_transaction("T01_MobileHomePage_getRegisteredUserDetailsInfo", 2);  
	}else{
		lr_end_sub_transaction("T01_MobileHomePage_getRegisteredUserDetailsInfo", 1);  
	}
	web_add_header("locStore", 
		"false");

	web_add_header("pageName", 
		"orderSummary");
	
	web_reg_find("TEXT/IC={","SaveCount=shippingCharge","LAST");
	lr_start_sub_transaction ( "T01_MobileHomePage_getOrderDetails", "T01_MobileHomePage" );
	web_custom_request("getOrderDetails", 
		"URL=https://{p_hostname}/api/getOrderDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t4.inf", 
		"LAST");
	if (atoi(lr_eval_string("{shippingCharge}"))>0){
		lr_end_sub_transaction("T01_MobileHomePage_getOrderDetails", 2);  
	}else{
		lr_end_sub_transaction("T01_MobileHomePage_getOrderDetails", 1);  
	}
	web_add_header("depthAndLimit", 
		"-1,-1,0");
	
	
	web_reg_save_param_json( "ParamName=subCategories", "QueryString=$.catalogGroupView..catalogGroupView..seo_token_ntk", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
	web_custom_request("getTopCategories", 
		"URL=https://{p_hostname}/api/tcpproduct/getTopCategories", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t7.inf", 
		"LAST");

	web_reg_find("TEXT/IC={","SaveCount=resourceName","LAST");
	lr_start_sub_transaction ( "T01_MobileHomePage_getPriceDetails", "T01_MobileHomePage" );
	web_custom_request("getPriceDetails", 
		"URL=https://{p_hostname}/api/tcpproduct/getPriceDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t7.inf", 
		"LAST");
	if (atoi(lr_eval_string("{resourceName}"))>0){
		lr_end_sub_transaction("T01_MobileHomePage_getPriceDetails", 2);  
	}else{
		lr_end_sub_transaction("T01_MobileHomePage_getPriceDetails", 1);  
	}
	
	lr_end_transaction("T01_MobileHomePage", 2);
	
	web_cleanup_auto_headers();		

	return 0;
}
# 5 "e:\\performance\\scripts\\2017_r6\\03_perf\\ecommerce\\tcp_mobile\\\\combined_TCP_Mobile.c" 2

# 1 "CategoryBrowse.c" 1
CategoryBrowse()
{

	lr_think_time(TT);	
	lr_save_string(lr_paramarr_random("subCategories"), "s_subCategory");

	 
	web_add_header("categoryId", 		"{s_subCategory}");

	web_add_auto_header("catalogId", 
		"10551");

	web_add_auto_header("deviceType", 
		"App");

	web_add_auto_header("facet", 
		"");

	web_add_auto_header("langId", 
		"-1");

	web_add_auto_header("orderBy", 
		"null");

	web_add_auto_header("pageNumber", 
		"1");

	web_add_auto_header("pageSize", 
		"30");

	web_add_auto_header("searchSource", 
		"E");

	web_add_auto_header("searchType", 
		"-1,-1,0");

	web_add_auto_header("storeId", 
		"10151");
	web_reg_find("TEXT/IC=recordSetTotalMatches","SaveCount=recordSetTotalMatches","LAST");
	lr_start_transaction("T02_MobileCategoryPage");
	 
	web_reg_save_param_json( "ParamName=s_products", "QueryString=$.catalogEntryView[?(@.partNumber)].uniqueID", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
	lr_start_sub_transaction("T02_MobileCategoryPage_getProductviewbyCategory","T02_MobileCategoryPage");
	web_custom_request("getProductviewbyCategory", 
		"URL=https://{p_hostname}/api/tcpproduct/getProductviewbyCategory", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t3.inf", 
		"LAST");

	if (atoi(lr_eval_string("{recordSetTotalMatches}"))>0){
		lr_end_sub_transaction("T02_MobileCategoryPage_getProductviewbyCategory", 2);  
	}else{
		lr_end_sub_transaction("T02_MobileCategoryPage_getProductviewbyCategory", 1);  
	}
	lr_end_transaction("T02_MobileCategoryPage", 2);

	web_cleanup_auto_headers();	
	
	return 0;
}
# 6 "e:\\performance\\scripts\\2017_r6\\03_perf\\ecommerce\\tcp_mobile\\\\combined_TCP_Mobile.c" 2

# 1 "ViewProduct.c" 1
ViewProduct()
{
 
	lr_think_time(TT);
	if (atoi(lr_eval_string("{s_products_count}"))>0){
	lr_save_string(lr_paramarr_random("s_products"), "s_product");
	}else{
				lr_exit(2,2);
				return 0;
	}
		web_add_auto_header("Accept", 
		"application/json, text/plain, */*");

	web_add_auto_header("Accept-Encoding", 
		"gzip,deflate");

	web_add_auto_header("Accept-Language", 
		"en-US");

	web_add_auto_header("User-Agent", 
		"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_auto_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");

	web_add_auto_header("catalogId", 
		"10551");

	web_add_auto_header("deviceType", 
		"App");

	web_add_auto_header("langId", "-1");

	web_add_header("productCatentryId", "{s_product}");

	web_add_auto_header("storeId","10151");
	web_reg_find("TEXT/IC=recordSetTotalMatches","SaveCount=recordSetTotalMatches_p","LAST");
	lr_start_transaction("T03_MobileProductViewPage");
	
	lr_start_sub_transaction("T03_MobileProductViewPage_getBundleIdAndBundleDetails","T03_MobileProductViewPage");
	
	web_custom_request("getBundleIdAndBundleDetails", 
		"URL=https://{p_hostname}/api/tcpproduct/getBundleIdAndBundleDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"LAST");
	
	if (atoi(lr_eval_string("{recordSetTotalMatches_p}"))>0){
		lr_end_sub_transaction("T03_MobileProductViewPage_getBundleIdAndBundleDetails", 2);  
	}else{
		lr_end_sub_transaction("T03_MobileProductViewPage_getBundleIdAndBundleDetails", 1);  
	}
	
	web_add_header("productId", "{s_product}");
	web_reg_find("TEXT/IC=getAllSKUInventoryByProductId","SaveCount=getAllSKUInventoryByProductId","LAST");
	web_reg_save_param_json( "ParamName=s_catentryIDs", "QueryString=$.getAllSKUInventoryByProductId..response..catentryId", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
	web_reg_save_param_json( "ParamName=s_quantities", "QueryString=$.getAllSKUInventoryByProductId..response..quantity", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
	lr_start_sub_transaction("T03_MobileProductViewPage_getSKUInventoryandProductCounterDetails","T03_MobileProductViewPage");
	
	web_custom_request("getSKUInventoryandProductCounterDetails", 
		"URL=https://{p_hostname}/api/tcpproduct/getSKUInventoryandProductCounterDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t2.inf", 
		"LAST");
	
	if (atoi(lr_eval_string("{getAllSKUInventoryByProductId}"))>0){
		lr_end_sub_transaction("T03_MobileProductViewPage_getSKUInventoryandProductCounterDetails", 2);  
	}else{
		lr_end_sub_transaction("T03_MobileProductViewPage_getSKUInventoryandProductCounterDetails", 1);  
	}	
	lr_end_transaction("T03_MobileProductViewPage", 2);
	web_cleanup_auto_headers();	
	return 0;
}
# 7 "e:\\performance\\scripts\\2017_r6\\03_perf\\ecommerce\\tcp_mobile\\\\combined_TCP_Mobile.c" 2

# 1 "AddToCart.c" 1
AddToCart()
{
	int i=0,j=0;
	int flag=0;
	char Kount[5];
	 
	lr_think_time(TT);
	if ((i=atoi(lr_eval_string("{s_catentryIDs_count}")))==0){
		lr_exit(5,2);
		return 0;
	}
	
	for (j=1;j<=i;++j){
		memset(Kount,0,sizeof(Kount));
		sprintf(Kount,"%d", j);
		lr_save_string(Kount,"dyn");
		if (atoi(lr_eval_string(lr_eval_string("{s_quantities_{dyn}}")))>10){
			flag=1;
			lr_save_string(lr_eval_string(lr_eval_string("{s_catentryIDs_{dyn}}")),"f_catentryID");
			break;
		}
	}

	if (flag==0){
		lr_exit(5,2);
		return 0;
	}
	
 

		web_add_auto_header("Accept", 
		"application/json, text/plain, */*");

	web_add_auto_header("Accept-Encoding", 
		"gzip,deflate");

	web_add_auto_header("Accept-Language", 
		"en-US");

	web_add_header("Origin", "file://");

	web_add_auto_header("User-Agent", 
		"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_auto_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");

	web_add_auto_header("catalogId", 
		"10551");

	web_add_auto_header("deviceType", 
		"App");

	web_add_auto_header("langId", 
		"-1");

	web_add_auto_header("storeId", 
		"10151");
	web_reg_find("TEXT/IC=orderId","SaveCount=storeId","LAST");   
	lr_start_transaction("T04_MobileProductAddToCart");
	lr_start_sub_transaction("T04_MobileProductAddToCart_addProductToCart","T04_MobileProductAddToCart");
	web_custom_request("addProductToCart", 
		"URL=https://{p_hostname}/api/tcpproduct/addProductToCart", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"Mode=HTTP", 
		"EncType=application/json", 
		"Body={\n  \"storeId\": \"10151\",\n  \"catalogId\": \"10551\",\n  \"langId\": \"-1\",\n  \"orderId\": \".\",\n  \"field2\": 0,\n  \"comment\": \"{f_catentryID}\",\n  \"calculationUsage\": \"-1,-2,-5,-6,-7\",\n  \"catEntryId\": \"{f_catentryID}\",\n  \"quantity\": \"1\",\n  \"requesttype\": \"ajax\"\n}", 
		"LAST");
	
	if (atoi(lr_eval_string("{storeId}"))>0){
		lr_end_sub_transaction("T04_MobileProductAddToCart_addProductToCart", 2);  
	}else{
		lr_end_sub_transaction("T04_MobileProductAddToCart_addProductToCart", 1);  
	}	

	web_reg_find("TEXT/IC=appliedCoupons","SaveCount=couponsapply","LAST");   
	lr_start_sub_transaction("T04_MobileProductAddToCart_getAllCoupons","T04_MobileProductAddToCart");
	web_custom_request("getAllCoupons", 
		"URL=https://{p_hostname}/api/tcporder/getAllCoupons", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t2.inf", 
		"LAST");
	
	if (atoi(lr_eval_string("{couponsapply}"))>0){
		lr_end_sub_transaction("T04_MobileProductAddToCart_getAllCoupons", 2);  
	}else{
		lr_end_sub_transaction("T04_MobileProductAddToCart_getAllCoupons", 1);  
	}	
	web_add_header("locStore", 
		"false");

	web_add_header("pageName", 
		"orderSummary");

	web_reg_find("TEXT/IC={","SaveCount=getOrderHistoryResponse","LAST");
	lr_start_sub_transaction ( "T04_MobileProductAddToCart_getOrderDetails", "T04_MobileProductAddToCart" );

	web_custom_request("getOrderDetails", 
		"URL=https://{p_hostname}/api/getOrderDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t3.inf", 
		"LAST");
	if (atoi(lr_eval_string("{getOrderHistoryResponse}"))>0){
		lr_end_sub_transaction("T04_MobileProductAddToCart_getOrderDetails", 2);  
	}else{
		lr_end_sub_transaction("T04_MobileProductAddToCart_getOrderDetails", 1);  
	}	
	
	lr_end_transaction("T04_MobileProductAddToCart", 2);
	web_cleanup_auto_headers();		
	return 0;
}
# 8 "e:\\performance\\scripts\\2017_r6\\03_perf\\ecommerce\\tcp_mobile\\\\combined_TCP_Mobile.c" 2

# 1 "LoginFirst.c" 1
LoginFirst()
{
	
	 
	lr_think_time(TT);
	web_add_auto_header("Accept", 
		"application/json, text/plain, */*");

	web_add_auto_header("Accept-Encoding", 
		"gzip,deflate");

	web_add_auto_header("Accept-Language", 
		"en-US");

	web_add_header("Origin", 
		"file://");

	web_add_auto_header("User-Agent", 
		"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_auto_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");

	web_add_auto_header("catalogId", 
		"10551");

	web_add_auto_header("deviceType", 
		"App");

	web_add_auto_header("langId", 
		"-1");

	web_add_auto_header("storeId", 
		"10151");
	 
	web_reg_find("TEXT/IC=LoginSuccess","SaveCount=LoginSuccess","LAST");
	lr_start_transaction("T02a_Mobile_LoginFirst");
	lr_start_sub_transaction ( "T02a_Mobile_LoginFirst_logon", "T02a_Mobile_LoginFirst" );
	web_custom_request("logon", 
		"URL=https://{p_hostname}/api/logon", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"Mode=HTTP", 
		"EncType=application/json", 
		"Body={\n  \"reLogonURL\": \"TCPAjaxLogonErrorView\",\n  \"URL\": \"TCPAjaxLogonSuccessView\",\n  \"storeId\": \"10151\",\n  \"rememberCheck\": \"true\",\n  \"logonId1\": \"{p_userID}\",\n  \"logonPassword1\": \"Asdf!234\",\n  \"rememberMe\": true,\n  \"useTouchId\": false\n}", 	
		"LAST");

 
	if (atoi(lr_eval_string("{LoginSuccess}"))>0){
		lr_end_sub_transaction("T02a_Mobile_LoginFirst_logon", 2);  
	}else{
		lr_end_sub_transaction("T02a_Mobile_LoginFirst_logon", 1);  
	}
	
	web_reg_find("TEXT/IC=resourceName","SaveCount=reguserdetails","LAST");
	lr_start_sub_transaction ( "T02a_Mobile_LoginFirst_getRegisteredUserDetailsInfo", "T02a_Mobile_LoginFirst" );
	web_custom_request("getRegisteredUserDetailsInfo", 
		"URL=https://{p_hostname}/api/payment/getRegisteredUserDetailsInfo", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"LAST");
	
	if (atoi(lr_eval_string("{reguserdetails}"))>0){
		lr_end_sub_transaction("T02a_Mobile_LoginFirst_getRegisteredUserDetailsInfo", 2);  
	}else{
		lr_end_sub_transaction("T02a_Mobile_LoginFirst_getRegisteredUserDetailsInfo", 1);  
	}
	
	web_add_header("fromRest", 
		"true");

	web_reg_find("TEXT/IC=getOrderHistoryResponse","SaveCount=getOrderHistoryResponse","LAST");
	lr_start_sub_transaction ( "T02a_Mobile_LoginFirst_getPointsAndOrderHistory", "T02a_Mobile_LoginFirst" );
	web_custom_request("getPointsAndOrderHistory", 
		"URL=https://{p_hostname}/api/tcporder/getPointsAndOrderHistory", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t2.inf", 
		"LAST");
	if (atoi(lr_eval_string("{getOrderHistoryResponse}"))>0){
		lr_end_sub_transaction("T02a_Mobile_LoginFirst_getPointsAndOrderHistory", 2);  
	}else{
		lr_end_sub_transaction("T02a_Mobile_LoginFirst_getPointsAndOrderHistory", 1);  
	}

	web_add_header("calc", "false");
	web_add_header("locStore", "True");
	web_add_header("pageName", "fullOrderInfo");

	web_reg_find("TEXT/IC=shippingCharge","SaveCount=shippingCharge","LAST");
	lr_start_sub_transaction ( "T02a_Mobile_LoginFirst_getOrderDetails", "T02a_Mobile_LoginFirst" );
	web_custom_request("getOrderDetails", 
		"URL=https://{p_hostname}/api/getOrderDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t4.inf", 
		"LAST");
	if (atoi(lr_eval_string("{shippingCharge}"))>0){
		lr_end_sub_transaction("T02a_Mobile_LoginFirst_getOrderDetails", 2);  
	}else{
		lr_end_sub_transaction("T02a_Mobile_LoginFirst_getOrderDetails", 1);  
	}

	web_add_header("action", 
		"get");
 

	web_reg_find("TEXT/IC={","SaveCount=getfavouriteStoreLocation","LAST");
	lr_start_sub_transaction ( "T02a_Mobile_LoginFirst_getFavouriteStoreLocation", "T02a_Mobile_LoginFirst" );
	web_custom_request("getFavouriteStoreLocation", 
		"URL=https://{p_hostname}/api/tcporder/getFavouriteStoreLocation/", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t5.inf", 
		"LAST");
	if (atoi(lr_eval_string("{getfavouriteStoreLocation}"))>0){
		lr_end_sub_transaction("T02a_Mobile_LoginFirst_getFavouriteStoreLocation", 2);  
	}else{
		lr_end_sub_transaction("T02a_Mobile_LoginFirst_getFavouriteStoreLocation", 1);  
	}
 
lr_continue_on_error(1);
 	
# 153 "LoginFirst.c"
	web_reg_find("TEXT/IC=appliedCoupons","SaveCount=couponsapply","LAST");   
	lr_start_sub_transaction("T02a_Mobile_LoginFirst_getAllCoupons","T02a_Mobile_LoginFirst");
	web_custom_request("getAllCoupons", 
		"URL=https://{p_hostname}/api/tcporder/getAllCoupons", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t2.inf", 
		"LAST");
	lr_continue_on_error(0);
	
	if (atoi(lr_eval_string("{couponsapply}"))>0){
		lr_end_sub_transaction("T02a_Mobile_LoginFirst_getAllCoupons", 2);  
	}else{
		lr_end_sub_transaction("T02a_Mobile_LoginFirst_getAllCoupons", 1);  
	}	
 
# 191 "LoginFirst.c"
	lr_end_transaction("T02a_Mobile_LoginFirst", 2);
	web_cleanup_auto_headers();		
	isLoggedin=1;
	return 0;
}
# 9 "e:\\performance\\scripts\\2017_r6\\03_perf\\ecommerce\\tcp_mobile\\\\combined_TCP_Mobile.c" 2

# 1 "BrowseFirstLogin.c" 1
BrowseFirstLogin()
{
	lr_think_time(TT);
	web_cleanup_auto_headers();		
	
		web_add_auto_header("Accept", 
		"application/json, text/plain, */*");

	web_add_auto_header("Accept-Encoding", 
		"gzip,deflate");

	web_add_auto_header("Accept-Language", 
		"en-US");

	web_add_auto_header("User-Agent", 
		"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_auto_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");

	web_add_header("calc", 
		"false");

	web_add_auto_header("catalogId", 
		"10551");

	web_add_auto_header("deviceType", 
		"App");

	web_add_auto_header("langId", 
		"-1");

	web_add_header("locStore", 
		"true");

	web_add_header("pageName", 
		"fullOrderInfo");

	web_add_auto_header("storeId", 
		"10151");

	lr_start_transaction("T07a_Mobile_BrowseFirstLogin");

	web_reg_find("TEXT/IC=shippingCharge","SaveCount=shippingCharge","LAST");
	lr_start_sub_transaction ( "T07a_Mobile_BrowseFirstLogin_getOrderDetails", "T07a_Mobile_BrowseFirstLogin" );
	web_custom_request("getOrderDetails", 
		"URL=https://{p_hostname}/api/getOrderDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"LAST");
	if (atoi(lr_eval_string("{shippingCharge}"))>0){
		lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_getOrderDetails", 2);  
	}else{
		lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_getOrderDetails", 1);  
	}
	web_add_header("Origin", 
		"file://");
	web_reg_find("TEXT/IC=LoginSuccess","SaveCount=LoginSuccess","LAST");
	
	lr_start_sub_transaction ( "T07a_Mobile_BrowseFirstLogin_logon", "T07a_Mobile_BrowseFirstLogin" );
	web_custom_request("logon", 
		"URL=https://{p_hostname}/api/logon", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t2.inf", 
		"Mode=HTTP", 
		"EncType=application/json", 
		"Body={\n  \"reLogonURL\": \"TCPAjaxLogonErrorView\",\n  \"URL\": \"TCPAjaxLogonSuccessView\",\n  \"storeId\": \"10151\",\n  \"rememberCheck\": \"true\",\n  \"logonId1\": \"{p_userID}\",\n  \"logonPassword1\": \"Asdf!234\",\n  \"rememberMe\": true,\n  \"useTouchId\": false\n}", 
		"LAST");
	   
 
if (atoi(lr_eval_string("{LoginSuccess}"))>0){
		lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_logon", 2);  
	}else{
		lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_logon", 1);  
	}
	  web_reg_find("TEXT/IC=resourceName","SaveCount=reguserdetails","LAST");
	lr_start_sub_transaction ( "T07a_Mobile_BrowseFirstLogin_getRegisteredUserDetailsInfo", "T07a_Mobile_BrowseFirstLogin" );
	web_custom_request("getRegisteredUserDetailsInfo", 
		"URL=https://{p_hostname}/api/payment/getRegisteredUserDetailsInfo", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t3.inf", 
		"LAST");
	if (atoi(lr_eval_string("{reguserdetails}"))>0){
		lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_getRegisteredUserDetailsInfo", 2);  
	}else{
		lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_getRegisteredUserDetailsInfo", 1);  
	}
	
	web_add_header("fromRest", 
		"true");
	web_reg_find("TEXT/IC=getOrderHistoryResponse","SaveCount=getOrderHistoryResponse","LAST");
	lr_start_sub_transaction ( "T07a_Mobile_BrowseFirstLogin_getPointsAndOrderHistory", "T07a_Mobile_BrowseFirstLogin" );
	web_custom_request("getPointsAndOrderHistory", 
		"URL=https://{p_hostname}/api/tcporder/getPointsAndOrderHistory", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t4.inf", 
		"LAST");
if (atoi(lr_eval_string("{getOrderHistoryResponse}"))>0){
		lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_getPointsAndOrderHistory", 2);  
	}else{
		lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_getPointsAndOrderHistory", 1);  
	}
	web_add_header("locStore", 
		"false");

	web_add_header("pageName", 
		"orderSummary");

	web_reg_find("TEXT/IC=shippingCharge","SaveCount=shippingCharge","LAST");
	lr_start_sub_transaction ( "T07a_Mobile_BrowseFirstLogin_getOrderDetails", "T07a_Mobile_BrowseFirstLogin" );
	web_custom_request("getOrderDetails", 
		"URL=https://{p_hostname}/api/getOrderDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"LAST");
	if (atoi(lr_eval_string("{shippingCharge}"))>0){
		lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_getOrderDetails", 2);  
	}else{
		lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_getOrderDetails", 1);  
	}

	web_add_header("action", 
		"get");
	web_reg_find("TEXT/IC={","SaveCount=getfavouriteStoreLocation","LAST");
	lr_start_sub_transaction ( "T07a_Mobile_BrowseFirstLogin_getFavouriteStoreLocation", "T07a_Mobile_BrowseFirstLogin" );
	web_custom_request("getFavouriteStoreLocation", 
		"URL=https://{p_hostname}/api/tcporder/getFavouriteStoreLocation/", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t6.inf", 
		"LAST");
	if (atoi(lr_eval_string("{getfavouriteStoreLocation}"))>0){
		lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_getFavouriteStoreLocation", 2);  
	}else{
		lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_getFavouriteStoreLocation", 1);  
	}
	web_add_header("isRest", 
		"true");
web_reg_find("TEXT/IC=creditCardInfoJson","SaveCount=creditCardInfoJson","LAST");   
	lr_start_sub_transaction("T07a_Mobile_BrowseFirstLogin_getCreditCardDetails","T07a_Mobile_BrowseFirstLogin");
	web_custom_request("getCreditCardDetails", 
		"URL=https://{p_hostname}/api/payment/getCreditCardDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t7.inf", 
		"LAST");
	if (atoi(lr_eval_string("{creditCardInfoJson}"))>0){
		lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_getCreditCardDetails", 2);  
	}else{
		lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_getCreditCardDetails", 1);  
	}
	web_custom_request("getAddressFromBook", 
		"URL=https://{p_hostname}/api/payment/getAddressFromBook", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t8.inf", 
		"LAST");

	web_add_header("calc", 
		"false");

	web_add_header("locStore", 
		"true");

	web_add_header("pageName", 
		"fullOrderInfo");

		web_reg_find("TEXT/IC=shippingCharge","SaveCount=shippingCharge","LAST");
	lr_start_sub_transaction ( "T07a_Mobile_BrowseFirstLogin_getOrderDetails", "T07a_Mobile_BrowseFirstLogin" );
	web_custom_request("getOrderDetails", 
		"URL=https://{p_hostname}/api/getOrderDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"LAST");
	if (atoi(lr_eval_string("{shippingCharge}"))>0){
		lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_getOrderDetails", 2);  
	}else{
		lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_getOrderDetails", 1);  
	}
	
	lr_continue_on_error(1);
	
	lr_start_sub_transaction ( "T07a_Mobile_BrowseFirstLogin_getListofDefaultWishlist", "T07a_Mobile_BrowseFirstLogin" );
	web_custom_request("getListofDefaultWishlist", 
		"URL=https://{p_hostname}/api/getListofDefaultWishlist", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t10.inf", 
		"Body={\"productId\":[]}",
		"LAST");
	lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_getListofDefaultWishlist", 2); 
	
	lr_continue_on_error(0);
	
	web_add_header("externalId", 
		"286018");

	lr_start_sub_transaction ( "T07a_Mobile_BrowseFirstLogin_getListofDefaultWishlistbyId", "T07a_Mobile_BrowseFirstLogin" );
	web_custom_request("getListofDefaultWishlistbyId", 
		"URL=https://{p_hostname}/api/tcpproduct/getListofDefaultWishlistbyId", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t11.inf", 
		"LAST");
	lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_getListofDefaultWishlistbyId", 2); 

	lr_start_sub_transaction ( "T07a_Mobile_BrowseFirstLogin_getAllCoupons", "T07a_Mobile_BrowseFirstLogin" );
	web_custom_request("getAllCoupons", 
		"URL=https://{p_hostname}/api/tcporder/getAllCoupons", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t12.inf", 
		"LAST");
	lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_getAllCoupons", 2); 

	lr_start_sub_transaction ( "T07a_Mobile_BrowseFirstLogin_giftOptionsCmd", "T07a_Mobile_BrowseFirstLogin" );
	web_custom_request("giftOptionsCmd", 
		"URL=https://{p_hostname}/api/payment/giftOptionsCmd", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t13.inf", 
		"LAST");
	lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_giftOptionsCmd", 2); 

	web_add_auto_header("zipCode", 
		"07094");

	lr_start_sub_transaction ( "T07a_Mobile_BrowseFirstLogin_getShipmentMethods", "T07a_Mobile_BrowseFirstLogin" );
	web_custom_request("getShipmentMethods", 
		"URL=https://{p_hostname}/api/payment/getShipmentMethods", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t14.inf", 
		"LAST");
	lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_getShipmentMethods", 2); 

	lr_start_sub_transaction ( "T07a_Mobile_BrowseFirstLogin_getShipmentMethods_2", "T07a_Mobile_BrowseFirstLogin" );
	web_custom_request("getShipmentMethods_2", 
		"URL=https://{p_hostname}/api/payment/getShipmentMethods", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t15.inf", 
		"LAST");
	lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_getShipmentMethods_2", 2); 
	lr_end_transaction("T07a_Mobile_BrowseFirstLogin", 2);

	return 0;
}
# 10 "e:\\performance\\scripts\\2017_r6\\03_perf\\ecommerce\\tcp_mobile\\\\combined_TCP_Mobile.c" 2

# 1 "GuestCheckout.c" 1

GuestCheckout(){
	lr_think_time(TT);
web_cleanup_auto_headers();
	web_add_header("Accept",
		"application/json, text/plain, */*");

	web_add_header("Accept-Encoding", 
		"gzip,deflate");

	web_add_header("Accept-Language", 
		"en-US");

	web_add_header("User-Agent", 
		"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");

	web_add_header("calc", 
		"false");

	web_add_header("catalogId", 
		"10551");

	web_add_header("deviceType", 
		"App");

	web_add_header("langId", 
		"-1");

	web_add_header("locStore", 
		"true");

	web_add_header("pageName", 
		"fullOrderInfo");

	web_add_header("storeId", 
		"10151");
	web_reg_find("TEXT/IC=shippingCharge","SaveCount=shippingCharge","LAST");
		lr_start_transaction("T06a_Mobile_GuestCheckouta");
	lr_start_sub_transaction ( "T06a_Mobile_GuestCheckouta_getOrderDetails", "T06a_Mobile_GuestCheckouta" );
	web_custom_request("getOrderDetails", 
		"URL=https://{p_hostname}/api/getOrderDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"LAST");

	if (atoi(lr_eval_string("{shippingCharge}"))>0){
		lr_end_sub_transaction("T06a_Mobile_GuestCheckouta_getOrderDetails", 2);  
	}else{
		lr_end_sub_transaction("T06a_Mobile_GuestCheckouta_getOrderDetails", 1);  
	}
	
	lr_end_transaction("T06a_Mobile_GuestCheckouta", 2);
	if (p_MobileRand<=LoginDropRatio)
		lr_exit(5, 2);
	lr_think_time(TT);
	
		web_add_auto_header("Accept", 
		"application/json, text/plain, */*");

	web_add_auto_header("Accept-Encoding", 
		"gzip,deflate");

	web_add_auto_header("Accept-Language", 
		"en-US");

	web_add_auto_header("User-Agent", 
		"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_auto_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");

	web_add_auto_header("catalogId", 
		"10551");

	web_add_auto_header("deviceType", 
		"App");

	web_add_auto_header("langId", 
		"-1");

	web_add_auto_header("storeId", 
		"10151");
	lr_start_transaction("T06a_Mobile_GuestCheckoutb");

	web_custom_request("getAllCoupons", 
		"URL=https://{p_hostname}/api/tcporder/getAllCoupons", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"LAST");

	web_custom_request("giftOptionsCmd", 
		"URL=https://{p_hostname}/api/payment/giftOptionsCmd", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t2.inf", 
		"LAST");

	web_custom_request("getShipmentMethods", 
		"URL=https://{p_hostname}/api/payment/getShipmentMethods", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t3.inf", 
		"LAST");

		lr_end_transaction("T06a_Mobile_GuestCheckoutb", 2);
				lr_think_time(TT);
web_cleanup_auto_headers();
		web_add_auto_header("Accept", 
		"application/json, text/plain, */*");

	web_add_auto_header("Accept-Encoding", 
		"gzip,deflate");

	web_add_auto_header("Accept-Language", 
		"en-US");

	web_add_auto_header("User-Agent", 
		"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_auto_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");

	web_add_header("addressField1", 
		"{GUEST_ADR1}");

	web_add_auto_header("catalogId", 
		"10551");

	web_add_auto_header("deviceType", 
		"App");

	web_add_auto_header("langId", 
		"-1");

	web_add_header("state", 
		"{GUEST_STATE}");

	web_add_auto_header("storeId", 
		"10151");

	web_add_header("zipCode", 
		"{GUEST_ZIP}");
				lr_think_time(TT);
	lr_start_transaction("T06a_Mobile_GuestShipping");

	web_custom_request("getShipmentMethods", 
		"URL=https://{p_hostname}/api/payment/getShipmentMethods", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"LAST");

	web_add_auto_header("Origin", 
		"file://");
	web_reg_save_param("addressId", "LB=\"addressId\": \"", "RB=\",\n", "NotFound=Warning", "LAST");
	web_reg_save_param ( "addressIdAll" , "LB=\"addressId\": \"" , "RB=\",\n" , "ORD=ALL", "NotFound=Warning", "LAST" ) ;  
	web_custom_request("addAddress", 
		"URL=https://{p_hostname}/api/payment/addAddress", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t2.inf", 
		"Mode=HTTP", 
		"EncType=application/json", 
		"Body={\n  \"contact\": [\n    {\n      \"addressLine\": [\n        \"{GUEST_ADR1}\",\n        \"\",\n        \"\"\n      ],\n      \"addressType\": \"Shipping\",\n      \"attributes\": [\n        {\n          \"key\": \"addressField3\",\n          \"value\": \"{GUEST_ZIP}\"\n        }\n      ],\n      \"city\": \"Robbinsville\",\n      \"country\": \"US\",\n      \"firstName\": \"Pavan\",\n      \"lastName\": \"Dusi\",\n      \"email1\": \"pdusi{p_rand}@childrensplace.com\",\n      \"phone1\": \"2123121212\","
		"\n      \"state\": \"{GUEST_STATE}\",\n      \"zipCode\": \"{GUEST_ZIP}\",\n      \"xcont_addressField3\": \"{GUEST_ZIP}\",\n      \"nickName\": \"1506015033814\",\n      \"fromPage\": \"\",\n      \"phone1Publish\": \"true\",\n      \"primary\": \"false\"\n    }\n  ]\n}", 
		"LAST");

	web_add_header("content-type", 
		"application/json");

	web_custom_request("updateShippingMethodSelection", 
		"URL=https://{p_hostname}/api/payment/updateShippingMethodSelection", 
		"Method=PUT", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t3.inf", 
		"Mode=HTTP", 
		"Body={\n  \"shipModeId\": \"901101\",\n  \"addressId\": \"{addressId}\",\n  \"x_calculationUsage\": \"-1,-2,-3,-4,-5,-6,-7\",\n  \"requesttype\": \"ajax\",\n  \"orderId\": \"{s_OrderId_1}\"\n}", 
		"LAST");

	(web_remove_auto_header("Origin", "ImplicitGen=Yes", "LAST"));

	web_add_header("calc", 
		"false");

	web_add_header("locStore", 
		"true");

	web_add_header("pageName", 
		"fullOrderInfo");

	web_custom_request("getOrderDetails", 
		"URL=https://{p_hostname}/api/getOrderDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t4.inf", 
		"LAST");

	web_custom_request("getAllCoupons", 
		"URL=https://{p_hostname}/api/tcporder/getAllCoupons", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t5.inf", 
		"LAST");
	lr_end_transaction("T06a_Mobile_GuestShipping", 2);
	lr_think_time(TT);
	if (p_MobileRand<=(LoginDropRatio+ShipPageRatio))
		lr_exit(5, 2);
web_cleanup_auto_headers();	
	lr_start_transaction("T07_Mobile_GuestBilling");
		web_add_auto_header("Accept", 
		"application/json, text/plain, */*");

	web_add_auto_header("Accept-Encoding", 
		"gzip,deflate");

	web_add_auto_header("Accept-Language", 
		"en-US");

	web_add_auto_header("Origin", 
		"file://");

	web_add_auto_header("User-Agent", 
		"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_auto_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");

	web_add_auto_header("catalogId", 
		"10551");

	web_add_header("content-type", 
		"application/json");

	web_add_auto_header("deviceType", 
		"App");

	web_add_auto_header("langId", 
		"-1");

	web_add_auto_header("storeId", 
		"10151");

	web_custom_request("updateAddress", 
		"URL=https://{p_hostname}/api/payment/updateAddress", 
		"Method=PUT", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"Mode=HTTP", 
		"Body={\n  \"fromPage\": \"checkout\",\n  \"addressId\": \"{addressId}\"\n}", 
		"LAST");

	web_custom_request("addPaymentInstruction", 
		"URL=https://{p_hostname}/api/payment/addPaymentInstruction", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t2.inf", 
		"Mode=HTTP", 
		"EncType=application/json", 
		"Body={  \"paymentInstruction\": [    {      \"account\": \"2818526303813893\",      \"isDefault\": \"false\",\n      \"billing_address_id\": \"{addressId}\",\n      \"payMethodId\": \"VISA\",\n      \"cc_brand\": \"VISA\",\n      \"expire_month\": \"11\",\n      \"expire_year\": \"2020\",\n      \"cc_cvc\": \"123\",\n      \"piAmount\": \"{piAmount}\"\n    }\n  ]\n}", 
		"LAST");
 
	(web_remove_auto_header("Origin", "ImplicitGen=Yes", "LAST"));

	web_add_header("calc", 
		"false");

	web_add_header("locStore", 
		"true");

	web_add_header("pageName", 
		"fullOrderInfo");

	web_custom_request("getOrderDetails", 
		"URL=https://{p_hostname}/api/getOrderDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t3.inf", 
		"LAST");

	web_custom_request("getAllCoupons", 
		"URL=https://{p_hostname}/api/tcporder/getAllCoupons", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t4.inf", 
		"LAST");

	lr_end_transaction("T07_Mobile_GuestBilling", 2);
		if (p_MobileRand<=(LoginDropRatio+ShipPageRatio+BillPageRatio))
		lr_exit(5, 2);
	web_cleanup_auto_headers();
			lr_think_time(TT);
	lr_start_transaction("T16_Submit_Order_Mobile_Guest");
	
	web_add_auto_header("Accept", 
		"application/json, text/plain, */*");

	web_add_auto_header("Accept-Encoding", 
		"gzip,deflate");

	web_add_auto_header("Accept-Language", 
		"en-US");

	web_add_header("Origin", 
		"file://");

	web_add_auto_header("User-Agent", 
		"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_auto_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");

	web_add_auto_header("catalogId", 
		"10551");

	web_add_auto_header("deviceType", 
		"App");

	web_add_auto_header("langId", 
		"-1");

	web_add_auto_header("storeId", 
		"10151");
 
	web_custom_request("addCheckout", 
		"URL=https://{p_hostname}/api/addCheckout", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"Mode=HTTP", 
		"EncType=application/json", 
		"Body={  \"orderId\": \"{s_OrderId_1}\",  \"isRest\": \"true\",  \"locStore\": \"True\"}", 
		"LAST");

	web_custom_request("getRegisteredUserDetailsInfo", 
		"URL=https://{p_hostname}/api/payment/getRegisteredUserDetailsInfo", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t2.inf", 
		"LAST");

	web_add_header("fromRest", 
		"true");

	web_custom_request("getPointsAndOrderHistory", 
		"URL=https://{p_hostname}/api/tcporder/getPointsAndOrderHistory", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t3.inf", 
		"LAST");
 
# 400 "GuestCheckout.c"
	web_add_header("depthAndLimit", 
		"-1,-1,0");

	web_custom_request("getTopCategories", 
		"URL=https://{p_hostname}/api/tcpproduct/getTopCategories", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t5.inf", 
		"LAST");
 	lr_end_transaction("T16_Submit_Order_Mobile_Guest", 2);
# 424 "GuestCheckout.c"


	return 0;
}

GuestCheckout1()
{

	lr_think_time(TT);

	web_add_header("Accept",
		"application/json, text/plain, */*");

	web_add_header("Accept-Encoding", 
		"gzip,deflate");

	web_add_header("Accept-Language", 
		"en-US");

	web_add_header("User-Agent", 
		"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");

	web_add_header("calc", 
		"false");

	web_add_header("catalogId", 
		"10551");

	web_add_header("deviceType", 
		"App");

	web_add_header("langId", 
		"-1");

	web_add_header("locStore", 
		"true");

	web_add_header("pageName", 
		"fullOrderInfo");

	web_add_header("storeId", 
		"10151");
	web_reg_find("TEXT/IC=shippingCharge","SaveCount=shippingCharge","LAST");
		lr_start_transaction("T06a_Mobile_GuestCheckouta");
	lr_start_sub_transaction ( "T06a_Mobile_GuestCheckouta_getOrderDetails", "T06a_Mobile_GuestCheckouta" );
	web_custom_request("getOrderDetails", 
		"URL=https://{p_hostname}/api/getOrderDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"LAST");

	if (atoi(lr_eval_string("{shippingCharge}"))>0){
		lr_end_sub_transaction("T06a_Mobile_GuestCheckouta_getOrderDetails", 2);  
	}else{
		lr_end_sub_transaction("T06a_Mobile_GuestCheckouta_getOrderDetails", 1);  
	}
	
	lr_end_transaction("T06a_Mobile_GuestCheckouta", 2);
	lr_think_time(TT);
	
		web_add_auto_header("Accept", 
		"application/json, text/plain, */*");

	web_add_auto_header("Accept-Encoding", 
		"gzip,deflate");

	web_add_auto_header("Accept-Language", 
		"en-US");

	web_add_auto_header("User-Agent", 
		"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_auto_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");

	web_add_auto_header("catalogId", 
		"10551");

	web_add_auto_header("deviceType", 
		"App");

	web_add_auto_header("langId", 
		"-1");

	web_add_auto_header("storeId", 
		"10151");
	lr_start_transaction("T06a_Mobile_GuestCheckoutb");

	web_custom_request("getAllCoupons", 
		"URL=https://{p_hostname}/api/tcporder/getAllCoupons", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"LAST");

	web_custom_request("giftOptionsCmd", 
		"URL=https://{p_hostname}/api/payment/giftOptionsCmd", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t2.inf", 
		"LAST");

	web_custom_request("getShipmentMethods", 
		"URL=https://{p_hostname}/api/payment/getShipmentMethods", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t3.inf", 
		"LAST");

		lr_end_transaction("T06a_Mobile_GuestCheckoutb", 2);
web_cleanup_auto_headers();
		lr_start_transaction("T06a_Mobile_GuestShipping");
	web_add_auto_header("Accept", 
		"application/json, text/plain, */*");

	web_add_auto_header("Accept-Encoding", 
		"gzip,deflate");

	web_add_auto_header("Accept-Language", 
		"en-US");

	web_add_auto_header("Origin", 
		"file://");

	web_add_auto_header("User-Agent", 
		"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_auto_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");

	web_add_auto_header("catalogId", 
		"10551");

	web_add_auto_header("content-type", 
		"application/json");

	web_add_auto_header("deviceType", 
		"App");

	web_add_auto_header("langId", 
		"-1");

	web_add_header("nickName", 
		"sb_2017-09-21 09:33:05.632");

	web_add_auto_header("storeId", 
		"10151");
	web_reg_save_param("addressId", "LB=\"addressId\": \"", "RB=\",\n", "NotFound=Warning", "LAST");
	web_reg_save_param ( "addressIdAll" , "LB=\"addressId\": \"" , "RB=\",\n" , "ORD=ALL", "NotFound=Warning", "LAST" ) ;  
	web_custom_request("updateAddress", 
		"URL=https://{p_hostname}/api/payment/addAddress", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"Mode=HTTP", 
		"Body={  \"contact\": [    {      \"addressLine\": [        \"{GUEST_ADR1}\",        \"\",        \"\"      ],      \"addressType\": \"Shipping\",      \"attributes\": [        {          \"key\": \"addressField3\",          \"value\": \"{GUEST_ZIP}\"        }      ],      \"city\": \"Robbinsville\",      \"country\": \"US\",      \"firstName\": \"Pavan\",      \"lastName\": \"Dusi\",      \"email1\": \"pdusi{p_rand}@childrensplace.com\",      \"phone1\": \"2123121212\",      \"state\": \"{GUEST_STATE}\",      \"zipCode\": \"{GUEST_ZIP}\",      \"xcont_addressField3\": \"{GUEST_ZIP}\",      \"nickName\": \"150601{p_rand}\",      \"fromPage\": \"\",      \"phone1Publish\": \"true\",      \"primary\": \"false\"    }  ]}",
		 
		 
		"LAST");

	web_custom_request("updateShippingMethodSelection", 
		"URL=https://{p_hostname}/api/payment/updateShippingMethodSelection", 
		"Method=PUT", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t2.inf", 
		"Mode=HTTP", 
		"Body={\n  \"shipModeId\": \"\",\n  \"addressId\": \"{addressId}\",\n  \"x_calculationUsage\": \"-1,-2,-3,-4,-5,-6,-7\",\n  \"requesttype\": \"ajax\",\n  \"orderId\": \"{s_OrderId_1}\"\n}", 
		"LAST");

	(web_remove_auto_header("Origin", "ImplicitGen=Yes", "LAST"));

	(web_remove_auto_header("content-type", "ImplicitGen=Yes", "LAST"));

	web_add_header("calc", 
		"false");

	web_add_header("locStore", 
		"true");

	web_add_header("pageName", 
		"fullOrderInfo");

	web_custom_request("getOrderDetails", 
		"URL=https://{p_hostname}/api/getOrderDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t3.inf", 
		"LAST");

	web_custom_request("getAllCoupons", 
		"URL=https://{p_hostname}/api/tcporder/getAllCoupons", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t4.inf", 
		"LAST");
	
	lr_end_transaction("T06a_Mobile_GuestShipping", 2);

	lr_start_transaction("T07_Mobile_GuestBilling");
		web_add_auto_header("Accept", 
		"application/json, text/plain, */*");

	web_add_auto_header("Accept-Encoding", 
		"gzip,deflate");

	web_add_auto_header("Accept-Language", 
		"en-US");

	web_add_auto_header("Origin", 
		"file://");

	web_add_auto_header("User-Agent", 
		"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_auto_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");

	web_add_auto_header("catalogId", 
		"10551");

	web_add_header("content-type", 
		"application/json");

	web_add_auto_header("deviceType", 
		"App");

	web_add_auto_header("langId", 
		"-1");

	web_add_auto_header("storeId", 
		"10151");

	web_custom_request("updateAddress", 
		"URL=https://{p_hostname}/api/payment/updateAddress", 
		"Method=PUT", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"Mode=HTTP", 
		"Body={\n  \"fromPage\": \"checkout\",\n  \"addressId\": \"{addressId}\"\n}", 
		"LAST");

	web_custom_request("addPaymentInstruction", 
		"URL=https://{p_hostname}/api/payment/addPaymentInstruction", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t2.inf", 
		"Mode=HTTP", 
		"EncType=application/json", 
		"Body={\n  \"paymentInstruction\": [\n    {\n      \"account\": \"4111111111111111\",\n      \"isDefault\": \"false\",\n      \"billing_address_id\": \"{addressId}\",\n      \"payMethodId\": \"COMPASSVISA\",\n      \"cc_brand\": \"VISA\",\n      \"expire_month\": \"3\",\n      \"expire_year\": \"2020\",\n      \"cc_cvc\": \"123\",\n      \"piAmount\": \"{piAmount}\"\n    }\n  ]\n}", 
		"LAST");

	(web_remove_auto_header("Origin", "ImplicitGen=Yes", "LAST"));

	web_add_header("calc", 
		"false");

	web_add_header("locStore", 
		"true");

	web_add_header("pageName", 
		"fullOrderInfo");

	web_custom_request("getOrderDetails", 
		"URL=https://{p_hostname}/api/getOrderDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t3.inf", 
		"LAST");

	web_custom_request("getAllCoupons", 
		"URL=https://{p_hostname}/api/tcporder/getAllCoupons", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t4.inf", 
		"LAST");

	lr_end_transaction("T07_Mobile_GuestBilling", 2);

	lr_start_transaction("T16_Submit_Order_Mobile_Guest");
	
	web_add_auto_header("Accept", 
		"application/json, text/plain, */*");

	web_add_auto_header("Accept-Encoding", 
		"gzip,deflate");

	web_add_auto_header("Accept-Language", 
		"en-US");

	web_add_header("Origin", 
		"file://");

	web_add_auto_header("User-Agent", 
		"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_auto_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");

	web_add_auto_header("catalogId", 
		"10551");

	web_add_auto_header("deviceType", 
		"App");

	web_add_auto_header("langId", 
		"-1");

	web_add_auto_header("storeId", 
		"10151");

	web_custom_request("addCheckout", 
		"URL=https://{p_hostname}/api/addCheckout", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"Mode=HTTP", 
		"EncType=application/json", 
		"Body={\n  \"orderId\": \"{s_OrderId_1}\",\n  \"isRest\": \"true\",\n  \"locStore\": \"True\"\n}", 
		"LAST");

	web_custom_request("getRegisteredUserDetailsInfo", 
		"URL=https://{p_hostname}/api/payment/getRegisteredUserDetailsInfo", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t2.inf", 
		"LAST");

	web_add_header("fromRest", 
		"true");

	web_custom_request("getPointsAndOrderHistory", 
		"URL=https://{p_hostname}/api/tcporder/getPointsAndOrderHistory", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t3.inf", 
		"LAST");
 
# 807 "GuestCheckout.c"
	web_add_header("depthAndLimit", 
		"-1,-1,0");

	web_custom_request("getTopCategories", 
		"URL=https://{p_hostname}/api/tcpproduct/getTopCategories", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t5.inf", 
		"LAST");
 	lr_end_transaction("T16_Submit_Order_Mobile_Guest", 2);
# 831 "GuestCheckout.c"


	
	return 0;
}
# 11 "e:\\performance\\scripts\\2017_r6\\03_perf\\ecommerce\\tcp_mobile\\\\combined_TCP_Mobile.c" 2

# 1 "ApplyCoupon.c" 1
ApplyCoupon()
{
	web_cleanup_auto_headers();		
	lr_think_time(TT);
	lr_start_transaction("T06_Mobile_ApplyCoupons");

	web_add_header("Accept", 
		"application/json, text/plain, */*");

	web_add_header("Accept-Encoding", 
		"gzip,deflate");

	web_add_header("Accept-Language", 
		"en-US");

	web_add_header("Origin", 
		"file://");

	web_add_header("User-Agent", 
		"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");

	web_add_header("catalogId", 
		"10551");

	web_add_header("deviceType", 
		"App");

	web_add_header("langId", 
		"-1");

	web_add_header("storeId", 
		"10151");
	lr_continue_on_error(1);
	web_custom_request("coupons", 
		"URL=https://{p_hostname}/api/payment/coupons", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"Mode=HTTP", 
		"EncType=application/json", 
		"Body={  \"URL\": \"\",  \"catalogId\": \"10551\",  \"langId\": \"-1\",  \"promoCode\": \"FORTYOFF\",  \"requesttype\": \"ajax\",  \"storeId\": \"10151\",  \"taskType\": \"A\"}", 
		"LAST");
 
	lr_continue_on_error(0);
	lr_end_transaction("T06_Mobile_ApplyCoupons", 2);
		web_cleanup_auto_headers();		
	return 0;
}
# 12 "e:\\performance\\scripts\\2017_r6\\03_perf\\ecommerce\\tcp_mobile\\\\combined_TCP_Mobile.c" 2

# 1 "ViewBag.c" 1
ViewBag()
{
	 
	if (!isLoggedin){   
		
	
	lr_think_time(TT);
	
	web_add_auto_header("Accept", 
		"application/json, text/plain, */*");

	web_add_auto_header("Accept-Encoding", 
		"gzip,deflate");

	web_add_auto_header("Accept-Language", 
		"en-US");

	web_add_auto_header("User-Agent", 
		"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_auto_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");

	web_add_auto_header("catalogId", 
		"10551");

	web_add_auto_header("deviceType", 
		"App");

	web_add_auto_header("langId", 
		"-1");

	web_add_auto_header("storeId", 
		"10151");
	lr_start_transaction("T06_MobileViewBag");
	web_reg_find("TEXT/IC=appliedCoupons","SaveCount=couponsapply","LAST");   
	lr_start_sub_transaction("T06_MobileViewBag_getAllCoupons","T06_MobileViewBag");
	web_custom_request("getAllCoupons", 
		"URL=https://{p_hostname}/api/tcporder/getAllCoupons", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"LAST");
	if (atoi(lr_eval_string("{couponsapply}"))>0){
		lr_end_sub_transaction("T06_MobileViewBag_getAllCoupons", 2);  
	}else{
		lr_end_sub_transaction("T06_MobileViewBag_getAllCoupons", 1);  
	}	
	web_add_header("locStore", 
		"true");

	web_add_header("pageName", 
		"fullOrderInfo");
	web_reg_find("TEXT/IC=shippingCharge","SaveCount=shippingCharge","LAST");
	lr_start_sub_transaction ( "T06_MobileViewBag_getOrderDetails", "T06_MobileViewBag" );
	web_reg_save_param_json( "ParamName=s_OrderId", "QueryString=$.parentOrderId", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
	web_reg_save_param("piAmount", "LB=\"piAmount\": \"", "RB=\",", "NotFound=Warning", "LAST");  
	web_custom_request("getOrderDetails", 
		"URL=https://{p_hostname}/api/getOrderDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t2.inf", 
		"LAST");
	
	if (atoi(lr_eval_string("{shippingCharge}"))>0){
		lr_end_sub_transaction("T06_MobileViewBag_getOrderDetails", 2);  
	}else{
		lr_end_sub_transaction("T06_MobileViewBag_getOrderDetails", 1);  
	}	
	
	lr_end_transaction("T06_MobileViewBag", 2);
	web_cleanup_auto_headers();
	}else{    
		
	web_add_auto_header("Accept", 
		"application/json, text/plain, */*");

	web_add_auto_header("Accept-Encoding", 
		"gzip,deflate");

	web_add_auto_header("Accept-Language", 
		"en-US");

	web_add_auto_header("User-Agent", 
		"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_auto_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");

	web_add_auto_header("catalogId", 
		"10551");

	web_add_auto_header("deviceType", 
		"App");

	web_add_auto_header("langId", 
		"-1");

	web_add_auto_header("storeId", 
		"10151");
	lr_start_transaction("T06_MobileViewBag");
	web_reg_find("TEXT/IC=appliedCoupons","SaveCount=couponsapply","LAST");   
	lr_start_sub_transaction("T06_MobileViewBag_getAllCoupons","T06_MobileViewBag");
	web_custom_request("getAllCoupons", 
		"URL=https://{p_hostname}/api/tcporder/getAllCoupons", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"LAST");
	if (atoi(lr_eval_string("{couponsapply}"))>0){
		lr_end_sub_transaction("T06_MobileViewBag_getAllCoupons", 2);  
	}else{
		lr_end_sub_transaction("T06_MobileViewBag_getAllCoupons", 1);  
	}	
	
	web_reg_save_param_json( "ParamName=s_addressID", "QueryString=$.contact..addressId", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
	web_reg_find("TEXT/IC=addressId","SaveCount=addressId","LAST");   
	lr_start_sub_transaction("T06_MobileViewBag_getAddressFromBook","T06_MobileViewBag");
	web_custom_request("getAddressFromBook", 
		"URL=https://{p_hostname}/api/payment/getAddressFromBook", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t2.inf", 
		"LAST");
	
	if (atoi(lr_eval_string("{addressId}"))>0){
		lr_end_sub_transaction("T06_MobileViewBag_getAddressFromBook", 2);  
	}else{
		lr_end_sub_transaction("T06_MobileViewBag_getAddressFromBook", 1);  
	}	
	web_add_auto_header("isRest", 
		"true");
	 
	web_reg_find("TEXT/IC=creditCardInfoJson","SaveCount=creditCardInfoJson","LAST");   
	lr_start_sub_transaction("T06_MobileViewBag_getCreditCardDetails","T06_MobileViewBag");
	web_custom_request("getCreditCardDetails", 
		"URL=https://{p_hostname}/api/payment/getCreditCardDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t3.inf", 
		"LAST");
	if (atoi(lr_eval_string("{creditCardInfoJson}"))>0){
		lr_end_sub_transaction("T06_MobileViewBag_getCreditCardDetails", 2);  
	}else{
		lr_end_sub_transaction("T06_MobileViewBag_getCreditCardDetails", 1);  
	}

	web_reg_find("TEXT/IC=creditCardInfoJson","SaveCount=creditCardInfoJson","LAST");   
	lr_start_sub_transaction("T06_MobileViewBag_getCreditCardDetails","T06_MobileViewBag");
	
	web_custom_request("getCreditCardDetails_2", 
		"URL=https://{p_hostname}/api/payment/getCreditCardDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t4.inf", 
		"LAST");
	if (atoi(lr_eval_string("{creditCardInfoJson}"))>0){
		lr_end_sub_transaction("T06_MobileViewBag_getCreditCardDetails", 2);  
	}else{
		lr_end_sub_transaction("T06_MobileViewBag_getCreditCardDetails", 1);  
	}

	(web_remove_auto_header("isRest", "ImplicitGen=Yes", "LAST"));

	web_add_header("locStore", 
		"true");

	web_add_header("pageName", 
		"fullOrderInfo");

	web_reg_find("TEXT/IC=shippingCharge","SaveCount=shippingCharge","LAST");
	lr_start_sub_transaction ( "T06_MobileViewBag_getOrderDetails", "T06_MobileViewBag" );
	web_reg_save_param_json( "ParamName=s_OrderId", "QueryString=$.parentOrderId", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
	web_reg_save_param("piAmount", "LB=\"piAmount\": \"", "RB=\",", "NotFound=Warning", "LAST");  

	web_custom_request("getOrderDetails", 
		"URL=https://{p_hostname}/api/getOrderDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t5.inf", 
		"LAST");
	if (atoi(lr_eval_string("{shippingCharge}"))>0){
		lr_end_sub_transaction("T06_MobileViewBag_getOrderDetails", 2);  
	}else{
		lr_end_sub_transaction("T06_MobileViewBag_getOrderDetails", 1);  
	}	
	
	lr_end_transaction("T06_MobileViewBag", 2);
	web_cleanup_auto_headers();
	}
	return 0;
}
# 13 "e:\\performance\\scripts\\2017_r6\\03_perf\\ecommerce\\tcp_mobile\\\\combined_TCP_Mobile.c" 2

# 1 "ViewMyAccount.c" 1
ViewMyAccount()
{
		web_add_header("Accept", 
		"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");

	web_add_auto_header("Accept-Encoding", 
		"gzip,deflate");

	web_add_auto_header("Accept-Language", 
		"en-US");

	web_add_auto_header("User-Agent", 
		"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_auto_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");

	web_custom_request("orders", 
		"URL=https://{p_hostname}/us/account/orders", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"Mode=HTTP", 
		"LAST");

	web_add_auto_header("Accept", 
		"application/json");

	web_add_auto_header("catalogId", 
		"10551");

	web_add_auto_header("deviceType", 
		"mobile");

	web_add_auto_header("langId", 
		"-1");

	web_add_auto_header("storeId", 
		"10151");

	web_custom_request("getXAppConfigValues", 
		"URL=https://{p_hostname}/api/tcporder/getXAppConfigValues", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=https://{p_hostname}/us/account/orders", 
		"Snapshot=t2.inf", 
		"LAST");

	web_custom_request("getRegisteredUserDetailsInfo", 
		"URL=https://{p_hostname}/api/payment/getRegisteredUserDetailsInfo?_=1505951049865", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=https://{p_hostname}/us/account/orders", 
		"Snapshot=t3.inf", 
		"LAST");
 
# 74 "ViewMyAccount.c"
	web_custom_request("getAllCoupons", 
		"URL=https://{p_hostname}/api/tcporder/getAllCoupons", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=https://{p_hostname}/us/account/orders", 
		"Snapshot=t5.inf", 
		"LAST");

	web_add_header("calc", 
		"false");

	web_add_header("locStore", "True");
	web_add_header("pageName", "fullOrderInfo");

	web_custom_request("getOrderDetails", 
		"URL=https://{p_hostname}/api/getOrderDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=https://{p_hostname}/us/account/orders", 
		"Snapshot=t6.inf", 
		"LAST");

	web_add_header("action", 
		"get");

	web_custom_request("getFavouriteStoreLocation", 
		"URL=https://{p_hostname}/api/tcporder/getFavouriteStoreLocation", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=https://{p_hostname}/us/account/orders", 
		"Snapshot=t7.inf", 
		"LAST");
 
# 122 "ViewMyAccount.c"
	web_add_header("fromPage", 
		"checkout");

	web_custom_request("getAddressFromBook", 
		"URL=https://{p_hostname}/api/payment/getAddressFromBook", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=https://{p_hostname}/us/account/orders", 
		"Snapshot=t9.inf", 
		"LAST");

	web_add_header("isRest", 
		"true");

	web_custom_request("getCreditCardDetails", 
		"URL=https://{p_hostname}/api/payment/getCreditCardDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=https://{p_hostname}/us/account/orders", 
		"Snapshot=t10.inf", 
		"LAST");

	web_add_header("fromRest", 
		"true");

	web_custom_request("getPointsAndOrderHistory", 
		"URL=https://{p_hostname}/api/tcporder/getPointsAndOrderHistory", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=https://{p_hostname}/us/account/orders", 
		"Snapshot=t11.inf", 
		"LAST");
	web_cleanup_auto_headers();	
	return 0;
}
# 14 "e:\\performance\\scripts\\2017_r6\\03_perf\\ecommerce\\tcp_mobile\\\\combined_TCP_Mobile.c" 2

# 1 "AddToCartBOPIS.c" 1
AddToCartBOPIS()
{
	return 0;
}
# 15 "e:\\performance\\scripts\\2017_r6\\03_perf\\ecommerce\\tcp_mobile\\\\combined_TCP_Mobile.c" 2

# 1 "LoggedInCheckout.c" 1
LoggedInCheckout()
{

	lr_start_transaction("T07_Mobile_LoginFirstCheckout");

	web_add_auto_header("Accept", 
		"application/json, text/plain, */*");

	web_add_auto_header("Accept-Encoding", 
		"gzip,deflate");

	web_add_auto_header("Accept-Language", 
		"en-US");

	web_add_auto_header("User-Agent", 
		"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_auto_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");

	web_add_auto_header("catalogId", 
		"10551");

	web_add_auto_header("deviceType", 
		"App");

	web_add_header("isRest", 
		"true");

	web_add_auto_header("langId", 
		"-1");

	web_add_auto_header("storeId", 
		"10151");

	web_custom_request("getCreditCardDetails", 
		"URL=https://{p_hostname}/api/payment/getCreditCardDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"LAST");

	web_custom_request("getAddressFromBook", 
		"URL=https://{p_hostname}/api/payment/getAddressFromBook", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t2.inf", 
		"LAST");

	web_add_header("calc", 
		"false");

	web_add_header("locStore", 
		"true");

	web_add_header("pageName", 
		"fullOrderInfo");

	web_custom_request("getOrderDetails", 
		"URL=https://{p_hostname}/api/getOrderDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t3.inf", 
		"LAST");

	web_add_header("isRest", 
		"true");

	web_custom_request("getCreditCardDetails_2", 
		"URL=https://{p_hostname}/api/payment/getCreditCardDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t4.inf", 	
		"LAST");

	web_custom_request("getAllCoupons", 
		"URL=https://{p_hostname}/api/tcporder/getAllCoupons", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t5.inf", 
		"LAST");

	web_custom_request("getShipmentMethods", 
		"URL=https://{p_hostname}/api/payment/getShipmentMethods", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t6.inf", 
		"LAST");

	web_custom_request("giftOptionsCmd", 
		"URL=https://{p_hostname}/api/payment/giftOptionsCmd", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t7.inf", 
		"LAST");

	web_add_auto_header("zipCode", 		"07094");

	web_custom_request("getShipmentMethods_2", 
		"URL=https://{p_hostname}/api/payment/getShipmentMethods", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t8.inf", 
		"LAST");

	web_custom_request("getShipmentMethods_3", 
		"URL=https://{p_hostname}/api/payment/getShipmentMethods", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t9.inf", 
		"LAST");
	
	lr_end_transaction("T07_Mobile_LoginFirstCheckout", 2);
	if (p_MobileRand<=(LoginDropRatio))
		lr_exit(5, 2);
	web_cleanup_auto_headers();	
	
	lr_start_transaction("T07_Mobile_LoginFirstShipping");
	
	web_add_auto_header("Accept-Language", 
		"en-US");

	web_add_auto_header("User-Agent", 
		"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_auto_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");


	web_add_auto_header("Accept", 
		"application/json, text/plain, */*");

	web_add_auto_header("addressField1", 
		"10 main st");

	web_add_auto_header("catalogId", 
		"10551");

	web_add_auto_header("deviceType", 
		"App");

	web_add_auto_header("langId", 
		"-1");

	web_add_auto_header("storeId", 
		"10151");

	web_add_auto_header("zipCode", 
		"07094");

	web_custom_request("getShipmentMethods", 
		"URL=https://{p_hostname}/api/payment/getShipmentMethods", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t4.inf", 
		"LAST");

	web_add_auto_header("state", 
		"DE");

	web_custom_request("getShipmentMethods_2", 
		"URL=https://{p_hostname}/api/payment/getShipmentMethods", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t5.inf", 
		"LAST");

	(web_remove_auto_header("zipCode", "ImplicitGen=Yes", "LAST"));

	web_custom_request("getShipmentMethods_3", 
		"URL=https://{p_hostname}/api/payment/getShipmentMethods", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t6.inf", 
		"LAST");

	web_add_header("zipCode", 
		"19801");

	web_custom_request("getShipmentMethods_4", 
		"URL=https://{p_hostname}/api/payment/getShipmentMethods", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t7.inf", 
		"LAST");

	(web_remove_auto_header("addressField1", "ImplicitGen=Yes", "LAST"));

	(web_remove_auto_header("state", "ImplicitGen=Yes", "LAST"));

	web_add_auto_header("Origin", 
		"file://");
	web_reg_save_param("addressId", "LB=\"addressId\": \"", "RB=\",\n", "NotFound=Warning", "LAST");
	web_reg_save_param ( "addressIdAll" , "LB=\"addressId\": \"" , "RB=\",\n" , "ORD=ALL", "NotFound=Warning", "LAST" ) ;  
	web_custom_request("addAddress", 
		"URL=https://{p_hostname}/api/payment/addAddress", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t8.inf", 
		"Mode=HTTP", 
		"EncType=application/json", 
		"Body={\n  \"contact\": [\n    {\n      \"addressLine\": [\n        \"10 W Main St\",\n        \"\",\n        \"\"\n      ],\n      \"addressType\": \"Shipping\",\n      \"attributes\": [\n        {\n          \"key\": \"addressField3\",\n          \"value\": \"19702\"\n        }\n      ],\n      \"city\": \"Christiana\",\n      \"country\": \"US\",\n      \"firstName\": \"john\",\n      \"lastName\": \"oliver\",\n      \"email1\": \"{p_userID}\",\n      \"phone1\": \"2122121212"
		"\",\n      \"state\": \"DE\",\n      \"zipCode\": \"19702\",\n      \"xcont_addressField3\": \"19702\",\n      \"nickName\": \"1506054056317\",\n      \"fromPage\": \"\",\n      \"phone1Publish\": \"true\",\n      \"primary\": \"false\"\n    }\n  ]\n}", 
		"LAST");

	web_add_header("content-type", 
		"application/json");

	web_custom_request("updateShippingMethodSelection", 
		"URL=https://{p_hostname}/api/payment/updateShippingMethodSelection", 
		"Method=PUT", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t9.inf", 
		"Mode=HTTP", 
		"Body={\n  \"shipModeId\": \"\",\n  \"addressId\": \"{addressId}\",\n  \"x_calculationUsage\": \"-1,-2,-3,-4,-5,-6,-7\",\n  \"requesttype\": \"ajax\",\n  \"orderId\": \"{s_OrderId_1}\"\n}", 
		"LAST");

	(web_remove_auto_header("Origin", "ImplicitGen=Yes", "LAST"));

	web_add_header("calc", 
		"false");

	web_add_header("locStore", 
		"true");

	web_add_header("pageName", 
		"fullOrderInfo");

	web_custom_request("getOrderDetails", 
		"URL=https://{p_hostname}/api/getOrderDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t10.inf", 
		"LAST");

	web_custom_request("getAllCoupons", 
		"URL=https://{p_hostname}/api/tcporder/getAllCoupons", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t11.inf", 
		"LAST");
	lr_end_transaction("T07_Mobile_LoginFirstShipping", 2);
	web_cleanup_auto_headers();	
	if (p_MobileRand<=(LoginDropRatio+ShipPageRatio))
		lr_exit(5, 2);
	lr_start_transaction("T07_Mobile_LoginFirstBilling");
	web_add_auto_header("Accept", 
		"application/json, text/plain, */*");

	web_add_auto_header("Accept-Encoding", 
		"gzip,deflate");

	web_add_auto_header("Accept-Language", 
		"en-US");

	web_add_auto_header("Origin", 
		"file://");

	web_add_auto_header("User-Agent", 
			"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_auto_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");

	web_add_auto_header("catalogId", 
		"10551");

	web_add_header("content-type", 
		"application/json");

	web_add_auto_header("deviceType", 
		"App");

	web_add_auto_header("langId", 
		"-1");

	web_add_auto_header("storeId", 
		"10151");

	web_custom_request("updateAddress", 
		"URL=https://{p_hostname}/api/payment/updateAddress", 
		"Method=PUT", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"Mode=HTTP", 
		"Body={\n  \"fromPage\": \"checkout\",\n  \"addressId\": \"{addressId}\"\n}", 
		"LAST");

	web_custom_request("addPaymentInstruction", 
		"URL=https://{p_hostname}/api/payment/addPaymentInstruction", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t2.inf", 
		"Mode=HTTP", 
		"EncType=application/json", 
		"Body={\n  \"paymentInstruction\": [\n    {\n      \"account\": \"2818526303813893\",\n      \"isDefault\": \"false\",\n      \"billing_address_id\": \"{addressId}\",\n      \"payMethodId\": \"VISA\",\n      \"cc_brand\": \"VISA\",\n      \"expire_month\": \"3\",\n      \"expire_year\": \"2020\",\n      \"cc_cvc\": \"123\",\n      \"piAmount\": \"{piAmount}\"\n    }\n  ]\n}", 
		"LAST");
 
 
	(web_remove_auto_header("Origin", "ImplicitGen=Yes", "LAST"));

	web_add_header("calc", 
		"false");

	web_add_header("locStore", 
		"true");

	web_add_header("pageName", 
		"fullOrderInfo");

	web_custom_request("getOrderDetails", 
		"URL=https://{p_hostname}/api/getOrderDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t3.inf", 
		"LAST");

	web_custom_request("getAllCoupons", 
		"URL=https://{p_hostname}/api/tcporder/getAllCoupons", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t4.inf", 
		"LAST");

	lr_end_transaction("T07_Mobile_LoginFirstBilling", 2);
	
	web_cleanup_auto_headers();	
	if (p_MobileRand<=(LoginDropRatio+ShipPageRatio+BillPageRatio))
		lr_exit(5, 2);
	
	lr_start_transaction("T16_Submit_Order_Mobile_Registered");
	web_add_auto_header("Accept", 
		"application/json, text/plain, */*");

	web_add_auto_header("Accept-Encoding", 
		"gzip,deflate");

	web_add_auto_header("Accept-Language", 
		"en-US");

	web_add_header("Origin", 
		"file://");

	web_add_auto_header("User-Agent", 
		"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_auto_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");

	web_add_auto_header("catalogId", 
		"10551");

	web_add_auto_header("deviceType", 
		"App");

	web_add_auto_header("langId", 
		"-1");

	web_add_auto_header("storeId", 
		"10151");

	web_custom_request("addCheckout", 
		"URL=https://{p_hostname}/api/addCheckout", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"Mode=HTTP", 
		"EncType=application/json", 
 
		"Body={\n  \"orderId\": \"{s_OrderId_1}\",\n  \"isRest\": \"true\",\n  \"locStore\": \"True\"\n}", 
		 
		"LAST");
 
 
# 499 "LoggedInCheckout.c"
	lr_end_transaction("T16_Submit_Order_Mobile_Registered", 2);


	return 0;
}

Billing2(){
			web_add_auto_header("Accept", 
		"application/json, text/plain, */*");

	web_add_auto_header("Accept-Encoding", 
		"gzip,deflate");

	web_add_auto_header("Accept-Language", 
		"en-US");

	web_add_header("Origin", 
		"file://");

	web_add_auto_header("User-Agent", 
		"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_auto_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");

	web_add_auto_header("catalogId", 
		"10551");

	web_add_auto_header("deviceType", 
		"App");

	web_add_auto_header("isRest", 
		"true");

	web_add_auto_header("langId", 
		"-1");

	web_add_auto_header("storeId", 
		"10151");

	web_add_header("isRest", 
		"true");

	web_custom_request("getCreditCardDetails", 
		"URL=https://{p_hostname}/api/payment/getCreditCardDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t2.inf", 
		"LAST");

	web_custom_request("modifyCreditCardDetails", 
		"URL=https://{p_hostname}/api/payment/modifyCreditCardDetails", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"Mode=HTTP", 
		"EncType=application/json", 
		"Body={\n  \"creditCardId\": \"\",\n  \"pay_account\": \"4111111111111111\",\n  \"addressId\": 1587775,\n  \"billing_firstName\": \"test\",\n  \"billing_lastName\": \"me\",\n  \"billing_city\": \"Christiana\",\n  \"billing_state\": \"DE\",\n  \"billing_address1\": \"10 W Main St  \",\n  \"billing_address2\": \"\",\n  \"billing_addressField3\": \"19702\",\n  \"billing_zipCode\": \"19702\",\n  \"billing_country\": \"US\",\n  \"billing_phone1\": \"3022121212\",\n  \"pay_expire_year\": \"2020\","
		"\n  \"pay_expire_month\": \"6\",\n  \"billing_nickName\": \"1506052135432\",\n  \"isDefault\": \"true\",\n  \"payMethodId\": \"COMPASSVISA\",\n  \"cc_brand\": \"VISA\"\n}", 
		"LAST");

	web_custom_request("getCreditCardDetails", 
		"URL=https://{p_hostname}/api/payment/getCreditCardDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t2.inf", 
		"LAST");

	(web_remove_auto_header("isRest", "ImplicitGen=Yes", "LAST"));

	web_add_header("Origin", 
		"file://");

	web_custom_request("addPaymentInstruction", 
		"URL=https://{p_hostname}/api/payment/addPaymentInstruction", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t3.inf", 
		"Mode=HTTP", 
		"EncType=application/json", 
		 
		"Body={  \"paymentInstruction\": [    {      \"account\": \"2818526303813893\",      \"isDefault\": \"false\",\n      \"billing_address_id\": \"{addressId}\",\n      \"payMethodId\": \"VISA\",\n      \"cc_brand\": \"VISA\",\n      \"expire_month\": \"11\",\n      \"expire_year\": \"2020\",\n      \"cc_cvc\": \"123\",\n      \"piAmount\": \"{piAmount}\"\n    }\n  ]\n}", 
		"LAST");

	web_add_header("calc", 
		"false");

	web_add_header("locStore", 
		"true");

	web_add_header("pageName", 
		"fullOrderInfo");

	web_custom_request("getOrderDetails", 
		"URL=https://{p_hostname}/api/getOrderDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t4.inf", 
		"LAST");

	web_add_header("Origin", 
		"file://");

	web_custom_request("addPaymentInstruction_2", 
		"URL=https://{p_hostname}/api/payment/addPaymentInstruction", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t5.inf", 
		"Mode=HTTP", 
		"EncType=application/json", 
		"Body={\n  \"paymentInstruction\": [\n    {\n      \"account\": \"************3893\",\n      \"isDefault\": \"true\",\n      \"billing_address_id\": \"1587775\",\n      \"payMethodId\": \"COMPASSVISA\",\n      \"cc_brand\": \"Visa\",\n      \"expire_month\": \"6 \",\n      \"expire_year\": \"2020\",\n      \"piAmount\": \"{piAmount}\",\n      \"creditCardId\": \"468529\"\n    }\n  ]\n}", 
		"LAST");

	web_add_header("calc", 
		"false");

	web_add_header("locStore", 
		"true");

	web_add_header("pageName", 
		"fullOrderInfo");

	web_custom_request("getOrderDetails_2", 
		"URL=https://{p_hostname}/api/getOrderDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t6.inf", 
		"LAST");

	web_custom_request("giftOptionsCmd", 
		"URL=https://{p_hostname}/api/payment/giftOptionsCmd", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t7.inf", 
		"LAST");

	return 0;
}
# 16 "e:\\performance\\scripts\\2017_r6\\03_perf\\ecommerce\\tcp_mobile\\\\combined_TCP_Mobile.c" 2

# 1 "SearchProductByTerm.c" 1
SearchProductByTerm()
{
	int ij=0,jk=0, retry=0,found=0, typeStr;
	char s_SearchStr[50];
	memset(s_SearchStr, 0, sizeof(s_SearchStr));
 
	
	while(found==0){
		if (retry==10)
			break; 
		lr_advance_param("p_searchTerm");
		lr_save_string(lr_eval_string("{p_searchTerm}"),"mySearchStr");
		ij = strlen(lr_eval_string("{mySearchStr}"));
		if (ij==0){  
			 
			++retry;
		}else{
			found=1;
			break;
		}
		
	}
	
	if (found==0){
		lr_exit(2,2);
		return 0;
	}
	lr_save_string(lr_eval_string("{mySearchStr}"),"s_SearchStr"); 
	 
	for (typeStr=3;typeStr<=ij;typeStr++){
		memset(s_SearchStr, 0, sizeof(s_SearchStr));
		strncpy(s_SearchStr,lr_eval_string("{mySearchStr}"),typeStr);
		lr_save_string(s_SearchStr,"s_SearchStr");
	
	web_add_header("term","{s_SearchStr}");

	web_add_header("searchTerm", "{s_SearchStr}");

	web_add_header("Accept", 
		"application/json, text/plain, */*");

	web_add_header("Accept-Encoding", 
		"gzip,deflate");

	web_add_header("Accept-Language", 
		"en-US");

	web_add_header("User-Agent", 
		"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");

	web_add_header("catalogId", 
		"10551");

	web_add_header("coreName", 
		"MC_10001_CatalogEntry_en_US");

	web_add_header("coreNameCategory", 
		"MC_10001_CatalogGroup_en_US");

	web_add_header("count", "4");

	web_add_header("deviceType", 
		"App");

	web_add_header("isRest", 
		"true");

	web_add_header("langId", 
		"-1");

	web_add_header("pageNumber", 
		"4");

	web_add_header("published", 
		"1");

	web_add_header("rows", 
		"4");

	web_add_header("start", 
		"0");

	web_add_header("storeId", 
		"10151");

	web_add_header("timeAllowed", 
		"15000");

	web_custom_request("getAutoSuggestions", 
		"URL=https://{p_hostname}/api/getAutoSuggestions", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"LAST");
	
	}

	web_convert_param("mySearchStr", "SourceEncoding=PLAIN","TargetEncoding=URL", "LAST" );
	web_add_header("langId", "-1");
	web_add_header("pageSize","30");
	web_add_header("catalogId", "10551");
	web_add_header("pageNumber","1");
	web_add_header("categoryId", "");
	web_add_header("User-Agent", "Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");
	web_add_header("orderBy", "null");
	web_add_header("facet", "");
	web_add_header("Accept", 	"application/json, text/plain, */*");
	web_add_header("searchTerm", "{mySearchStr}");
	web_add_header("storeId", "10151");
	web_add_header("deviceType", "App");
	web_add_header("Accept-Encoding", 
		"gzip,deflate");
	web_add_header("Accept-Language", 
		"en-US");
	web_add_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");
	web_reg_save_param_json( "ParamName=s_products", "QueryString=$.catalogEntryView[?(@.partNumber)].uniqueID", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
	
	lr_start_transaction("T02b_Mobile_SearchByTerm");

	web_custom_request("getProductsBySearchTerm", 
		"URL=https://{p_hostname}/api/tcpproduct/getProductsBySearchTerm", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t5.inf", 
		"LAST");

	lr_end_transaction("T02b_Mobile_SearchByTerm", 2);

	web_add_header("term","{s_SearchStr}");

	web_add_header("searchTerm", "{s_SearchStr}");

	web_add_header("Accept", 
		"application/json, text/plain, */*");

	web_add_header("Accept-Encoding", 
		"gzip,deflate");

	web_add_header("Accept-Language", 
		"en-US");

	web_add_header("User-Agent", 
		"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");

	web_add_header("catalogId", 
		"10551");

	web_add_header("coreName", 
		"MC_10001_CatalogEntry_en_US");

	web_add_header("coreNameCategory", 
		"MC_10001_CatalogGroup_en_US");

	web_add_header("count", "4");

	web_add_header("deviceType", 
		"App");

	web_add_header("isRest", 
		"true");

	web_add_header("langId", 
		"-1");

	web_add_header("pageNumber", 
		"4");

	web_add_header("published", 
		"1");

	web_add_header("rows", 
		"4");

	web_add_header("start", 
		"0");

	web_add_header("storeId", 
		"10151");

	web_add_header("timeAllowed", 
		"15000");

	web_custom_request("getAutoSuggestions_5", 
		"URL=https://{p_hostname}/api/getAutoSuggestions", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t6.inf", 
		"LAST");

	web_cleanup_auto_headers();
	 
	return 0;
}
# 17 "e:\\performance\\scripts\\2017_r6\\03_perf\\ecommerce\\tcp_mobile\\\\combined_TCP_Mobile.c" 2

# 1 "BrowseFirstCheckout.c" 1
BrowseFirstCheckout()
{
	web_cleanup_auto_headers();
	lr_think_time(TT);
	lr_start_transaction("T07_Mobile_LoginFirstCheckout");

	web_add_auto_header("Accept", 
		"application/json, text/plain, */*");

	web_add_auto_header("Accept-Encoding", 
		"gzip,deflate");

	web_add_auto_header("Accept-Language", 
		"en-US");

	web_add_auto_header("User-Agent", 
		"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_auto_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");

	web_add_auto_header("catalogId", 
		"10551");

	web_add_auto_header("deviceType", 
		"App");

	web_add_header("isRest", 
		"true");

	web_add_auto_header("langId", 
		"-1");

	web_add_auto_header("storeId", 
		"10151");

	web_custom_request("getCreditCardDetails", 
		"URL=https://{p_hostname}/api/payment/getCreditCardDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"LAST");

	web_custom_request("getAddressFromBook", 
		"URL=https://{p_hostname}/api/payment/getAddressFromBook", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t2.inf", 
		"LAST");

	web_add_header("calc", 
		"false");

	web_add_header("locStore", 
		"true");

	web_add_header("pageName", 
		"fullOrderInfo");

	web_custom_request("getOrderDetails", 
		"URL=https://{p_hostname}/api/getOrderDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t3.inf", 
		"LAST");

	web_add_header("isRest", 
		"true");

	web_custom_request("getCreditCardDetails_2", 
		"URL=https://{p_hostname}/api/payment/getCreditCardDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t4.inf", 	
		"LAST");

	web_custom_request("getAllCoupons", 
		"URL=https://{p_hostname}/api/tcporder/getAllCoupons", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t5.inf", 
		"LAST");

	web_custom_request("getShipmentMethods", 
		"URL=https://{p_hostname}/api/payment/getShipmentMethods", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t6.inf", 
		"LAST");

	web_custom_request("giftOptionsCmd", 
		"URL=https://{p_hostname}/api/payment/giftOptionsCmd", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t7.inf", 
		"LAST");

	web_add_auto_header("zipCode", 		"07094");

	web_custom_request("getShipmentMethods_2", 
		"URL=https://{p_hostname}/api/payment/getShipmentMethods", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t8.inf", 
		"LAST");

	web_custom_request("getShipmentMethods_3", 
		"URL=https://{p_hostname}/api/payment/getShipmentMethods", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t9.inf", 
		"LAST");
	
	lr_end_transaction("T07_Mobile_LoginFirstCheckout", 2);
	if (p_MobileRand<=(LoginDropRatio))
		lr_exit(5, 2);
	web_cleanup_auto_headers();	
	
	lr_start_transaction("T07_Mobile_LoginFirstShipping");
	
	web_add_auto_header("Accept-Language", 
		"en-US");

	web_add_auto_header("User-Agent", 
		"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_auto_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");


	web_add_auto_header("Accept", 
		"application/json, text/plain, */*");

	web_add_auto_header("addressField1", 
		"10 main st");

	web_add_auto_header("catalogId", 
		"10551");

	web_add_auto_header("deviceType", 
		"App");

	web_add_auto_header("langId", 
		"-1");

	web_add_auto_header("storeId", 
		"10151");

	web_add_auto_header("zipCode", 
		"07094");

	web_custom_request("getShipmentMethods", 
		"URL=https://{p_hostname}/api/payment/getShipmentMethods", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t4.inf", 
		"LAST");

	web_add_auto_header("state", 
		"DE");

	web_custom_request("getShipmentMethods_2", 
		"URL=https://{p_hostname}/api/payment/getShipmentMethods", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t5.inf", 
		"LAST");

	(web_remove_auto_header("zipCode", "ImplicitGen=Yes", "LAST"));

	web_custom_request("getShipmentMethods_3", 
		"URL=https://{p_hostname}/api/payment/getShipmentMethods", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t6.inf", 
		"LAST");

	web_add_header("zipCode", 
		"19801");

	web_custom_request("getShipmentMethods_4", 
		"URL=https://{p_hostname}/api/payment/getShipmentMethods", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t7.inf", 
		"LAST");

	(web_remove_auto_header("addressField1", "ImplicitGen=Yes", "LAST"));

	(web_remove_auto_header("state", "ImplicitGen=Yes", "LAST"));

	web_add_auto_header("Origin", 
		"file://");
	web_reg_save_param("addressId", "LB=\"addressId\": \"", "RB=\",\n", "NotFound=Warning", "LAST");
	web_reg_save_param ( "addressIdAll" , "LB=\"addressId\": \"" , "RB=\",\n" , "ORD=ALL", "NotFound=Warning", "LAST" ) ;  
	web_custom_request("addAddress", 
		"URL=https://{p_hostname}/api/payment/addAddress", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t8.inf", 
		"Mode=HTTP", 
		"EncType=application/json", 
		"Body={\n  \"contact\": [\n    {\n      \"addressLine\": [\n        \"10 W Main St\",\n        \"\",\n        \"\"\n      ],\n      \"addressType\": \"Shipping\",\n      \"attributes\": [\n        {\n          \"key\": \"addressField3\",\n          \"value\": \"19702\"\n        }\n      ],\n      \"city\": \"Christiana\",\n      \"country\": \"US\",\n      \"firstName\": \"john\",\n      \"lastName\": \"oliver\",\n      \"email1\": \"{p_userID}\",\n      \"phone1\": \"2122121212"
		"\",\n      \"state\": \"DE\",\n      \"zipCode\": \"19702\",\n      \"xcont_addressField3\": \"19702\",\n      \"nickName\": \"1506054056317\",\n      \"fromPage\": \"\",\n      \"phone1Publish\": \"true\",\n      \"primary\": \"false\"\n    }\n  ]\n}", 
		"LAST");

	web_add_header("content-type", 
		"application/json");

	web_custom_request("updateShippingMethodSelection", 
		"URL=https://{p_hostname}/api/payment/updateShippingMethodSelection", 
		"Method=PUT", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t9.inf", 
		"Mode=HTTP", 
		"Body={\n  \"shipModeId\": \"901101\",\n  \"addressId\": \"{addressId}\",\n  \"x_calculationUsage\": \"-1,-2,-3,-4,-5,-6,-7\",\n  \"requesttype\": \"ajax\",\n  \"orderId\": \"{s_OrderId_1}\"\n}", 
		"LAST");

	(web_remove_auto_header("Origin", "ImplicitGen=Yes", "LAST"));

	web_add_header("calc", 
		"false");

	web_add_header("locStore", 
		"true");

	web_add_header("pageName", 
		"fullOrderInfo");

	web_custom_request("getOrderDetails", 
		"URL=https://{p_hostname}/api/getOrderDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t10.inf", 
		"LAST");

	web_custom_request("getAllCoupons", 
		"URL=https://{p_hostname}/api/tcporder/getAllCoupons", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t11.inf", 
		"LAST");
	lr_end_transaction("T07_Mobile_LoginFirstShipping", 2);
	web_cleanup_auto_headers();	
	if (p_MobileRand<=(LoginDropRatio+ShipPageRatio))
		lr_exit(5, 2);
	lr_start_transaction("T07_Mobile_LoginFirstBilling");
	web_add_auto_header("Accept", 
		"application/json, text/plain, */*");

	web_add_auto_header("Accept-Encoding", 
		"gzip,deflate");

	web_add_auto_header("Accept-Language", 
		"en-US");

	web_add_auto_header("Origin", 
		"file://");

	web_add_auto_header("User-Agent", 
			"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_auto_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");

	web_add_auto_header("catalogId", 
		"10551");

	web_add_header("content-type", 
		"application/json");

	web_add_auto_header("deviceType", 
		"App");

	web_add_auto_header("langId", 
		"-1");

	web_add_auto_header("storeId", 
		"10151");

	web_custom_request("updateAddress", 
		"URL=https://{p_hostname}/api/payment/updateAddress", 
		"Method=PUT", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"Mode=HTTP", 
		"Body={\n  \"fromPage\": \"checkout\",\n  \"addressId\": \"{addressId}\"\n}", 
		"LAST");

	web_custom_request("addPaymentInstruction", 
		"URL=https://{p_hostname}/api/payment/addPaymentInstruction", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t2.inf", 
		"Mode=HTTP", 
		"EncType=application/json", 
		"Body={\n  \"paymentInstruction\": [\n    {\n      \"account\": \"4111111111111111\",\n      \"isDefault\": \"false\",\n      \"billing_address_id\": \"{addressId}\",\n      \"payMethodId\": \"COMPASSVISA\",\n      \"cc_brand\": \"VISA\",\n      \"expire_month\": \"3\",\n      \"expire_year\": \"2020\",\n      \"cc_cvc\": \"123\",\n      \"piAmount\": \"{piAmount}\"\n    }\n  ]\n}", 
		"LAST");

	(web_remove_auto_header("Origin", "ImplicitGen=Yes", "LAST"));

	web_add_header("calc", 
		"false");

	web_add_header("locStore", 
		"true");

	web_add_header("pageName", 
		"fullOrderInfo");

	web_custom_request("getOrderDetails", 
		"URL=https://{p_hostname}/api/getOrderDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t3.inf", 
		"LAST");

	web_custom_request("getAllCoupons", 
		"URL=https://{p_hostname}/api/tcporder/getAllCoupons", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t4.inf", 
		"LAST");

	lr_end_transaction("T07_Mobile_LoginFirstBilling", 2);
	web_cleanup_auto_headers();	
	if (p_MobileRand<=(LoginDropRatio+ShipPageRatio+BillPageRatio))
		lr_exit(5, 2);
	lr_start_transaction("T16_Submit_Order_Mobile_Registered");
	web_add_auto_header("Accept", 
		"application/json, text/plain, */*");

	web_add_auto_header("Accept-Encoding", 
		"gzip,deflate");

	web_add_auto_header("Accept-Language", 
		"en-US");

	web_add_header("Origin", 
		"file://");

	web_add_auto_header("User-Agent", 
		"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_auto_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");

	web_add_auto_header("catalogId", 
		"10551");

	web_add_auto_header("deviceType", 
		"App");

	web_add_auto_header("langId", 
		"-1");

	web_add_auto_header("storeId", 
		"10151");

	web_custom_request("addCheckout", 
		"URL=https://{p_hostname}/api/addCheckout", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"Mode=HTTP", 
		"EncType=application/json", 
		 
		"Body={\n  \"orderId\": \"{s_OrderId_1}\",\n  \"isRest\": \"true\",\n  \"locStore\": \"True\"\n}", 
		"LAST");
 
	web_custom_request("getRegisteredUserDetailsInfo", 
		"URL=https://{p_hostname}/api/payment/getRegisteredUserDetailsInfo", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t2.inf", 
		"LAST");

	web_add_header("fromRest", 
		"true");

	web_custom_request("getPointsAndOrderHistory", 
		"URL=https://{p_hostname}/api/tcporder/getPointsAndOrderHistory", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t3.inf", 
		"LAST");

	web_custom_request("getListofDefaultWishlist",
		"URL=https://{p_hostname}/api/tcpstore/getListofDefaultWishlist",
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTTP",
		"Body={[\"productId\":[\"279132\",\"279138\",\"604392\",\"604393\",\"604394\",\"604395\",\"604396\",\"604776\",\"604777\",\"604778\",\"604787\",\"604788\",\"604789\",\"604790\",\"604791\",\"604792\",\"604793\",\"604794\"]}",
		"LAST");
 
# 470 "BrowseFirstCheckout.c"
	web_add_header("depthAndLimit", 
		"-1,-1,0");

	web_custom_request("getTopCategories", 
		"URL=https://{p_hostname}/api/tcpproduct/getTopCategories", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t6.inf", 
		"LAST");

	web_add_header("action", 
		"get");

	web_custom_request("getFavouriteStoreLocation", 
		"URL=https://{p_hostname}/api/tcporder/getFavouriteStoreLocation/", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t7.inf", 
		"LAST");
 
# 506 "BrowseFirstCheckout.c"
 
	lr_end_transaction("T16_Submit_Order_Mobile_Registered", 2);


	return 0;
}
# 18 "e:\\performance\\scripts\\2017_r6\\03_perf\\ecommerce\\tcp_mobile\\\\combined_TCP_Mobile.c" 2

# 1 "vuser_end.c" 1
vuser_end()
{
	return 0;
}
# 19 "e:\\performance\\scripts\\2017_r6\\03_perf\\ecommerce\\tcp_mobile\\\\combined_TCP_Mobile.c" 2

