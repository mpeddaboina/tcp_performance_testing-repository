ApplyCoupon()
{
	web_cleanup_auto_headers();		
	lr_think_time(TT);
	lr_start_transaction("T06_Mobile_ApplyCoupons");

	web_add_header("Accept", 
		"application/json, text/plain, */*");

	web_add_header("Accept-Encoding", 
		"gzip,deflate");

	web_add_header("Accept-Language", 
		"en-US");

	web_add_header("Origin", 
		"file://");

	web_add_header("User-Agent", 
		"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");

	web_add_header("catalogId", 
		"10551");

	web_add_header("deviceType", 
		"App");

	web_add_header("langId", 
		"-1");

	web_add_header("storeId", 
		"10151");
	lr_continue_on_error(1);
	web_custom_request("coupons", 
		"URL=https://{p_hostname}/api/payment/coupons", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"Mode=HTTP", 
		"EncType=application/json", 
		"Body={  \"URL\": \"\",  \"catalogId\": \"10551\",  \"langId\": \"-1\",  \"promoCode\": \"FORTYOFF\",  \"requesttype\": \"ajax\",  \"storeId\": \"10151\",  \"taskType\": \"A\"}", 
		LAST);
//		"Body={\n  \"URL\": \"\",\n  \"catalogId\": \"10551\",\n  \"langId\": \"-1\",\n  \"promoCode\": \"FORTYOFF\",\n  \"requesttype\": \"ajax\",\n  \"storeId\": \"10151\",\n  \"taskType\": \"A\"\n}", 
	lr_continue_on_error(0);
	lr_end_transaction("T06_Mobile_ApplyCoupons", LR_AUTO);
		web_cleanup_auto_headers();		
	return 0;
}
