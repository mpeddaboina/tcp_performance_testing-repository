LoggedInCheckout()
{

	lr_start_transaction("T07_Mobile_LoginFirstCheckout");

	web_add_auto_header("Accept", 
		"application/json, text/plain, */*");

	web_add_auto_header("Accept-Encoding", 
		"gzip,deflate");

	web_add_auto_header("Accept-Language", 
		"en-US");

	web_add_auto_header("User-Agent", 
		"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_auto_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");

	web_add_auto_header("catalogId", 
		"10551");

	web_add_auto_header("deviceType", 
		"App");

	web_add_header("isRest", 
		"true");

	web_add_auto_header("langId", 
		"-1");

	web_add_auto_header("storeId", 
		"10151");

	web_custom_request("getCreditCardDetails", 
		"URL=https://{p_hostname}/api/payment/getCreditCardDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		LAST);

	web_custom_request("getAddressFromBook", 
		"URL=https://{p_hostname}/api/payment/getAddressFromBook", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t2.inf", 
		LAST);

	web_add_header("calc", 
		"false");

	web_add_header("locStore", 
		"true");

	web_add_header("pageName", 
		"fullOrderInfo");

	web_custom_request("getOrderDetails", 
		"URL=https://{p_hostname}/api/getOrderDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t3.inf", 
		LAST);

	web_add_header("isRest", 
		"true");

	web_custom_request("getCreditCardDetails_2", 
		"URL=https://{p_hostname}/api/payment/getCreditCardDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t4.inf", 	
		LAST);

	web_custom_request("getAllCoupons", 
		"URL=https://{p_hostname}/api/tcporder/getAllCoupons", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t5.inf", 
		LAST);

	web_custom_request("getShipmentMethods", 
		"URL=https://{p_hostname}/api/payment/getShipmentMethods", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t6.inf", 
		LAST);

	web_custom_request("giftOptionsCmd", 
		"URL=https://{p_hostname}/api/payment/giftOptionsCmd", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t7.inf", 
		LAST);

	web_add_auto_header("zipCode", 		"07094");

	web_custom_request("getShipmentMethods_2", 
		"URL=https://{p_hostname}/api/payment/getShipmentMethods", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t8.inf", 
		LAST);

	web_custom_request("getShipmentMethods_3", 
		"URL=https://{p_hostname}/api/payment/getShipmentMethods", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t9.inf", 
		LAST);
	
	lr_end_transaction("T07_Mobile_LoginFirstCheckout", LR_AUTO);
	if (p_MobileRand<=(LoginDropRatio))
		lr_exit(LR_EXIT_MAIN_ITERATION_AND_CONTINUE, LR_AUTO);
	web_cleanup_auto_headers();	
	
	lr_start_transaction("T07_Mobile_LoginFirstShipping");
	
	web_add_auto_header("Accept-Language", 
		"en-US");

	web_add_auto_header("User-Agent", 
		"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_auto_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");


	web_add_auto_header("Accept", 
		"application/json, text/plain, */*");

	web_add_auto_header("addressField1", 
		"10 main st");

	web_add_auto_header("catalogId", 
		"10551");

	web_add_auto_header("deviceType", 
		"App");

	web_add_auto_header("langId", 
		"-1");

	web_add_auto_header("storeId", 
		"10151");

	web_add_auto_header("zipCode", 
		"07094");

	web_custom_request("getShipmentMethods", 
		"URL=https://{p_hostname}/api/payment/getShipmentMethods", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t4.inf", 
		LAST);

	web_add_auto_header("state", 
		"DE");

	web_custom_request("getShipmentMethods_2", 
		"URL=https://{p_hostname}/api/payment/getShipmentMethods", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t5.inf", 
		LAST);

	web_revert_auto_header("zipCode");

	web_custom_request("getShipmentMethods_3", 
		"URL=https://{p_hostname}/api/payment/getShipmentMethods", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t6.inf", 
		LAST);

	web_add_header("zipCode", 
		"19801");

	web_custom_request("getShipmentMethods_4", 
		"URL=https://{p_hostname}/api/payment/getShipmentMethods", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t7.inf", 
		LAST);

	web_revert_auto_header("addressField1");

	web_revert_auto_header("state");

	web_add_auto_header("Origin", 
		"file://");
	web_reg_save_param("addressId", "LB=\"addressId\": \"", "RB=\",\n", "NotFound=Warning", LAST);
	web_reg_save_param ( "addressIdAll" , "LB=\"addressId\": \"" , "RB=\",\n" , "ORD=ALL", "NotFound=Warning", LAST ) ; //"addressId": "1398789",\n
	web_custom_request("addAddress", 
		"URL=https://{p_hostname}/api/payment/addAddress", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t8.inf", 
		"Mode=HTTP", 
		"EncType=application/json", 
		"Body={\n  \"contact\": [\n    {\n      \"addressLine\": [\n        \"10 W Main St\",\n        \"\",\n        \"\"\n      ],\n      \"addressType\": \"Shipping\",\n      \"attributes\": [\n        {\n          \"key\": \"addressField3\",\n          \"value\": \"19702\"\n        }\n      ],\n      \"city\": \"Christiana\",\n      \"country\": \"US\",\n      \"firstName\": \"john\",\n      \"lastName\": \"oliver\",\n      \"email1\": \"{p_userID}\",\n      \"phone1\": \"2122121212"
		"\",\n      \"state\": \"DE\",\n      \"zipCode\": \"19702\",\n      \"xcont_addressField3\": \"19702\",\n      \"nickName\": \"1506054056317\",\n      \"fromPage\": \"\",\n      \"phone1Publish\": \"true\",\n      \"primary\": \"false\"\n    }\n  ]\n}", 
		LAST);

	web_add_header("content-type", 
		"application/json");

	web_custom_request("updateShippingMethodSelection", 
		"URL=https://{p_hostname}/api/payment/updateShippingMethodSelection", 
		"Method=PUT", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t9.inf", 
		"Mode=HTTP", 
		"Body={\n  \"shipModeId\": \"\",\n  \"addressId\": \"{addressId}\",\n  \"x_calculationUsage\": \"-1,-2,-3,-4,-5,-6,-7\",\n  \"requesttype\": \"ajax\",\n  \"orderId\": \"{s_OrderId_1}\"\n}", 
		LAST);

	web_revert_auto_header("Origin");

	web_add_header("calc", 
		"false");

	web_add_header("locStore", 
		"true");

	web_add_header("pageName", 
		"fullOrderInfo");

	web_custom_request("getOrderDetails", 
		"URL=https://{p_hostname}/api/getOrderDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t10.inf", 
		LAST);

	web_custom_request("getAllCoupons", 
		"URL=https://{p_hostname}/api/tcporder/getAllCoupons", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t11.inf", 
		LAST);
	lr_end_transaction("T07_Mobile_LoginFirstShipping", LR_AUTO);
	web_cleanup_auto_headers();	
	if (p_MobileRand<=(LoginDropRatio+ShipPageRatio))
		lr_exit(LR_EXIT_MAIN_ITERATION_AND_CONTINUE, LR_AUTO);
	lr_start_transaction("T07_Mobile_LoginFirstBilling");
	web_add_auto_header("Accept", 
		"application/json, text/plain, */*");

	web_add_auto_header("Accept-Encoding", 
		"gzip,deflate");

	web_add_auto_header("Accept-Language", 
		"en-US");

	web_add_auto_header("Origin", 
		"file://");

	web_add_auto_header("User-Agent", 
			"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_auto_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");

	web_add_auto_header("catalogId", 
		"10551");

	web_add_header("content-type", 
		"application/json");

	web_add_auto_header("deviceType", 
		"App");

	web_add_auto_header("langId", 
		"-1");

	web_add_auto_header("storeId", 
		"10151");

	web_custom_request("updateAddress", 
		"URL=https://{p_hostname}/api/payment/updateAddress", 
		"Method=PUT", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"Mode=HTTP", 
		"Body={\n  \"fromPage\": \"checkout\",\n  \"addressId\": \"{addressId}\"\n}", 
		LAST);

	web_custom_request("addPaymentInstruction", 
		"URL=https://{p_hostname}/api/payment/addPaymentInstruction", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t2.inf", 
		"Mode=HTTP", 
		"EncType=application/json", 
		"Body={\n  \"paymentInstruction\": [\n    {\n      \"account\": \"2818526303813893\",\n      \"isDefault\": \"false\",\n      \"billing_address_id\": \"{addressId}\",\n      \"payMethodId\": \"VISA\",\n      \"cc_brand\": \"VISA\",\n      \"expire_month\": \"3\",\n      \"expire_year\": \"2020\",\n      \"cc_cvc\": \"123\",\n      \"piAmount\": \"{piAmount}\"\n    }\n  ]\n}", 
		LAST);
//		"Body={\n  \"paymentInstruction\": [\n    {\n      \"account\": \"2818526303813893\",\n      \"isDefault\": \"false\",\n      \"billing_address_id\": \"{addressId}\",\n      \"payMethodId\": \"COMPASSVISA\",\n      \"cc_brand\": \"VISA\",\n      \"expire_month\": \"3\",\n      \"expire_year\": \"2020\",\n      \"cc_cvc\": \"123\",\n      \"piAmount\": \"{piAmount}\"\n    }\n  ]\n}", 
//"Body={  \"paymentInstruction\": [    {      \"account\": \"2818526303813893\",      \"isDefault\": \"false\",\n      \"billing_address_id\": \"{addressId}\",\n      \"payMethodId\": \"VISA\",\n      \"cc_brand\": \"VISA\",\n      \"expire_month\": \"11\",\n      \"expire_year\": \"2020\",\n      \"cc_cvc\": \"123\",\n      \"piAmount\": \"{piAmount}\"\n    }\n  ]\n}", 
	web_revert_auto_header("Origin");

	web_add_header("calc", 
		"false");

	web_add_header("locStore", 
		"true");

	web_add_header("pageName", 
		"fullOrderInfo");

	web_custom_request("getOrderDetails", 
		"URL=https://{p_hostname}/api/getOrderDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t3.inf", 
		LAST);

	web_custom_request("getAllCoupons", 
		"URL=https://{p_hostname}/api/tcporder/getAllCoupons", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t4.inf", 
		LAST);

	lr_end_transaction("T07_Mobile_LoginFirstBilling", LR_AUTO);
	
	web_cleanup_auto_headers();	
	if (p_MobileRand<=(LoginDropRatio+ShipPageRatio+BillPageRatio))
		lr_exit(LR_EXIT_MAIN_ITERATION_AND_CONTINUE, LR_AUTO);
	
	lr_start_transaction("T16_Submit_Order_Mobile_Registered");
	web_add_auto_header("Accept", 
		"application/json, text/plain, */*");

	web_add_auto_header("Accept-Encoding", 
		"gzip,deflate");

	web_add_auto_header("Accept-Language", 
		"en-US");

	web_add_header("Origin", 
		"file://");

	web_add_auto_header("User-Agent", 
		"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_auto_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");

	web_add_auto_header("catalogId", 
		"10551");

	web_add_auto_header("deviceType", 
		"App");

	web_add_auto_header("langId", 
		"-1");

	web_add_auto_header("storeId", 
		"10151");

	web_custom_request("addCheckout", 
		"URL=https://{p_hostname}/api/addCheckout", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"Mode=HTTP", 
		"EncType=application/json", 
//		"Body={\n  \"orderId\": \"8056536188\",\n  \"isRest\": \"true\",\n  \"locStore\": \"True\"\n}", 
		"Body={\n  \"orderId\": \"{s_OrderId_1}\",\n  \"isRest\": \"true\",\n  \"locStore\": \"True\"\n}", 
		//"Body={  \"orderId\": \"{s_OrderId_1}\",  \"isRest\": \"true\",  \"locStore\": \"True\"}", 
		LAST);
//web_cleanup_auto_headers();	
/*	web_custom_request("getRegisteredUserDetailsInfo", 
		"URL=https://{p_hostname}/api/payment/getRegisteredUserDetailsInfo", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t2.inf", 
		LAST);

	web_add_header("fromRest", 
		"true");

	web_custom_request("getPointsAndOrderHistory", 
		"URL=https://{p_hostname}/api/tcporder/getPointsAndOrderHistory", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t3.inf", 
		LAST);

	web_add_header("externalId", 
		"286018");

	web_custom_request("getListofDefaultWishlistbyId", 
		"URL=https://{p_hostname}/api/tcpproduct/getListofDefaultWishlistbyId", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t4.inf", 
		LAST);

	web_add_header("espotName", 
		"AppHomepageEspot1");

	web_custom_request("getESpot", 
		"URL=https://{p_hostname}/api/getESpot", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t5.inf", 
		LAST);

	web_add_header("depthAndLimit", 
		"-1,-1,0");

	web_custom_request("getTopCategories", 
		"URL=https://{p_hostname}/api/tcpproduct/getTopCategories", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t6.inf", 
		LAST);

	web_add_header("action", 
		"get");

	web_custom_request("getFavouriteStoreLocation", 
		"URL=https://{p_hostname}/api/tcporder/getFavouriteStoreLocation/", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t7.inf", 
		LAST);

	web_add_header("espotName", 
		"AppDepartmentThumbs");

	web_custom_request("getESpot_2", 
		"URL=https://{p_hostname}/api/getESpot", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t8.inf", 
		LAST);
*/
	lr_end_transaction("T16_Submit_Order_Mobile_Registered", LR_AUTO);


	return 0;
}

Billing2(){
			web_add_auto_header("Accept", 
		"application/json, text/plain, */*");

	web_add_auto_header("Accept-Encoding", 
		"gzip,deflate");

	web_add_auto_header("Accept-Language", 
		"en-US");

	web_add_header("Origin", 
		"file://");

	web_add_auto_header("User-Agent", 
		"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_auto_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");

	web_add_auto_header("catalogId", 
		"10551");

	web_add_auto_header("deviceType", 
		"App");

	web_add_auto_header("isRest", 
		"true");

	web_add_auto_header("langId", 
		"-1");

	web_add_auto_header("storeId", 
		"10151");

	web_add_header("isRest", 
		"true");

	web_custom_request("getCreditCardDetails", 
		"URL=https://{p_hostname}/api/payment/getCreditCardDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t2.inf", 
		LAST);

	web_custom_request("modifyCreditCardDetails", 
		"URL=https://{p_hostname}/api/payment/modifyCreditCardDetails", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"Mode=HTTP", 
		"EncType=application/json", 
		"Body={\n  \"creditCardId\": \"\",\n  \"pay_account\": \"4111111111111111\",\n  \"addressId\": 1587775,\n  \"billing_firstName\": \"test\",\n  \"billing_lastName\": \"me\",\n  \"billing_city\": \"Christiana\",\n  \"billing_state\": \"DE\",\n  \"billing_address1\": \"10 W Main St  \",\n  \"billing_address2\": \"\",\n  \"billing_addressField3\": \"19702\",\n  \"billing_zipCode\": \"19702\",\n  \"billing_country\": \"US\",\n  \"billing_phone1\": \"3022121212\",\n  \"pay_expire_year\": \"2020\","
		"\n  \"pay_expire_month\": \"6\",\n  \"billing_nickName\": \"1506052135432\",\n  \"isDefault\": \"true\",\n  \"payMethodId\": \"COMPASSVISA\",\n  \"cc_brand\": \"VISA\"\n}", 
		LAST);

	web_custom_request("getCreditCardDetails", 
		"URL=https://{p_hostname}/api/payment/getCreditCardDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t2.inf", 
		LAST);

	web_revert_auto_header("isRest");

	web_add_header("Origin", 
		"file://");

	web_custom_request("addPaymentInstruction", 
		"URL=https://{p_hostname}/api/payment/addPaymentInstruction", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t3.inf", 
		"Mode=HTTP", 
		"EncType=application/json", 
		//"Body={\n  \"paymentInstruction\": [\n    {\n      \"account\": \"4111111111111111\",\n      \"isDefault\": \"true\",\n      \"billing_address_id\": \"1587775\",\n      \"payMethodId\": \"COMPASSVISA\",\n      \"cc_brand\": \"VISA\",\n      \"expire_month\": \"6\",\n      \"expire_year\": \"2020\",\n      \"piAmount\": \"12.47\",\n      \"creditCardId\": \"468529\"\n    }\n  ]\n}", 
		"Body={  \"paymentInstruction\": [    {      \"account\": \"2818526303813893\",      \"isDefault\": \"false\",\n      \"billing_address_id\": \"{addressId}\",\n      \"payMethodId\": \"VISA\",\n      \"cc_brand\": \"VISA\",\n      \"expire_month\": \"11\",\n      \"expire_year\": \"2020\",\n      \"cc_cvc\": \"123\",\n      \"piAmount\": \"{piAmount}\"\n    }\n  ]\n}", 
		LAST);

	web_add_header("calc", 
		"false");

	web_add_header("locStore", 
		"true");

	web_add_header("pageName", 
		"fullOrderInfo");

	web_custom_request("getOrderDetails", 
		"URL=https://{p_hostname}/api/getOrderDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t4.inf", 
		LAST);

	web_add_header("Origin", 
		"file://");

	web_custom_request("addPaymentInstruction_2", 
		"URL=https://{p_hostname}/api/payment/addPaymentInstruction", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t5.inf", 
		"Mode=HTTP", 
		"EncType=application/json", 
		"Body={\n  \"paymentInstruction\": [\n    {\n      \"account\": \"************3893\",\n      \"isDefault\": \"true\",\n      \"billing_address_id\": \"1587775\",\n      \"payMethodId\": \"COMPASSVISA\",\n      \"cc_brand\": \"Visa\",\n      \"expire_month\": \"6 \",\n      \"expire_year\": \"2020\",\n      \"piAmount\": \"{piAmount}\",\n      \"creditCardId\": \"468529\"\n    }\n  ]\n}", 
		LAST);

	web_add_header("calc", 
		"false");

	web_add_header("locStore", 
		"true");

	web_add_header("pageName", 
		"fullOrderInfo");

	web_custom_request("getOrderDetails_2", 
		"URL=https://{p_hostname}/api/getOrderDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t6.inf", 
		LAST);

	web_custom_request("giftOptionsCmd", 
		"URL=https://{p_hostname}/api/payment/giftOptionsCmd", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t7.inf", 
		LAST);

	return 0;
}