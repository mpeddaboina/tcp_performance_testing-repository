ViewBag()
{
	//lr_set_debug_message(16|8|4|2,1);
	if (!isLoggedin){  //Guest Checkout or Browse first
		
	
	lr_think_time(TT);
	
	web_add_auto_header("Accept", 
		"application/json, text/plain, */*");

	web_add_auto_header("Accept-Encoding", 
		"gzip,deflate");

	web_add_auto_header("Accept-Language", 
		"en-US");

	web_add_auto_header("User-Agent", 
		"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_auto_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");

	web_add_auto_header("catalogId", 
		"10551");

	web_add_auto_header("deviceType", 
		"App");

	web_add_auto_header("langId", 
		"-1");

	web_add_auto_header("storeId", 
		"10151");
	lr_start_transaction("T06_MobileViewBag");
	web_reg_find("TEXT/IC=appliedCoupons","SaveCount=couponsapply",LAST);  //appliedCoupons
	lr_start_sub_transaction("T06_MobileViewBag_getAllCoupons","T06_MobileViewBag");
	web_custom_request("getAllCoupons", 
		"URL=https://{p_hostname}/api/tcporder/getAllCoupons", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		LAST);
	if (atoi(lr_eval_string("{couponsapply}"))>0){
		lr_end_sub_transaction("T06_MobileViewBag_getAllCoupons", LR_AUTO); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}else{
		lr_end_sub_transaction("T06_MobileViewBag_getAllCoupons", LR_FAIL); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}	
	web_add_header("locStore", 
		"true");

	web_add_header("pageName", 
		"fullOrderInfo");
	web_reg_find("TEXT/IC=shippingCharge","SaveCount=shippingCharge",LAST);
	lr_start_sub_transaction ( "T06_MobileViewBag_getOrderDetails", "T06_MobileViewBag" );
	web_reg_save_param_json( "ParamName=s_OrderId", "QueryString=$.parentOrderId", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param("piAmount", "LB=\"piAmount\": \"", "RB=\",", "NotFound=Warning", LAST); //"piAmount": "7.95",\n  "piAmount": "22.30",\n
	web_custom_request("getOrderDetails", 
		"URL=https://{p_hostname}/api/getOrderDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t2.inf", 
		LAST);
	
	if (atoi(lr_eval_string("{shippingCharge}"))>0){
		lr_end_sub_transaction("T06_MobileViewBag_getOrderDetails", LR_AUTO); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}else{
		lr_end_sub_transaction("T06_MobileViewBag_getOrderDetails", LR_FAIL); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}	
	
	lr_end_transaction("T06_MobileViewBag", LR_AUTO);
	web_cleanup_auto_headers();
	}else{   //This is logged in First User.
		
	web_add_auto_header("Accept", 
		"application/json, text/plain, */*");

	web_add_auto_header("Accept-Encoding", 
		"gzip,deflate");

	web_add_auto_header("Accept-Language", 
		"en-US");

	web_add_auto_header("User-Agent", 
		"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_auto_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");

	web_add_auto_header("catalogId", 
		"10551");

	web_add_auto_header("deviceType", 
		"App");

	web_add_auto_header("langId", 
		"-1");

	web_add_auto_header("storeId", 
		"10151");
	lr_start_transaction("T06_MobileViewBag");
	web_reg_find("TEXT/IC=appliedCoupons","SaveCount=couponsapply",LAST);  //appliedCoupons
	lr_start_sub_transaction("T06_MobileViewBag_getAllCoupons","T06_MobileViewBag");
	web_custom_request("getAllCoupons", 
		"URL=https://{p_hostname}/api/tcporder/getAllCoupons", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		LAST);
	if (atoi(lr_eval_string("{couponsapply}"))>0){
		lr_end_sub_transaction("T06_MobileViewBag_getAllCoupons", LR_AUTO); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}else{
		lr_end_sub_transaction("T06_MobileViewBag_getAllCoupons", LR_FAIL); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}	
	
	web_reg_save_param_json( "ParamName=s_addressID", "QueryString=$.contact..addressId", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_find("TEXT/IC=addressId","SaveCount=addressId",LAST);  //appliedCoupons
	lr_start_sub_transaction("T06_MobileViewBag_getAddressFromBook","T06_MobileViewBag");
	web_custom_request("getAddressFromBook", 
		"URL=https://{p_hostname}/api/payment/getAddressFromBook", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t2.inf", 
		LAST);
	
	if (atoi(lr_eval_string("{addressId}"))>0){
		lr_end_sub_transaction("T06_MobileViewBag_getAddressFromBook", LR_AUTO); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}else{
		lr_end_sub_transaction("T06_MobileViewBag_getAddressFromBook", LR_FAIL); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}	
	web_add_auto_header("isRest", 
		"true");
	//creditCardInfoJson
	web_reg_find("TEXT/IC=creditCardInfoJson","SaveCount=creditCardInfoJson",LAST);  //appliedCoupons
	lr_start_sub_transaction("T06_MobileViewBag_getCreditCardDetails","T06_MobileViewBag");
	web_custom_request("getCreditCardDetails", 
		"URL=https://{p_hostname}/api/payment/getCreditCardDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t3.inf", 
		LAST);
	if (atoi(lr_eval_string("{creditCardInfoJson}"))>0){
		lr_end_sub_transaction("T06_MobileViewBag_getCreditCardDetails", LR_AUTO); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}else{
		lr_end_sub_transaction("T06_MobileViewBag_getCreditCardDetails", LR_FAIL); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}

	web_reg_find("TEXT/IC=creditCardInfoJson","SaveCount=creditCardInfoJson",LAST);  //appliedCoupons
	lr_start_sub_transaction("T06_MobileViewBag_getCreditCardDetails","T06_MobileViewBag");
	
	web_custom_request("getCreditCardDetails_2", 
		"URL=https://{p_hostname}/api/payment/getCreditCardDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t4.inf", 
		LAST);
	if (atoi(lr_eval_string("{creditCardInfoJson}"))>0){
		lr_end_sub_transaction("T06_MobileViewBag_getCreditCardDetails", LR_AUTO); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}else{
		lr_end_sub_transaction("T06_MobileViewBag_getCreditCardDetails", LR_FAIL); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}

	web_revert_auto_header("isRest");

	web_add_header("locStore", 
		"true");

	web_add_header("pageName", 
		"fullOrderInfo");

	web_reg_find("TEXT/IC=shippingCharge","SaveCount=shippingCharge",LAST);
	lr_start_sub_transaction ( "T06_MobileViewBag_getOrderDetails", "T06_MobileViewBag" );
	web_reg_save_param_json( "ParamName=s_OrderId", "QueryString=$.parentOrderId", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param("piAmount", "LB=\"piAmount\": \"", "RB=\",", "NotFound=Warning", LAST); //"piAmount": "7.95",\n  "piAmount": "22.30",\n

	web_custom_request("getOrderDetails", 
		"URL=https://{p_hostname}/api/getOrderDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t5.inf", 
		LAST);
	if (atoi(lr_eval_string("{shippingCharge}"))>0){
		lr_end_sub_transaction("T06_MobileViewBag_getOrderDetails", LR_AUTO); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}else{
		lr_end_sub_transaction("T06_MobileViewBag_getOrderDetails", LR_FAIL); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}	
	
	lr_end_transaction("T06_MobileViewBag", LR_AUTO);
	web_cleanup_auto_headers();
	}
	return 0;
}
