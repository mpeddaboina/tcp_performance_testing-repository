SearchProductByTerm()
{
	int ij=0,jk=0, retry=0,found=0, typeStr;
	char s_SearchStr[50];
	memset(s_SearchStr, 0, sizeof(s_SearchStr));
//	lr_set_debug_message(16|8|4|2,1);
	
	while(found==0){
		if (retry==10)
			break;//  10 attempts to find a suitable search term and we give up.. 
		lr_advance_param("p_searchTerm");
		lr_save_string(lr_eval_string("{p_searchTerm}"),"mySearchStr");
		ij = strlen(lr_eval_string("{mySearchStr}"));
		if (ij==0){ //nothing to search on -- bad data
			//we will get the next data point
			++retry;
		}else{
			found=1;
			break;
		}
		
	}
	
	if (found==0){
		lr_exit(LR_EXIT_ITERATION_AND_CONTINUE,LR_AUTO);
		return 0;
	}
	lr_save_string(lr_eval_string("{mySearchStr}"),"s_SearchStr");//in case it happens to be 3
	// 3 types of typers - Fast typers 4 characters in 1 sec 10%, medium typers 70% 2 characters in 1 sec- slow typers 1 character every sec
	for (typeStr=3;typeStr<=ij;typeStr++){
		memset(s_SearchStr, 0, sizeof(s_SearchStr));
		strncpy(s_SearchStr,lr_eval_string("{mySearchStr}"),typeStr);
		lr_save_string(s_SearchStr,"s_SearchStr");
	
	web_add_header("term","{s_SearchStr}");

	web_add_header("searchTerm", "{s_SearchStr}");

	web_add_header("Accept", 
		"application/json, text/plain, */*");

	web_add_header("Accept-Encoding", 
		"gzip,deflate");

	web_add_header("Accept-Language", 
		"en-US");

	web_add_header("User-Agent", 
		"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");

	web_add_header("catalogId", 
		"10551");

	web_add_header("coreName", 
		"MC_10001_CatalogEntry_en_US");

	web_add_header("coreNameCategory", 
		"MC_10001_CatalogGroup_en_US");

	web_add_header("count", "4");

	web_add_header("deviceType", 
		"App");

	web_add_header("isRest", 
		"true");

	web_add_header("langId", 
		"-1");

	web_add_header("pageNumber", 
		"4");

	web_add_header("published", 
		"1");

	web_add_header("rows", 
		"4");

	web_add_header("start", 
		"0");

	web_add_header("storeId", 
		"10151");

	web_add_header("timeAllowed", 
		"15000");

	web_custom_request("getAutoSuggestions", 
		"URL=https://{p_hostname}/api/getAutoSuggestions", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		LAST);
	
	}

	web_convert_param("mySearchStr", "SourceEncoding=PLAIN","TargetEncoding=URL", LAST );
	web_add_header("langId", "-1");
	web_add_header("pageSize","30");
	web_add_header("catalogId", "10551");
	web_add_header("pageNumber","1");
	web_add_header("categoryId", "");
	web_add_header("User-Agent", "Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");
	web_add_header("orderBy", "null");
	web_add_header("facet", "");
	web_add_header("Accept", 	"application/json, text/plain, */*");
	web_add_header("searchTerm", "{mySearchStr}");
	web_add_header("storeId", "10151");
	web_add_header("deviceType", "App");
	web_add_header("Accept-Encoding", 
		"gzip,deflate");
	web_add_header("Accept-Language", 
		"en-US");
	web_add_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");
	web_reg_save_param_json( "ParamName=s_products", "QueryString=$.catalogEntryView[?(@.partNumber)].uniqueID", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	
	lr_start_transaction("T02b_Mobile_SearchByTerm");

	web_custom_request("getProductsBySearchTerm", 
		"URL=https://{p_hostname}/api/tcpproduct/getProductsBySearchTerm", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t5.inf", 
		LAST);

	lr_end_transaction("T02b_Mobile_SearchByTerm", LR_AUTO);

	web_add_header("term","{s_SearchStr}");

	web_add_header("searchTerm", "{s_SearchStr}");

	web_add_header("Accept", 
		"application/json, text/plain, */*");

	web_add_header("Accept-Encoding", 
		"gzip,deflate");

	web_add_header("Accept-Language", 
		"en-US");

	web_add_header("User-Agent", 
		"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");

	web_add_header("catalogId", 
		"10551");

	web_add_header("coreName", 
		"MC_10001_CatalogEntry_en_US");

	web_add_header("coreNameCategory", 
		"MC_10001_CatalogGroup_en_US");

	web_add_header("count", "4");

	web_add_header("deviceType", 
		"App");

	web_add_header("isRest", 
		"true");

	web_add_header("langId", 
		"-1");

	web_add_header("pageNumber", 
		"4");

	web_add_header("published", 
		"1");

	web_add_header("rows", 
		"4");

	web_add_header("start", 
		"0");

	web_add_header("storeId", 
		"10151");

	web_add_header("timeAllowed", 
		"15000");

	web_custom_request("getAutoSuggestions_5", 
		"URL=https://{p_hostname}/api/getAutoSuggestions", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t6.inf", 
		LAST);

	web_cleanup_auto_headers();
	//lr_abort();
	return 0;
}
