LoginFirst()
{
	
	//lr_set_debug_message(16|8|4|2,1);
	lr_think_time(TT);
	web_add_auto_header("Accept", 
		"application/json, text/plain, */*");

	web_add_auto_header("Accept-Encoding", 
		"gzip,deflate");

	web_add_auto_header("Accept-Language", 
		"en-US");

	web_add_header("Origin", 
		"file://");

	web_add_auto_header("User-Agent", 
		"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_auto_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");

	web_add_auto_header("catalogId", 
		"10551");

	web_add_auto_header("deviceType", 
		"App");

	web_add_auto_header("langId", 
		"-1");

	web_add_auto_header("storeId", 
		"10151");
	//LoginSuccess
	web_reg_find("TEXT/IC=LoginSuccess","SaveCount=LoginSuccess",LAST);
	lr_start_transaction("T02a_Mobile_LoginFirst");
	lr_start_sub_transaction ( "T02a_Mobile_LoginFirst_logon", "T02a_Mobile_LoginFirst" );
	web_custom_request("logon", 
		"URL=https://{p_hostname}/api/logon", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"Mode=HTTP", 
		"EncType=application/json", 
		"Body={\n  \"reLogonURL\": \"TCPAjaxLogonErrorView\",\n  \"URL\": \"TCPAjaxLogonSuccessView\",\n  \"storeId\": \"10151\",\n  \"rememberCheck\": \"true\",\n  \"logonId1\": \"{p_userID}\",\n  \"logonPassword1\": \"Asdf!234\",\n  \"rememberMe\": true,\n  \"useTouchId\": false\n}", 	
		LAST);

//		"Body={\n  \"reLogonURL\": \"TCPAjaxLogonErrorView\",\n  \"URL\": \"TCPAjaxLogonSuccessView\",\n  \"storeId\": \"10151\",\n  \"rememberCheck\": \"true\",\n  \"logonId1\": \"manny654321@gmail.com\",\n  \"logonPassword1\": \"Asdf!234\",\n  \"rememberMe\": true,\n  \"useTouchId\": false\n}", 
	if (atoi(lr_eval_string("{LoginSuccess}"))>0){
		lr_end_sub_transaction("T02a_Mobile_LoginFirst_logon", LR_AUTO); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}else{
		lr_end_sub_transaction("T02a_Mobile_LoginFirst_logon", LR_FAIL); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}
	
	web_reg_find("TEXT/IC=resourceName","SaveCount=reguserdetails",LAST);
	lr_start_sub_transaction ( "T02a_Mobile_LoginFirst_getRegisteredUserDetailsInfo", "T02a_Mobile_LoginFirst" );
	web_custom_request("getRegisteredUserDetailsInfo", 
		"URL=https://{p_hostname}/api/payment/getRegisteredUserDetailsInfo", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		LAST);
	
	if (atoi(lr_eval_string("{reguserdetails}"))>0){
		lr_end_sub_transaction("T02a_Mobile_LoginFirst_getRegisteredUserDetailsInfo", LR_AUTO); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}else{
		lr_end_sub_transaction("T02a_Mobile_LoginFirst_getRegisteredUserDetailsInfo", LR_FAIL); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}
	
	web_add_header("fromRest", 
		"true");

	web_reg_find("TEXT/IC=getOrderHistoryResponse","SaveCount=getOrderHistoryResponse",LAST);
	lr_start_sub_transaction ( "T02a_Mobile_LoginFirst_getPointsAndOrderHistory", "T02a_Mobile_LoginFirst" );
	web_custom_request("getPointsAndOrderHistory", 
		"URL=https://{p_hostname}/api/tcporder/getPointsAndOrderHistory", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t2.inf", 
		LAST);
	if (atoi(lr_eval_string("{getOrderHistoryResponse}"))>0){
		lr_end_sub_transaction("T02a_Mobile_LoginFirst_getPointsAndOrderHistory", LR_AUTO); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}else{
		lr_end_sub_transaction("T02a_Mobile_LoginFirst_getPointsAndOrderHistory", LR_FAIL); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}

	web_add_header("calc", "false");
	web_add_header("locStore", "True");
	web_add_header("pageName", "fullOrderInfo");

	web_reg_find("TEXT/IC=shippingCharge","SaveCount=shippingCharge",LAST);
	lr_start_sub_transaction ( "T02a_Mobile_LoginFirst_getOrderDetails", "T02a_Mobile_LoginFirst" );
	web_custom_request("getOrderDetails", 
		"URL=https://{p_hostname}/api/getOrderDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t4.inf", 
		LAST);
	if (atoi(lr_eval_string("{shippingCharge}"))>0){
		lr_end_sub_transaction("T02a_Mobile_LoginFirst_getOrderDetails", LR_AUTO); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}else{
		lr_end_sub_transaction("T02a_Mobile_LoginFirst_getOrderDetails", LR_FAIL); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}

	web_add_header("action", 
		"get");
//	web_add_header("Accept", "application/json");

	web_reg_find("TEXT/IC={","SaveCount=getfavouriteStoreLocation",LAST);
	lr_start_sub_transaction ( "T02a_Mobile_LoginFirst_getFavouriteStoreLocation", "T02a_Mobile_LoginFirst" );
	web_custom_request("getFavouriteStoreLocation", 
		"URL=https://{p_hostname}/api/tcporder/getFavouriteStoreLocation/", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t5.inf", 
		LAST);
	if (atoi(lr_eval_string("{getfavouriteStoreLocation}"))>0){
		lr_end_sub_transaction("T02a_Mobile_LoginFirst_getFavouriteStoreLocation", LR_AUTO); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}else{
		lr_end_sub_transaction("T02a_Mobile_LoginFirst_getFavouriteStoreLocation", LR_FAIL); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}
//$..giftListExternalIdentifier//status
lr_continue_on_error(1);
/*
	web_reg_find("TEXT/IC=status","SaveCount=status",LAST);  //appliedCoupons
	web_reg_save_param_json( "ParamName=s_getListofDefaultWishlist", "QueryString=$..giftListExternalIdentifier", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	lr_start_sub_transaction("T02a_Mobile_LoginFirst_getListofDefaultWishlist","T02a_Mobile_LoginFirst");
	web_custom_request("getListofDefaultWishlist", 
		"URL=https://{p_hostname}/api/getListofDefaultWishlist", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t6.inf", 
		LAST);
	if (atoi(lr_eval_string("{status}"))>0){
		lr_end_sub_transaction("T02a_Mobile_LoginFirst_getListofDefaultWishlist", LR_AUTO); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}else{
		lr_end_sub_transaction("T02a_Mobile_LoginFirst_getListofDefaultWishlist", LR_FAIL); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}
*/	
	web_reg_find("TEXT/IC=appliedCoupons","SaveCount=couponsapply",LAST);  //appliedCoupons
	lr_start_sub_transaction("T02a_Mobile_LoginFirst_getAllCoupons","T02a_Mobile_LoginFirst");
	web_custom_request("getAllCoupons", 
		"URL=https://{p_hostname}/api/tcporder/getAllCoupons", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t2.inf", 
		LAST);
	lr_continue_on_error(0);
	
	if (atoi(lr_eval_string("{couponsapply}"))>0){
		lr_end_sub_transaction("T02a_Mobile_LoginFirst_getAllCoupons", LR_AUTO); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}else{
		lr_end_sub_transaction("T02a_Mobile_LoginFirst_getAllCoupons", LR_FAIL); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}	
/*
	if (atoi(lr_eval_string("{s_getListofDefaultWishlist_count}"))>0){
	
	web_add_header("externalId", "{s_getListofDefaultWishlist_1}");
	web_reg_find("TEXT/IC=accessSpecifier","SaveCount=accessSpecifier",LAST);  //appliedCoupons
	lr_start_sub_transaction("T02a_Mobile_LoginFirst_getListofDefaultWishlistbyId","T02a_Mobile_LoginFirst");
	web_custom_request("getListofDefaultWishlistbyId", 
		"URL=https://{p_hostname}/api/tcpproduct/getListofDefaultWishlistbyId", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t8.inf", 
		LAST);
	if (atoi(lr_eval_string("{accessSpecifier}"))>0){
		lr_end_sub_transaction("T02a_Mobile_LoginFirst_getListofDefaultWishlistbyId", LR_AUTO); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}else{
		lr_end_sub_transaction("T02a_Mobile_LoginFirst_getListofDefaultWishlistbyId", LR_FAIL); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}		
	}
*/
	lr_end_transaction("T02a_Mobile_LoginFirst", LR_AUTO);
	web_cleanup_auto_headers();		
	isLoggedin=1;
	return 0;
}
