HomePage()
{	

//	web_set_sockets_option("SSL_VERSION", "TLS"); 
	web_set_sockets_option("SSL_VERSION", "TLS1.1");  //02232018
		lr_think_time(TT);
	web_set_max_html_param_len ( "3072" ) ;
	web_cleanup_cookies () ;
	web_cache_cleanup();
	
		p_MobileRand=atoi(lr_eval_string("{p_Random}"));
		
	lr_start_transaction("T01_MobileHomePage");

	web_add_auto_header("Accept", 
		"application/json, text/plain, */*");

	web_add_auto_header("Accept-Encoding", 
		"gzip,deflate");

	web_add_auto_header("Accept-Language", 
		"en-US");

	web_add_auto_header("User-Agent", 
		"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_auto_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");

	web_add_auto_header("catalogId", 
		"10551");

	web_add_auto_header("deviceType", 
		"App");

	web_add_auto_header("langId", 
		"-1");

	web_add_auto_header("storeId", 
		"10151");
	web_reg_find("TEXT/IC=resourceName","SaveCount=reguserdetails",LAST);
	lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	web_custom_request("getRegisteredUserDetailsInfo", 
		"URL=https://{p_hostname}/api/payment/getRegisteredUserDetailsInfo", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		LAST);
	
	if (atoi(lr_eval_string("{reguserdetails}"))>0){
		lr_end_sub_transaction("T01_MobileHomePage_getRegisteredUserDetailsInfo", LR_AUTO); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}else{
		lr_end_sub_transaction("T01_MobileHomePage_getRegisteredUserDetailsInfo", LR_FAIL); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}
	web_add_header("locStore", 
		"false");

	web_add_header("pageName", 
		"orderSummary");
	
	web_reg_find("TEXT/IC={","SaveCount=shippingCharge",LAST);
	lr_start_sub_transaction ( "T01_MobileHomePage_getOrderDetails", "T01_MobileHomePage" );
	web_custom_request("getOrderDetails", 
		"URL=https://{p_hostname}/api/getOrderDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t4.inf", 
		LAST);
	if (atoi(lr_eval_string("{shippingCharge}"))>0){
		lr_end_sub_transaction("T01_MobileHomePage_getOrderDetails", LR_AUTO); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}else{
		lr_end_sub_transaction("T01_MobileHomePage_getOrderDetails", LR_FAIL); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}
	web_add_header("depthAndLimit", 
		"-1,-1,0");
	
	
	web_reg_save_param_json( "ParamName=subCategories", "QueryString=$.catalogGroupView..catalogGroupView..seo_token_ntk", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_custom_request("getTopCategories", 
		"URL=https://{p_hostname}/api/tcpproduct/getTopCategories", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t7.inf", 
		LAST);

	web_reg_find("TEXT/IC={","SaveCount=resourceName",LAST);
	lr_start_sub_transaction ( "T01_MobileHomePage_getPriceDetails", "T01_MobileHomePage" );
	web_custom_request("getPriceDetails", 
		"URL=https://{p_hostname}/api/tcpproduct/getPriceDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t7.inf", 
		LAST);
	if (atoi(lr_eval_string("{resourceName}"))>0){
		lr_end_sub_transaction("T01_MobileHomePage_getPriceDetails", LR_AUTO); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}else{
		lr_end_sub_transaction("T01_MobileHomePage_getPriceDetails", LR_FAIL); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}
	
	lr_end_transaction("T01_MobileHomePage", LR_AUTO);
	
	web_cleanup_auto_headers();		

	return 0;
}