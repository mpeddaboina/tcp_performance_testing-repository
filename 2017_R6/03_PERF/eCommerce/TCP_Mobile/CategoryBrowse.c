CategoryBrowse()
{

	lr_think_time(TT);	
	lr_save_string(lr_paramarr_random("subCategories"), "s_subCategory");

	//web_add_header("categoryId", 		"girls-tops-girls-shirts");
	web_add_header("categoryId", 		"{s_subCategory}");

	web_add_auto_header("catalogId", 
		"10551");

	web_add_auto_header("deviceType", 
		"App");

	web_add_auto_header("facet", 
		"");

	web_add_auto_header("langId", 
		"-1");

	web_add_auto_header("orderBy", 
		"null");

	web_add_auto_header("pageNumber", 
		"1");

	web_add_auto_header("pageSize", 
		"30");

	web_add_auto_header("searchSource", 
		"E");

	web_add_auto_header("searchType", 
		"-1,-1,0");

	web_add_auto_header("storeId", 
		"10151");
	web_reg_find("TEXT/IC=recordSetTotalMatches","SaveCount=recordSetTotalMatches",LAST);
	lr_start_transaction("T02_MobileCategoryPage");
	//$.catalogEntryView[?(@.partNumber)].uniqueID
	web_reg_save_param_json( "ParamName=s_products", "QueryString=$.catalogEntryView[?(@.partNumber)].uniqueID", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	lr_start_sub_transaction("T02_MobileCategoryPage_getProductviewbyCategory","T02_MobileCategoryPage");
	web_custom_request("getProductviewbyCategory", 
		"URL=https://{p_hostname}/api/tcpproduct/getProductviewbyCategory", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t3.inf", 
		LAST);

	if (atoi(lr_eval_string("{recordSetTotalMatches}"))>0){
		lr_end_sub_transaction("T02_MobileCategoryPage_getProductviewbyCategory", LR_AUTO); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}else{
		lr_end_sub_transaction("T02_MobileCategoryPage_getProductviewbyCategory", LR_FAIL); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}
	lr_end_transaction("T02_MobileCategoryPage", LR_AUTO);

	web_cleanup_auto_headers();	
	
	return 0;
}
