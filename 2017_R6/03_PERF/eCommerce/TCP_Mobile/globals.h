#ifndef _GLOBALS_H 
#define _GLOBALS_H

//--------------------------------------------------------------------
// Include Files
#include "lrun.h"
#include "web_api.h"
#include "lrw_custom_body.h"
#include "Mobile.h"

//--------------------------------------------------------------------
// Global Variables
int TT=10;
int isLoggedin=0;   //0 not loggedIn; 1 is logged in
int p_MobileRand=0;
int DropCartRatio=0,LoginDropRatio=0,ShipPageRatio=0,BillPageRatio=0;
//int DropCartRatio=60,LoginDropRatio=20,ShipPageRatio=20,BillPageRatio=20;

char *EncodePlainToURL(const char *sIn, char *sOut)
{
    int i;
    char cCurChar;
    char sCurStr[4] = {0};

    sOut[0] = '\0';

    for (i = 0; cCurChar = sIn[i]; i++)
    {
        // if this is a digit or an alphabetic letter
        if (isdigit(cCurChar) || isalpha(cCurChar))
        {
            // then write the current character "as is"
            sprintf(sCurStr, "%c", cCurChar);
        }
        else
        {
            // else convert it to hex-form. "_" -> "%5F"
            sprintf(sCurStr, "%%%X", cCurChar);
        }

        // append current item to the output string
        strcat(sOut, sCurStr);
    }

    return sOut;
}

#endif // _GLOBALS_H
