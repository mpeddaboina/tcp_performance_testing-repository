AddToCart()
{
	int i=0,j=0;
	int flag=0;
	char Kount[5];
	//lr_set_debug_message(16|8|4|2,1);
	lr_think_time(TT);
	if ((i=atoi(lr_eval_string("{s_catentryIDs_count}")))==0){
		lr_exit(LR_EXIT_MAIN_ITERATION_AND_CONTINUE,LR_AUTO);
		return 0;
	}
	
	for (j=1;j<=i;++j){
		memset(Kount,0,sizeof(Kount));
		sprintf(Kount,"%d", j);
		lr_save_string(Kount,"dyn");
		if (atoi(lr_eval_string(lr_eval_string("{s_quantities_{dyn}}")))>10){
			flag=1;
			lr_save_string(lr_eval_string(lr_eval_string("{s_catentryIDs_{dyn}}")),"f_catentryID");
			break;
		}
	}

	if (flag==0){
		lr_exit(LR_EXIT_MAIN_ITERATION_AND_CONTINUE,LR_AUTO);
		return 0;
	}
	
//	lr_output_message("Pavan Reached Here");

		web_add_auto_header("Accept", 
		"application/json, text/plain, */*");

	web_add_auto_header("Accept-Encoding", 
		"gzip,deflate");

	web_add_auto_header("Accept-Language", 
		"en-US");

	web_add_header("Origin", "file://");

	web_add_auto_header("User-Agent", 
		"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_auto_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");

	web_add_auto_header("catalogId", 
		"10551");

	web_add_auto_header("deviceType", 
		"App");

	web_add_auto_header("langId", 
		"-1");

	web_add_auto_header("storeId", 
		"10151");
	web_reg_find("TEXT/IC=orderId","SaveCount=storeId",LAST);  //appliedCoupons
	lr_start_transaction("T04_MobileProductAddToCart");
	lr_start_sub_transaction("T04_MobileProductAddToCart_addProductToCart","T04_MobileProductAddToCart");
	web_custom_request("addProductToCart", 
		"URL=https://{p_hostname}/api/tcpproduct/addProductToCart", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"Mode=HTTP", 
		"EncType=application/json", 
		"Body={\n  \"storeId\": \"10151\",\n  \"catalogId\": \"10551\",\n  \"langId\": \"-1\",\n  \"orderId\": \".\",\n  \"field2\": 0,\n  \"comment\": \"{f_catentryID}\",\n  \"calculationUsage\": \"-1,-2,-5,-6,-7\",\n  \"catEntryId\": \"{f_catentryID}\",\n  \"quantity\": \"1\",\n  \"requesttype\": \"ajax\"\n}", 
		LAST);
	
	if (atoi(lr_eval_string("{storeId}"))>0){
		lr_end_sub_transaction("T04_MobileProductAddToCart_addProductToCart", LR_AUTO); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}else{
		lr_end_sub_transaction("T04_MobileProductAddToCart_addProductToCart", LR_FAIL); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}	

	web_reg_find("TEXT/IC=appliedCoupons","SaveCount=couponsapply",LAST);  //appliedCoupons
	lr_start_sub_transaction("T04_MobileProductAddToCart_getAllCoupons","T04_MobileProductAddToCart");
	web_custom_request("getAllCoupons", 
		"URL=https://{p_hostname}/api/tcporder/getAllCoupons", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t2.inf", 
		LAST);
	
	if (atoi(lr_eval_string("{couponsapply}"))>0){
		lr_end_sub_transaction("T04_MobileProductAddToCart_getAllCoupons", LR_AUTO); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}else{
		lr_end_sub_transaction("T04_MobileProductAddToCart_getAllCoupons", LR_FAIL); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}	
	web_add_header("locStore", 
		"false");

	web_add_header("pageName", 
		"orderSummary");

	web_reg_find("TEXT/IC={","SaveCount=getOrderHistoryResponse",LAST);
	lr_start_sub_transaction ( "T04_MobileProductAddToCart_getOrderDetails", "T04_MobileProductAddToCart" );

	web_custom_request("getOrderDetails", 
		"URL=https://{p_hostname}/api/getOrderDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t3.inf", 
		LAST);
	if (atoi(lr_eval_string("{getOrderHistoryResponse}"))>0){
		lr_end_sub_transaction("T04_MobileProductAddToCart_getOrderDetails", LR_AUTO); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}else{
		lr_end_sub_transaction("T04_MobileProductAddToCart_getOrderDetails", LR_FAIL); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}	
	
	lr_end_transaction("T04_MobileProductAddToCart", LR_AUTO);
	web_cleanup_auto_headers();		
	return 0;
}
