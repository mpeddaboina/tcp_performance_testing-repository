ViewMyAccount()
{
		web_add_header("Accept", 
		"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");

	web_add_auto_header("Accept-Encoding", 
		"gzip,deflate");

	web_add_auto_header("Accept-Language", 
		"en-US");

	web_add_auto_header("User-Agent", 
		"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_auto_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");

	web_custom_request("orders", 
		"URL=https://{p_hostname}/us/account/orders", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"Mode=HTTP", 
		LAST);

	web_add_auto_header("Accept", 
		"application/json");

	web_add_auto_header("catalogId", 
		"10551");

	web_add_auto_header("deviceType", 
		"mobile");

	web_add_auto_header("langId", 
		"-1");

	web_add_auto_header("storeId", 
		"10151");

	web_custom_request("getXAppConfigValues", 
		"URL=https://{p_hostname}/api/tcporder/getXAppConfigValues", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=https://{p_hostname}/us/account/orders", 
		"Snapshot=t2.inf", 
		LAST);

	web_custom_request("getRegisteredUserDetailsInfo", 
		"URL=https://{p_hostname}/api/payment/getRegisteredUserDetailsInfo?_=1505951049865", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=https://{p_hostname}/us/account/orders", 
		"Snapshot=t3.inf", 
		LAST);
/*
	web_add_header("espotName", 
		"GlobalHeaderBannerAboveHeader,GlobalHeaderBannerWithinHeader,GlobalHeaderLogo,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,create_account_drawer,MinicartLoginBanner,MinicartRegisterBanner,drawer_myaccount_overview_banner,coupon_help"
		",bag_empty_banner,drawer_empty_bag,MinicartLoginFromFavoritesBanner,bag_ledger_promo_banner,gift_services_espot,CheckoutConfirmationBanner,CheckoutConfirmationMPRPromo,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1");

	web_custom_request("getESpot", 
		"URL=https://{p_hostname}/api/getESpot", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=https://{p_hostname}/us/account/orders", 
		"Snapshot=t4.inf", 
		LAST);
*/
	web_custom_request("getAllCoupons", 
		"URL=https://{p_hostname}/api/tcporder/getAllCoupons", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=https://{p_hostname}/us/account/orders", 
		"Snapshot=t5.inf", 
		LAST);

	web_add_header("calc", 
		"false");

	web_add_header("locStore", "True");
	web_add_header("pageName", "fullOrderInfo");

	web_custom_request("getOrderDetails", 
		"URL=https://{p_hostname}/api/getOrderDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=https://{p_hostname}/us/account/orders", 
		"Snapshot=t6.inf", 
		LAST);

	web_add_header("action", 
		"get");

	web_custom_request("getFavouriteStoreLocation", 
		"URL=https://{p_hostname}/api/tcporder/getFavouriteStoreLocation", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=https://{p_hostname}/us/account/orders", 
		"Snapshot=t7.inf", 
		LAST);
/*
	web_add_header("espotName", 
		"GlobalHeaderBannerAboveHeader,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1");

	web_custom_request("getESpot_2", 
		"URL=https://{p_hostname}/api/getESpot", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=https://{p_hostname}/us/account/orders", 
		"Snapshot=t8.inf", 
		LAST);
*/
	web_add_header("fromPage", 
		"checkout");

	web_custom_request("getAddressFromBook", 
		"URL=https://{p_hostname}/api/payment/getAddressFromBook", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=https://{p_hostname}/us/account/orders", 
		"Snapshot=t9.inf", 
		LAST);

	web_add_header("isRest", 
		"true");

	web_custom_request("getCreditCardDetails", 
		"URL=https://{p_hostname}/api/payment/getCreditCardDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=https://{p_hostname}/us/account/orders", 
		"Snapshot=t10.inf", 
		LAST);

	web_add_header("fromRest", 
		"true");

	web_custom_request("getPointsAndOrderHistory", 
		"URL=https://{p_hostname}/api/tcporder/getPointsAndOrderHistory", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=https://{p_hostname}/us/account/orders", 
		"Snapshot=t11.inf", 
		LAST);
	web_cleanup_auto_headers();	
	return 0;
}
