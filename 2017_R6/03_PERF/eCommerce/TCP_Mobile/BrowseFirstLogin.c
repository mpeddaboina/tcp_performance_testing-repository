BrowseFirstLogin()
{
	lr_think_time(TT);
	web_cleanup_auto_headers();		
	
		web_add_auto_header("Accept", 
		"application/json, text/plain, */*");

	web_add_auto_header("Accept-Encoding", 
		"gzip,deflate");

	web_add_auto_header("Accept-Language", 
		"en-US");

	web_add_auto_header("User-Agent", 
		"Mozilla/5.0 (Linux; Android 4.4.2; SM-G900F Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");

	web_add_auto_header("X-Requested-With", 
		"com.mobiquityinc.tcpdev");

	web_add_header("calc", 
		"false");

	web_add_auto_header("catalogId", 
		"10551");

	web_add_auto_header("deviceType", 
		"App");

	web_add_auto_header("langId", 
		"-1");

	web_add_header("locStore", 
		"true");

	web_add_header("pageName", 
		"fullOrderInfo");

	web_add_auto_header("storeId", 
		"10151");

	lr_start_transaction("T07a_Mobile_BrowseFirstLogin");

	web_reg_find("TEXT/IC=shippingCharge","SaveCount=shippingCharge",LAST);
	lr_start_sub_transaction ( "T07a_Mobile_BrowseFirstLogin_getOrderDetails", "T07a_Mobile_BrowseFirstLogin" );
	web_custom_request("getOrderDetails", 
		"URL=https://{p_hostname}/api/getOrderDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		LAST);
	if (atoi(lr_eval_string("{shippingCharge}"))>0){
		lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_getOrderDetails", LR_AUTO); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}else{
		lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_getOrderDetails", LR_FAIL); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}
	web_add_header("Origin", 
		"file://");
	web_reg_find("TEXT/IC=LoginSuccess","SaveCount=LoginSuccess",LAST);
	
	lr_start_sub_transaction ( "T07a_Mobile_BrowseFirstLogin_logon", "T07a_Mobile_BrowseFirstLogin" );
	web_custom_request("logon", 
		"URL=https://{p_hostname}/api/logon", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t2.inf", 
		"Mode=HTTP", 
		"EncType=application/json", 
		"Body={\n  \"reLogonURL\": \"TCPAjaxLogonErrorView\",\n  \"URL\": \"TCPAjaxLogonSuccessView\",\n  \"storeId\": \"10151\",\n  \"rememberCheck\": \"true\",\n  \"logonId1\": \"{p_userID}\",\n  \"logonPassword1\": \"Asdf!234\",\n  \"rememberMe\": true,\n  \"useTouchId\": false\n}", 
		LAST);
	  //"Body={\n  \"reLogonURL\": \"TCPAjaxLogonErrorView\",\n  \"URL\": \"TCPAjaxLogonSuccessView\",\n  \"storeId\": \"10151\",\n  \"rememberCheck\": \"true\",\n  \"logonId1\": \"johnoliver@childrensplace.com\",\n  \"logonPassword1\": \"!Scholastic99\",\n  \"rememberMe\": true,\n  \"useTouchId\": false\n}", 
//		"Body={\"reLogonURL\": \"TCPAjaxLogonErrorView\",\"URL\": \"TCPAjaxLogonSuccessView\",\"storeId\": \"10151\",\"rememberCheck\": \"true\",\"logonId1\": \"{p_userID}\",\"logonPassword1\": \"Asdf!234\",\"rememberMe\": true,\n  \"useTouchId\": false}", LAST);
if (atoi(lr_eval_string("{LoginSuccess}"))>0){
		lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_logon", LR_AUTO); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}else{
		lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_logon", LR_FAIL); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}
	  web_reg_find("TEXT/IC=resourceName","SaveCount=reguserdetails",LAST);
	lr_start_sub_transaction ( "T07a_Mobile_BrowseFirstLogin_getRegisteredUserDetailsInfo", "T07a_Mobile_BrowseFirstLogin" );
	web_custom_request("getRegisteredUserDetailsInfo", 
		"URL=https://{p_hostname}/api/payment/getRegisteredUserDetailsInfo", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t3.inf", 
		LAST);
	if (atoi(lr_eval_string("{reguserdetails}"))>0){
		lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_getRegisteredUserDetailsInfo", LR_AUTO); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}else{
		lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_getRegisteredUserDetailsInfo", LR_FAIL); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}
	
	web_add_header("fromRest", 
		"true");
	web_reg_find("TEXT/IC=getOrderHistoryResponse","SaveCount=getOrderHistoryResponse",LAST);
	lr_start_sub_transaction ( "T07a_Mobile_BrowseFirstLogin_getPointsAndOrderHistory", "T07a_Mobile_BrowseFirstLogin" );
	web_custom_request("getPointsAndOrderHistory", 
		"URL=https://{p_hostname}/api/tcporder/getPointsAndOrderHistory", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t4.inf", 
		LAST);
if (atoi(lr_eval_string("{getOrderHistoryResponse}"))>0){
		lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_getPointsAndOrderHistory", LR_AUTO); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}else{
		lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_getPointsAndOrderHistory", LR_FAIL); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}
	web_add_header("locStore", 
		"false");

	web_add_header("pageName", 
		"orderSummary");

	web_reg_find("TEXT/IC=shippingCharge","SaveCount=shippingCharge",LAST);
	lr_start_sub_transaction ( "T07a_Mobile_BrowseFirstLogin_getOrderDetails", "T07a_Mobile_BrowseFirstLogin" );
	web_custom_request("getOrderDetails", 
		"URL=https://{p_hostname}/api/getOrderDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		LAST);
	if (atoi(lr_eval_string("{shippingCharge}"))>0){
		lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_getOrderDetails", LR_AUTO); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}else{
		lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_getOrderDetails", LR_FAIL); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}

	web_add_header("action", 
		"get");
	web_reg_find("TEXT/IC={","SaveCount=getfavouriteStoreLocation",LAST);
	lr_start_sub_transaction ( "T07a_Mobile_BrowseFirstLogin_getFavouriteStoreLocation", "T07a_Mobile_BrowseFirstLogin" );
	web_custom_request("getFavouriteStoreLocation", 
		"URL=https://{p_hostname}/api/tcporder/getFavouriteStoreLocation/", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t6.inf", 
		LAST);
	if (atoi(lr_eval_string("{getfavouriteStoreLocation}"))>0){
		lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_getFavouriteStoreLocation", LR_AUTO); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}else{
		lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_getFavouriteStoreLocation", LR_FAIL); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}
	web_add_header("isRest", 
		"true");
web_reg_find("TEXT/IC=creditCardInfoJson","SaveCount=creditCardInfoJson",LAST);  //appliedCoupons
	lr_start_sub_transaction("T07a_Mobile_BrowseFirstLogin_getCreditCardDetails","T07a_Mobile_BrowseFirstLogin");
	web_custom_request("getCreditCardDetails", 
		"URL=https://{p_hostname}/api/payment/getCreditCardDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t7.inf", 
		LAST);
	if (atoi(lr_eval_string("{creditCardInfoJson}"))>0){
		lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_getCreditCardDetails", LR_AUTO); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}else{
		lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_getCreditCardDetails", LR_FAIL); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}
	web_custom_request("getAddressFromBook", 
		"URL=https://{p_hostname}/api/payment/getAddressFromBook", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t8.inf", 
		LAST);

	web_add_header("calc", 
		"false");

	web_add_header("locStore", 
		"true");

	web_add_header("pageName", 
		"fullOrderInfo");

		web_reg_find("TEXT/IC=shippingCharge","SaveCount=shippingCharge",LAST);
	lr_start_sub_transaction ( "T07a_Mobile_BrowseFirstLogin_getOrderDetails", "T07a_Mobile_BrowseFirstLogin" );
	web_custom_request("getOrderDetails", 
		"URL=https://{p_hostname}/api/getOrderDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t1.inf", 
		LAST);
	if (atoi(lr_eval_string("{shippingCharge}"))>0){
		lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_getOrderDetails", LR_AUTO); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}else{
		lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_getOrderDetails", LR_FAIL); //lr_start_sub_transaction ( "T01_MobileHomePage_getRegisteredUserDetailsInfo", "T01_MobileHomePage" );
	}
	
	lr_continue_on_error(1);
	
	lr_start_sub_transaction ( "T07a_Mobile_BrowseFirstLogin_getListofDefaultWishlist", "T07a_Mobile_BrowseFirstLogin" );
	web_custom_request("getListofDefaultWishlist", 
		"URL=https://{p_hostname}/api/getListofDefaultWishlist", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t10.inf", 
		"Body={\"productId\":[]}",
		LAST);
	lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_getListofDefaultWishlist", LR_AUTO); 
	
	lr_continue_on_error(0);
	
	web_add_header("externalId", 
		"286018");

	lr_start_sub_transaction ( "T07a_Mobile_BrowseFirstLogin_getListofDefaultWishlistbyId", "T07a_Mobile_BrowseFirstLogin" );
	web_custom_request("getListofDefaultWishlistbyId", 
		"URL=https://{p_hostname}/api/tcpproduct/getListofDefaultWishlistbyId", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t11.inf", 
		LAST);
	lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_getListofDefaultWishlistbyId", LR_AUTO); 

	lr_start_sub_transaction ( "T07a_Mobile_BrowseFirstLogin_getAllCoupons", "T07a_Mobile_BrowseFirstLogin" );
	web_custom_request("getAllCoupons", 
		"URL=https://{p_hostname}/api/tcporder/getAllCoupons", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t12.inf", 
		LAST);
	lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_getAllCoupons", LR_AUTO); 

	lr_start_sub_transaction ( "T07a_Mobile_BrowseFirstLogin_giftOptionsCmd", "T07a_Mobile_BrowseFirstLogin" );
	web_custom_request("giftOptionsCmd", 
		"URL=https://{p_hostname}/api/payment/giftOptionsCmd", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t13.inf", 
		LAST);
	lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_giftOptionsCmd", LR_AUTO); 

	web_add_auto_header("zipCode", 
		"07094");

	lr_start_sub_transaction ( "T07a_Mobile_BrowseFirstLogin_getShipmentMethods", "T07a_Mobile_BrowseFirstLogin" );
	web_custom_request("getShipmentMethods", 
		"URL=https://{p_hostname}/api/payment/getShipmentMethods", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t14.inf", 
		LAST);
	lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_getShipmentMethods", LR_AUTO); 

	lr_start_sub_transaction ( "T07a_Mobile_BrowseFirstLogin_getShipmentMethods_2", "T07a_Mobile_BrowseFirstLogin" );
	web_custom_request("getShipmentMethods_2", 
		"URL=https://{p_hostname}/api/payment/getShipmentMethods", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t15.inf", 
		LAST);
	lr_end_sub_transaction("T07a_Mobile_BrowseFirstLogin_getShipmentMethods_2", LR_AUTO); 
	lr_end_transaction("T07a_Mobile_BrowseFirstLogin", LR_AUTO);

	return 0;
}
