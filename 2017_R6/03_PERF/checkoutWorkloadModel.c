int CART_PAGE_DROP, LOGIN_PAGE_DROP, SHIP_PAGE_DROP, BILL_PAGE_DROP, REVIEW_PAGE_DROP; //Checkout Funnel ratio for DropCart Scripts.
//int LINK_TT, FORM_TT, TYPE_SPEED; //Think Time
int RATIO_CHECKOUT_GUEST, RATIO_CHECKOUT_LOGIN, RATIO_CHECKOUT_LOGIN_FIRST; // User Type
int OFFLINE_PLUGIN, VISA, MASTER, AMEX, PLCC, GIFT, DISCOVER, OFFLINE_PLUGIN; // Payment Method
int RATIO_PROMO_APPLY, RATIO_PROMO_MULTIUSE, RATIO_PROMO_SINGLEUSE, RATIO_PROMO_COUPON_REMOVE, RATIO_REDEEM_LOYALTY_POINTS, RATIO_PROMO_APPLY_ALL; // Promo Type 
int RATIO_REGISTER, RATIO_UPDATE_QUANTITY, RATIO_DELETE_PROMOCODE, RATIO_DELETE_ITEM, RATIO_SELECT_COLOR, RATIO_ORDER_HISTORY, RATIO_POINTS_HISTORY, RATIO_RESERVATION_HISTORY, RATIO_RESERVATION_WISHLIST;// Registration, Update_Quantity, Delete_Item from cart
int RATIO_CART_MERGE, RATIO_DROP_CART, RATIO_WISHLIST, cartMerge, currentCartSize; RATIO_BUILDCART_DRILLDOWN, USE_LOW_INVENTORY, RATIO_KIDS_SQUAD;
int	RATIO_WL_VIEW, RATIO_WL_CREATE,	RATIO_WL_DELETE, RATIO_WL_CHANGE, RATIO_WL_DELETE_ITEM, RATIO_WL_ADD_ITEM, RATIO_WL_ADD_TO_CART;
int MOVE_FROM_CART_TO_WISHLIST, MOVE_FROM_WISHLIST_TO_CART, RATIO_BUILDCART_LARGE;
int RATIO_ACCOUNT_OVERVIEW, RATIO_ACCOUNT_REWARDS, RATIO_ORDER_HISTORY, RATIO_RESERVATION_HISTORY, RATIO_ADDRESSBOOK, RATIO_PAYMENT, RATIO_PROFILE, RATIO_PREFERENCES, RATIO_LOGOUT, RATIO_FORGOT_PASSWORD,ONLINE_ORDER_SUBMIT, LOGINFIRSTCHECK; RATIO_ADDBIRTHDAYSAVINGS, RATIO_RTPS, RATIO_WIC, RATIO_ADDTOCART_PLP;
  
	//set drop point ratio for checkout funnel. Sum must be 100. All variables must either 0 or +ve value.
	// Reconciled with CM access log checkout funnel
/*	LOGIN_PAGE_DROP = 0; //30; //11;
	SHIP_PAGE_DROP = 0; //30; //46;
	BILL_PAGE_DROP = 80; //27; //23; //13;
	//REVIEW_PAGE_DROP = 100; 
*/	
	LOGIN_PAGE_DROP = 30; //11;
	SHIP_PAGE_DROP = 30; //46;
	BILL_PAGE_DROP = 27; //23; //13;
	//REVIEW_PAGE_DROP = 100; 
	 
	//set user checkout type T10 transaction. Sum must be 100. All variables must have either 0 or +ve value.
	RATIO_CHECKOUT_LOGIN = 90; //70;
	RATIO_CHECKOUT_GUEST = 10; //30;

	//Ratio for Drop Cart vusers
	RATIO_DROP_CART = 60;

    //set users checkout that starts with login flow
    RATIO_CHECKOUT_LOGIN_FIRST = 0;

	//  11/19/2017
	RATIO_ORDER_STATUS = 0; //50; //5; //25; //100; //50;

    //sets a percent of users to drilldown before selecting a product when building the cart.
    RATIO_BUILDCART_DRILLDOWN = 100; //25; //20; //25;
	
	USE_LOW_INVENTORY = 5;
	RATIO_BUILDCART_LARGE = 0; //1;
	
	//PayMethod
	OFFLINE_PLUGIN = 100;
	VISA = 0;
	MASTER = 0;
	AMEX = 0;
	PLCC = 0;
	GIFT = 0;
	DISCOVER = 0;
	PAYPAL = 0;

	//PromoType
//	RATIO_PROMO_APPLY = 100;
//	RATIO_PROMO_MULTIUSE = 100; 
//	RATIO_PROMO_SINGLEUSE = 100;
	RATIO_PROMO_APPLY = 35; //70; 
	RATIO_PROMO_MULTIUSE = 100; //95; 
	RATIO_PROMO_SINGLEUSE = 0; //5; 
	RATIO_PROMO_APPLY_ALL = 0; //1; 
	RATIO_REDEEM_LOYALTY_POINTS = 15;
	
	//Ratio for deleting a promo code
	RATIO_DELETE_PROMOCODE = 0;

	//Ratio for Cart Merge
	RATIO_CART_MERGE = 0; //50;	

//% of Guest user completing checkout that also register
	RATIO_REGISTER = 100;

	//Ratio for Update Quantity
	RATIO_UPDATE_QUANTITY = 100;

	//Ratio for Delete an item from cart
	RATIO_DELETE_ITEM = 100; //60;

	//Ratio for Selecting a Color
	RATIO_SELECT_COLOR = 50;
	///RATIO_ORDER_HISTORY + RATIO_POINTS_HISTORY + RATIO_RESERVATION_HISTORY // All this should be total 100
	//Ratio for Order history / View Order Detail / Reservation

	//RATIO_RESERVATION_HISTORY = 10; 
	//  11/19/2017
//	RATIO_POINTS_HISTORY = 30; //50;
	//RATIO_ORDER_HISTORY = 60; //50;


	//Ratio for Wishlist
	RATIO_WISHLIST		 = 50; //10;
/*
	RATIO_WL_CREATE      = 0;
	RATIO_WL_DELETE      = 0; 
	RATIO_WL_CHANGE      = 0; 
	RATIO_WL_DELETE_ITEM = 0;
	RATIO_WL_ADD_ITEM    = 0; 
	RATIO_WL_ADD_TO_CART = 100;
*/	
	RATIO_WL_CREATE      = 35; //60;
	RATIO_WL_DELETE      = 2; 
	RATIO_WL_CHANGE      = 3; 
	RATIO_WL_DELETE_ITEM = 5;
	RATIO_WL_ADD_ITEM    = 1; 
	RATIO_WL_ADD_TO_CART = 25;

	MOVE_FROM_CART_TO_WISHLIST = 25;
	MOVE_FROM_WISHLIST_TO_CART = 30;

	RATIO_ACCOUNT_OVERVIEW 		= 40; //100;

	RATIO_ACCOUNT_REWARDS 		= 5; 
	RATIO_ORDER_HISTORY			= 80;
	RATIO_RESERVATION_HISTORY	= 0;
	RATIO_ADDRESSBOOK			= 5;
	RATIO_PAYMENT				= 5;
	RATIO_PROFILE				= 5;
	RATIO_PREFERENCES			= 5;

	RATIO_ADDBIRTHDAYSAVINGS = 20;
	RATIO_LOGOUT			= 10;
	RATIO_FORGOT_PASSWORD	= 10;
	RATIO_KIDS_SQUAD		= 5;
	ONLINE_ORDER_SUBMIT		= 0;
	LOGINFIRSTCHECK			= 0;
 
	RATIO_RTPS = 0;//50;
	RATIO_WIC  = 0;//50;
	RATIO_ADDTOCART_PLP = 28; //50;