#include "..\..\browseWorkloadModel.c"

int index , length , i, randomPercent, isLoggedIn;
char *searchString ;
char *nav_by ; // T02 - Category Navigation selection
char *drill_by ; // T03 - Facet / SubCategory drill selection
char *sort_by; // T03 - Sort selection
char *product_by; // T04 - Product Display Method
//int form_TT, link_TT, typeSpeed_TT;
typedef long time_t;
time_t t;
//int authcookielen , authtokenlen ;
//authcookielen = 0;

void addHeader()
{
	web_add_header("storeId", lr_eval_string("{storeId}") );
	web_add_header("catalogId", lr_eval_string("{catalogId}") );
	web_add_header("langId", "-1");

}

void webURL(char *Url, char *pageName)
{
	lr_save_string(Url, "Url");
	lr_save_string(pageName, "pageName");
	web_url ( lr_eval_string("{pageName}") ,
    	     "URL={Url}" ,
             "Resource=0" ,
             "Mode=HTML" ,
             LAST ) ;
}//end webURL

void api_getESpot_second(){
	if (ESPOT_FLAG == 1) {
		if (isLoggedIn == 1) { //turned off for akamai caching
			addHeader();
			web_add_header("espotName","GlobalHeaderBannerAboveHeader,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1");
			web_reg_find("TEXT/IC=espotName", "SaveCount=apiCheck", LAST);
			//lr_start_transaction("T99_API_Second_getESpot");
			web_custom_request("getESpots",
				"URL=https://{api_host}/getESpot",
				"Method=GET",
				"Resource=0",
				"RecContentType=application/json",
				LAST);
			//if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			//	lr_end_sub_transaction("T99_API_Second_getESpot", LR_FAIL);
			//else
			//	lr_end_sub_transaction("T99_API_Second_getESpot", LR_AUTO);
		}
	}
}

void TCPGetWishListForUsersView()
{
	lr_start_transaction("T25_Common_S01_TCPGetWishListForUsersView");

	web_custom_request("TCPGetWishListForUsersView",
		"URL=http://{host}/webapp/wcs/stores/servlet/TCPGetWishListForUsersView?tcpCallBack=jQuery1113014590106982485151_1473881708524&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&sortBy=&_=1473881708525",
		"Method=GET",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		"EncType=application/x-www-form-urlencoded",
		LAST);

	lr_end_sub_transaction("T25_Common_S01_TCPGetWishListForUsersView", LR_AUTO);

}


int tcp_api(char *apiCall, char *method)
{	int rc;
	lr_param_sprintf ( "apiTransactionName" , "T30_API %s" , apiCall ) ;
	lr_param_sprintf ( "apiURL" , "https://%s/%s" , lr_eval_string("{api_host}"), apiCall ) ;
	lr_param_sprintf ( "apiMethod" , "%s" , method ) ;
	web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", LAST); //"errorCode": "",\n
	web_reg_save_param("errorMessage", "LB=\"errorMessage\": \"", "RB=\",\n", "NotFound=Warning", LAST); //"errorMessage": "",\n

	lr_start_transaction(lr_eval_string("{apiTransactionName}"));

	web_custom_request(lr_eval_string(apiCall),
		"URL={apiURL}",
		"Method={apiMethod}",
		"RecContentType=text/html",
		"Mode=HTML",
		LAST);
	// rc = web_get_int_property(HTTP_INFO_RETURN_CODE);

//	if (rc == 200) {
		if ( strcmp( lr_eval_string("{errorCode}") ,"") != 0 && strcmp(lr_eval_string("{errorMessage}") ,"") != 0 ) {
			lr_fail_trans_with_error( lr_eval_string("{apiTransactionName} Failed with  Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_transaction( lr_eval_string("{apiTransactionName}"), LR_FAIL);
		}
		else
			lr_end_transaction( lr_eval_string("{apiTransactionName}"), LR_PASS);
	// }
	// else {
		// lr_fail_trans_with_error( lr_eval_string("{apiTransactionName} Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
		// lr_end_transaction( lr_eval_string("{apiTransactionName}"), LR_FAIL);
	// }
	return 0;
}

void callAPI()
{
	//lr_continue_on_error(0);

	if ( isLoggedIn == 1 && strcmp(lr_eval_string("{callerId}"), "home") != 0) {
		tcp_api2("tcpstore/getListofDefaultWishlist", "POST", lr_eval_string("{mainTransaction}") );
	}

	web_add_header("action", "get");
	tcp_api2("tcporder/getFavouriteStoreLocation", "GET", lr_eval_string("{mainTransaction}") );

//	if (strcmp(lr_eval_string("{callerId}"), "home") != 0)
//	{
		web_add_header("locStore", "False");
		web_add_header("pageName", "orderSummary");
//		web_reg_save_param("cartCount", "LB=CartCount\": ", "RB=,", "NotFound=Warning", LAST);			//"CartCount": 1,\n
		tcp_api2("getOrderDetails", "GET", lr_eval_string("{mainTransaction}") );                      // api_getOrderDetails();

//	}

	web_reg_save_param ( "userId" , "LB=\"userId\": \"" , "RB=\",\n" , "NotFound=Warning", LAST ) ; // "userId": "202684824"
	tcp_api2("payment/getRegisteredUserDetailsInfo", "GET",  lr_eval_string("{mainTransaction}") ); // api_getRegisteredUserDetailsInfo();

	if (ESPOT_FLAG == 1)
	{
		tcp_api2("getESpot", "GET", lr_eval_string("{mainTransaction}") );                             // turned off for akamai caching
		if ( isLoggedIn == 1 ) {
			tcp_api2("getESpot", "GET", lr_eval_string("{mainTransaction}") );                             // turned off for akamai caching
		}
	}

//	tcp_api2("tcporder/getAllCoupons", "GET",  lr_eval_string("{mainTransaction}") ); //got replaced by getPriceDetails
	web_add_header("partNumber", "2089039_1361-2083215_10-2089763_01-2083216_10-2088607_10-2083213_01");
	tcp_api2("tcpproduct/getPriceDetails", "GET",  lr_eval_string("{mainTransaction}") );


	if (strcmp(lr_eval_string("{callerId}"), "productDisplay") == 0)
	{
		web_reg_save_param("atc_catentryIds", "LB=catentryId\": \"", "RB=\",", "ORD=All", "NotFound=Warning", LAST); //"catentryId": "735396",
		web_reg_save_param("atc_quantity"  ,  "LB=quantity\": \"", "RB=.0\",", "ORD=All", "NotFound=Warning", LAST); //"quantity": "99999.0",
		web_add_header( "productId", "{pdpProdID}" ); // only for Bopis debugging in uatlive1
//		web_add_header( "productId", lr_eval_string("{generalProductId}") );
		tcp_api2("tcpproduct/getSKUInventoryandProductCounterDetails", "GET",  lr_eval_string("{mainTransaction}") );
//		tcp_api2("getESpot", "GET", lr_eval_string("{mainTransaction}") );                             // 03202018
	}
	//lr_continue_on_error(1);
}

void call_OPTIONS(char *apiCall)
{
	lr_param_sprintf ( "apiURL" , "https://%s/%s" , lr_eval_string("{api_host}"), apiCall ) ;
	web_add_header("Accept", "*/*");
	web_add_header("Access-Control-Request-Method", "GET");
	web_add_header("Origin", "https://{host}");

	if (strcmp(apiCall, "payment/getPointsService") == 0 ){

		web_add_header("Access-Control-Request-Headers", "catalogid,langid,storeid");

	}else if (strcmp(apiCall, "getOrderDetails") == 0 ){

		web_add_header("Access-Control-Request-Headers", "calc,catalogid,langid,locstore,pagename,storeid");

	}else if (strcmp(apiCall, "getESpot") == 0 ){

		web_add_header("Access-Control-Request-Headers", "catalogid,devicetype,espotname,langid,storeid");

	}else if (strcmp(apiCall, "tcporder/getAllCoupons") == 0 ){

		web_add_header("Access-Control-Request-Headers", "catalogid,langid,storeid");

	}else if (strcmp(apiCall, "payment/getRegisteredUserDetailsInfo") == 0 ){

		web_add_header("Access-Control-Request-Headers", "catalogid,langid,storeid");

	}else if (strcmp(apiCall, "payment/giftOptionsCmd") == 0 ){

		web_add_header("Access-Control-Request-Headers", "catalogid,langid,storeid");

	}else if (strcmp(apiCall, "payment/getCreditCardDetails") == 0 ){

		web_add_header("Access-Control-Request-Headers", "catalogid,isrest,langid,storeid");

	}else if (strcmp(apiCall, "payment/getAddressFromBook") == 0 ){

		web_add_header("Access-Control-Request-Headers", "catalogid,frompage,langid,storeid");

	}
/*
	if (strcmp(apiCall, "payment/getPointsService") == 0 && strcmp(lr_eval_string("{payment/getPointsService}"), "0") == 0){

		web_add_header("Access-Control-Request-Headers", "catalogid,langid,storeid");
		executeOptions();
		lr_save_string("1", "payment/getPointsService");

	}else if (strcmp(apiCall, "getOrderDetails") == 0 ){

		web_add_header("Access-Control-Request-Headers", "calc,catalogid,langid,locstore,pagename,storeid");
		executeOptions();
		lr_save_string("1", "getOrderDetails");

	}else if (strcmp(apiCall, "getESpot") == 0 ){

		web_add_header("Access-Control-Request-Headers", "catalogid,devicetype,espotname,langid,storeid");
		executeOptions();
		lr_save_string("1", "getESpot");

	}else if (strcmp(apiCall, "tcporder/getAllCoupons") == 0 ){

		web_add_header("Access-Control-Request-Headers", "catalogid,langid,storeid");
		executeOptions();
		lr_save_string("1", "tcporder/getAllCoupons");

	}else if (strcmp(apiCall, "payment/getRegisteredUserDetailsInfo") == 0 ){

		web_add_header("Access-Control-Request-Headers", "catalogid,langid,storeid");
		executeOptions();
		lr_save_string("1", "payment/getRegisteredUserDetailsInfo");

	}else if (strcmp(apiCall, "payment/giftOptionsCmd") == 0 ){

		web_add_header("Access-Control-Request-Headers", "catalogid,langid,storeid");
		executeOptions();
		lr_save_string("1", "payment/giftOptionsCmd");

	}else if (strcmp(apiCall, "payment/getCreditCardDetails") == 0 ){

		web_add_header("Access-Control-Request-Headers", "catalogid,isrest,langid,storeid");
		executeOptions();
		lr_save_string("1", "payment/getCreditCardDetails");

	}else if (strcmp(apiCall, "payment/getAddressFromBook") == 0 ){

		web_add_header("Access-Control-Request-Headers", "catalogid,frompage,langid,storeid");
		executeOptions();
		lr_save_string("1", "payment/getAddressFromBook");

	}
*/
}

void executeOptions()
{/*
		web_custom_request("OPTIONS",
			"URL={apiURL}",
			"Method=OPTIONS",
			"Resource=0",
			LAST);
*/
}

int tcp_api2(char *apiCall, char *method, char *mainTransaction)
{	int rc;

	lr_save_string("", "body");

	if (strcmp(apiCall, "tcpstore/getListofDefaultWishlist") == 0  )
	{
		lr_save_string("[","CheckString");
		lr_save_string("{\"productId\":[\"624004\"]}", "body");
	}

	if (strcmp(apiCall, "getESpot") != 0 )
	{
		web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", LAST); //"errorCode": "",\n
		web_reg_save_param("errorMessage", "LB=\"errorMessage\": \"", "RB=\",\n", "NotFound=Warning", LAST); //"errorMessage": "",\n
	}

	if (strcmp(apiCall, "payment/getRegisteredUserDetailsInfo") == 0 )
	{
		web_reg_save_param ( "addressId" , "LB=\"addressId\": \"" , "RB=\",\n" , "ORD=1", "NotFound=Warning", LAST ) ; //"addressId": "1398789",\n
		web_reg_save_param ( "addressIdAll" , "LB=\"addressId\": \"" , "RB=\",\n" , "ORD=ALL", "NotFound=Warning", LAST ) ; //"addressId": "1398789",\n
		lr_save_string("resourceName","CheckString");
	}
	if (strcmp(apiCall, "payment/getPointsService") == 0 ){
		lr_save_string("LoyaltyWebsiteInd","CheckString");
	}else if (strcmp(apiCall, "getOrderDetails") == 0 ){
		lr_save_string("{","CheckString");
	}else if (strcmp(apiCall, "getESpot") == 0 ){
		lr_save_string("espotName","CheckString");
		//Pavan Dusi added the following line 0626
		                          //GlobalHeaderBannerAboveHeader,GlobalHeaderBannerWithinHeader,GlobalHeaderLogo,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,create_account_drawer,MinicartLoginBanner,MinicartRegisterBanner,drawer_myaccount_overview_banner,coupon_help,bag_empty_banner,drawer_empty_bag,MinicartLoginFromFavoritesBanner,bag_ledger_promo_banner,gift_services_espot,CheckoutConfirmationBanner,CheckoutConfirmationMPRPromo
		web_add_header("espotName","GlobalHeaderBannerAboveHeader,GlobalHeaderBannerWithinHeader,GlobalHeaderLogo,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,create_account_drawer,MinicartLoginBanner,MinicartRegisterBanner,drawer_myaccount_overview_banner,coupon_help,bag_empty_banner,drawer_empty_bag,MinicartLoginFromFavoritesBanner,bag_ledger_promo_banner,gift_services_espot,CheckoutConfirmationBanner,CheckoutConfirmationMPRPromo,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1");
		//web_add_header("espotName","GlobalHeaderBannerAboveHeader,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1");
		web_add_header("deviceType","desktop");

	}else if (strcmp(apiCall, "tcporder/getAllCoupons") == 0 ){
		lr_save_string("availableCoupons","CheckString");
	}else if (strcmp(apiCall, "tcpproduct/getPriceDetails") == 0 ){
		lr_save_string("{","CheckString");
	}else if (strcmp(apiCall, "tcporder/getXAppConfigValues") == 0 ){
		lr_save_string("xAppAttrValues","CheckString");
	}else if (strcmp(apiCall, "tcpproduct/getProductviewbyCategory") == 0 ){
		lr_save_string("recordSetTotalMatches","CheckString");
	}else if (strcmp(apiCall, "tcpproduct/getSKUInventoryandProductCounterDetails") == 0 ){
		lr_save_string("getAllSKUInventoryByProductId","CheckString");
	}else if (strcmp(apiCall, "tcporder/getFavouriteStoreLocation") == 0 ){
		lr_save_string("{","CheckString");
	}else if (strcmp(apiCall, "tcpproduct/getWishListbyId") == 0 ){
		lr_save_string("accessSpecifier","CheckString");
	}else if (strcmp(apiCall, "tcpstore/getListofWishList") == 0 ){
		lr_save_string("giftListExternalIdentifier","CheckString");
	}else if (strcmp(apiCall, "tcpproduct/getTopCategories") == 0 ){
		lr_save_string("{","CheckString");
	}


	if (API_SUB_TRANSACTION_SWITCH == 1) //1 = ON == Let the sub transactions show on the LRA Summary
	{
		lr_param_sprintf ( "apiMainTransactionName" , "%s" , mainTransaction ) ;

		if (strcmp(lr_eval_string("{apiMainTransactionName}"),"") !=0 ) {
			lr_param_sprintf ( "apiTransactionName" , "%s_%s" , lr_eval_string("{apiMainTransactionName}"), apiCall ) ;
			lr_start_sub_transaction(lr_eval_string("{apiTransactionName}"), lr_eval_string("{apiMainTransactionName}"));
		} else {
			lr_param_sprintf ( "apiTransactionName" , "%s_%s" , lr_eval_string("{noBrowseTransaction}"), apiCall ) ;
			lr_start_transaction(lr_eval_string("{apiTransactionName}"));
		}
/*
		if (strcmp(lr_eval_string("{noBrowseTransaction}"),"") !=0 ) {
			lr_param_sprintf ( "apiTransactionName" , "%s_%s" , lr_eval_string("{noBrowseTransaction}"), apiCall ) ;
			lr_start_transaction(lr_eval_string("{apiTransactionName}"));
		} else {
			lr_param_sprintf ( "apiTransactionName" , "%s_%s" , lr_eval_string("{apiMainTransactionName}"), apiCall ) ;
			lr_start_sub_transaction(lr_eval_string("{apiTransactionName}"), lr_eval_string("{apiMainTransactionName}"));
		}
*/
		#if OPTIONSENABLED
			if (isLoggedIn == 0)
			{
				call_OPTIONS(apiCall);
			}
			else {
				if (BROWSE_OPTIONS_FLAG == 0)
					call_OPTIONS(apiCall);

			}
		#endif

		lr_param_sprintf ( "apiURL" , "https://%s/%s" , lr_eval_string("{api_host}"), apiCall ) ;
		lr_param_sprintf ( "apiMethod" , "%s" , method ) ;

		web_add_header("storeId", lr_eval_string("{storeId}") );
		web_add_header("catalogId", lr_eval_string("{catalogId}") );
		web_add_header("langId", "-1");
		web_reg_find("TEXT/IC={CheckString}","Fail=NotFound", "SaveCount=Mycount",LAST);

		if (strlen(lr_eval_string("{body}")) > 0 ) {
			web_custom_request(lr_eval_string(apiCall),
				"URL={apiURL}",
				"Method={apiMethod}",
				"RecContentType=text/html",
				"Mode=HTML",
				"Body={body}",
				LAST);
		} else {
			web_custom_request(lr_eval_string(apiCall),
				"URL={apiURL}",
				"Method={apiMethod}",
				"RecContentType=text/html",
				"Mode=HTML",
				LAST);
		}

//		if ( strcmp( lr_eval_string("{errorCode}") ,"") != 0 && strcmp(lr_eval_string("{errorMessage}") ,"") != 0  && (atoi(lr_eval_string("{Mycount}"))==0) )
		if ( atoi(lr_eval_string("{Mycount}")) ==0 )
		{
			lr_fail_trans_with_error( lr_eval_string("{apiTransactionName} Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			if (strcmp(lr_eval_string("{apiMainTransactionName}"),"") !=0 )
				lr_end_sub_transaction( lr_eval_string("{apiTransactionName}"), LR_FAIL);
			else
				lr_end_transaction( lr_eval_string("T30_API {apiTransactionName}"), LR_FAIL);
		}
		else
		{
			if (atoi(lr_eval_string("{Mycount}")) > 0 )  {
				if (strcmp(lr_eval_string("{apiMainTransactionName}"),"") !=0 )
					lr_end_sub_transaction( lr_eval_string("{apiTransactionName}"), LR_AUTO);
				else
					lr_end_transaction( lr_eval_string("T30_API {apiTransactionName}"), LR_AUTO);
			}
			else{
				if (strcmp(lr_eval_string("{apiMainTransactionName}"),"") !=0 )
					lr_end_sub_transaction( lr_eval_string("{apiTransactionName}"), LR_FAIL);
				else
					lr_end_transaction( lr_eval_string("T30_API {apiTransactionName}"), LR_FAIL);
			}
		}

	}
	else
	{
		lr_param_sprintf ( "apiTransactionName" , "%s_%s" , lr_eval_string("{mainTransaction}"), apiCall ) ;
		lr_start_transaction(lr_eval_string("{apiTransactionName}"));
		lr_param_sprintf ( "apiURL" , "https://%s/%s" , lr_eval_string("{api_host}"), apiCall ) ;
		lr_param_sprintf ( "apiMethod" , "%s" , method ) ;

		web_add_header("storeId", lr_eval_string("{storeId}") );
		web_add_header("catalogId", lr_eval_string("{catalogId}") );
		web_add_header("langId", "-1");
		web_reg_find("TEXT/IC={CheckString}","Fail=NotFound", "SaveCount=Mycount",LAST);

		web_custom_request(lr_eval_string(apiCall),
			"URL={apiURL}",
			"Method={apiMethod}",
			"RecContentType=text/html",
			"Mode=HTML",
//			"Body={body}",
			LAST);

		if ( atoi(lr_eval_string("{Mycount}")) ==0 )
		{
			lr_fail_trans_with_error( lr_eval_string("{apiTransactionName} Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_transaction( lr_eval_string("{apiTransactionName}"), LR_FAIL);
		}
		else
		{
			if (atoi(lr_eval_string("{Mycount}")) > 0 )  {
				lr_end_transaction( lr_eval_string("{apiTransactionName}"), LR_AUTO);
			}
			else {
				lr_end_transaction( lr_eval_string("{apiTransactionName}"), LR_FAIL);
			}
		}
/*

		rc = web_get_int_property(HTTP_INFO_RETURN_CODE);

		if (rc == 200) {
			if ( strcmp( lr_eval_string("{errorCode}") ,"") != 0 && strcmp(lr_eval_string("{errorMessage}") ,"") != 0 ) {
				lr_fail_trans_with_error( lr_eval_string("{apiTransactionName} Failed with  Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
				lr_end_transaction( lr_eval_string("T30_API {apiTransactionName}"), LR_FAIL);
			}
			else {
				lr_end_transaction( lr_eval_string("T30_API {apiTransactionName}"), LR_PASS);
			}
		}
		else {
			lr_fail_trans_with_error( lr_eval_string("T30_API {apiTransactionName} Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_transaction( lr_eval_string("T30_API {apiTransactionName}"), LR_FAIL);
		}
	}
*/
	}
	return 0;
}

webURL2(char *Url, char *pageName, char *textCheck)
{
	lr_save_string(Url, "Url");
	lr_save_string(pageName, "pageName");
	lr_save_string(textCheck, "textCheck");
	web_reg_find("SaveCount=txtCheckCount", "Text/IC={textCheck}", LAST);

	web_reg_find("SaveCount=noProducts", "Text/IC=We're sorry, there are no products available for purchase at this time. Please check back later.", LAST);

	web_url ( lr_eval_string("{pageName}") ,
    	     "URL={Url}" ,
             "Resource=0" ,
             "Mode=HTML" ,
             LAST ) ;

	if ( atoi(lr_eval_string("{txtCheckCount}")) > 0 )
		return LR_PASS;
	else
		return LR_FAIL;

}//end webURL


void endIteration()
{
	//lr_message ( "in endIteration" ) ;
}

void homePageCorrelations()
{
	int categoryIndex = 0;

	web_reg_save_param ( "categories" ,
//				 "LB=<a href=\"http://{host}{store_home_page}c/" ,  //uatlive1
				 "LB=<a href=\"{store_home_page}c/" ,  //perflive
				 "RB=\" class=\"navigation-level-one-link" ,
				 "ORD=ALL" , "Convert=HTML_TO_TEXT" , "NotFound=Warning", LAST ) ;


	if ( strcmp(lr_eval_string("{storeId}"), "10152") == 0 ) {
		index = rand () % 6 + 1 ;
	} else {
		index = rand () % 7 + 1 ;
	}

	switch(index)
	{
		case 1: web_reg_save_param_regexp ("ParamName=subcategory",	"RegExp=navigationTree.*?\"Girl\"(.*?)Toddler Girl\"",  "NotFound=Warning", SEARCH_FILTERS, "RequestUrl=*", LAST ); break;
		case 2: web_reg_save_param_regexp ("ParamName=subcategory", "RegExp=navigationTree.*?\"Boy\"(.*?)\"Toddler Boy\"",  "NotFound=Warning", SEARCH_FILTERS, "RequestUrl=*", LAST ); break;
		case 3: web_reg_save_param_regexp ("ParamName=subcategory", "RegExp=navigationTree.*?\"Toddler Boy\"(.*?)\"Baby\"", "NotFound=Warning", SEARCH_FILTERS, "RequestUrl=*", LAST ); break;
		case 4: web_reg_save_param_regexp ("ParamName=subcategory",	"RegExp=navigationTree.*?\"Baby\"(.*?)\"Shoes\"",       "NotFound=Warning", SEARCH_FILTERS, "RequestUrl=*", LAST ); break;
		case 5: web_reg_save_param_regexp ("ParamName=subcategory",	"RegExp=navigationTree.*?\"Baby\"(.*?)\"Shoes\"",	    "NotFound=Warning", SEARCH_FILTERS, "RequestUrl=*", LAST ); break;
		case 6: web_reg_save_param_regexp ("ParamName=subcategory", "RegExp=navigationTree.*?\"Shoes\"(.*?)\"Accessories\"","NotFound=Warning", SEARCH_FILTERS, "RequestUrl=*",	LAST ); break;
		case 7: web_reg_save_param_regexp ("ParamName=subcategory", "RegExp=navigationTree.*?\"Accessories\"(.*?)}]]}]",    "NotFound=Warning", SEARCH_FILTERS, "RequestUrl=*",	LAST ); break;
		default: break;
	}

}

void homePageCorrelations_OLD() //correlations needed for category navigations
{
/*   	web_reg_save_param ( "catalogId" ,
	                     "LB=catalogId=" ,
	                     "RB=&",
						 "ORD=1",
	                     "NotFound=Warning", LAST ) ;
*/

/*
	web_reg_save_param_regexp (	"ParamName=categories",
						"RegExp=<li class=\"navigation-level-one\" data-reactid=\"[0-9]*\"><a href=\"(.*?)\"",
						"Ordinal=All",
						SEARCH_FILTERS,
						"RequestUrl=*",	LAST );
*/

	web_reg_save_param ( "categories" ,
				 "LB=\t\t\t\t<a href=\"http://{host}{store_home_page}c/" ,
				 "RB=\"" ,
				 "ORD=ALL" , "Convert=HTML_TO_TEXT" , "NotFound=Warning", LAST ) ;

/*
	web_reg_save_param ( "storeId" ,
	                     "LB=<meta name=\"CommerceSearch\" content=\"storeId_=" ,
	                     "RB=\"",
						 "ORD=1",
	                     "NotFound=Warning", LAST ) ;
*/
	/*web_reg_save_param ( "categories" ,
				 "LB=\t\t\t\t<a href=\"http://{host}{store_home_page}c/" ,
				 "RB=\"" ,
				 "ORD=ALL" , "Convert=HTML_TO_TEXT" , "NotFound=Warning", LAST ) ;
		*/

/*		web_reg_save_param_json(
        "ParamName=categories",
        "QueryString=$.headerInfo[:1].*.categoryAttributes.url",
        "SelectAll=Yes",
        "SEARCH_FILTERS",
        "Scope=Body",
        "LAST");
*/

/******* This is commented out by PD on 03/07/2017 due to change in workflow. Henceforth from R2, clearances will be categories_6
				 web_reg_save_param ( "clearances" ,
						 "LB=<a href=\"http://{host}/shop/SearchDisplay?" , //works in prod and perf
						 "RB=\"" ,
						 "ORD=ALL" , "Convert=HTML_TO_TEXT" , "NotFound=Warning", LAST ) ;
*******/
	// include correlation for Place Shop (in PERF environment)

/**********************OBSOLETE: 03/07/2017 The following three web_reg_save_params are being commented**************************************/
/*
	web_reg_save_param ( "searchAutoSuggestURL" ,
						 "LB=setAutoSuggestURL('",
						 "RB='" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=Warning", LAST ) ;

	web_reg_save_param ( "searchCachedSuggestionsURL" ,
						 "LB=setCachedSuggestionsURL('" ,
						 "RB='" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=Warning", LAST ) ;

//						 <a href="http://tcp-perf.childrensplace.com/shop/us/content/mvp-shop-spring-2014"
   	web_reg_save_param ( "placeShopCategory" ,
						 "LB={store_home_page}content/" ,
	                     "RB=\"",
						 "ORD=All",
	                     "NotFound=Warning", LAST ) ;
*/
}//end homePageCorrelations()

/*
void categoryPageCorrelations() //correlations needed for facet and subcategory navigations // rhy 08242017
{

   	web_reg_save_param ( "subcategories" ,
						 "LB=navigation-level-two-link\" href=\"http://{host}{store_home_page}c/" ,
	                     "RB=/\"",
						 "ORD=All",
	                     "NotFound=Warning", LAST ) ;


}//end categoryPageCorrelations()
*/

void categoryPageCorrelationsR5() //correlations needed for facet and subcategory navigations
{

	web_reg_save_param_regexp( "ParamName=subcategories" ,
								"RegExp=class=\"navigation-level-one\"[^<]*?<a href=\"(.*?)\"" ,
							   //"RegExp=class=\"leftNavLevel0Title block\">[^<]*?<a id=\"[^\"]*?\" title=\"[^\"]*?\"[^h]*?href=\"(.*?)\"" ,
	                           //"RegExp=class=\"leftNavLevel0Title block\">[^<]*?<a id=\"[^'\"]*?['\"] title=['\"][^']*?['"][^h]*?href=['\"](.*?)['\"]" ,
			                   "Ordinal=ALL" ,
			                   "Notfound=warning" ,
			                   SEARCH_FILTERS ,
							   "IgnoreRedirections=Yes",
			                   //"Group=1" ,
							   "RequestUrl=*{host}*",
							  LAST ) ;


	web_reg_save_param_regexp( "ParamName=facetsID" ,
	                           "RegExp=facetCheckBox\" id =\"(.*?)\" value=\"(.*?)\"" ,
			                   "Ordinal=ALL" ,
			                   "Notfound=warning" ,
			                   SEARCH_FILTERS ,
			                   "Group=2" ,
			                   LAST ) ;

	web_reg_save_param ( "sortURL" ,
				  		 "LB=<option value=\"http://{host}" ,
	                   	 "RB=\"" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 LAST ) ;

	web_reg_save_param ( "searchSubCategories" ,
				  		 "LB=href='http://{host}" ,
	                   	 "RB='>" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 LAST ) ;

	web_reg_save_param ( "paginationNextURL" ,
				  		 "LB=goToResultPage('http://{host}" ,
	                   	 "RB='" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 LAST ) ;

	web_reg_save_param ( "paginationPageIndex" ,
				  		 "LB=catalogSearchResultDisplay_Context','" ,
	                   	 "RB='" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 LAST ) ;

	web_reg_save_param ( "paginationShowAllURL" ,
				  		 "LB=viewall-right-bottom\">\r\n\t\t\t\t\t                <a href=\"http://{host}" ,
	                   	 "RB=\"" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 LAST ) ;

}//end categoryPageCorrelations()

/*
void subCategoryPageCorrelations() //correlations needed for Product Display & Quick View navigations // rhy 08242017 -obsole
{

	if (strcmp( lr_eval_string("{host}"), "tcp-perf.childrensplace.com" ) == 0)
	{	web_reg_save_param ( "pdpURL" ,
			 "LB=productRow name\">\r\n\t\t\t\r\n\t\t\t\t<a href=\"http://{host}" , //tcp-perf
			 "RB=\" id" ,
			 "ORD=ALL" ,
			 "Convert=HTML_TO_TEXT" ,
			 "NotFound=warning" ,
			 LAST ) ;
	}
	else {
		web_reg_save_param ( "pdpURL" ,
			 "LB=\"<a href=\"{store_home_page}p/" ,
			 "RB=\"",
			 "ORD=All",
			 "NotFound=Warning",
			 LAST ) ;
	}

	web_reg_save_param ( "quickviewURL" ,
	                     "LB=openModal2\" link=\"http://{host}" ,
						 "RB=\" href=" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 LAST ) ;

	web_reg_find("SaveCount=noProducts", "Text/IC=We're sorry, there are no products available for purchase at this time. Please check back later.", LAST);


}//end subCategoryPageCorrelations()
*/

void productDisplayPageCorrelations()
{

	web_reg_save_param ( "skuSelectionSwitchProductID" ,
	                     "LB=confirmSwitchProduct(event,'product_" ,
	                     "RB='" ,
	                     "notFound=Warning",
	                     "ORD=ALL" ,
	                     LAST ) ;

	web_reg_save_param ( "skuSelectionProductID" ,
	                     "LB=input type=\"hidden\" id=\"prodId\" value=\"" ,
	                     "RB=\"" ,
	                     "notFound=Warning",
	                     LAST ) ;

	//BOPIS parameters 03/22/2017
	web_reg_save_param("productId", "LB=\"productId\" : \"", "RB=\",", "ORD=ALL","NotFound=Warning", LAST); //"productId" : "120557",\r\n
	web_reg_save_param("TCPProductInd", "LB=\"TCPProductInd\" : \"", "RB=\"", "ORD=ALL", "NotFound=Warning", LAST); //"TCPProductInd" : "Clearance"
	web_reg_save_param("bopisCatEntryId", "LB=\"catentry_id\" : \"", "RB=\"", "ORD=ALL", "NotFound=Warning", LAST); //"catentry_id" : "771570"
	web_reg_save_param("bopisQty", "LB=\"qty\" : \"", "RB=\"", "ORD=ALL", "NotFound=Warning", LAST); //"qty" : "8"

} //end productDisplayPageCorrelations()


//void Correlations()
void addToCartCorrelations()
{

	web_reg_save_param_regexp( "ParamName=atc_catentryIds" ,
	                           "RegExp=\"catentry_id\" : \"(.*?)\",\r\n\t\t\t\t\t\t\t\t\t\t\t\"item_sku\" : \"(.*?)\",\r\n\t\t\t\t\t\t\t\t\t\t\t\"qty\" : \"([1-9][0-9]*)\"" ,
			                   "Ordinal=ALL" ,
			                   "Notfound=warning" ,
			                   SEARCH_FILTERS ,
			                   "Group=1" ,
			                   LAST ) ;

 	web_reg_save_param ( "atc_comment" ,
                    	 "LB=<input type=\"hidden\" name=\"comment\" value=\"" ,
                    	 "RB=\"" ,
                    	 "Notfound=warning" ,
                    	 LAST ) ;

} // end addToCartCorrelations()
/*
void api_getCountryListAndHeaderInfo(){

	lr_start_transaction("T30_API getCountryListAndHeaderInfo");
		web_custom_request("getCountryListAndHeaderInfo",
			"URL=https://{api_host}/getCountryListAndHeaderInfo",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
	lr_end_transaction("T30_API getCountryListAndHeaderInfo", LR_AUTO);

}

void api_getAvailableOffers(){

	lr_start_transaction("T30_API getAvailableOffers");
			web_custom_request("getAvailableOffers",
						"URL=https://{api_host}/payment/getAvailableOffers",
						"Method=GET",
						"Resource=0",
						"RecContentType=application/json",
						LAST);
	lr_end_transaction("T30_API getAvailableOffers", LR_AUTO);
}

void api_getESpot(){


	lr_start_transaction("T30_API getESpot");
					web_custom_request("getESpot",
						"URL=https://{api_host}/getESpot",
						"Method=GET",
						"Resource=0",
						"RecContentType=application/json",
						LAST);
	lr_end_transaction("T30_API getESpot", LR_AUTO);

}


void api_getOrderDetails(){

	lr_start_transaction("T30_API getOrderDetails");
					web_custom_request("getOrderDetails",
						"URL=https://{api_host}/getOrderDetails",
						"Method=GET",
						"Resource=0",
						"RecContentType=application/json",
						LAST);
	lr_end_transaction("T30_API getOrderDetails", LR_AUTO);

}

void api_getRegisteredUserDetailsInfo(){

	lr_start_transaction("T30_API getRegisteredUserDetailsInfo");
					web_custom_request("getRegisteredUserDetailsInfo",
						"URL=https://{api_host}/payment/getRegisteredUserDetailsInfo",
						"Method=GET",
						"Resource=0",
						"RecContentType=application/json",
						LAST);
	lr_end_transaction("T30_API getRegisteredUserDetailsInfo", LR_AUTO);

}

void api_getAllCoupons(){

	web_add_header("storeId","{storeId}");
	web_add_header("catalogId","{catalogId}");
	web_add_header("langId","-1");
	lr_start_transaction("T30_API getAllCoupons");
					web_custom_request("getAllCoupons",
						"URL=https://{api_host}/tcporder/getAllCoupons",
						"Method=GET",
						"Resource=0",
						"RecContentType=application/json",
						LAST);
	lr_end_transaction("T30_API getAllCoupons", LR_AUTO);
}

void api_getXAppConfigValues(){

	lr_start_transaction("T30_API getXAppConfigValues");
					web_custom_request("getXAppConfigValues",
						"URL=https://{api_host}/tcporder/getXAppConfigValues",
						"Method=GET",
						"Resource=0",
						"RecContentType=application/json",
						LAST);
	lr_end_transaction("T30_API getXAppConfigValues", LR_AUTO);
}

void api_getCoupon(){

	lr_start_transaction("T30_API getCoupon");
					web_custom_request("getCoupon",
						"URL=https://{api_host}/payment/getCoupon",
						"Method=GET",
						"Resource=0",
						"RecContentType=application/json",
						LAST);
	lr_end_transaction("T30_API getCoupon", LR_AUTO);
}

void api_getPointsService(){

	web_add_header("storeId","{storeId}");
	web_add_header("catalogId","{catalogId}");
	web_add_header("langId","-1");

	lr_start_transaction("T30_API getPointsService");
					web_custom_request("getPointsService",
						"URL=https://{api_host}/payment/getPointsService",
						"Method=GET",
						"Resource=0",
						"RecContentType=application/json",
						LAST);
	lr_end_transaction("T30_API getPointsService", LR_AUTO);
}
*/

void viewHomePage()
{
	lr_save_string("home", "callerId");

	homePageCorrelations();

	web_global_verification("Text/IC=h1 role=\"main\">The store has encountered a problem processing the last request",  "Fail=Found","ID=FindStoreProblem", LAST );


	lr_param_sprintf ( "homePageUrl" , "http://%s%shome" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}") ) ;

	lr_save_string("T01_Home Page", "mainTransaction");
	if( atoi(lr_eval_string("{akamaiRatio}")) <= (100 - NO_BROWSE_RATIO_HOME) ) {

		lr_save_string("akamai", "callerId2");
		API_SUB_TRANSACTION_SWITCH = 0;
		callAPI();

	} else {
		lr_start_transaction( "T01_Home Page" ) ;

		if ( webURL2(lr_eval_string ( "{homePageUrl}" ), "T01_Home Page" , "Kids Clothes & Baby Clothes | The Children's Place" ) == LR_PASS) {
			callAPI();
			lr_end_transaction( "T01_Home Page" , LR_PASS ) ;
		} else {
			lr_fail_trans_with_error( lr_eval_string("T01_Home Page Text Check Failed - {homePageUrl}") ) ;
			lr_end_transaction( "T01_Home Page" , LR_FAIL ) ;
		}

	}

	BROWSE_OPTIONS_FLAG = 1;

	lr_save_string("Other", "callerId");

}

int getTopCategories()
{
	web_reg_save_param("aaaaaa", "LB=url\": \"", "RB=\"", "ORD=All", "NotFound=Warning", LAST);

//	lr_start_transaction("T0?-getTopCategories");

	web_submit_data("TCPLocaleSelection",
	    "Action=http://{host}/shop/{store}/getTopCategories",
		"Method=POST",
		"Resource=0",
		"Mode=HTML",
		ITEMDATA,
            "Name=storeId", "Value={storeId}", ENDITEM,
            "Name=catalogId", "Value={catalogId}", ENDITEM,
		LAST);

//	if( strcmp(lr_eval_string("{redirectToStore}"), "us") == 0 || strcmp(lr_eval_string("{redirectToStore}"), "ca") == 0)
//		return lr_end_transaction("T0?-getTopCategories", LR_PASS);
//	else
//		return lr_end_transaction("T0?-getTopCategories", LR_FAIL);
	return 0;
}

void getProductviewbyCategory()
{
	addHeader();
	web_add_header( "categoryId", lr_eval_string("{category}") );
	web_add_header( "pageNumber", "1" );
	web_add_header( "pageSize", "18" );
	web_add_header( "searchSource", "E" );
	web_add_header( "searchType", "-1,-1,0" );

//	web_reg_save_param("pdpUrl", "LB=seoURL\":\"http:\\/\\/{host}\\/us\\/p\\/", "RB=\"", "ORD=All", "NotFound=Warning", LAST); //seoURL":"http:\/\/uatlive1.childrensplace.com\/us\/p\/Toddler-Girls-Microfiber-Tights-1083124-Y0"
//	web_reg_save_param("subCategories", "LB=seo_token_ntk\":\"", "RB=\"", "ORD=All", "NotFound=Warning", LAST); //seo_token_ntk":"girls-outfits"

	web_custom_request("getProductviewbyCategory",
		"URL=https://{api_host}/tcpproduct/getProductviewbyCategory",
		"Method=GET",
		"Resource=0",
		"RecContentType=text/html",
		"EncType=application/x-www-form-urlencoded",
		LAST);
}

void navByBrowse()
{
	int categoryIndex = 0;

	lr_think_time ( LINK_TT ) ;

	if ( strcmp(lr_eval_string("{storeId}"), "10152") == 0 )
	{
		index = rand () % 6 + 1 ;
		switch(index)
		{
			case 1: lr_save_string( "349517", "category"); break;
			case 2: lr_save_string( "349514", "category"); break;
			case 3: lr_save_string( "349515", "category"); break;
			case 4: lr_save_string( "349513", "category"); break;
			case 5: lr_save_string( "349518", "category"); break;
//			case 6: lr_save_string( "childrens-shoes-kids-shoes", "category"); break;
//			case 7: lr_save_string( "kids-accessories-canada", "category"); break;
			default: break;
		}
	}
	else
	{
			index = rand () % 5 + 1 ;
			switch(index)
			{
				case 1: lr_save_string( "girls-clothing", "category"); break;
				case 2: lr_save_string( "toddler-girl-clothes", "category"); break;
				case 3: lr_save_string( "boys-clothing", "category"); break;
				case 4: lr_save_string( "toddler-boy-clothes", "category"); break;
				case 5: lr_save_string( "baby-clothes", "category"); break;
//				case 6: lr_save_string( "childrens-shoes-kids-shoes", "category"); break;
//				case 7: lr_save_string( "kids-accessories-us", "category"); break;
				default: break;
			}
	}

	lr_param_sprintf ( "cateogryPageUrl" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/") , lr_eval_string( "{category}" ) ) ;

//	categoryIndex = rand() % lr_paramarr_len("categories");
//	lr_save_string(lr_paramarr_idx( "categories", categoryIndex ), "category" ) ;
//	lr_save_int(categoryIndex, "categoryIndex");

//	lr_save_string(lr_paramarr_random( "categories" ), "category" ) ;
	lr_param_sprintf ( "cateogryPageUrl" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/") , lr_eval_string( "{category}" ) ) ;

	web_convert_param("cateogryPageUrl", "SourceEncoding=HTML","TargetEncoding=PLAIN", LAST );
	web_reg_save_param( "subCategories", "LB=navigation-level-two-link\" href=\"{store_home_page}c/", "RB=\"", "ORD=All", "NotFound=Warning", LAST); //navigation-level-two-link" href="/us/c/girls-outerwear-jackets"


/*
	getSubCategories();
	lr_param_sprintf ( "cateogryPageUrl" , "https://%s%s" , lr_eval_string("{host}"), lr_eval_string( "{category}" ) ) ;
*/

	lr_save_string("categoryDisplay", "callerId");
	lr_save_string("T02_Category Display", "mainTransaction");

	if( atoi(lr_eval_string("{akamaiRatio}")) <= (100 - NO_BROWSE_RATIO_CATEGORY) ) {

		lr_save_string("akamai", "callerId2");
		API_SUB_TRANSACTION_SWITCH = 0;
		callAPI();

	} else {
		lr_start_transaction( "T02_Category Display" ) ;
		if (webURL2(lr_eval_string ( "{cateogryPageUrl}" ), "T02_Category Display", "html " ) == LR_PASS ) {
			callAPI();
			lr_end_transaction ( "T02_Category Display" , LR_PASS ) ;
		}
		else if ((lr_paramarr_len("subcategories")) == 0)
		{
			lr_fail_trans_with_error( lr_eval_string("T02_Category Display Failed subcategories==0 for- {cateogryPageUrl}") ) ;
			lr_end_transaction ( "T02_Category Display" , LR_FAIL ) ; // confirm.
		} else {

			lr_fail_trans_with_error( lr_eval_string("T02_Category Display Text Check Failed for - {cateogryPageUrl}") ) ;
			lr_end_transaction ( "T02_Category Display" , LR_FAIL ) ; // confirm.
		}
	}

	lr_save_string("", "mainTransaction");
	/*
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) < 75 ) {
		drillSubCategory();
	}
	*/
}

void navByBrowseR3()
{
	int categoryCount = atoi( lr_eval_string("{categories_count}"))-1;
	lr_think_time ( LINK_TT ) ;

	//lr_save_string( lr_paramarr_random ( "categories" ), "category");
	index = rand ( ) % categoryCount + 1 ;
	lr_save_string( lr_paramarr_idx ( "categories", index ), "category");

	lr_param_sprintf ( "cateogryPageUrl" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/") , lr_eval_string( "{category}" ) ) ;

	web_convert_param("cateogryPageUrl", "SourceEncoding=HTML","TargetEncoding=PLAIN", LAST );

	//lr_continue_on_error(1);

	lr_save_string("T02_Category Display", "mainTransaction");
	lr_start_transaction( "T02_Category Display" ) ;

//	if (webURL2(lr_eval_string ( "{cateogryPageUrl}" ), "T02_Category Display", "End - SEO H1/Body Contents for Category page" ) == LR_PASS ) //R2/R3
	if (webURL2(lr_eval_string ( "{cateogryPageUrl}" ), "T02_Category Display", "html " ) == LR_PASS ) //uatlive1
	{
		lr_save_string("categoryDisplay", "callerId");
		callAPI();

		lr_end_transaction ( "T02_Category Display" , LR_PASS ) ;
	}
	else if ((lr_paramarr_len("subcategories")) == 0)
	{
			lr_fail_trans_with_error( lr_eval_string("T02_Category Display Failed subcategories==0 for- {cateogryPageUrl}") ) ;
			lr_end_transaction ( "T02_Category Display" , LR_FAIL ) ; // confirm.
	}
	else {
		lr_fail_trans_with_error( lr_eval_string("T02_Category Display Text Check Failed for - {cateogryPageUrl}") ) ;
		lr_end_transaction ( "T02_Category Display" , LR_FAIL ) ; // confirm.
	}

	//lr_continue_on_error(0);

} // end navByBrowse


void navByClearance()
{
 	lr_think_time ( LINK_TT ) ;
	//index = rand ( ) % lr_paramarr_len( "clearances" ) + 1 ;
//	findNewArrivals(); //NOT NEEDED ANYMORE, noProducts handling already in place
//	lr_param_sprintf ( "clearancePageUrl" , "http://%s/shop/SearchDisplay?%s" , lr_eval_string("{host}"),  lr_paramarr_idx ( "clearances" , index ) ) ;
//	lr_param_sprintf ( "clearancePageUrl" , "http://%s/shop/SearchDisplay?%s" , lr_eval_string("{host}"),  lr_paramarr_random( "clearances" ) ) ;
	//lr_param_sprintf ( "clearancePageUrl" , "%s", "http://tcp-perf.childrensplace.com/shop/SearchDisplay?storeId=10151&catalogId=10551&langId=-1&beginIndex=0&searchSource=Q&sType=SimpleSearch&showResultsPage=true&pageView=image&facet=ads_f10001_ntk_cs:%22New Arrivals%22&categoryId=47502" );

	lr_param_sprintf ( "clearancePageUrl" , "%s" ,  lr_eval_string("{categories_3}") ) ; //Please continue to monitor this peice. on 03/07/2017 this was categories_3
	lr_save_string("categoryDisplay", "callerId");

//	categoryPageCorrelations();
//	subCategoryPageCorrelations(); /// rhy added 11/23/2015, 11/27 without this pdpURL will not have any vale

	web_reg_find("SaveCount=noProducts", "Text/IC=We're sorry, there are no products available for purchase at this time. Please check back later.", LAST);
	web_reg_find("Text=FILTER BY", "SaveCount=FilterBy_Count", LAST );

	lr_save_string("T02_Clearance Display", "mainTransaction");
	lr_start_transaction ( "T02_Clearance Display" ) ;

	//webURL(lr_eval_string ( "{clearancePageUrl}" ), "T02_Clearance Display" );
	if (webURL2(lr_eval_string ( "{clearancePageUrl}" ), "T02_Clearance Display", "<!-- BEGIN TCPSearchSetup.jspf-->" ) == LR_PASS )
	{
		callAPI();

		if ( atoi(lr_eval_string("{noProducts}")) == 1 )
		{
			lr_param_sprintf ( "failedPageUrl" , "T02_Clearance Display Failure : %s", lr_eval_string("{clearancePageUrl}") ) ;
			lr_fail_trans_with_error( lr_eval_string("T02_Clearance Display Text Check Failed for - {failedPageUrl}") ) ;
			lr_end_transaction ( "T02_Clearance Display" , LR_FAIL ) ;

		}
		else
			lr_end_transaction ( "T02_Clearance Display" , LR_PASS ) ;

	}
	else
	{
//		callAPI();

		lr_end_transaction ( "T02_Clearance Display" , LR_FAIL ) ;
	}
} // end navByClearance


findNewArrivals() // used in navByClearance()
{
	int offset = 1;

    char * position;

    char * str = "";

    char * search_str = "Arrivals"; //try not to pick New Arrival Clearance Products

    while( offset >= 1 ) {

		str = lr_paramarr_random( "clearances" );
		//lr_message("str is %s:", str);
	    position = (char *)strstr(str, search_str);

	    offset = (int)(position - str + 1);
    }

    lr_save_string(str, "clearance");

	return 0;
}

void typeAheadSearch()
{
	//lr_continue_on_error(1);

	//lr_message ( "In typeAheadSearch" ) ;
    for (i = 0; i < length; i++)
    {
       	//lr_message("source: %c", searchString[i]);
       	if (i == 0)
           	lr_param_sprintf ("SEARCH_STRING_PARAM", "%c", searchString[i]);
       	else
           	lr_param_sprintf ("SEARCH_STRING_PARAM", "%s%c", lr_eval_string("{SEARCH_STRING_PARAM}"), searchString[i]);
           	//lr_message("Partial SEARCH_STRING_PARAM: %s", lr_eval_string("{SEARCH_STRING_PARAM}"));

		if (i == 1) lr_save_string(lr_eval_string("{SEARCH_STRING_PARAM}"),"forDidYouMean");

//     	type ahead search call after every 3rd characters
		if (i == 2 || i == 5 || i == 8 || i == 11 || i == 14 || i == 17 || i == 20 )
       	{
    		lr_think_time ( TYPE_SPEED );

			web_reg_save_param_json(
				"ParamName=autoSelectOptionTerm",
				"QueryString=$.autosuggestions..termsArray..term",
				"SelectAll=Yes",
				"SEARCH_FILTERS",
				"NotFound=Warning",
			"LAST");
			web_reg_save_param_json(
				"ParamName=autoSelectOptionCategory",
				"QueryString=$.category..response..categoryUrl",
				"SelectAll=Yes",
				"SEARCH_FILTERS",
				"NotFound=Warning",
			"LAST");
			web_add_header("isRest","true");
			web_add_header("Content-Type","application/json");
			web_add_header("term","{SEARCH_STRING_PARAM}");
			web_add_header("coreName","MC_10001_CatalogEntry_en_US");
			addHeader();

			lr_start_transaction ( "T02_Search_getAutoSuggestion" ) ;
			web_custom_request("getAutoSuggestions",
						"URL=https://{api_host}/getAutoSuggestions",
						"Method=GET",
						"Resource=0",
						"RecContentType=application/json",
						LAST);
			lr_end_transaction ( "T02_Search_getAutoSuggestion" , LR_PASS ) ;
			//if there is a suggestion, capture it, exit the loop, use the suggestion for submitCompleteSearch
			if (atoi(lr_eval_string("{autoSelectOptionTerm_count}")) > 0) {
				searchString = lr_paramarr_idx("autoSelectOptionTerm", 1);
				break;
			}
			else if (atoi(lr_eval_string("{autoSelectOptionCategory_count}")) > 0) {
				searchString = lr_paramarr_idx("autoSelectOptionCategory", 1);
				break;
			}
//			else

//if (atoi(lr_eval_string("{autoSelectOptionURL_count}")) > 0) {
//				searchString = lr_paramarr_idx("autoSelectOptionURL", 1);
//				break;
//			}

       	} // end iF
   	 } // end for
 	//lr_continue_on_error(0);

} // end typeAheadSearch()


void submitCompleteSearch() // 	Search for complete string should be placed
{
	lr_think_time (LINK_TT);

	web_reg_save_param("totalProductsCount", "LB=totalProductsCount\":", "RB=,\"breadcrumbs", "NotFound=Warning", LAST);

	lr_save_string(lr_eval_string("{searchStr}"), "searchTerm");
	lr_start_transaction ( "T02_Search" );

	if( atoi(lr_eval_string("{akamaiRatio}")) <= (100 - NO_BROWSE_RATIO_CATEGORY) ) {

//		web_reg_save_param( "pdpURL", "LB=,\"pdpUrl\":\"{store_home_page}p/", "RB=\",\"shortDescription", "ORD=All", "NotFound=Warning", LAST); //,"pdpUrl":"/us/p/Girls-Basic-Skinny-Jeans---True-Indigo-Wash-2069116-JP","shortDescription
		web_reg_save_param( "pdpURL", "LB=<a href=\"/{store_home_page}p/", "RB=\"", "ORD=All", "NotFound=Warning", LAST); //,"pdpUrl":"/us/p/Girls-Basic-Skinny-Jeans---True-Indigo-Wash-2069116-JP","shortDescription
		web_reg_save_param( "productId", "LB=generalProductId\":\"", "RB=\"", "ORD=All", "NotFound=Warning", LAST); //"generalProductId":"
		lr_save_string("akamai", "callerId2");
		addHeader();
		web_add_header("count", "4");
		web_add_header("resultCount", "4");
		web_add_header("searchTerm", lr_eval_string("{searchTerm}"));
		web_add_header("timeAllowed", "15000");
		web_reg_find("TEXT/IC=autosuggestions", "SaveCount=apiCheck", LAST);

		web_url("searchTermSuggestions",
			"URL=https://{api_host}/tcpproduct/searchTermSuggestions",
			"Resource=0",
			"Mode=HTTP",
			LAST);

		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction("T02_Search_tcpproduct/searchTermSuggestions", LR_FAIL);
		else
			lr_end_sub_transaction("T02_Search_tcpproduct/searchTermSuggestions", LR_AUTO);

		lr_save_string("T02_Search", "mainTransaction");
		callAPI();

		lr_end_transaction("T02_Search", LR_AUTO);
		
	} else {
//		web_reg_save_param( "pdpURL", "LB=,\"pdpUrl\":\"{store_home_page}p/", "RB=\",\"shortDescription", "ORD=All", "NotFound=Warning", LAST); //,"pdpUrl":"/us/p/Girls-Basic-Skinny-Jeans---True-Indigo-Wash-2069116-JP","shortDescription
		web_reg_save_param( "pdpURL", "LB=<a href=\"/{store_home_page}p/", "RB=\"", "ORD=All", "NotFound=Warning", LAST); //,"pdpUrl":"/us/p/Girls-Basic-Skinny-Jeans---True-Indigo-Wash-2069116-JP","shortDescription
		web_reg_save_param( "productId", "LB=generalProductId\":\"", "RB=\"", "ORD=All", "NotFound=Warning", LAST); //"generalProductId":"
		addHeader();
		web_add_header("count", "4");
		web_add_header("resultCount", "4");
		web_add_header("searchTerm", lr_eval_string("{searchTerm}"));
		web_add_header("timeAllowed", "15000");

		web_url("search",
			"URL=https://{host}{store_home_page}search/{searchTerm}",
			"Resource=0",
			"RecContentType=text/html",
			"Mode=HTTP",
			LAST);


		if ( atoi(lr_eval_string("{totalProductsCount}")) > 0  ||  atoi(lr_eval_string("{productId_count}")) > 0 ) {

			lr_start_sub_transaction("T02_Search_tcpproduct/searchTermSuggestions", "T02_Search");
			addHeader();
			web_add_header("count", "4");
			web_add_header("resultCount", "4");
			web_add_header("searchTerm", lr_eval_string("{searchTerm}"));
			web_add_header("timeAllowed", "15000");
			web_reg_find("TEXT/IC=autosuggestions", "SaveCount=apiCheck", LAST);

			web_url("searchTermSuggestions",
				"URL=https://{api_host}/tcpproduct/searchTermSuggestions",
				"Resource=0",
				"Mode=HTTP",
				LAST);

			if (atoi(lr_eval_string("{apiCheck}")) == 0 )
				lr_end_sub_transaction("T02_Search_tcpproduct/searchTermSuggestions", LR_FAIL);
			else
				lr_end_sub_transaction("T02_Search_tcpproduct/searchTermSuggestions", LR_AUTO);
			
			lr_save_string("T02_Search", "mainTransaction");
			callAPI();

			lr_end_transaction("T02_Search", LR_AUTO);
		}
		else {
			lr_end_sub_transaction("T02_Search", LR_FAIL);
		}
	}

	lr_save_string("search", "paginateBy");

	return;

} // end submitCompleteSearch


void submitCompleteSearchDidYouMean() // 	Search with "Did You Mean"
{
	//lr_message ( "In submitCompleteSearch" ) ;
 	lr_think_time (LINK_TT);
//	categoryPageCorrelations();
//	subCategoryPageCorrelations();
//	productDisplayPageCorrelations();

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_SEARCH_SUGGEST )
		lr_param_sprintf ("SEARCH_STRING", "%s", lr_eval_string("{forDidYouMean}"));
	else
		lr_param_sprintf ("SEARCH_STRING", "%s", searchString);

	web_reg_find("Text= 0 matches", "SaveCount=searchResults_Count", LAST );
	web_reg_find("Text=The store has encountered a problem", "SaveCount=storeProblem_Count", LAST ); //The store has encountered a problem processing the last request. Try again later. If the problem persists, contact your site administrator.
	web_reg_save_param("didYouMeanId", "LB=http://{host}/shop/SearchDisplay?searchTermScope=&searchType=1002&filterTerm=&orderBy=&maxPrice=&showResultsPage=true&langId=-1&departmentId=&sType=",
	"RB=\" class=\"result\">", "ORD=All", "NotFound=Warning", LAST);

	   //URL=https://{host}/shop/SearchDisplay?storeId=10151&catalogId=10551&langId=-1&pageSize=100&beginIndex=0&searchSource=Q&sType=SimpleSearch&resultCatEntryType=2&showResultsPage=true&pageView=image&custSrch=search&searchTerm=clips&TCPSearchSubmit=
	lr_start_transaction ( "T02_Search" ) ;

	// AND-258 - Search | Did You Mean?
	 //"URL=https://{host}/shop/SearchDisplay?searchTerm={{SEARCH_STRING}}&storeId={storeId}&catalogId={catalogId}&langId=-1&pageSize=100&beginIndex=0&searchSource=Q&sType=SimpleSearch&resultCatEntry=2&showResultsPage=true&pageView=image&custSrch=search"
	web_url("submitSearch",
		//"URL=https://{host}/shop/SearchDisplay?storeId={storeId}&catalogId={catalogId}&langId=-1&pageSize=100&beginIndex=0&searchSource=Q&sType=SimpleSearch&resultCatEntryType=2&showResultsPage=true&pageView=image&custSrch=search&searchTerm={SEARCH_STRING}&TCPSearchSubmit=",
		"URL=https://{host}/shop/SearchDisplay?searchTerm={{SEARCH_STRING}}&storeId={storeId}&catalogId={catalogId}&langId=-1&pageSize=100&beginIndex=0&searchSource=Q&sType=SimpleSearch&resultCatEntry=2&showResultsPage=true&pageView=image&custSrch=search"
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		LAST);

		lr_save_string("T02_Search", "mainTransaction");
//		callAPI();

	if (atoi(lr_eval_string("{searchResults_Count}")) == 0 || atoi(lr_eval_string("{storeProblem_Count}")) == 0 )
		lr_end_transaction ( "T02_Search" , LR_FAIL ) ;
	else
		lr_end_transaction ( "T02_Search" , LR_PASS ) ;

} // end submitCompleteSearch

writeToFile1()
{
	char *ip;
    char fullpath[1024], * filename1 = "\\e$\\Performance\\LogOutput\\LowInventory_US_v2.dat";
	long file1;
//	char * filename1 = "\\\\10.56.29.36\\e$\\Performance\\Scripts\\2016_R1\\03_PERF\\Datafiles\\TestData\\searchStrNoResult.dat";
    strcpy(fullpath, "\\\\" );
	ip = lr_get_host_name( );
    strcat(fullpath, ip);
	strcat(fullpath, filename1);

    if ((file1 = fopen(fullpath, "a+")) == NULL) {
        lr_output_message ("Unable to open %s", fullpath);
        return -1;
    }

	fprintf(file1, "%s\n", lr_eval_string("{logLowCatEntryId}"));

    fclose(file1);
	return 0;
}

writeToFile2()
{
	char *ip;
    char fullpath[1024], * filename1 = "\\e$\\Performance\\LogOutput\\LowInventory_CA_v2.dat";
	long file1;
//	char * filename1 = "\\\\10.56.29.36\\e$\\Performance\\Scripts\\2016_R1\\03_PERF\\Datafiles\\TestData\\searchStrNoResult.dat";
    strcpy(fullpath, "\\\\" );
	ip = lr_get_host_name( );
    strcat(fullpath, ip);
	strcat(fullpath, filename1);

    if ((file1 = fopen(fullpath, "a+")) == NULL) {
        lr_output_message ("Unable to open %s", fullpath);
        return -1;
    }

	fprintf(file1, "%s\n", lr_eval_string("{logLowCatEntryId}"));

    fclose(file1);
	return 0;
}

void navBySearch()
{
	int searchSuggest = atoi(lr_eval_string("{RANDOM_PERCENT}"));

	lr_think_time ( TYPE_SPEED );
//	searchString = "00889705462289"; //this is the search string from the data file
	searchString = lr_eval_string("{searchStr}"); //this is the search string from the data file
	lr_save_string(searchString,"originalSearchString");
	/****************REMOVE**********/
/*	searchString="boys";
	lr_save_string("boys",	"SEARCH_STRING_PARAM");
	lr_save_string("boys",	"originalSearchString");
	lr_save_string("boys",	"searchStr");
*/
    length = (int)strlen(searchString);

    if (length >= 3) //type ahead only if search term is greater than 3 characters
    	typeAheadSearch();

	if ( searchSuggest < RATIO_SEARCH_SUGGEST )
		submitCompleteSearchDidYouMean();
	else
		submitCompleteSearch();

	//should we be calling callAPI here???

} //end navBySearch


void navByPlace()
{
	//lr_message ( "In navByPlace" ) ;
	if ( atoi( lr_eval_string( "{placeShopCategory_count}")) != 0 ) {

		lr_save_string( lr_paramarr_random("placeShopCategory"), "placeShop");

		if ( strcmp( lr_eval_string("{placeShop}") ,"placeShops-pjplace-2014" ) == 0)
			lr_save_string( lr_paramarr_random("placeShopCategory"), "placeShop");

		lr_start_transaction ( "T02_PlaceShop" ) ;

		web_url("placeShop",
			"URL=http://{host}{store_home_page}content/{placeShop}",
			"Resource=0",
			"RecContentType=text/html",
			LAST);

		lr_end_transaction ( "T02_PlaceShop" , LR_PASS ) ;
	}
/*
 <a href="http://tcp-perf.childrensplace.com/shop/us/content/mvp-shop-spring-2014"

   	web_reg_save_param ( "placeShopCategory" ,
	                     "LB=/shop/us/content/" ,
	                     "RB=\"",
						 "ORD=All",
	                     "NotFound=Warning", LAST ) ;
	*/
} //end navByPlace

void drill()
{
	// T03_Drill Facets or SubCategory
	randomPercent = atoi(lr_eval_string("{RANDOM_PERCENT}"));

	if ( randomPercent <= DRILL_ONE_FACET ) {
		drillOneFacet();
	} else if ( randomPercent <= (DRILL_ONE_FACET + DRILL_TWO_FACET) ) {
		drillTwoFacets();
	} else if ( randomPercent <= (DRILL_ONE_FACET + DRILL_TWO_FACET + DRILL_THREE_FACET)) {
		drillThreeFacets();
	} else if ( randomPercent <= (DRILL_ONE_FACET + DRILL_TWO_FACET + DRILL_THREE_FACET + DRILL_SUB_CATEGORY)) {
		drillSubCategory();
	}

} // end drill


// T02_Category / Clearance / Search / PlaceShop Display
// select navigation method from NAV_BY parameter, "stop" will end iteration. This is a mandatory step to continue further and cannot be passed
void topNav()
{
//	nav_by = lr_eval_string("{RATIO_NAV_BY}") ;

	randomPercent = atoi(lr_eval_string("{RANDOM_PERCENT}"));

	if ( randomPercent <= NAV_BROWSE ) {
		navByBrowse();
//		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_BUILDCART_DRILLDOWN )
			drill();	
	}
	else if ( randomPercent <= (NAV_BROWSE + NAV_CLEARANCE) )
		navByClearance();
	else if ( randomPercent <= (NAV_BROWSE + NAV_CLEARANCE + NAV_SEARCH))
		//navBySearch();
		submitCompleteSearch();
	else if (randomPercent <= (NAV_BROWSE + NAV_CLEARANCE + NAV_SEARCH + NAV_PLACE))
		navByPlace();

} // end topNav


void paginate()
{

	int itemCounter = 0;

//	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) < APPLY_PAGINATE && atoi(lr_eval_string("{totalProductsCount}")) > 0  ) {

		lr_start_transaction ( "T03_Paginate" ) ;

		if(strcmp(lr_eval_string("{paginateBy}"), "search") == 0 ) {

//01262017			web_reg_save_param("uniqueId", "LB=\"uniqueID\":\"", "RB=\",\"seoURL", "ORD=All", "NotFound=Warning", LAST);
//			web_reg_save_param("uniqueId", "LB=\"uniqueID\":\"", "RB=\",\"seoURL", "ORD=All", "NotFound=Warning", LAST); //01262017
//			web_reg_save_param("productId", "LB=\"uniqueID\":\"", "RB=\",\"seoURL", "ORD=All", "NotFound=Warning", LAST); //01262017
//			web_reg_save_param("pdpURL", "LB=\"seoURL\": \"{store_home_page}p/", "RB=\",", "ORD=All", "NotFound=Warning", LAST);
/********************03212018 Needs updated**** PAVAN ROMANO*********/
//			web_reg_save_param_json("ParamName=uniqueId", "QueryString=$..catalogEntryView..[?(@.seoURL)].uniqueID", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
//			web_reg_save_param_json("ParamName=productId", "QueryString=$..catalogEntryView..[?(@.seoURL)].uniqueID", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
//			web_reg_save_param_json("ParamName=pdpURL", "QueryString=$..catalogEntryView..[?(@.seoURL)].seoURL", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
			web_reg_save_param_json("ParamName=uniqueId", "QueryString=$..catalogEntryView..[?(@.resourceId)].uniqueID", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
			web_reg_save_param_json("ParamName=productId", "QueryString=$..catalogEntryView..[?(@.resourceId)].uniqueID", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
			web_reg_save_param_json("ParamName=pdpURL", "QueryString=$..catalogEntryView..[?(@.resourceId)].uniqueID", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);

			addHeader();
			lr_start_sub_transaction("T03_Paginate getProductsBySearchTerm", "T03_Paginate");
			web_add_header( "searchTerm", lr_eval_string("{searchTerm}") );
			web_add_header( "pageNumber", lr_eval_string("{pageNumber}") );
			web_add_header( "pageSize", "20" );
			web_add_header( "searchSource", "E" );
			web_add_header( "searchType", "-1,-1,0" );

			web_custom_request("getProductsBySearchTerm",
				"URL=https://{api_host}/tcpproduct/getProductsBySearchTerm?departments=",
//01262017				"URL=https://{api_host}/tcpproduct/getProductsBySearchTerm",
				"Method=GET",
				"Resource=0",
				"RecContentType=text/html",
				"EncType=application/x-www-form-urlencoded",
				LAST);
			lr_end_sub_transaction("T03_Paginate getProductsBySearchTerm", LR_AUTO);

			if ( lr_paramarr_len("uniqueId") != 0 ) {

				lr_param_sprintf ( "body", "%s", "{\"productId\":[\"" );

				for( itemCounter = 1; itemCounter <= lr_paramarr_len("uniqueId"); itemCounter++ ) {

					lr_param_sprintf( "body", "%s%s%s", lr_eval_string("{body}") , lr_paramarr_idx("uniqueId", itemCounter), "\"" );

					if (lr_paramarr_len("uniqueId") != itemCounter )
						lr_param_sprintf( "body", "%s%s", lr_eval_string("{body}"), ",\"" );
					else
						lr_param_sprintf( "body", "%s%s", lr_eval_string("{body}") , "\"]}" );

				}
			}

			//{"productId":["624004","628934","614001","614606","614607","614608","614609","614610","615388","618285","618353","618355","618404","618416","619482","113216","361568","361576"]}
			addHeader();

			if ( isLoggedIn == 1	) {
			lr_start_sub_transaction("T03_Paginate getListofDefaultWishlist", "T03_Paginate");
			web_custom_request("getListofDefaultWishlist",
				"URL=https://{api_host}/tcpstore/getListofDefaultWishlist",
				"Method=POST",
				"Resource=0",
				"RecContentType=application/json",
				"Mode=HTTP",
				"Body={body}",
				LAST);
			lr_end_sub_transaction("T03_Paginate getListofDefaultWishlist", LR_AUTO);
			}


		} else {

//01262017			web_reg_save_param("uniqueId", "LB=\"extendedData\": {\n            \"uniqueId\": \"", "RB=\",\n            \"parentIds", "ORD=All", "NotFound=Warning", LAST);
//			web_reg_save_param("uniqueId", "LB=\"uniqueID\":\"", "RB=\",\"seoURL", "ORD=All", "NotFound=Warning", LAST); //01262017
//			web_reg_save_param("productId", "LB=\"uniqueID\":\"", "RB=\",\"seoURL", "ORD=All", "NotFound=Warning", LAST); //01262017
//			web_reg_save_param("pdpURL", "LB=\"seoURL\": \"{store_home_page}p/", "RB=\",", "ORD=All", "NotFound=Warning", LAST);


/******************Needs revisiting Pavan Romano 03212018****************/
//			web_reg_save_param_json("ParamName=uniqueId", "QueryString=$..catalogEntryView..[?(@.seoURL)].uniqueID", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
//			web_reg_save_param_json("ParamName=productId", "QueryString=$..catalogEntryView..[?(@.seoURL)].uniqueID", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
//			web_reg_save_param_json("ParamName=pdpURL", "QueryString=$..catalogEntryView..[?(@.seoURL)].seoURL", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
						web_reg_save_param_json("ParamName=uniqueId", "QueryString=$..catalogEntryView..[?(@.resourceId)].uniqueID", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
						web_reg_save_param_json("ParamName=productId", "QueryString=$..catalogEntryView..[?(@.resourceId)].uniqueID", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
						web_reg_save_param_json("ParamName=pdpURL", "QueryString=$..catalogEntryView..[?(@.resourceId)].uniqueID", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);

			lr_start_sub_transaction("T03_Paginate getProductviewbyCategory", "T03_Paginate");
			web_add_header( "categoryId", "girls-leggings" );  //get the correct category id e.g. "girls-tops-girls-shirts"
			web_add_header( "pageNumber", "2" );
			web_add_header( "pageSize", "20" );
			web_add_header( "searchSource", "E" );
			web_add_header( "searchType", "-1,-1,0" );

			web_custom_request("getProductviewbyCategory",
				"URL=https://{api_host}/tcpproduct/getProductviewbyCategory?departments=",
				"Method=GET",
				"Resource=0",
				"RecContentType=text/html",
				"EncType=application/x-www-form-urlencoded",
				LAST);
				
			lr_end_sub_transaction("T03_Paginate getProductviewbyCategory", LR_AUTO);

			if ( lr_paramarr_len("uniqueId") != 0 ) {

				lr_param_sprintf ( "body", "%s", "{\"productId\":[\"" );

				for( itemCounter = 1; itemCounter <= lr_paramarr_len("uniqueId"); itemCounter++ ) {

					lr_param_sprintf( "body", "%s%s%s", lr_eval_string("{body}") , lr_paramarr_idx("uniqueId", itemCounter), "\"" );

					if (lr_paramarr_len("uniqueId") != itemCounter )
						lr_param_sprintf( "body", "%s%s", lr_eval_string("{body}"), ",\"" );
					else
						lr_param_sprintf( "body", "%s%s", lr_eval_string("{body}"), "]}" );

				}
			}

			if ( isLoggedIn == 1	) {
			lr_start_sub_transaction("T03_Paginate getListofDefaultWishlist", "T03_Paginate");
			web_custom_request("getListofDefaultWishlist",
				"URL=https://{api_host}/tcpstore/getListofDefaultWishlist",
				"Method=POST",
				"Resource=0",
				"RecContentType=application/json",
				"Mode=HTTP",
				"Body={body}",
				LAST);
			lr_end_sub_transaction("T03_Paginate getListofDefaultWishlist", LR_AUTO);
			}
		}

		lr_end_transaction ( "T03_Paginate" , LR_PASS ) ;

/*
		if (( lr_paramarr_len ( "paginationNextURL" ) > 0 ) ) {

			lr_think_time ( LINK_TT ) ;

		//	categoryPageCorrelations();
		//	subCategoryPageCorrelations();
		//	productDisplayPageCorrelations();
			index = rand ( ) % lr_paramarr_len( "paginationNextURL" ) + 1 ;
			lr_param_sprintf ("paginationURL", "http://%s%s&beginIndex=%s", lr_eval_string("{host}"), lr_paramarr_idx ( "paginationNextURL" , index ), lr_paramarr_idx ( "paginationPageIndex" , index ));
			lr_param_sprintf ("paginationIndex", "paginationPageIndex_%d", index);

			lr_start_transaction ( "T03_Paginate" ) ;

				web_submit_data("AjaxCatalogSearchResultView",
					"Action={paginationURL}",
					"Method=POST",
					"RecContentType=text/html",
					"Mode=HTML",
					ITEMDATA,
					"Name=searchResultsPageNum", "Value=-{paginationIndex}", ENDITEM,
					"Name=searchResultsView", "Value=", ENDITEM,
					"Name=searchResultsURL", "Value={paginationURL}", ENDITEM,
					"Name=objectId", "Value=", ENDITEM,
					"Name=requesttype", "Value=ajax", ENDITEM,
					LAST);

			lr_end_transaction ( "T03_Paginate" , LR_PASS ) ;
		}
*/
//	}

} // end pagination


void sortResults()
{
	//lr_message ( "in sortResults" ) ;
 	lr_think_time ( LINK_TT ) ;
	if (( lr_paramarr_len ( "sortURL" ) > 0 )) {
//		categoryPageCorrelations();
//		subCategoryPageCorrelations();
//		productDisplayPageCorrelations();
	//	index = rand ( ) % lr_paramarr_len( "sortURL" ) + 1 ;
	//	lr_param_sprintf ( "drillUrl" , "http://%s%s" , lr_eval_string("{host}") , lr_paramarr_idx ( "sortURL" , index ) ) ;
		lr_param_sprintf ( "drillUrl" , "http://%s%s" , lr_eval_string("{host}") , lr_paramarr_random ( "sortURL" ) ) ;

		lr_start_transaction ( "T03_Sort Results" ) ;
			webURL(lr_eval_string ( "{drillUrl}" ), "T03_Sort Results" );
		lr_end_transaction ( "T03_Sort Results" , LR_PASS ) ;
	} // end if
} // end sortResults

void sort()
{
// T03_Sort Results
// Optional step - Apply Sort. This step will be skipped if the return value is "pass",
// sort_by = lr_eval_string("{SORT_BY}") ;

//	randomPercent = atoi(lr_eval_string("{RANDOM_PERCENT}"));

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) < APPLY_SORT ){
		//lr_message ( "Applying Sort" ) ;
		sortResults();
		} // end if
	else {
		//lr_message ( "Skipping Sort" ) ;
		return;
		} // end if

} // end sort

void drillOneFacet()
{
	if ( lr_paramarr_len ( "subCategories" ) > 0 ) {

		lr_think_time ( LINK_TT ) ;
//		while ( checkOutfit() == LR_FAIL) {}
		web_reg_save_param ( "sizesFacetsURL", "LB=\"id\":\"ads_f16501_ntk_cs", "RB=\"},{\"displayName", "ORD=ALL" , "Convert=HTML_TO_URL", "NotFound=warning", LAST );//value":"ads_f16501_ntk_cs%3A%22YOUTH+12%22","extendedData
//		web_reg_save_param ( "colorFacetsURL", "LB=\"id\":\"ads_f12001_ntk_cs", "RB=\"},{\"displayName", "ORD=ALL" , "Convert=HTML_TO_URL", "NotFound=warning", LAST );//value":"ads_f12001_ntk_cs%3A%22YOUTH+12%22","extendedData
		web_reg_save_param ( "colorFacetsURL", "LB=\"id\":\"ads_f12001_ntk_cs", "RB=\"}", "ORD=ALL" , "Convert=HTML_TO_URL", "NotFound=warning", LAST );//value":"ads_f12001_ntk_cs%3A%22YOUTH+12%22","extendedData
//		web_reg_save_param( "pdpURL", "LB=,\"pdpUrl\":\"{store_home_page}p/", "RB=\",\"shortDescription", "ORD=All", "NotFound=Warning", LAST); //,"pdpUrl":"/us/p/Girls-Basic-Skinny-Jeans---True-Indigo-Wash-2069116-JP","shortDescription
//		web_reg_save_param_regexp ("ParamName=pdpURL",	"RegExp=pdpUrl\":\".*002F(.*?)\"",  "NotFound=Warning", SEARCH_FILTERS, "RequestUrl=*", LAST );

		web_reg_save_param( "productId", "LB=generalProductId\":\"", "RB=\"", "ORD=All", "NotFound=Warning", LAST); //"generalProductId":"
//		lr_param_sprintf ( "drillUrl" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ( "{subCategory}" ) ) ;
		lr_param_sprintf ( "drillUrl" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_paramarr_random ( "subCategories" ) ) ; //03232018

		lr_save_string("T03_Sub-Category Display", "mainTransaction");
		if( atoi(lr_eval_string("{akamaiRatio}")) <= (100 - NO_BROWSE_RATIO_SUBCATEGORY) ) {

			lr_save_string("akamai", "callerId2");
			API_SUB_TRANSACTION_SWITCH = 0;
			callAPI();
		} else {
			lr_save_string("T03_Sub-Category Display","callerId");
			lr_start_transaction ( "T03_Sub-Category Display" ) ;
			if ( webURL2(lr_eval_string ( "{drillUrl}" ), "T03_Sub-Category Display", "html" ) == LR_PASS)
			{
				callAPI();
				lr_end_transaction ( "T03_Sub-Category Display" , LR_PASS ) ;
				lr_save_string("", "mainTransaction");
			}
			else if ( atoi(lr_eval_string("{sizesFacetsURL}")) == 1 )
			{
				lr_fail_trans_with_error( lr_eval_string("T03_Sub-Category Display Text Check Failed - {drillUrl} AND no products found") ) ;
				lr_end_transaction ( "T03_Sub-Category Display" , LR_FAIL ) ;
			}
			else {
				lr_fail_trans_with_error( lr_eval_string("T03_Sub-Category Display Text Check Failed - {drillUrl}") ) ;
				lr_end_transaction ( "T03_Sub-Category Display" , LR_FAIL ) ;
			}
		}

		lr_save_string("categoryDisplay", "callerId");
	    if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 50 )
		{
			if (( lr_paramarr_len ( "sizesFacetsURL" ) > 0 )) {
				lr_save_string(lr_paramarr_random("sizesFacetsURL"), "facet");
				lr_param_sprintf ( "drillUrlOneFacet" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ( "{subCategory}?sizes=ads_f16501_ntk_cs{facet}" ) ) ;
			}
			else
				return;
		}
		else
		{
			if (( lr_paramarr_len ( "colorFacetsURL" ) > 0 )) {
				lr_save_string(lr_paramarr_random("colorFacetsURL"), "facet");
				lr_param_sprintf ( "drillUrlOneFacet" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ( "{subCategory}?colors=ads_f12001_ntk_cs{facet}" ) ) ;
			}
			else
				return;
		}

/****Needs revisiting PAVAN ROMAN 03212018*******/
//		web_reg_save_param( "pdpURL", "LB=,\"pdpUrl\":\"{store_home_page}p/", "RB=\",\"shortDescription", "ORD=All", "NotFound=Warning", LAST); //,"pdpUrl":"/us/p/Girls-Basic-Skinny-Jeans---True-Indigo-Wash-2069116-JP","shortDescription
//		web_reg_save_param( "productId", "LB=generalProductId\":\"", "RB=\"", "ORD=All", "NotFound=Warning", LAST); //"generalProductId":"
		//03232018
//		web_reg_save_param( "pdpURL", "LB=/{store_home_page}p/", "RB=\" class", "ORD=All", "NotFound=Warning", LAST); //,"pdpUrl":"/us/p/Girls-Basic-Skinny-Jeans---True-Indigo-Wash-2069116-JP","shortDescription
//		web_reg_save_param( "productId", "LB=generalProductId\":\"", "RB=\",\"name", "ORD=All", "NotFound=Warning", LAST); //"generalProductId":"
		
//		web_reg_save_param("totalProductsCount", "LB=totalProductsCount\":", "RB=,\"breadcrumbs", "NotFound=Warning", LAST);
		lr_start_transaction ( "T03_Facet Display_1facet" ) ;

		webURL(lr_eval_string ( "{drillUrlOneFacet}" ), "T03_Facet Display_1facet" );
		lr_save_string("T03_Facet Display_1facet", "mainTransaction");

//		callAPI();
		
//		if (atoi(lr_eval_string("{totalProductsCount}")) == 0)
//			lr_end_transaction ( "T03_Facet Display_1facet" , LR_FAIL ) ;
//		else
			lr_end_transaction ( "T03_Facet Display_1facet" , LR_PASS ) ;

	} // end if
} // end drillOneFacet


void drillTwoFacets()
{
	int nCount = 0;

	if ( lr_paramarr_len ( "subCategories" ) > 0 ) {

	 	lr_think_time ( LINK_TT ) ;
//		while ( checkOutfit() == LR_FAIL) {}
		web_reg_save_param ( "sizesFacetsURL", "LB=\"id\":\"ads_f16501_ntk_cs", "RB=\"},{\"displayName", "ORD=ALL" , "Convert=HTML_TO_URL", "NotFound=warning", LAST );//value":"ads_f16501_ntk_cs%3A%22YOUTH+12%22","extendedData
//		web_reg_save_param ( "colorFacetsURL", "LB=\"id\":\"ads_f12001_ntk_cs", "RB=\"},{\"displayName", "ORD=ALL" , "Convert=HTML_TO_URL", "NotFound=warning", LAST );//value":"ads_f12001_ntk_cs%3A%22YOUTH+12%22","extendedData
		web_reg_save_param ( "colorFacetsURL", "LB=\"id\":\"ads_f12001_ntk_cs", "RB=\"}", "ORD=ALL" , "Convert=HTML_TO_URL", "NotFound=warning", LAST );//value":"ads_f12001_ntk_cs%3A%22YOUTH+12%22","extendedData
		web_reg_save_param( "pdpURL", "LB=,\"pdpUrl\":\"{store_home_page}p/", "RB=\",\"shortDescription", "ORD=All", "NotFound=Warning", LAST); //,"pdpUrl":"/us/p/Girls-Basic-Skinny-Jeans---True-Indigo-Wash-2069116-JP","shortDescription

		web_reg_save_param( "productId", "LB=generalProductId\":\"", "RB=\"", "ORD=All", "NotFound=Warning", LAST); //"generalProductId":"
//		lr_param_sprintf ( "drillUrl" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ( "{subCategory}" ) ) ;
		lr_param_sprintf ( "drillUrl" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_paramarr_random ( "subCategories" ) ) ; //03232018
		
		lr_save_string("T03_Sub-Category Display", "mainTransaction");
		if( atoi(lr_eval_string("{akamaiRatio}")) <= (100 - NO_BROWSE_RATIO_SUBCATEGORY) ) {

			lr_save_string("akamai", "callerId2");
			API_SUB_TRANSACTION_SWITCH = 0;
			callAPI();

		} else {

			lr_start_transaction ( "T03_Sub-Category Display" ) ;
			if ( webURL2(lr_eval_string ( "{drillUrl}" ), "T03_Sub-Category Display", "html" ) == LR_PASS)
			{
				callAPI();
				lr_end_transaction ( "T03_Sub-Category Display" , LR_PASS ) ;
				lr_save_string("", "mainTransaction");
			}
			else if ( atoi(lr_eval_string("{sizesFacetsURL}")) == 1 )
			{
					lr_fail_trans_with_error( lr_eval_string("T03_Sub-Category Display Text Check Failed - {drillUrl} AND no products found") ) ;
					lr_end_transaction ( "T03_Sub-Category Display" , LR_FAIL ) ;
			}
			else {
					lr_fail_trans_with_error( lr_eval_string("T03_Sub-Category Display Text Check Failed - {drillUrl}") ) ;
					lr_end_transaction ( "T03_Sub-Category Display" , LR_FAIL ) ;
			}
		}

		lr_save_string("categoryDisplay", "callerId");
	    if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 80 )
		{
			if (( lr_paramarr_len ( "sizesFacetsURL" ) > 0 )) {
				lr_save_string(lr_paramarr_random("sizesFacetsURL"), "facet1");
				lr_param_sprintf ( "drillUrlTwoFacets" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ("{subCategory}?sizes=ads_f16501_ntk_cs{facet1}" ) ) ;
//				lr_param_sprintf ( "drillUrlTwoFacets" , "https://%s%s" , lr_eval_string("{host}"), lr_eval_string ("{subCategory}?sizes=ads_f16501_ntk_cs{facet1}" ) ) ;
				nCount = lr_paramarr_len("sizesFacetsURL");
				while( nCount >= 2)
				{
					lr_save_string(lr_paramarr_random("sizesFacetsURL"), "facet2");
					if (strcmp(lr_eval_string("{facet2}"), lr_eval_string("{facet1}")) != 0 )
					{	lr_param_sprintf ( "drillUrlTwoFacets" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ("{subCategory}?sizes=ads_f16501_ntk_cs{facet1}%2Csizes=ads_f16501_ntk_cs{facet2}" ) ) ;
//					{	lr_param_sprintf ( "drillUrlTwoFacets" , "https://%s%s" , lr_eval_string("{host}"), lr_eval_string ("{subCategory}?sizes=ads_f16501_ntk_cs{facet1}%2Csizes=ads_f16501_ntk_cs{facet2}" ) ) ;
						break;
					}
				}
			}
			else
				return;
		}
		else
		{
			if (( lr_paramarr_len ( "colorFacetsURL" ) > 0 )) {
				lr_save_string(lr_paramarr_random("colorFacetsURL"), "facet1");
				lr_param_sprintf ( "drillUrlTwoFacets" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ("{subCategory}?colors=ads_f12001_ntk_cs{facet1}" ) );
//				lr_param_sprintf ( "drillUrlTwoFacets" , "https://%s%s" , lr_eval_string("{host}"), lr_eval_string ("{subCategory}?colors=ads_f12001_ntk_cs{facet1}" ) );
				nCount = lr_paramarr_len("colorFacetsURL");
				while( nCount >= 2)
				{
					lr_save_string(lr_paramarr_random("colorFacetsURL"), "facet2");
					if (strcmp(lr_eval_string("{facet2}"), lr_eval_string("{facet1}")) != 0 )
					{	lr_param_sprintf ( "drillUrlTwoFacets" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ("{subCategory}?colors=ads_f12001_ntk_cs{facet1}%2Cads_f12001_ntk_cs{facet2}" ) ) ;
//					{	lr_param_sprintf ( "drillUrlTwoFacets" , "https://%s%s" , lr_eval_string("{host}"), lr_eval_string ("{subCategory}?colors=ads_f12001_ntk_cs{facet1}%2Cads_f12001_ntk_cs{facet2}" ) ) ;
						break;
					}
				}
			}
			else
				return;
		}

		productDisplayPageCorrelations();
//		web_reg_save_param( "pdpURL", "LB=,\"pdpUrl\":\"{store_home_page}p/", "RB=\",\"shortDescription", "ORD=All", "NotFound=Warning", LAST); //,"pdpUrl":"/us/p/Girls-Basic-Skinny-Jeans---True-Indigo-Wash-2069116-JP","shortDescription
//		web_reg_save_param( "productId", "LB=generalProductId\":\"", "RB=\"", "ORD=All", "NotFound=Warning", LAST); //"generalProductId":"
		//03232018
//		web_reg_save_param( "pdpURL", "LB=/{store_home_page}p/", "RB=\" class", "ORD=All", "NotFound=Warning", LAST); //,"pdpUrl":"/us/p/Girls-Basic-Skinny-Jeans---True-Indigo-Wash-2069116-JP","shortDescription
//		web_reg_save_param( "productId", "LB=generalProductId\":\"", "RB=\",\"name", "ORD=All", "NotFound=Warning", LAST); //"generalProductId":"
//		web_reg_save_param("totalProductsCount", "LB=totalProductsCount\":", "RB=,\"breadcrumbs", "NotFound=Warning", LAST);
		lr_start_transaction ( "T03_Facet Display_2facets" ) ;

			webURL(lr_eval_string ( "{drillUrlTwoFacets}" ), "T03_Facet Display_2facets" );

			lr_save_string("T03_Facet Display_2facets", "mainTransaction");
//			callAPI();

//		if (atoi(lr_eval_string("{totalProductsCount}")) == 0)
//			lr_end_transaction ( "T03_Facet Display_2facets" , LR_FAIL ) ;
//		else
			lr_end_transaction ( "T03_Facet Display_2facets" , LR_PASS ) ;

	} // end if
} // end drillTowFacets


void drillThreeFacets()
{
	int nCount = 0;
	//drillTwoFacets();

	if ( lr_paramarr_len ( "subCategories" ) > 0 ) {
		lr_think_time ( LINK_TT ) ;

//		while ( checkOutfit() == LR_FAIL) {}
		web_reg_save_param ( "sizesFacetsURL", "LB=\"id\":\"ads_f16501_ntk_cs", "RB=\"},{\"displayName", "ORD=ALL" , "Convert=HTML_TO_URL", "NotFound=warning", LAST );//value":"ads_f16501_ntk_cs%3A%22YOUTH+12%22","extendedData
//		web_reg_save_param ( "colorFacetsURL", "LB=\"id\":\"ads_f12001_ntk_cs", "RB=\"},{\"displayName", "ORD=ALL" , "Convert=HTML_TO_URL", "NotFound=warning", LAST );//value":"ads_f12001_ntk_cs%3A%22YOUTH+12%22","extendedData
		web_reg_save_param ( "colorFacetsURL", "LB=\"id\":\"ads_f12001_ntk_cs", "RB=\"}", "ORD=ALL" , "Convert=HTML_TO_URL", "NotFound=warning", LAST );//value":"ads_f12001_ntk_cs%3A%22YOUTH+12%22","extendedData
		lr_param_sprintf ( "drillUrl" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ( "{subCategory}" ) ) ;

		web_reg_save_param( "productId", "LB=generalProductId\":\"", "RB=\"", "ORD=All", "NotFound=Warning", LAST); //"generalProductId":"
//		lr_param_sprintf ( "drillUrl" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ( "{subCategory}" ) ) ;
		lr_param_sprintf ( "drillUrl" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_paramarr_random ( "subCategories" ) ) ; //03232018

		lr_save_string("T03_Sub-Category Display", "mainTransaction");
		if( atoi(lr_eval_string("{akamaiRatio}")) <= (100 - NO_BROWSE_RATIO_SUBCATEGORY) ) {

			lr_save_string("akamai", "callerId2");
			API_SUB_TRANSACTION_SWITCH = 0;
			callAPI();
		} else {

			lr_start_transaction ( "T03_Sub-Category Display" ) ;
			if ( webURL2(lr_eval_string ( "{drillUrl}" ), "T03_Sub-Category Display", "html" ) == LR_PASS)
			{
				callAPI();
				lr_end_transaction ( "T03_Sub-Category Display" , LR_PASS ) ;
				lr_save_string("", "mainTransaction");
			}
			else if ( atoi(lr_eval_string("{sizesFacetsURL}")) == 1 )
			{
					lr_fail_trans_with_error( lr_eval_string("T03_Sub-Category Display Text Check Failed - {drillUrl} AND no products found") ) ;
					lr_end_transaction ( "T03_Sub-Category Display" , LR_FAIL ) ;
			}
			else {
					lr_fail_trans_with_error( lr_eval_string("T03_Sub-Category Display Text Check Failed - {drillUrl}") ) ;
					lr_end_transaction ( "T03_Sub-Category Display" , LR_FAIL ) ;
			}
		}

		lr_save_string("categoryDisplay", "callerId");
	    if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 80 )
		{
			if (( lr_paramarr_len ( "sizesFacetsURL" ) > 0 )) {
				lr_save_string(lr_paramarr_random("sizesFacetsURL"), "facet1");
				lr_param_sprintf ( "drillUrlThreeFacets" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ("{subCategory}?sizes=ads_f16501_ntk_cs{facet1}" ) ) ;
//				lr_param_sprintf ( "drillUrlThreeFacets" , "https://%s%s" , lr_eval_string("{host}"),  lr_eval_string ("{subCategory}?sizes=ads_f16501_ntk_cs{facet1}" ) ) ;

				nCount = lr_paramarr_len("sizesFacetsURL");
				while( nCount >= 2)
				{
					lr_save_string(lr_paramarr_random("sizesFacetsURL"), "facet2");
					if (strcmp(lr_eval_string("{facet2}"), lr_eval_string("{facet1}")) != 0 )
					{	lr_param_sprintf ( "drillUrlThreeFacets" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ("{subCategory}?sizes=ads_f16501_ntk_cs{facet1}%2Csizes=ads_f16501_ntk_cs{facet2}" ) ) ;
//					{	lr_param_sprintf ( "drillUrlThreeFacets" , "https://%s%s" , lr_eval_string("{host}"),  lr_eval_string ("{subCategory}?sizes=ads_f16501_ntk_cs{facet1}%2Csizes=ads_f16501_ntk_cs{facet2}" ) ) ;
						break;
					}
				}
				nCount = lr_paramarr_len("sizesFacetsURL");
				while( nCount >= 3)
				{
					lr_save_string(lr_paramarr_random("sizesFacetsURL"), "facet3");
					if (strcmp(lr_eval_string("{facet3}"), lr_eval_string("{facet1}")) != 0 && strcmp(lr_eval_string("{facet3}"), lr_eval_string("{facet2}")) != 0 )
					{	lr_param_sprintf ( "drillUrlThreeFacets" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ("{subCategory}?sizes=ads_f16501_ntk_cs{facet1}%2Cads_f16501_ntk_cs{facet2}%2Cads_f16501_ntk_cs{facet3}" ) ) ;
						break;
					}
				}
			}
			else
				return;
		}
		else
		{
			if (( lr_paramarr_len ( "colorFacetsURL" ) > 0 )) {
				lr_save_string(lr_paramarr_random("colorFacetsURL"), "facet1");
				lr_param_sprintf ( "drillUrlThreeFacets" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ("{subCategory}?colors=ads_f12001_ntk_cs{facet1}" ) );
//				lr_param_sprintf ( "drillUrlThreeFacets" , "https://%s%s" , lr_eval_string("{host}"), lr_eval_string ("{subCategory}?colors=ads_f12001_ntk_cs{facet1}" ) );
				nCount = lr_paramarr_len("colorFacetsURL");
				while( nCount >= 2)
				{
					lr_save_string(lr_paramarr_random("colorFacetsURL"), "facet2");
					if (strcmp(lr_eval_string("{facet2}"), lr_eval_string("{facet1}")) != 0 )
					{	lr_param_sprintf ( "drillUrlThreeFacets" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ("{subCategory}?colors=ads_f12001_ntk_cs{facet1}%2Cads_f12001_ntk_cs{facet2}" ) ) ;
//					{	lr_param_sprintf ( "drillUrlThreeFacets" , "https://%s%s" , lr_eval_string("{host}"),  lr_eval_string ("{subCategory}?colors=ads_f12001_ntk_cs{facet1}%2Cads_f12001_ntk_cs{facet2}" ) ) ;
						break;
					}
				}
				nCount = lr_paramarr_len("colorFacetsURL");
				while( nCount >= 3)
				{
					lr_save_string(lr_paramarr_random("colorFacetsURL"), "facet3");
					if (strcmp(lr_eval_string("{facet3}"), lr_eval_string("{facet1}")) != 0 && strcmp(lr_eval_string("{facet3}"), lr_eval_string("{facet2}")) != 0 )
					{
						lr_param_sprintf ( "drillUrlThreeFacets" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ("{subCategory}?colors=ads_f12001_ntk_cs{facet1}%2Cads_f12001_ntk_cs{facet2}%2Cads_f12001_ntk_cs{facet3}" ) ) ;
//						lr_param_sprintf ( "drillUrlThreeFacets" , "https://%s%s" , lr_eval_string("{host}"), lr_eval_string ("{subCategory}?colors=ads_f12001_ntk_cs{facet1}%2Cads_f12001_ntk_cs{facet2}%2Cads_f12001_ntk_cs{facet3}" ) ) ;
						break;
					}
				}
			}
			else
				return;
		}

		productDisplayPageCorrelations();
//		web_reg_save_param( "pdpURL", "LB=,\"pdpUrl\":\"{store_home_page}p/", "RB=\",\"shortDescription", "ORD=All", "NotFound=Warning", LAST); //,"pdpUrl":"/us/p/Girls-Basic-Skinny-Jeans---True-Indigo-Wash-2069116-JP","shortDescription
//		web_reg_save_param( "productId", "LB=generalProductId\":\"", "RB=\"", "ORD=All", "NotFound=Warning", LAST); //"generalProductId":"
		//03232018
//		web_reg_save_param( "pdpURL", "LB=/{store_home_page}p/", "RB=\" class", "ORD=All", "NotFound=Warning", LAST); //,"pdpUrl":"/us/p/Girls-Basic-Skinny-Jeans---True-Indigo-Wash-2069116-JP","shortDescription
//		web_reg_save_param( "productId", "LB=generalProductId\":\"", "RB=\",\"name", "ORD=All", "NotFound=Warning", LAST); //"generalProductId":"
//		web_reg_save_param("totalProductsCount", "LB=totalProductsCount\":", "RB=,\"breadcrumbs", "NotFound=Warning", LAST);

		lr_start_transaction ( "T03_Facet Display_3facets" ) ;
/*
			web_url ( "T03_Facet Display_3facets"  ,
				 "URL={drillUrlThreeFacets}" ,
				 "Resource=0" ,
				 "Mode=HTML" ,
				 LAST ) ;
*/
			web_custom_request("T03_Facet Display_3facets" ,
				 "URL={drillUrlThreeFacets}" ,
				"Method=GET",
				"Resource=0",
				 "Mode=HTML" ,
				LAST);

			lr_save_string("T03_Facet Display_3facets", "mainTransaction");
//			callAPI();

			lr_save_string("", "drillUrlThreeFacets");

//		if (atoi(lr_eval_string("{totalProductsCount}")) == 0)
//			lr_end_transaction ( "T03_Facet Display_3facets" , LR_FAIL ) ;
//		else
			lr_end_transaction ( "T03_Facet Display_3facets" , LR_PASS ) ;

	}// end if
}// end drillThreeFacets

int checkOutfit()
{
    extern char * strtok(char * string, const char * delimiters );
    char path[1000] = "";
    char separators[] = "-";
    char * token;
//    char fullpath[1024];
//    int counter, index = 0;

//	lr_param_sprintf ( "drillUrl" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_paramarr_random ( "subCategories" ) ) ;

	lr_save_string(lr_paramarr_random ( "subCategories" ), "subCategory");
    strcpy(path, lr_eval_string("{subCategory}") );

    // Get the first token
    token = (char *)strtok(path, separators);
    if (!token) {
        lr_output_message ("No tokens found in string!");
    }

    while (token != NULL ) {
//        lr_output_message ("%s", token );
		// Get the next token
        token = (char *)strtok(NULL, separators);

        if(token != NULL) {
	        if (strcmp(token,"outfits") == 0) {
				return LR_FAIL;
	        }
        }
    }

	return LR_PASS;

}

int getSubCategories()
{
    extern char * strtok(char * string, const char * delimiters );
    char path[9999] = "";
    char separators[] = "\"";
    char * token;
//    char fullpath[1024];
    int counterX, counterY, counterZ = 0;
    char *arr[20]; // = malloc(sizeof(int)*20);

    strcpy(path, lr_eval_string("{subcategory}") );

    // Get the first token
    token = (char *)strtok(path, separators);
    if (!token) {
        lr_output_message ("No tokens found in string!");
    }

    while (token != NULL ) {
        token = (char *)strtok(NULL, separators);
//        lr_output_message ("%s", token );
//        lr_save_string(token, "url");

		if(token != NULL) { // && strcmp(token,"url") == 0) {
	        if (strcmp(token,"url") == 0) {
				token = (char *)strtok(NULL, separators);
				token = (char *)strtok(NULL, separators);
				counterX++;

				arr[counterX] = token;
				lr_message("CounterX is %d", counterX);
				if (counterX == 1) {
					lr_param_sprintf("category", "%s", token);
				} else {
					counterZ++;
					lr_param_sprintf("subcategories", "subcategories_%d", counterZ);
					lr_param_sprintf(lr_eval_string("{subcategories}"), "%s", token );
					lr_save_int(counterZ, "subcategories_count");
				}
			}
        }
    }
    lr_message("parameter length is %d", atoi(lr_eval_string("{subcategories_count}")) );
    //lr_save_string(lr_paramarr_random("subcategories"), "subCategory");
    while ( checkOutfit() == LR_FAIL) {}
	return LR_PASS;
}

void drillSubCategory()
{
 	lr_think_time ( LINK_TT ) ;

//	if (( lr_paramarr_len ( "subCategories" ) > 0 )) {

		//lr_continue_on_error(1);

//		while ( checkOutfit() == LR_FAIL) {}
		lr_param_sprintf ( "drillUrl" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string("{subCategory}") ) ;
		web_reg_save_param( "pdpURL", "LB=,\"pdpUrl\":\"{store_home_page}p/", "RB=\",\"shortDescription", "ORD=All", "NotFound=Warning", LAST); //,"pdpUrl":"/us/p/Girls-Basic-Skinny-Jeans---True-Indigo-Wash-2069116-JP","shortDescription
		web_reg_save_param( "productId", "LB=generalProductId\":\"", "RB=\"", "ORD=All", "NotFound=Warning", LAST); //"generalProductId":"
		web_reg_save_param("totalProductsCount", "LB=totalProductsCount\":", "RB=,\"breadcrumbs", "NotFound=Warning", LAST);

//		productDisplayPageCorrelations(); 03212018
		lr_save_string(lr_eval_string("{subCategory}"), "category"); //for pagination
		lr_save_string("T03_Sub-Category Display", "mainTransaction");
		if( atoi(lr_eval_string("{akamaiRatio}")) <= (100 - NO_BROWSE_RATIO_SUBCATEGORY) ) {

			lr_save_string("akamai", "callerId2");
			API_SUB_TRANSACTION_SWITCH = 0;
			callAPI();

		} else {

			lr_start_transaction ( "T03_Sub-Category Display" ) ;

	//		webURL(lr_eval_string ( "{drillUrl}" ), "T03_Sub-Category Display" );

			if ( webURL2(lr_eval_string ( "{drillUrl}" ), "T03_Sub-Category Display", "html" ) == LR_PASS)
			{

				lr_save_string("categoryDisplay", "callerId");
				callAPI();
				lr_end_transaction ( "T03_Sub-Category Display" , LR_PASS ) ;
			}
			else if ( atoi(lr_eval_string("{noProducts}")) == 1 )
			{
					lr_fail_trans_with_error( lr_eval_string("T03_Sub-Category Display Text Check Failed - {drillUrl} AND no products found") ) ;
					lr_end_transaction ( "T03_Sub-Category Display" , LR_FAIL ) ;
			}
			else {
					lr_fail_trans_with_error( lr_eval_string("T03_Sub-Category Display Text Check Failed - {drillUrl}") ) ;
					lr_end_transaction ( "T03_Sub-Category Display" , LR_FAIL ) ;
			}
		}

//	} // end if

	//lr_continue_on_error(0);

}// end drillSubCategory



int getValueOf()
{
    extern char * strtok(char * string, const char * delimiters );
    char path[9999] = "";
    char separators[] = "\"";
    char * token;
    strcpy(path, lr_eval_string("{getProductId}") );

    // Get the first token
    token = (char *)strtok(path, separators);
    if (!token) {
        lr_output_message ("No tokens found in string!");
    }

    while (token != NULL ) {
//        lr_output_message ("%s", token );
//        lr_save_string(token, "generalProductId");
        token = (char *)strtok(NULL, separators);
//        lr_output_message ("%s", token );
        lr_save_string(token, "generalProductId");
        break;
    }
	return LR_PASS;

}

void productDetailViewBOPIS()
{
 	lr_think_time ( LINK_TT ) ;

//	if (( lr_paramarr_len ( "pdpURL" ) > 0 )) {

//		productDisplayPageCorrelations();

		//lr_continue_on_error(0);

		lr_param_sprintf ( "drillUrl" , "http://%s%s" , lr_eval_string("{host}") , lr_eval_string("/us/p/{randCatEntryId_BOPIS}") ) ;
//		lr_param_sprintf ( "drillUrl" , "http://%s%s" , lr_eval_string("{host}") , "/us/p/447051" ) ; //old 11/06
//		lr_param_sprintf ( "drillUrl" , "http://%s%s%s" , lr_eval_string("{host}") , lr_paramarr_random ( "pdpURL" ) ) ;
//		addToCartCorrelations();
		web_reg_save_param_regexp ("ParamName=getProductId",
							"RegExp=navigationTree.*?generalProductId(.*?)name\"",
							"NotFound=Warning",
							SEARCH_FILTERS,
							"RequestUrl=*",	LAST );
//		web_add_header("tcpState", "NJ");
		lr_start_transaction ( "T04_Product Display Page" ) ;

		if (webURL2(lr_eval_string ( "{drillUrl}" ), "T04_Product Display Page", "satellite.pageBottom") == LR_PASS) {

			lr_save_string("T04_Product Display Page", "mainTransaction");
			lr_save_string("productDisplay", "callerId");
			callAPI();

			lr_save_string("PDP","PDP_or_PQV");
			lr_end_transaction ( "T04_Product Display Page" , LR_PASS ) ;

		}
		else {
			lr_fail_trans_with_error( lr_eval_string("T04_Product Display Text Check Failed - {drillUrl} - {storeId}") ) ;
			lr_end_transaction ( "T04_Product Display Page" , LR_FAIL ) ;
		}

		//lr_continue_on_error(1);

//	}

}


int productDetailView()
{
 	lr_think_time ( LINK_TT ) ;

//	if (( lr_paramarr_len ( "pdpURL" ) > 0 )) {
	if (( lr_paramarr_len ( "productId" ) > 0 )) {

//		index = rand ( ) % lr_paramarr_len( "pdpURL" ) + 1 ;
//		lr_save_string( lr_paramarr_idx("pdpURL", index), "pdpURL") ;
//		lr_save_string( lr_paramarr_idx("productId", index), "productId") ;

		index = rand ( ) % lr_paramarr_len( "productId" ) + 1 ;
		lr_save_string( lr_paramarr_idx("productId", index), "pdpURL") ;
		lr_save_string( lr_paramarr_idx("productId", index), "productId") ;

//01262017		lr_save_string( lr_eval_string("{pdpURLData}") , "pdpURL") ;

		lr_param_sprintf ( "drillUrl" , "http://%s%s%s" , lr_eval_string("{host}") , lr_eval_string("{store_home_page}p/"), lr_eval_string( "{pdpURL}" ) ) ;
		//lr_continue_on_error(1);	//"skuId": "790846",

		//web_reg_save_param( "productId", "LB=generalProductId\":\"", "RB=\"", "ORD=All", "NotFound=Warning", LAST); //"generalProductId":"


		lr_save_string("T04_Product Display Page", "mainTransaction");

		lr_save_string("productDisplay", "callerId");

		if( atoi(lr_eval_string("{akamaiRatio}")) <= (100 - NO_BROWSE_RATIO_PDP) ) {

			lr_save_string("akamai", "callerId2");
			API_SUB_TRANSACTION_SWITCH = 0;
			callAPI();

		} else {
			web_reg_save_param_regexp ("ParamName=getProductId",
					"RegExp=navigationTree.*?generalProductId(.*?)name\"",
					"NotFound=Warning",
					SEARCH_FILTERS,
					"RequestUrl=*",	LAST );

			lr_start_transaction ( "T04_Product Display Page" ) ;

			if (webURL2(lr_eval_string ( "{drillUrl}" ), "T04_Product Display Page", "satellite.pageBottom") == LR_PASS) {

				getValueOf();
				lr_save_string("productDisplay", "callerId");
				callAPI();

				if ( isLoggedIn == 1 && atoi(lr_eval_string("{RANDOM_PERCENT}")) <= PDP_to_WL )
				{
					lr_start_sub_transaction("T04_Product Display Page_addOrUpdateWishlist", "T04_Product Display Page");
					addHeader();
					web_add_header("Content-Type","application/json");
					web_add_header("addItem", "true");
					web_custom_request("addOrUpdateWishlist",
						"URL=https://{api_host}/addOrUpdateWishlist",
						"Method=PUT",
						"Resource=0",
						"RecContentType=application/json",
						"Mode=HTTP",
						"Body={\"item\":[{\"productId\":\"{generalProductId}\",\"quantityRequested\":\"1\",\"isProduct\":\"TRUE\"}]}",  //generalProductId
	//					"Body={\"item\":[{\"productId\":\"{productId}\",\"quantityRequested\":\"1\",\"isProduct\":\"TRUE\"}]}",  //generalProductId
						LAST);
					lr_end_sub_transaction("T04_Product Display Page_addOrUpdateWishlist", LR_AUTO);

					lr_start_sub_transaction("T04_Product Display Page_tcpproduct/getSKUInventoryandProductCounterDetails", "T04_Product Display Page");
					addHeader();
					web_add_header("productId", lr_eval_string("{productId}"));
					web_url("getSKUInventoryandProductCounterDetails",
						"URL=https://{api_host}/tcpproduct/getSKUInventoryandProductCounterDetails",
						"Resource=1",
						"RecContentType=application/json",
						LAST);
					lr_end_sub_transaction("T04_Product Display Page_tcpproduct/getSKUInventoryandProductCounterDetails", LR_AUTO);
				}

				lr_save_string("PDP","PDP_or_PQV");
				lr_end_transaction ( "T04_Product Display Page" , LR_PASS ) ;

			} else {
				lr_fail_trans_with_error( lr_eval_string("T04_Product Display Text Check Failed - {drillUrl} - {storeId}") ) ;
				lr_end_transaction ( "T04_Product Display Page" , LR_FAIL ) ;
				return LR_FAIL;
			}
		}
		//lr_continue_on_error(0);
	}
	else {
		return LR_FAIL;
	}
	return 0;
} // end productDisplay


void productQuickView()
{
 	lr_think_time ( LINK_TT ) ;

	if (( lr_paramarr_len ( "quickviewURL" ) > 0 )) {

		productDisplayPageCorrelations();

		lr_param_sprintf ( "drillUrl" , "http://%s%s" , lr_eval_string("{host}") , lr_paramarr_random ( "quickviewURL" ) ) ;

		lr_start_transaction ( "T04_Product Quickview Page" ) ;

		webURL(lr_eval_string ( "{drillUrl}" ), "T04_Product Quickview Page" );

		lr_start_sub_transaction ("T04_Product Quickview Page - GetSwatchesAndSizeInfo", "T04_Product Quickview Page" );

		web_custom_request("GetSwatchesAndSizeInfo",
			"URL=http://{host}/webapp/wcs/stores/servlet/GetSwatchesAndSizeInfo?langId=-1&storeId={storeId}&catalogId={catalogId}&productId={skuSelectionProductID}",
			"Method=GET",
			"Resource=0",
			"RecContentType=text/html",
			"Snapshot=t18.inf",
			"Mode=HTML",
			"EncType=application/x-www-form-urlencoded",
			LAST);

		lr_end_sub_transaction ( "T04_Product Quickview Page - GetSwatchesAndSizeInfo", LR_PASS );

//		addToCartCorrelations();
//		web_reg_save_param("atc_catentryIds", "LB=\t\t\t{ \"", "RB=\" : \"", "ORD=All", "NotFound=Warning", LAST);
//		web_reg_save_param("catentryidQTY", "LB=\" : \"", "RB=\" }\r\n", "ORD=All", "NotFound=Warning", LAST);
		web_reg_save_param_regexp( "ParamName=atc_catentryIds" ,
	                          "RegExp={ \"(.*?)\" : \"([1-9][0-9]*)\"" ,
	                           //"RegExp=\"catentry_id\" : \"(.*?)\",\r\n\t\t\t\t\t\t\t\t\t\t\t\"item_sku\" : \"(.*?)\",\r\n\t\t\t\t\t\t\t\t\t\t\t\"qty\" : \"([1-9][0-9]*)\"" ,  //used on PDP
			                   "Ordinal=ALL" ,
			                   "Notfound=warning" ,
			                   SEARCH_FILTERS ,
			                   "Group=1" ,
			                   LAST ) ;

//		web_reg_save_param("lowCatEntryId", "LB=\"catentry_id\" : \"", "RB=\"", "ORD=ALL", "NotFound=Warning", LAST); //"catentry_id" : "771570"
//		web_reg_save_param("lowQty", "LB=\"qty\" : \"", "RB=\"", "ORD=ALL", "NotFound=Warning", LAST); //"qty" : "8"

//pdp https://uatlive1.childrensplace.com/shop/us/p/girls-clothing/girls-denim-bottoms/girls-bootcut-jeans/Girls-Basic-Bootcut-Jeans---Odyssey-Wash-1150077-B4
	   lr_start_sub_transaction ("T04_Product Quickview Page - TCPGetSKUDetailsView", "T04_Product Quickview Page" );

		web_custom_request("TCPGetSKUDetailsView",
			"URL=http://{host}/webapp/wcs/stores/servlet/TCPGetSKUDetailsView?langId=-1&storeId={storeId}&catalogId={catalogId}&productId={skuSelectionProductID}",
			"Method=GET",
			"Resource=0",
			"RecContentType=text/html",
			"Snapshot=t18.inf",
			"Mode=HTML",
			"EncType=application/x-www-form-urlencoded",
			LAST);

		lr_end_sub_transaction ( "T04_Product Quickview Page - TCPGetSKUDetailsView", LR_PASS );


		if ( isLoggedIn == 1 )
			TCPGetWishListForUsersView();//1204 5pm to reduce TCPGetWishListForUsersView by 75k

		lr_save_string("PQV","PDP_or_PQV");

		lr_end_transaction ( "T04_Product Quickview Page" , LR_PASS ) ;

		//lr_continue_on_error(0);

	} // end if

} // end ProductQuickView

void productQuickviewService() //Add this call right after a Product Display Page Call
{
	web_reg_find("SaveCount=swatchesAndSizeInfo", "Text/IC=itemTCPBogo",LAST);

	//lr_continue_on_error(1);

	lr_start_transaction ("T04_Product Quickview Service - GetSwatchesAndSizeInfo" );

		web_custom_request("GetSwatchesAndSizeInfo",
			"URL=http://{host}/webapp/wcs/stores/servlet/GetSwatchesAndSizeInfo?storeId={storeId}&productId={skuSelectionProductID}",
			"Method=GET",
			"Mode=HTML",
			LAST);

	if(atoi(lr_eval_string("{swatchesAndSizeInfo}")) > 0)
		lr_end_transaction ( "T04_Product Quickview Service - GetSwatchesAndSizeInfo", LR_PASS );
	else {
		lr_param_sprintf ( "failedPageUrl" , "T03_Sub-Category Display Failure : %s", lr_eval_string("http://{host}/webapp/wcs/stores/servlet/GetSwatchesAndSizeInfo?storeId={storeId}&productId={skuSelectionProductID}") ) ;
		lr_fail_trans_with_error( lr_eval_string("T04_Product Display Text Check Failed - {failedPageUrl} - {storeId}") ) ;
		lr_end_transaction ( "T04_Product Quickview Service - GetSwatchesAndSizeInfo", LR_FAIL );
	}
	//lr_continue_on_error(0);

	web_reg_find("SaveCount=detailsView", "Text/IC=inventoryFlag",LAST);

	lr_start_transaction ("T04_Product Quickview Service - TCPGetSKUDetailsView" );

		web_custom_request("TCPGetSKUDetailsView",
			"URL=http://{host}/webapp/wcs/stores/servlet/TCPGetSKUDetailsView?storeId={storeId}&catalogId={catalogId}&langId=-1&productId={skuSelectionProductID}",
			"Method=GET",
			"Mode=HTML",
			LAST);


	if(atoi(lr_eval_string("{detailsView}")) > 0)
		lr_end_transaction ( "T04_Product Quickview Service - TCPGetSKUDetailsView", LR_PASS );
	else
		lr_end_transaction ( "T04_Product Quickview Service - TCPGetSKUDetailsView", LR_FAIL );

}

int productDisplay()
{
	if (productDetailView() == LR_FAIL)
	    return LR_FAIL;
/*
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= PDP )
	{
//		if (( lr_paramarr_len ( "pdpURL" ) > 0 ))
			productDetailView();
//		else
//			productQuickView();
	}
	else
	{
		//disabled PQV 03202018
//		if (( lr_paramarr_len ( "quickviewURL" ) > 0 ))
//			productQuickView(); //calls the TCPGetWishListForUsersView 1x
//		else
			productDetailView();
	}
*/
	return 0;
} // end ProductDisplay

void productDisplayBOPIS()
{
	productDetailViewBOPIS(); //calls the TCPGetWishListForUsersView 2x
}


void switchColor()
{
} // end switchColor

void emailSignUp()
{
	lr_start_transaction ( "T16_Submit Shipping Address" ) ;
	lr_end_transaction ( "T16_Submit Shipping Address" , LR_PASS) ;

} // end emailSignUp


void StoreLocator()
{
 	lr_think_time ( LINK_TT ) ;

	lr_start_transaction("T22_StoreLocator");

		lr_save_string("T22_StoreLocator", "mainTransaction" );

		lr_start_sub_transaction("T22_StoreLocator_store-locator", lr_eval_string("{mainTransaction}") );

			web_url("StoreLocator",
			"URL=http://{host}/{country}/store-locator",
			"TargetFrame=",
			"Resource=0",
			"RecContentType=text/html",
			"Mode=HTML",
			LAST);

//			tcp_api2("tcporder/getXAppConfigValues", "GET", lr_eval_string("{mainTransaction}") );
			tcp_api2("payment/getRegisteredUserDetailsInfo", "GET",  lr_eval_string("{mainTransaction}") );
//			tcp_api2("getESpot", "GET", lr_eval_string("{mainTransaction}") );
//			tcp_api2("tcporder/getAllCoupons", "GET",  lr_eval_string("{mainTransaction}") );
			tcp_api2("tcpproduct/getPriceDetails", "GET",  lr_eval_string("{mainTransaction}") );

			web_add_header("locStore", "False");
			web_add_header("pageName", "orderSummary");
			tcp_api2("getOrderDetails", "GET", lr_eval_string("{mainTransaction}") );
//			api_getESpot_second();
//			web_add_header("action", "get");
//			tcp_api2("tcporder/getFavouriteStoreLocation", "GET",  lr_eval_string("{mainTransaction}") );

		lr_end_sub_transaction("T22_StoreLocator_store-locator", LR_AUTO);

		lr_start_sub_transaction("T22_StoreLocator_Find", "T22_StoreLocator" );
			addHeader();
			web_add_header("latitude", "40.7879061");
			web_add_header("longitude", "-74.0444976");
			web_add_header("maxItems", "5");
			web_add_header("radius", "75");
//			web_reg_save_param("uniqueId", "LB=\\t\\t\\t\\t\"uniqueId\": ", "RB=\"", "ORD=1", "NotFound=Warning", LAST); //"uniqueId": "114298"
		web_reg_save_param_json("ParamName=uniqueId", "QueryString=$.PhysicalStore..uniqueId", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
			web_reg_find("TEXT/IC=uniqueId", "SaveCount=apiCheck", LAST);
			web_url("findStoresbyLatitudeandLongitude",
				"URL=https://{api_host}/tcpproduct/findStoresbyLatitudeandLongitude",
				"Resource=0",
				"RecContentType=application/json",
				LAST);
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction("T22_StoreLocator_Find", LR_FAIL);
		else
			lr_end_sub_transaction("T22_StoreLocator_Find", LR_AUTO);

		lr_save_string("T22_StoreLocator_Set_As_Favorite_Store", "mainTransaction" );
		lr_start_sub_transaction("T22_StoreLocator_Set_As_Favorite_Store", "T22_StoreLocator" );

			addHeader();
//			web_add_header("Content-Type", "application/json");
			web_add_header("fromPage", "StoreLocator");
			web_add_header("action", "add");
			web_add_header("storeLocId", lr_eval_string("{uniqueId}") );
//			web_add_header("userId", "-1002" );
			web_add_header("userId", lr_eval_string("{userId}") );
			web_reg_find("TEXT/IC={uniqueId}", "SaveCount=apiCheck", LAST);
			web_custom_request("addFavouriteStoreLocation",
				"URL=https://{api_host}/tcporder/addFavouriteStoreLocation",
				"Method=POST",
				"Resource=0",
				"RecContentType=application/json",
				"Mode=HTTP",
				"EncType=application/json",
				"Body={}",
				LAST);
			if (atoi(lr_eval_string("{apiCheck}")) == 0 )
				lr_end_sub_transaction("T22_StoreLocator_Set_As_Favorite_Store", LR_FAIL);
			else
				lr_end_sub_transaction("T22_StoreLocator_Set_As_Favorite_Store", LR_AUTO);

/* // this is for see store details - need to get thr ff from findStoresbyLatitudeandLongitude

				"displayStoreName": "secaucus outlet"
				"stateOrProvinceName": "NJ",
				"city": "secaucus",
				"postalCode": "07094                                   ",
				"uniqueId": "114298"
*/
		lr_save_string("T22_StoreLocator_See_StoreDetails", "mainTransaction" );
		lr_start_sub_transaction("T22_StoreLocator_See_StoreDetails", "T22_StoreLocator" );

			web_url("See Store Details",
				"URL=https://{host}/us/store/secaucusoutlet-nj-secaucus-07094-114298",
				"Resource=0",
				"RecContentType=text/html",
				"Mode=HTTP",
					LAST);

//			tcp_api2("tcporder/getXAppConfigValues", "GET", lr_eval_string("{mainTransaction}") );
			tcp_api2("payment/getRegisteredUserDetailsInfo", "GET",  lr_eval_string("{mainTransaction}") );
//			tcp_api2("getESpot", "GET", lr_eval_string("{mainTransaction}") );
//			tcp_api2("tcporder/getAllCoupons", "GET",  lr_eval_string("{mainTransaction}") );
			tcp_api2("tcpproduct/getPriceDetails", "GET",  lr_eval_string("{mainTransaction}") );
			web_add_header("locStore", "False");
			web_add_header("pageName", "orderSummary");
			tcp_api2("getOrderDetails", "GET", lr_eval_string("{mainTransaction}") );
//			api_getESpot_second();
//			web_add_header("action", "get");
//			tcp_api2("tcporder/getFavouriteStoreLocation", "GET",  lr_eval_string("{mainTransaction}") );

		lr_end_sub_transaction("T22_StoreLocator_See_StoreDetails", LR_AUTO);


	lr_end_transaction("T22_StoreLocator", LR_AUTO);


}

void TCPGetWishListForUsersViewBrowse()
{
	lr_think_time ( LINK_TT ) ;

	lr_start_transaction("T25_Common_S01_TCPGetWishListForUsersView");

	web_custom_request("TCPGetWishListForUsersView",
		"URL=http://{host}/webapp/wcs/stores/servlet/TCPGetWishListForUsersView?tcpCallBack=jQuery1113014590106982485151_1473881708524&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&sortBy=&_=1473881708525",
		"Method=GET",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		"EncType=application/x-www-form-urlencoded",
		LAST);

	lr_end_sub_transaction("T25_Common_S01_TCPGetWishListForUsersView", LR_AUTO);

}
void TCPIShippingView()
{
	lr_think_time ( LINK_TT ) ;

	lr_start_transaction("T27_TCPIShippingView");

	web_custom_request("TCPIShippingView",
		"URL=https://{host}/shop/TCPIShippingView?langId=-1&storeId={storeId}&catalogId={catalogId}",
		"Method=GET",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		"EncType=application/x-www-form-urlencoded",
		LAST);

	lr_end_transaction("T27_TCPIShippingView", LR_AUTO);

	TCPGetWishListForUsersViewBrowse();

}
