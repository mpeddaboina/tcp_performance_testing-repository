# 1 "e:\\performance\\scripts\\2017\\03_perf\\ecommerce\\ca_checkout\\\\combined_CA_Checkout.c"
# 1 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h" 1
 
 












 











# 103 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"








































































	

 



















 
 
 
 
 


 
 
 
 
 
 














int     lr_start_transaction   (char * transaction_name);
int lr_start_sub_transaction          (char * transaction_name, char * trans_parent);
long lr_start_transaction_instance    (char * transaction_name, long parent_handle);
int   lr_start_cross_vuser_transaction		(char * transaction_name, char * trans_id_param); 



int     lr_end_transaction     (char * transaction_name, int status);
int lr_end_sub_transaction            (char * transaction_name, int status);
int lr_end_transaction_instance       (long transaction, int status);
int   lr_end_cross_vuser_transaction	(char * transaction_name, char * trans_id_param, int status);


 
typedef char* lr_uuid_t;
 



lr_uuid_t lr_generate_uuid();

 


int lr_generate_uuid_free(lr_uuid_t uuid);

 



int lr_generate_uuid_on_buf(lr_uuid_t buf);

   
# 266 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"
int lr_start_distributed_transaction  (char * transaction_name, lr_uuid_t correlator, long timeout  );

   







int lr_end_distributed_transaction  (lr_uuid_t correlator, int status);


double lr_stop_transaction            (char * transaction_name);
double lr_stop_transaction_instance   (long parent_handle);


void lr_resume_transaction           (char * trans_name);
void lr_resume_transaction_instance  (long trans_handle);


int lr_update_transaction            (const char *trans_name);


 
void lr_wasted_time(long time);


 
int lr_set_transaction(const char *name, double duration, int status);
 
long lr_set_transaction_instance(const char *name, double duration, int status, long parent_handle);


int   lr_user_data_point                      (char *, double);
long lr_user_data_point_instance                   (char *, double, long);
 



int lr_user_data_point_ex(const char *dp_name, double value, int log_flag);
long lr_user_data_point_instance_ex(const char *dp_name, double value, long parent_handle, int log_flag);


int lr_transaction_add_info      (const char *trans_name, char *info);
int lr_transaction_instance_add_info   (long trans_handle, char *info);
int lr_dpoint_add_info           (const char *dpoint_name, char *info);
int lr_dpoint_instance_add_info        (long dpoint_handle, char *info);


double lr_get_transaction_duration       (char * trans_name);
double lr_get_trans_instance_duration    (long trans_handle);
double lr_get_transaction_think_time     (char * trans_name);
double lr_get_trans_instance_think_time  (long trans_handle);
double lr_get_transaction_wasted_time    (char * trans_name);
double lr_get_trans_instance_wasted_time (long trans_handle);
int    lr_get_transaction_status		 (char * trans_name);
int	   lr_get_trans_instance_status		 (long trans_handle);

 



int lr_set_transaction_status(int status);

 



int lr_set_transaction_status_by_name(int status, const char *trans_name);
int lr_set_transaction_instance_status(int status, long trans_handle);


typedef void* merc_timer_handle_t;
 

merc_timer_handle_t lr_start_timer();
double lr_end_timer(merc_timer_handle_t timer_handle);


 
 
 
 
 
 











 



int   lr_rendezvous  (char * rendezvous_name);
 




int   lr_rendezvous_ex (char * rendezvous_name);



 
 
 
 
 
char *lr_get_vuser_ip (void);
void   lr_whoami (int *vuser_id, char ** sgroup, int *scid);
char *	  lr_get_host_name (void);
char *	  lr_get_master_host_name (void);

 
long     lr_get_attrib_long	(char * attr_name);
char *   lr_get_attrib_string	(char * attr_name);
double   lr_get_attrib_double      (char * attr_name);

char * lr_paramarr_idx(const char * paramArrayName, unsigned int index);
char * lr_paramarr_random(const char * paramArrayName);
int    lr_paramarr_len(const char * paramArrayName);

int	lr_param_unique(const char * paramName);
int lr_param_sprintf(const char * paramName, const char * format, ...);


 
 
static void *ci_this_context = 0;






 








void lr_continue_on_error (int lr_continue);
char *   lr_decrypt (const char *EncodedString);


 
 
 
 
 
 



 







 















void   lr_abort (void);
void lr_exit(int exit_option, int exit_status);
void lr_abort_ex (unsigned long flags);

void   lr_peek_events (void);


 
 
 
 
 


void   lr_think_time (double secs);

 


void lr_force_think_time (double secs);


 
 
 
 
 



















int   lr_msg (char * fmt, ...);
int   lr_debug_message (unsigned int msg_class,
									    char * format,
										...);
# 505 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"
void   lr_new_prefix (int type,
                                 char * filename,
                                 int line);
# 508 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"
int   lr_log_message (char * fmt, ...);
int   lr_message (char * fmt, ...);
int   lr_error_message (char * fmt, ...);
int   lr_output_message (char * fmt, ...);
int   lr_vuser_status_message (char * fmt, ...);
int   lr_error_message_without_fileline (char * fmt, ...);
int   lr_fail_trans_with_error (char * fmt, ...);

 
 
 
 
 
# 532 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"

 
 
 
 
 





int   lr_next_row ( char * table);
int lr_advance_param ( char * param);



														  
														  

														  
														  

													      
 


char *   lr_eval_string (char * str);
int   lr_eval_string_ext (const char *in_str,
                                     unsigned long const in_len,
                                     char ** const out_str,
                                     unsigned long * const out_len,
                                     unsigned long const options,
                                     const char *file,
								     long const line);
# 566 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"
void   lr_eval_string_ext_free (char * * pstr);

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
int lr_param_increment (char * dst_name,
                              char * src_name);
# 589 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"













											  
											  

											  
											  
											  

int	  lr_save_var (char *              param_val,
							  unsigned long const param_val_len,
							  unsigned long const options,
							  char *			  param_name);
# 613 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"
int   lr_save_string (const char * param_val, const char * param_name);



int   lr_set_custom_error_message (const char * param_val, ...);

int   lr_remove_custom_error_message ();


int   lr_free_parameter (const char * param_name);
int   lr_save_int (const int param_val, const char * param_name);
int   lr_save_timestamp (const char * tmstampParam, ...);
int   lr_save_param_regexp (const char *bufferToScan, unsigned int bufSize, ...);

int   lr_convert_double_to_integer (const char *source_param_name, const char * target_param_name);
int   lr_convert_double_to_double (const char *source_param_name, const char *format_string, const char * target_param_name);

 
 
 
 
 
 
# 692 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"
void   lr_save_datetime (const char *format, int offset, const char *name);









 











 
 
 
 
 






 



char * lr_error_context_get_entry (char * key);

 



long   lr_error_context_get_error_id (void);


 
 
 

int lr_table_get_rows_num (char * param_name);

int lr_table_get_cols_num (char * param_name);

char * lr_table_get_cell_by_col_index (char * param_name, int row, int col);

char * lr_table_get_cell_by_col_name (char * param_name, int row, const char* col_name);

int lr_table_get_column_name_by_index (char * param_name, int col, 
											char * * const col_name,
											int * col_name_len);
# 753 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"

int lr_table_get_column_name_by_index_free (char * col_name);

 
 
 
 
# 768 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"
int   lr_zip (const char* param1, const char* param2);
int   lr_unzip (const char* param1, const char* param2);

 
 
 
 
 
 
 
 

 
 
 
 
 
 
int   lr_param_substit (char * file,
                                   int const line,
                                   char * in_str,
                                   int const in_len,
                                   char * * const out_str,
                                   int * const out_len);
# 792 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"
void   lr_param_substit_free (char * * pstr);


 
# 804 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"





char *   lrfnc_eval_string (char * str,
                                      char * file_name,
                                      long const line_num);
# 812 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"


int   lrfnc_save_string ( const char * param_val,
                                     const char * param_name,
                                     const char * file_name,
                                     long const line_num);
# 818 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"

int   lrfnc_free_parameter (const char * param_name );







typedef struct _lr_timestamp_param
{
	int iDigits;
}lr_timestamp_param;

extern const lr_timestamp_param default_timestamp_param;

int   lrfnc_save_timestamp (const char * param_name, const lr_timestamp_param* time_param);

int lr_save_searched_string(char * buffer, long buf_size, unsigned int occurrence,
			    char * search_string, int offset, unsigned int param_val_len, 
			    char * param_name);

 
char *   lr_string (char * str);

 
# 917 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"

int   lr_save_value (char * param_val,
                                unsigned long const param_val_len,
                                unsigned long const options,
                                char * param_name,
                                char * file_name,
                                long const line_num);
# 924 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"


 
 
 
 
 











int   lr_printf (char * fmt, ...);
 
int   lr_set_debug_message (unsigned int msg_class,
                                       unsigned int swtch);
# 946 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"
unsigned int   lr_get_debug_message (void);


 
 
 
 
 

void   lr_double_think_time ( double secs);
void   lr_usleep (long);


 
 
 
 
 
 




int *   lr_localtime (long offset);


int   lr_send_port (long port);


# 1022 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"



struct _lr_declare_identifier{
	char signature[24];
	char value[128];
};

int   lr_pt_abort (void);

void vuser_declaration (void);






# 1051 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"


# 1063 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"
















 
 
 
 
 







int    _lr_declare_transaction   (char * transaction_name);


 
 
 
 
 







int   _lr_declare_rendezvous  (char * rendezvous_name);

 
 
 
 
 


int vtc_connect(char * servername, int portnum, int options);
int vtc_disconnect(int pvci);
int vtc_get_last_error(int pvci);
int vtc_query_column(int pvci, char * columnName, int columnIndex, char * *outvalue);
int vtc_query_row(int pvci, int rowIndex, char * **outcolumns, char * **outvalues);
int vtc_send_message(int pvci, char * column, char * message, unsigned short *outRc);
int vtc_send_if_unique(int pvci, char * column, char * message, unsigned short *outRc);
int vtc_send_row1(int pvci, char * columnNames, char * messages, char * delimiter, unsigned char sendflag, unsigned short *outUpdates);
int vtc_update_message(int pvci, char * column, int index , char * message, unsigned short *outRc);
int vtc_update_message_ifequals(int pvci, char * columnName, int index,	char * message, char * ifmessage, unsigned short 	*outRc);
int vtc_update_row1(int pvci, char * columnNames, int index , char * messages, char * delimiter, unsigned short *outUpdates);
int vtc_retrieve_message(int pvci, char * column, char * *outvalue);
int vtc_retrieve_messages1(int pvci, char * columnNames, char * delimiter, char * **outvalues);
int vtc_retrieve_row(int pvci, char * **outcolumns, char * **outvalues);
int vtc_rotate_message(int pvci, char * column, char * *outvalue, unsigned char sendflag);
int vtc_rotate_messages1(int pvci, char * columnNames, char * delimiter, char * **outvalues, unsigned char sendflag);
int vtc_rotate_row(int pvci, char * **outcolumns, char * **outvalues, unsigned char sendflag);
int vtc_increment(int pvci, char * column, int index , int incrValue, int *outValue);
int vtc_clear_message(int pvci, char * column, int index , unsigned short *outRc);
int vtc_clear_column(int pvci, char * column, unsigned short *outRc);
int vtc_ensure_index(int pvci, char * column, unsigned short *outRc);
int vtc_drop_index(int pvci, char * column, unsigned short *outRc);
int vtc_clear_row(int pvci, int rowIndex, unsigned short *outRc);
int vtc_create_column(int pvci, char * column,unsigned short *outRc);
int vtc_column_size(int pvci, char * column, int *size);
void vtc_free(char * msg);
void vtc_free_list(char * *msglist);

int lrvtc_connect(char * servername, int portnum, int options);
int lrvtc_disconnect();
int lrvtc_query_column(char * columnName, int columnIndex);
int lrvtc_query_row(int columnIndex);
int lrvtc_send_message(char * columnName, char * message);
int lrvtc_send_if_unique(char * columnName, char * message);
int lrvtc_send_row1(char * columnNames, char * messages, char * delimiter, unsigned char sendflag);
int lrvtc_update_message(char * columnName, int index , char * message);
int lrvtc_update_message_ifequals(char * columnName, int index, char * message, char * ifmessage);
int lrvtc_update_row1(char * columnNames, int index , char * messages, char * delimiter);
int lrvtc_retrieve_message(char * columnName);
int lrvtc_retrieve_messages1(char * columnNames, char * delimiter);
int lrvtc_retrieve_row();
int lrvtc_rotate_message(char * columnName, unsigned char sendflag);
int lrvtc_rotate_messages1(char * columnNames, char * delimiter, unsigned char sendflag);
int lrvtc_rotate_row(unsigned char sendflag);
int lrvtc_increment(char * columnName, int index , int incrValue);
int lrvtc_noop();
int lrvtc_clear_message(char * columnName, int index);
int lrvtc_clear_column(char * columnName); 
int lrvtc_ensure_index(char * columnName); 
int lrvtc_drop_index(char * columnName); 
int lrvtc_clear_row(int rowIndex);
int lrvtc_create_column(char * columnName);
int lrvtc_column_size(char * columnName);



 
 
 
 
 

 
int lr_enable_ip_spoofing();
int lr_disable_ip_spoofing();


 




int lr_convert_string_encoding(char * sourceString, char * fromEncoding, char * toEncoding, char * paramName);


 
int lr_db_connect (char * pFirstArg, ...);
int lr_db_disconnect (char * pFirstArg,	...);
int lr_db_executeSQLStatement (char * pFirstArg, ...);
int lr_db_dataset_action(char * pFirstArg, ...);
int lr_checkpoint(char * pFirstArg,	...);
int lr_db_getvalue(char * pFirstArg, ...);







 
 



















# 1 "e:\\performance\\scripts\\2017\\03_perf\\ecommerce\\ca_checkout\\\\combined_CA_Checkout.c" 2

# 1 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/SharedParameter.h" 1



 
 
 
 
# 100 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/SharedParameter.h"





typedef int PVCI2;
typedef int VTCERR2;

 
 
 

 
extern PVCI2    vtc_connect(char *servername, int portnum, int options);
extern VTCERR2  vtc_disconnect(int pvci);
extern VTCERR2  vtc_get_last_error(int pvci);

 
extern VTCERR2  vtc_query_column(int pvci, char *columnName, int columnIndex, char **outvalue);
extern VTCERR2  vtc_query_row(int pvci, int columnIndex, char ***outcolumns, char ***outvalues);
extern VTCERR2  vtc_send_message(int pvci, char *column, char *message, unsigned short *outRc);
extern VTCERR2  vtc_send_if_unique(int pvci, char *column, char *message, unsigned short *outRc);
extern VTCERR2  vtc_send_row1(int pvci, char *columnNames, char *messages, char *delimiter,  unsigned char sendflag, unsigned short *outUpdates);
extern VTCERR2  vtc_update_message(int pvci, char *column, int index , char *message, unsigned short *outRc);
extern VTCERR2  vtc_update_message_ifequals(int pvci, char	*columnName, int index,	char *message, char	*ifmessage,	unsigned short 	*outRc);
extern VTCERR2  vtc_update_row1(int pvci, char *columnNames, int index , char *messages, char *delimiter, unsigned short *outUpdates);
extern VTCERR2  vtc_retrieve_message(int pvci, char *column, char **outvalue);
extern VTCERR2  vtc_retrieve_messages1(int pvci, char *columnNames, char *delimiter, char ***outvalues);
extern VTCERR2  vtc_retrieve_row(int pvci, char ***outcolumns, char ***outvalues);
extern VTCERR2  vtc_rotate_message(int pvci, char *column, char **outvalue, unsigned char sendflag);
extern VTCERR2  vtc_rotate_messages1(int pvci, char *columnNames, char *delimiter, char ***outvalues, unsigned char sendflag);
extern VTCERR2  vtc_rotate_row(int pvci, char ***outcolumns, char ***outvalues, unsigned char sendflag);
extern VTCERR2  vtc_increment(int pvci, char *column, int index , int incrValue, int *outValue);
extern VTCERR2  vtc_clear_message(int pvci, char *column, int index , unsigned short *outRc);
extern VTCERR2  vtc_clear_column(int pvci, char *column, unsigned short *outRc);

extern VTCERR2  vtc_clear_row(int pvci, int rowIndex, unsigned short *outRc);

extern VTCERR2  vtc_create_column(int pvci, char *column,unsigned short *outRc);
extern VTCERR2  vtc_column_size(int pvci, char *column, int *size);
extern VTCERR2  vtc_ensure_index(int pvci, char *column, unsigned short *outRc);
extern VTCERR2  vtc_drop_index(int pvci, char *column, unsigned short *outRc);

extern VTCERR2  vtc_noop(int pvci);

 
extern void vtc_free(char *msg);
extern void vtc_free_list(char **msglist);

 


 




 




















 




 
 
 

extern VTCERR2  lrvtc_connect(char *servername, int portnum, int options);
extern VTCERR2  lrvtc_disconnect();
extern VTCERR2  lrvtc_query_column(char *columnName, int columnIndex);
extern VTCERR2  lrvtc_query_row(int columnIndex);
extern VTCERR2  lrvtc_send_message(char *columnName, char *message);
extern VTCERR2  lrvtc_send_if_unique(char *columnName, char *message);
extern VTCERR2  lrvtc_send_row1(char *columnNames, char *messages, char *delimiter,  unsigned char sendflag);
extern VTCERR2  lrvtc_update_message(char *columnName, int index , char *message);
extern VTCERR2  lrvtc_update_message_ifequals(char *columnName, int index, char 	*message, char *ifmessage);
extern VTCERR2  lrvtc_update_row1(char *columnNames, int index , char *messages, char *delimiter);
extern VTCERR2  lrvtc_retrieve_message(char *columnName);
extern VTCERR2  lrvtc_retrieve_messages1(char *columnNames, char *delimiter);
extern VTCERR2  lrvtc_retrieve_row();
extern VTCERR2  lrvtc_rotate_message(char *columnName, unsigned char sendflag);
extern VTCERR2  lrvtc_rotate_messages1(char *columnNames, char *delimiter, unsigned char sendflag);
extern VTCERR2  lrvtc_rotate_row(unsigned char sendflag);
extern int     lrvtc_increment(char *columnName, int index , int incrValue);
extern VTCERR2  lrvtc_clear_message(char *columnName, int index);
extern VTCERR2  lrvtc_clear_column(char *columnName);
extern VTCERR2  lrvtc_clear_row(int rowIndex);
extern VTCERR2  lrvtc_create_column(char *columnName);
extern int     lrvtc_column_size(char *columnName);
extern VTCERR2  lrvtc_ensure_index(char *columnName);
extern VTCERR2  lrvtc_drop_index(char *columnName);

extern VTCERR2  lrvtc_noop();

 
 
 

                               


 
 
 





















# 2 "e:\\performance\\scripts\\2017\\03_perf\\ecommerce\\ca_checkout\\\\combined_CA_Checkout.c" 2

# 1 "globals.h" 1



 
 

# 1 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/web_api.h" 1







# 1 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/as_web.h" 1
























































 




 








 
 
 

  int
	web_add_filter(
		const char *		mpszArg,
		...
	);									 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_add_auto_filter(
		const char *		mpszArg,
		...
	);									 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
	
  int
	web_add_auto_header(
		const char *		mpszHeader,
		const char *		mpszValue);

  int
	web_add_header(
		const char *		mpszHeader,
		const char *		mpszValue);
  int
	web_add_cookie(
		const char *		mpszCookie);
  int
	web_cleanup_auto_headers(void);
  int
	web_cleanup_cookies(void);
  int
	web_concurrent_end(
		const char * const	mpszReserved,
										 
		...								 
	);
  int
	web_concurrent_start(
		const char * const	mpszConcurrentGroupName,
										 
										 
		...								 
										 
	);
  int
	web_create_html_param(
		const char *		mpszParamName,
		const char *		mpszLeftDelim,
		const char *		mpszRightDelim);
  int
	web_create_html_param_ex(
		const char *		mpszParamName,
		const char *		mpszLeftDelim,
		const char *		mpszRightDelim,
		const char *		mpszNum);
  int
	web_custom_request(
		const char *		mpszReqestName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	spdy_custom_request(
		const char *		mpszReqestName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_disable_keep_alive(void);
  int
	web_enable_keep_alive(void);
  int
	web_find(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_get_int_property(
		const int			miHttpInfoType);
  int
	web_image(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_image_check(
		const char *		mpszName,
		...);
  int
	web_java_check(
		const char *		mpszName,
		...);
  int
	web_link(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

	
  int
	web_global_verification(
		const char *		mpszArg1,
		...);							 
										 
										 
										 
										 
										 
  int
	web_reg_find(
		const char *		mpszArg1,
		...);							 
										 
										 
										 
										 
										 
										 
										 
				
  int
	web_reg_save_param(
		const char *		mpszParamName,
		...);							 
										 
										 
										 
										 
										 
										 

  int
	web_convert_param(
		const char * 		mpszParamName, 
										 
		...);							 
										 
										 


										 

										 
  int
	web_remove_auto_filter(
		const char *		mpszArg,
		...
	);									 
										 
				
  int
	web_remove_auto_header(
		const char *		mpszHeaderName,
		...);							 
										 



  int
	web_remove_cookie(
		const char *		mpszCookie);

  int
	web_save_header(
		const char *		mpszType,	 
		const char *		mpszName);	 
  int
	web_set_certificate(
		const char *		mpszIndex);
  int
	web_set_certificate_ex(
		const char *		mpszArg1,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_set_connections_limit(
		const char *		mpszLimit);
  int
	web_set_max_html_param_len(
		const char *		mpszLen);
  int
	web_set_max_retries(
		const char *		mpszMaxRetries);
  int
	web_set_proxy(
		const char *		mpszProxyHost);
  int
	web_set_pac(
		const char *		mpszPacUrl);
  int
	web_set_proxy_bypass(
		const char *		mpszBypass);
  int
	web_set_secure_proxy(
		const char *		mpszProxyHost);
  int
	web_set_sockets_option(
		const char *		mpszOptionID,
		const char *		mpszOptionValue
	);
  int
	web_set_option(
		const char *		mpszOptionID,
		const char *		mpszOptionValue,
		...								 
	);
  int
	web_set_timeout(
		const char *		mpszWhat,
		const char *		mpszTimeout);
  int
	web_set_user(
		const char *		mpszUserName,
		const char *		mpszPwd,
		const char *		mpszHost);

  int
	web_sjis_to_euc_param(
		const char *		mpszParamName,
										 
		const char *		mpszParamValSjis);
										 

  int
	web_submit_data(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	spdy_submit_data(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_submit_form(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										  
										 
										 
										 
										 
										 
										  
										 
										 
										 
										 
										 
										 
										 
										  
										 
										 
										 
										 
										 
										  
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_url(
		const char *		mpszUrlName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	spdy_url(
		const char *		mpszUrlName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int 
	web_set_proxy_bypass_local(
		const char * mpszNoLocal
		);

  int 
	web_cache_cleanup(void);

  int
	web_create_html_query(
		const char* mpszStartQuery,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int 
	web_create_radio_button_param(
		const char *NameFiled,
		const char *NameAndVal,
		const char *ParamName
		);

  int
	web_convert_from_formatted(
		const char * mpszArg1,
		...);							 
										 
										 
										 
										 
										 
										
  int
	web_convert_to_formatted(
		const char * mpszArg1,
		...);							 
										 
										 
										 
										 
										 

  int
	web_reg_save_param_ex(
		const char * mpszParamName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_reg_save_param_xpath(
		const char * mpszParamName,
		...);							
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_reg_save_param_json(
		const char * mpszParamName,
		...);							
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_reg_save_param_regexp(
		 const char * mpszParamName,
		 ...);							
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_js_run(
		const char * mpszCode,
		...);							
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_js_reset(void);

  int
	web_convert_date_param(
		const char * 		mpszParamName,
		...);










# 737 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/as_web.h"


# 750 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/as_web.h"



























# 788 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/as_web.h"

 
 
 


  int
	FormSubmit(
		const char *		mpszFormName,
		...);
  int
	InitWebVuser(void);
  int
	SetUser(
		const char *		mpszUserName,
		const char *		mpszPwd,
		const char *		mpszHost);
  int
	TerminateWebVuser(void);
  int
	URL(
		const char *		mpszUrlName);
























# 856 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/as_web.h"


  int
	web_rest(
		const char *		mpszReqestName,
		...);							 
										 
										 
										 
										 

 
 
 






# 9 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/web_api.h" 2

















 







 














  int
	web_reg_add_cookie(
		const char *		mpszCookie,
		...);							 
										 

  int
	web_report_data_point(
		const char *		mpszEventType,
		const char *		mpszEventName,
		const char *		mpszDataPointName,
		const char *		mpszLAST);	 
										 
										 
										 

  int
	web_text_link(
		const char *		mpszStepName,
		...);

  int
	web_element(
		const char *		mpszStepName,
		...);

  int
	web_image_link(
		const char *		mpszStepName,
		...);

  int
	web_static_image(
		const char *		mpszStepName,
		...);

  int
	web_image_submit(
		const char *		mpszStepName,
		...);

  int
	web_button(
		const char *		mpszStepName,
		...);

  int
	web_edit_field(
		const char *		mpszStepName,
		...);

  int
	web_radio_group(
		const char *		mpszStepName,
		...);

  int
	web_check_box(
		const char *		mpszStepName,
		...);

  int
	web_list(
		const char *		mpszStepName,
		...);

  int
	web_text_area(
		const char *		mpszStepName,
		...);

  int
	web_map_area(
		const char *		mpszStepName,
		...);

  int
	web_eval_java_script(
		const char *		mpszStepName,
		...);

  int
	web_reg_dialog(
		const char *		mpszArg1,
		...);

  int
	web_reg_cross_step_download(
		const char *		mpszArg1,
		...);

  int
	web_browser(
		const char *		mpszStepName,
		...);

  int
	web_control(
		const char *		mpszStepName,
		...);

  int
	web_set_rts_key(
		const char *		mpszArg1,
		...);

  int
	web_save_param_length(
		const char * 		mpszParamName,
		...);

  int
	web_save_timestamp_param(
		const char * 		mpszParamName,
		...);

  int
	web_load_cache(
		const char *		mpszStepName,
		...);							 
										 

  int
	web_dump_cache(
		const char *		mpszStepName,
		...);							 
										 
										 

  int
	web_reg_find_in_log(
		const char *		mpszArg1,
		...);							 
										 
										 

  int
	web_get_sockets_info(
		const char *		mpszArg1,
		...);							 
										 
										 
										 
										 

  int
	web_add_cookie_ex(
		const char *		mpszArg1,
		...);							 
										 
										 
										 

  int
	web_hook_java_script(
		const char *		mpszArg1,
		...);							 
										 
										 
										 

 
 
 
 
 
 
 
 
 
 
 
 
  int
	web_reg_async_attributes(
		const char *		mpszArg,
		...
	);

 
 
 
 
 
 
  int
	web_sync(
		 const char *		mpszArg1,
		 ...
	);

 
 
 
 
  int
	web_stop_async(
		const char *		mpszArg1,
		...
	);

 
 
 
 
 

 
 
 

typedef enum WEB_ASYNC_CB_RC_ENUM_T
{
	WEB_ASYNC_CB_RC_OK,				 

	WEB_ASYNC_CB_RC_ABORT_ASYNC_NOT_ERROR,
	WEB_ASYNC_CB_RC_ABORT_ASYNC_ERROR,
										 
										 
										 
										 
	WEB_ASYNC_CB_RC_ENUM_COUNT
} WEB_ASYNC_CB_RC_ENUM;

 
 
 

typedef enum WEB_CONVERS_CB_CALL_REASON_ENUM_T
{
	WEB_CONVERS_CB_CALL_REASON_BUFFER_RECEIVED,
	WEB_CONVERS_CB_CALL_REASON_END_OF_TASK,

	WEB_CONVERS_CB_CALL_REASON_ENUM_COUNT
} WEB_CONVERS_CB_CALL_REASON_ENUM;

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

typedef
int														 
	(*RequestCB_t)();

typedef
int														 
	(*ResponseBodyBufferCB_t)(
		  const char *		aLastBufferStr,
		  int				aLastBufferLen,
		  const char *		aAccumulatedStr,
		  int				aAccumulatedLen,
		  int				aHttpStatusCode);

typedef
int														 
	(*ResponseCB_t)(
		  const char *		aResponseHeadersStr,
		  int				aResponseHeadersLen,
		  const char *		aResponseBodyStr,
		  int				aResponseBodyLen,
		  int				aHttpStatusCode);

typedef
int														 
	(*ResponseHeadersCB_t)(
		  int				aHttpStatusCode,
		  const char *		aAccumulatedHeadersStr,
		  int				aAccumulatedHeadersLen);



 
 
 

typedef enum WEB_CONVERS_UTIL_RC_ENUM_T
{
	WEB_CONVERS_UTIL_RC_OK,
	WEB_CONVERS_UTIL_RC_CONVERS_NOT_FOUND,
	WEB_CONVERS_UTIL_RC_TASK_NOT_FOUND,
	WEB_CONVERS_UTIL_RC_INFO_NOT_FOUND,
	WEB_CONVERS_UTIL_RC_INFO_UNAVIALABLE,
	WEB_CONVERS_UTIL_RC_INVALID_ARGUMENT,

	WEB_CONVERS_UTIL_RC_ENUM_COUNT
} WEB_CONVERS_UTIL_RC_ENUM;

 
 
 

  int					 
	web_util_set_request_url(
		  const char *		aUrlStr);

  int					 
	web_util_set_request_body(
		  const char *		aRequestBodyStr);

  int					 
	web_util_set_formatted_request_body(
		  const char *		aRequestBodyStr);


 
 
 
 
 

 
 
 
 
 

 
 
 
 
 
 
 
 

 
 
  int
web_websocket_connect(
		 const char *	mpszArg1,
		 ...
		 );


 
 
 
 
 																						
  int
web_websocket_send(
	   const char *		mpszArg1,
		...
	   );

 
 
 
 
 
 
  int
web_websocket_close(
		const char *	mpszArg1,
		...
		);

 
typedef
void														
(*OnOpen_t)(
			  const char* connectionID,  
			  const char * responseHeader,  
			  int length  
);

typedef
void														
(*OnMessage_t)(
	  const char* connectionID,  
	  int isbinary,  
	  const char * data,  
	  int length  
	);

typedef
void														
(*OnError_t)(
	  const char* connectionID,  
	  const char * message,  
	  int length  
	);

typedef
void														
(*OnClose_t)(
	  const char* connectionID,  
	  int isClosedByClient,  
	  int code,  
	  const char* reason,  
	  int length  
	 );
 
 
 
 
 





# 7 "globals.h" 2

# 1 "lrw_custom_body.h" 1
 




# 8 "globals.h" 2


 
 
int LINK_TT, FORM_TT, TYPE_SPEED;  
	 
	LINK_TT = 10;
	FORM_TT = 10;
	TYPE_SPEED = 1;
int authcookielen , authtokenlen ;
authcookielen = 0;	
int orderItemIdscount = 0;


# 3 "e:\\performance\\scripts\\2017\\03_perf\\ecommerce\\ca_checkout\\\\combined_CA_Checkout.c" 2

# 1 "vuser_init.c" 1
# 1 "..\\..\\browseFunctions.c" 1
# 1 "..\\..\\browseWorkloadModel.c" 1
int NAV_BROWSE, NAV_SEARCH, NAV_CLEARANCE, NAV_PLACE;  
int DRILL_ONE_FACET, DRILL_TWO_FACET, DRILL_THREE_FACET, DRILL_SUB_CATEGORY;  
int PDP, QUICKVIEW, PRODUCT_QUICKVIEW_SERVICE, RESERVE_ONLINE;  
int APPLY_SORT, RATIO_TCPSkuSelectionView;  
int APPLY_PAGINATE, RATIO_STORE_LOCATOR, RATIO_SEARCH_SUGGEST;  

	 

	NAV_BROWSE = 45;  
	NAV_SEARCH = 5;  
	NAV_CLEARANCE = 50;  
	NAV_PLACE = 0;  

	 
	DRILL_ONE_FACET = 35;  
	DRILL_TWO_FACET = 10;  
	DRILL_THREE_FACET = 5;  
	DRILL_SUB_CATEGORY = 50;

	 
	APPLY_SORT = 25;

	 
	APPLY_PAGINATE = 15;  

	 
	 
	PDP = 72;  
	QUICKVIEW = 28;  
	
	RATIO_STORE_LOCATOR  = 5;
	RATIO_SEARCH_SUGGEST = 0;  

	PRODUCT_QUICKVIEW_SERVICE = 15;  
	RESERVE_ONLINE = 100;  
	
	RATIO_TCPSkuSelectionView = 20;
# 1 "..\\..\\browseFunctions.c" 2


int index , length , i, randomPercent, isLoggedIn;
char *searchString ;
char *nav_by ;  
char *drill_by ;  
char *sort_by;  
char *product_by;  
 
typedef long time_t;
time_t t;
 
 

void webURL(char *Url, char *pageName)
{
	lr_save_string(Url, "Url");
	lr_save_string(pageName, "pageName");
	web_url ( lr_eval_string("{pageName}") ,
    	     "URL={Url}" ,
             "Resource=0" ,
             "Mode=HTML" ,
             "LAST" ) ;
} 

void endIteration()
{
	 
}

void homePageCorrelations()  
{
	web_reg_save_param ( "clearances" ,
 
						 "LB=<a href=\"http://{host}/shop/SearchDisplay?" ,  
						 "RB=\"" ,
						 "ORD=ALL" , "Convert=HTML_TO_TEXT" , "NotFound=Warning", "LAST" ) ;

	web_reg_save_param ( "categories" ,
						 "LB=<a href=\"http://{host}{store_home_page}search" ,
						 "RB=\"" ,
						 "ORD=ALL" , "Convert=HTML_TO_TEXT" , "NotFound=Warning", "LAST" ) ;

	 

	 
 
# 60 "..\\..\\browseFunctions.c"
	web_reg_save_param ( "searchAutoSuggestURL" ,
						 "LB=setAutoSuggestURL('",
						 "RB='" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=Warning", "LAST" ) ;

	web_reg_save_param ( "searchCachedSuggestionsURL" ,
						 "LB=setCachedSuggestionsURL('" ,
						 "RB='" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=Warning", "LAST" ) ;

 

   	web_reg_save_param ( "placeShopCategory" ,
						 "LB={store_home_page}content/" ,
	                     "RB=\"",
						 "ORD=All",
	                     "NotFound=Warning", "LAST" ) ;

	web_set_sockets_option("SSL_VERSION","TLS");  
} 


void categoryPageCorrelations()  
{
	web_reg_save_param ( "subcategories" ,
	                     "LB=class='notSelected' href='http://{host}" ,
	                     "RB='>" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 "LAST" ) ;

	web_reg_save_param ( "facetsURL" ,
				  		 "LB=refreshAfterFilterGotSelected('http://{host}" ,
 
	                   	 "RB='" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 "LAST" ) ;

	web_reg_save_param_regexp( "ParamName=facetsID" ,
	                           "RegExp=facetCheckBox\" id =\"(.*?)\" value=\"(.*?)\"" ,
			                   "Ordinal=ALL" ,
			                   "Notfound=warning" ,
			                   "SEARCH_FILTERS" ,
			                   "Group=2" ,
			                   "LAST" ) ;

	web_reg_save_param ( "sortURL" ,
				  		 "LB=<option value=\"http://{host}" ,
	                   	 "RB=\"" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 "LAST" ) ;

	web_reg_save_param ( "searchSubCategories" ,
				  		 "LB=href='http://{host}" ,
	                   	 "RB='>" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 "LAST" ) ;

	web_reg_save_param ( "paginationNextURL" ,
				  		 "LB=goToResultPage('http://{host}" ,
	                   	 "RB='" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 "LAST" ) ;

	web_reg_save_param ( "paginationPageIndex" ,
				  		 "LB=catalogSearchResultDisplay_Context','" ,
	                   	 "RB='" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 "LAST" ) ;

	web_reg_save_param ( "paginationShowAllURL" ,
				  		 "LB=viewall-right-bottom\">\r\n\t\t\t\t\t                <a href=\"http://{host}" ,
	                   	 "RB=\"" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 "LAST" ) ;
						 
} 


void subCategoryPageCorrelations()  
{
	web_reg_save_param ( "pdpURL" ,
	                     "LB=productRow name\">\r\n\t\t\t\r\n\t\t\t\t<a href=\"http://{host}" ,
	                     "RB=\" id" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 "LAST" ) ;

	web_reg_save_param ( "quickviewURL" ,
	                     "LB=openModal2\" link=\"http://{host}" ,
						 "RB=\" href=" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 "LAST" ) ;
						 
	web_reg_find("SaveCount=noProducts", "Text/IC=We're sorry, there are no products available for purchase at this time. Please check back later.", "LAST");

	
} 


void productDisplayPageCorrelations()
{

	web_reg_save_param ( "skuSelectionSwitchProductID" ,
	                     "LB=confirmSwitchProduct(event,'product_" ,
	                     "RB='" ,
	                     "notFound=Warning",
	                     "ORD=ALL" ,
	                     "LAST" ) ;

	web_reg_save_param ( "skuSelectionProductID" ,
	                     "LB=input type=\"hidden\" id=\"prodId\" value=\"" ,
	                     "RB=\"" ,
	                     "notFound=Warning",
	                     "LAST" ) ;
						 
}  


 
void addToCartCorrelations()
{
	web_reg_save_param_regexp( "ParamName=atc_catentryIds" ,
	                           "RegExp=\"catentry_id\" : \"(.*?)\",\r\n\t\t\t\t\t\t\t\t\t\t\t\"item_sku\" : \"(.*?)\",\r\n\t\t\t\t\t\t\t\t\t\t\t\"qty\" : \"([1-9][0-9]*)\"" ,
			                   "Ordinal=ALL" ,
			                   "Notfound=warning" ,
			                   "SEARCH_FILTERS" ,
			                   "Group=1" ,
			                   "LAST" ) ;

 	web_reg_save_param ( "atc_comment" ,
                    	 "LB=<input type=\"hidden\" name=\"comment\" value=\"" ,
                    	 "RB=\"" ,
                    	 "Notfound=warning" ,
                    	 "LAST" ) ;

}  

void viewHomePage()
{
	 
	homePageCorrelations();
	lr_param_sprintf ( "homePageUrl" , "http://%s%shome" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}") ) ;
	lr_start_transaction( "T01_Home Page" ) ;
		webURL(lr_eval_string ( "{homePageUrl}" ), "T01_Home Page" );
    lr_end_transaction( "T01_Home Page" , 2 ) ;
}  


void navByBrowse()
{
	 
 	lr_think_time ( LINK_TT ) ;
 
 
	lr_param_sprintf ( "cateogryPageUrl" , "http://%s%ssearch%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}") , lr_paramarr_random ( "categories" ) ) ;
	
	categoryPageCorrelations();
	subCategoryPageCorrelations();  

	lr_start_transaction( "T02_Category Display" ) ;
	
		webURL(lr_eval_string ( "{cateogryPageUrl}" ), "T02_Category Display" );
		
		if ( isLoggedIn == 1 ) {
			 
		}
		
	if ((lr_paramarr_len("subcategories")) == 0)
	{
		lr_end_transaction ( "T02_Category Display" , 1 ) ;  
		return;
	}  
	else
		lr_end_transaction ( "T02_Category Display" , 0 ) ;

}  


void navByClearance()
{
	 
 	lr_think_time ( LINK_TT ) ;
	 
 
 
	lr_param_sprintf ( "clearancePageUrl" , "http://%s/shop/SearchDisplay?%s" , lr_eval_string("{host}"),  lr_paramarr_random( "clearances" ) ) ;
	 

	categoryPageCorrelations();

	subCategoryPageCorrelations();  
	web_reg_find("SaveCount=noProducts", "Text/IC=We're sorry, there are no products available for purchase at this time. Please check back later.", "LAST");
	web_reg_find("Text=FILTER BY", "SaveCount=FilterBy_Count", "LAST" );

	lr_start_transaction ( "T02_Clearance Display" ) ;
	
		webURL(lr_eval_string ( "{clearancePageUrl}" ), "T02_Clearance Display" );

 
 
 
		
 
# 291 "..\\..\\browseFunctions.c"
	
	if ( atoi(lr_eval_string("{noProducts}")) == 1 ) {
		lr_end_transaction ( "T02_Clearance Display" , 1 ) ;
 
	}
	else if ( atoi(lr_eval_string("{FilterBy_Count}")) == 0 )
	{
		lr_output_message("Not Found FILTER BY: %s", lr_eval_string ( "{clearancePageUrl}" ) );  
 
		lr_end_transaction ( "T02_Clearance Display" , 1 ) ;
 
	}  
	else
		lr_end_transaction ( "T02_Clearance Display" , 0 ) ;


}  

findNewArrivals()  
{
	int offset = 1;

    char * position;

    char * str = "";

    char * search_str = "Arrivals";  

    while( offset >= 1 ) {

		str = lr_paramarr_random( "clearances" );
		 
	    position = (char *)strstr(str, search_str);

	    offset = (int)(position - str + 1);
    }

    lr_save_string(str, "clearance");

	return 0;
}

void typeAheadSearch()
{
	 
    for (i = 0; i < length; i++)
    {
       	 
       	if (i == 0)
           	lr_param_sprintf ("SEARCH_STRING_PARAM", "%c", searchString[i]);
       	else
           	lr_param_sprintf ("SEARCH_STRING_PARAM", "%s%c", lr_eval_string("{SEARCH_STRING_PARAM}"), searchString[i]);
           	 

		if (i == 1) lr_save_string(lr_eval_string("{SEARCH_STRING_PARAM}"),"forDidYouMean");
			
 
		if (i == 2 || i == 5 || i == 8 || i == 11 || i == 14 || i == 17 || i == 20 )
       	{
    		lr_think_time ( TYPE_SPEED );

			web_reg_save_param ( "autoSelectOption" , "LB=title=\"" ,"RB=\" id='autoSelectOption_" , "Notfound=warning" , "ORD=All", "LAST" ) ;
			
		
			lr_start_transaction ( "T02_Search_ajaxAutoSuggestion" ) ;

			web_submit_data("TCPAJAXAutoSuggestView",
				"Action=http://{host}/webapp/wcs/stores/servlet/{searchAutoSuggestURL}&term={SEARCH_STRING_PARAM}&showHeader=true&count=4",
				"Method=POST",
				"RecContentType=text/html",
				"Snapshot=t45.inf",
				"Mode=HTML",
				"ITEMDATA",
				"Name=objectId", "Value=", "ENDITEM",
				"Name=requesttype", "Value=ajax", "ENDITEM",
				"LAST");

			lr_end_transaction ( "T02_Search_ajaxAutoSuggestion" , 0 ) ;

			 
			if (atoi(lr_eval_string("{autoSelectOption_count}")) > 0) {
				searchString = lr_eval_string("{autoSelectOption_1}");
				break;
			} 
			
       	}  
   	 }  
}  


void submitCompleteSearch()  
{
	 
 	lr_think_time (LINK_TT);
	categoryPageCorrelations();
	subCategoryPageCorrelations();
	productDisplayPageCorrelations();
	
 
	lr_param_sprintf ("SEARCH_STRING", "%s", lr_eval_string("{originalSearchString}"));   
	
 
	web_reg_find("Text= 0 matches", "SaveCount=searchResults_Count", "LAST" );
	
 
 		




	lr_start_transaction ( "T02_Search" );
 
	web_url("submitSearch",
		"URL=https://{host}/shop/SearchDisplay?storeId={storeId}&catalogId={catalogId}&langId=-1&pageSize=100&beginIndex=0&searchSource=Q&sType=SimpleSearch&resultCatEntryType=2&showResultsPage=true&pageView=image&custSrch=search&searchTerm={SEARCH_STRING}&TCPSearchSubmit=", 
		"Resource=0",
		"RecContentType=text/html",
		"Snapshot=t59.inf",
		"Mode=HTML",
		"LAST");
		
	if ( isLoggedIn == 1 ) {
		TCPGetWishListForUsersView();
	}
	
	if (atoi(lr_eval_string("{searchResults_Count}")) > 0) {  
		lr_end_transaction ( "T02_Search" , 1 ) ; 
	} 
	else {
		lr_end_transaction ( "T02_Search" , 0 ) ;
	}
	
	return;
		
}  
		
		
void submitCompleteSearchDidYouMean()  
{
	 
 	lr_think_time (LINK_TT);
	categoryPageCorrelations();
	subCategoryPageCorrelations();
	productDisplayPageCorrelations();
	
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_SEARCH_SUGGEST )
		lr_param_sprintf ("SEARCH_STRING", "%s", lr_eval_string("{forDidYouMean}"));
	else
		lr_param_sprintf ("SEARCH_STRING", "%s", searchString);
	
	web_reg_find("Text= 0 matches", "SaveCount=searchResults_Count", "LAST" );
	web_reg_save_param("didYouMeanId", "LB=http://{host}/shop/SearchDisplay?searchTermScope=&searchType=1002&filterTerm=&orderBy=&maxPrice=&showResultsPage=true&langId=-1&departmentId=&sType=", 
	"RB=\" class=\"result\">", "ORD=All", "NotFound=Warning", "LAST");


	lr_start_transaction ( "T02_Search" ) ;

	 
	web_url("submitSearch", 
		"URL=https://{host}/shop/SearchDisplay?storeId={storeId}&catalogId={catalogId}&langId=-1&pageSize=100&beginIndex=0&searchSource=Q&sType=SimpleSearch&resultCatEntryType=2&showResultsPage=true&pageView=image&custSrch=search&searchTerm={SEARCH_STRING}&TCPSearchSubmit=", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Mode=HTML", 
		"LAST");
		
	lr_end_transaction ( "T02_Search" , 0 ) ;

	if ( atoi(lr_eval_string("{didYouMeanId_count}")) > 0 ) {  
		
		lr_think_time (LINK_TT);
		categoryPageCorrelations();
		subCategoryPageCorrelations();
		productDisplayPageCorrelations();

		lr_save_string( lr_paramarr_random("didYouMeanId"), "searchStringId");

		web_reg_find("Text= 0 matches", "SaveCount=searchResults_Count", "LAST" );
		
		lr_start_transaction("T02_Search_DidYouMean");

		web_url("SelectDidYouMeanSuggestion", 
			"URL=http://{host}/shop/SearchDisplay?searchTermScope=&searchType=1002&filterTerm=&orderBy=&maxPrice=&showResultsPage=true&langId=-1&departmentId=&sType={searchStringId}", 
			"TargetFrame=", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Mode=HTML", 
			"LAST");

		if (atoi(lr_eval_string("{searchResults_Count}")) == 0 )   
			lr_end_transaction ( "T02_Search_DidYouMean" , 1 ) ; 
		else
			lr_end_transaction ( "T02_Search_DidYouMean" , 0 ) ;
	}
	
}  
		
writeToFile1() 
{
	char *ip;
    char fullpath[1024], * filename1 = "\\e$\\Performance\\Scripts\\2016_R1\\03_PERF\\Datafiles\\TestData\\searchStrNoResult.dat";
	long file1;
 
    strcpy(fullpath, "\\\\" );
	ip = lr_get_host_name( );
    strcat(fullpath, ip);
	strcat(fullpath, filename1);
       
    if ((file1 = fopen(fullpath, "a+")) == 0) {
        lr_output_message ("Unable to open %s", fullpath);
        return -1;
    }
	
	fprintf(file1, "%s\n", lr_eval_string("{SEARCH_STRING}"));
	
    fclose(file1);
	return 0;
}

writeToFile2() 
{
	long file;
	char *ip;
    char fullpath[1024], * filename2 = "\\e$\\Performance\\Scripts\\2016_R1\\03_PERF\\Datafiles\\TestData\\searchStrWithResult.dat";
    strcpy(fullpath, "\\\\" );
	ip = lr_get_host_name( );
    strcat(fullpath,  ip);
	strcat(fullpath, filename2);
 
       
    if ((file = fopen(fullpath, "a+")) == 0) {
        lr_output_message ("Unable to open %s", fullpath);
        return -1;
    }

	fprintf(file, "%s\n", lr_eval_string("{SEARCH_STRING}"));
	
    fclose(file);
	return 0;
		
}

void navBySearch()
{
	 

	int searchSuggest = atoi(lr_eval_string("{RANDOM_PERCENT}"));

	lr_think_time ( TYPE_SPEED );

	lr_start_transaction ( "T02_Search_ajaxCachedSuggestionView" ) ;

	web_submit_data("TCPAJAXCachedSuggestionsView",
		"Action=http://{host}/webapp/wcs/stores/servlet/{searchCachedSuggestionsURL}",
		"Method=POST",
		"RecContentType=text/html",
		"Snapshot=t44.inf",
		"Mode=HTML",
		"ITEMDATA",
		"Name=objectId", "Value=", "ENDITEM",
		"Name=requesttype", "Value=ajax", "ENDITEM",
		"LAST");

	lr_end_transaction ( "T02_Search_ajaxCachedSuggestionView" , 0 ) ;

	searchString = lr_eval_string("{searchStr}");  
	lr_save_string(searchString,"originalSearchString");
	
    length = (int)strlen(searchString);

    if (length >= 3)  
    	typeAheadSearch();
	
	if ( searchSuggest <= RATIO_SEARCH_SUGGEST )
		submitCompleteSearchDidYouMean();
	else
		submitCompleteSearch();

}  


void navByPlace()
{
	 
	if ( atoi( lr_eval_string( "{placeShopCategory_count}")) != 0 ) {
		
		lr_save_string( lr_paramarr_random("placeShopCategory"), "placeShop");
		
		if ( strcmp( lr_eval_string("{placeShop}") ,"placeShops-pjplace-2014" ) == 0) 
			lr_save_string( lr_paramarr_random("placeShopCategory"), "placeShop");
		
		lr_start_transaction ( "T02_PlaceShop" ) ;

		web_url("placeShop",
			"URL=http://{host}{store_home_page}content/{placeShop}",
			"Resource=0",
			"RecContentType=text/html",
			"LAST");
		
		lr_end_transaction ( "T02_PlaceShop" , 0 ) ;
	}
 
# 600 "..\\..\\browseFunctions.c"
}  


 
 
void topNav()
{
 

	randomPercent = atoi(lr_eval_string("{RANDOM_PERCENT}"));

	if ( randomPercent < NAV_BROWSE ){
		 
		navByBrowse();
		}  
	else if ( randomPercent < (NAV_BROWSE + NAV_CLEARANCE) ) {
		 
		navByClearance();
		}  
	else if ( randomPercent < (NAV_BROWSE + NAV_CLEARANCE + NAV_SEARCH)) {
		 
		navBySearch();
		}  
	else if (randomPercent < (NAV_BROWSE + NAV_CLEARANCE + NAV_SEARCH + NAV_PLACE)) {
		 
		navByPlace();
		}  

}  


void paginate()
{
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) < APPLY_PAGINATE ) {
		
		if (( lr_paramarr_len ( "paginationNextURL" ) > 0 ) ) {
			
			lr_think_time ( LINK_TT ) ;
			
		 
			subCategoryPageCorrelations();
			productDisplayPageCorrelations();
			index = rand ( ) % lr_paramarr_len( "paginationNextURL" ) + 1 ;
			lr_param_sprintf ("paginationURL", "http://%s%s&beginIndex=%s", lr_eval_string("{host}"), lr_paramarr_idx ( "paginationNextURL" , index ), lr_paramarr_idx ( "paginationPageIndex" , index ));
			lr_param_sprintf ("paginationIndex", "paginationPageIndex_%d", index);

			lr_start_transaction ( "T03_Paginate" ) ;
			
				web_submit_data("AjaxCatalogSearchResultView",
					"Action={paginationURL}",
					"Method=POST",
					"RecContentType=text/html",
					"Mode=HTML",
					"ITEMDATA",
					"Name=searchResultsPageNum", "Value=-{paginationIndex}", "ENDITEM",
					"Name=searchResultsView", "Value=", "ENDITEM",
					"Name=searchResultsURL", "Value={paginationURL}", "ENDITEM",
					"Name=objectId", "Value=", "ENDITEM",
					"Name=requesttype", "Value=ajax", "ENDITEM",
					"LAST");
					
			lr_end_transaction ( "T03_Paginate" , 0 ) ;
		}
	}
	
}  


void sortResults()
{
	 
 	lr_think_time ( LINK_TT ) ;
	if (( lr_paramarr_len ( "sortURL" ) > 0 )) {
		categoryPageCorrelations();
		subCategoryPageCorrelations();
		productDisplayPageCorrelations();
	 
	 
		lr_param_sprintf ( "drillUrl" , "http://%s%s" , lr_eval_string("{host}") , lr_paramarr_random ( "sortURL" ) ) ;

		lr_start_transaction ( "T03_Sort Results" ) ;
			webURL(lr_eval_string ( "{drillUrl}" ), "T03_Sort Results" );
		lr_end_transaction ( "T03_Sort Results" , 0 ) ;
	}  
}  

void sort()
{
 
 
 

 

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) < APPLY_SORT ){
		 
		sortResults();
		}  
	else {
		 
		return;
		}  

}  

void drillOneFacet()
{
	 
 	lr_think_time ( LINK_TT ) ;

	if (( lr_paramarr_len ( "facetsID" ) > 0 ) ) {
		categoryPageCorrelations();
		subCategoryPageCorrelations();
		productDisplayPageCorrelations();
		index = rand ( ) % lr_paramarr_len( "facetsID" ) + 1 ;
		lr_param_sprintf ( "drillUrlOneFacet" , "http://%s%s&facet=%s" , lr_eval_string("{host}") , lr_paramarr_idx ( "facetsURL" , index ) , lr_paramarr_idx ( "facetsID" , index ) ) ;
 
 

		lr_start_transaction ( "T03_Facet Display_1facet" ) ;

			webURL(lr_eval_string ( "{drillUrlOneFacet}" ), "T03_Facet Display_1facet" );

			if ( isLoggedIn == 1 ) {
				 
				 
			}

		lr_end_transaction ( "T03_Facet Display_1facet" , 0 ) ;

	}  
}  


void drillTwoFacets()
{
	 
	drillOneFacet();
 	lr_think_time ( LINK_TT ) ;
	if (( lr_paramarr_len ( "facetsID" ) > 0 ) ) {
		categoryPageCorrelations();
		subCategoryPageCorrelations();
		productDisplayPageCorrelations();
 
 
		lr_param_sprintf ( "drillUrlTwoFacets" , "%s&facet=%s" , lr_eval_string("{drillUrlOneFacet}") , lr_paramarr_random ( "facetsID"  ) ) ;
		
		lr_start_transaction ( "T03_Facet Display_2facets" ) ;

			webURL(lr_eval_string ( "{drillUrlTwoFacets}" ), "T03_Facet Display_2facets" );

			if ( isLoggedIn == 1 ) {
				TCPGetWishListForUsersView();
				 
			}

		lr_end_transaction ( "T03_Facet Display_2facets" , 0 ) ;

	}  
}  


void drillThreeFacets()
{
	 
	drillTwoFacets();
 	lr_think_time ( LINK_TT ) ;
	if (( lr_paramarr_len ( "facetsID" ) > 0 ) ) {
		categoryPageCorrelations();
		subCategoryPageCorrelations();
		productDisplayPageCorrelations();
 
 
		lr_param_sprintf ( "drillUrlThreeFacets" , "%s&facet=%s" , lr_eval_string("{drillUrlTwoFacets}") , lr_paramarr_random ( "facetsID" ) ) ;
		
		lr_start_transaction ( "T03_Facet Display_3facets" ) ;

			webURL(lr_eval_string ( "{drillUrlThreeFacets}" ), "T03_Facet Display_3facets" );

			if ( isLoggedIn == 1 ) {
				TCPGetWishListForUsersView();
				 
			}

		lr_end_transaction ( "T03_Facet Display_3facets" , 0 ) ;

	} 
} 


void drillSubCategory()
{
	 
 	lr_think_time ( LINK_TT ) ;
	categoryPageCorrelations();
	subCategoryPageCorrelations();
	productDisplayPageCorrelations();

	if (( lr_paramarr_len ( "subcategories" ) > 0 )) {
 
 
		lr_param_sprintf ( "drillUrl" , "http://%s%s" , lr_eval_string("{host}") , lr_paramarr_random ( "subcategories" ) ) ;
		
		lr_start_transaction ( "T03_Sub-Category Display" ) ;

			webURL(lr_eval_string ( "{drillUrl}" ), "T03_Sub-Category Display" );

			if ( isLoggedIn == 1 ) {
				TCPGetWishListForUsersView();
				 
			}

		if ( atoi(lr_eval_string("{noProducts}")) == 1 ) {
			lr_end_transaction ( "T03_Sub-Category Display" , 1 ) ;
 
		}
		else
			lr_end_transaction ( "T03_Sub-Category Display" , 0 ) ;

	}  
	else if ((lr_paramarr_len( "searchSubCategories") > 0)) {
 
 
		lr_param_sprintf ( "drillUrl" , "http://%s%s" , lr_eval_string("{host}") , lr_paramarr_random ( "searchSubCategories" ) ) ;

		lr_start_transaction ( "T03_Sub-Category Display" ) ;

			webURL(lr_eval_string ( "{drillUrl}" ), "T03_Sub-Category Display" );

			if ( isLoggedIn == 1 ) {
				TCPGetWishListForUsersView();
				 
			}

		if ( atoi(lr_eval_string("{noProducts}")) == 1 ) {
			lr_end_transaction ( "T03_Sub-Category Display" , 1 ) ;
 
		}
		else
			lr_end_transaction ( "T03_Sub-Category Display" , 0 ) ;

	}  
	
} 


void drill()
{
 
 
	if (( lr_paramarr_len( "subcategories" ) == 0 ) && (lr_paramarr_len( "searchSubCategories") == 0) && ( lr_paramarr_len ( "facetsID" ) == 0 ) ) {
		 
 
		return;
	}  
	else if ( lr_paramarr_len ( "subcategories" ) == 0 && (lr_paramarr_len( "searchSubCategories") == 0)) {
		drill_by = "ONE_FACET" ;
		 
		}  
	else if ( lr_paramarr_len ( "facetsID" ) == 0 ) {
		drill_by = "SUB_CATEGORY" ;
		 
		}  
	else {
		 
		 

		randomPercent = atoi(lr_eval_string("{RANDOM_PERCENT}"));

		if ( randomPercent <= DRILL_SUB_CATEGORY ){
			drill_by = "SUB_CATEGORY" ;
			}  
		else if ( randomPercent <= (DRILL_SUB_CATEGORY + DRILL_TWO_FACET) ) {
			drill_by = "TWO_FACET" ;
			}  
		else if ( randomPercent <= (DRILL_SUB_CATEGORY + DRILL_TWO_FACET + DRILL_THREE_FACET)) {
			drill_by = "THREE_FACET" ;
			}  
		else if (randomPercent <= (DRILL_SUB_CATEGORY + DRILL_TWO_FACET + DRILL_THREE_FACET + DRILL_ONE_FACET)) {
			drill_by = "ONE_FACET" ;
			}  

	}  

	if (strcmp(drill_by, "ONE_FACET") == 0) {
		 
		drillOneFacet();
		}  
	else if (strcmp(drill_by, "TWO_FACET") == 0) {
		 
		drillTwoFacets();
		}  
	else if (strcmp(drill_by, "THREE_FACET") == 0) {
		 
		drillThreeFacets();
		}  
	else if (strcmp(drill_by, "SUB_CATEGORY") == 0) {
		 
		drillSubCategory();
		}  
	else if (strcmp(drill_by, "stop") == 0) {
		 
 
		}  
	else if (strcmp(drill_by, "pass") == 0){
 
		 
	}  
}  


void productDetailView()
{
	 
 	lr_think_time ( LINK_TT ) ;
	
	if (( lr_paramarr_len ( "pdpURL" ) > 0 )) {
		
		TCPIShippingView();
		
		productDisplayPageCorrelations();
 
 
		lr_param_sprintf ( "drillUrl" , "http://%s%s" , lr_eval_string("{host}") , lr_paramarr_random ( "pdpURL" ) ) ;
		
		lr_start_transaction ( "T04_Product Display Page" ) ;
		
		webURL(lr_eval_string ( "{drillUrl}" ), "T04_Product Display Page" );
	
 
		
			addToCartCorrelations();
			
 

				lr_start_sub_transaction ("T04_Product Display Page - TCPSkuSelectionView", "T04_Product Display Page" );

				web_custom_request("TCPSKUSelectionView",
					"URL=http://{host}/webapp/wcs/stores/servlet/TCPSKUSelectionView?langId=-1&storeId={storeId}&catalogId={catalogId}&LimQty=undefined&TCPWebOnlyFlag=&productId={skuSelectionProductID}",
					"Method=GET",
					"Resource=0",
					"RecContentType=text/html",
					"Snapshot=t18.inf",
					"Mode=HTML",
					"EncType=application/x-www-form-urlencoded",
					"LAST");
						
				lr_end_sub_transaction ( "T04_Product Display Page - TCPSkuSelectionView", 0 );
 

			if ( isLoggedIn == 1 ) {
				TCPGetWishListForUsersView();
				 
			}
	 

		lr_end_transaction ( "T04_Product Display Page" , 0 ) ;
		
		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) < PRODUCT_QUICKVIEW_SERVICE )
			productQuickviewService();

	}  
	
}  


void productQuickView()
{
	 
 	lr_think_time ( LINK_TT ) ;
	if (( lr_paramarr_len ( "quickviewURL" ) > 0 )) {
		
		productDisplayPageCorrelations();
 
 
		lr_param_sprintf ( "drillUrl" , "http://%s%s" , lr_eval_string("{host}") , lr_paramarr_random ( "quickviewURL" ) ) ;
		
		lr_start_transaction ( "T04_Product Quickview Page" ) ;
		
			webURL(lr_eval_string ( "{drillUrl}" ), "T04_Product Quickview Page" );
		
		 
			addToCartCorrelations();
			
 

				lr_start_sub_transaction ("T04_Product Quickview Page - TCPSkuSelectionView", "T04_Product Quickview Page" );

				web_custom_request("TCPSKUSelectionView",
					"URL=http://{host}/webapp/wcs/stores/servlet/TCPSKUSelectionView?langId=-1&storeId={storeId}&catalogId={catalogId}&LimQty=undefined&TCPWebOnlyFlag=&productId={skuSelectionProductID}",
					"Method=GET",
					"Resource=0",
					"RecContentType=text/html",
					"Snapshot=t18.inf",
					"Mode=HTML",
					"EncType=application/x-www-form-urlencoded",
					"LAST");

				lr_end_sub_transaction ( "T04_Product Quickview Page - TCPSkuSelectionView", 0 );
 

			 
				 
 
		
		lr_end_transaction ( "T04_Product Quickview Page" , 0 ) ;


	}  
}  

void productQuickviewService()  
{
		web_reg_find("SaveCount=swatchesAndSizeInfo", "Text/IC=itemTCPBogo","LAST");
		
		lr_start_transaction ("T04_Product Quickview Service - GetSwatchesAndSizeInfo" );

			web_custom_request("GetSwatchesAndSizeInfo",
 
				"URL=http://{host}/webapp/wcs/stores/servlet/GetSwatchesAndSizeInfo?storeId={storeId}&productId={skuSelectionProductID}",
				"Method=GET",
				"Mode=HTML",
				"LAST");


		if(atoi(lr_eval_string("{swatchesAndSizeInfo}")) > 0)
			lr_end_transaction ( "T04_Product Quickview Service - GetSwatchesAndSizeInfo", 0 );	
		else
			lr_end_transaction ( "T04_Product Quickview Service - GetSwatchesAndSizeInfo", 1 );	
	

		web_reg_find("SaveCount=detailsView", "Text/IC=inventoryFlag","LAST");

		lr_start_transaction ("T04_Product Quickview Service - TCPGetSKUDetailsView" );

			web_custom_request("TCPGetSKUDetailsView",
 
				"URL=http://{host}/webapp/wcs/stores/servlet/TCPGetSKUDetailsView?storeId={storeId}&catalogId={catalogId}&langId=-1&productId={skuSelectionProductID}",
				"Method=GET",
				"Mode=HTML",
				"LAST");

	
		if(atoi(lr_eval_string("{detailsView}")) > 0)
			lr_end_transaction ( "T04_Product Quickview Service - TCPGetSKUDetailsView", 0 );	
		else
			lr_end_transaction ( "T04_Product Quickview Service - TCPGetSKUDetailsView", 1 );	

}

void productDisplay()
{
 
 

	 

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= PDP ){
		 
		productDetailView();  
		}  
	else {
		 
		productQuickView();  
	}  
 

}  



void switchColor()
{
}  

void emailSignUp()
{
	lr_start_transaction ( "T16_Submit Shipping Address" ) ;
	lr_end_transaction ( "T16_Submit Shipping Address" , 0) ;

}  


void StoreLocator()
{

 	lr_think_time ( LINK_TT ) ;

	lr_start_transaction("T22_StoreLocator");

	web_url("Stores", 
		"URL=http://{host}/shop/AjaxStoreLocatorDisplayView?catalogId={catalogId}&langId=-1&storeId={storeId}", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=http://{host}/shop/us/home", 
		"Snapshot=t325.inf", 
		"Mode=HTML", 
		"LAST");

	lr_end_transaction("T22_StoreLocator", 2);
	
	lr_start_transaction("T22_StoreLocator_Find");

	web_submit_data("AjaxStoreLocatorResultsView", 
		"Action=http://{host}/shop/AjaxStoreLocatorResultsView?catalogId={catalogId}&langId=-1&orderId=&storeId={storeId}", 
		"Method=POST", 
		"TargetFrame=", 
		"RecContentType=text/html", 
		"Referer=http://{host}/shop/AjaxStoreLocatorDisplayView?catalogId={catalogId}&langId=-1&storeId={storeId}", 
		"Snapshot=t326.inf", 
		"Mode=HTML", 
		"ITEMDATA", 
		"Name=distance", "Value={storeLocatorDistance}", "ENDITEM", 
		"Name=latitude", "Value={storeLocatorLatitude}", "ENDITEM", 
		"Name=longitude", "Value={storeLocatorLongitude}", "ENDITEM", 
		"Name=displayStoreInfo", "Value=false", "ENDITEM", 
		"Name=fromPage", "Value=StoreLocator", "ENDITEM", 
		"Name=objectId", "Value=", "ENDITEM", 
		"Name=requesttype", "Value=ajax", "ENDITEM", 
		"LAST");

	lr_end_transaction("T22_StoreLocator_Find", 2);

}

void TCPIShippingView()
{
	lr_think_time ( LINK_TT ) ;
	
	 
	
	lr_start_transaction("T27_TCPIShippingView");
	
	web_custom_request("TCPIShippingView",
		"URL=https://{host}/shop/TCPIShippingView?langId=-1&storeId={storeId}&catalogId={catalogId}",
		"Method=GET",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		"EncType=application/x-www-form-urlencoded",
		"LAST");
		
	lr_end_transaction("T27_TCPIShippingView", 2);
	 
	 
	
}

void TCPGetWishListForUsersViewBrowse()
{
	lr_start_transaction("T25_Common_S01_TCPGetWishListForUsersView");

	web_custom_request("TCPGetWishListForUsersView",
		"URL=http://{host}/webapp/wcs/stores/servlet/TCPGetWishListForUsersView?tcpCallBack=jQuery1113014590106982485151_1473881708524&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&sortBy=&_=1473881708525", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Mode=HTML", 
		"EncType=application/x-www-form-urlencoded", 
		"LAST");

	lr_end_sub_transaction("T25_Common_S01_TCPGetWishListForUsersView", 2);
	
}
# 1 "vuser_init.c" 2

# 1 "..\\..\\checkoutFunctions.c" 1
# 1 "..\\..\\checkoutWorkloadModel.c" 1
int CART_PAGE_DROP, LOGIN_PAGE_DROP, SHIP_PAGE_DROP, BILL_PAGE_DROP, REVIEW_PAGE_DROP;  
 
int RATIO_CHECKOUT_GUEST, RATIO_CHECKOUT_LOGIN, RATIO_CHECKOUT_LOGIN_FIRST;  
int OFFLINE_PLUGIN, VISA, MASTER, AMEX, PLCC, GIFT, DISCOVER;  
int RATIO_PROMO_APPLY, RATIO_PROMO_MULTIUSE, RATIO_PROMO_SINGLEUSE, RATIO_PROMO_COUPON_REMOVE, RATIO_REDEEM_LOYALTY_POINTS;  
int RATIO_REGISTER, RATIO_UPDATE_QUANTITY, RATIO_DELETE_ITEM, RATIO_SELECT_COLOR, RATIO_ORDER_HISTORY, RATIO_POINTS_HISTORY, RATIO_RESERVATION_HISTORY, RATIO_RESERVATION_WISHLIST; 
int RATIO_CART_MERGE, RATIO_DROP_CART, RATIO_WISHLIST, cartMerge, currentCartSize; RATIO_BUILDCART_DRILLDOWN, USE_LOW_INVENTORY;
int	RATIO_WL_VIEW, RATIO_WL_CREATE,	RATIO_WL_DELETE, RATIO_WL_CHANGE, RATIO_WL_DELETE_ITEM, RATIO_WL_ADD_ITEM, RATIO_WL_ADD_TO_CART;
int MOVE_FROM_CART_TO_WISHLIST, MOVE_FROM_WISHLIST_TO_CART;
  
	 
	LOGIN_PAGE_DROP = 0;  
	SHIP_PAGE_DROP = 60;  
	BILL_PAGE_DROP = 30;  
	 
 





	 
 
 
	RATIO_CHECKOUT_LOGIN = 90;  
	RATIO_CHECKOUT_GUEST = 10;  

	 
	RATIO_DROP_CART = 60;  

     
    RATIO_CHECKOUT_LOGIN_FIRST = 70;  

     
    RATIO_BUILDCART_DRILLDOWN = 20;  
	
	USE_LOW_INVENTORY = 5;
	
	 
	OFFLINE_PLUGIN = 100;
	VISA = 0;
	MASTER = 0;
	AMEX = 0;
	PLCC = 0;
	GIFT = 0;
	DISCOVER = 0;

	 
	RATIO_PROMO_APPLY = 35;  
	RATIO_PROMO_MULTIUSE = 95; 
	RATIO_PROMO_SINGLEUSE = 5; 
	RATIO_REDEEM_LOYALTY_POINTS = 15;
	
	 



	 
	RATIO_CART_MERGE = 0;  

 
	RATIO_REGISTER = 100;

	 
	RATIO_UPDATE_QUANTITY = 100;

	 
	RATIO_DELETE_ITEM = 100;  
	
	 
	RATIO_SELECT_COLOR = 50;
	 
	 
	
	RATIO_RESERVATION_HISTORY = 10;  
	RATIO_POINTS_HISTORY = 30;  
	RATIO_ORDER_HISTORY = 60;  

	RATIO_ORDER_STATUS = 25;  

	 
	RATIO_WISHLIST		 = 50;  

	RATIO_WL_CREATE      = 60;  
	RATIO_WL_DELETE      = 2;  
	RATIO_WL_CHANGE      = 3;  
	RATIO_WL_DELETE_ITEM = 5;
	RATIO_WL_ADD_ITEM    = 1;  
	RATIO_WL_ADD_TO_CART = 25;  

	MOVE_FROM_CART_TO_WISHLIST = 100;  
	MOVE_FROM_WISHLIST_TO_CART = 100;  
	
# 1 "..\\..\\checkoutFunctions.c" 2


int index , length , i, target_itemsInCart, index_buildCart, randomPercent, isLoggedIn;
int isLoggedIn=0;
char *searchString ;
char *nav_by ;  
char *drill_by ;  
char *sort_by;  
char *product_by;  
int atc_Stat = 0;  
int HttpRetCode;
int start_time, target_time;

 
int rNum;
unsigned short updateStatus;
char **colnames = 0;
char **rowdata = 0;
PVCI2 pvci = 0;

 
 
 
 
 

void getCatEntryID() {
 

	lr_save_string(lr_eval_string("{lowQty_SKU}"), "atc_catentryId");
	
	if ( strcmp(lr_eval_string("{atc_catentryId}"), lr_eval_string("{lastvalue}") ) == 0 ) {
		lr_param_sprintf ( "atc_catentryId" , "%s" , lr_paramarr_random ( "atc_catentryIds" ) ) ;
		USE_LOW_INVENTORY = 0;
	}
	else {
		lr_save_string(lr_eval_string("{atc_catentryId}"), "lastvalue");
		lr_save_string(lr_eval_string("{atc_catentryId}"), "atc_comment");
		lr_start_transaction("T20_Low_QTY_Count");
		lr_end_transaction("T20_Low_QTY_Count", 2);
	}
	
	return;
}

void addToCart()
{
	int k = 0, newTime, RANDOM_PERCENT = 0;
	atc_Stat = 0;  

	if ( lr_paramarr_len ( "atc_catentryIds" ) > 0 ) {  

		lr_param_sprintf ( "atc_catentryId" , "%s" , lr_paramarr_random ( "atc_catentryIds" ) ) ;
		web_reg_save_param ( "orderId" , "LB=\"orderId\": [\"" , "RB=\"]" , "NotFound=Warning",  "LAST" ) ;
		web_reg_save_param ( "orderItemId" , "LB=\"orderItemId\": [\"" , "RB=\"]" , "NotFound=Warning", "LAST" ) ;
		web_reg_find("Text=the products you wish to purchase are not available", "SaveCount=atcErrorFound");
	 

	 
 
# 87 "..\\..\\checkoutFunctions.c"
		lr_think_time ( FORM_TT ) ;
 
 

		lr_start_transaction ( "T05_Add To Cart" ) ;

		if ( authcookielen == 0 ) {
			web_reg_save_param_regexp ( "ParamName=authTokens" , "RegExp=WC_AUTHENTICATION_[0-9]+=([^D][^;]+);" , "SEARCH_FILTERS" , "Scope=Headers" , "NotFound=Warning", "Ordinal=All", "LAST" ) ;
			lr_start_sub_transaction ("T05_Add To Cart_S01_AjaxOrderChangeServiceItemAdd", "T05_Add To Cart" ) ;

			web_submit_data("addtocart_AjaxOrderChangeServiceItemAdd",
				"Action=https://{host}/webapp/wcs/stores/servlet/AjaxOrderChangeServiceItemAdd",
				"Method=POST",
				"RecContentType=text/html",
				"Snapshot=t25.inf",
				"Mode=HTML",
				"ITEMDATA",
				"Name=storeId", "Value={storeId}", "ENDITEM",
				"Name=catalogId", "Value={catalogId}", "ENDITEM",
				"Name=langId", "Value=-1", "ENDITEM",
				"Name=orderId", "Value=.", "ENDITEM",
				"Name=field2", "Value=0", "ENDITEM",
				"Name=comment", "Value={atc_comment}", "ENDITEM", 
				"Name=calculationUsage", "Value=-1,-2,-5,-6,-7", "ENDITEM",
				"Name=catEntryId", "Value={atc_catentryId}", "ENDITEM", 
				"Name=quantity", "Value=1", "ENDITEM",
				"Name=requesttype", "Value=ajax", "ENDITEM",
				"Name=visitorId", "Value=[CS]v1|2B0B56810507A725-40000116E00E1B28[CE]", "ENDITEM",  
				"LAST");

			if ( atoi(lr_eval_string("{atcErrorFound}")) == 1 ) {
 
 
				atc_Stat = 1;  
				lr_end_sub_transaction ("T05_Add To Cart_S01_AjaxOrderChangeServiceItemAdd", 1) ;
				lr_end_transaction ( "T05_Add To Cart" , 1 ) ;
				return;
			}  

			if ( strlen(lr_eval_string("{orderId}")) <= 0 ) {
				atc_Stat = 1;  
				lr_end_sub_transaction ("T05_Add To Cart_S01_AjaxOrderChangeServiceItemAdd", 1) ;
				 
				lr_end_transaction ( "T05_Add To Cart" , 1 ) ;
				return;
			}  

			lr_end_sub_transaction ("T05_Add To Cart_S01_AjaxOrderChangeServiceItemAdd", 2) ;

			if ( lr_paramarr_len( "authTokens" ) > 0 ) {
				if ( authcookielen == 0 ) {
					lr_save_string ( lr_paramarr_idx( "authTokens" , 1 ) , "authToken" ) ;
					authcookielen = 1 ;
				}  
			}  
		}  
		else {
			lr_start_sub_transaction ("T05_Add To Cart_S01_AjaxOrderChangeServiceItemAdd", "T05_Add To Cart" ) ;

			web_submit_data("addtocart_AjaxOrderChangeServiceItemAdd",
				"Action=https://{host}/webapp/wcs/stores/servlet/AjaxOrderChangeServiceItemAdd",
				"Method=POST",
				"RecContentType=text/html",
				"Snapshot=t25.inf",
				"Mode=HTML",
				"ITEMDATA",
				"Name=storeId", "Value={storeId}", "ENDITEM",
				"Name=catalogId", "Value={catalogId}", "ENDITEM",
				"Name=langId", "Value=-1", "ENDITEM",
				"Name=orderId", "Value=.", "ENDITEM",
				"Name=field2", "Value=0", "ENDITEM",
				"Name=comment", "Value={atc_comment}", "ENDITEM", 
				"Name=calculationUsage", "Value=-1,-2,-5,-6,-7", "ENDITEM",
				"Name=catEntryId", "Value={atc_catentryId}", "ENDITEM",
				"Name=quantity", "Value=1", "ENDITEM",
				"Name=requesttype", "Value=ajax", "ENDITEM",
				"Name=visitorId", "Value=[CS]v1|2B0B56810507A725-40000116E00E1B28[CE]", "ENDITEM",  
				"LAST");

			if ( atoi(lr_eval_string("{atcErrorFound}")) == 1 ) {
 
 
				atc_Stat = 1;  
				lr_end_sub_transaction ("T05_Add To Cart_S01_AjaxOrderChangeServiceItemAdd", 1) ;
				lr_end_transaction ( "T05_Add To Cart" , 1 ) ;
				return;
			}  
			
			if ( strlen(lr_eval_string("{orderId}")) <= 0 ) {
				atc_Stat = 1;  
				 
				lr_end_sub_transaction ("T05_Add To Cart_S01_AjaxOrderChangeServiceItemAdd", 1) ;
				lr_end_transaction ( "T05_Add To Cart" , 1 ) ;
				return;
			}  

			lr_end_sub_transaction ("T05_Add To Cart_S01_AjaxOrderChangeServiceItemAdd", 2) ;
		}  

		lr_start_sub_transaction ("T05_Add To Cart_S02_TCPAdd2CartQuickView", "T05_Add To Cart" ) ;

		web_custom_request("addtocart_TCPAdd2CartQuickView",
			"URL=https://{host}/webapp/wcs/stores/servlet/TCPAdd2CartQuickView?langId=-1&storeId={storeId}&catalogId={catalogId}",
			"Method=GET",
			"Resource=0",
			"RecContentType=text/html",
			"Snapshot=t27.inf",
			"Mode=HTML",
			"EncType=application/x-www-form-urlencoded",
			"LAST");

		lr_end_sub_transaction ("T05_Add To Cart_S02_TCPAdd2CartQuickView", 2) ;

		lr_start_sub_transaction ("T05_Add To Cart_S03_CreateCookieCmd", "T05_Add To Cart" ) ;

		web_custom_request("addtocart_CreateCookieCmd",
			"URL=https://{host}/webapp/wcs/stores/servlet/CreateCookieCmd?langId=-1&storeId={storeId}&goToPage=NoURL&catalogId={catalogId}",
			"Method=GET",
			"Resource=0",
			"RecContentType=text/html",
			"Snapshot=t28.inf",
			"Mode=HTML",
			"EncType=application/x-www-form-urlencoded",
			"LAST");

		lr_end_sub_transaction ("T05_Add To Cart_S03_CreateCookieCmd", 2) ;
		
		createCookieCmd();

		lr_start_sub_transaction ("T05_Add To Cart_S04_TCPMiniShopCartDisplayView", "T05_Add To Cart" ) ;

		web_submit_data("addtocart_TCPMiniShopCartDisplayView",
			"Action=https://{host}/shop/TCPMiniShopCartDisplayView?catalogId={catalogId}&langId=-1&storeId={storeId}",
			"Method=POST",
			"RecContentType=text/html",
			"Snapshot=t29.inf",
			"Mode=HTML",
			"ITEMDATA",
			"Name=addedOrderItemId", "Value={orderItemId}", "ENDITEM", 
			"Name=showredEye", "Value=true", "ENDITEM",
			"Name=objectId", "Value=", "ENDITEM",
			"Name=requesttype", "Value=ajax", "ENDITEM",
			"LAST");

		lr_end_sub_transaction ("T05_Add To Cart_S04_TCPMiniShopCartDisplayView" , 2) ;

		lr_end_transaction ( "T05_Add To Cart" , 2 ) ;

	}  
	else
	{
		atc_Stat = 1;  
		return;
	}  

}  

void parseOrderItemId() {  

    extern char * strtok(char * string, const char * delimiters ); 
    char path[1000] = "";
    char separators[] = "\""; 
    char * token;
    char fullpath[1024];
    int counter = 0;
    strcpy(path, lr_eval_string("{unavailId_1}"));

     

    token = (char *)strtok(path, separators); 
    if (!token) {
        lr_output_message ("No tokens found in string!");
    }
 
    while (token != 0 ) { 
 
 
        token = (char *)strtok(0, separators); 
        
        if(token != 0) {
	        if (strlen(token) > 10) {
	        	counter++;
	        	lr_param_sprintf ("count", "%d", counter);
	        	lr_save_string(token, lr_eval_string("upcId{count}") );
	        }
        }
    }
	return;
}

void getOutOfStockItemIds(){
	
	int i;
	
	if (atoi(lr_eval_string("{unavailId_count}")) > 0) {
		
 		
# 315 "..\\..\\checkoutFunctions.c"
		web_reg_save_param("orderItemId", "LB=id=\"{unavailId_1}\" data-item-id=\"", "RB=\">", "NotFound=Warning", "LAST"); 
		
		web_url("OrderCalculate", 
		"URL=https://{host}/shop/OrderCalculate?calculationUsageId=-1&updatePrices=1&catalogId={catalogId}&errorViewName=AjaxCheckoutDisplayView&orderId=.&langId=-1&storeId={storeId}&URL=AjaxCheckoutDisplayView", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Mode=HTML", 
		"LAST");
		
		removeOutOfStockItem();
		
	}	
	return;
}

void removeOutOfStockItem() {
	
	int i;

 
	while ( atoi(lr_eval_string("{unavailId_count}")) !=0 ) {
		 
 		
# 358 "..\\..\\checkoutFunctions.c"
		lr_start_transaction("T20_Remove_OutOfStockItem");

		web_submit_data("AjaxOrderChangeServiceItemDelete", 
			"Action=https://{host}/webapp/wcs/stores/servlet/AjaxOrderChangeServiceItemDelete", 
			"Method=POST", 
			"TargetFrame=", 
			"RecContentType=application/json", 
			"Mode=HTML", 
			"ITEMDATA", 
			"Name=storeId", "Value={storeId}", "ENDITEM", 
			"Name=catalogId", "Value={catalogId}", "ENDITEM", 
			"Name=langId", "Value=-1", "ENDITEM", 
			"Name=orderId", "Value={orderId}", "ENDITEM", 
			"Name=orderItemId", "Value={orderItemId}", "ENDITEM", 
			"Name=visitorId", "Value=", "ENDITEM", 
			"Name=calculationUsage", "Value=-1,-2,-5,-6,-7", "ENDITEM", 
			"Name=requesttype", "Value=ajax", "ENDITEM", 
			"LAST");

		web_submit_data("AjaxTCPShutterflyPromoDisplayEspotView", 
			"Action=https://{host}/shop/AjaxTCPShutterflyPromoDisplayEspotView?catalogId={catalogId}&emsName=ShutterflyPromoEspot&storeId={storeId}&storeName=us&domainName=BlaBlaDomain", 
			"Method=POST", 
			"TargetFrame=", 
			"RecContentType=text/html", 
			"Mode=HTML", 
			"ITEMDATA", 
			"Name=objectId", "Value=", "ENDITEM", 
			"Name=requesttype", "Value=ajax", "ENDITEM", 
			"LAST");

		web_custom_request("CreateCookieCmd", 
			"URL=https://{host}/webapp/wcs/stores/servlet/CreateCookieCmd?langId=-1&storeId={storeId}&goToPage=NoURL&catalogId={catalogId}", 
			"Method=GET", 
			"TargetFrame=", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Mode=HTML", 
			"EncType=application/x-www-form-urlencoded", 
			"LAST");

		web_submit_data("TCPMiniShopCartDisplayView1", 
			"Action=https://{host}/shop/TCPMiniShopCartDisplayView1?catalogId={catalogId}&langId=-1&storeId={storeId}", 
			"Method=POST", 
			"TargetFrame=", 
			"RecContentType=text/html", 
			"Mode=HTML", 
			"ITEMDATA", 
			"Name=showredEye", "Value=true", "ENDITEM", 
			"Name=objectId", "Value=", "ENDITEM", 
			"Name=requesttype", "Value=ajax", "ENDITEM", 
			"LAST");

		web_submit_data("ShopCartDisplayView", 
			"Action=https://{host}/shop/ShopCartDisplayView?shipmentType=single&catalogId={catalogId}&langId=-1&storeId={storeId}", 
			"Method=POST", 
			"TargetFrame=", 
			"RecContentType=text/html", 
			"Mode=HTML", 
			"ITEMDATA", 
			"Name=showredEye", "Value=true", "ENDITEM", 
			"Name=objectId", "Value=", "ENDITEM", 
			"Name=requesttype", "Value=ajax", "ENDITEM", 
			"LAST");

		lr_end_transaction("T20_Remove_OutOfStockItem",2);
 
			web_reg_save_param("unavailId", "LB=unavailId\": [\"", "RB=\"", "ORD=All", "NotFound=Warning", "LAST");
 
 

			web_submit_data("ViewCart_TCPAjaxCheckInventoryAvail",
				"Action=https://{host}/webapp/wcs/stores/servlet/TCPAjaxCheckInventoryAvail",
				"Method=POST",
				"RecContentType=text/html",
				"Snapshot=t22.inf",
				"Mode=HTML",
				"ITEMDATA",
				"Name=orderId", "Value={orderId}", "ENDITEM",
				"Name=requesttype", "Value=ajax", "ENDITEM",
				"LAST");
				
			if (atoi(lr_eval_string("{unavailId_count}")) !=0 ) {
				web_reg_save_param("orderItemId", "LB=id=\"{unavailId_1}\" data-item-id=\"", "RB=\">", "NotFound=Warning", "LAST");  
				
				web_url("OrderCalculate", 
				"URL=https://{host}/shop/OrderCalculate?calculationUsageId=-1&updatePrices=1&catalogId={catalogId}&errorViewName=AjaxCheckoutDisplayView&orderId=.&langId=-1&storeId={storeId}&URL=AjaxCheckoutDisplayView", 
				"TargetFrame=", 
				"Resource=0", 
				"RecContentType=text/html", 
				"Mode=HTML", 
				"LAST");
			}
	}
	
	lr_save_string("0", "OutOfStock_Count");
	
	return;
}


void viewCart()
{
	 
	lr_think_time ( LINK_TT ) ;
	web_reg_save_param ( "totalNumberOfItems", "LB=totalNumberOfItems\" value='", "RB=\'", "Ord=ALL", "NotFound=Warning", "LAST");
	web_reg_save_param ( "orderItemIds" , "LB=<input type=\"hidden\" value='" , "RB=' name='orderItem_" , "Ord=ALL" , "NotFound=Warning", "LAST" ) ;
	web_reg_save_param ("catalogIds", "LB=id=\"catalogEntry_img", "RB=\"", "ORD=All", "NotFound=Warning", "LAST");  

	 

	lr_start_transaction ( "T06_View Cart" ) ;

		lr_start_sub_transaction ( "T06_View Cart_S01_OrderCalculate", "T06_View Cart" );

		web_url("ViewCart_OrderCalculate",
			"URL=https://{host}/shop/OrderCalculate?calculationUsageId=-1&updatePrices=1&catalogId={catalogId}&errorViewName=AjaxOrderItemDisplayView&orderId=.&langId=-1&storeId={storeId}&URL=AjaxOrderItemDisplayView",
			"Resource=0",
			"RecContentType=text/html",
			"Snapshot=t21.inf",
			"Mode=HTML",
			"LAST");

		lr_end_sub_transaction("T06_View Cart_S01_OrderCalculate", 2);

 
		if ( atoi( lr_eval_string("{orderItemIds_count}") ) != 0 ) {

			web_reg_save_param("unavailId", "LB=unavailId\": [\"", "RB=\"", "ORD=All", "NotFound=Warning", "LAST");
 
 
			lr_start_sub_transaction ( "T06_View Cart_S02_TCPAjaxCheckInventoryAvail", "T06_View Cart" );  

			web_submit_data("ViewCart_TCPAjaxCheckInventoryAvail",
				"Action=https://{host}/webapp/wcs/stores/servlet/TCPAjaxCheckInventoryAvail",
				"Method=POST",
				"RecContentType=text/html",
				"Snapshot=t22.inf",
				"Mode=HTML",
				"ITEMDATA",
				"Name=orderId", "Value={orderId}", "ENDITEM",
				"Name=requesttype", "Value=ajax", "ENDITEM",
				"LAST");

			lr_end_sub_transaction("T06_View Cart_S02_TCPAjaxCheckInventoryAvail", 2);

			orderItemIdscount = atoi( lr_eval_string("{orderItemIds_count}") );
			
			 
			
			lr_start_sub_transaction ( "T06_View Cart_S03_Shopping_Cart_Total_Espot", "T06_View Cart" ); 
			
			web_url("ViewCart_Shopping_Cart_Total_Espot",
				"URL=https://{host}/wcs/resources/store/{storeId}/espot/Shopping_Cart_Total_Espot?responseFormat=json",
				"Resource=0",
				"RecContentType=text/html",
				"Snapshot=t21.inf",
				"Mode=HTML",
				"LAST");
				
			lr_end_sub_transaction("T06_View Cart_S03_Shopping_Cart_Total_Espot", 2);
			
			lr_start_sub_transaction ( "T06_View Cart_S04_AjaxOrderItemDisplayView", "T06_View Cart" ); 
			
			web_custom_request("viewCart_AjaxOrderItemDisplayView",
				"URL=https://{host}/shop/AjaxOrderItemDisplayView?catalogId={catalogId}&langId=-1&storeId={storeId}&krypto=dUE94QLj%2B8l%2F61T%2BhJO9u4yS7EVZpPkI84e3SBBgVuCk8jRJ6zCYMjCwYUq1cu9TXa709PFhVY0d%0A5tJ5RMWNsEyGSr2pHXhjxgzAtKj%2Fj%2FqCjbnYT9tr%2Fo6aMm8C%2BUsD&ddkey=https:OrderCalculate",
				"Method=GET",
				"Resource=0",
				"RecContentType=text/html",
				"Snapshot=t27.inf",
				"Mode=HTML",
				"EncType=application/x-www-form-urlencoded",
				"LAST");
				
			lr_end_sub_transaction("T06_View Cart_S04_AjaxOrderItemDisplayView", 2);
			
			
			lr_end_transaction ( "T06_View Cart" , 0 ) ;
			
			getOutOfStockItemIds();

		}
		else {
			orderItemIdscount = 0;  
			
			lr_end_transaction ( "T06_View Cart" , 0 ) ;

		}
		
}  

void viewCartFromLogin()
{
	 
	lr_think_time ( LINK_TT ) ;
	web_reg_save_param("totalNumberOfItems", "LB=totalNumberOfItems\" value='", "RB=\'", "Ord=All", "NotFound=Warning", "LAST");
	web_reg_save_param ( "orderItemIds" , "LB=<input type=\"hidden\" value='" , "RB=' name='orderItem_" , "NotFound=Warning", "Ord=ALL" , "LAST" ) ;

	lr_start_transaction ( "T06_View Cart" ) ;

		lr_start_sub_transaction ( "T06_View Cart_S01_OrderCalculate", "T06_View Cart" );

		web_url("viewCartFromLogin_OrderCalculate",
			"URL=http://{host}/shop/OrderCalculate?calculationUsageId=-1&updatePrices=1&catalogId={catalogId}&errorViewName=AjaxOrderItemDisplayView&orderId=.&langId=-1&storeId={storeId}&URL=AjaxOrderItemDisplayView",
			"Resource=0",
			"RecContentType=text/html",
			"Snapshot=t21.inf",
			"Mode=HTML",
			"LAST");

		lr_end_sub_transaction("T06_View Cart_S01_OrderCalculate", 2);

		web_reg_save_param("unavailId", "LB=unavailId\": [\n", "RB=\t\t],\n", "ORD=All", "NotFound=Warning", "LAST");

 

			lr_start_sub_transaction ( "T06_View Cart_S02_TCPAjaxCheckInventoryAvail", "T06_View Cart" );  

			web_submit_data("viewCartFromLogin_TCPAjaxCheckInventoryAvail",
				"Action=https://{host}/webapp/wcs/stores/servlet/TCPAjaxCheckInventoryAvail",
				"Method=POST",
				"RecContentType=text/html",
				"Snapshot=t22.inf",
				"Mode=HTML",
				"ITEMDATA",
				"Name=orderId", "Value={orderId}", "ENDITEM",
				"Name=requesttype", "Value=ajax", "ENDITEM",
				"LAST");

			lr_end_sub_transaction("T06_View Cart_S02_TCPAjaxCheckInventoryAvail", 2);
 		






	lr_end_transaction ( "T06_View Cart" , 0 ) ;

	getOutOfStockItemIds();
	
}  

void applyPromoCode(int useRewards)  
{
	lr_think_time ( FORM_TT ) ;

	if ( useRewards == 1 ){
		lr_save_string( lr_eval_string ( "{promocodeRewards}" ) , "promocode" ) ;
 
		lr_save_string( "Rewards" , "promotype" ) ;
	}
	else if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_PROMO_MULTIUSE ) {
		lr_save_string( lr_eval_string ( "{multiusePromoCode}" ) , "promocode" ) ;
		lr_save_string( "Multi-Use" , "promotype" ) ;
	}  
	else if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= (RATIO_PROMO_SINGLEUSE + RATIO_PROMO_MULTIUSE) ) {
		
		if ( strcmp(lr_eval_string("{scriptOrigin}"), "US") == 0)
			getPromoCode();
		else
			lr_save_string( lr_eval_string ( "{singleusePromocode}" ) , "promocode" ) ;
		
		lr_save_string( "Single-Use" , "promotype" ) ;
	}

	web_reg_save_param( "invalidCoupon", "LB=This code is ", "RB=t applicable", "NotFound=Warning", "LAST");
	web_reg_save_param( "invalidCoupon2", "LB=errorMessageKey\": \"ERR_PROMOTI", "RB=_CODE_INVALID", "NotFound=Warning", "LAST");  
	
	lr_start_transaction ( lr_eval_string ( "T07_Enter Promo {promotype}" ) ) ;

		lr_start_transaction ( "T07_Enter Promo_S01_AjaxPromotionCodeManage" );

		web_submit_data ( lr_eval_string("{promotype}_AjaxPromotionCodeManage") ,
		  "Action=https://{host}/webapp/wcs/stores/servlet/AjaxPromotionCodeManage" ,
		  "Method=POST" ,
		  "Mode=HTML" ,
		  "ITEMDATA" ,
			"Name=orderId" , "Value={orderId}" , "ENDITEM" ,
			"Name=taskType" , "Value=A" , "ENDITEM" ,
			"Name=URL" , "Value=" , "ENDITEM" ,
			"Name=storeId" , "Value={storeId}" , "ENDITEM" ,
			"Name=langId" , "Value=-1" , "ENDITEM" ,
			"Name=catalogId" , "Value={catalogId}" , "ENDITEM" ,
			"Name=finalView" , "Value=AjaxOrderItemDisplayView" , "ENDITEM" ,
			"Name=fromPage" , "Value=shoppingCartDisplay" , "ENDITEM" ,
			"Name=promoCode" , "Value={promocode}" , "ENDITEM" ,
			"Name=requesttype" , "Value=ajax" , "ENDITEM" ,
		  "LAST" ) ;

	if (  strcmp( lr_eval_string("{invalidCoupon2}"), "ON" ) == 0 ) {
		lr_end_transaction("T07_Enter Promo_S01_AjaxPromotionCodeManage", 0)	;
		lr_end_transaction ( lr_eval_string ( "T07_Enter Promo {promotype}" ) , 0 ) ;
	} 
	else  
	{ 
		lr_end_transaction("T07_Enter Promo_S01_AjaxPromotionCodeManage", 2)	;
			
		lr_start_transaction ( "T07_Enter Promo_S02_AjaxOrderChangeServiceItemUpdate" );

		web_submit_data ( lr_eval_string("{promotype}_AjaxOrderChangeServiceItemUpdate") ,
						  "Action=https://{host}/webapp/wcs/stores/servlet/AjaxOrderChangeServiceItemUpdate" ,
						  "Method=POST" ,
						  "Mode=HTML" ,
						  "ITEMDATA" ,
							"Name=orderId" , "Value=." , "ENDITEM" ,
							"Name=calculationUsage" , "Value=-1,-2,-5,-6,-7" , "ENDITEM" ,
							"Name=requesttype" , "Value=ajax" , "ENDITEM" ,
						  "LAST" ) ;

		lr_end_transaction("T07_Enter Promo_S02_AjaxOrderChangeServiceItemUpdate", 2)	;

		lr_start_transaction ( "T07_Enter Promo_S03_CreateCookieCmd" );

		web_url ( lr_eval_string("{promotype}_CreateCookieCmd") ,
				  "URL=https://{host}/webapp/wcs/stores/servlet/CreateCookieCmd?langId=-1&storeId={storeId}&goToPage=NoURL&catalogId={catalogId}" ,
				  "Resource=0" ,
				  "Mode=HTML" ,
				  "LAST" ) ;

		lr_end_transaction("T07_Enter Promo_S03_CreateCookieCmd", 2);

		createCookieCmd();
		
		lr_start_transaction ( "T07_Enter Promo_S04_TCPMiniShopCartDisplayView1" );

		web_submit_data ( lr_eval_string("{promotype}_TCPMiniShopCartDisplayView1" ),
				  "Action=https://{host}/shop/TCPMiniShopCartDisplayView1?catalogId={catalogId}&langId=-1&storeId={storeId}" ,  
				  "Method=POST" ,
				  "Mode=HTML" ,
				  "ITEMDATA" ,
					"Name=objectId" , "Value=" , "ENDITEM" ,
					"Name=showredEye" , "Value=true" , "ENDITEM" ,
					"Name=requesttype" , "Value=ajax" , "ENDITEM" ,
				  "LAST" ) ;

		lr_end_transaction("T07_Enter Promo_S04_TCPMiniShopCartDisplayView1", 2);

		lr_start_transaction ( "T07_Enter Promo_S05_ShopCartDisplayView" );

		web_submit_data ( lr_eval_string("{promotype}_ShopCartDisplayView") , 
				  "Action=https://{host}/shop/ShopCartDisplayView?shipmentType=single&catalogId={catalogId}&langId=-1&storeId={storeId}" ,  
				  "Method=POST" ,
				  "Mode=HTML" ,
				  "ITEMDATA" ,
					"Name=objectId" , "Value=" , "ENDITEM" ,
					"Name=requesttype" , "Value=ajax" , "ENDITEM" ,
				  "LAST" ) ;

		lr_end_transaction("T07_Enter Promo_S05_ShopCartDisplayView", 2);
			
		if (isLoggedIn == 1) 
			TCPGetWishListForUsersView();
		
		lr_end_transaction ( lr_eval_string ( "T07_Enter Promo {promotype}" ) , 2 ) ;
 
	}
	
 
 
 

}  


void deletePromoCode()
{
     
 

    lr_think_time ( FORM_TT ) ;

    lr_start_transaction ( lr_eval_string ( "T07_Delete Promocode" ) ) ;

    lr_start_transaction ( "T07_Delete Promo_S01_AjaxPromotionCodeManage" );

    web_submit_data ( "delete_promocode_AjaxPromotionCodeManage" ,
			  "Action=https://{host}/webapp/wcs/stores/servlet/AjaxPromotionCodeManage" ,
			  "Method=POST" ,
			  "Mode=HTML" ,
			  "ITEMDATA" ,
				 "Name=orderId" , "Value={orderId}" , "ENDITEM" ,
				"Name=taskType" , "Value=R" , "ENDITEM" ,
				"Name=URL" , "Value=" , "ENDITEM" ,
				"Name=storeId" , "Value={storeId}" , "ENDITEM" ,
				"Name=langId" , "Value=-1" , "ENDITEM" ,
				"Name=catalogId" , "Value={catalogId}" , "ENDITEM" ,
				"Name=finalView" , "Value=AjaxOrderItemDisplayView" , "ENDITEM" ,
				"Name=fromPage" , "Value=shoppingCartDisplay" , "ENDITEM" ,
				"Name=promoCode" , "Value={promocode}" , "ENDITEM" , 
				"Name=requesttype" , "Value=ajax" , "ENDITEM" ,
			  "LAST" ) ;

    lr_end_transaction("T07_Delete Promo_S01_AjaxPromotionCodeManage", 2);

    lr_start_transaction ( "T07_Delete Promo_S02_AjaxOrderChangeServiceItemUpdate" );

    web_submit_data ( "delete_promocode_AjaxOrderChangeServiceItemUpdate" ,
			  "Action=https://{host}/webapp/wcs/stores/servlet/AjaxOrderChangeServiceItemUpdate" ,
			  "Method=POST" ,
			  "Mode=HTML" ,
			  "ITEMDATA" ,
				"Name=orderId" , "Value=." , "ENDITEM" , 
				"Name=calculationUsage" , "Value=-1,-2,-5,-6,-7" , "ENDITEM" ,
				"Name=requesttype" , "Value=ajax" , "ENDITEM" ,
			  "LAST" ) ;

    lr_end_transaction("T07_Delete Promo_S02_AjaxOrderChangeServiceItemUpdate", 2)    ;

    lr_start_transaction ( "T07_Delete Promo_S03_CreateCookieCmd" );

    web_url ( "delete_promcode_CreateCookieCmd" ,
              "URL=https://{host}/webapp/wcs/stores/servlet/CreateCookieCmd?langId=-1&storeId={storeId}&goToPage=NoURL&catalogId={catalogId}" ,
              "Resource=0" ,
              "Mode=HTML" ,
              "LAST" ) ;

    lr_end_transaction("T07_Delete Promo_S03_CreateCookieCmd", 2);

	createCookieCmd();

    lr_start_transaction ( "T07_Delete Promo_S04_TCPMiniShopCartDisplayView1" );

    web_submit_data ( "delete_promocode_TCPMiniShopCartDisplayView1" ,
			  "Action=https://{host}/shop/TCPMiniShopCartDisplayView1?catalogId={catalogId}&langId=-1&storeId={storeId}" ,  
			  "Method=POST" ,
			  "Mode=HTML" ,
			  "ITEMDATA" ,
				"Name=objectId" , "Value=" , "ENDITEM" ,
				"Name=showredEye" , "Value=true" , "ENDITEM" ,
				"Name=requesttype" , "Value=ajax" , "ENDITEM" ,
			  "LAST" ) ;

    lr_end_transaction("T07_Delete Promo_S04_TCPMiniShopCartDisplayView1", 2);

    lr_start_transaction ( "T07_Delete Promo_S05_ShopCartDisplayView" );

    web_submit_data ( "promocode_ShopCartDisplayView" ,
			  "Action=https://{host}/shop/ShopCartDisplayView?shipmentType=single&catalogId={catalogId}&langId=-1&storeId={storeId}" ,
			  "Method=POST" ,
			  "Mode=HTML" ,
			  "ITEMDATA" ,
				"Name=objectId" , "Value=" , "ENDITEM" ,
				"Name=requesttype" , "Value=ajax" , "ENDITEM" ,
			  "LAST" ) ;

    lr_end_transaction("T07_Delete Promo_S05_ShopCartDisplayView", 2);

    lr_end_transaction ( lr_eval_string ( "T07_Delete Promocode" ) , 0 ) ;
	
  
}  

void deleteItem()
{
	 
 
	if ( lr_paramarr_len ( "orderItemIds" ) > 1 ){
 
 
		lr_save_string ( lr_paramarr_random( "orderItemIds" ) , "orderItemId" ) ;
		
		lr_think_time ( LINK_TT ) ;
		lr_start_transaction ( "T08_Remove Item" ) ;

			lr_start_sub_transaction("T08_Remove Item_S01_AjaxOrderChangeServiceItemDelete", "T08_Remove Item");

			web_submit_data ( "AjaxOrderChangeServiceItemDelete" ,
				  "Action=https://{host}/webapp/wcs/stores/servlet/AjaxOrderChangeServiceItemDelete" ,
				  "Method=POST" ,
				  "Mode=HTML" ,
				  "ITEMDATA" ,
					"Name=orderId" , "Value={orderId}" , "ENDITEM" ,
					"Name=orderItemId" , "Value={orderItemId}" , "ENDITEM" ,
					"Name=storeId" , "Value={storeId}" , "ENDITEM" ,
					"Name=langId" , "Value=-1" , "ENDITEM" ,
					"Name=catalogId" , "Value={catalogId}" , "ENDITEM" ,
					"Name=calculationUsage" , "Value=-1,-2,-5,-6,-7" , "ENDITEM" ,
					"Name=requesttype" , "Value=ajax" , "ENDITEM" ,
				 "LAST" ) ;

			lr_end_sub_transaction("T08_Remove Item_S01_AjaxOrderChangeServiceItemDelete", 2);

			lr_start_sub_transaction("T08_Remove Item_S02_TCPMiniShopCartDisplayView1", "T08_Remove Item");

			web_submit_data ( "removeItem_TCPMiniShopCartDisplayView1" ,
				  "Action=https://{host}/webapp/wcs/stores/servlet/TCPMiniShopCartDisplayView1?catalogId={catalogId}&langId=-1&storeId={storeId}" ,
				  "Method=POST" ,
				  "Mode=HTML" ,
				  "ITEMDATA" ,
					"Name=objectId" , "Value=" , "ENDITEM" ,
					"Name=showredEye" , "Value=true" , "ENDITEM" ,
					"Name=requesttype" , "Value=ajax" , "ENDITEM" ,
				  "LAST" ) ;

			lr_end_sub_transaction("T08_Remove Item_S02_TCPMiniShopCartDisplayView1", 2);

			lr_start_sub_transaction("T08_Remove Item_S03_ShopCartDisplayView", "T08_Remove Item");

			web_submit_data ( "removeItem_ShopCartDisplayView" ,
				  "Action=https://{host}/webapp/wcs/stores/servlet/ShopCartDisplayView?shipmentType=single&catalogId={catalogId}&langId=-1&storeId={storeId}" ,
				  "Method=POST" ,
				  "Mode=HTML" ,
				  "ITEMDATA" ,
					"Name=objectId" , "Value=" , "ENDITEM" ,
					"Name=requesttype" , "Value=ajax" , "ENDITEM" ,
				  "LAST" ) ;

			lr_end_sub_transaction("T08_Remove Item_S03_ShopCartDisplayView", 2);
			
			 
			lr_start_sub_transaction ("T08_Remove Item_S04_CreateCookieCmd", "T08_Remove Item" ) ;

			web_custom_request("UpdateQuantity_CreateCookieCmd",
				"URL=https://{host}/webapp/wcs/stores/servlet/CreateCookieCmd?langId=-1&storeId={storeId}&goToPage=NoURL&catalogId={catalogId}",
				"Method=GET",
				"Resource=0",
				"RecContentType=text/html",
				"Snapshot=t28.inf",
				"Mode=HTML",
				"EncType=application/x-www-form-urlencoded",
				"LAST");

			lr_end_sub_transaction ("T08_Remove Item_S04_CreateCookieCmd", 2) ;

			createCookieCmd();
			
			TCPGetWishListForUsersView();
		
		lr_end_sub_transaction("T08_Remove Item", 2);
	}  
}  


void proceedToCheckout()
{
	lr_think_time ( LINK_TT ) ;
	web_reg_save_param ( "addressIds" , "LB=<option id=\"" , "RB=\" value=" , "Ord=ALL" , "NotFound=Warning", "LAST" ) ;
	web_reg_save_param ( "TCPMyAddressBook" , "LB=edit shipping address\" id=\"TCPMyAddressBook_" , "RB=\"" , "Ord=ALL", "NotFound=Warning", "LAST" ) ;
	web_reg_save_param ( "emailAddress1" , "LB=<input type=\"hidden\" name=\"email1\" value=\"" , "RB=\" id=\"email1\" />" , "NotFound=Warning", "LAST" ) ;
	web_reg_save_param ( "authToken" , "LB=<input type=\"hidden\" name=\"authToken\" value=\"" , "RB=\"", "NotFound=Warning", "LAST" ) ;

	lr_start_transaction ( "T09_Proceed to Checkout_LogonForm" ) ;
	
		web_url ( "T09_Proceed to Checkout_LogonForm" ,
 
			  "URL=https://{host}/shop/LogonForm?catalogId={catalogId}&myAcctMain=1&langId=-1&storeId={storeId}&URL=https://{host}/webapp/wcs/stores/servlet/TCPOrderShippingView?shipmentType=single&catalogId={catalogId}&langId=-1&storeId={storeId}" ,
			  "Resource=0" ,
			  "Mode=HTML" ,
			  "LAST" ) ;

	lr_end_transaction ( "T09_Proceed to Checkout_LogonForm" , 0) ;

}  

void proceedToCheckout_ShippingView()
{
	lr_think_time ( LINK_TT ) ;

	web_reg_save_param ( "addressIds" , "LB=<option id=\"" , "RB=\" value=" , "Ord=ALL" , "NotFound=Warning", "LAST" ) ;
	web_reg_save_param ( "TCPMyAddressBook" , "LB=edit shipping address\" id=\"TCPMyAddressBook_" , "RB=\"" , "Ord=ALL", "NotFound=Warning", "LAST" ) ;
	web_reg_save_param ( "emailAddress1" , "LB=<input type=\"hidden\" name=\"email1\" value=\"" , "RB=\" id=\"email1\" />" , "NotFound=Warning", "LAST" ) ;
	web_reg_save_param ( "authToken" , "LB=<input type=\"hidden\" name=\"authToken\" value=\"" , "RB=\"", "NotFound=Warning", "LAST" ) ;

	lr_start_transaction ( "T09_Proceed to Checkout_ShippingView" ) ;
	web_url ( "T09_Proceed to Checkout_ShippingView" ,
			  "URL=https://{host}/shop/TCPOrderShippingView?shipmentType=single&catalogId={catalogId}&langId=-1&storeId={storeId}" ,
			  "Resource=0" ,
			  "Mode=HTML" ,
			  "LAST" ) ;
	lr_end_transaction ( "T09_Proceed to Checkout_ShippingView" , 0) ;

}  


void proceedAsGuest()
{
	lr_think_time ( LINK_TT ) ;

	web_reg_save_param ( "emailAddress1" , "LB=<input type=\"hidden\" name=\"email1\" value=\"" , "RB=\" id=\"email1\" />" ,"NotFound=Warning", "LAST" ) ;
	web_reg_save_param ( "authToken" , "LB=<input type=\"hidden\" name=\"authToken\" value=\"" , "RB=\"" , "NotFound=Warning", "LAST" ) ;

	lr_start_transaction ( "T10_Proceed As Guest" ) ;

	web_url("T10_Proceed As Guest_TCPOrderShippingView",
			"URL=https://{host}/webapp/wcs/stores/servlet/TCPOrderShippingView?shipmentType=single&catalogId={catalogId}&langId=-1&storeId={storeId}",
			"Resource=0",
			"RecContentType=text/html",
			"Mode=HTML",
			"LAST");

	lr_end_transaction ( "T10_Proceed As Guest" , 0) ;
}  



void forgetPassword()
{	
	lr_think_time ( FORM_TT ) ;

	web_url("LogonForm",
			"URL=https://{host}/shop/LogonForm?catalogId={catalogId}&myAcctMain=1&langId=-1&storeId={storeId}",
			"Mode=HTTP",
			"LAST");	

	lr_start_transaction ( "T29_ForgotPassword" ) ;

	web_url("Forgot password?",
		"URL=http://{host}/shop/ResetPasswordGuestErrorView?state=forgetpassword&catalogId={catalogId}&langId=-1&storeId={storeId}",
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=", 
		"Snapshot=t3.inf", 
		"Mode=HTML", 
		"LAST");	
			
	lr_end_transaction ( "T29_ForgotPassword" , 0) ;

	lr_start_transaction ( "T29_PasswordReset" ) ;

	web_submit_data("PersonChangeServicePasswordReset", 
		"Action=http://{host}//shop/PersonChangeServicePasswordReset?storeId={storeId}", 
		"Method=POST", 
		"TargetFrame=", 
		"RecContentType=text/html", 
		"Referer=http://{host}//shop/ResetPasswordGuestErrorView?state=forgetpassword&catalogId={catalogId}&langId=-1&storeId={storeId}", 
		"Mode=HTML", 
		"EncodeAtSign=YES", 
		"ITEMDATA", 
		"Name=storeId", "Value={storeId}", "ENDITEM", 
		"Name=catalogId", "Value={catalogId}", "ENDITEM", 
		"Name=langId", "Value=-1", "ENDITEM", 
		"Name=state", "Value=passwdconfirm", "ENDITEM", 
		"Name=URL", "Value=ResetPasswordForm", "ENDITEM", 
		"Name=formFlag", "Value=true", "ENDITEM", 
		"Name=errorViewName", "Value=ResetPasswordGuestErrorView", "ENDITEM", 
		"Name=checkEmailAddress", "Value=-", "ENDITEM", 
		"Name=logonId", "Value=TCPPERF_US2_1947594112@CHILDRENSPLACE.COM", "ENDITEM", 
		"Name=logonIdInput", "Value=TCPPERF_US2_1947594112@CHILDRENSPLACE.COM", "ENDITEM", 
		"LAST");

	lr_end_transaction ( "T29_PasswordReset" , 0) ;

}

void login()
{
	
	lr_think_time ( FORM_TT ) ;
	web_reg_save_param ( "addressIds" , "LB=<option id=\"" , "RB=\" value=" , "Ord=ALL" , "NotFound=Warning", "LAST" ) ;
	web_reg_save_param ( "TCPMyAddressBook" , "LB=edit shipping address\" id=\"TCPMyAddressBook_" , "RB=\"" , "Ord=ALL", "NotFound=Warning", "LAST" ) ;
	web_reg_save_param ( "emailAddress1" , "LB=<input type=\"hidden\" name=\"email1\" value=\"" , "RB=\" id=\"email1\" />" , "NotFound=Warning", "LAST" ) ;
	web_reg_save_param ( "authToken" , "LB=<input type=\"hidden\" name=\"authToken\" value=\"" , "RB=\"", "NotFound=Warning", "LAST" ) ;
 
# 1021 "..\\..\\checkoutFunctions.c"
	web_reg_find ( "Text=entered is incorrect" , "SaveCount=loginerror_count" , "LAST" ) ;

	lr_start_transaction ( "T10_Logon_ShippingView" ) ;

 

		web_submit_data ( "Logon" ,
			  "Action=https://{host}/shop/Logon" ,
			  "Method=POST" ,
			  "Mode=HTML" ,
			  "ITEMDATA" ,
			  "Name=emc" , "Value=" , "ENDITEM" ,
			  "Name=emcUserId" , "Value=" , "ENDITEM" ,
			  "Name=storeId" , "Value={storeId}" , "ENDITEM" ,
			  "Name=catalogId" , "Value={catalogId}" , "ENDITEM" ,
			  "Name=reLogonURL" , "Value=LogonForm" , "ENDITEM" ,
			  "Name=myAcctMain" , "Value=1" , "ENDITEM" ,
			  "Name=fromOrderId" , "Value=*" , "ENDITEM" ,
			  "Name=toOrderId" , "Value=." , "ENDITEM" ,
			  "Name=deleteIfEmpty" , "Value=*" , "ENDITEM" ,
			  "Name=continue" , "Value=1" , "ENDITEM" ,
			  "Name=createIfEmpty" , "Value=1" , "ENDITEM" ,
			  "Name=calculationUsageId" , "Value=-1" , "ENDITEM" ,
			  "Name=updatePrices" , "Value=0" , "ENDITEM" ,
			  "Name=errorViewName" , "Value=LogonForm" , "ENDITEM" ,
			  "Name=previousPage" , "Value=logon" , "ENDITEM" ,
			  "Name=returnPage" , "Value=" , "ENDITEM" ,
			  "Name=visitorId_1", "Value=", "ENDITEM",
 
 
			  "Name=URL", "Value=https://{host}/webapp/wcs/stores/servlet/TCPOrderShippingView?shipmentType=single&catalogId={catalogId}&langId=-1&storeId={storeId}" , "ENDITEM" ,  
 
			  "Name=logonId1" , "Value={userEmail}" , "ENDITEM" ,
 
 
			  "Name=logonPassword1" , "Value=asdf1234" , "ENDITEM" ,
			  "Name=rememberCheck", "Value=true", "ENDITEM", 
			  "Name=rememberMe", "Value=true", "ENDITEM",   
			  "LAST" ) ;

 
		
 		if ( atoi ( lr_eval_string ( "{TCPMyAddressBook_count}" ) ) > 0 ) {
			lr_save_string(lr_eval_string ( "{TCPMyAddressBook_1}" ),"addressIds_2");
			lr_save_string(lr_eval_string ( "{TCPMyAddressBook_1}" ),"addressId");
		}

		if ( atoi ( lr_eval_string ( "{loginerror_count}" ) ) == 1 ) {
			lr_fail_trans_with_error( "registered login failed for %d",lr_eval_string ("{logonid}") ) ;
 
			lr_end_transaction ( "T10_Logon_ShippingView" , 1) ;
			isLoggedIn = 0;
			return;
		}
	isLoggedIn = 1;

	lr_end_transaction ( "T10_Logon_ShippingView" , 0) ;
 
# 1091 "..\\..\\checkoutFunctions.c"
}  

void loginFromHomePage()
{

	web_url("LogonForm",
		"URL=https://{host}/shop/LogonForm?catalogId={catalogId}&myAcctMain=1&langId=-1&storeId={storeId}",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Snapshot=t58.inf",
		"Mode=HTML",
		"LAST");

	lr_think_time ( FORM_TT ) ;

	web_reg_save_param ( "addressIds" , "LB=<option id=\"" , "RB=\" value=" , "Ord=ALL" , "NotFound=Warning", "LAST" ) ;
	web_reg_save_param ( "TCPMyAddressBook" , "LB=edit shipping address\" id=\"TCPMyAddressBook_" , "RB=\"" , "Ord=ALL", "NotFound=Warning", "LAST" ) ;
	web_reg_save_param ( "emailAddress1" , "LB=<input type=\"hidden\" name=\"email1\" value=\"" , "RB=\" id=\"email1\" />" , "NotFound=Warning", "LAST" ) ;
	web_reg_save_param ( "authToken" , "LB=<input type=\"hidden\" name=\"authToken\" value=\"" , "RB=\"" , "NotFound=Warning", "LAST" ) ;
	web_reg_save_param ( "userId" , "LB=Set-Cookie: WC_AUTHENTICATION_" , "RB==" , "ORD=2", "NotFound=Warning", "LAST" ) ;
	web_reg_save_param ( "orderId" , "LB=orderId=" , "RB=&URL=LogonForm" , "NotFound=Warning", "LAST" ) ;
 
	 
	web_reg_find ( "Text=Children's Place Employee?" , "SaveCount=loginerror_count" , "LAST" ) ;

	lr_start_transaction ( "T10_Logon_AjaxLogonForm" ) ;

 

			web_submit_data("LogonFromHomePage",
			"Action=https://{host}/shop/Logon",
			"Method=POST",
			"TargetFrame=",
			"RecContentType=text/html",
			"Snapshot=t59.inf",
			"Mode=HTML",
			"EncodeAtSign=YES",
			"ITEMDATA",
			"Name=emc", "Value=", "ENDITEM",
			"Name=emcUserId", "Value=", "ENDITEM",
			"Name=storeId", "Value={storeId}", "ENDITEM",
			"Name=catalogId", "Value={catalogId}", "ENDITEM",
			"Name=reLogonURL", "Value=LogonForm", "ENDITEM",
			"Name=myAcctMain", "Value=1", "ENDITEM",
			"Name=fromOrderId", "Value=*", "ENDITEM",
			"Name=toOrderId", "Value=.", "ENDITEM",
			"Name=deleteIfEmpty", "Value=*", "ENDITEM",
			"Name=continue", "Value=1", "ENDITEM",
			"Name=createIfEmpty", "Value=1", "ENDITEM",
			"Name=calculationUsageId", "Value=-1", "ENDITEM",
			"Name=updatePrices", "Value=0", "ENDITEM",
			"Name=errorViewName", "Value=LogonForm", "ENDITEM",
			"Name=previousPage", "Value=logon", "ENDITEM",
			"Name=returnPage", "Value=", "ENDITEM",
			"Name=visitorId_1", "Value=", "ENDITEM",
			"Name=URL", "Value=AjaxLogonForm", "ENDITEM",  
 
 
 
 
 
			"Name=logonId1" , "Value={userEmail}" , "ENDITEM" ,
			"Name=logonPassword1", "Value=asdf1234", "ENDITEM",
			"Name=rememberCheck", "Value=true", "ENDITEM", 
			"Name=rememberMe", "Value=true", "ENDITEM",   
			"LAST");

 


		if ( atoi ( lr_eval_string ( "{loginerror_count}" ) ) == 0 ) {
			lr_fail_trans_with_error( "registered login failed for %d",lr_eval_string ("{userEmail}") ) ;
			lr_end_transaction ( "T10_Logon_AjaxLogonForm" , 1) ;
			isLoggedIn = 0;
			return;
		}

		isLoggedIn = 1;

	lr_end_transaction ( "T10_Logon_AjaxLogonForm" , 0) ;
}


void loginForOrderHistory()
{
	web_url("LogonForm",
		"URL=https://{host}/shop/LogonForm?catalogId={catalogId}&myAcctMain=1&langId=-1&storeId={storeId}",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Snapshot=t58.inf",
		"Mode=HTML",
		"LAST");

	lr_think_time ( FORM_TT ) ;
 
 
 
	web_reg_find ( "Text=entered is incorrect" , "SaveCount=loginerror_count" , "LAST" ) ;

	lr_start_transaction ( "T10_Logon_OrderHistory" ) ;

 

			web_submit_data("LogonForOrderHistory",
			"Action=https://{host}/shop/Logon",
			"Method=POST",
			"TargetFrame=",
			"RecContentType=text/html",
			"Snapshot=t59.inf",
			"Mode=HTML",
			"EncodeAtSign=YES",
			"ITEMDATA",
			"Name=emc", "Value=", "ENDITEM",
			"Name=emcUserId", "Value=", "ENDITEM",
			"Name=storeId", "Value={storeId}", "ENDITEM",
			"Name=catalogId", "Value={catalogId}", "ENDITEM",
			"Name=reLogonURL", "Value=LogonForm", "ENDITEM",
			"Name=myAcctMain", "Value=1", "ENDITEM",
			"Name=fromOrderId", "Value=*", "ENDITEM",
			"Name=toOrderId", "Value=.", "ENDITEM",
			"Name=deleteIfEmpty", "Value=*", "ENDITEM",
			"Name=continue", "Value=1", "ENDITEM",
			"Name=createIfEmpty", "Value=1", "ENDITEM",
			"Name=calculationUsageId", "Value=-1", "ENDITEM",
			"Name=updatePrices", "Value=0", "ENDITEM",
			"Name=errorViewName", "Value=LogonForm", "ENDITEM",
			"Name=previousPage", "Value=logon", "ENDITEM",
			"Name=returnPage", "Value=", "ENDITEM",
			"Name=visitorId_1", "Value=", "ENDITEM",
			"Name=URL", "Value=AjaxLogonForm", "ENDITEM",  
 
			"Name=logonId1" , "Value={logonidForOrderHistory}" , "ENDITEM" ,
 
 
			"Name=logonPassword1", "Value=asdf1234", "ENDITEM",
			"Name=rememberCheck", "Value=true", "ENDITEM", 
			"Name=rememberMe", "Value=true", "ENDITEM",   
			"LAST");
			

 
 		
# 1245 "..\\..\\checkoutFunctions.c"
		if ( atoi ( lr_eval_string ( "{loginerror_count}" ) ) == 1 ) {
			 
	 
			lr_end_transaction ( "T10_Logon_OrderHistory" , 1) ;
			return;
		}

	lr_end_transaction ( "T10_Logon_OrderHistory" , 0) ;

}  


void convertAndApplyPoints()
{
 
 
	lr_think_time ( FORM_TT ) ;

	web_reg_save_param ( "myPlaceId" , "LB=<input type=\"hidden\" value=\"" , "RB=\" name=\"myPlaceId\" />", "Notfound=warning", "LAST" ) ;
	web_reg_save_param ( "myAccountId" , "LB=<input type=\"hidden\" value=\"" , "RB=\" name=\"accountId\" />", "Notfound=warning", "LAST" ) ;

	web_reg_save_param ( "addressIds" , "LB=<option id=\"" , "RB=\" value=" , "Ord=ALL" , "NotFound=Warning", "LAST" ) ;
	web_reg_save_param ( "TCPMyAddressBook" , "LB=edit shipping address\" id=\"TCPMyAddressBook_" , "RB=\"" , "Ord=ALL", "NotFound=Warning", "LAST" ) ;
	web_reg_save_param ( "emailAddress1" , "LB=<input type=\"hidden\" name=\"email1\" value=\"" , "RB=\" id=\"email1\" />" , "NotFound=Warning", "LAST" ) ;
	web_reg_save_param ( "authToken" , "LB=<input type=\"hidden\" name=\"authToken\" value=\"" , "RB=\"", "NotFound=Warning", "LAST" ) ;

	lr_start_transaction ( "T11_Convert Points To Coupon" ) ;

		lr_start_sub_transaction( "T11_Convert Points_S01_Click MyPlace Rewards", "T11_Convert Points To Coupon" );

		web_url("AjaxLogonForm",
			"URL=https://{host}/shop/AjaxLogonForm?catalogId={catalogId}&myAcctMain=1&langId=-1&storeId={storeId}",
			"Mode=HTTP",
			"LAST");

		lr_end_sub_transaction( "T11_Convert Points_S01_Click MyPlace Rewards", 2 );

		web_reg_save_param ( "promocodeRewards" , "LB=\"javascript:applyLoyaltyCode('" , "RB=');", "NotFound=Warning", "LAST" ) ;

		lr_start_sub_transaction("T11_Convert Points_S02_TCPRedeemLoyaltyPoints", "T11_Convert Points To Coupon" );

		web_submit_data("TCPRedeemLoyaltyPoints",
				"Action=https://{host}/shop/TCPRedeemLoyaltyPoints",     
				"Method=POST",
				"Mode=HTTP",
				"ITEMDATA",
				"Name=storeId", "Value={storeId}", "ENDITEM",
				"Name=langId", "Value=-1", "ENDITEM",
				"Name=catalogId", "Value={catalogId}", "ENDITEM",
				"Name=myPlaceId", "Value={myPlaceId}", "ENDITEM",
				"Name=accountId", "Value={myAccountId}", "ENDITEM",
				"Name=visitorId", "Value=[CS]v1|2B2AE4E305078968-600001048004DD05[CE]", "ENDITEM", 
				"Name=amountToRedeem", "Value=5", "ENDITEM",
				"Name=cpnWalletValue", "Value=0", "ENDITEM",  
				"LAST");

		lr_end_transaction("T11_Convert Points_S02_TCPRedeemLoyaltyPoints", 2);

	lr_end_transaction ( "T11_Convert Points To Coupon" , 0) ;

 
	if ( strlen(lr_eval_string("{promocodeRewards}")) == 16 ) {
	
		viewCart();
		
	    applyPromoCode(1);
	}
	
 
 
 
 
 
}  

void submitShippingAddress()
{
	lr_think_time ( FORM_TT ) ;

	lr_start_transaction ( "T13_Submit Shipping Address" );

		lr_start_sub_transaction ( "T13_Submit_SA_S03_AjaxOrderProcessServiceOrderPrepare", "T13_Submit Shipping Address" ) ;

		web_submit_data ( "T13_Submit Shipping Address AjaxOrderProcessServiceOrderPrepare" ,
						  "Action=https://{host}/shop/AjaxOrderProcessServiceOrderPrepare" ,
						  "Method=POST" ,
						  "Mode=HTML" ,
						  "ITEMDATA" ,
							"Name=storeId" , "Value={storeId}" , "ENDITEM" ,
							"Name=langId" , "Value=-1" , "ENDITEM" ,
							"Name=catalogId" , "Value={catalogId}" , "ENDITEM" ,
							"Name=orderId" , "Value=." , "ENDITEM" ,
							"Name=requesttype" , "Value=ajax" , "ENDITEM" ,
						  "LAST" ) ;

		lr_end_sub_transaction ( "T13_Submit_SA_S03_AjaxOrderProcessServiceOrderPrepare" , 2 ) ;

		web_reg_save_param ( "piAmount" , "LB=name=\"OrderTotalAmount\" value=\"" , "RB=\"" , "NotFound=Warning", "LAST" ) ;

		lr_start_sub_transaction ( "T13_Submit_SA_S04_TCPOrderBillingCmd", "T13_Submit Shipping Address" );

		web_url ( "T13_Submit Shipping Address TCPOrderBillingCmd" ,
				  "URL=https://{host}/shop/TCPOrderBillingCmd?URL=OrderBillingView&langId=-1&storeId={storeId}&catalogId={catalogId}&forceShipmentType=1" ,
				  "Resource=0" ,
				  "Mode=HTML" ,
				  "LAST" ) ;

		lr_end_sub_transaction ( "T13_Submit_SA_S04_TCPOrderBillingCmd" , 2 ) ;

	lr_end_transaction ( "T13_Submit Shipping Address" , 0) ;
	
} 

void submitShippingAddressAsGuest()
{
	lr_think_time ( FORM_TT ) ;

	lr_param_sprintf ( "ts" , "%ld%d" , _time32(&t) , rand() % 1000 ) ;

	lr_start_transaction ( "T13_Submit Shipping Address As Guest" ) ;

		lr_start_sub_transaction ( "T13_Submit Shipping Address TCPAVSResponseView", "T13_Submit Shipping Address As Guest") ;

				web_url ( "TCPAVSResponseView" ,
				  	"URL=https://{host}/webapp/wcs/stores/servlet/TCPAVSResponseView?callback=TCPAVSHandler&storeId={storeId}&catalogId={catalogId}&langId=-1&company=&firstName=teddy&lastName=bear&address1={guestAdr1}&address2=&city={guestCity}&state={guestState}&country={guestCountry}&zip={guestZip}&suite=&cvsLocalEndPoint=http%3A%2F%2F10.2.0.222%3A9082%2Fadv%2Fservices%2FAddressVerification&action=addressSearch&formpart=TCPsaveAddressForm-AddShippingAddress&_={ts}"
				  	"Resource=0" ,
				  	"Mode=HTML" ,
				  	"LAST" ) ;

		lr_end_transaction ( "T13_Submit Shipping Address TCPAVSResponseView" , 0 ) ;

		web_reg_save_param ( "addressId" , "LB=\"addressId\": [\"" , "RB=\"]" , "NotFound=Warning", "LAST" ) ;

	 	lr_start_sub_transaction ( "T13_Submit_SA_S01_AjaxPersonChangeServiceAddressAdd", "T13_Submit Shipping Address As Guest" ) ;

		web_submit_data ( "T12_Submit Shipping Address AjaxPersonChangeServiceAddressAdd" ,
				  "Action=https://{host}/shop/AjaxPersonChangeServiceAddressAdd" ,
				  "Method=POST" ,
				  "Mode=HTML" ,
				  "ITEMDATA" ,
				  "Name=storeId" , "Value={storeId}" , "ENDITEM" ,
				  "Name=langId" , "Value=-1" , "ENDITEM" ,
				  "Name=catalogId" , "Value={catalogId}" , "ENDITEM" ,
				  "Name=authToken" , "Value={authToken}" , "ENDITEM" ,
				  "Name=email1" , "Value={emailAddress1}" , "ENDITEM" ,
				  "Name=addressType" , "Value=SB" , "ENDITEM" ,
				  "Name=originalAddressType" , "Value=SB" , "ENDITEM" ,
				  "Name=nickName" , "Value=SB_{ts}" , "ENDITEM" ,
				  "Name=avsAddressId" , "Value={ts}" , "ENDITEM" ,
				  "Name=firstName" , "Value=Teddy" , "ENDITEM" ,
				  "Name=ofirstName" , "Value=" , "ENDITEM" ,
				  "Name=lastName" , "Value=Bear" , "ENDITEM" ,
				  "Name=olastName" , "Value=" , "ENDITEM" ,
				  "Name=address1" , "Value={guestAdr1}" , "ENDITEM" ,
				  "Name=oaddress1" , "Value=" , "ENDITEM" ,
				  "Name=address2" , "Value=" , "ENDITEM" ,
				  "Name=oaddress2" , "Value=" , "ENDITEM" ,
				  "Name=city" , "Value={guestCity}" , "ENDITEM" ,
				  "Name=ocity" , "Value=" , "ENDITEM" ,
				  "Name=state" , "Value={guestState}" , "ENDITEM" ,
				  "Name=ostate" , "Value=" , "ENDITEM" ,
				  "Name=zipCode" , "Value={guestZip}" , "ENDITEM" ,
				  "Name=ozipCode" , "Value=" , "ENDITEM" ,
				  "Name=addressField3" , "Value={guestZip}" , "ENDITEM" ,
				  "Name=oaddressField3" , "Value=" , "ENDITEM" ,
				  "Name=country" , "Value={guestCountry}" , "ENDITEM" ,
				  "Name=ocountry" , "Value={guestCountry}" , "ENDITEM" ,
				  "Name=phone1" , "Value=4163218351" , "ENDITEM" ,
				  "Name=ophone1" , "Value=" , "ENDITEM" ,
				  "Name=address3" , "Value=" , "ENDITEM" ,
				  "Name=isZipMandatory" , "Value=true" , "ENDITEM" ,
				  "Name=requesttype" , "Value=ajax" , "ENDITEM" ,
				  "LAST" ) ;

	 	lr_end_sub_transaction ( "T13_Submit_SA_S01_AjaxPersonChangeServiceAddressAdd" , 0 ) ;

		web_reg_find ( "Text=errorCode" , "SaveCount=error_count" , "LAST" ) ;

		lr_start_sub_transaction ( "T13_Submit_SA_S02_AjaxOrderChangeServiceShipInfoUpdate", "T13_Submit Shipping Address As Guest" ) ;

		web_submit_data ( "T13_Submit Shipping Address AjaxOrderChangeServiceShipInfoUpdate" ,
				  "Action=https://{host}/webapp/wcs/stores/servlet/AjaxOrderChangeServiceShipInfoUpdate" ,
				  "Method=POST" ,
				  "Mode=HTML" ,
				  "ITEMDATA" ,
					"Name=storeId" , "Value={storeId}" , "ENDITEM" ,
					"Name=langId" , "Value=-1" , "ENDITEM" ,
					"Name=catalogId" , "Value={catalogId}" , "ENDITEM" ,
					"Name=orderId" , "Value=." , "ENDITEM" ,
					"Name=calculationUsage" , "Value=-1,-2,-3,-4,-5,-6,-7" , "ENDITEM" ,
					"Name=allocate" , "Value=***" , "ENDITEM" ,
					"Name=backorder" , "Value=***" , "ENDITEM" ,
					"Name=remerge" , "Value=***" , "ENDITEM" ,
					"Name=check" , "Value=*n" , "ENDITEM" ,
					"Name=addressId" , "Value={addressId}" , "ENDITEM" ,
					"Name=requesttype" , "Value=ajax" , "ENDITEM" ,
				  "LAST" ) ;

		lr_end_sub_transaction ( "T13_Submit_SA_S02_AjaxOrderChangeServiceShipInfoUpdate" , 2 ) ;

		if ( atoi ( lr_eval_string ( "{error_count}" ) ) > 0 ) {
 
				lr_end_transaction ( "T13_Submit Shipping Address As Guest" , 1 ) ;
				lr_exit(2, 0);
		}

		lr_start_sub_transaction ( "T13_Submit_SA_S03_AjaxOrderProcessServiceOrderPrepare", "T13_Submit Shipping Address As Guest" ) ;

		web_submit_data ( "T13_Submit Shipping Address AjaxOrderProcessServiceOrderPrepare" ,
				  "Action=https://{host}/webapp/wcs/stores/servlet/AjaxOrderProcessServiceOrderPrepare" ,
				  "Method=POST" ,
				  "Mode=HTML" ,
				  "ITEMDATA" ,
					"Name=storeId" , "Value={storeId}" , "ENDITEM" ,
					"Name=langId" , "Value=-1" , "ENDITEM" ,
					"Name=catalogId" , "Value={catalogId}" , "ENDITEM" ,
					"Name=orderId" , "Value=." , "ENDITEM" ,
					"Name=requesttype" , "Value=ajax" , "ENDITEM" ,
				  "LAST" ) ;

		lr_end_sub_transaction ( "T13_Submit_SA_S03_AjaxOrderProcessServiceOrderPrepare" , 2 ) ;

		web_reg_save_param ( "piAmount" , "LB=name=\"OrderTotalAmount\" value=\"" , "RB=\"" , "NotFound=Warning", "LAST" ) ;

		lr_start_sub_transaction ( "T13_Submit_SA_S04_TCPOrderBillingCmd", "T13_Submit Shipping Address As Guest" );

		web_url ( "T13_Submit Shipping Address TCPOrderBillingCmd" ,
				  "URL=https://{host}/webapp/wcs/stores/servlet/TCPOrderBillingCmd?URL=OrderBillingView&langId=-1&storeId={storeId}&catalogId={catalogId}&forceShipmentType=1" ,
				  "Resource=0" ,
				  "Mode=HTML" ,
				  "LAST" ) ;

		lr_end_sub_transaction ( "T13_Submit_SA_S04_TCPOrderBillingCmd" , 2 ) ;

	lr_end_transaction ( "T13_Submit Shipping Address As Guest" , 0) ;

}  

int selectShippingAddress()
{
	lr_think_time ( FORM_TT ) ;
	index = 2;
	lr_save_string ( lr_paramarr_idx( "addressIds" , index ) , "addressId" ) ;
	if ( atoi ( lr_eval_string ( "{TCPMyAddressBook_count}" ) ) > 0 ) {
		lr_save_string(lr_eval_string ( "{TCPMyAddressBook_1}" ),"addressId");
	}

	lr_start_transaction ( "T13_Select a Shipping Address" ) ;

	web_reg_save_param ( "zipCode" , "LB=\"zipCode\":\"" , "RB=\"," , "NotFound=Warning", "LAST" ) ;

	lr_start_sub_transaction ( "T13_Select_SA_S01_TCPAjaxAddressDisplayView", "T13_Select a Shipping Address" ) ;

	web_submit_data ( "T13_Select a Shipping Address_TCPAjaxAddressDisplayView" ,
			  "Action=https://{host}/shop/TCPAjaxAddressDisplayView?catalogId={catalogId}&langId=-1&storeId={storeId}" ,
			  "Method=POST" ,
			  "Mode=HTML" ,
			  "ITEMDATA" ,
				"Name=addressId" , "Value={addressId}" , "ENDITEM" ,
				"Name=objectId" , "Value=" , "ENDITEM" ,
				"Name=requesttype" , "Value=ajax" , "ENDITEM" ,
			"LAST" ) ;

	lr_end_sub_transaction ( "T13_Select_SA_S01_TCPAjaxAddressDisplayView", 2 ) ;

	lr_start_sub_transaction ( "T13_Select_SA_S02_AjaxOrderChangeServiceShipInfoUpdate", "T13_Select a Shipping Address" ) ;

	web_submit_data ( "T13_Select a Shipping Address_AjaxOrderChangeServiceShipInfoUpdate" ,
			  "Action=https://{host}/shop/AjaxOrderChangeServiceShipInfoUpdate" ,
			  "Method=POST" ,
			  "Mode=HTML" ,
			  "ITEMDATA" ,
				"Name=storeId" , "Value={storeId}" , "ENDITEM" ,
				"Name=langId" , "Value=-1" , "ENDITEM" ,
				"Name=catalogId" , "Value={catalogId}" , "ENDITEM" ,
				"Name=orderId" , "Value=." , "ENDITEM" ,
				"Name=calculationUsage" , "Value=-1,-2,-3,-4,-5,-6,-7" , "ENDITEM" ,
				"Name=allocate" , "Value=***" , "ENDITEM" ,
				"Name=backorder" , "Value=***" , "ENDITEM" ,
				"Name=remerge" , "Value=***" , "ENDITEM" ,
				"Name=check" , "Value=*n" , "ENDITEM" ,
				"Name=addressId" , "Value={addressId}" , "ENDITEM" ,
				"Name=requesttype" , "Value=ajax" , "ENDITEM" ,
			  "LAST" ) ;

	lr_end_sub_transaction ( "T13_Select_SA_S02_AjaxOrderChangeServiceShipInfoUpdate", 2 ) ;

	lr_start_sub_transaction ( "T13_Select_SA_S03_TCPTraditionalAjaxShippingDetailsURL", "T13_Select a Shipping Address" ) ;	
	
	web_submit_data("TCPTraditionalAjaxShippingDetailsURL", 
		"Action=https://{host}/shop/TCPTraditionalAjaxShippingDetailsURL?catalogId={catalogId}&langId=-1&storeId={storeId}", 
		"Method=POST", 
		"TargetFrame=", 
		"RecContentType=text/html", 
		"Mode=HTML", 
		"ITEMDATA", 
		"Name=shipmentDetailsArea", "Value=update", "ENDITEM", 
		"Name=objectId", "Value=", "ENDITEM", 
		"Name=requesttype", "Value=ajax", "ENDITEM", 
		"LAST");
	
	lr_end_sub_transaction ( "T13_Select_SA_S03_TCPTraditionalAjaxShippingDetailsURL", 2 ) ;
	
	lr_start_sub_transaction ( "T13_Select_SA_S04_TCPCurrentOrderInformationView", "T13_Select a Shipping Address" ) ;

	web_submit_data ( "T13_Select a Shipping Address_TCPCurrentOrderInformationView" ,
			  "Action=https://{host}/shop/TCPCurrentOrderInformationView?catalogId={catalogId}&orderId=&langId=-1&storeId={storeId}" ,
			  "Method=POST" ,
			  "Mode=HTML" ,
			  "ITEMDATA" ,
			  "Name=objectId" , "Value=" , "ENDITEM" ,
			  "Name=requesttype" , "Value=ajax" , "ENDITEM" ,
			  "LAST" ) ;

	lr_end_sub_transaction ( "T13_Select_SA_S04_TCPCurrentOrderInformationView", 2 ) ;
	

	lr_end_transaction ( "T13_Select a Shipping Address" , 0 ) ;
	return 0;
}  

void selectShipMode()
{
	lr_think_time ( FORM_TT ) ;

	lr_start_transaction ( "T12_Select Ship Mode" ) ;

		lr_start_sub_transaction ( "T12_Select_SM_S01_AjaxOrderChangeServiceShipInfoUpdate", "T12_Select Ship Mode" ) ;

		web_submit_data ( "T12_Select Ship Mode_AjaxOrderChangeServiceShipInfoUpdate" ,
				  "Action=https://{host}/shop/AjaxOrderChangeServiceShipInfoUpdate" ,
				  "Method=POST" ,
				  "Mode=HTML" ,
				  "ITEMDATA" ,
					"Name=storeId" , "Value={storeId}" , "ENDITEM" ,
					"Name=langId" , "Value=-1" , "ENDITEM" ,
					"Name=catalogId" , "Value={catalogId}" , "ENDITEM" ,
					"Name=orderId" , "Value=." , "ENDITEM" ,
					"Name=calculationUsage" , "Value=-1,-2,-5,-6,-7" , "ENDITEM" ,
					"Name=allocate" , "Value=***" , "ENDITEM" ,
					"Name=backorder" , "Value=***" , "ENDITEM" ,
					"Name=remerge" , "Value=***" , "ENDITEM" ,
					"Name=check" , "Value=*n" , "ENDITEM" ,
					"Name=shipModeId" , "Value={shipmode_id}" , "ENDITEM" ,
					"Name=requesttype" , "Value=ajax" , "ENDITEM" ,
				  "LAST" ) ;

		lr_end_sub_transaction ( "T12_Select_SM_S01_AjaxOrderChangeServiceShipInfoUpdate", 2 ) ;

		lr_start_sub_transaction ( "T12_Select_SM_S02_TCPTraditionalAjaxShippingDetailsURL", "T12_Select Ship Mode" ) ;	
		
		web_submit_data("TCPTraditionalAjaxShippingDetailsURL", 
			"Action=https://{host}/shop/TCPTraditionalAjaxShippingDetailsURL?catalogId={catalogId}&langId=-1&storeId={storeId}", 
			"Method=POST", 
			"TargetFrame=", 
			"RecContentType=text/html", 
			"Mode=HTML", 
			"ITEMDATA", 
			"Name=shipmentDetailsArea", "Value=update", "ENDITEM", 
			"Name=objectId", "Value=", "ENDITEM", 
			"Name=requesttype", "Value=ajax", "ENDITEM", 
			"LAST");
		
		lr_end_sub_transaction ( "T12_Select_SM_S02_TCPTraditionalAjaxShippingDetailsURL", 2 ) ;

		lr_start_sub_transaction ( "T12_Select_SM_S03_TCPCurrentOrderInformationView", "T12_Select Ship Mode" ) ;

		web_submit_data ( "T12_Select Ship Mode_TCPCurrentOrderInformationView" ,
				  "Action=https://{host}/shop/TCPCurrentOrderInformationView?catalogId={catalogId}&orderId=&langId=-1&storeId={storeId}" ,
				  "Method=POST" ,
				  "Mode=HTML" ,
				  "ITEMDATA" ,
				  "Name=objectId" , "Value=" , "ENDITEM" ,
				  "Name=requesttype" , "Value=ajax" , "ENDITEM" ,
				  "LAST" ) ;

		lr_end_sub_transaction ( "T12_Select_SM_S03_TCPCurrentOrderInformationView", 2 ) ;

		
	lr_end_transaction ( "T12_Select Ship Mode" , 0 ) ;
	
}  

int selectBillingAddress()
{
		lr_think_time ( FORM_TT ) ;

		lr_start_transaction ( "T14_Select Billing Address" ) ;

		lr_start_sub_transaction ( "T14_Select_BA_S01_TCPBillingAddressDisplayView", "T14_Select Billing Address" ) ;

		web_submit_data ( "T14_Select Billing Address_TCPBillingAddressDisplayView" ,
 
				  "Action=https://{host}/shop/TCPBillingAddressDisplayView?catalogId={catalogId}&langId=-1&storeId={storeId}" ,
				  "Method=POST" ,
				  "Mode=HTML" ,
				  "ITEMDATA" ,
					"Name=selectedAddressId" , "Value={addressId}" , "ENDITEM" ,
					"Name=shipAddressId" , "Value={addressId}" , "ENDITEM" ,
					"Name=objectId" , "Value=tcpBillingAddressSelectBoxArea" , "ENDITEM" ,
					"Name=requesttype" , "Value=ajax" , "ENDITEM" ,
				  "LAST" ) ;

		lr_end_sub_transaction ( "T14_Select_BA_S01_TCPBillingAddressDisplayView", 2 ) ;

		web_reg_save_param ( "authToken" , "LB=name=\"authToken\" value=\"" , "RB=\" id=\"WC_ShopcartAddressFormDisplay_inputs_authToken", "Notfound=warning", "LAST" ) ;
		
		lr_start_sub_transaction ( "T14_Select_BA_S02_TCPAddressEditView", "T14_Select Billing Address" ) ;

		web_submit_data ( "T14_Select Billing Address_TCPAddressEditView" ,
 
				  "Action=https://{host}/webapp/wcs/stores/servlet/TCPAddressEditView?catalogId={catalogId}&langId=-1&storeId={storeId}" ,
				  "Method=POST" ,
				  "Mode=HTML" ,
				  "ITEMDATA" ,
					"Name=addressId" , "Value={addressId}" , "ENDITEM" ,
					"Name=fromPage" , "Value=billingPage" , "ENDITEM" ,
					"Name=shipAddressId" , "Value={addressId}" , "ENDITEM" ,
					"Name=objectId" , "Value=editBillingAddressArea" , "ENDITEM" ,
					"Name=requesttype" , "Value=ajax" , "ENDITEM" ,
					"LAST" ) ;
	 
# 1677 "..\\..\\checkoutFunctions.c"
 
		if (lr_end_sub_transaction ( "T14_Select_BA_S02_TCPAddressEditView", 2 ) != 0)
				lr_log_message("Failed addressId=%s, user=%s", lr_eval_string("{addressId}"), lr_eval_string("{logonid}"));

	lr_end_transaction ( "T14_Select Billing Address" , 0 ) ;
	return 0;
} 

void selectBillingAddressAsGuest()
{
		lr_think_time ( FORM_TT ) ;

		lr_start_transaction ( "T14_Select Billing Address As Guest" ) ;

		lr_start_sub_transaction ( "T14_Select_BA_Guest_S01_TCPBillingAddressDisplayView", "T14_Select Billing Address As Guest" ) ;

		web_submit_data ( "T14_Select Billing Address_TCPBillingAddressDisplayView" ,
				  "Action=https://{host}/shop/TCPBillingAddressDisplayView?catalogId={catalogId}&langId=-1&storeId={storeId}" ,
				  "Method=POST" ,
				  "Mode=HTML" ,
				  "ITEMDATA" ,
					"Name=selectedAddressId" , "Value={addressId}" , "ENDITEM" ,
					"Name=shipAddressId" , "Value={addressId}" , "ENDITEM" ,
					"Name=objectId" , "Value=tcpBillingAddressSelectBoxArea" , "ENDITEM" ,
					"Name=requesttype" , "Value=ajax" , "ENDITEM" ,
				  "LAST" ) ;

		lr_end_sub_transaction ( "T14_Select_BA_Guest_S01_TCPBillingAddressDisplayView", 2 ) ;

		lr_start_sub_transaction ( "T14_Select_BA_Guest_S02_TCPAddressEditView", "T14_Select Billing Address As Guest" ) ;

		web_submit_data ( "T14_Select Billing Address_TCPAddressEditView" ,
				  "Action=https://{host}/webapp/wcs/stores/servlet/TCPAddressEditView?catalogId={catalogId}&langId=-1&storeId={storeId}" ,
				  "Method=POST" ,
				  "Mode=HTML" ,
				  "ITEMDATA" ,
					"Name=addressId" , "Value={addressId}" , "ENDITEM" ,
					"Name=fromPage" , "Value=billingPage" , "ENDITEM" ,
					"Name=shipAddressId" , "Value={addressId}" , "ENDITEM" ,
					"Name=objectId" , "Value=editBillingAddressArea" , "ENDITEM" ,
					"Name=requesttype" , "Value=ajax" , "ENDITEM" ,
					"LAST" ) ;
 
# 1730 "..\\..\\checkoutFunctions.c"
		lr_end_sub_transaction ( "T14_Select_BA_Guest_S02_TCPAddressEditView", 2 ) ;

	lr_end_transaction ( "T14_Select Billing Address As Guest" , 0 ) ;

} 

void submitBillingAddressAsGuest()
{
	lr_think_time ( FORM_TT ) ;
 
	lr_param_sprintf ( "emailAddress1" , "g%s%ld@childrensplace.com" , lr_eval_string ( "{vuser}" ) , _time32(&t) ) ;
 

	lr_start_transaction ( "T15_Submit Billing Address As Guest" ) ;

			lr_start_sub_transaction("T15_Submit_BA_S01_TCPAjaxEmailVerificationCmd", "T15_Submit Billing Address As Guest");   

				web_url ( "T15_Submit Shipping Address As Guest_TCPAjaxEmailVerificationCmd" ,
						 "URL=https://{host}/webapp/wcs/stores/servlet/TCPAjaxEmailVerificationCmd?email={emailAddress1}&page=billing&requesttype=ajax" ,
						 "Resource=0" ,
						 "Mode=HTML" ,
						 "LAST" ) ;

			lr_end_sub_transaction("T15_Submit_BA_S01_TCPAjaxEmailVerificationCmd", 2);

	 
# 1790 "..\\..\\checkoutFunctions.c"
			lr_start_sub_transaction ( "T15_Submit_BA_S02_AjaxUpdateBillingAddressId", "T15_Submit Billing Address As Guest" ) ;

			web_submit_data ( "T15_Submit_BA_AjaxUpdateBillingAddressId" ,
					  "Action=https://{host}/webapp/wcs/stores/servlet/AjaxUpdateBillingAddressId" ,
					  "Method=POST" ,
					  "Mode=HTML" ,
					  "ITEMDATA" ,
					  "Name=storeId" , "Value={storeId}" , "ENDITEM" ,
					  "Name=langId" , "Value=-1" , "ENDITEM" ,
					  "Name=catalogId" , "Value={catalogId}" , "ENDITEM" ,
					  "Name=orderId" , "Value={orderId}" , "ENDITEM" ,
					  "Name=addressId" , "Value={addressId}" , "ENDITEM" ,
					  "Name=requesttype" , "Value=ajax" , "ENDITEM" ,
					  "Name=email" , "Value={emailAddress1}" , "ENDITEM" ,
					  "Name=newsletterSignUp" , "Value=true" , "ENDITEM" ,
					  "Name=response" , "Value=accept_all::false:false" , "ENDITEM" ,
					  "LAST" ) ;

			lr_end_sub_transaction ( "T15_Submit_BA_S02_AjaxUpdateBillingAddressId", 2 ) ;

			web_reg_find ( "Text=piId" , "SaveCount=pi_id_count" , "LAST" ) ;
			web_reg_find ( "Text=errorCode" , "SaveCount=error_count" , "LAST" ) ;

			lr_start_sub_transaction( "T15_Submit_BA_S03_AjaxOrderChangeServicePIAdd", "T15_Submit Billing Address As Guest" );

				web_submit_data ( "T15_Submit_BA_AjaxOrderChangeServicePIAdd" ,
						  "Action=https://{host}/webapp/wcs/stores/servlet/AjaxOrderChangeServicePIAdd" ,
						  "Method=POST" ,
						  "Mode=HTML" ,
						  "ITEMDATA" ,
						  "Name=storeId" , "Value={storeId}" , "ENDITEM" ,
						  "Name=langId" , "Value=-1" , "ENDITEM" ,
						  "Name=catalogId" , "Value={catalogId}" , "ENDITEM" ,
						  "Name=valueFromPaymentTC" , "Value= " , "ENDITEM" ,
						  "Name=paymentTCId" , "Value=" , "ENDITEM" ,
						   
						  "Name=payMethodId" , "Value=VISA" , "ENDITEM" ,
						   
						  "Name=piAmount" , "Value={piAmount}" , "ENDITEM" ,
						  "Name=billing_address_id" , "Value={addressId}" , "ENDITEM" ,
						  "Name=cc_brand" , "Value=Visa" , "ENDITEM" ,
						  "Name=cc_cvc" , "Value=111" , "ENDITEM" ,
						   
						  "Name=account" , "Value=4012000033330026" , "ENDITEM" ,
						  "Name=expire_month" , "Value=12" , "ENDITEM" ,
						  "Name=expire_year" , "Value=2016" , "ENDITEM" ,
						  "Name=check_routing_number" , "Value= " , "ENDITEM" ,
						  "Name=checkAccountNumber" , "Value= " , "ENDITEM" ,
						  "Name=checkRoutingNumber" , "Value= " , "ENDITEM" ,
						  "Name=requesttype" , "Value=ajax" , "ENDITEM" ,
						  "Name=authToken" , "Value={authToken}" ,"ENDITEM" ,
						  "Name=callFromPage" , "Value=CheckoutBillingInfo" ,"ENDITEM" ,
						  "LAST" ) ;

			lr_end_sub_transaction("T15_Submit_BA_S03_AjaxOrderChangeServicePIAdd", 2);

			if ( atoi ( lr_eval_string ( "{pi_id_count}" ) ) == 0 ) {
					 
					lr_end_transaction ( "T15_Submit Billing Address As Guest" , 1 ) ;
					lr_exit(2, 0);
			}

			if ( atoi ( lr_eval_string ( "{error_count}" ) ) > 0 ) {
					 
					lr_end_transaction ( "T15_Submit Billing Address As Guest" , 1 ) ;
					lr_exit(2, 0);
			}

			lr_start_sub_transaction( "T15_Submit_BA_S04_OrderCalculate", "T15_Submit Billing Address As Guest" );

			web_url ( "T15_Submit_BA_OrderCalculate" ,
					  "URL=https://{host}/webapp/wcs/stores/servlet/OrderCalculate?calculationUsageId=-1&calculationUsageId=-2&calculationUsageId=-3&calculationUsageId=-4&calculationUsageId=-5&calculationUsageId=-6&calculationUsageId=-7&URL=TCPSingleShipmentOrderSummaryView&langId=-1&storeId={storeId}&catalogId={catalogId}&purchaseorder_id=&quickCheckoutProfileForPayment=false" ,
					  "Resource=0" ,
					  "Mode=HTML" ,
					  "LAST" ) ;

			lr_end_sub_transaction("T15_Submit_BA_S04_OrderCalculate", 2);

			lr_start_sub_transaction( "T15_Submit_BA_S05_CreateCookieCmd", "T15_Submit Billing Address As Guest" );

			web_url ( "T15_Submit_BA_CreateCookieCmd" ,
					  "URL=https://{host}/webapp/wcs/stores/servlet/CreateCookieCmd?langId=-1&storeId={storeId}&goToPage=NoURL&catalogId={catalogId}" ,
					  "Resource=0" ,
					  "Mode=HTML" ,
					  "LAST" ) ;

			lr_end_sub_transaction("T15_Submit_BA_S05_CreateCookieCmd", 2);

			createCookieCmd();

		lr_end_transaction ( "T15_Submit Billing Address As Guest" , 0) ;

}  

int submitBillingAddress()
{
 
 
 
 
	lr_param_sprintf ( "ts" , "%ld%d" , _time32(&t) , rand() % 1000 ) ;
	lr_save_string ( lr_eval_string ( "{addressId}" ) , "billingAddressId" ) ;
	lr_think_time ( FORM_TT ) ;

	lr_start_transaction ( "T15_Submit Billing Address" ) ;
 
# 1930 "..\\..\\checkoutFunctions.c"
		lr_start_sub_transaction ( "T15_Submit_BA_S02_AjaxUpdateBillingAddressId", "T15_Submit Billing Address" ) ;

			web_submit_data ( "T15_Submit_BA_AjaxUpdateBillingAddressId" ,
				  "Action=https://{host}/webapp/wcs/stores/servlet/AjaxUpdateBillingAddressId" ,
				  "Method=POST" ,
				  "Mode=HTML" ,
				  "ITEMDATA" ,
				  "Name=storeId" , "Value={storeId}" , "ENDITEM" ,
				  "Name=langId" , "Value=-1" , "ENDITEM" ,
				  "Name=catalogId" , "Value={catalogId}" , "ENDITEM" ,
				  "Name=orderId" , "Value={orderId}" , "ENDITEM" ,
				  "Name=addressId" , "Value={billingAddressId}" , "ENDITEM" ,
				  "Name=requesttype" , "Value=ajax" , "ENDITEM" ,
				  "Name=newsletterSignUp" , "Value=false" , "ENDITEM" ,
				  "LAST" ) ;

		lr_end_sub_transaction ( "T15_Submit_BA_S02_AjaxUpdateBillingAddressId", 2 ) ;
 
# 1979 "..\\..\\checkoutFunctions.c"
		web_reg_find ( "Text=piId" , "SaveCount=pi_id_count" , "LAST" ) ;
		web_reg_find ( "Text=errorCode" , "SaveCount=error_count" , "LAST" ) ;

		 

		if (atoi(lr_eval_string("{RANDOM_PERCENT}")) <= OFFLINE_PLUGIN) {

			lr_start_sub_transaction( "T15_Submit_BA_S05_AjaxOrderChangeServicePIAdd_OFFLINE_PLUGIN", "T15_Submit Billing Address" );

				web_submit_data ( "T15_Submit_BA_AjaxOrderChangeServicePIAdd_OFFLINE_PLUGIN" ,
						  "Action=https://{host}/webapp/wcs/stores/servlet/AjaxOrderChangeServicePIAdd" ,
						  "Method=POST" ,
						  "Mode=HTML" ,
						  "ITEMDATA" ,
						  "Name=storeId" , "Value={storeId}" , "ENDITEM" ,
						  "Name=langId" , "Value=-1" , "ENDITEM" ,
						  "Name=catalogId" , "Value={catalogId}" , "ENDITEM" ,
						  "Name=valueFromPaymentTC" , "Value= " , "ENDITEM" ,
						  "Name=paymentTCId" , "Value=" , "ENDITEM" ,
						  "Name=payMethodId" , "Value=VISA" , "ENDITEM" ,
						   
						  "Name=piAmount" , "Value={piAmount}" , "ENDITEM" ,
						  "Name=billing_address_id" , "Value={billingAddressId}" , "ENDITEM" ,
						  "Name=cc_brand" , "Value=Visa" , "ENDITEM" ,
						  "Name=cc_cvc" , "Value=111" , "ENDITEM" ,
						   
						  "Name=account" , "Value=4012000033330026" , "ENDITEM" ,
 
 
						  "Name=expire_month" , "Value=12" , "ENDITEM" ,
						  "Name=expire_year" , "Value=2016" , "ENDITEM" ,
						  "Name=check_routing_number" , "Value= " , "ENDITEM" ,
						  "Name=checkAccountNumber" , "Value= " , "ENDITEM" ,
						  "Name=checkRoutingNumber" , "Value= " , "ENDITEM" ,
						  "Name=requesttype" , "Value=ajax" , "ENDITEM" ,
						  "Name=authToken" , "Value={authToken}" ,"ENDITEM" ,
						  "Name=callFromPage" , "Value=CheckoutBillingInfo" ,"ENDITEM" ,
						  "LAST" ) ;

			lr_end_sub_transaction("T15_Submit_BA_S05_AjaxOrderChangeServicePIAdd_OFFLINE_PLUGIN", 2);

		} else if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= PLCC) {

			lr_start_sub_transaction( "T15_Submit_BA_S05_AjaxOrderChangeServicePIAdd_PLCC", "T15_Submit Billing Address" );

				web_submit_data ( "T15_Submit_BA_AjaxOrderChangeServicePIAdd_PLCC" ,
					  "Action=https://{host}/webapp/wcs/stores/servlet/AjaxOrderChangeServicePIAdd" ,
					  "Method=POST" ,
					  "Mode=HTML" ,
					  "ITEMDATA" ,
					  "Name=storeId" , "Value={storeId}" , "ENDITEM" ,
					  "Name=langId" , "Value=-1" , "ENDITEM" ,
					  "Name=catalogId" , "Value={catalogId}" , "ENDITEM" ,
					  "Name=valueFromPaymentTC" , "Value= " , "ENDITEM" ,
					  "Name=paymentTCId" , "Value=" , "ENDITEM" ,
					  "Name=payMethodId" , "Value=CITIPlaceCard" , "ENDITEM" ,
					  "Name=piAmount" , "Value={piAmount}" , "ENDITEM" ,
					  "Name=billing_address_id" , "Value={billingAddressId}" , "ENDITEM" ,
					  "Name=cc_brand" , "Value=PLACE CARD" , "ENDITEM" ,
					  "Name=cc_cvc" , "Value=" , "ENDITEM" ,
					  "Name=account" , "Value=6011644423719045" , "ENDITEM" ,
 
					  "Name=expire_month" , "Value=12" , "ENDITEM" ,
					  "Name=expire_year" , "Value=2015" , "ENDITEM" ,
					  "Name=check_routing_number" , "Value= " , "ENDITEM" ,
					  "Name=checkAccountNumber" , "Value= " , "ENDITEM" ,
					  "Name=checkRoutingNumber" , "Value= " , "ENDITEM" ,
					  "Name=requesttype" , "Value=ajax" , "ENDITEM" ,
					  "Name=authToken" , "Value={authToken}" ,"ENDITEM" ,
					   
					  "LAST" ) ;

			lr_end_sub_transaction( "T15_Submit_BA_S05_AjaxOrderChangeServicePIAdd_PLCC", 2);

		} else if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= GIFT ) {  

 
 

			web_reg_save_param ( "gcBalance" , "LB=\"authorizedAmount\": " , "RB=," , "NotFound=Warning", "LAST" ) ;

			lr_start_sub_transaction( "T15_Submit_BA_S05_Check gift card balance", "T15_Submit Billing Address" );

			 

			web_submit_data("AjaxCheckGiftCardBalance",
				"Action=https://{host}/webapp/wcs/stores/servlet/AjaxCheckGiftCardBalance",
				"Method=POST",
				"RecContentType=text/html",
				"Mode=HTTP",
				"ITEMDATA",
				"Name=storeId", "Value={storeId}", "ENDITEM",
				"Name=catalogId", "Value={catalogId}", "ENDITEM",
				"Name=langId", "Value=-1", "ENDITEM",
 
 
				"Name=giftCardPin", "Value=8456", "ENDITEM",
				"Name=giftCardNbr", "Value=6006491259499906427", "ENDITEM", 
				 
				 
				"Name=startGiftcardRangeUS", "Value=6006491259000000000", "ENDITEM",
				"Name=endGiftcardRangeUS", "Value=6006491259999999999", "ENDITEM",
				"Name=startGiftcardRangePinUS", "Value=6006491259081925900", "ENDITEM",
				"Name=endGiftcardRangePinUS", "Value=6006491259999999999", "ENDITEM",
				"Name=startGiftcardRangePR", "Value=6006492601000000000", "ENDITEM",
				"Name=endGiftcardRangePR", "Value=6006492601499999999", "ENDITEM",
				"Name=startGiftcardRangePinPR", "Value=6006492601002811000", "ENDITEM",
				"Name=endGiftcardRangePinPR", "Value=6006492601499999999", "ENDITEM",
				"Name=startGiftcardRangeTPV", "Value=6006491366000000000", "ENDITEM",
				"Name=endGiftcardRangeTPV", "Value=6006491367999999999", "ENDITEM",
				"Name=startGiftcardRangePinTPV", "Value=6006491366003104405", "ENDITEM",
				"Name=endGiftcardRangePinTPV", "Value=6006491367999999999", "ENDITEM",
				"Name=startGiftcardRangeCA", "Value=6006491364000000000", "ENDITEM",
				"Name=endGiftcardRangeCA", "Value=6006491365999999999", "ENDITEM",
				"Name=giftCardBalanceInquiryCaller", "Value=CheckOutPage", "ENDITEM",
				"Name=requesttype", "Value=ajax", "ENDITEM",
				"LAST");

			web_url("TCPGiftCardBalancePopupView",
				"URL=https://{host}/shop/TCPGiftCardBalancePopupView?catalogId={catalogId}&langId=-1&storeId={storeId}&balance={gcBalance}&isGiftCardAlreadyApplied=false",
				"Resource=0",
				"RecContentType=text/html",
				"Mode=HTTP",
				"LAST");

			lr_end_transaction("T15_Submit_BA_S05_Check gift card balance",2);

			if (!strcmp(lr_eval_string("{gcBalance}"), "0.00") )
				lr_fail_trans_with_error( "Gift Card Balance is ZERO." ) ;
			else
			{
				web_reg_find ( "Text=piId" , "SaveCount=pi_id_count" , "LAST" ) ;
				web_reg_find ( "Text=errorCode" , "SaveCount=error_count" , "LAST" ) ;

				lr_start_sub_transaction( "T15_Submit_BA_S06_Apply GC To Order", "T15_Submit Billing Address" );

				web_submit_data ( "T15_Submit_BA_S06_Apply GC To Order" ,
					  "Action=https://{host}/webapp/wcs/stores/servlet/AjaxOrderChangeServicePIAdd" ,
					  "Method=POST" ,
					  "Mode=HTML" ,
					  "ITEMDATA" ,
					  "Name=storeId" , "Value={storeId}" , "ENDITEM" ,
					  "Name=langId" , "Value=-1" , "ENDITEM" ,
					  "Name=catalogId" , "Value={catalogId}" , "ENDITEM" ,
					  "Name=valueFromPaymentTC" , "Value= " , "ENDITEM" ,
					  "Name=paymentTCId" , "Value=" , "ENDITEM" ,
					  "Name=payMethodId" , "Value=GiftCard" , "ENDITEM" ,
					  "Name=piAmount" , "Value={piAmount}" , "ENDITEM" ,
					  "Name=billing_address_id" , "Value={billingAddressId}" , "ENDITEM" ,
					  "Name=cc_brand" , "Value=GC" , "ENDITEM" ,
					  "Name=cc_cvc" , "Value=" , "ENDITEM" ,
					  "Name=account_pin" , "Value=8456" , "ENDITEM" ,
					  "Name=balance=", "Value={gcBalance}" , "ENDITEM" ,
					  "Name=account" , "Value=6006491259499906427" , "ENDITEM" ,
					  "Name=expire_month" , "Value=12" , "ENDITEM" ,
					  "Name=expire_year" , "Value=2016" , "ENDITEM" ,
					  "Name=check_routing_number" , "Value= " , "ENDITEM" ,
					  "Name=checkAccountNumber" , "Value= " , "ENDITEM" ,
					  "Name=checkRoutingNumber" , "Value= " , "ENDITEM" ,
					  "Name=requesttype" , "Value=ajax" , "ENDITEM" ,
					  "Name=authToken" , "Value={authToken}" ,"ENDITEM" ,
					  "LAST" ) ;

				web_submit_data("AjaxOrderChangeServiceItemUpdate",
					"Action=https://{host}/webapp/wcs/stores/servlet/AjaxOrderChangeServiceItemUpdate",
					"Method=POST",
					"RecContentType=text/html",
					"Mode=HTTP",
					"ITEMDATA",
					"Name=storeId", "Value={storeId}", "ENDITEM",
					"Name=catalogId", "Value={catalogId}", "ENDITEM",
					"Name=langId", "Value=-1", "ENDITEM",
					"Name=orderId", "Value=.", "ENDITEM",
					"Name=calculationUsage", "Value=-1,-2,-3,-4,-5,-6,-7", "ENDITEM",
					"Name=requesttype", "Value=ajax", "ENDITEM",
					"LAST");

				web_submit_data("TCPCurrentOrderPaymentSummaryView",
					"Action=https://{host}/shop/TCPCurrentOrderPaymentSummaryView?catalogId={catalogId}&orderId={orderId}&langId=-1&storeId={storeId}",
					"Method=POST",
					"RecContentType=text/html",
					"Mode=HTTP",
					"ITEMDATA",
					"Name=fromPage", "Value=billingPage", "ENDITEM",
					"Name=objectId", "Value=", "ENDITEM",
					"Name=requesttype", "Value=ajax", "ENDITEM",
					"LAST");

				web_submit_data("TCPCurrentOrderGiftCardView",
					"Action=https://{host}/shop/TCPCurrentOrderGiftCardView?catalogId={catalogId}&orderId={orderId}&langId=-1&storeId={storeId}",
					"Method=POST",
					"RecContentType=text/html",
					"Mode=HTTP",
					"ITEMDATA",
					"Name=giftAction", "Value=add", "ENDITEM",
					"Name=objectId", "Value=", "ENDITEM",
					"Name=requesttype", "Value=ajax", "ENDITEM",
					"LAST");

				web_submit_data("TCPMiniShopCartDisplayView1",
					"Action=https://{host}/shop/TCPMiniShopCartDisplayView1?catalogId={catalogId}&langId=-1&storeId={storeId}",
					"Method=POST",
					"RecContentType=text/html",
					"Mode=HTTP",
					"ITEMDATA",
					"Name=showredEye", "Value=true", "ENDITEM",
					"Name=objectId", "Value=", "ENDITEM",
					"Name=requesttype", "Value=ajax", "ENDITEM",
					"LAST");

				web_submit_data("TCPVoucherEarnPlaceCashCheckOutView",
					"Action=https://{host}/shop/TCPVoucherEarnPlaceCashCheckOutView?catalogId={catalogId}&orderId={orderId}&langId=-1&storeId={storeId}",
					"Method=POST",
					"RecContentType=text/html",
					"Mode=HTTP",
					"ITEMDATA",
					"Name=objectId", "Value=", "ENDITEM",
					"Name=requesttype", "Value=ajax", "ENDITEM",
					"LAST");

				web_submit_data("TraditionalAjaxShippingDetailsURL",
					"Action=https://{host}/shop/TraditionalAjaxShippingDetailsURL?catalogId={catalogId}&langId=-1&storeId={storeId}",
					"Method=POST",
					"RecContentType=text/html",
					"Mode=HTTP",
					"ITEMDATA",
					"Name=shipmentDetailsArea", "Value=update", "ENDITEM",
					"Name=objectId", "Value=", "ENDITEM",
					"Name=requesttype", "Value=ajax", "ENDITEM",
					"LAST");

				web_submit_data("TCPCurrentOrderPaymentSummaryView",
					"Action=https://{host}/shop/TCPCurrentOrderPaymentSummaryView?catalogId={catalogId}&orderId={orderId}&langId=-1&storeId={storeId}",
					"Method=POST",
					"RecContentType=text/html",
					"Mode=HTTP",
					"ITEMDATA",
					"Name=fromPage", "Value=billingPage", "ENDITEM",
					"Name=objectId", "Value=", "ENDITEM",
					"Name=requesttype", "Value=ajax", "ENDITEM",
					"LAST");

				web_submit_data("orderTotalAsJSON",
					"Action=https://{host}/webapp/wcs/stores/servlet/orderTotalAsJSON",
					"Method=POST",
					"RecContentType=text/html",
					"Mode=HTTP",
					"ITEMDATA",
					"LAST");

				web_submit_data("TCPCurrentOrderGiftCardView",
					"Action=https://{host}/shop/TCPCurrentOrderGiftCardView?catalogId={catalogId}&orderId={orderId}&langId=-1&storeId={storeId}",
					"Method=POST",
					"RecContentType=text/html",
					"Mode=HTTP",
					"ITEMDATA",
					"Name=giftAction", "Value=", "ENDITEM",
					"Name=objectId", "Value=", "ENDITEM",
					"Name=requesttype", "Value=ajax", "ENDITEM",
					"LAST");

				web_submit_data("orderTotalAsJSON",
					"Action=https://{host}/webapp/wcs/stores/servlet/orderTotalAsJSON",
					"Method=POST",
					"RecContentType=text/html",
					"Mode=HTTP",
					"ITEMDATA",
					"LAST");

				web_submit_data("TCPVoucherEarnPlaceCashCheckOutView",
					"Action=https://{host}/shop/TCPVoucherEarnPlaceCashCheckOutView?catalogId={catalogId}&orderId={orderId}&langId=-1&storeId={storeId}",
					"Method=POST",
					"Mode=HTTP",
					"ITEMDATA",
					"Name=objectId", "Value=", "ENDITEM",
					"Name=requesttype", "Value=ajax", "ENDITEM",
					"LAST");

				web_submit_data("TCPCurrentOrderPaymentInformationView",
					"Action=https://{host}/shop/TCPCurrentOrderPaymentInformationView?catalogId={catalogId}&orderId={orderId}&langId=-1&storeId={storeId}",
					"Method=POST",
					"RecContentType=text/html",
					"Mode=HTTP",
					"ITEMDATA",
					"Name=objectId", "Value=", "ENDITEM",
					"Name=requesttype", "Value=ajax", "ENDITEM",
					"LAST");

				lr_end_transaction("T15_Submit_BA_S06_Apply GC To Order",2);

			}  
		}

 
			if ( atoi ( lr_eval_string ( "{pi_id_count}" ) ) == 0 ) {
					 
					lr_end_transaction ( "T15_Submit Billing Address" , 1 ) ;
					lr_exit(2, 0);
			}

			if ( atoi ( lr_eval_string ( "{error_count}" ) ) > 0 ) {
					 
					lr_end_transaction ( "T15_Submit Billing Address" , 1 ) ;
					lr_exit(2, 0);
			}
 

		lr_start_sub_transaction( "T15_Submit_BA_S06_OrderCalculate", "T15_Submit Billing Address" );

			web_url ( "T15_Submit_BA_OrderCalculate" ,
					  "URL=https://{host}/webapp/wcs/stores/servlet/OrderCalculate?calculationUsageId=-1&calculationUsageId=-2&calculationUsageId=-3&calculationUsageId=-4&calculationUsageId=-5&calculationUsageId=-6&calculationUsageId=-7&URL=TCPSingleShipmentOrderSummaryView&langId=-1&storeId={storeId}&catalogId={catalogId}&purchaseorder_id=&quickCheckoutProfileForPayment=false" ,
					  "Resource=0" ,
					  "Mode=HTML" ,
					  "LAST" ) ;

			lr_end_sub_transaction("T15_Submit_BA_S06_OrderCalculate", 2);

			lr_start_sub_transaction( "T15_Submit_BA_S07_CreateCookieCmd", "T15_Submit Billing Address" );

			web_url ( "T15_Submit_BA_CreateCookieCmd" ,
					  "URL=https://{host}/webapp/wcs/stores/servlet/CreateCookieCmd?langId=-1&storeId={storeId}&goToPage=NoURL&catalogId={catalogId}" ,
					  "Resource=0" ,
					  "Mode=HTML" ,
					  "LAST" ) ;

			lr_end_sub_transaction("T15_Submit_BA_S07_CreateCookieCmd", 2);

			createCookieCmd();

	lr_end_transaction ( "T15_Submit Billing Address" , 0) ;
	return 0;
}  

void submitOrder()
{
	lr_think_time ( FORM_TT ) ;

	lr_start_transaction ( "T16_Submit Order" ) ;

	lr_start_sub_transaction("T16_Submit Order_S01_AjaxOrderProcessServiceOrderPrepare", "T16_Submit Order");

	web_submit_data ( "T16_Submit Order_submitOrder_AjaxOrderProcessServiceOrderPrepare" ,
					  "Action=https://{host}/webapp/wcs/stores/servlet/AjaxOrderProcessServiceOrderPrepare" ,
					  "Method=POST" ,
					  "Mode=HTML" ,
					  "ITEMDATA" ,
					  "Name=requesttype" , "Value=ajax" , "ENDITEM" ,
					  "LAST" ) ;

	lr_end_sub_transaction("T16_Submit Order_S01_AjaxOrderProcessServiceOrderPrepare", 2);

			
	lr_start_sub_transaction("T16_Submit Order_S02_AjaxOrderProcessServiceOrderSubmit", "T16_Submit Order");

	web_submit_data ( "T16_Submit Order_AjaxOrderProcessServiceOrderSubmit" ,
					  "Action=https://{host}/webapp/wcs/stores/servlet/AjaxOrderProcessServiceOrderSubmit" ,
					  "Method=POST" ,
					  "Mode=HTML" ,
					  "ITEMDATA" ,
					  "Name=orderId" , "Value={orderId}" , "ENDITEM" ,
					  "Name=notifyMerchant" , "Value=0" , "ENDITEM" ,
					  "Name=notifyShopper" , "Value=0" , "ENDITEM" ,
					  "Name=notifyOrderSubmitted" , "Value=0" , "ENDITEM" ,
					  "Name=storeId" , "Value={storeId}" , "ENDITEM" ,
					  "Name=langId" , "Value=-1" , "ENDITEM" ,
					  "Name=catalogId" , "Value={catalogId}" , "ENDITEM" ,
					  "Name=chosenLocale" , "Value=en" , "ENDITEM" ,
					  "Name=visitorId", "Value=[CS]v1|29E7E17E8507A4B7-4000010E6006518F[CE]", "ENDITEM",
					  "Name=notify_EMailSender_recipient" , "Value=" , "ENDITEM" ,
					  "Name=requesttype" , "Value=ajax" , "ENDITEM" ,
					  "LAST" ) ;

	lr_end_sub_transaction("T16_Submit Order_S02_AjaxOrderProcessServiceOrderSubmit", 2);
	

	web_reg_find ( "Text=thanks for your order!" , "SaveCount=confirmationFound_count" , "LAST" ) ;

	lr_start_sub_transaction("T16_Submit Order_S03_TCPOrderShippingBillingConfirmationView", "T16_Submit Order");

	web_url ( "T16_Submit Order_TCPOrderShippingBillingConfirmationView" ,
			  "URL=https://{host}/webapp/wcs/stores/servlet/TCPOrderShippingBillingConfirmationView?storeId={storeId}&catalogId={catalogId}&langId=-1&orderId={orderId}&shipmentTypeId=1" ,
			  "Resource=0" ,
			  "Mode=HTML" ,
			  "LAST" ) ;

	lr_save_string(lr_eval_string("{orderId}"), "orderIdNumber");
	
 
	if ( strlen ( lr_eval_string ( "{orderIdNumber}" ) ) != 15 || atoi(lr_eval_string ( "{confirmationFound_count}" )) != 0 ) {
		lr_end_sub_transaction("T16_Submit Order_S03_TCPOrderShippingBillingConfirmationView", 0);
		lr_end_transaction("T16_Submit Order", 0);
	} else {
		lr_end_sub_transaction("T16_Submit Order_S03_TCPOrderShippingBillingConfirmationView", 1);
		lr_end_transaction("T16_Submit Order", 1);
	}
}  


void submitOrderGuest()
{
	lr_think_time ( FORM_TT ) ;

	lr_start_transaction ( "T16_Submit Order Guest" ) ;

	lr_start_sub_transaction("T16_Submit Order Guest_S01_AjaxOrderProcessServiceOrderPrepare", "T16_Submit Order Guest");

	web_submit_data ( "T16_Submit Order_submitOrder_AjaxOrderProcessServiceOrderPrepare" ,
					  "Action=https://{host}/webapp/wcs/stores/servlet/AjaxOrderProcessServiceOrderPrepare" ,
					  "Method=POST" ,
					  "Mode=HTML" ,
					  "ITEMDATA" ,
					  "Name=requesttype" , "Value=ajax" , "ENDITEM" ,
					  "LAST" ) ;

	lr_end_sub_transaction("T16_Submit Order Guest_S01_AjaxOrderProcessServiceOrderPrepare", 2);

	lr_start_sub_transaction("T16_Submit Order Guest_S02_AjaxOrderProcessServiceOrderSubmit", "T16_Submit Order Guest");

	web_submit_data ( "T16_Submit Order_AjaxOrderProcessServiceOrderSubmit" ,
					  "Action=https://{host}/webapp/wcs/stores/servlet/AjaxOrderProcessServiceOrderSubmit" ,
					  "Method=POST" ,
					  "Mode=HTML" ,
					  "ITEMDATA" ,
					  "Name=orderId" , "Value={orderId}" , "ENDITEM" ,
					  "Name=notifyMerchant" , "Value=0" , "ENDITEM" ,
					  "Name=notifyShopper" , "Value=0" , "ENDITEM" ,
					  "Name=notifyOrderSubmitted" , "Value=0" , "ENDITEM" ,
					  "Name=storeId" , "Value={storeId}" , "ENDITEM" ,
					  "Name=langId" , "Value=-1" , "ENDITEM" ,
					  "Name=catalogId" , "Value={catalogId}" , "ENDITEM" ,
					  "Name=chosenLocale" , "Value=en" , "ENDITEM" ,
					  "Name=visitorId", "Value=[CS]v1|29E7E17E8507A4B7-4000010E6006518F[CE]", "ENDITEM",
					  "Name=notify_EMailSender_recipient" , "Value=" , "ENDITEM" ,
					  "Name=requesttype" , "Value=ajax" , "ENDITEM" ,
					  "LAST" ) ;

	lr_end_sub_transaction("T16_Submit Order Guest_S02_AjaxOrderProcessServiceOrderSubmit", 2);

 
	web_reg_find ( "Text=thanks for your order!" , "SaveCount=confirmationFound_count" , "LAST" ) ;

	lr_start_sub_transaction("T16_Submit Order Guest_S03_TCPOrderShippingBillingConfirmation", "T16_Submit Order Guest");

	web_url ( "T16_Submit Order_TCPOrderShippingBillingConfirmationView" ,
			  "URL=https://{host}/webapp/wcs/stores/servlet/TCPOrderShippingBillingConfirmationView?storeId={storeId}&catalogId={catalogId}&langId=-1&orderId={orderId}&shipmentTypeId=1" ,
			  "Resource=0" ,
			  "Mode=HTML" ,
			  "LAST" ) ;

	if ( atoi ( lr_eval_string ( "{confirmationFound_count}" ) ) == 1 ) {
		lr_end_sub_transaction("T16_Submit Order Guest_S03_TCPOrderShippingBillingConfirmation", 0);
		lr_end_transaction("T16_Submit Order Guest", 0);
	} else {
 
		lr_end_sub_transaction("T16_Submit Order Guest_S03_TCPOrderShippingBillingConfirmation", 1);
		lr_end_transaction("T16_Submit Order Guest", 1);
	}
	

}  

void viewReservationHistory()
{
	lr_think_time ( LINK_TT );
	 
	 
	
	lr_start_transaction("T18_ViewReservationHistory");

	web_url("TCPDOMMyReservationHistoryView", 
		"URL=https://{host}/webapp/wcs/stores/servlet/TCPDOMMyReservationHistoryView?storeId={storeId}&catalogId={catalogId}&langId=-1&sortRank=&sortKey=&curentPage=1&pageLength=1000", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Mode=HTML", 
		"LAST");

		lr_end_transaction("T18_ViewReservationHistory",2);
	
}

void viewPointsHistory()
{
	lr_think_time ( LINK_TT );
	
	lr_start_transaction("T18_ViewPointsHistory");

	web_custom_request("TCPMyPointsHistoryView", 
		"URL=https://{host}/shop/TCPMyPointsHistoryView?catalogId={catalogId}&langId=-1&storeId={storeId}", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Mode=HTML", 
		"EncType=", 
		"LAST");

	lr_end_transaction("T18_ViewPointsHistory",2);
}

void viewOrderHistory()
{
	lr_think_time ( LINK_TT );

	web_reg_save_param("orderIDs", "LB=&orderId=", "RB=&langId=-1&storeId=", "NotFound=Warning", "ORD=ALL", "LAST");
	web_reg_save_param("fromOrderDetail", "LB=/shop/", "RB=OrderDetail?catalogId=", "NotFound=Warning", "LAST");
	web_reg_find("Text=Transaction Number", "SaveCount=checkOrderhistoryHistory");

	lr_start_transaction ( "T18_ViewOrderHistory" ) ;
	
	web_custom_request("T18_ViewOrderHistory",
	"URL=https://{host}/shop/TCPDOMMyOrderHistoryDisplayContent?catalogId={catalogId}&langId=-1&storeId={storeId}",  
		"Method=POST",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		"EncType=",
		"LAST");

	if ( atoi ( lr_eval_string ( "{checkOrderhistoryHistory}" ) ) > 0 ) 
		lr_end_transaction ( "T18_ViewOrderHistory" , 0) ;
	else
		lr_end_transaction ( "T18_ViewOrderHistory" , 1) ;

} 

void viewOrderStatus()
{
	lr_think_time ( LINK_TT );
	
	if ( atoi( lr_eval_string("{orderIDs_count}")) > 0 ) {

		lr_save_string( lr_paramarr_random("orderIDs"),"orderId");

		lr_start_transaction("T18_ViewOrderStatus");

		web_url("T18_ViewOrderStatus",
 
 
			"URL=http://{host}/shop/{fromOrderDetail}OrderDetail?catalogId={catalogId}&orderNumber={orderId}&companyId=1&orderId={orderId}&langId=-1&storeId={storeId}&shipmentTypeId=1",
			"TargetFrame=_self",
			"Resource=0",
			"RecContentType=text/html",
			 
			"Snapshot=t50.inf",
			"Mode=HTML",
			"LAST");

		lr_end_transaction("T18_ViewOrderStatus", 2);

	}
} 


void logoff()
{
	lr_start_transaction("T21_Logoff");

	web_url("T21_Logoff",
		"URL=https://{host}/shop/Logoff?catalogId={catalogId}&rememberMe=false&myAcctMain=1&langId=-1&storeId={storeId}&URL=LogonForm",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Snapshot=t62.inf",
		"Mode=HTML",
		"LAST");

	lr_end_transaction("T21_Logoff", 2);

}

void registerUser()
{
	lr_think_time ( FORM_TT ) ;

	lr_start_transaction ( "T17_Register User" ) ;

	lr_start_sub_transaction("T17_Register User_S01_TCPAjaxEmailVerificationCmd", "T17_Register User");

	web_url ( "T17_Register User_TCPAjaxEmailVerificationCmd" ,
			  "URL=https://{host}/webapp/wcs/stores/servlet/TCPAjaxEmailVerificationCmd?email={emailAddress1}&page=registration&requesttype=ajax" ,
			  "Resource=0" ,
			  "Mode=HTML" ,
			  "LAST" ) ;

	lr_end_sub_transaction("T17_Register User_S01_TCPAjaxEmailVerificationCmd", 2);

	lr_start_sub_transaction("T17_Register User_S02_PersonProcessServicePersonRegister", "T17_Register User");

	web_submit_data ( "T17_Register User_PersonProcessServicePersonRegister" ,
			  "Action=https://{host}/shop/PersonProcessServicePersonRegister" ,
			  "Method=POST" ,
			  "Mode=HTML" ,
			  "ITEMDATA" ,
			  "Name=logonPassword" , "Value=asdf1234" , "ENDITEM" ,
			  "Name=logonPasswordVerify" , "Value=asdf1234" , "ENDITEM" ,
			  "Name=termsCheck" , "Value=on" , "ENDITEM" ,
			  "Name=myAcctMain" , "Value=" , "ENDITEM" ,
			  "Name=new" , "Value=Y" , "ENDITEM" ,
			  "Name=storeId" , "Value={storeId}" , "ENDITEM" ,
			  "Name=catalogId" , "Value={catalogId}" , "ENDITEM" ,
			  "Name=URL" , "Value=Logon?reLogonURL=LogonForm&storeId={storeId}&catalogId={catalogId}&langId=-1&URL=TCPAccountVerifyView&from=Register&Country=" , "ENDITEM" ,
			  "Name=receiveSMS" , "Value=false" , "ENDITEM" ,
			  "Name=addressType" , "Value=ShippingAndBilling" , "ENDITEM" ,
			  "Name=errorViewName" , "Value=TCPOrderShippingBillingConfirmationView" , "ENDITEM" ,
			  "Name=orderId" , "Value={orderId}" , "ENDITEM" ,
			  "Name=guestRegistrationFlag" , "Value=true" , "ENDITEM" ,
			  "Name=orderConfirmFlag" , "Value=true" , "ENDITEM" ,
			  "Name=page" , "Value=account" , "ENDITEM" ,
			  "Name=registerType" , "Value=Guest" , "ENDITEM" ,
			  "Name=primary" , "Value=true" , "ENDITEM" ,
			  "Name=profileType" , "Value=Consumer" , "ENDITEM" ,
			  "Name=RegistrationApprovalStatus" , "Value=3" , "ENDITEM" ,
			  "Name=firstName" , "Value=Teddy" , "ENDITEM" ,
			  "Name=lastName" , "Value=Bear" , "ENDITEM" ,
			  "Name=logonId" , "Value={emailAddress1}" , "ENDITEM" ,
			  "Name=email1Verify" , "Value={emailAddress1}" , "ENDITEM" ,
			  "Name=address1" , "Value={guestAdr1}" , "ENDITEM" ,
			  "Name=address2" , "Value=" , "ENDITEM" ,
			  "Name=city" , "Value={guestCity}" , "ENDITEM" ,
			  "Name=state" , "Value={guestState}" , "ENDITEM" ,
			  "Name=addressField3" , "Value={guestZip}" , "ENDITEM" ,
			  "Name=zipCode" , "Value={guestZip}" , "ENDITEM" ,
			  "Name=country" , "Value={guestCountry}" , "ENDITEM" ,
			  "Name=phone1" , "Value=4163218351" , "ENDITEM" ,
			  "Name=uniform-WC_TCPUserRegistrationAddForm_FormInput_state" , "Value=" , "ENDITEM" ,
			  "Name=receiveSMSNotification" , "Value=false" , "ENDITEM" ,
			  "Name=demographicField1" , "Value=0" , "ENDITEM" ,
			  "LAST" ) ;

	lr_end_sub_transaction("T17_Register User_S02_PersonProcessServicePersonRegister", 2);

	lr_end_transaction ( "T17_Register User" , 0) ;

	 

}  

void updateQuantity()
{
	lr_think_time ( FORM_TT ) ;

	lr_start_transaction ( "T09_Update Quantity" ) ;

	lr_start_sub_transaction("T09_Update Quantity_S01_AjaxOrderChangeServiceItemUpdate", "T09_Update Quantity");

		web_submit_data("AjaxOrderChangeServiceItemUpdate",
			"Action=https://{host}/webapp/wcs/stores/servlet/AjaxOrderChangeServiceItemUpdate",
			"Method=POST",
			"TargetFrame=",
			"RecContentType=text/html",
			"Mode=HTML",
			"ITEMDATA",
			"Name=orderId", "Value=.", "ENDITEM",
			"Name=storeId", "Value={storeId}", "ENDITEM",
			"Name=catalogId", "Value={catalogId}", "ENDITEM",
			"Name=langId", "Value=-1", "ENDITEM",
			"Name=from", "Value=shoppingCart", "ENDITEM",
			"Name=calculationUsage", "Value=-1,-2,-5,-6,-7", "ENDITEM",
			"Name=inventoryValidation", "Value=true", "ENDITEM",
			"Name=orderItemId_1", "Value={orderItemId}", "ENDITEM",
			"Name=quantity_1", "Value=2", "ENDITEM",
			"Name=requesttype", "Value=ajax", "ENDITEM",
			"LAST");

	lr_end_sub_transaction("T09_Update Quantity_S01_AjaxOrderChangeServiceItemUpdate", 2);

	lr_start_sub_transaction("T09_Update Quantity_S02_CreateCookieCmd", "T09_Update Quantity");

		web_custom_request("T09_Update Quantity_CreateCookieCmd",
			"URL=http://{host}/webapp/wcs/stores/servlet/CreateCookieCmd?langId=-1&storeId={storeId}&goToPage=NoURL&catalogId={catalogId}" ,
			"Method=GET",
			"TargetFrame=",
			"Resource=0",
			"RecContentType=text/html",
			"Mode=HTML",
			"EncType=application/x-www-form-urlencoded",
			"LAST");

	lr_end_sub_transaction("T09_Update Quantity_S02_CreateCookieCmd", 2);

	createCookieCmd();

	lr_start_sub_transaction("T09_Update Quantity_S03_TCPMiniShopCartDisplayView1", "T09_Update Quantity");

		web_submit_data("T09_Update Quantity_TCPMiniShopCartDisplayView1",
			"Action=https://{host}/webapp/wcs/stores/servlet/TCPMiniShopCartDisplayView1?catalogId={catalogId}&langId=-1&storeId={storeId}" ,
			"Method=POST",
			"TargetFrame=",
			"RecContentType=text/html",
			"Mode=HTML",
			"ITEMDATA",
			"Name=showredEye", "Value=true", "ENDITEM",
			"Name=objectId", "Value=", "ENDITEM",
			"Name=requesttype", "Value=ajax", "ENDITEM",
		"LAST");

	lr_end_sub_transaction("T09_Update Quantity_S03_TCPMiniShopCartDisplayView1", 2);

	 
		lr_start_sub_transaction ("T09_Update Quantity_S04_CreateCookieCmd", "T09_Update Quantity" ) ;

		web_custom_request("UpdateQuantity_CreateCookieCmd",
			"URL=https://{host}/webapp/wcs/stores/servlet/CreateCookieCmd?langId=-1&storeId={storeId}&goToPage=NoURL&catalogId={catalogId}",
			"Method=GET",
			"Resource=0",
			"RecContentType=text/html",
			"Snapshot=t28.inf",
			"Mode=HTML",
			"EncType=application/x-www-form-urlencoded",
			"LAST");

		lr_end_sub_transaction ("T09_Update Quantity_S04_CreateCookieCmd", 2) ;

		createCookieCmd();

	lr_end_transaction ( "T09_Update Quantity" , 0) ;

}  
 
# 2710 "..\\..\\checkoutFunctions.c"

 
# 2724 "..\\..\\checkoutFunctions.c"


void buildCartDrop(int userProfile)
{
 
	int iLoop = 0;

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CART_MERGE ) {  
		target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_MERGE_CART_SIZE}"));
 
	}
	else {
		target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_DROP_CART_SIZE}"));
 
	}
	if ( strcmp(strupr(lr_eval_string("{buildCart}")),  "TRUE") == 0) {
		target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_BUILD_CART}"));
 
	}

	iLoop = target_itemsInCart + 2;
	
	lr_start_transaction("T20_New_Cart");
	lr_end_transaction("T20_New_Cart", 0);

 
	if ( userProfile == 0 && atoi(lr_eval_string("{totalNumberOfItems_count}")) == 1) {  
 
 
		orderItemIdscount = 0;
	}
	else {
		if (atoi(lr_eval_string("{totalNumberOfItems_count}")) == 1) {  
 
 
			orderItemIdscount = 0;
		}
		else
			orderItemIdscount = atoi(lr_eval_string("{totalNumberOfItems_2}"));
	}

 
 
 
 

	if (  orderItemIdscount < target_itemsInCart ) {
	
	    target_itemsInCart = target_itemsInCart - orderItemIdscount ;

 
		
		for(index_buildCart=0; index_buildCart < target_itemsInCart ; index_buildCart++)
		{
			topNav();
			
			if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_BUILDCART_DRILLDOWN ){
				drill();
			}  

			paginate();			
		
			productDisplay();

			addToCart();
		
			iLoop--;
			
			if (iLoop == 0)
			    break;

			if (atc_Stat == 1)   
				index_buildCart--;

 

		}  

 
	}
}  


void buildCartCheckout(int userProfile)
{
 
	int iLoop = 0;
	
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CART_MERGE ) {
		target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_MERGE_CART_SIZE}"));
 
	}
	else {
		target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_CART_SIZE}"));
 
	}

	iLoop = target_itemsInCart + 2;
	
	lr_start_transaction("T20_New_Cart");
	lr_end_transaction("T20_New_Cart", 0);

 
	if ( userProfile == 0 && atoi(lr_eval_string("{totalNumberOfItems_count}")) == 1) {  
 
 
		orderItemIdscount = 0;
	}
	else {
		if (atoi(lr_eval_string("{totalNumberOfItems_count}")) == 1) {  
 
 
			orderItemIdscount = 0;
		}
		else
			orderItemIdscount = atoi(lr_eval_string("{totalNumberOfItems_2}"));
	}
	
	if (  orderItemIdscount < target_itemsInCart ) {
	
	    target_itemsInCart = target_itemsInCart - orderItemIdscount ;
 
		
 

		for(index_buildCart=0; index_buildCart < target_itemsInCart ; index_buildCart++)
		{
			topNav(); 

			if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_BUILDCART_DRILLDOWN ){
				drill();
			}  

			paginate();			

			productDisplay();

			addToCart();

			iLoop--;
			
			if (iLoop == 0)
			    break;
				
			if (atc_Stat == 1)   
				index_buildCart--;

 

		}  

 
	
	}
	
}  

void buildCartRopis()
{
 		productDisplay();
# 2893 "..\\..\\checkoutFunctions.c"
		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RESERVE_ONLINE && atoi(lr_eval_string("{atc_catentryIds_count}")) != 0) {
			lr_save_string( lr_paramarr_random("atc_catentryIds"), "catentryId");
			reserveOnline();
		}
}


void reserveOnline()
{
 	lr_think_time ( LINK_TT ) ;

	web_reg_save_param("storeUniqueID", "LB=storeUniqueID\":\"", "RB=\",\"itemAvailability", "NotFound=Warning", "LAST");

	lr_start_transaction("T04_Product_Reserve_Online_FIND_IT");

	web_url("GetStoreAndProductInventoryInfo", 
		"URL=http://{host}/webapp/wcs/stores/servlet/GetStoreAndProductInventoryInfo?storeId={storeId}&catalogId={catalogId}&langId=-1&catentryId={catentryId}&latitude={storeLocatorLatitude}&longitude={storeLocatorLongitude}", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Mode=HTML", 
		"LAST");

	lr_end_transaction("T04_Product_Reserve_Online_FIND_IT",2);

 	lr_think_time ( LINK_TT ) ;

	web_reg_save_param("reservationOrderID", "LB=reservationOrderId\": \"", "RB=\",", "NotFound=Warning", "LAST");

	lr_start_transaction("T04_Product_Reserve_Online_Submit");

	web_url("TCPReservationSubmitCmd", 
		"URL=http://{host}/webapp/wcs/stores/servlet/TCPReservationSubmitCmd?storeId={storeId}&catalogId={catalogId}&langId=-1&quantity=1&catEntryId={catentryId}&stlocId={storeUniqueID}&firstname=manny&lastname=Paquiao&phone=2014537616&email={userEmail}&optInEmail&optInEmail=true", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Mode=HTML", 
		"LAST");

	if(strlen(lr_eval_string("{reservationOrderID}")) == 9)
		lr_end_transaction("T04_Product_Reserve_Online_Submit",0);
	else
		lr_end_transaction("T04_Product_Reserve_Online_Submit",1);
	
}

void inCartEdits()
{            
	viewCart(); 
	
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_UPDATE_QUANTITY )
			updateQuantity();   

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_PROMO_APPLY )
			applyPromoCode(0);

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_DELETE_ITEM )
			deleteItem();

	if (isLoggedIn == 1) {
		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= MOVE_FROM_CART_TO_WISHLIST  ) {
 
			moveFromCartToWishlist();
				
 
 
		}
		
 
	}

	viewCart();
	viewCart();  
	
	return;
}


void wlGetProduct()
{
	 

	drill();

	productDisplay();
} 

void wlCreate()
{
	wlView();
	wlGetAll();  

 	lr_think_time ( LINK_TT ) ;
	if (lr_paramarr_len("WishlistIDs") < 5)  {
	 
	 
	 
		lr_start_transaction("T19_Wishlist Create");

		web_url("AjaxGiftListServiceCreate",  
			"URL=http://{host}/webapp/wcs/stores/servlet/AjaxGiftListServiceCreate?name=PerfWL{WL_Random_Number}&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&tcpCallBack=jQuery1113027745301206596196_1440517580196&_=1440517580198",
			"TargetFrame=",
			"Resource=0",
			"RecContentType=text/html",
			"Referer=",
			"Snapshot=t21.inf",
			"Mode=HTML",
			"LAST");

		lr_end_transaction("T19_Wishlist Create", 2);
	}

}

void wlDelete()
{
	wlView();
	wlGetAll();  

 	lr_think_time ( LINK_TT ) ;
	if (lr_paramarr_len("WishlistIDs") > 1)  {
	 
	 
	 
		lr_start_transaction("T19_Wishlist Delete");

		web_url("AjaxGiftListServiceDeleteGiftList",  
			"URL=http://{host}/webapp/wcs/stores/servlet/AjaxGiftListServiceDeleteGiftList?&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&tcpCallBack=jQuery1113008221043809317052_1440783988176&giftListId={WishlistID}&_=1440783988185",
			"TargetFrame=",
			"Resource=0",
			"RecContentType=text/html",
			"Referer=",
			"Snapshot=t21.inf",
			"Mode=HTML",
			"LAST");

		lr_end_transaction("T19_Wishlist Delete", 2);
	}
}

void wlChange()
{
	wlView();
	wlGetAll();  

 	lr_think_time ( LINK_TT ) ;
	 
	 
	 
	lr_start_transaction("T19_Wishlist Change Name");

	web_url("AjaxGiftListServiceUpdateDescription",  
		"URL=http://{host}/webapp/wcs/stores/servlet/AjaxGiftListServiceUpdateDescription?&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&tcpCallBack=jQuery1113008221043809317052_1440783988176&giftListId={WishlistID}&name=Perf Changed WL_{WL_Random_Number}&type=name&_=1440783988193",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Referer=",
		"Snapshot=t21.inf",
		"Mode=HTML",
		"LAST");

	lr_end_transaction("T19_Wishlist Change Name", 2);

}

void TCPGetWishListForUsersView()
{
	lr_start_transaction("T25_Common_S01_TCPGetWishListForUsersView");

	web_custom_request("TCPGetWishListForUsersView",
		"URL=http://{host}/webapp/wcs/stores/servlet/TCPGetWishListForUsersView?tcpCallBack=jQuery1113014590106982485151_1473881708524&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&sortBy=&_=1473881708525", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Mode=HTML", 
		"EncType=application/x-www-form-urlencoded", 
		"LAST");

	lr_end_sub_transaction("T25_Common_S01_TCPGetWishListForUsersView", 2);
	
}

void wlView()
{
 	lr_think_time ( LINK_TT ) ;

	lr_start_transaction("T19_Wishlist View");

		lr_start_sub_transaction("T19_Wishlist View_S01_TCPMiniShopCartDisplayView", "T19_Wishlist View");
		
			web_url("TCPViewWL",
				"URL=http://tcp-perf.childrensplace.com/shop/us/kids-baby-gifts-registry-wishlist",
				"TargetFrame=",
				"Resource=0",
				"RecContentType=text/html",
				"Snapshot=t41.inf",
				"Mode=HTML",
				"LAST");

		lr_end_sub_transaction("T19_Wishlist View_S01_TCPMiniShopCartDisplayView", 2);

		web_reg_save_param("WishlistIDs", "LB=\"giftListExternalIdentifier\": ", "RB=,", "ORD=All", "NotFound=Warning", "LAST");
		
		TCPGetWishListForUsersView();
		
	lr_end_transaction("T19_Wishlist View", 2);
}

void wlAddItem()
{
	wlGetAll();  

	target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_CART_SIZE}"));

	 

 
 
		wlGetProduct();

		if (lr_paramarr_len("atc_catentryIds") > 0) {

			lr_save_string( lr_paramarr_random( "atc_catentryIds" ), "atc_catentryId");

			 
			 
			 

			lr_start_transaction("T19_Wishlist Add Item");

			web_url("AjaxGiftListServiceAddItem",  
				"URL=http://{host}/webapp/wcs/stores/servlet/AjaxGiftListServiceAddItem?&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&tcpCallBack=jQuery111301397454545367509_1440779623033&giftListId={WishlistID}&catEntryId_1={atc_catentryId}&quantity_1=1&_=1440779623035",
				"TargetFrame=",
				"Resource=0",
				"RecContentType=text/html",
				"Referer=",
				"Snapshot=t21.inf",
				"Mode=HTML",
				"LAST");

			lr_end_transaction("T19_Wishlist Add Item", 2);

		}  

		lr_think_time ( LINK_TT ) ;
 

}

void wlAddToCart()
{
	wlView();
	wlGetAll();  
	wlGetItems();

 	lr_think_time ( LINK_TT ) ;

	if (lr_paramarr_len("catEntryIds") > 0) {

		lr_save_string( lr_paramarr_random( "catEntryIds" ), "catEntryId");
		web_reg_save_param("orderId", "LB=\"orderId\": [\"", "RB=\"", "NotFound=Warning", "LAST");
		web_reg_save_param("orderItemId", "LB=\"orderItemId\": [\"", "RB=\"", "NotFound=Warning", "LAST");

		 
		 
		 

		lr_start_transaction("T19_Wishlist Add To Cart");

		web_url("AjaxOrderChangeServiceItemAdd",
			"URL=http://{host}/webapp/wcs/stores/servlet/AjaxOrderChangeServiceItemAdd?&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&tcpCallBack=jQuery111307849840587005019_1440779729870&orderId=.&calculationUsage=-1%2C-2%2C-5%2C-6%2C-7&catEntryId={catEntryId}&quantity=1&requesttype=ajax&externalId={WishlistID}&_=1440779729880",
			"TargetFrame=",
			"Resource=0",
			"RecContentType=text/html",
			"Referer=",
			"Snapshot=t21.inf",
			"Mode=HTML",
			"LAST");

		lr_end_transaction("T19_Wishlist Add To Cart", 2);

	}

}

void wlDeleteItem()
{
	wlView();
	wlGetAll();  
	wlGetItems();

 	lr_think_time ( LINK_TT ) ;
	 
	 
	 
	if ( lr_paramarr_len("catEntryIds") > 0 ) {  

		lr_save_string(lr_paramarr_random("wishListItemIds"), "wishListItemId");

		lr_start_transaction("T19_Wishlist Delete Item");

		web_url("AjaxGiftListServiceUpdateItem",  
			"URL=http://{host}/webapp/wcs/stores/servlet/AjaxGiftListServiceUpdateItem?&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&tcpCallBack=jQuery111307849840587005019_1440779729870&giftListId={WishlistID}&giftListItemId={wishListItemId}&quantity=0&_=1440779729876",
			"TargetFrame=",
			"Resource=0",
			"RecContentType=text/html",
			"Referer=",
			"Snapshot=t21.inf",
			"Mode=HTML",
			"LAST");

		lr_end_transaction("T19_Wishlist Delete Item", 2);

	}

}

void wlGetCatEntryId()
{
	wlView();
	wlGetAll();  
	wlGetItems();

 	lr_think_time ( LINK_TT ) ;
}

void wlGetItems()
{
	wlView();

 	lr_think_time ( LINK_TT ) ;

	web_reg_save_param("catEntryIds", "LB=\"itemId\": \"", "RB=\"", "NotFound=Warning", "Ord=All", "LAST");
	web_reg_save_param("wishListItemIds", "LB=\"wishListItemId\": \"", "RB=\"", "NotFound=Warning", "Ord=All", "LAST");

	 
	 
	 

	lr_start_transaction("T19_Wishlist Get Items");

	web_url("TCPGetWishListItemsForSelectedListView",  
		"URL=http://{host}/webapp/wcs/stores/servlet/TCPGetWishListItemsForSelectedListView?&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&wishListId={WishlistID}&tcpCallBack=jQuery111308157584751024842_1440519437271&_=1440519437287",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Referer=",
		"Snapshot=t21.inf",
		"Mode=HTML",
		"LAST");

	lr_end_transaction("T19_Wishlist Get Items", 2);

}

void wlGetAll()
{
	if (lr_paramarr_len("WishlistIDs") == 0)  { 
	 
		web_reg_save_param("WishlistIDs", "LB=\"giftListExternalIdentifier\": ", "RB=,", "ORD=All", "NotFound=Warning", "LAST");
	 
	 
	 

		 
		 
		 
		lr_start_transaction("T19_Wishlist Get List");

			TCPGetWishListForUsersView();
		
			if (lr_paramarr_len("WishlistIDs") == 0)  { 
		
			web_reg_save_param("WishlistIDs", "LB=\"giftListId\": ", "RB=,", "ORD=All", "NotFound=Warning", "LAST");
		 
			web_url("AjaxGiftListServiceCreate",
				"URL=http://{host}/webapp/wcs/stores/servlet/AjaxGiftListServiceCreate?tcpCallBack=jQuery111309054115856997669_1446156866892&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&name=Joe%27s+Wishlist&_=1446156866894",
				"TargetFrame=",
				"Resource=0",
				"RecContentType=text/html",
				"Referer=",
				"Snapshot=t21.inf",
				"Mode=HTML",
				"LAST");
				
			if (lr_paramarr_len("WishlistIDs") == 0)   
				lr_end_transaction("T19_Wishlist Get List", 1);
			else
				lr_end_transaction("T19_Wishlist Get List", 0);	
		}
		else
			lr_end_transaction("T19_Wishlist Get List", 0);

		if (lr_paramarr_len("WishlistIDs") > 0)
			lr_save_string(lr_paramarr_random("WishlistIDs"), "WishlistID");
	}
	
} 


void wishList()
{
	int iRandomTask = 0;
 

 

 

	iRandomTask = atoi(lr_eval_string("{RANDOM_PERCENT}"));

	if (iRandomTask <= RATIO_WL_CREATE)
		wlCreate();
	else if (iRandomTask <= RATIO_WL_DELETE + RATIO_WL_CREATE)
		wlDelete();
	else if (iRandomTask <= RATIO_WL_CHANGE + RATIO_WL_CREATE + RATIO_WL_DELETE)
		wlChange();
 
 
	else if (iRandomTask <= RATIO_WL_ADD_ITEM + RATIO_WL_CREATE + RATIO_WL_DELETE + RATIO_WL_CHANGE + RATIO_WL_DELETE_ITEM)
		wlAddItem();
	else if (iRandomTask <= RATIO_WL_ADD_ITEM + RATIO_WL_CREATE + RATIO_WL_DELETE + RATIO_WL_CHANGE + RATIO_WL_DELETE_ITEM + RATIO_WL_ADD_TO_CART)
		wlAddToCart();

}

	
void dropCartTransaction()
{
	lr_start_transaction("T20_Abandon_Cart");
	lr_end_transaction("T20_Abandon_Cart", 0);
	
	 
}

void getDataPoints()
{

	lr_param_sprintf("LOGIN_PAGE_DROP", "01. LOGIN_PAGE_DROP = %d", LOGIN_PAGE_DROP);
	lr_user_data_point(lr_eval_string("{LOGIN_PAGE_DROP}"), 0);
	lr_param_sprintf("SHIP_PAGE_DROP", "01. SHIP_PAGE_DROP = %d", SHIP_PAGE_DROP);
	lr_user_data_point(lr_eval_string("{SHIP_PAGE_DROP}"), 0);
	lr_param_sprintf("BILL_PAGE_DROP", "01. BILL_PAGE_DROP = %d", BILL_PAGE_DROP);
	lr_user_data_point(lr_eval_string("{BILL_PAGE_DROP}"), 0);

	lr_param_sprintf("RATIO_CHECKOUT_LOGIN", "02. RATIO_CHECKOUT_LOGIN = %d", RATIO_CHECKOUT_LOGIN);
	lr_user_data_point(lr_eval_string("{RATIO_CHECKOUT_LOGIN}"), 0);
	lr_param_sprintf("RATIO_CHECKOUT_GUEST", "02. RATIO_CHECKOUT_GUEST = %d", RATIO_CHECKOUT_GUEST);
	lr_user_data_point(lr_eval_string("{RATIO_CHECKOUT_GUEST}"), 0);
	
	
	lr_param_sprintf("RATIO_DROP_CART", "03. RATIO_DROP_CART = %d", RATIO_DROP_CART);
	lr_user_data_point(lr_eval_string("{RATIO_DROP_CART}"), 0);
    
	lr_param_sprintf("RATIO_CHECKOUT_LOGIN_FIRST", "04. RATIO_CHECKOUT_LOGIN_FIRST = %d", RATIO_CHECKOUT_LOGIN_FIRST);
	lr_user_data_point(lr_eval_string("{RATIO_CHECKOUT_LOGIN_FIRST}"), 0);

	lr_param_sprintf("RATIO_BUILDCART_DRILLDOWN", "04. RATIO_BUILDCART_DRILLDOWN = %d", RATIO_BUILDCART_DRILLDOWN);
	lr_user_data_point(lr_eval_string("{RATIO_BUILDCART_DRILLDOWN}"), 0);
	
	lr_param_sprintf("USE_LOW_INVENTORY", "05. USE_LOW_INVENTORY = %d", USE_LOW_INVENTORY);
	lr_user_data_point(lr_eval_string("{USE_LOW_INVENTORY}"), 0);
	
	lr_param_sprintf("OFFLINE_PLUGIN", "06. OFFLINE_PLUGIN = %d", OFFLINE_PLUGIN);
	lr_user_data_point(lr_eval_string("{OFFLINE_PLUGIN}"), 0);

	lr_param_sprintf("RATIO_PROMO_APPLY", "07. RATIO_PROMO_APPLY = %d", RATIO_PROMO_APPLY);
	lr_user_data_point(lr_eval_string("{RATIO_PROMO_APPLY}"), 0);
	lr_param_sprintf("RATIO_PROMO_MULTIUSE", "07. RATIO_PROMO_MULTIUSE = %d", RATIO_PROMO_MULTIUSE);
	lr_user_data_point(lr_eval_string("{RATIO_PROMO_MULTIUSE}"), 0);
	lr_param_sprintf("RATIO_PROMO_SINGLEUSE", "07. RATIO_PROMO_SINGLEUSE = %d", RATIO_PROMO_SINGLEUSE);
	lr_user_data_point(lr_eval_string("{RATIO_PROMO_SINGLEUSE}"), 0);
	lr_param_sprintf("RATIO_REDEEM_LOYALTY_POINTS", "07. RATIO_REDEEM_LOYALTY_POINTS = %d", RATIO_REDEEM_LOYALTY_POINTS);
	lr_user_data_point(lr_eval_string("{RATIO_REDEEM_LOYALTY_POINTS}"), 0);
	
	lr_param_sprintf("RATIO_CART_MERGE", "08. RATIO_CART_MERGE = %d", RATIO_CART_MERGE);
	lr_user_data_point(lr_eval_string("{RATIO_CART_MERGE}"), 0);

	lr_param_sprintf("RATIO_REGISTER", "09. RATIO_REGISTER = %d", RATIO_REGISTER);
	lr_user_data_point(lr_eval_string("{RATIO_REGISTER}"), 0);

	lr_param_sprintf("RATIO_UPDATE_QUANTITY", "10. RATIO_UPDATE_QUANTITY = %d", RATIO_UPDATE_QUANTITY);
	lr_user_data_point(lr_eval_string("{RATIO_UPDATE_QUANTITY}"), 0);

	lr_param_sprintf("RATIO_DELETE_ITEM", "11. RATIO_DELETE_ITEM = %d", RATIO_DELETE_ITEM);
	lr_user_data_point(lr_eval_string("{RATIO_DELETE_ITEM}"), 0);
	
	lr_param_sprintf("RATIO_SELECT_COLOR", "12. RATIO_SELECT_COLOR = %d", RATIO_SELECT_COLOR);
	lr_user_data_point(lr_eval_string("{RATIO_SELECT_COLOR}"), 0);

	lr_param_sprintf("RATIO_RESERVATION_HISTORY", "13. RATIO_RESERVATION_HISTORY = %d", RATIO_RESERVATION_HISTORY);
	lr_user_data_point(lr_eval_string("{RATIO_RESERVATION_HISTORY}"), 0);
	lr_param_sprintf("RATIO_POINTS_HISTORY", "13. RATIO_POINTS_HISTORY = %d", RATIO_POINTS_HISTORY);
	lr_user_data_point(lr_eval_string("{RATIO_POINTS_HISTORY}"), 0);
	lr_param_sprintf("RATIO_ORDER_HISTORY", "13. RATIO_ORDER_HISTORY = %d", RATIO_ORDER_HISTORY);
	lr_user_data_point(lr_eval_string("{RATIO_ORDER_HISTORY}"), 0);
	lr_param_sprintf("RATIO_ORDER_STATUS", "13. RATIO_ORDER_STATUS = %d", RATIO_ORDER_STATUS);
	lr_user_data_point(lr_eval_string("{RATIO_ORDER_STATUS}"), 0);

	lr_param_sprintf("RATIO_WISHLIST", "14. RATIO_WISHLIST = %d", RATIO_WISHLIST);
	lr_user_data_point(lr_eval_string("{RATIO_WISHLIST}"), 0);
	lr_param_sprintf("RATIO_WL_CREATE", "14. RATIO_WL_CREATE = %d", RATIO_WL_CREATE);
	lr_user_data_point(lr_eval_string("{RATIO_WL_CREATE}"), 0);
	lr_param_sprintf("RATIO_WL_DELETE", "14. RATIO_WL_DELETE = %d", RATIO_WL_DELETE);
	lr_user_data_point(lr_eval_string("{RATIO_WL_DELETE}"), 0);
	lr_param_sprintf("RATIO_WL_CHANGE", "14. RATIO_WL_CHANGE = %d", RATIO_WL_CHANGE);
	lr_user_data_point(lr_eval_string("{RATIO_WL_CHANGE}"), 0);
	lr_param_sprintf("RATIO_WL_DELETE_ITEM", "14. RATIO_WL_DELETE_ITEM = %d", RATIO_WL_DELETE_ITEM);
	lr_user_data_point(lr_eval_string("{RATIO_WL_DELETE_ITEM}"), 0);
	lr_param_sprintf("RATIO_WL_ADD_ITEM", "14. RATIO_WL_ADD_ITEM = %d", RATIO_WL_ADD_ITEM);
	lr_user_data_point(lr_eval_string("{RATIO_WL_ADD_ITEM}"), 0);
	
	lr_param_sprintf("NAV_BROWSE", "15. NAV_BROWSE = %d", NAV_BROWSE);
	lr_user_data_point(lr_eval_string("{NAV_BROWSE}"), 0);
	lr_param_sprintf("NAV_SEARCH", "15. NAV_SEARCH = %d", NAV_SEARCH);
	lr_user_data_point(lr_eval_string("{NAV_SEARCH}"), 0);
	lr_param_sprintf("NAV_CLEARANCE", "15. NAV_CLEARANCE = %d", NAV_CLEARANCE);
	lr_user_data_point(lr_eval_string("{NAV_CLEARANCE}"), 0);
	lr_param_sprintf("NAV_PLACE", "15. NAV_PLACE = %d", NAV_PLACE);
	lr_user_data_point(lr_eval_string("{NAV_PLACE}"), 0);

	lr_param_sprintf("DRILL_ONE_FACET", "16. DRILL_ONE_FACET = %d", DRILL_ONE_FACET);
	lr_user_data_point(lr_eval_string("{DRILL_ONE_FACET}"), 0);
	lr_param_sprintf("DRILL_TWO_FACET", "16. DRILL_TWO_FACET = %d", DRILL_TWO_FACET);
	lr_user_data_point(lr_eval_string("{DRILL_TWO_FACET}"), 0);
	lr_param_sprintf("DRILL_THREE_FACET", "16. DRILL_THREE_FACET = %d", DRILL_THREE_FACET);
	lr_user_data_point(lr_eval_string("{DRILL_THREE_FACET}"), 0);
	lr_param_sprintf("DRILL_SUB_CATEGORY", "16. DRILL_SUB_CATEGORY = %d", DRILL_SUB_CATEGORY);
	lr_user_data_point(lr_eval_string("{DRILL_SUB_CATEGORY}"), 0);

	lr_param_sprintf("APPLY_SORT", "17. APPLY_SORT = %d", APPLY_SORT);
	lr_user_data_point(lr_eval_string("{APPLY_SORT}"), 0);

	lr_param_sprintf("APPLY_PAGINATE", "17. APPLY_PAGINATE = %d", APPLY_PAGINATE);
	lr_user_data_point(lr_eval_string("{APPLY_PAGINATE}"), 0);

	lr_param_sprintf("PDP", "18. PDP = %d", PDP);
	lr_user_data_point(lr_eval_string("{PDP}"), 0);
	lr_param_sprintf("QUICKVIEW", "18. QUICKVIEW = %d", QUICKVIEW);
	lr_user_data_point(lr_eval_string("{QUICKVIEW}"), 0);
	
	lr_param_sprintf("RATIO_STORE_LOCATOR", "19. RATIO_STORE_LOCATOR = %d", RATIO_STORE_LOCATOR);
	lr_user_data_point(lr_eval_string("{RATIO_STORE_LOCATOR}"), 0);
	lr_param_sprintf("RATIO_SEARCH_SUGGEST", "19. RATIO_SEARCH_SUGGEST = %d", RATIO_SEARCH_SUGGEST);
	lr_user_data_point(lr_eval_string("{RATIO_SEARCH_SUGGEST}"), 0);
	
	lr_param_sprintf("PRODUCT_QUICKVIEW_SERVICE", "20. PRODUCT_QUICKVIEW_SERVICE = %d", PRODUCT_QUICKVIEW_SERVICE);
	lr_user_data_point(lr_eval_string("{PRODUCT_QUICKVIEW_SERVICE}"), 0);
	lr_param_sprintf("RESERVE_ONLINE", "21. RESERVE_ONLINE = %d", RESERVE_ONLINE);
	lr_user_data_point(lr_eval_string("{RESERVE_ONLINE}"), 0);
	
	lr_param_sprintf("MOVE_FROM_CART_TO_WISHLIST", "22. MOVE_FROM_CART_TO_WISHLIST = %d", MOVE_FROM_CART_TO_WISHLIST);
	lr_user_data_point(lr_eval_string("{MOVE_FROM_CART_TO_WISHLIST}"), 0);
	
	lr_param_sprintf("MOVE_FROM_WISHLIST_TO_CART", "22. MOVE_FROM_WISHLIST_TO_CART = %d", MOVE_FROM_WISHLIST_TO_CART);
	lr_user_data_point(lr_eval_string("{MOVE_FROM_WISHLIST_TO_CART}"), 0);
	
}

	
void moveFromWishlistToCart()
{
	lr_think_time ( LINK_TT );
	
	lr_start_transaction("T24_MoveFromWishlistToCart");

		lr_start_sub_transaction("T24_S01_AjaxOrderChangeServiceItemAdd", "T24_MoveFromWishlistToCart");

		web_custom_request("T24_S01_AjaxOrderChangeServiceItemAdd", 
			"URL=http://{host}/webapp/wcs/stores/servlet/AjaxOrderChangeServiceItemAdd?tcpCallBack=jQuery111300307750510271938_1473867270738&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&orderId=.&calculationUsage=-1%2C-2%2C-5%2C-6%2C-7&catEntryId={catEntryId}&quantity=1&externalId={wishListId}&_=1473867270742", 
			"Method=GET", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Referer=http://{host}/shop/us/kids-baby-gifts-registry-wishlist", 
			"Snapshot=t16.inf", 
			"Mode=HTML", 
			"LAST");
		
		lr_end_sub_transaction("T24_S01_AjaxOrderChangeServiceItemAdd", 2);

		lr_start_sub_transaction("T24_S02_CreateCookieCmd", "T24_MoveFromWishlistToCart");

		web_custom_request("T24_S02_CreateCookieCmd", 
			"URL=http://{host}/webapp/wcs/stores/servlet/CreateCookieCmd", 
			"Method=HEAD", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Referer=http://{host}/shop/us/kids-baby-gifts-registry-wishlist", 
			"Snapshot=t17.inf", 
			"Mode=HTML", 
			"LAST");

		lr_end_sub_transaction("T24_S02_CreateCookieCmd", 2);

		createCookieCmd();

	lr_end_transaction("T24_MoveFromWishlistToCart", 2);
	
}

void moveFromCartToWishlist()
{
	lr_think_time ( LINK_TT );
	
	web_reg_save_param("WishlistIDs", "LB=\"giftListExternalIdentifier\": ", "RB=,", "ORD=All", "NotFound=Warning", "LAST");
	
	web_custom_request("moveFromCartToWishlist_TCPGetWishListForUsersView",   
			"URL=http://{host}/webapp/wcs/stores/servlet/TCPGetWishListForUsersView?tcpCallBack=jQuery1113014590106982485151_1473881708524&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&sortBy=&_=1473881708525", 
			"Method=GET", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Mode=HTML", 
			"EncType=application/x-www-form-urlencoded", 
			"LAST");
	
	viewCart();  

	if ( atoi( lr_eval_string("{orderItemIds_count}") ) != 0 && atoi( lr_eval_string("{WishlistIDs_count}") ) != 0 && atoi( lr_eval_string("{catalogIds_count}") ) != 0) {
		
		lr_save_string ( lr_paramarr_random( "orderItemIds" ) , "orderItemId" ) ;
		lr_save_string ( lr_paramarr_idx( "WishlistIDs" , 1 ) , "wishListId" ) ;
		lr_save_string ( lr_paramarr_idx( "catalogIds" , 1 ) , "catEntryId" ) ;

		lr_start_transaction("T23_MoveFromCartToWishlist");
	
			lr_start_sub_transaction("T23_S01_AjaxOrderChangeServiceItemDelete", "T23_MoveFromCartToWishlist");

			web_submit_data("T23_S01_AjaxOrderChangeServiceItemDelete", 
				"Action=https://{host}/webapp/wcs/stores/servlet/AjaxOrderChangeServiceItemDelete", 
				"Method=POST", 
				"RecContentType=application/json", 
				"Mode=HTML", 
				"ITEMDATA", 
				"Name=storeId", "Value={storeId}", "ENDITEM", 
				"Name=catalogId", "Value={catalogId}", "ENDITEM", 
				"Name=langId", "Value=-1", "ENDITEM", 
				"Name=orderId", "Value={orderId}", "ENDITEM", 
				"Name=orderItemId", "Value={orderItemId}", "ENDITEM", 
				"Name=visitorId", "Value=[CS]v1|2BECB4F385078A07-4000010DC0038E68[CE]", "ENDITEM", 
				"Name=calculationUsage", "Value=-1,-2,-5,-6,-7", "ENDITEM", 
				"Name=requesttype", "Value=ajax", "ENDITEM", 
				"LAST");
				
			lr_end_sub_transaction("T23_S01_AjaxOrderChangeServiceItemDelete", 2);
		
			lr_start_sub_transaction("T23_S02_AjaxGiftListServiceAddItem", "T23_MoveFromCartToWishlist");
			
			web_custom_request("T23_S02_AjaxGiftListServiceAddItem", 
				"URL=https://{host}/webapp/wcs/stores/servlet/AjaxGiftListServiceAddItem?tcpCallBack=jQuery111303919879446517399_1473869598542&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&giftListId={wishListId}&catEntryId_1={catEntryId}&quantity_1=1&_=1473869598545", 
				"Method=GET", 
				"Resource=0", 
				"RecContentType=text/html", 
				"Mode=HTML", 
				"EncType=application/x-www-form-urlencoded", 
				"LAST");
		
			lr_end_sub_transaction("T23_S02_AjaxGiftListServiceAddItem", 2);

			lr_start_sub_transaction("T23_S03_CreateCookieCmd", "T23_MoveFromCartToWishlist");

			web_custom_request("T23_S03_CreateCookieCmd", 
				"URL=https://{host}/webapp/wcs/stores/servlet/CreateCookieCmd?langId=-1&storeId={storeId}&goToPage=NoURL&catalogId={catalogId}", 
				"Method=GET", 
				"Resource=0", 
				"RecContentType=text/html", 
				"Mode=HTML", 
				"EncType=application/x-www-form-urlencoded", 
				"LAST");
		
			lr_end_sub_transaction("T23_S03_CreateCookieCmd", 2);

			createCookieCmd();

			lr_start_sub_transaction("T23_S04_TCPMiniShopCartDisplayView1", "T23_MoveFromCartToWishlist");
			
			web_submit_data("T23_S04_TCPMiniShopCartDisplayView1", 
				"Action=https://{host}/shop/TCPMiniShopCartDisplayView1?catalogId={catalogId}&langId=-1&storeId={storeId}", 
				"Method=POST", 
				"RecContentType=text/html", 
				"Mode=HTML", 
				"ITEMDATA", 
				"Name=showredEye", "Value=true", "ENDITEM", 
				"Name=objectId", "Value=", "ENDITEM", 
				"Name=requesttype", "Value=ajax", "ENDITEM", 
				"LAST");
		
			lr_end_sub_transaction("T23_S04_TCPMiniShopCartDisplayView1", 2);

	 
		
			lr_start_sub_transaction("T23_S05_ShopCartDisplayView", "T23_MoveFromCartToWishlist");
			
			web_submit_data("T23_S05_ShopCartDisplayView", 
				"Action=https://{host}/shop/ShopCartDisplayView?shipmentType=single&catalogId={catalogId}&langId=-1&storeId={storeId}", 
				"Method=POST", 
				"RecContentType=text/html", 
				"Mode=HTML", 
				"ITEMDATA", 
				"Name=showredEye", "Value=true", "ENDITEM", 
				"Name=objectId", "Value=", "ENDITEM", 
				"Name=requesttype", "Value=ajax", "ENDITEM", 
				"LAST");
			lr_end_sub_transaction("T23_S05_ShopCartDisplayView", 2);
	
		lr_end_transaction("T23_MoveFromCartToWishlist",2);
	}

 
	
}	

int getPromoCode() {  
    
    int row, rc, loopCtr = 0;
 






    char FieldValue[50];
    int totalCoupon = lr_get_attrib_long("SinglePromoCodeCount");
	char  *VtsServer = "10.56.29.36";
	int   nPort = 8888;
 	





    pvci = vtc_connect(VtsServer,nPort,0x01);
 
        
 
	
	while (pvci != 0) {
   		
        rNum = rand() % totalCoupon + 1;
        loopCtr++;
        vtc_query_row(pvci, rNum, &colnames, &rowdata);
		 
		 
        
        if ( strcmp(rowdata[0], "") != 0) {  

        	lr_save_string(rowdata[0], "promocode");  
 
        	vtc_clear_row(pvci, rNum, &updateStatus);  

            break;
        }
    }

    if ( strcmp(lr_eval_string("{promocode}"), "") == 0) {
        lr_error_message("Single-Use promo code out of data.");
 
    }
	    vtc_free_list(colnames);
	    vtc_free_list(rowdata);
		rc = vtc_disconnect( pvci );

    return 0;
    
}

void newsLetterSignup()
{
	lr_think_time ( LINK_TT ) ;
	
	web_set_sockets_option("SSL_VERSION", "TLS1.1");

	lr_start_transaction("T26_NewsLetterSignup");

		lr_start_sub_transaction("T26_NewsLetterSignup_S01_email-subscribe", "T26_NewsLetterSignup");

		web_url("email-subscribe", 
			"URL=https://{host}/shop/us/content/email-subscribe", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Referer=", 
			"Snapshot=t3.inf", 
			"Mode=HTML", 
			"LAST");
			
		lr_end_sub_transaction("T26_NewsLetterSignup_S01_email-subscribe", 2);
		
		lr_start_sub_transaction("T26_NewsLetterSignup_S02_TCPAjaxEmailVerificationCmd", "T26_NewsLetterSignup");

		web_submit_data("TCPAjaxEmailVerificationCmd", 
			"Action=https://{host}/webapp/wcs/stores/servlet/TCPAjaxEmailVerificationCmd", 
			"Method=POST", 
			"RecContentType=application/json", 
			"Referer=https://{host}/shop/us/content/email-subscribe", 
			"Snapshot=t7.inf", 
			"Mode=HTML", 
			"EncodeAtSign=YES", 
			"ITEMDATA", 
			"Name=email", "Value={emailVerification}@gmail.com", "ENDITEM", 
			"Name=page", "Value=newsletterSignUp", "ENDITEM", 
			"Name=requesttype", "Value=ajax", "ENDITEM", 
			"LAST");

		lr_end_sub_transaction("T26_NewsLetterSignup_S02_TCPAjaxEmailVerificationCmd", 2);

 

		lr_start_sub_transaction("T26_NewsLetterSignup_S03_AddEmailCmd", "T26_NewsLetterSignup");

		web_submit_data("AddEmailCmd", 
			"Action=https://{host}/shop/us/content/AddEmailCmd", 
			"Method=POST", 
			"RecContentType=text/html", 
			"Referer=https://{host}/shop/us/content/email-subscribe", 
			"Snapshot=t8.inf", 
			"Mode=HTML", 
			"EncodeAtSign=YES", 
			"ITEMDATA", 
			"Name=emailaddr", "Value={emailVerification}@gmail.com", "ENDITEM", 
			"Name=storeId", "Value={storeId}", "ENDITEM", 
			"Name=catalogId", "Value={catalogId}", "ENDITEM", 
			"Name=langId", "Value=-1", "ENDITEM", 
			"Name=URL", "Value=email-confirmation", "ENDITEM", 
			"Name=response", "Value=valid::false:false", "ENDITEM", 
			"LAST");

		lr_end_sub_transaction("T26_NewsLetterSignup_S03_AddEmailCmd", 2);

	lr_end_transaction("T26_NewsLetterSignup", 2);
}

void createCookieCmd()
{
	lr_start_transaction("T28_CreateCookieCmd");

	web_custom_request("CreateCookieCmd", 
		"URL=https://{host}/webapp/wcs/stores/servlet/CreateCookieCmd?langId=-1&storeId={storeId}&goToPage=NoURL&catalogId={catalogId}", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Mode=HTML", 
		"EncType=application/x-www-form-urlencoded", 
		"LAST");

	lr_end_transaction("T28_CreateCookieCmd", 2);
		
}
# 2 "vuser_init.c" 2

	
vuser_init()
{	
	
	lr_save_string ( lr_get_attrib_string ( "TestEnvironment") , "host" ) ;
	lr_save_string(lr_get_attrib_string ("BuildCart"), "buildCart");

	lr_save_string ( "0" , "lastvalue");
	lr_save_string ( "0" , "startATC");
	lr_save_string ( lr_eval_string("{elapsedTime}") , "startTime");
	start_time = atoi(lr_eval_string("{startTime}"));
	target_time = start_time + 20;
	lr_save_string("scriptOrigin", "CA");
	
	return 0;
}
# 4 "e:\\performance\\scripts\\2017\\03_perf\\ecommerce\\ca_checkout\\\\combined_CA_Checkout.c" 2

# 1 "Action.c" 1
Action()
{
	web_set_max_html_param_len ( "3072" ) ;
	web_cleanup_cookies ( ) ;
	isLoggedIn=0;

 
 
 
	lr_save_string("10152", "storeId");
	lr_save_string("10552", "catalogId");
	
	if ( strcmp(strupr(lr_eval_string("{buildCart}")),  "TRUE") == 0) {
		RATIO_CHECKOUT_GUEST = 0;	
		RATIO_CHECKOUT_LOGIN_FIRST = 100;
		RATIO_DROP_CART=100;
		PRODUCT_QUICKVIEW_SERVICE = 0;
	}
	lr_save_string( lr_eval_string("{logonid}"), "userEmail" );
	
 

	viewHomePage();

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_DROP_CART ) {
		
		dropCart();
	}
	else {
	
		checkOut();
	}
	
	return 0;
}
# 5 "e:\\performance\\scripts\\2017\\03_perf\\ecommerce\\ca_checkout\\\\combined_CA_Checkout.c" 2

# 1 "dropCart.c" 1
dropCart()
{
	lr_user_data_point("Abandon Cart Rate", 1);
	lr_user_data_point("Checkout Rate", 0);
	lr_save_string("dropCartFlow", "currentFlow");
	
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CHECKOUT_GUEST ) {
		
		dropCartGuest();
		
	} 

	else {
		
		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CHECKOUT_LOGIN_FIRST ) {  
        
			dropCartLoginFirst();
            
        }
        else { 

			dropCartBuildCartFirst();
		} 
	
	}
	
	return 0;
}

dropCartGuest()
{
	lr_save_string("1", "totalNumberOfItems_count");
	
	buildCartDrop(0);

 
		viewCart(); 
	
	deleteItem(); 
	updateQuantity(); 

	inCartEdits();

	proceedToCheckout();

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= LOGIN_PAGE_DROP )
	{		dropCartTransaction();
			return 0;
	}
	viewCart(); 
	proceedAsGuest();
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= SHIP_PAGE_DROP )
	{		dropCartTransaction();
			return 0;
	}

	selectShipMode();
	submitShippingAddressAsGuest();
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= BILL_PAGE_DROP )
	{	dropCartTransaction();
		return 0;
	}

	selectBillingAddress();       
	 
	submitBillingAddressAsGuest();

	dropCartTransaction();
	
	return 0;		
}

dropCartLoginFirst()
{
	loginFromHomePage();
    
    viewCartFromLogin();

	buildCartDrop(1);

 
		viewCart(); 
	
	deleteItem(); 


	if ( strcmp(strupr(lr_eval_string("{buildCart}")),  "TRUE") != 0) {
		inCartEdits();
        
 
 
        
        proceedToCheckout_ShippingView();        

        if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= SHIP_PAGE_DROP )
		{	dropCartTransaction();
			return 0;
		}
 
        if (selectShippingAddress() == 0 ) {
        	 
 
            submitShippingAddress();
    
            if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= BILL_PAGE_DROP )
			{		dropCartTransaction();
					return 0;
			}

            if (selectBillingAddress() == 0 ) {
             
	            submitBillingAddress();
	    
				dropCartTransaction();

            } 
        } 
	}
	
	return 0;            
	
}

dropCartBuildCartFirst()
{
	lr_save_string("1", "totalNumberOfItems_count");
	
	buildCartDrop(0);

 
		viewCart(); 
	
	deleteItem(); 

	inCartEdits();
    
    proceedToCheckout();
 
    if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= LOGIN_PAGE_DROP )
	{	dropCartTransaction();
		return 0;
	}

    login();

    if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= SHIP_PAGE_DROP )
	{	dropCartTransaction();
		return 0;
	}

 
    if (selectShippingAddress() == 0 ) {
    	 
 
        submitShippingAddress();

        if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= BILL_PAGE_DROP )
		{		dropCartTransaction();
				return 0;
		}

        if (selectBillingAddress() == 0 ) {
         
            submitBillingAddress();
    
			dropCartTransaction();

        } 
    } 
	
	return 0;
}
# 6 "e:\\performance\\scripts\\2017\\03_perf\\ecommerce\\ca_checkout\\\\combined_CA_Checkout.c" 2

# 1 "checkOut.c" 1
checkOut()
{
	lr_user_data_point("Abandon Cart Rate", 0);
	lr_user_data_point("Checkout Rate", 1);
	lr_save_string("checkoutFlow", "currentFlow");

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CHECKOUT_GUEST ) {
		
		checkoutGuest();
		
	} 
	else {
		
  		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CHECKOUT_LOGIN_FIRST ) {  
            
			checkoutLoginFirst();
            
        }
        else { 
            
			checkoutBuildCartFirst();
        } 

	} 

	return 0;
}

checkoutGuest()
{
	lr_save_string("1", "totalNumberOfItems_count");
	
	buildCartCheckout(0);

	viewCart(); 

	inCartEdits();
		
	proceedToCheckout();

	proceedAsGuest();

	selectShipMode();
	submitShippingAddressAsGuest();

	selectBillingAddress();       
	 
	submitBillingAddressAsGuest();

	submitOrder(); 
	 

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_REGISTER ) 
		registerUser();
		
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_STORE_LOCATOR ) { 
		StoreLocator();
	}
	
	return 0;
}

checkoutLoginFirst()
{
    loginFromHomePage(); 
    
    viewCartFromLogin();
	viewCart(); 

	buildCartCheckout(1);

	inCartEdits();
    
    proceedToCheckout_ShippingView();    
    
    if (selectShippingAddress() == 0 ) {
    	 
 
        submitShippingAddress();

        if (selectBillingAddress() == 0 ) {
         
            if (submitBillingAddress() == 0 ) {
             
    
	            submitOrder();

	            if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 20 ) {
	            	viewOrderHistory();
	            	
	                if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ORDER_STATUS ) 
	                    viewOrderStatus();
	  
	            	viewReservationHistory();
		            
					if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_POINTS_HISTORY ) {
						viewPointsHistory();
					}
	            }
         	} 






        } 
    } 

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_STORE_LOCATOR ) { 
		StoreLocator();
	}
    
	forgetPassword();	
    
   return 0;
}

checkoutBuildCartFirst()
{
	lr_save_string("1", "totalNumberOfItems_count");
	
	buildCartCheckout(0);

	inCartEdits();
    
    proceedToCheckout();

    login();
    
    if (selectShippingAddress() == 0 ) {
    	 
 
        submitShippingAddress();

        if (selectBillingAddress() == 0 ) {
         
            if (submitBillingAddress() == 0 ) {
             
    
	            submitOrder();
	            
	            if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 20 ) {
	            	viewOrderHistory();
	            	
	                if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ORDER_STATUS ) 
	                    viewOrderStatus();
	  
	            	viewReservationHistory();
	
	            	if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_POINTS_HISTORY ) {
						viewPointsHistory();
					}
	            }
         	} 






        } 
    } 

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_STORE_LOCATOR ) { 
		StoreLocator();
	}
	return 0;
}
# 7 "e:\\performance\\scripts\\2017\\03_perf\\ecommerce\\ca_checkout\\\\combined_CA_Checkout.c" 2

# 1 "vuser_end.c" 1
vuser_end()
{
	return 0;
}
# 8 "e:\\performance\\scripts\\2017\\03_perf\\ecommerce\\ca_checkout\\\\combined_CA_Checkout.c" 2

