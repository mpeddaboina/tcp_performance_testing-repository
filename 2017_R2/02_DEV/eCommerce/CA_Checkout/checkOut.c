checkOut()
{
	lr_user_data_point("Abandon Cart Rate", 0);
	lr_user_data_point("Checkout Rate", 1);
	lr_save_string("checkoutFlow", "currentFlow");

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CHECKOUT_GUEST ) {
		
		checkoutGuest();
		
	} 
	else {
		
  		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CHECKOUT_LOGIN_FIRST ) { //login first
            
			checkoutLoginFirst();
            
        }
        else { 
            
			checkoutBuildCartFirst();
        } 

	} 

	return 0;
}

checkoutGuest()
{
	lr_save_string("1", "totalNumberOfItems_count");
	
	buildCartCheckout(0);

	viewCart();//12/04 to add more ordercalculate

	inCartEdits();
		
	proceedToCheckout();

	proceedAsGuest();

	selectShipMode();
	submitShippingAddressAsGuest();

	selectBillingAddress();      //0113 - to see if it is the guest who is making the failures
	//selectBillingAddressAsGuest(); //0113 - to see if it is the guest who is making the failures
	submitBillingAddressAsGuest();

	submitOrder();//0113 - to see if it is the guest who is making the failures
	//submitOrderGuest();//0113 - to see if it is the guest who is making the failures

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_REGISTER ) 
		registerUser();
		
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_STORE_LOCATOR ) { 
		StoreLocator();
	}
	
	return 0;
}

checkoutLoginFirst()
{
    loginFromHomePage(); 
    
    viewCartFromLogin();
	viewCart();//12/04 to add more ordercalculate

	buildCartCheckout(1);

	inCartEdits();
    
    proceedToCheckout_ShippingView();    
    
    if (selectShippingAddress() == LR_PASS ) {
    	//selectShippingAddress();
//            selectShipMode(); // see if it still works without this 02/22
        submitShippingAddress();

        if (selectBillingAddress() == LR_PASS ) {
        //	selectBillingAddress();
            if (submitBillingAddress() == LR_PASS ) {
            //submitBillingAddress();
    
	            submitOrder();

	            if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 20 ) {
	            	viewOrderHistory();
	            	
	                if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ORDER_STATUS ) 
	                    viewOrderStatus();
	  
	            	viewReservationHistory();
		            
					if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_POINTS_HISTORY ) {
						viewPointsHistory();
					}
	            }
/*	            else if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= (RATIO_ORDER_HISTORY + RATIO_POINTS_HISTORY) ) {
	            	viewOrderHistory();
	//            	lr_message("RATIO_ORDER_STATUS: %d", RATIO_ORDER_STATUS);
	                if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ORDER_STATUS ) 
	                    viewOrderStatus();
            	}//if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ORDER_HISTORY ) {
*/        	}//if (submitBillingAddress() == LR_PASS ) {
        }//if (selectBillingAddress() == LR_PASS ) {
    }//if (selectShippingAddress() == LR_PASS ) {

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_STORE_LOCATOR ) { 
		StoreLocator();
	}
    
	forgetPassword();	
    
   return 0;
}

checkoutBuildCartFirst()
{
	lr_save_string("1", "totalNumberOfItems_count");
	
	buildCartCheckout(0);

	inCartEdits();
    
    proceedToCheckout();

    login();
    
    if (selectShippingAddress() == LR_PASS ) {
    	//selectShippingAddress();
//            selectShipMode(); // see if it still works without this 02/22
        submitShippingAddress();

        if (selectBillingAddress() == LR_PASS ) {
        //	selectBillingAddress();
            if (submitBillingAddress() == LR_PASS ) {
            //submitBillingAddress();
    
	            submitOrder();
	            
	            if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 20 ) {
	            	viewOrderHistory();
	            	
	                if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ORDER_STATUS ) 
	                    viewOrderStatus();
	  
	            	viewReservationHistory();
	
	            	if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_POINTS_HISTORY ) {
						viewPointsHistory();
					}
	            }
/*				else if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= (RATIO_ORDER_HISTORY + RATIO_POINTS_HISTORY) ) {
	            	viewOrderHistory();
	//            	lr_message("RATIO_ORDER_STATUS: %d", RATIO_ORDER_STATUS);
	                if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ORDER_STATUS ) 
	                    viewOrderStatus();
            	}//if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ORDER_HISTORY ) {
*/        	}//if (submitBillingAddress() == LR_PASS ) {
        }//if (selectBillingAddress() == LR_PASS ) {
    }//if (selectShippingAddress() == LR_PASS ) {

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_STORE_LOCATOR ) { 
		StoreLocator();
	}
	return 0;
}