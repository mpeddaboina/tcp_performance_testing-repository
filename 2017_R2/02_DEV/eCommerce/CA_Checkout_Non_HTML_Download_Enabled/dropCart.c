dropCart()
{
	lr_user_data_point("Abandon Cart Rate", 1);
	lr_user_data_point("Checkout Rate", 0);
	lr_save_string("dropCartFlow", "currentFlow");
	
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CHECKOUT_GUEST ) {
		
		dropCartGuest();
		
	} 

	else {
		
		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CHECKOUT_LOGIN_FIRST ) { //login first
        
			dropCartLoginFirst();
            
        }
        else { 

			dropCartBuildCartFirst();
		} 
	
	}
	
	return 0;
}

dropCartGuest()
{
	lr_save_string("1", "totalNumberOfItems_count");
	
	buildCartDrop(0);

	inCartEdits();

	proceedToCheckout();

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= LOGIN_PAGE_DROP )
	{		dropCartTransaction();
			return 0;
	}
	viewCart();//0527
	proceedAsGuest();
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= SHIP_PAGE_DROP )
	{		dropCartTransaction();
			return 0;
	}

	selectShipMode();
	submitShippingAddressAsGuest();
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= BILL_PAGE_DROP )
	{	dropCartTransaction();
		return 0;
	}

	selectBillingAddress();      //0113 - to see if it is the guest who is making the failures
	//selectBillingAddressAsGuest(); //0113 - to see if it is the guest who is making the failures
	submitBillingAddressAsGuest();

	dropCartTransaction();
	
	return 0;		
}

dropCartLoginFirst()
{
	loginFromHomePage();
    
    viewCartFromLogin();

	buildCartDrop(1);

	if ( strcmp(strupr(lr_eval_string("{buildCart}")),  "TRUE") != 0) {
		inCartEdits();
        
//            if ( atoi(lr_eval_string("{RANDOM_WL_PERCENT}")) <= RATIO_WISHLIST ) 
//                wishList();
        
        proceedToCheckout_ShippingView();        

        if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= SHIP_PAGE_DROP )
		{	dropCartTransaction();
			return 0;
		}
//=======================================================================
        if (selectShippingAddress() == LR_PASS ) {
        	//selectShippingAddress();
//            selectShipMode(); // see if it still works without this 02/22
            submitShippingAddress();
    
            if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= BILL_PAGE_DROP )
			{		dropCartTransaction();
					return 0;
			}

            if (selectBillingAddress() == LR_PASS ) {
            //	selectBillingAddress();
	            submitBillingAddress();
	    
				dropCartTransaction();

            }//if (selectBillingAddress() == LR_PASS ) {
        }//if (selectShippingAddress() == LR_PASS ) {
	}
	
	return 0;            
	
}

dropCartBuildCartFirst()
{
	lr_save_string("1", "totalNumberOfItems_count");
	
	buildCartDrop(0);

	inCartEdits();
    
    proceedToCheckout();
 
    if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= LOGIN_PAGE_DROP )
	{	dropCartTransaction();
		return 0;
	}

    login();

    if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= SHIP_PAGE_DROP )
	{	dropCartTransaction();
		return 0;
	}

//=======================================================================
    if (selectShippingAddress() == LR_PASS ) {
    	//selectShippingAddress();
//            selectShipMode(); // see if it still works without this 02/22
        submitShippingAddress();

        if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= BILL_PAGE_DROP )
		{		dropCartTransaction();
				return 0;
		}

        if (selectBillingAddress() == LR_PASS ) {
        //	selectBillingAddress();
            submitBillingAddress();
    
			dropCartTransaction();

        }//if (selectBillingAddress() == LR_PASS ) {
    }//if (selectShippingAddress() == LR_PASS ) {
	
	return 0;
}