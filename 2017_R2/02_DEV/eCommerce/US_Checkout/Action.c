Action()
{
	web_set_max_html_param_len ( "3072" ) ;
	web_cleanup_cookies ( ) ;
	isLoggedIn=0;
	
	// Temp Debugging Parameters
//	RATIO_PROMO_MULTIUSE = 0;
//	RATIO_PROMO_SINGLEUSE = 100;
	RATIO_DROP_CART = 0; //45;
	RATIO_CHECKOUT_GUEST = 0; 
	RATIO_CHECKOUT_LOGIN_FIRST = 100;
//	RATIO_POINTS_HISTORY = 0;
//	RATIO_ORDER_HISTORY = 0;
//	RATIO_REDEEM_LOYALTY_POINTS = 100;
//	getPromoCode();		// Using VTS this works, just need to get new coupon codes
	//getDataPoints();
//	lr_save_string("10151", "storeId");
//	lr_save_string("10551", "catalogId");
	if ( strcmp(strupr(lr_eval_string("{buildCart}")),  "TRUE") == 0) {
		RATIO_CHECKOUT_GUEST = 0;	
		RATIO_CHECKOUT_LOGIN_FIRST = 100;
		RATIO_DROP_CART=100;
		PRODUCT_QUICKVIEW_SERVICE = 0;
	}
	
	lr_save_string( lr_eval_string("{logonid}"), "userEmail" );
	
	viewHomePage();
	
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_DROP_CART ) {
		
		dropCart();
	}
	else {
		
		checkOut(); 
		
	}
	
	return 0;

}
