int CART_PAGE_DROP, LOGIN_PAGE_DROP, SHIP_PAGE_DROP, BILL_PAGE_DROP, REVIEW_PAGE_DROP; //Checkout Funnel ratio for DropCart Scripts.
//int LINK_TT, FORM_TT, TYPE_SPEED; //Think Time
int RATIO_CHECKOUT_GUEST, RATIO_CHECKOUT_LOGIN, RATIO_CHECKOUT_LOGIN_FIRST; // User Type
int OFFLINE_PLUGIN, VISA, MASTER, AMEX, PLCC, GIFT, DISCOVER; // Payment Method
int RATIO_PROMO_APPLY, RATIO_PROMO_MULTIUSE, RATIO_PROMO_SINGLEUSE, RATIO_PROMO_COUPON_REMOVE, RATIO_REDEEM_LOYALTY_POINTS; // Promo Type 
int RATIO_REGISTER, RATIO_UPDATE_QUANTITY, RATIO_DELETE_ITEM, RATIO_SELECT_COLOR, RATIO_ORDER_HISTORY, RATIO_POINTS_HISTORY, RATIO_RESERVATION_HISTORY, RATIO_RESERVATION_WISHLIST;// Registration, Update_Quantity, Delete_Item from cart
int RATIO_CART_MERGE, RATIO_DROP_CART, RATIO_WISHLIST, cartMerge, currentCartSize; RATIO_BUILDCART_DRILLDOWN, USE_LOW_INVENTORY;
int	RATIO_WL_VIEW, RATIO_WL_CREATE,	RATIO_WL_DELETE, RATIO_WL_CHANGE, RATIO_WL_DELETE_ITEM, RATIO_WL_ADD_ITEM, RATIO_WL_ADD_TO_CART;
int MOVE_FROM_CART_TO_WISHLIST, MOVE_FROM_WISHLIST_TO_CART;
  
	//set drop point ratio for checkout funnel. Sum must be 100. All variables must either 0 or +ve value.
	// Reconciled with CM access log checkout funnel
	LOGIN_PAGE_DROP = 0; //30; //11;
	SHIP_PAGE_DROP = 0; //30; //46;
	BILL_PAGE_DROP = 80; //27; //23; //13;
	//REVIEW_PAGE_DROP = 100;
	
/*  reconciled with CM Omniture page detail report	checkout funnel
	LOGIN_PAGE_DROP = 0; //30; //11;
	SHIP_PAGE_DROP = 60; //30; //46;
	BILL_PAGE_DROP = 30; //27; //23; //13;
	//REVIEW_PAGE_DROP = 100;
*/

/*   before CM 2016
	LOGIN_PAGE_DROP = 11;
	SHIP_PAGE_DROP = 46;
	BILL_PAGE_DROP = 23; //13;
	//REVIEW_PAGE_DROP = 100;
*/
	//set user checkout type T10 transaction. Sum must be 100. All variables must have either 0 or +ve value.
//	RATIO_CHECKOUT_LOGIN = 89; //12/04 10pm
//	RATIO_CHECKOUT_GUEST = 11; //12/04 10pm
	RATIO_CHECKOUT_LOGIN = 90; //70;
	RATIO_CHECKOUT_GUEST = 10; //30;

	//Ratio for Drop Cart vusers
	RATIO_DROP_CART = 60; //50;	

    //set users checkout that starts with login flow
    RATIO_CHECKOUT_LOGIN_FIRST = 70; //50;

    //sets a percent of users to drilldown before selecting a product when building the cart.
    RATIO_BUILDCART_DRILLDOWN = 20; //100; //25;
	
	USE_LOW_INVENTORY = 5;
	
	//PayMethod
	OFFLINE_PLUGIN = 100;
	VISA = 0;
	MASTER = 0;
	AMEX = 0;
	PLCC = 0;
	GIFT = 0;
	DISCOVER = 0;

	//PromoType
	RATIO_PROMO_APPLY = 35; //70; 
	RATIO_PROMO_MULTIUSE = 95; 
	RATIO_PROMO_SINGLEUSE = 5; 
	RATIO_REDEEM_LOYALTY_POINTS = 15;
	
	/*
	Follwing variables values change for each script and therefore are defined as a Parameter in the script.
	RATIO_AVG_CART_SIZE
	*/
	//Ratio for Cart Merge
	RATIO_CART_MERGE = 0; //50;	

//% of Guest user completing checkout that also register
	RATIO_REGISTER = 100;

	//Ratio for Update Quantity
	RATIO_UPDATE_QUANTITY = 100;

	//Ratio for Delete an item from cart
	RATIO_DELETE_ITEM = 100; //60;
	
	//Ratio for Selecting a Color
	RATIO_SELECT_COLOR = 50;
	///RATIO_ORDER_HISTORY + RATIO_POINTS_HISTORY + RATIO_RESERVATION_HISTORY // All this should be total 100
	//Ratio for Order history / View Order Detail / Reservation
	
	RATIO_RESERVATION_HISTORY = 10; // This should eventually be only 1.25%
	RATIO_POINTS_HISTORY = 30; //50;
	RATIO_ORDER_HISTORY = 60; //50;

	RATIO_ORDER_STATUS = 25; //100; //50;

	//Ratio for Wishlist
	RATIO_WISHLIST		 = 50; //10;

	RATIO_WL_CREATE      = 60; //5;
	RATIO_WL_DELETE      = 2; //5;
	RATIO_WL_CHANGE      = 3; //5;
	RATIO_WL_DELETE_ITEM = 5;
	RATIO_WL_ADD_ITEM    = 1; //25; //1;
	RATIO_WL_ADD_TO_CART = 25; //1;

	MOVE_FROM_CART_TO_WISHLIST = 100; //25;
	MOVE_FROM_WISHLIST_TO_CART = 100; //30;
	