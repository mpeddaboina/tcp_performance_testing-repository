#include "..\..\checkoutWorkloadModel.c"

int index , length , i, target_itemsInCart, index_buildCart, randomPercent, isLoggedIn;
int isLoggedIn=0;
char *searchString ;
char *nav_by ; // T02 - Category Navigation selection
char *drill_by ; // T03 - Facet / SubCategory drill selection
char *sort_by; // T03 - Sort selection
char *product_by; // T04 - Product Display Method
int atc_Stat = 0; //0-Pass 1-Fail
int HttpRetCode;
int start_time, target_time;

//VTS variables
int rNum;
unsigned short updateStatus;
char **colnames = NULL;
char **rowdata = NULL;
PVCI2 pvci = 0;

//int form_TT, link_TT, typeSpeed_TT;
//typedef long time_t;
//time_t t;
//int authcookielen , authtokenlen ;
//authcookielen = 0;

void getCatEntryID() {
//	lr_error_message("Should not come here.");

	lr_save_string(lr_eval_string("{lowQty_SKU}"), "atc_catentryId");
	
	if ( strcmp(lr_eval_string("{atc_catentryId}"), lr_eval_string("{lastvalue}") ) == 0 ) {
		lr_param_sprintf ( "atc_catentryId" , "%s" , lr_paramarr_random ( "atc_catentryIds" ) ) ;
		USE_LOW_INVENTORY = 0;
	}
	else {
		lr_save_string(lr_eval_string("{atc_catentryId}"), "lastvalue");
		lr_save_string(lr_eval_string("{atc_catentryId}"), "atc_comment");
		lr_start_transaction("T20_Low_QTY_Count");
		lr_end_transaction("T20_Low_QTY_Count", LR_AUTO);
	}
	
	return;
}

void addToCart()
{
	int k = 0, newTime, RANDOM_PERCENT = 0;
	atc_Stat = 0; //0-Pass 1-Fail

	if ( lr_paramarr_len ( "atc_catentryIds" ) > 0 ) { // at least 1 product with positive quantity check

		lr_param_sprintf ( "atc_catentryId" , "%s" , lr_paramarr_random ( "atc_catentryIds" ) ) ;
		web_reg_save_param ( "orderId" , "LB=\"orderId\": [\"" , "RB=\"]" , "NotFound=Warning",  LAST ) ;
		web_reg_save_param ( "orderItemId" , "LB=\"orderItemId\": [\"" , "RB=\"]" , "NotFound=Warning", LAST ) ;
		web_reg_find("Text=the products you wish to purchase are not available", "SaveCount=atcErrorFound");
	//	web_reg_save_param("notAvailableQTY", "LB=Your request cannot be completed, as one or more of the products you wish to purchase are ", "RB= in the quantity you requested.", "NotFound=Warning", LAST);

	//RANDOM_PERCENT = atoi(lr_eval_string("{RANDOM_PERCENT}"));
/*
		if ( atoi(lr_eval_string("{RANDOM_PERCENT}") ) <= USE_LOW_INVENTORY) {
			if ( atoi(lr_eval_string("{RANDOM_PERCENT}") ) <= USE_LOW_INVENTORY) {
				getCatEntryID();
			}
		}		
		
		newTime = atoi( lr_eval_string("{elapsedTime}"));
		// use these catentryid's at the 20th minute from scenario start, this is to test an out of stock item
		if (newTime <= 59 && target_time <=59) {
			if ( RANDOM_PERCENT <= USE_LOW_INVENTORY) {
				if ( newTime >= target_time) {
					getCatEntryID();
				}
			}
		}
		else {
			if ( (newTime + 60) <= target_time )
			{
				if ( RANDOM_PERCENT <= USE_LOW_INVENTORY) {
					if ( (newTime + 60) >= target_time ) {
						getCatEntryID();
					}
				}
			}
		}
*/
		lr_think_time ( FORM_TT ) ;
//	lr_save_string("26099", "atc_catentryId");
//	lr_save_string("26099", "atc_comment");

		lr_start_transaction ( "T05_Add To Cart" ) ;

		if ( authcookielen == 0 ) {
			web_reg_save_param_regexp ( "ParamName=authTokens" , "RegExp=WC_AUTHENTICATION_[0-9]+=([^D][^;]+);" , SEARCH_FILTERS , "Scope=Headers" , "NotFound=Warning", "Ordinal=All", LAST ) ;
			lr_start_sub_transaction ("T05_Add To Cart_S01_AjaxOrderChangeServiceItemAdd", "T05_Add To Cart" ) ;

			web_submit_data("addtocart_AjaxOrderChangeServiceItemAdd",
				"Action=https://{host}/webapp/wcs/stores/servlet/AjaxOrderChangeServiceItemAdd",
				"Method=POST",
				"RecContentType=text/html",
				"Snapshot=t25.inf",
				"Mode=HTML",
				ITEMDATA,
				"Name=storeId", "Value={storeId}", ENDITEM,
				"Name=catalogId", "Value={catalogId}", ENDITEM,
				"Name=langId", "Value=-1", ENDITEM,
				"Name=orderId", "Value=.", ENDITEM,
				"Name=field2", "Value=0", ENDITEM,
				"Name=comment", "Value={atc_comment}", ENDITEM, 
				"Name=calculationUsage", "Value=-1,-2,-5,-6,-7", ENDITEM,
				"Name=catEntryId", "Value={atc_catentryId}", ENDITEM, 
				"Name=quantity", "Value=1", ENDITEM,
				"Name=requesttype", "Value=ajax", ENDITEM,
				"Name=visitorId", "Value=[CS]v1|2B0B56810507A725-40000116E00E1B28[CE]", ENDITEM, //0802
				LAST);

			if ( atoi(lr_eval_string("{atcErrorFound}")) == 1 ) {
//				lr_output_message("The Catentry Id=%s is not available", lr_eval_string("{atc_catentryId}"));
//				lr_error_message("The Catentry Id=%s is not available.", lr_eval_string("{atc_catentryId}"));
				atc_Stat = 1; //0-Pass 1-Fail
				lr_end_sub_transaction ("T05_Add To Cart_S01_AjaxOrderChangeServiceItemAdd", LR_FAIL) ;
				lr_end_transaction ( "T05_Add To Cart" , LR_FAIL ) ;
				return;
			} // end if

			if ( strlen(lr_eval_string("{orderId}")) <= 0 ) {
				atc_Stat = 1; //0-Pass 1-Fail
				lr_end_sub_transaction ("T05_Add To Cart_S01_AjaxOrderChangeServiceItemAdd", LR_FAIL) ;
				//lr_error_message("OrderID not created for The Catentry Id=%s.", lr_eval_string("{atc_catentryId}"));
				lr_end_transaction ( "T05_Add To Cart" , LR_FAIL ) ;
				return;
			} // end if

			lr_end_sub_transaction ("T05_Add To Cart_S01_AjaxOrderChangeServiceItemAdd", LR_AUTO) ;

			if ( lr_paramarr_len( "authTokens" ) > 0 ) {
				if ( authcookielen == 0 ) {
					lr_save_string ( lr_paramarr_idx( "authTokens" , 1 ) , "authToken" ) ;
					authcookielen = 1 ;
				} //end if
			} // end if
		} // end if authcookielen==0
		else {
			lr_start_sub_transaction ("T05_Add To Cart_S01_AjaxOrderChangeServiceItemAdd", "T05_Add To Cart" ) ;

			web_submit_data("addtocart_AjaxOrderChangeServiceItemAdd",
				"Action=https://{host}/webapp/wcs/stores/servlet/AjaxOrderChangeServiceItemAdd",
				"Method=POST",
				"RecContentType=text/html",
				"Snapshot=t25.inf",
				"Mode=HTML",
				ITEMDATA,
				"Name=storeId", "Value={storeId}", ENDITEM,
				"Name=catalogId", "Value={catalogId}", ENDITEM,
				"Name=langId", "Value=-1", ENDITEM,
				"Name=orderId", "Value=.", ENDITEM,
				"Name=field2", "Value=0", ENDITEM,
				"Name=comment", "Value={atc_comment}", ENDITEM, 
				"Name=calculationUsage", "Value=-1,-2,-5,-6,-7", ENDITEM,
				"Name=catEntryId", "Value={atc_catentryId}", ENDITEM,
				"Name=quantity", "Value=1", ENDITEM,
				"Name=requesttype", "Value=ajax", ENDITEM,
				"Name=visitorId", "Value=[CS]v1|2B0B56810507A725-40000116E00E1B28[CE]", ENDITEM, //0802
				LAST);

			if ( atoi(lr_eval_string("{atcErrorFound}")) == 1 ) {
//				lr_output_message("The Catentry Id=%s is not available.", lr_eval_string("{atc_catentryId}"));
//				lr_error_message("The Catentry Id=%s is not available.", lr_eval_string("{atc_catentryId}"));
				atc_Stat = 1; //0-Pass 1-Fail
				lr_end_sub_transaction ("T05_Add To Cart_S01_AjaxOrderChangeServiceItemAdd", LR_FAIL) ;
				lr_end_transaction ( "T05_Add To Cart" , LR_FAIL ) ;
				return;
			} // end if
			
			if ( strlen(lr_eval_string("{orderId}")) <= 0 ) {
				atc_Stat = 1; //0-Pass 1-Fail
				//lr_error_message("OrderID not created for The Catentry Id=%s.", lr_eval_string("{atc_catentryId}"));
				lr_end_sub_transaction ("T05_Add To Cart_S01_AjaxOrderChangeServiceItemAdd", LR_FAIL) ;
				lr_end_transaction ( "T05_Add To Cart" , LR_FAIL ) ;
				return;
			} // end if

			lr_end_sub_transaction ("T05_Add To Cart_S01_AjaxOrderChangeServiceItemAdd", LR_AUTO) ;
		} // end else authcookielen==0

		lr_start_sub_transaction ("T05_Add To Cart_S02_TCPAdd2CartQuickView", "T05_Add To Cart" ) ;

		web_custom_request("addtocart_TCPAdd2CartQuickView",
			"URL=https://{host}/webapp/wcs/stores/servlet/TCPAdd2CartQuickView?langId=-1&storeId={storeId}&catalogId={catalogId}",
			"Method=GET",
			"Resource=0",
			"RecContentType=text/html",
			"Snapshot=t27.inf",
			"Mode=HTML",
			"EncType=application/x-www-form-urlencoded",
			LAST);

		lr_end_sub_transaction ("T05_Add To Cart_S02_TCPAdd2CartQuickView", LR_AUTO) ;

		lr_start_sub_transaction ("T05_Add To Cart_S03_CreateCookieCmd", "T05_Add To Cart" ) ;

		web_custom_request("addtocart_CreateCookieCmd",
			"URL=https://{host}/webapp/wcs/stores/servlet/CreateCookieCmd?langId=-1&storeId={storeId}&goToPage=NoURL&catalogId={catalogId}",
			"Method=GET",
			"Resource=0",
			"RecContentType=text/html",
			"Snapshot=t28.inf",
			"Mode=HTML",
			"EncType=application/x-www-form-urlencoded",
			LAST);

		lr_end_sub_transaction ("T05_Add To Cart_S03_CreateCookieCmd", LR_AUTO) ;
		
		createCookieCmd();

		lr_start_sub_transaction ("T05_Add To Cart_S04_TCPMiniShopCartDisplayView", "T05_Add To Cart" ) ;

		web_submit_data("addtocart_TCPMiniShopCartDisplayView",
			"Action=https://{host}/shop/TCPMiniShopCartDisplayView?catalogId={catalogId}&langId=-1&storeId={storeId}",
			"Method=POST",
			"RecContentType=text/html",
			"Snapshot=t29.inf",
			"Mode=HTML",
			ITEMDATA,
			"Name=addedOrderItemId", "Value={orderItemId}", ENDITEM, 
			"Name=showredEye", "Value=true", ENDITEM,
			"Name=objectId", "Value=", ENDITEM,
			"Name=requesttype", "Value=ajax", ENDITEM,
			LAST);

		lr_end_sub_transaction ("T05_Add To Cart_S04_TCPMiniShopCartDisplayView" , LR_AUTO) ;

		lr_end_transaction ( "T05_Add To Cart" , LR_AUTO ) ;

	} // end if
	else
	{
		atc_Stat = 1; //0-Pass 1-Fail
		return;
	} // end else

} // end addToCart

void parseOrderItemId() { //This is to grab the upcIDs and this can only be called  after TCPAjaxCheckInventoryAvail(viewCart) when OutOfStock_Count > 0

    extern char * strtok(char * string, const char * delimiters ); 
    char path[1000] = "";
    char separators[] = "\""; 
    char * token;
    char fullpath[1024];
    int counter = 0;
    strcpy(path, lr_eval_string("{unavailId_1}"));

    // Get the first token

    token = (char *)strtok(path, separators); 
    if (!token) {
        lr_output_message ("No tokens found in string!");
    }
// While valid tokens are returned
    while (token != NULL ) { 
//        lr_output_message ("%s", token );
// Get the next token
        token = (char *)strtok(NULL, separators); 
        
        if(token != NULL) {
	        if (strlen(token) > 10) {
	        	counter++;
	        	lr_param_sprintf ("count", "%d", counter);
	        	lr_save_string(token, lr_eval_string("upcId{count}") );
	        }
        }
    }
	return;
}

void getOutOfStockItemIds(){
	
	int i;
	
	if (atoi(lr_eval_string("{unavailId_count}")) > 0) {
		
/*		parseOrderItemId();

		if (atoi(lr_eval_string("{count}")) > 0) {
			for (i = 1; i <= atoi(lr_eval_string("{count}")); i++) {
	
		if (atoi(lr_eval_string("{unavailId_count}")) > 0) {
			for (i = 1; i <= atoi(lr_eval_string("{unavailId_count}")); i++) {
				lr_eval_string("{unavailId_1}")
				lr_save_string(token, lr_eval_string("upcId{count}") );
				switch(i) {
					case 1:
						web_reg_save_param("orderItemId_1", "LB=id=\"{upcId1}\" data-item-id=\"", "RB=\">", "NotFound=Warning", LAST); break;
					case 2:
						web_reg_save_param("orderItemId_2", "LB=id=\"{upcId2}\" data-item-id=\"", "RB=\">", "NotFound=Warning", LAST); break;
					case 3:
						web_reg_save_param("orderItemId_3", "LB=id=\"{upcId3}\" data-item-id=\"", "RB=\">", "NotFound=Warning", LAST); break;
					case 4:
						web_reg_save_param("orderItemId_4", "LB=id=\"{upcId4}\" data-item-id=\"", "RB=\">", "NotFound=Warning", LAST); break;
					case 5:
						web_reg_save_param("orderItemId_5", "LB=id=\"{upcId5}\" data-item-id=\"", "RB=\">", "NotFound=Warning", LAST); break;
					case 6:
						web_reg_save_param("orderItemId_6", "LB=id=\"{upcId6}\" data-item-id=\"", "RB=\">", "NotFound=Warning", LAST); break;
					case 7:
						web_reg_save_param("orderItemId_7", "LB=id=\"{upcId7}\" data-item-id=\"", "RB=\">", "NotFound=Warning", LAST); break;
					case 8:
						web_reg_save_param("orderItemId_8", "LB=id=\"{upcId8}\" data-item-id=\"", "RB=\">", "NotFound=Warning", LAST); break;
					default: break;
				}
			}
	
		}
*/		
		web_reg_save_param("orderItemId", "LB=id=\"{unavailId_1}\" data-item-id=\"", "RB=\">", "NotFound=Warning", LAST); 
		
		web_url("OrderCalculate", 
		"URL=https://{host}/shop/OrderCalculate?calculationUsageId=-1&updatePrices=1&catalogId={catalogId}&errorViewName=AjaxCheckoutDisplayView&orderId=.&langId=-1&storeId={storeId}&URL=AjaxCheckoutDisplayView", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Mode=HTML", 
		LAST);
		
		removeOutOfStockItem();
		
	}	
	return;
}

void removeOutOfStockItem() {
	
	int i;

//	for (i = 1; i <= atoi(lr_eval_string("{unavailId_count}")); i++) {
	while ( atoi(lr_eval_string("{unavailId_count}")) !=0 ) {
		//unavailId_1
/*		switch(i) {
			case 1:
				lr_save_string(lr_eval_string("{orderItemId_1}"), "orderItemIdDelete"); break;
			case 2:
				lr_save_string(lr_eval_string("{orderItemId_2}"), "orderItemIdDelete"); break;
			case 3:
				lr_save_string(lr_eval_string("{orderItemId_3}"), "orderItemIdDelete"); break;
			case 4:
				lr_save_string(lr_eval_string("{orderItemId_4}"), "orderItemIdDelete"); break;
			case 5:
				lr_save_string(lr_eval_string("{orderItemId_5}"), "orderItemIdDelete"); break;
			case 6:
				lr_save_string(lr_eval_string("{orderItemId_6}"), "orderItemIdDelete"); break;
			case 7:
				lr_save_string(lr_eval_string("{orderItemId_7}"), "orderItemIdDelete"); break;
			case 8:
				lr_save_string(lr_eval_string("{orderItemId_8}"), "orderItemIdDelete"); break;
			default: break;
		}
*/		
		lr_start_transaction("T20_Remove_OutOfStockItem");

		web_submit_data("AjaxOrderChangeServiceItemDelete", 
			"Action=https://{host}/webapp/wcs/stores/servlet/AjaxOrderChangeServiceItemDelete", 
			"Method=POST", 
			"TargetFrame=", 
			"RecContentType=application/json", 
			"Mode=HTML", 
			ITEMDATA, 
			"Name=storeId", "Value={storeId}", ENDITEM, 
			"Name=catalogId", "Value={catalogId}", ENDITEM, 
			"Name=langId", "Value=-1", ENDITEM, 
			"Name=orderId", "Value={orderId}", ENDITEM, 
			"Name=orderItemId", "Value={orderItemId}", ENDITEM, 
			"Name=visitorId", "Value=", ENDITEM, 
			"Name=calculationUsage", "Value=-1,-2,-5,-6,-7", ENDITEM, 
			"Name=requesttype", "Value=ajax", ENDITEM, 
			LAST);

		web_submit_data("AjaxTCPShutterflyPromoDisplayEspotView", 
			"Action=https://{host}/shop/AjaxTCPShutterflyPromoDisplayEspotView?catalogId={catalogId}&emsName=ShutterflyPromoEspot&storeId={storeId}&storeName=us&domainName=BlaBlaDomain", 
			"Method=POST", 
			"TargetFrame=", 
			"RecContentType=text/html", 
			"Mode=HTML", 
			ITEMDATA, 
			"Name=objectId", "Value=", ENDITEM, 
			"Name=requesttype", "Value=ajax", ENDITEM, 
			LAST);

		web_custom_request("CreateCookieCmd", 
			"URL=https://{host}/webapp/wcs/stores/servlet/CreateCookieCmd?langId=-1&storeId={storeId}&goToPage=NoURL&catalogId={catalogId}", 
			"Method=GET", 
			"TargetFrame=", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Mode=HTML", 
			"EncType=application/x-www-form-urlencoded", 
			LAST);

		web_submit_data("TCPMiniShopCartDisplayView1", 
			"Action=https://{host}/shop/TCPMiniShopCartDisplayView1?catalogId={catalogId}&langId=-1&storeId={storeId}", 
			"Method=POST", 
			"TargetFrame=", 
			"RecContentType=text/html", 
			"Mode=HTML", 
			ITEMDATA, 
			"Name=showredEye", "Value=true", ENDITEM, 
			"Name=objectId", "Value=", ENDITEM, 
			"Name=requesttype", "Value=ajax", ENDITEM, 
			LAST);

		web_submit_data("ShopCartDisplayView", 
			"Action=https://{host}/shop/ShopCartDisplayView?shipmentType=single&catalogId={catalogId}&langId=-1&storeId={storeId}", 
			"Method=POST", 
			"TargetFrame=", 
			"RecContentType=text/html", 
			"Mode=HTML", 
			ITEMDATA, 
			"Name=showredEye", "Value=true", ENDITEM, 
			"Name=objectId", "Value=", ENDITEM, 
			"Name=requesttype", "Value=ajax", ENDITEM, 
			LAST);

		lr_end_transaction("T20_Remove_OutOfStockItem",LR_AUTO);
//================= 07/25 romano
			web_reg_save_param("unavailId", "LB=unavailId\": [\"", "RB=\"", "ORD=All", "NotFound=Warning", LAST);
//			web_reg_save_param("unavailId", "LB=unavailId\": [\n", "RB=\t\t],\n", "ORD=All", "NotFound=Warning", LAST);
//			  \t"unavailId": ["00472512916825"],\n

			web_submit_data("ViewCart_TCPAjaxCheckInventoryAvail",
				"Action=https://{host}/webapp/wcs/stores/servlet/TCPAjaxCheckInventoryAvail",
				"Method=POST",
				"RecContentType=text/html",
				"Snapshot=t22.inf",
				"Mode=HTML",
				ITEMDATA,
				"Name=orderId", "Value={orderId}", ENDITEM,
				"Name=requesttype", "Value=ajax", ENDITEM,
				LAST);
				
			if (atoi(lr_eval_string("{unavailId_count}")) !=0 ) {
				web_reg_save_param("orderItemId", "LB=id=\"{unavailId_1}\" data-item-id=\"", "RB=\">", "NotFound=Warning", LAST); //break;
				
				web_url("OrderCalculate", 
				"URL=https://{host}/shop/OrderCalculate?calculationUsageId=-1&updatePrices=1&catalogId={catalogId}&errorViewName=AjaxCheckoutDisplayView&orderId=.&langId=-1&storeId={storeId}&URL=AjaxCheckoutDisplayView", 
				"TargetFrame=", 
				"Resource=0", 
				"RecContentType=text/html", 
				"Mode=HTML", 
				LAST);
			}
	}
	
	lr_save_string("0", "OutOfStock_Count");
	
	return;
}


void viewCart()
{
	//lr_message("in View Cart");
	lr_think_time ( LINK_TT ) ;
	web_reg_save_param ( "totalNumberOfItems", "LB=totalNumberOfItems\" value='", "RB=\'", "Ord=ALL", "NotFound=Warning", LAST);
	web_reg_save_param ( "orderItemIds" , "LB=<input type=\"hidden\" value='" , "RB=' name='orderItem_" , "Ord=ALL" , "NotFound=Warning", LAST ) ;
	web_reg_save_param ("catalogIds", "LB=id=\"catalogEntry_img", "RB=\"", "ORD=All", "NotFound=Warning", LAST); // catentryid for moving

	//web_reg_find("Text=have a coupon code?", LAST);

	lr_start_transaction ( "T06_View Cart" ) ;

		lr_start_sub_transaction ( "T06_View Cart_S01_OrderCalculate", "T06_View Cart" );

		web_url("ViewCart_OrderCalculate",
			"URL=https://{host}/shop/OrderCalculate?calculationUsageId=-1&updatePrices=1&catalogId={catalogId}&errorViewName=AjaxOrderItemDisplayView&orderId=.&langId=-1&storeId={storeId}&URL=AjaxOrderItemDisplayView",
			"Resource=0",
			"RecContentType=text/html",
			"Snapshot=t21.inf",
			"Mode=HTML",
			LAST);

		lr_end_sub_transaction("T06_View Cart_S01_OrderCalculate", LR_AUTO);

//		if ( strlen( lr_eval_string("{orderItemIds_count}") ) != 20 && atoi( lr_eval_string("{orderItemIds_count}") ) != 0 ) {
		if ( atoi( lr_eval_string("{orderItemIds_count}") ) != 0 ) {

			web_reg_save_param("unavailId", "LB=unavailId\": [\"", "RB=\"", "ORD=All", "NotFound=Warning", LAST);
//			web_reg_save_param("unavailId", "LB=unavailId\": [\n", "RB=\t\t],\n", "ORD=All", "NotFound=Warning", LAST);
//			  \t"unavailId": ["00472512916825"],\n
			lr_start_sub_transaction ( "T06_View Cart_S02_TCPAjaxCheckInventoryAvail", "T06_View Cart" ); //called only if cart is not empty and then checks if any item in cart is out of stock

			web_submit_data("ViewCart_TCPAjaxCheckInventoryAvail",
				"Action=https://{host}/webapp/wcs/stores/servlet/TCPAjaxCheckInventoryAvail",
				"Method=POST",
				"RecContentType=text/html",
				"Snapshot=t22.inf",
				"Mode=HTML",
				ITEMDATA,
				"Name=orderId", "Value={orderId}", ENDITEM,
				"Name=requesttype", "Value=ajax", ENDITEM,
				LAST);

			lr_end_sub_transaction("T06_View Cart_S02_TCPAjaxCheckInventoryAvail", LR_AUTO);

			orderItemIdscount = atoi( lr_eval_string("{orderItemIds_count}") );
			
			//https://www.childrensplace.com/wcs/resources/store/10151/espot/Shopping_Cart_Total_Espot?responseFormat=json
			
			lr_start_sub_transaction ( "T06_View Cart_S03_Shopping_Cart_Total_Espot", "T06_View Cart" ); 
			
			web_url("ViewCart_Shopping_Cart_Total_Espot",
				"URL=https://{host}/wcs/resources/store/{storeId}/espot/Shopping_Cart_Total_Espot?responseFormat=json",
				"Resource=0",
				"RecContentType=text/html",
				"Snapshot=t21.inf",
				"Mode=HTML",
				LAST);
				
			lr_end_sub_transaction("T06_View Cart_S03_Shopping_Cart_Total_Espot", LR_AUTO);
			
			lr_start_sub_transaction ( "T06_View Cart_S04_AjaxOrderItemDisplayView", "T06_View Cart" ); 
			
			web_custom_request("viewCart_AjaxOrderItemDisplayView",
				"URL=https://{host}/shop/AjaxOrderItemDisplayView?catalogId={catalogId}&langId=-1&storeId={storeId}&krypto=dUE94QLj%2B8l%2F61T%2BhJO9u4yS7EVZpPkI84e3SBBgVuCk8jRJ6zCYMjCwYUq1cu9TXa709PFhVY0d%0A5tJ5RMWNsEyGSr2pHXhjxgzAtKj%2Fj%2FqCjbnYT9tr%2Fo6aMm8C%2BUsD&ddkey=https:OrderCalculate",
				"Method=GET",
				"Resource=0",
				"RecContentType=text/html",
				"Snapshot=t27.inf",
				"Mode=HTML",
				"EncType=application/x-www-form-urlencoded",
				LAST);
				
			lr_end_sub_transaction("T06_View Cart_S04_AjaxOrderItemDisplayView", LR_AUTO);
			
			
			lr_end_transaction ( "T06_View Cart" , LR_PASS ) ;
			
			getOutOfStockItemIds();

		}
		else {
			orderItemIdscount = 0; //Moved to Global
			
			lr_end_transaction ( "T06_View Cart" , LR_PASS ) ;

		}
		
} // end viewCart

void viewCartFromLogin()
{
	//lr_message("in View Cart");
	lr_think_time ( LINK_TT ) ;
	web_reg_save_param("totalNumberOfItems", "LB=totalNumberOfItems\" value='", "RB=\'", "Ord=All", "NotFound=Warning", LAST);
	web_reg_save_param ( "orderItemIds" , "LB=<input type=\"hidden\" value='" , "RB=' name='orderItem_" , "NotFound=Warning", "Ord=ALL" , LAST ) ;

	lr_start_transaction ( "T06_View Cart" ) ;

		lr_start_sub_transaction ( "T06_View Cart_S01_OrderCalculate", "T06_View Cart" );

		web_url("viewCartFromLogin_OrderCalculate",
			"URL=http://{host}/shop/OrderCalculate?calculationUsageId=-1&updatePrices=1&catalogId={catalogId}&errorViewName=AjaxOrderItemDisplayView&orderId=.&langId=-1&storeId={storeId}&URL=AjaxOrderItemDisplayView",
			"Resource=0",
			"RecContentType=text/html",
			"Snapshot=t21.inf",
			"Mode=HTML",
			LAST);

		lr_end_sub_transaction("T06_View Cart_S01_OrderCalculate", LR_AUTO);

		web_reg_save_param("unavailId", "LB=unavailId\": [\n", "RB=\t\t],\n", "ORD=All", "NotFound=Warning", LAST);

//		if ( strlen( lr_eval_string("{orderItemIds_count}") ) != 20 && atoi( lr_eval_string("{orderItemIds_count}") ) != 0 ) {

			lr_start_sub_transaction ( "T06_View Cart_S02_TCPAjaxCheckInventoryAvail", "T06_View Cart" ); //called only if cart is not empty and then checks if any item in cart is out of stock

			web_submit_data("viewCartFromLogin_TCPAjaxCheckInventoryAvail",
				"Action=https://{host}/webapp/wcs/stores/servlet/TCPAjaxCheckInventoryAvail",
				"Method=POST",
				"RecContentType=text/html",
				"Snapshot=t22.inf",
				"Mode=HTML",
				ITEMDATA,
				"Name=orderId", "Value={orderId}", ENDITEM,
				"Name=requesttype", "Value=ajax", ENDITEM,
				LAST);

			lr_end_sub_transaction("T06_View Cart_S02_TCPAjaxCheckInventoryAvail", LR_AUTO);
/*			
			orderItemIdscount = atoi( lr_eval_string("{orderItemIds_count}") );
		}
		else {
			orderItemIdscount = 0; //Moved to Global
		}
*/		
	lr_end_transaction ( "T06_View Cart" , LR_PASS ) ;

	getOutOfStockItemIds();
	
} // end viewCart

void applyPromoCode(int useRewards) //1 to use rewards points, 0 to use either single or multi-use promo
{
	lr_think_time ( FORM_TT ) ;

	if ( useRewards == 1 ){
		lr_save_string( lr_eval_string ( "{promocodeRewards}" ) , "promocode" ) ;
//		lr_save_string( lr_eval_string ( "Y022053295EB53F6" ) , "promocode" ) ;
		lr_save_string( "Rewards" , "promotype" ) ;
	}
	else if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_PROMO_MULTIUSE ) {
		lr_save_string( lr_eval_string ( "{multiusePromoCode}" ) , "promocode" ) ;
		lr_save_string( "Multi-Use" , "promotype" ) ;
	} // end if
	else if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= (RATIO_PROMO_SINGLEUSE + RATIO_PROMO_MULTIUSE) ) {
		
		if ( strcmp(lr_eval_string("{scriptOrigin}"), "US") == 0)
			getPromoCode();
		else
			lr_save_string( lr_eval_string ( "{singleusePromocode}" ) , "promocode" ) ;
		
		lr_save_string( "Single-Use" , "promotype" ) ;
	}

	web_reg_save_param( "invalidCoupon", "LB=This code is ", "RB=t applicable", "NotFound=Warning", LAST);
	web_reg_save_param( "invalidCoupon2", "LB=errorMessageKey\": \"ERR_PROMOTI", "RB=_CODE_INVALID", "NotFound=Warning", LAST); //errorMessageKey": "ERR_PROMOTION_CODE_INVALID	
	
	lr_start_transaction ( lr_eval_string ( "T07_Enter Promo {promotype}" ) ) ;

		lr_start_transaction ( "T07_Enter Promo_S01_AjaxPromotionCodeManage" );

		web_submit_data ( lr_eval_string("{promotype}_AjaxPromotionCodeManage") ,
		  "Action=https://{host}/webapp/wcs/stores/servlet/AjaxPromotionCodeManage" ,
		  "Method=POST" ,
		  "Mode=HTML" ,
		  ITEMDATA ,
			"Name=orderId" , "Value={orderId}" , ENDITEM ,
			"Name=taskType" , "Value=A" , ENDITEM ,
			"Name=URL" , "Value=" , ENDITEM ,
			"Name=storeId" , "Value={storeId}" , ENDITEM ,
			"Name=langId" , "Value=-1" , ENDITEM ,
			"Name=catalogId" , "Value={catalogId}" , ENDITEM ,
			"Name=finalView" , "Value=AjaxOrderItemDisplayView" , ENDITEM ,
			"Name=fromPage" , "Value=shoppingCartDisplay" , ENDITEM ,
			"Name=promoCode" , "Value={promocode}" , ENDITEM ,
			"Name=requesttype" , "Value=ajax" , ENDITEM ,
		  LAST ) ;

	if (  strcmp( lr_eval_string("{invalidCoupon2}"), "ON" ) == 0 ) {
		lr_end_transaction("T07_Enter Promo_S01_AjaxPromotionCodeManage", LR_PASS)	;
		lr_end_transaction ( lr_eval_string ( "T07_Enter Promo {promotype}" ) , LR_PASS ) ;
	} 
	else //if promocode is NOT "not applicable", continue with rest of the calls
	{ 
		lr_end_transaction("T07_Enter Promo_S01_AjaxPromotionCodeManage", LR_AUTO)	;
			
		lr_start_transaction ( "T07_Enter Promo_S02_AjaxOrderChangeServiceItemUpdate" );

		web_submit_data ( lr_eval_string("{promotype}_AjaxOrderChangeServiceItemUpdate") ,
						  "Action=https://{host}/webapp/wcs/stores/servlet/AjaxOrderChangeServiceItemUpdate" ,
						  "Method=POST" ,
						  "Mode=HTML" ,
						  ITEMDATA ,
							"Name=orderId" , "Value=." , ENDITEM ,
							"Name=calculationUsage" , "Value=-1,-2,-5,-6,-7" , ENDITEM ,
							"Name=requesttype" , "Value=ajax" , ENDITEM ,
						  LAST ) ;

		lr_end_transaction("T07_Enter Promo_S02_AjaxOrderChangeServiceItemUpdate", LR_AUTO)	;

		lr_start_transaction ( "T07_Enter Promo_S03_CreateCookieCmd" );

		web_url ( lr_eval_string("{promotype}_CreateCookieCmd") ,
				  "URL=https://{host}/webapp/wcs/stores/servlet/CreateCookieCmd?langId=-1&storeId={storeId}&goToPage=NoURL&catalogId={catalogId}" ,
				  "Resource=0" ,
				  "Mode=HTML" ,
				  LAST ) ;

		lr_end_transaction("T07_Enter Promo_S03_CreateCookieCmd", LR_AUTO);

		createCookieCmd();
		
		lr_start_transaction ( "T07_Enter Promo_S04_TCPMiniShopCartDisplayView1" );

		web_submit_data ( lr_eval_string("{promotype}_TCPMiniShopCartDisplayView1" ),
				  "Action=https://{host}/shop/TCPMiniShopCartDisplayView1?catalogId={catalogId}&langId=-1&storeId={storeId}" , //rhy 10/25/2015 changed to "/shop/"
				  "Method=POST" ,
				  "Mode=HTML" ,
				  ITEMDATA ,
					"Name=objectId" , "Value=" , ENDITEM ,
					"Name=showredEye" , "Value=true" , ENDITEM ,
					"Name=requesttype" , "Value=ajax" , ENDITEM ,
				  LAST ) ;

		lr_end_transaction("T07_Enter Promo_S04_TCPMiniShopCartDisplayView1", LR_AUTO);

		lr_start_transaction ( "T07_Enter Promo_S05_ShopCartDisplayView" );

		web_submit_data ( lr_eval_string("{promotype}_ShopCartDisplayView") , 
				  "Action=https://{host}/shop/ShopCartDisplayView?shipmentType=single&catalogId={catalogId}&langId=-1&storeId={storeId}" , //rhy 10/25/2015 changed to "/shop/"
				  "Method=POST" ,
				  "Mode=HTML" ,
				  ITEMDATA ,
					"Name=objectId" , "Value=" , ENDITEM ,
					"Name=requesttype" , "Value=ajax" , ENDITEM ,
				  LAST ) ;

		lr_end_transaction("T07_Enter Promo_S05_ShopCartDisplayView", LR_AUTO);
			
		if (isLoggedIn == 1) 
			TCPGetWishListForUsersView();
		
		lr_end_transaction ( lr_eval_string ( "T07_Enter Promo {promotype}" ) , LR_AUTO ) ;
//       	vtc_clear_row(pvci, rNum, &updateStatus); //delete the code from the datafile
	}
	
//    vtc_free_list(colnames);
//    vtc_free_list(rowdata);
//    vtc_disconnect( pvci );

} // end applyPromoCode


void deletePromoCode()
{
    //lr_message("in applyPromoCode");
//    if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) < RATIO_PROMO_DELETE ) {

    lr_think_time ( FORM_TT ) ;

    lr_start_transaction ( lr_eval_string ( "T07_Delete Promocode" ) ) ;

    lr_start_transaction ( "T07_Delete Promo_S01_AjaxPromotionCodeManage" );

    web_submit_data ( "delete_promocode_AjaxPromotionCodeManage" ,
			  "Action=https://{host}/webapp/wcs/stores/servlet/AjaxPromotionCodeManage" ,
			  "Method=POST" ,
			  "Mode=HTML" ,
			  ITEMDATA ,
				 "Name=orderId" , "Value={orderId}" , ENDITEM ,
				"Name=taskType" , "Value=R" , ENDITEM ,
				"Name=URL" , "Value=" , ENDITEM ,
				"Name=storeId" , "Value={storeId}" , ENDITEM ,
				"Name=langId" , "Value=-1" , ENDITEM ,
				"Name=catalogId" , "Value={catalogId}" , ENDITEM ,
				"Name=finalView" , "Value=AjaxOrderItemDisplayView" , ENDITEM ,
				"Name=fromPage" , "Value=shoppingCartDisplay" , ENDITEM ,
				"Name=promoCode" , "Value={promocode}" , ENDITEM , 
				"Name=requesttype" , "Value=ajax" , ENDITEM ,
			  LAST ) ;

    lr_end_transaction("T07_Delete Promo_S01_AjaxPromotionCodeManage", LR_AUTO);

    lr_start_transaction ( "T07_Delete Promo_S02_AjaxOrderChangeServiceItemUpdate" );

    web_submit_data ( "delete_promocode_AjaxOrderChangeServiceItemUpdate" ,
			  "Action=https://{host}/webapp/wcs/stores/servlet/AjaxOrderChangeServiceItemUpdate" ,
			  "Method=POST" ,
			  "Mode=HTML" ,
			  ITEMDATA ,
				"Name=orderId" , "Value=." , ENDITEM , 
				"Name=calculationUsage" , "Value=-1,-2,-5,-6,-7" , ENDITEM ,
				"Name=requesttype" , "Value=ajax" , ENDITEM ,
			  LAST ) ;

    lr_end_transaction("T07_Delete Promo_S02_AjaxOrderChangeServiceItemUpdate", LR_AUTO)    ;

    lr_start_transaction ( "T07_Delete Promo_S03_CreateCookieCmd" );

    web_url ( "delete_promcode_CreateCookieCmd" ,
              "URL=https://{host}/webapp/wcs/stores/servlet/CreateCookieCmd?langId=-1&storeId={storeId}&goToPage=NoURL&catalogId={catalogId}" ,
              "Resource=0" ,
              "Mode=HTML" ,
              LAST ) ;

    lr_end_transaction("T07_Delete Promo_S03_CreateCookieCmd", LR_AUTO);

	createCookieCmd();

    lr_start_transaction ( "T07_Delete Promo_S04_TCPMiniShopCartDisplayView1" );

    web_submit_data ( "delete_promocode_TCPMiniShopCartDisplayView1" ,
			  "Action=https://{host}/shop/TCPMiniShopCartDisplayView1?catalogId={catalogId}&langId=-1&storeId={storeId}" , //rhy 10/25/2015 changed to "/shop/"
			  "Method=POST" ,
			  "Mode=HTML" ,
			  ITEMDATA ,
				"Name=objectId" , "Value=" , ENDITEM ,
				"Name=showredEye" , "Value=true" , ENDITEM ,
				"Name=requesttype" , "Value=ajax" , ENDITEM ,
			  LAST ) ;

    lr_end_transaction("T07_Delete Promo_S04_TCPMiniShopCartDisplayView1", LR_AUTO);

    lr_start_transaction ( "T07_Delete Promo_S05_ShopCartDisplayView" );

    web_submit_data ( "promocode_ShopCartDisplayView" ,
			  "Action=https://{host}/shop/ShopCartDisplayView?shipmentType=single&catalogId={catalogId}&langId=-1&storeId={storeId}" ,
			  "Method=POST" ,
			  "Mode=HTML" ,
			  ITEMDATA ,
				"Name=objectId" , "Value=" , ENDITEM ,
				"Name=requesttype" , "Value=ajax" , ENDITEM ,
			  LAST ) ;

    lr_end_transaction("T07_Delete Promo_S05_ShopCartDisplayView", LR_AUTO);

    lr_end_transaction ( lr_eval_string ( "T07_Delete Promocode" ) , LR_PASS ) ;
	
 //  }
} // end deletePromoCode

void deleteItem()
{
	//lr_message("in deleteItem");
//	if ( lr_paramarr_len ( "orderItemIds" ) > 0 ){
	if ( lr_paramarr_len ( "orderItemIds" ) > 1 ){
//		index = rand ( ) % lr_paramarr_len( "orderItemIds" ) + 1 ;
//		lr_save_string ( lr_paramarr_idx( "orderItemIds" , index ) , "orderItemId" ) ;
		lr_save_string ( lr_paramarr_random( "orderItemIds" ) , "orderItemId" ) ;
		
		lr_think_time ( LINK_TT ) ;
		lr_start_transaction ( "T08_Remove Item" ) ;

			lr_start_sub_transaction("T08_Remove Item_S01_AjaxOrderChangeServiceItemDelete", "T08_Remove Item");

			web_submit_data ( "AjaxOrderChangeServiceItemDelete" ,
				  "Action=https://{host}/webapp/wcs/stores/servlet/AjaxOrderChangeServiceItemDelete" ,
				  "Method=POST" ,
				  "Mode=HTML" ,
				  ITEMDATA ,
					"Name=orderId" , "Value={orderId}" , ENDITEM ,
					"Name=orderItemId" , "Value={orderItemId}" , ENDITEM ,
					"Name=storeId" , "Value={storeId}" , ENDITEM ,
					"Name=langId" , "Value=-1" , ENDITEM ,
					"Name=catalogId" , "Value={catalogId}" , ENDITEM ,
					"Name=calculationUsage" , "Value=-1,-2,-5,-6,-7" , ENDITEM ,
					"Name=requesttype" , "Value=ajax" , ENDITEM ,
				 LAST ) ;

			lr_end_sub_transaction("T08_Remove Item_S01_AjaxOrderChangeServiceItemDelete", LR_AUTO);

			lr_start_sub_transaction("T08_Remove Item_S02_TCPMiniShopCartDisplayView1", "T08_Remove Item");

			web_submit_data ( "removeItem_TCPMiniShopCartDisplayView1" ,
				  "Action=https://{host}/webapp/wcs/stores/servlet/TCPMiniShopCartDisplayView1?catalogId={catalogId}&langId=-1&storeId={storeId}" ,
				  "Method=POST" ,
				  "Mode=HTML" ,
				  ITEMDATA ,
					"Name=objectId" , "Value=" , ENDITEM ,
					"Name=showredEye" , "Value=true" , ENDITEM ,
					"Name=requesttype" , "Value=ajax" , ENDITEM ,
				  LAST ) ;

			lr_end_sub_transaction("T08_Remove Item_S02_TCPMiniShopCartDisplayView1", LR_AUTO);

			lr_start_sub_transaction("T08_Remove Item_S03_ShopCartDisplayView", "T08_Remove Item");

			web_submit_data ( "removeItem_ShopCartDisplayView" ,
				  "Action=https://{host}/webapp/wcs/stores/servlet/ShopCartDisplayView?shipmentType=single&catalogId={catalogId}&langId=-1&storeId={storeId}" ,
				  "Method=POST" ,
				  "Mode=HTML" ,
				  ITEMDATA ,
					"Name=objectId" , "Value=" , ENDITEM ,
					"Name=requesttype" , "Value=ajax" , ENDITEM ,
				  LAST ) ;

			lr_end_sub_transaction("T08_Remove Item_S03_ShopCartDisplayView", LR_AUTO);
			
			//https://tcp-perf.childrensplace.com/webapp/wcs/stores/servlet/CreateCookieCmd?langId=-1&storeId=10151&goToPage=NoURL&catalogId=10551
			lr_start_sub_transaction ("T08_Remove Item_S04_CreateCookieCmd", "T08_Remove Item" ) ;

			web_custom_request("UpdateQuantity_CreateCookieCmd",
				"URL=https://{host}/webapp/wcs/stores/servlet/CreateCookieCmd?langId=-1&storeId={storeId}&goToPage=NoURL&catalogId={catalogId}",
				"Method=GET",
				"Resource=0",
				"RecContentType=text/html",
				"Snapshot=t28.inf",
				"Mode=HTML",
				"EncType=application/x-www-form-urlencoded",
				LAST);

			lr_end_sub_transaction ("T08_Remove Item_S04_CreateCookieCmd", LR_AUTO) ;

			createCookieCmd();
			
			TCPGetWishListForUsersView();
		
		lr_end_sub_transaction("T08_Remove Item", LR_AUTO);
	} // end if
} // end deleteItem


void proceedToCheckout()
{
	lr_think_time ( LINK_TT ) ;
	web_reg_save_param ( "addressIds" , "LB=<option id=\"" , "RB=\" value=" , "Ord=ALL" , "NotFound=Warning", LAST ) ;
	web_reg_save_param ( "TCPMyAddressBook" , "LB=edit shipping address\" id=\"TCPMyAddressBook_" , "RB=\"" , "Ord=ALL", "NotFound=Warning", LAST ) ;
	web_reg_save_param ( "emailAddress1" , "LB=<input type=\"hidden\" name=\"email1\" value=\"" , "RB=\" id=\"email1\" />" , "NotFound=Warning", LAST ) ;
	web_reg_save_param ( "authToken" , "LB=<input type=\"hidden\" name=\"authToken\" value=\"" , "RB=\"", "NotFound=Warning", LAST ) ;

	lr_start_transaction ( "T09_Proceed to Checkout_LogonForm" ) ;
	
		web_url ( "T09_Proceed to Checkout_LogonForm" ,
			  "URL=https://{host}/shop/LogonForm?catalogId={catalogId}&myAcctMain=1&langId=-1&storeId={storeId}&URL=https://{host}/webapp/wcs/stores/servlet/TCPOrderShippingView?shipmentType=single&catalogId={catalogId}&langId=-1&storeId={storeId}" ,
			  "Resource=0" ,
			  "Mode=HTML" ,
			  LAST ) ;

	lr_end_transaction ( "T09_Proceed to Checkout_LogonForm" , LR_PASS) ;

} // end proceedToCheckout

void proceedToCheckout_ShippingView()
{
	lr_think_time ( LINK_TT ) ;

	web_reg_save_param ( "addressIds" , "LB=<option id=\"" , "RB=\" value=" , "Ord=ALL" , "NotFound=Warning", LAST ) ;
	web_reg_save_param ( "TCPMyAddressBook" , "LB=edit shipping address\" id=\"TCPMyAddressBook_" , "RB=\"" , "Ord=ALL", "NotFound=Warning", LAST ) ;
	web_reg_save_param ( "emailAddress1" , "LB=<input type=\"hidden\" name=\"email1\" value=\"" , "RB=\" id=\"email1\" />" , "NotFound=Warning", LAST ) ;
	web_reg_save_param ( "authToken" , "LB=<input type=\"hidden\" name=\"authToken\" value=\"" , "RB=\"", "NotFound=Warning", LAST ) ;

	lr_start_transaction ( "T09_Proceed to Checkout_ShippingView" ) ;
	web_url ( "T09_Proceed to Checkout_ShippingView" ,
			  "URL=https://{host}/shop/TCPOrderShippingView?shipmentType=single&catalogId={catalogId}&langId=-1&storeId={storeId}" ,
			  "Resource=0" ,
			  "Mode=HTML" ,
			  LAST ) ;
	lr_end_transaction ( "T09_Proceed to Checkout_ShippingView" , LR_PASS) ;

} // end proceedToCheckout_ShippingView


void proceedAsGuest()
{
	lr_think_time ( LINK_TT ) ;

	web_reg_save_param ( "emailAddress1" , "LB=<input type=\"hidden\" name=\"email1\" value=\"" , "RB=\" id=\"email1\" />" ,"NotFound=Warning", LAST ) ;
	web_reg_save_param ( "authToken" , "LB=<input type=\"hidden\" name=\"authToken\" value=\"" , "RB=\"" , "NotFound=Warning", LAST ) ;

	lr_start_transaction ( "T10_Proceed As Guest" ) ;

	web_url("T10_Proceed As Guest_TCPOrderShippingView",
			"URL=https://{host}/webapp/wcs/stores/servlet/TCPOrderShippingView?shipmentType=single&catalogId={catalogId}&langId=-1&storeId={storeId}",
			"Resource=0",
			"RecContentType=text/html",
			"Mode=HTML",
			LAST);

	lr_end_transaction ( "T10_Proceed As Guest" , LR_PASS) ;
} // end proceedAsGuest



void forgetPassword()
{	
	lr_think_time ( FORM_TT ) ;

	web_url("LogonForm",
			"URL=https://{host}/shop/LogonForm?catalogId={catalogId}&myAcctMain=1&langId=-1&storeId={storeId}",
			"Mode=HTTP",
			LAST);	

	lr_start_transaction ( "T29_ForgotPassword" ) ;

	web_url("Forgot password?",
		"URL=http://{host}/shop/ResetPasswordGuestErrorView?state=forgetpassword&catalogId={catalogId}&langId=-1&storeId={storeId}",
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=", 
		"Snapshot=t3.inf", 
		"Mode=HTML", 
		LAST);	
			
	lr_end_transaction ( "T29_ForgotPassword" , LR_PASS) ;
//Need to add verification point here
	lr_start_transaction ( "T29_PasswordReset" ) ;

	web_submit_data("PersonChangeServicePasswordReset", 
		"Action=http://{host}//shop/PersonChangeServicePasswordReset?storeId={storeId}", 
		"Method=POST", 
		"TargetFrame=", 
		"RecContentType=text/html", 
		"Referer=http://{host}//shop/ResetPasswordGuestErrorView?state=forgetpassword&catalogId={catalogId}&langId=-1&storeId={storeId}", 
		"Mode=HTML", 
		"EncodeAtSign=YES", 
		ITEMDATA, 
		"Name=storeId", "Value={storeId}", ENDITEM, 
		"Name=catalogId", "Value={catalogId}", ENDITEM, 
		"Name=langId", "Value=-1", ENDITEM, 
		"Name=state", "Value=passwdconfirm", ENDITEM, 
		"Name=URL", "Value=ResetPasswordForm", ENDITEM, 
		"Name=formFlag", "Value=true", ENDITEM, 
		"Name=errorViewName", "Value=ResetPasswordGuestErrorView", ENDITEM, 
		"Name=checkEmailAddress", "Value=-", ENDITEM, 
		"Name=logonId", "Value=TCPPERF_US2_1947594112@CHILDRENSPLACE.COM", ENDITEM, 
		"Name=logonIdInput", "Value=TCPPERF_US2_1947594112@CHILDRENSPLACE.COM", ENDITEM, 
		LAST);

	lr_end_transaction ( "T29_PasswordReset" , LR_PASS) ;
//Need to add verification point here

}

void login()
{
	
	lr_think_time ( FORM_TT ) ;
	web_reg_save_param ( "addressIds" , "LB=<option id=\"" , "RB=\" value=" , "Ord=ALL" , "NotFound=Warning", LAST ) ;
	web_reg_save_param ( "TCPMyAddressBook" , "LB=edit shipping address\" id=\"TCPMyAddressBook_" , "RB=\"" , "Ord=ALL", "NotFound=Warning", LAST ) ;
	web_reg_save_param ( "emailAddress1" , "LB=<input type=\"hidden\" name=\"email1\" value=\"" , "RB=\" id=\"email1\" />" , "NotFound=Warning", LAST ) ;
	web_reg_save_param ( "authToken" , "LB=<input type=\"hidden\" name=\"authToken\" value=\"" , "RB=\"", "NotFound=Warning", LAST ) ;
/*
		web_url("LogonForm",
//				"URL=https://{host}/webapp/wcs/stores/servlet/AjaxLogonForm?catalogId={catalogId}&myAcctMain=1&langId=-1&storeId={storeId}", //rhy 11/25/2015
				"URL=https://{host}/shop/LogonForm?catalogId={catalogId}&myAcctMain=1&langId=-1&storeId={storeId}",
				"Mode=HTTP",
				LAST);	
//	web_reg_save_param ( "WishlistID", "LB=MultipleWishLists.setDefaultListId(\'", "RB=\');", "ORD=All", "NotFound=Warning", LAST);
//	web_reg_save_param ( "cartItemCount" , "LB=<input type=\"hidden\" id = \"totalNumberOfItems\" name=\"totalNumberOfItems\" value='", "RB='/>", "ORD=All", LAST ) ;

*/
	web_reg_find ( "Text=entered is incorrect" , "SaveCount=loginerror_count" , LAST ) ;

	lr_start_transaction ( "T10_Logon_ShippingView" ) ;

//		lr_start_sub_transaction ("T10_Logon_S01_LogonServlet","T10_Logon_ShippingView");

		web_submit_data ( "Logon" ,
			  "Action=https://{host}/shop/Logon" ,
			  "Method=POST" ,
			  "Mode=HTML" ,
			  ITEMDATA ,
			  "Name=emc" , "Value=" , ENDITEM ,
			  "Name=emcUserId" , "Value=" , ENDITEM ,
			  "Name=storeId" , "Value={storeId}" , ENDITEM ,
			  "Name=catalogId" , "Value={catalogId}" , ENDITEM ,
			  "Name=reLogonURL" , "Value=LogonForm" , ENDITEM ,
			  "Name=myAcctMain" , "Value=1" , ENDITEM ,
			  "Name=fromOrderId" , "Value=*" , ENDITEM ,
			  "Name=toOrderId" , "Value=." , ENDITEM ,
			  "Name=deleteIfEmpty" , "Value=*" , ENDITEM ,
			  "Name=continue" , "Value=1" , ENDITEM ,
			  "Name=createIfEmpty" , "Value=1" , ENDITEM ,
			  "Name=calculationUsageId" , "Value=-1" , ENDITEM ,
			  "Name=updatePrices" , "Value=0" , ENDITEM ,
			  "Name=errorViewName" , "Value=LogonForm" , ENDITEM ,
			  "Name=previousPage" , "Value=logon" , ENDITEM ,
			  "Name=returnPage" , "Value=" , ENDITEM ,
			  "Name=visitorId_1", "Value=", ENDITEM,
//			  "Name=URL", "Value=OrderItemMove?URL=TCPOrderShippingView&shipmentType=single&catalogId={catalogId}&langId=-1&storeId={storeId}", ENDITEM, 
//			  "Name=URL", "Value=OrderItemMove?URL=OrderCalculate&URL=AjaxLogonForm&calculationUsageId=-1&calculationUsageId=-2&calculationUsageId=-7", ENDITEM, //from Bhaskar 04/26
			  "Name=URL", "Value=https://{host}/webapp/wcs/stores/servlet/TCPOrderShippingView?shipmentType=single&catalogId={catalogId}&langId=-1&storeId={storeId}" , ENDITEM , //0426 original				  
//			  "Name=URL", "Value=AjaxLogonForm" , ENDITEM , //0802 original				  
			  "Name=logonId1" , "Value={userEmail}" , ENDITEM ,
//			  "Name=logonId1" , "Value={logonid}" , ENDITEM ,
//			  "Name=logonId1" , "Value=TCPPERF_US2_9543822884@CHILDRENSPLACE.COM" , ENDITEM ,
			  "Name=logonPassword1" , "Value=asdf1234" , ENDITEM ,
			  "Name=rememberCheck", "Value=true", ENDITEM, 
			  "Name=rememberMe", "Value=true", ENDITEM,  //AND-198              Persistent Cookie/"Remember Me" 
			  LAST ) ;

//		lr_end_sub_transaction ("T10_Logon_S01_LogonServlet", LR_AUTO);
		
 		if ( atoi ( lr_eval_string ( "{TCPMyAddressBook_count}" ) ) > 0 ) {
			lr_save_string(lr_eval_string ( "{TCPMyAddressBook_1}" ),"addressIds_2");
			lr_save_string(lr_eval_string ( "{TCPMyAddressBook_1}" ),"addressId");
		}

		if ( atoi ( lr_eval_string ( "{loginerror_count}" ) ) == 1 ) {
			lr_fail_trans_with_error( "registered login failed for %d",lr_eval_string ("{logonid}") ) ;
//			lr_end_transaction ( "Registered - Regular - User Logon to Checkout" , LR_FAIL ) ;
			lr_end_transaction ( "T10_Logon_ShippingView" , LR_FAIL) ;
			isLoggedIn = 0;
			return;
		}
	isLoggedIn = 1;

	lr_end_transaction ( "T10_Logon_ShippingView" , LR_PASS) ;
/*	
	lr_think_time ( FORM_TT ) ;
	
	web_reg_save_param ( "addressIds" , "LB=<option id=\"" , "RB=\" value=" , "Ord=ALL" , "NotFound=Warning", LAST ) ;
	web_reg_save_param ( "TCPMyAddressBook" , "LB=edit shipping address\" id=\"TCPMyAddressBook_" , "RB=\"" , "Ord=ALL", "NotFound=Warning", LAST ) ;
	web_reg_save_param ( "emailAddress1" , "LB=<input type=\"hidden\" name=\"email1\" value=\"" , "RB=\" id=\"email1\" />" , "NotFound=Warning", LAST ) ;
	web_reg_save_param ( "authToken" , "LB=<input type=\"hidden\" name=\"authToken\" value=\"" , "RB=\"", "NotFound=Warning", LAST ) ;
	//myRewards
		web_url("AjaxLogonForm",
				"URL=https://{host}/shop/AjaxLogonForm?catalogId={catalogId}&myAcctMain=1&langId=-1&storeId={storeId}",
				"Mode=HTTP",
				LAST);	
*/
} // end login

void loginFromHomePage()
{

	web_url("LogonForm",
		"URL=https://{host}/shop/LogonForm?catalogId={catalogId}&myAcctMain=1&langId=-1&storeId={storeId}",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Snapshot=t58.inf",
		"Mode=HTML",
		LAST);

	lr_think_time ( FORM_TT ) ;

	web_reg_save_param ( "addressIds" , "LB=<option id=\"" , "RB=\" value=" , "Ord=ALL" , "NotFound=Warning", LAST ) ;
	web_reg_save_param ( "TCPMyAddressBook" , "LB=edit shipping address\" id=\"TCPMyAddressBook_" , "RB=\"" , "Ord=ALL", "NotFound=Warning", LAST ) ;
	web_reg_save_param ( "emailAddress1" , "LB=<input type=\"hidden\" name=\"email1\" value=\"" , "RB=\" id=\"email1\" />" , "NotFound=Warning", LAST ) ;
	web_reg_save_param ( "authToken" , "LB=<input type=\"hidden\" name=\"authToken\" value=\"" , "RB=\"" , "NotFound=Warning", LAST ) ;
	web_reg_save_param ( "userId" , "LB=Set-Cookie: WC_AUTHENTICATION_" , "RB==" , "ORD=2", "NotFound=Warning", LAST ) ;
	web_reg_save_param ( "orderId" , "LB=orderId=" , "RB=&URL=LogonForm" , "NotFound=Warning", LAST ) ;
//	orderId=625766001&URL=LogonForm
	//Set-Cookie: WC_AUTHENTICATION_230096549=
	web_reg_find ( "Text=Children's Place Employee?" , "SaveCount=loginerror_count" , LAST ) ;

	lr_start_transaction ( "T10_Logon_AjaxLogonForm" ) ;

//		lr_start_sub_transaction ("T10_Logon_S01_LogonServlet","T10_Logon_AjaxLofonForm");

			web_submit_data("LogonFromHomePage",
			"Action=https://{host}/shop/Logon",
			"Method=POST",
			"TargetFrame=",
			"RecContentType=text/html",
			"Snapshot=t59.inf",
			"Mode=HTML",
			"EncodeAtSign=YES",
			ITEMDATA,
			"Name=emc", "Value=", ENDITEM,
			"Name=emcUserId", "Value=", ENDITEM,
			"Name=storeId", "Value={storeId}", ENDITEM,
			"Name=catalogId", "Value={catalogId}", ENDITEM,
			"Name=reLogonURL", "Value=LogonForm", ENDITEM,
			"Name=myAcctMain", "Value=1", ENDITEM,
			"Name=fromOrderId", "Value=*", ENDITEM,
			"Name=toOrderId", "Value=.", ENDITEM,
			"Name=deleteIfEmpty", "Value=*", ENDITEM,
			"Name=continue", "Value=1", ENDITEM,
			"Name=createIfEmpty", "Value=1", ENDITEM,
			"Name=calculationUsageId", "Value=-1", ENDITEM,
			"Name=updatePrices", "Value=0", ENDITEM,
			"Name=errorViewName", "Value=LogonForm", ENDITEM,
			"Name=previousPage", "Value=logon", ENDITEM,
			"Name=returnPage", "Value=", ENDITEM,
			"Name=visitorId_1", "Value=", ENDITEM,
			"Name=URL", "Value=AjaxLogonForm", ENDITEM, //08/02 Siva
//			"Name=URL", "Value=https://{host}/shop/AjaxLogonForm?catalogId={catalogId}&langId=-1&storeId={storeId}", ENDITEM, //04/26 original
//			"Name=URL", "Value=OrderItemMove?URL=OrderCalculate&URL=AjaxLogonForm&calculationUsageId=-1&calculationUsageId=-2&calculationUsageId=-7", ENDITEM, //from Bhaskar 04/26
//			"Name=URL", "Value=OrderItemMove?URL=AjaxLogonForm", ENDITEM,  //0707 recording
//			"Name=logonId1" , "Value=mannyyamzon@gmail.com" , ENDITEM ,
//			"Name=logonPassword1", "Value=Asdf!234", ENDITEM,
			"Name=logonId1" , "Value={userEmail}" , ENDITEM ,
			"Name=logonPassword1", "Value=asdf1234", ENDITEM,
			"Name=rememberCheck", "Value=true", ENDITEM, 
			"Name=rememberMe", "Value=true", ENDITEM,  //AND-198 Persistent Cookie/"Remember Me" 
			LAST);

//		lr_end_sub_transaction ("T10_Logon_S01_LogonServlet", LR_AUTO);


		if ( atoi ( lr_eval_string ( "{loginerror_count}" ) ) == 0 ) {
//			lr_fail_trans_with_error( "registered login failed for %d",lr_eval_string ("{userEmail}") ) ;
			lr_end_transaction ( "T10_Logon_AjaxLogonForm" , LR_FAIL) ;
			isLoggedIn = 0;
			return;
		}

		isLoggedIn = 1;

	lr_end_transaction ( "T10_Logon_AjaxLogonForm" , LR_PASS) ;
}


void loginForOrderHistory()
{
	web_url("LogonForm",
		"URL=https://{host}/shop/LogonForm?catalogId={catalogId}&myAcctMain=1&langId=-1&storeId={storeId}",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Snapshot=t58.inf",
		"Mode=HTML",
		LAST);

	lr_think_time ( FORM_TT ) ;
//	web_reg_save_param ( "addressIds" , "LB=<option id=\"" , "RB=\" value=" , "Ord=ALL" , LAST ) ;
//	web_reg_save_param ( "emailAddress1" , "LB=<input type=\"hidden\" name=\"email1\" value=\"" , "RB=\" id=\"email1\" />" , LAST ) ;
//	web_reg_save_param ( "authToken" , "LB=<input type=\"hidden\" name=\"authToken\" value=\"" , "RB=\"" , LAST ) ;
	web_reg_find ( "Text=entered is incorrect" , "SaveCount=loginerror_count" , LAST ) ;

	lr_start_transaction ( "T10_Logon_OrderHistory" ) ;

//		lr_start_sub_transaction ("T10_Logon_S01_LogonServlet","T10_Logon_OrderHistory");

			web_submit_data("LogonForOrderHistory",
			"Action=https://{host}/shop/Logon",
			"Method=POST",
			"TargetFrame=",
			"RecContentType=text/html",
			"Snapshot=t59.inf",
			"Mode=HTML",
			"EncodeAtSign=YES",
			ITEMDATA,
			"Name=emc", "Value=", ENDITEM,
			"Name=emcUserId", "Value=", ENDITEM,
			"Name=storeId", "Value={storeId}", ENDITEM,
			"Name=catalogId", "Value={catalogId}", ENDITEM,
			"Name=reLogonURL", "Value=LogonForm", ENDITEM,
			"Name=myAcctMain", "Value=1", ENDITEM,
			"Name=fromOrderId", "Value=*", ENDITEM,
			"Name=toOrderId", "Value=.", ENDITEM,
			"Name=deleteIfEmpty", "Value=*", ENDITEM,
			"Name=continue", "Value=1", ENDITEM,
			"Name=createIfEmpty", "Value=1", ENDITEM,
			"Name=calculationUsageId", "Value=-1", ENDITEM,
			"Name=updatePrices", "Value=0", ENDITEM,
			"Name=errorViewName", "Value=LogonForm", ENDITEM,
			"Name=previousPage", "Value=logon", ENDITEM,
			"Name=returnPage", "Value=", ENDITEM,
			"Name=visitorId_1", "Value=", ENDITEM,
			"Name=URL", "Value=AjaxLogonForm", ENDITEM, //08/02 Siva
//			"Name=URL", "Value=OrderItemMove?URL=AjaxLogonForm", ENDITEM,  //0707 recording
			"Name=logonId1" , "Value={logonidForOrderHistory}" , ENDITEM ,
//		    "Name=logonId1" , "Value=TCPPERF_US2_7927773678@CHILDRENSPLACE.COM" , ENDITEM ,
//			"Name=logonId1" , "Value=TCPPERF_US2_0664816429@CHILDRENSPLACE.COM" , ENDITEM ,
			"Name=logonPassword1", "Value=asdf1234", ENDITEM,
			"Name=rememberCheck", "Value=true", ENDITEM, 
			"Name=rememberMe", "Value=true", ENDITEM,  //AND-198              Persistent Cookie/"Remember Me" 
			LAST);
			

//		lr_end_sub_transaction ("T10_Logon_S01_LogonServlet", LR_AUTO);
/*
		lr_start_sub_transaction ("T10_Logon_S02_CreateCookieCmd","T10_Logon_OrderHistory");

		web_url ( "CreateCookieCmd" ,
				  "URL=https://{host}/webapp/wcs/stores/servlet/CreateCookieCmd?langId=-1&storeId={storeId}&goToPage=NoURL&catalogId={catalogId}" ,
				  "Resource=0" ,
				  "Mode=HTML" ,
				  LAST ) ;

		lr_end_sub_transaction ("T10_Logon_S02_CreateCookieCmd", LR_AUTO);
*/		
		if ( atoi ( lr_eval_string ( "{loginerror_count}" ) ) == 1 ) {
			//lr_fail_trans_with_error( "registered login failed for %d",lr_eval_string ("{logonidForOrderHistory}") ) ;
	//		lr_end_transaction ( "Registered - Regular - User Logon to Checkout" , LR_FAIL ) ;
			lr_end_transaction ( "T10_Logon_OrderHistory" , LR_FAIL) ;
			return;
		}

	lr_end_transaction ( "T10_Logon_OrderHistory" , LR_PASS) ;

} // end loginForOrderHistory()


void convertAndApplyPoints()
{
//	if (pointsBalance > 5000) and (converPoints random selector is positive)
//	{
	lr_think_time ( FORM_TT ) ;

	web_reg_save_param ( "myPlaceId" , "LB=<input type=\"hidden\" value=\"" , "RB=\" name=\"myPlaceId\" />", "Notfound=warning", LAST ) ;
	web_reg_save_param ( "myAccountId" , "LB=<input type=\"hidden\" value=\"" , "RB=\" name=\"accountId\" />", "Notfound=warning", LAST ) ;

	web_reg_save_param ( "addressIds" , "LB=<option id=\"" , "RB=\" value=" , "Ord=ALL" , "NotFound=Warning", LAST ) ;
	web_reg_save_param ( "TCPMyAddressBook" , "LB=edit shipping address\" id=\"TCPMyAddressBook_" , "RB=\"" , "Ord=ALL", "NotFound=Warning", LAST ) ;
	web_reg_save_param ( "emailAddress1" , "LB=<input type=\"hidden\" name=\"email1\" value=\"" , "RB=\" id=\"email1\" />" , "NotFound=Warning", LAST ) ;
	web_reg_save_param ( "authToken" , "LB=<input type=\"hidden\" name=\"authToken\" value=\"" , "RB=\"", "NotFound=Warning", LAST ) ;

	lr_start_transaction ( "T11_Convert Points To Coupon" ) ;

		lr_start_sub_transaction( "T11_Convert Points_S01_Click MyPlace Rewards", "T11_Convert Points To Coupon" );

		web_url("AjaxLogonForm",
			"URL=https://{host}/shop/AjaxLogonForm?catalogId={catalogId}&myAcctMain=1&langId=-1&storeId={storeId}",
			"Mode=HTTP",
			LAST);

		lr_end_sub_transaction( "T11_Convert Points_S01_Click MyPlace Rewards", LR_AUTO );

		web_reg_save_param ( "promocodeRewards" , "LB=\"javascript:applyLoyaltyCode('" , "RB=');", "NotFound=Warning", LAST ) ;

		lr_start_sub_transaction("T11_Convert Points_S02_TCPRedeemLoyaltyPoints", "T11_Convert Points To Coupon" );

		web_submit_data("TCPRedeemLoyaltyPoints",
				"Action=https://{host}/shop/TCPRedeemLoyaltyPoints",    //rhy 06/02/2016
				"Method=POST",
				"Mode=HTTP",
				ITEMDATA,
				"Name=storeId", "Value={storeId}", ENDITEM,
				"Name=langId", "Value=-1", ENDITEM,
				"Name=catalogId", "Value={catalogId}", ENDITEM,
				"Name=myPlaceId", "Value={myPlaceId}", ENDITEM,
				"Name=accountId", "Value={myAccountId}", ENDITEM,
				"Name=visitorId", "Value=[CS]v1|2B2AE4E305078968-600001048004DD05[CE]", ENDITEM, 
				"Name=amountToRedeem", "Value=5", ENDITEM,
				"Name=cpnWalletValue", "Value=0", ENDITEM, //this was zero in the recording
				LAST);

		lr_end_transaction("T11_Convert Points_S02_TCPRedeemLoyaltyPoints", LR_AUTO);

	lr_end_transaction ( "T11_Convert Points To Coupon" , LR_PASS) ;

// 	promocodeRewards - Y022053295A08021
	if ( strlen(lr_eval_string("{promocodeRewards}")) == 16 ) {
	
		viewCart();
		
	    applyPromoCode(1);
	}
	
//	else if
//	{
//		//lr_message("No points convereted");
//		return;
//	}
} //end convertAndApplyPoints

void submitShippingAddress()
{
	lr_think_time ( FORM_TT ) ;

	lr_start_transaction ( "T13_Submit Shipping Address" );

		lr_start_sub_transaction ( "T13_Submit_SA_S03_AjaxOrderProcessServiceOrderPrepare", "T13_Submit Shipping Address" ) ;

		web_submit_data ( "T13_Submit Shipping Address AjaxOrderProcessServiceOrderPrepare" ,
						  "Action=https://{host}/shop/AjaxOrderProcessServiceOrderPrepare" ,
						  "Method=POST" ,
						  "Mode=HTML" ,
						  ITEMDATA ,
							"Name=storeId" , "Value={storeId}" , ENDITEM ,
							"Name=langId" , "Value=-1" , ENDITEM ,
							"Name=catalogId" , "Value={catalogId}" , ENDITEM ,
							"Name=orderId" , "Value=." , ENDITEM ,
							"Name=requesttype" , "Value=ajax" , ENDITEM ,
						  LAST ) ;

		lr_end_sub_transaction ( "T13_Submit_SA_S03_AjaxOrderProcessServiceOrderPrepare" , LR_AUTO ) ;

		web_reg_save_param ( "piAmount" , "LB=name=\"OrderTotalAmount\" value=\"" , "RB=\"" , "NotFound=Warning", LAST ) ;

		lr_start_sub_transaction ( "T13_Submit_SA_S04_TCPOrderBillingCmd", "T13_Submit Shipping Address" );

		web_url ( "T13_Submit Shipping Address TCPOrderBillingCmd" ,
				  "URL=https://{host}/shop/TCPOrderBillingCmd?URL=OrderBillingView&langId=-1&storeId={storeId}&catalogId={catalogId}&forceShipmentType=1" ,
				  "Resource=0" ,
				  "Mode=HTML" ,
				  LAST ) ;

		lr_end_sub_transaction ( "T13_Submit_SA_S04_TCPOrderBillingCmd" , LR_AUTO ) ;

	lr_end_transaction ( "T13_Submit Shipping Address" , LR_PASS) ;
	
}// end submitShippingAddress

void submitShippingAddressAsGuest()
{
	lr_think_time ( FORM_TT ) ;

	lr_param_sprintf ( "ts" , "%ld%d" , time(&t) , rand() % 1000 ) ;

	lr_start_transaction ( "T13_Submit Shipping Address As Guest" ) ;

		lr_start_sub_transaction ( "T13_Submit Shipping Address TCPAVSResponseView", "T13_Submit Shipping Address As Guest") ;

				web_url ( "TCPAVSResponseView" ,
				  	"URL=https://{host}/webapp/wcs/stores/servlet/TCPAVSResponseView?callback=TCPAVSHandler&storeId={storeId}&catalogId={catalogId}&langId=-1&company=&firstName=teddy&lastName=bear&address1={guestAdr1}&address2=&city={guestCity}&state={guestState}&country={guestCountry}&zip={guestZip}&suite=&cvsLocalEndPoint=http%3A%2F%2F10.2.0.222%3A9082%2Fadv%2Fservices%2FAddressVerification&action=addressSearch&formpart=TCPsaveAddressForm-AddShippingAddress&_={ts}"
				  	"Resource=0" ,
				  	"Mode=HTML" ,
				  	LAST ) ;

		lr_end_transaction ( "T13_Submit Shipping Address TCPAVSResponseView" , LR_PASS ) ;

		web_reg_save_param ( "addressId" , "LB=\"addressId\": [\"" , "RB=\"]" , "NotFound=Warning", LAST ) ;

	 	lr_start_sub_transaction ( "T13_Submit_SA_S01_AjaxPersonChangeServiceAddressAdd", "T13_Submit Shipping Address As Guest" ) ;

		web_submit_data ( "T12_Submit Shipping Address AjaxPersonChangeServiceAddressAdd" ,
				  "Action=https://{host}/shop/AjaxPersonChangeServiceAddressAdd" ,
				  "Method=POST" ,
				  "Mode=HTML" ,
				  ITEMDATA ,
				  "Name=storeId" , "Value={storeId}" , ENDITEM ,
				  "Name=langId" , "Value=-1" , ENDITEM ,
				  "Name=catalogId" , "Value={catalogId}" , ENDITEM ,
				  "Name=authToken" , "Value={authToken}" , ENDITEM ,
				  "Name=email1" , "Value={emailAddress1}" , ENDITEM ,
				  "Name=addressType" , "Value=SB" , ENDITEM ,
				  "Name=originalAddressType" , "Value=SB" , ENDITEM ,
				  "Name=nickName" , "Value=SB_{ts}" , ENDITEM ,
				  "Name=avsAddressId" , "Value={ts}" , ENDITEM ,
				  "Name=firstName" , "Value=Teddy" , ENDITEM ,
				  "Name=ofirstName" , "Value=" , ENDITEM ,
				  "Name=lastName" , "Value=Bear" , ENDITEM ,
				  "Name=olastName" , "Value=" , ENDITEM ,
				  "Name=address1" , "Value={guestAdr1}" , ENDITEM ,
				  "Name=oaddress1" , "Value=" , ENDITEM ,
				  "Name=address2" , "Value=" , ENDITEM ,
				  "Name=oaddress2" , "Value=" , ENDITEM ,
				  "Name=city" , "Value={guestCity}" , ENDITEM ,
				  "Name=ocity" , "Value=" , ENDITEM ,
				  "Name=state" , "Value={guestState}" , ENDITEM ,
				  "Name=ostate" , "Value=" , ENDITEM ,
				  "Name=zipCode" , "Value={guestZip}" , ENDITEM ,
				  "Name=ozipCode" , "Value=" , ENDITEM ,
				  "Name=addressField3" , "Value={guestZip}" , ENDITEM ,
				  "Name=oaddressField3" , "Value=" , ENDITEM ,
				  "Name=country" , "Value={guestCountry}" , ENDITEM ,
				  "Name=ocountry" , "Value={guestCountry}" , ENDITEM ,
				  "Name=phone1" , "Value=4163218351" , ENDITEM ,
				  "Name=ophone1" , "Value=" , ENDITEM ,
				  "Name=address3" , "Value=" , ENDITEM ,
				  "Name=isZipMandatory" , "Value=true" , ENDITEM ,
				  "Name=requesttype" , "Value=ajax" , ENDITEM ,
				  LAST ) ;

	 	lr_end_sub_transaction ( "T13_Submit_SA_S01_AjaxPersonChangeServiceAddressAdd" , LR_PASS ) ;

		web_reg_find ( "Text=errorCode" , "SaveCount=error_count" , LAST ) ;

		lr_start_sub_transaction ( "T13_Submit_SA_S02_AjaxOrderChangeServiceShipInfoUpdate", "T13_Submit Shipping Address As Guest" ) ;

		web_submit_data ( "T13_Submit Shipping Address AjaxOrderChangeServiceShipInfoUpdate" ,
				  "Action=https://{host}/webapp/wcs/stores/servlet/AjaxOrderChangeServiceShipInfoUpdate" ,
				  "Method=POST" ,
				  "Mode=HTML" ,
				  ITEMDATA ,
					"Name=storeId" , "Value={storeId}" , ENDITEM ,
					"Name=langId" , "Value=-1" , ENDITEM ,
					"Name=catalogId" , "Value={catalogId}" , ENDITEM ,
					"Name=orderId" , "Value=." , ENDITEM ,
					"Name=calculationUsage" , "Value=-1,-2,-3,-4,-5,-6,-7" , ENDITEM ,
					"Name=allocate" , "Value=***" , ENDITEM ,
					"Name=backorder" , "Value=***" , ENDITEM ,
					"Name=remerge" , "Value=***" , ENDITEM ,
					"Name=check" , "Value=*n" , ENDITEM ,
					"Name=addressId" , "Value={addressId}" , ENDITEM ,
					"Name=requesttype" , "Value=ajax" , ENDITEM ,
				  LAST ) ;

		lr_end_sub_transaction ( "T13_Submit_SA_S02_AjaxOrderChangeServiceShipInfoUpdate" , LR_AUTO ) ;

		if ( atoi ( lr_eval_string ( "{error_count}" ) ) > 0 ) {
//				lr_fail_trans_with_error( "error return from AjaxOrderChangeServiceShipInfoUpdate From transaction:  T13_Submit Shipping Address As Guest" ) ;
				lr_end_transaction ( "T13_Submit Shipping Address As Guest" , LR_FAIL ) ;
				lr_exit(LR_EXIT_ITERATION_AND_CONTINUE, LR_PASS);
		}

		lr_start_sub_transaction ( "T13_Submit_SA_S03_AjaxOrderProcessServiceOrderPrepare", "T13_Submit Shipping Address As Guest" ) ;

		web_submit_data ( "T13_Submit Shipping Address AjaxOrderProcessServiceOrderPrepare" ,
				  "Action=https://{host}/webapp/wcs/stores/servlet/AjaxOrderProcessServiceOrderPrepare" ,
				  "Method=POST" ,
				  "Mode=HTML" ,
				  ITEMDATA ,
					"Name=storeId" , "Value={storeId}" , ENDITEM ,
					"Name=langId" , "Value=-1" , ENDITEM ,
					"Name=catalogId" , "Value={catalogId}" , ENDITEM ,
					"Name=orderId" , "Value=." , ENDITEM ,
					"Name=requesttype" , "Value=ajax" , ENDITEM ,
				  LAST ) ;

		lr_end_sub_transaction ( "T13_Submit_SA_S03_AjaxOrderProcessServiceOrderPrepare" , LR_AUTO ) ;

		web_reg_save_param ( "piAmount" , "LB=name=\"OrderTotalAmount\" value=\"" , "RB=\"" , "NotFound=Warning", LAST ) ;

		lr_start_sub_transaction ( "T13_Submit_SA_S04_TCPOrderBillingCmd", "T13_Submit Shipping Address As Guest" );

		web_url ( "T13_Submit Shipping Address TCPOrderBillingCmd" ,
				  "URL=https://{host}/webapp/wcs/stores/servlet/TCPOrderBillingCmd?URL=OrderBillingView&langId=-1&storeId={storeId}&catalogId={catalogId}&forceShipmentType=1" ,
				  "Resource=0" ,
				  "Mode=HTML" ,
				  LAST ) ;

		lr_end_sub_transaction ( "T13_Submit_SA_S04_TCPOrderBillingCmd" , LR_AUTO ) ;

	lr_end_transaction ( "T13_Submit Shipping Address As Guest" , LR_PASS) ;

} // end submitShippingAddressAsGuest

int selectShippingAddress()
{
	lr_think_time ( FORM_TT ) ;
	index = 2;
	lr_save_string ( lr_paramarr_idx( "addressIds" , index ) , "addressId" ) ;
	if ( atoi ( lr_eval_string ( "{TCPMyAddressBook_count}" ) ) > 0 ) {
		lr_save_string(lr_eval_string ( "{TCPMyAddressBook_1}" ),"addressId");
	}

	lr_start_transaction ( "T13_Select a Shipping Address" ) ;

	web_reg_save_param ( "zipCode" , "LB=\"zipCode\":\"" , "RB=\"," , "NotFound=Warning", LAST ) ;

	lr_start_sub_transaction ( "T13_Select_SA_S01_TCPAjaxAddressDisplayView", "T13_Select a Shipping Address" ) ;

	web_submit_data ( "T13_Select a Shipping Address_TCPAjaxAddressDisplayView" ,
			  "Action=https://{host}/shop/TCPAjaxAddressDisplayView?catalogId={catalogId}&langId=-1&storeId={storeId}" ,
			  "Method=POST" ,
			  "Mode=HTML" ,
			  ITEMDATA ,
				"Name=addressId" , "Value={addressId}" , ENDITEM ,
				"Name=objectId" , "Value=" , ENDITEM ,
				"Name=requesttype" , "Value=ajax" , ENDITEM ,
			LAST ) ;

	lr_end_sub_transaction ( "T13_Select_SA_S01_TCPAjaxAddressDisplayView", LR_AUTO ) ;

	lr_start_sub_transaction ( "T13_Select_SA_S02_AjaxOrderChangeServiceShipInfoUpdate", "T13_Select a Shipping Address" ) ;

	web_submit_data ( "T13_Select a Shipping Address_AjaxOrderChangeServiceShipInfoUpdate" ,
			  "Action=https://{host}/shop/AjaxOrderChangeServiceShipInfoUpdate" ,
			  "Method=POST" ,
			  "Mode=HTML" ,
			  ITEMDATA ,
				"Name=storeId" , "Value={storeId}" , ENDITEM ,
				"Name=langId" , "Value=-1" , ENDITEM ,
				"Name=catalogId" , "Value={catalogId}" , ENDITEM ,
				"Name=orderId" , "Value=." , ENDITEM ,
				"Name=calculationUsage" , "Value=-1,-2,-3,-4,-5,-6,-7" , ENDITEM ,
				"Name=allocate" , "Value=***" , ENDITEM ,
				"Name=backorder" , "Value=***" , ENDITEM ,
				"Name=remerge" , "Value=***" , ENDITEM ,
				"Name=check" , "Value=*n" , ENDITEM ,
				"Name=addressId" , "Value={addressId}" , ENDITEM ,
				"Name=requesttype" , "Value=ajax" , ENDITEM ,
			  LAST ) ;

	lr_end_sub_transaction ( "T13_Select_SA_S02_AjaxOrderChangeServiceShipInfoUpdate", LR_AUTO ) ;

	lr_start_sub_transaction ( "T13_Select_SA_S03_TCPTraditionalAjaxShippingDetailsURL", "T13_Select a Shipping Address" ) ;	
	
	web_submit_data("TCPTraditionalAjaxShippingDetailsURL", 
		"Action=https://{host}/shop/TCPTraditionalAjaxShippingDetailsURL?catalogId={catalogId}&langId=-1&storeId={storeId}", 
		"Method=POST", 
		"TargetFrame=", 
		"RecContentType=text/html", 
		"Mode=HTML", 
		ITEMDATA, 
		"Name=shipmentDetailsArea", "Value=update", ENDITEM, 
		"Name=objectId", "Value=", ENDITEM, 
		"Name=requesttype", "Value=ajax", ENDITEM, 
		LAST);
	
	lr_end_sub_transaction ( "T13_Select_SA_S03_TCPTraditionalAjaxShippingDetailsURL", LR_AUTO ) ;
	
	lr_start_sub_transaction ( "T13_Select_SA_S04_TCPCurrentOrderInformationView", "T13_Select a Shipping Address" ) ;

	web_submit_data ( "T13_Select a Shipping Address_TCPCurrentOrderInformationView" ,
			  "Action=https://{host}/shop/TCPCurrentOrderInformationView?catalogId={catalogId}&orderId=&langId=-1&storeId={storeId}" ,
			  "Method=POST" ,
			  "Mode=HTML" ,
			  ITEMDATA ,
			  "Name=objectId" , "Value=" , ENDITEM ,
			  "Name=requesttype" , "Value=ajax" , ENDITEM ,
			  LAST ) ;

	lr_end_sub_transaction ( "T13_Select_SA_S04_TCPCurrentOrderInformationView", LR_AUTO ) ;
	

	lr_end_transaction ( "T13_Select a Shipping Address" , LR_PASS ) ;
	return 0;
} // end selectShippingAddress

void selectShipMode()
{
	lr_think_time ( FORM_TT ) ;

	lr_start_transaction ( "T12_Select Ship Mode" ) ;

		lr_start_sub_transaction ( "T12_Select_SM_S01_AjaxOrderChangeServiceShipInfoUpdate", "T12_Select Ship Mode" ) ;

		web_submit_data ( "T12_Select Ship Mode_AjaxOrderChangeServiceShipInfoUpdate" ,
				  "Action=https://{host}/shop/AjaxOrderChangeServiceShipInfoUpdate" ,
				  "Method=POST" ,
				  "Mode=HTML" ,
				  ITEMDATA ,
					"Name=storeId" , "Value={storeId}" , ENDITEM ,
					"Name=langId" , "Value=-1" , ENDITEM ,
					"Name=catalogId" , "Value={catalogId}" , ENDITEM ,
					"Name=orderId" , "Value=." , ENDITEM ,
					"Name=calculationUsage" , "Value=-1,-2,-5,-6,-7" , ENDITEM ,
					"Name=allocate" , "Value=***" , ENDITEM ,
					"Name=backorder" , "Value=***" , ENDITEM ,
					"Name=remerge" , "Value=***" , ENDITEM ,
					"Name=check" , "Value=*n" , ENDITEM ,
					"Name=shipModeId" , "Value={shipmode_id}" , ENDITEM ,
					"Name=requesttype" , "Value=ajax" , ENDITEM ,
				  LAST ) ;

		lr_end_sub_transaction ( "T12_Select_SM_S01_AjaxOrderChangeServiceShipInfoUpdate", LR_AUTO ) ;

		lr_start_sub_transaction ( "T12_Select_SM_S02_TCPTraditionalAjaxShippingDetailsURL", "T12_Select Ship Mode" ) ;	
		
		web_submit_data("TCPTraditionalAjaxShippingDetailsURL", 
			"Action=https://{host}/shop/TCPTraditionalAjaxShippingDetailsURL?catalogId={catalogId}&langId=-1&storeId={storeId}", 
			"Method=POST", 
			"TargetFrame=", 
			"RecContentType=text/html", 
			"Mode=HTML", 
			ITEMDATA, 
			"Name=shipmentDetailsArea", "Value=update", ENDITEM, 
			"Name=objectId", "Value=", ENDITEM, 
			"Name=requesttype", "Value=ajax", ENDITEM, 
			LAST);
		
		lr_end_sub_transaction ( "T12_Select_SM_S02_TCPTraditionalAjaxShippingDetailsURL", LR_AUTO ) ;

		lr_start_sub_transaction ( "T12_Select_SM_S03_TCPCurrentOrderInformationView", "T12_Select Ship Mode" ) ;

		web_submit_data ( "T12_Select Ship Mode_TCPCurrentOrderInformationView" ,
				  "Action=https://{host}/shop/TCPCurrentOrderInformationView?catalogId={catalogId}&orderId=&langId=-1&storeId={storeId}" ,
				  "Method=POST" ,
				  "Mode=HTML" ,
				  ITEMDATA ,
				  "Name=objectId" , "Value=" , ENDITEM ,
				  "Name=requesttype" , "Value=ajax" , ENDITEM ,
				  LAST ) ;

		lr_end_sub_transaction ( "T12_Select_SM_S03_TCPCurrentOrderInformationView", LR_AUTO ) ;

		
	lr_end_transaction ( "T12_Select Ship Mode" , LR_PASS ) ;
	
} // end selectShipMode()

int selectBillingAddress()
{
		lr_think_time ( FORM_TT ) ;

		lr_start_transaction ( "T14_Select Billing Address" ) ;

		lr_start_sub_transaction ( "T14_Select_BA_S01_TCPBillingAddressDisplayView", "T14_Select Billing Address" ) ;

		web_submit_data ( "T14_Select Billing Address_TCPBillingAddressDisplayView" ,
//				  "Action=https://{host}/webapp/wcs/stores/servlet/TCPBillingAddressDisplayView?catalogId={catalogId}&langId=-1&storeId={storeId}" ,
				  "Action=https://{host}/shop/TCPBillingAddressDisplayView?catalogId={catalogId}&langId=-1&storeId={storeId}" ,
				  "Method=POST" ,
				  "Mode=HTML" ,
				  ITEMDATA ,
					"Name=selectedAddressId" , "Value={addressId}" , ENDITEM ,
					"Name=shipAddressId" , "Value={addressId}" , ENDITEM ,
					"Name=objectId" , "Value=tcpBillingAddressSelectBoxArea" , ENDITEM ,
					"Name=requesttype" , "Value=ajax" , ENDITEM ,
				  LAST ) ;

		lr_end_sub_transaction ( "T14_Select_BA_S01_TCPBillingAddressDisplayView", LR_AUTO ) ;

		web_reg_save_param ( "authToken" , "LB=name=\"authToken\" value=\"" , "RB=\" id=\"WC_ShopcartAddressFormDisplay_inputs_authToken", "Notfound=warning", LAST ) ;
		
		lr_start_sub_transaction ( "T14_Select_BA_S02_TCPAddressEditView", "T14_Select Billing Address" ) ;

		web_submit_data ( "T14_Select Billing Address_TCPAddressEditView" ,
//				  "Action=https://{host}/shop/TCPAddressEditView?catalogId={catalogId}&langId=-1&storeId={storeId}" ,
				  "Action=https://{host}/webapp/wcs/stores/servlet/TCPAddressEditView?catalogId={catalogId}&langId=-1&storeId={storeId}" ,
				  "Method=POST" ,
				  "Mode=HTML" ,
				  ITEMDATA ,
					"Name=addressId" , "Value={addressId}" , ENDITEM ,
					"Name=fromPage" , "Value=billingPage" , ENDITEM ,
					"Name=shipAddressId" , "Value={addressId}" , ENDITEM ,
					"Name=objectId" , "Value=editBillingAddressArea" , ENDITEM ,
					"Name=requesttype" , "Value=ajax" , ENDITEM ,
					LAST ) ;
	/*				
		HttpRetCode = web_get_int_property(HTTP_INFO_RETURN_CODE);
			lr_output_message("HTTP-%d%s",  HttpRetCode, lr_eval_string(" on T14_Select_BA_S02_TCPAddressEditView -AddressId={addressId}, UserId={logonid}") );

		if (HttpRetCode != 200) {

			lr_set_debug_message(LR_MSG_CLASS_EXTENDED_LOG | LR_MSG_CLASS_FULL_TRACE, LR_SWITCH_ON );
			lr_output_message("HTTP-%d%s",  HttpRetCode, lr_eval_string(" on T14_Select_BA_S02_TCPAddressEditView -AddressId={addressId}, UserId={logonid}") );
			lr_set_debug_message(LR_MSG_CLASS_EXTENDED_LOG | LR_MSG_CLASS_FULL_TRACE, LR_SWITCH_OFF );
		}
*/
//		lr_end_sub_transaction ( "T14_Select_BA_S02_TCPAddressEditView", LR_PASS ) ;
		if (lr_end_sub_transaction ( "T14_Select_BA_S02_TCPAddressEditView", LR_AUTO ) != 0)
				lr_log_message("Failed addressId=%s, user=%s", lr_eval_string("{addressId}"), lr_eval_string("{logonid}"));

	lr_end_transaction ( "T14_Select Billing Address" , LR_PASS ) ;
	return 0;
}// end selectBillingAddress()

void selectBillingAddressAsGuest()
{
		lr_think_time ( FORM_TT ) ;

		lr_start_transaction ( "T14_Select Billing Address As Guest" ) ;

		lr_start_sub_transaction ( "T14_Select_BA_Guest_S01_TCPBillingAddressDisplayView", "T14_Select Billing Address As Guest" ) ;

		web_submit_data ( "T14_Select Billing Address_TCPBillingAddressDisplayView" ,
				  "Action=https://{host}/shop/TCPBillingAddressDisplayView?catalogId={catalogId}&langId=-1&storeId={storeId}" ,
				  "Method=POST" ,
				  "Mode=HTML" ,
				  ITEMDATA ,
					"Name=selectedAddressId" , "Value={addressId}" , ENDITEM ,
					"Name=shipAddressId" , "Value={addressId}" , ENDITEM ,
					"Name=objectId" , "Value=tcpBillingAddressSelectBoxArea" , ENDITEM ,
					"Name=requesttype" , "Value=ajax" , ENDITEM ,
				  LAST ) ;

		lr_end_sub_transaction ( "T14_Select_BA_Guest_S01_TCPBillingAddressDisplayView", LR_AUTO ) ;

		lr_start_sub_transaction ( "T14_Select_BA_Guest_S02_TCPAddressEditView", "T14_Select Billing Address As Guest" ) ;

		web_submit_data ( "T14_Select Billing Address_TCPAddressEditView" ,
				  "Action=https://{host}/webapp/wcs/stores/servlet/TCPAddressEditView?catalogId={catalogId}&langId=-1&storeId={storeId}" ,
				  "Method=POST" ,
				  "Mode=HTML" ,
				  ITEMDATA ,
					"Name=addressId" , "Value={addressId}" , ENDITEM ,
					"Name=fromPage" , "Value=billingPage" , ENDITEM ,
					"Name=shipAddressId" , "Value={addressId}" , ENDITEM ,
					"Name=objectId" , "Value=editBillingAddressArea" , ENDITEM ,
					"Name=requesttype" , "Value=ajax" , ENDITEM ,
					LAST ) ;
/*					
		HttpRetCode = web_get_int_property(HTTP_INFO_RETURN_CODE);
			lr_output_message("HTTP-%d%s",  HttpRetCode, lr_eval_string(" on T14_Select_BA_S02_TCPAddressEditView -AddressId={addressId}, UserId={logonid}") );

		if (HttpRetCode != 200) {

			lr_set_debug_message(LR_MSG_CLASS_EXTENDED_LOG | LR_MSG_CLASS_FULL_TRACE, LR_SWITCH_ON );
			lr_output_message("HTTP-%d%s",  HttpRetCode, lr_eval_string(" on T14_Select_BA_S02_TCPAddressEditView -AddressId={addressId}, UserId={logonid}") );
			lr_set_debug_message(LR_MSG_CLASS_EXTENDED_LOG | LR_MSG_CLASS_FULL_TRACE, LR_SWITCH_OFF );
		}
*/
		lr_end_sub_transaction ( "T14_Select_BA_Guest_S02_TCPAddressEditView", LR_AUTO ) ;

	lr_end_transaction ( "T14_Select Billing Address As Guest" , LR_PASS ) ;

}// end selectBillingAddressAsGuest()

void submitBillingAddressAsGuest()
{
	lr_think_time ( FORM_TT ) ;
//	lr_param_sprintf ( "emailAddress2" , "%s" , lr_eval_string ( "{logonidRegister}" ) ) ;
	lr_param_sprintf ( "emailAddress1" , "g%s%ld@childrensplace.com" , lr_eval_string ( "{vuser}" ) , time(&t) ) ;
//	web_reg_save_param ( "billingAddressId" , "LB=\"addressId\": [\"" , "RB=\"]" , LAST ) ;

	lr_start_transaction ( "T15_Submit Billing Address As Guest" ) ;

			lr_start_sub_transaction("T15_Submit_BA_S01_TCPAjaxEmailVerificationCmd", "T15_Submit Billing Address As Guest");  //ry-yk 03182015

				web_url ( "T15_Submit Shipping Address As Guest_TCPAjaxEmailVerificationCmd" ,
						 "URL=https://{host}/webapp/wcs/stores/servlet/TCPAjaxEmailVerificationCmd?email={emailAddress1}&page=billing&requesttype=ajax" ,
						 "Resource=0" ,
						 "Mode=HTML" ,
						 LAST ) ;

			lr_end_sub_transaction("T15_Submit_BA_S01_TCPAjaxEmailVerificationCmd", LR_AUTO);

	/* Removed in R2
			lr_start_sub_transaction ( "T15_Submit_BA_S01_AjaxPersonChangeServiceAddressAdd", "T15_Submit Billing Address As Guest" ) ;

				web_submit_data ( "T15_Submit_BA_AjaxPersonChangeServiceAddressAdd" ,
							  "Action=https://{host}/webapp/wcs/stores/servlet/AjaxPersonChangeServiceAddressAdd" ,
							  "Method=POST" ,
							  "Mode=HTML" ,
							  ITEMDATA ,
							  "Name=storeId" , "Value={storeId}" , ENDITEM ,
							  "Name=langId" , "Value=-1" , ENDITEM ,
							  "Name=catalogId" , "Value={catalogId}" , ENDITEM ,
							  "Name=authToken" , "Value={authToken}" , ENDITEM ,
							  "Name=email1" , "Value={emailAddress1}" , ENDITEM ,
							  "Name=addressType" , "Value=SB" , ENDITEM ,
							  "Name=originalAddressType" , "Value=SB" , ENDITEM ,
							  "Name=nickName" , "Value=billingPageSB_SP_{ts}" , ENDITEM ,
							  "Name=avsAddressId" , "Value={addressId}" , ENDITEM ,
							  "Name=addressModified" , "Value=true" , ENDITEM ,
							  "Name=firstName" , "Value=Teddy" , ENDITEM ,
							  "Name=lastName" , "Value=Bear" , ENDITEM ,
							  "Name=address1" , "Value={guestAdr1}" , ENDITEM ,
							  "Name=address2" , "Value=" , ENDITEM ,
							  "Name=city" , "Value={guestCity}" , ENDITEM ,
							  "Name=state" , "Value={guestState}" , ENDITEM ,
							  "Name=zipCode" , "Value={guestZip}" , ENDITEM ,
							  "Name=addressField3" , "Value={guestZip}" , ENDITEM ,
							  "Name=country" , "Value={guestCountry}" , ENDITEM ,
							  "Name=phone1" , "Value=4163218351" , ENDITEM ,
							  "Name=email1" , "Value={emailAddress1}" , ENDITEM ,
							  "Name=email1Verify" , "Value={emailAddress1}" , ENDITEM ,
							  "Name=requesttype" , "Value=ajax" , ENDITEM ,
						 	  LAST ) ;

			lr_end_sub_transaction ( "T15_Submit_BA_S01_AjaxPersonChangeServiceAddressAdd", LR_AUTO ) ;
		*/
			lr_start_sub_transaction ( "T15_Submit_BA_S02_AjaxUpdateBillingAddressId", "T15_Submit Billing Address As Guest" ) ;

			web_submit_data ( "T15_Submit_BA_AjaxUpdateBillingAddressId" ,
					  "Action=https://{host}/webapp/wcs/stores/servlet/AjaxUpdateBillingAddressId" ,
					  "Method=POST" ,
					  "Mode=HTML" ,
					  ITEMDATA ,
					  "Name=storeId" , "Value={storeId}" , ENDITEM ,
					  "Name=langId" , "Value=-1" , ENDITEM ,
					  "Name=catalogId" , "Value={catalogId}" , ENDITEM ,
					  "Name=orderId" , "Value={orderId}" , ENDITEM ,
					  "Name=addressId" , "Value={addressId}" , ENDITEM ,
					  "Name=requesttype" , "Value=ajax" , ENDITEM ,
					  "Name=email" , "Value={emailAddress1}" , ENDITEM ,
					  "Name=newsletterSignUp" , "Value=true" , ENDITEM ,
					  "Name=response" , "Value=accept_all::false:false" , ENDITEM ,
					  LAST ) ;

			lr_end_sub_transaction ( "T15_Submit_BA_S02_AjaxUpdateBillingAddressId", LR_AUTO ) ;

			web_reg_find ( "Text=piId" , "SaveCount=pi_id_count" , LAST ) ;
			web_reg_find ( "Text=errorCode" , "SaveCount=error_count" , LAST ) ;

			lr_start_sub_transaction( "T15_Submit_BA_S03_AjaxOrderChangeServicePIAdd", "T15_Submit Billing Address As Guest" );

				web_submit_data ( "T15_Submit_BA_AjaxOrderChangeServicePIAdd" ,
						  "Action=https://{host}/webapp/wcs/stores/servlet/AjaxOrderChangeServicePIAdd" ,
						  "Method=POST" ,
						  "Mode=HTML" ,
						  ITEMDATA ,
						  "Name=storeId" , "Value={storeId}" , ENDITEM ,
						  "Name=langId" , "Value=-1" , ENDITEM ,
						  "Name=catalogId" , "Value={catalogId}" , ENDITEM ,
						  "Name=valueFromPaymentTC" , "Value= " , ENDITEM ,
						  "Name=paymentTCId" , "Value=" , ENDITEM ,
						  //"Name=payMethodId" , "Value=CITI" , ENDITEM ,
						  "Name=payMethodId" , "Value=VISA" , ENDITEM ,
						  //"Name=payMethodId" , "Value=COMPASSVISA" , ENDITEM ,
						  "Name=piAmount" , "Value={piAmount}" , ENDITEM ,
						  "Name=billing_address_id" , "Value={addressId}" , ENDITEM ,
						  "Name=cc_brand" , "Value=Visa" , ENDITEM ,
						  "Name=cc_cvc" , "Value=111" , ENDITEM ,
						  //"Name=account" , "Value=6011644423719045" , ENDITEM ,
						  "Name=account" , "Value=4012000033330026" , ENDITEM ,
						  "Name=expire_month" , "Value=12" , ENDITEM ,
						  "Name=expire_year" , "Value=2016" , ENDITEM ,
						  "Name=check_routing_number" , "Value= " , ENDITEM ,
						  "Name=checkAccountNumber" , "Value= " , ENDITEM ,
						  "Name=checkRoutingNumber" , "Value= " , ENDITEM ,
						  "Name=requesttype" , "Value=ajax" , ENDITEM ,
						  "Name=authToken" , "Value={authToken}" ,ENDITEM ,
						  "Name=callFromPage" , "Value=CheckoutBillingInfo" ,ENDITEM ,
						  LAST ) ;

			lr_end_sub_transaction("T15_Submit_BA_S03_AjaxOrderChangeServicePIAdd", LR_AUTO);

			if ( atoi ( lr_eval_string ( "{pi_id_count}" ) ) == 0 ) {
					//lr_fail_trans_with_error( "payment instruction id not found after PIAdd - payment problem" ) ;
					lr_end_transaction ( "T15_Submit Billing Address As Guest" , LR_FAIL ) ;
					lr_exit(LR_EXIT_ITERATION_AND_CONTINUE, LR_PASS);
			}

			if ( atoi ( lr_eval_string ( "{error_count}" ) ) > 0 ) {
					//lr_fail_trans_with_error( "potential error response from PIAdd - payment problem" ) ;
					lr_end_transaction ( "T15_Submit Billing Address As Guest" , LR_FAIL ) ;
					lr_exit(LR_EXIT_ITERATION_AND_CONTINUE, LR_PASS);
			}

			lr_start_sub_transaction( "T15_Submit_BA_S04_OrderCalculate", "T15_Submit Billing Address As Guest" );

			web_url ( "T15_Submit_BA_OrderCalculate" ,
					  "URL=https://{host}/webapp/wcs/stores/servlet/OrderCalculate?calculationUsageId=-1&calculationUsageId=-2&calculationUsageId=-3&calculationUsageId=-4&calculationUsageId=-5&calculationUsageId=-6&calculationUsageId=-7&URL=TCPSingleShipmentOrderSummaryView&langId=-1&storeId={storeId}&catalogId={catalogId}&purchaseorder_id=&quickCheckoutProfileForPayment=false" ,
					  "Resource=0" ,
					  "Mode=HTML" ,
					  LAST ) ;

			lr_end_sub_transaction("T15_Submit_BA_S04_OrderCalculate", LR_AUTO);

			lr_start_sub_transaction( "T15_Submit_BA_S05_CreateCookieCmd", "T15_Submit Billing Address As Guest" );

			web_url ( "T15_Submit_BA_CreateCookieCmd" ,
					  "URL=https://{host}/webapp/wcs/stores/servlet/CreateCookieCmd?langId=-1&storeId={storeId}&goToPage=NoURL&catalogId={catalogId}" ,
					  "Resource=0" ,
					  "Mode=HTML" ,
					  LAST ) ;

			lr_end_sub_transaction("T15_Submit_BA_S05_CreateCookieCmd", LR_AUTO);

			createCookieCmd();

		lr_end_transaction ( "T15_Submit Billing Address As Guest" , LR_PASS) ;

} // end submitBillingAddressAsGuest()

int submitBillingAddress()
{
//	lr_think_time ( FORM_TT ) ;
//	lr_save_string ( lr_eval_string ( "{addressId}" ) , "billingAddressId" ) ;
//	lr_param_sprintf ( "emailAddress1" , "g%s%ld@childrensplace.com" , lr_eval_string ( "{vuser}" ) , time(&t) ) ;
//	web_reg_save_param ( "billingAddressId" , "LB=\"addressId\": [\"" , "RB=\"]" , LAST ) ;
	lr_param_sprintf ( "ts" , "%ld%d" , time(&t) , rand() % 1000 ) ;
	lr_save_string ( lr_eval_string ( "{addressId}" ) , "billingAddressId" ) ;
	lr_think_time ( FORM_TT ) ;

	lr_start_transaction ( "T15_Submit Billing Address" ) ;
/* Issue in R1
		lr_start_sub_transaction ( "T15_Submit_BA_S01_AjaxPersonChangeServiceAddressAdd", "T15_Submit Billing Address" ) ;

			web_submit_data ( "T15_Submit_BA_AjaxPersonChangeServiceAddressAdd" ,
				  "Action=https://{host}/webapp/wcs/stores/servlet/AjaxPersonChangeServiceAddressAdd" ,
				  "Method=POST" ,
				  "Mode=HTML" ,
				  ITEMDATA ,
				  "Name=storeId" , "Value={storeId}" , ENDITEM ,
				  "Name=langId" , "Value=-1" , ENDITEM ,
				  "Name=catalogId" , "Value={catalogId}" , ENDITEM ,
				  "Name=authToken" , "Value={authToken}" , ENDITEM ,
				  "Name=addressType" , "Value=SB" , ENDITEM ,
				  "Name=detailedAddressType" , "Value=BILLING" , ENDITEM ,
				  "Name=callFromPage" , "Value=CheckoutBillingInfo" , ENDITEM ,
				  "Name=nickName" , "Value=billingPageSB_SP_{ts}" , ENDITEM ,
				  "Name=avsAddressId" , "Value={addressId}" , ENDITEM ,
				  "Name=addressModified" , "Value=false" , ENDITEM ,
				  "Name=firstName" , "Value=Teddy" , ENDITEM ,
				  "Name=lastName" , "Value=Bear" , ENDITEM ,
				  "Name=address1" , "Value={guestAdr1}" , ENDITEM ,
				  "Name=address2" , "Value=" , ENDITEM ,
				  "Name=city" , "Value={guestCity}" , ENDITEM ,
				  "Name=state" , "Value={guestState}" , ENDITEM ,
				  "Name=zipCode" , "Value={guestZip}" , ENDITEM ,
				  "Name=IsZipMandatory" , "Value=true" , ENDITEM ,
				  "Name=addressField3" , "Value={guestZip}" , ENDITEM ,
				  "Name=country" , "Value={guestCountry}" , ENDITEM ,
				  "Name=phone1" , "Value=4163218351" , ENDITEM ,
				  "Name=email1" , "Value={logonid}" , ENDITEM ,
				  "Name=requesttype" , "Value=ajax" , ENDITEM ,
			 	  LAST ) ;

		lr_end_sub_transaction ( "T15_Submit_BA_S01_AjaxPersonChangeServiceAddressAdd", LR_AUTO ) ;
*/
		lr_start_sub_transaction ( "T15_Submit_BA_S02_AjaxUpdateBillingAddressId", "T15_Submit Billing Address" ) ;

			web_submit_data ( "T15_Submit_BA_AjaxUpdateBillingAddressId" ,
				  "Action=https://{host}/webapp/wcs/stores/servlet/AjaxUpdateBillingAddressId" ,
				  "Method=POST" ,
				  "Mode=HTML" ,
				  ITEMDATA ,
				  "Name=storeId" , "Value={storeId}" , ENDITEM ,
				  "Name=langId" , "Value=-1" , ENDITEM ,
				  "Name=catalogId" , "Value={catalogId}" , ENDITEM ,
				  "Name=orderId" , "Value={orderId}" , ENDITEM ,
				  "Name=addressId" , "Value={billingAddressId}" , ENDITEM ,
				  "Name=requesttype" , "Value=ajax" , ENDITEM ,
				  "Name=newsletterSignUp" , "Value=false" , ENDITEM ,
				  LAST ) ;

		lr_end_sub_transaction ( "T15_Submit_BA_S02_AjaxUpdateBillingAddressId", LR_AUTO ) ;
/*
		lr_start_sub_transaction ( "T15_Select_BA_S03_TCPBillingAddressDisplayView", "T15_Submit Billing Address" ) ;

			web_submit_data ( "T15_Select Billing Address_TCPBillingAddressDisplayView" ,
				  "Action=https://{host}/shop/TCPBillingAddressDisplayView?catalogId={catalogId}&langId=-1&storeId={storeId}" ,
				  "Method=POST" ,
				  "Mode=HTML" ,
				  ITEMDATA ,
				  "Name=selectedAddressId" , "Value={billingAddressId}" , ENDITEM ,
				  "Name=shipAddressId" , "Value={addressId}" , ENDITEM ,
				  "Name=objectId" , "Value=tcpBillingAddressSelectBoxArea" , ENDITEM ,
				  "Name=requesttype" , "Value=ajax" , ENDITEM ,
				  LAST ) ;

		lr_end_sub_transaction ( "T15_Select_BA_S03_TCPBillingAddressDisplayView", LR_AUTO ) ;

		lr_start_sub_transaction ( "T15_Select_BA_S04_TCPAddressEditView", "T15_Submit Billing Address" ) ;

			web_submit_data ( "T15_Select Billing Address_TCPAddressEditView" ,
				  "Action=https://{host}/webapp/wcs/stores/servlet/TCPAddressEditView?catalogId={catalogId}&langId=-1&storeId={storeId}" ,
				  "Method=POST" ,
				  "Mode=HTML" ,
				  ITEMDATA ,
				  "Name=addressId" , "Value={billingAddressId}" , ENDITEM ,
				  "Name=fromPage" , "Value=billingPage" , ENDITEM ,
				  "Name=shipAddressId" , "Value={addressId}" , ENDITEM ,
				  "Name=objectId" , "Value=editBillingAddressArea" , ENDITEM ,
				  "Name=requesttype" , "Value=ajax" , ENDITEM ,
				  LAST ) ;

		lr_end_sub_transaction ( "T15_Select_BA_S04_TCPAddressEditView", LR_AUTO ) ;
*/
		web_reg_find ( "Text=piId" , "SaveCount=pi_id_count" , LAST ) ;
		web_reg_find ( "Text=errorCode" , "SaveCount=error_count" , LAST ) ;

		//randomPercent = atoi(lr_eval_string("{RANDOM_PERCENT}"));

		if (atoi(lr_eval_string("{RANDOM_PERCENT}")) <= OFFLINE_PLUGIN) {

			lr_start_sub_transaction( "T15_Submit_BA_S05_AjaxOrderChangeServicePIAdd_OFFLINE_PLUGIN", "T15_Submit Billing Address" );

				web_submit_data ( "T15_Submit_BA_AjaxOrderChangeServicePIAdd_OFFLINE_PLUGIN" ,
						  "Action=https://{host}/webapp/wcs/stores/servlet/AjaxOrderChangeServicePIAdd" ,
						  "Method=POST" ,
						  "Mode=HTML" ,
						  ITEMDATA ,
						  "Name=storeId" , "Value={storeId}" , ENDITEM ,
						  "Name=langId" , "Value=-1" , ENDITEM ,
						  "Name=catalogId" , "Value={catalogId}" , ENDITEM ,
						  "Name=valueFromPaymentTC" , "Value= " , ENDITEM ,
						  "Name=paymentTCId" , "Value=" , ENDITEM ,
						  "Name=payMethodId" , "Value=VISA" , ENDITEM ,
						  //"Name=payMethodId" , "Value=COMPASSVISA" , ENDITEM ,
						  "Name=piAmount" , "Value={piAmount}" , ENDITEM ,
						  "Name=billing_address_id" , "Value={billingAddressId}" , ENDITEM ,
						  "Name=cc_brand" , "Value=Visa" , ENDITEM ,
						  "Name=cc_cvc" , "Value=111" , ENDITEM ,
						  //"Name=account" , "Value=6006491259499906427" , ENDITEM ,
						  "Name=account" , "Value=4012000033330026" , ENDITEM ,
//						  "Name=account" , "Value=2818526303813893" , ENDITEM ,
//						  "Name=account1" , "Value=************3893" , ENDITEM ,
						  "Name=expire_month" , "Value=12" , ENDITEM ,
						  "Name=expire_year" , "Value=2016" , ENDITEM ,
						  "Name=check_routing_number" , "Value= " , ENDITEM ,
						  "Name=checkAccountNumber" , "Value= " , ENDITEM ,
						  "Name=checkRoutingNumber" , "Value= " , ENDITEM ,
						  "Name=requesttype" , "Value=ajax" , ENDITEM ,
						  "Name=authToken" , "Value={authToken}" ,ENDITEM ,
						  "Name=callFromPage" , "Value=CheckoutBillingInfo" ,ENDITEM ,
						  LAST ) ;

			lr_end_sub_transaction("T15_Submit_BA_S05_AjaxOrderChangeServicePIAdd_OFFLINE_PLUGIN", LR_AUTO);

		} else if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= PLCC) {

			lr_start_sub_transaction( "T15_Submit_BA_S05_AjaxOrderChangeServicePIAdd_PLCC", "T15_Submit Billing Address" );

				web_submit_data ( "T15_Submit_BA_AjaxOrderChangeServicePIAdd_PLCC" ,
					  "Action=https://{host}/webapp/wcs/stores/servlet/AjaxOrderChangeServicePIAdd" ,
					  "Method=POST" ,
					  "Mode=HTML" ,
					  ITEMDATA ,
					  "Name=storeId" , "Value={storeId}" , ENDITEM ,
					  "Name=langId" , "Value=-1" , ENDITEM ,
					  "Name=catalogId" , "Value={catalogId}" , ENDITEM ,
					  "Name=valueFromPaymentTC" , "Value= " , ENDITEM ,
					  "Name=paymentTCId" , "Value=" , ENDITEM ,
					  "Name=payMethodId" , "Value=CITIPlaceCard" , ENDITEM ,
					  "Name=piAmount" , "Value={piAmount}" , ENDITEM ,
					  "Name=billing_address_id" , "Value={billingAddressId}" , ENDITEM ,
					  "Name=cc_brand" , "Value=PLACE CARD" , ENDITEM ,
					  "Name=cc_cvc" , "Value=" , ENDITEM ,
					  "Name=account" , "Value=6011644423719045" , ENDITEM ,
//					  "Name=account" , "Value={PLACE_CARD_ACCOUNT}" , ENDITEM , //6011644423719003
					  "Name=expire_month" , "Value=12" , ENDITEM ,
					  "Name=expire_year" , "Value=2015" , ENDITEM ,
					  "Name=check_routing_number" , "Value= " , ENDITEM ,
					  "Name=checkAccountNumber" , "Value= " , ENDITEM ,
					  "Name=checkRoutingNumber" , "Value= " , ENDITEM ,
					  "Name=requesttype" , "Value=ajax" , ENDITEM ,
					  "Name=authToken" , "Value={authToken}" ,ENDITEM ,
					  //"Name=callFromPage" , "Value=CheckoutBillingInfo" ,ENDITEM ,
					  LAST ) ;

			lr_end_sub_transaction( "T15_Submit_BA_S05_AjaxOrderChangeServicePIAdd_PLCC", LR_AUTO);

		} else if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= GIFT ) { //PAY_GIFTCARD

//Gift card -6006491259499906427  PIN=8456
//CITI - 6011644423719045   PIN=any 4 digit number

			web_reg_save_param ( "gcBalance" , "LB=\"authorizedAmount\": " , "RB=," , "NotFound=Warning", LAST ) ;

			lr_start_sub_transaction( "T15_Submit_BA_S05_Check gift card balance", "T15_Submit Billing Address" );

			//lr_start_transaction("check gift card balance");

			web_submit_data("AjaxCheckGiftCardBalance",
				"Action=https://{host}/webapp/wcs/stores/servlet/AjaxCheckGiftCardBalance",
				"Method=POST",
				"RecContentType=text/html",
				"Mode=HTTP",
				ITEMDATA,
				"Name=storeId", "Value={storeId}", ENDITEM,
				"Name=catalogId", "Value={catalogId}", ENDITEM,
				"Name=langId", "Value=-1", ENDITEM,
//				"Name=giftCardPin", "Value=8523", ENDITEM,
//				"Name=giftCardNbr", "Value=6006491259499902517", ENDITEM,
				"Name=giftCardPin", "Value=8456", ENDITEM,
				"Name=giftCardNbr", "Value=6006491259499906427", ENDITEM, 
				//"Name=giftCardPin", "Value={GIFT_CARD_PIN}", ENDITEM,
				//"Name=giftCardNbr", "Value={GIFT_CARD_ACC}", ENDITEM,
				"Name=startGiftcardRangeUS", "Value=6006491259000000000", ENDITEM,
				"Name=endGiftcardRangeUS", "Value=6006491259999999999", ENDITEM,
				"Name=startGiftcardRangePinUS", "Value=6006491259081925900", ENDITEM,
				"Name=endGiftcardRangePinUS", "Value=6006491259999999999", ENDITEM,
				"Name=startGiftcardRangePR", "Value=6006492601000000000", ENDITEM,
				"Name=endGiftcardRangePR", "Value=6006492601499999999", ENDITEM,
				"Name=startGiftcardRangePinPR", "Value=6006492601002811000", ENDITEM,
				"Name=endGiftcardRangePinPR", "Value=6006492601499999999", ENDITEM,
				"Name=startGiftcardRangeTPV", "Value=6006491366000000000", ENDITEM,
				"Name=endGiftcardRangeTPV", "Value=6006491367999999999", ENDITEM,
				"Name=startGiftcardRangePinTPV", "Value=6006491366003104405", ENDITEM,
				"Name=endGiftcardRangePinTPV", "Value=6006491367999999999", ENDITEM,
				"Name=startGiftcardRangeCA", "Value=6006491364000000000", ENDITEM,
				"Name=endGiftcardRangeCA", "Value=6006491365999999999", ENDITEM,
				"Name=giftCardBalanceInquiryCaller", "Value=CheckOutPage", ENDITEM,
				"Name=requesttype", "Value=ajax", ENDITEM,
				LAST);

			web_url("TCPGiftCardBalancePopupView",
				"URL=https://{host}/shop/TCPGiftCardBalancePopupView?catalogId={catalogId}&langId=-1&storeId={storeId}&balance={gcBalance}&isGiftCardAlreadyApplied=false",
				"Resource=0",
				"RecContentType=text/html",
				"Mode=HTTP",
				LAST);

			lr_end_transaction("T15_Submit_BA_S05_Check gift card balance",LR_AUTO);

			if (!strcmp(lr_eval_string("{gcBalance}"), "0.00") )
				lr_fail_trans_with_error( "Gift Card Balance is ZERO." ) ;
			else
			{
				web_reg_find ( "Text=piId" , "SaveCount=pi_id_count" , LAST ) ;
				web_reg_find ( "Text=errorCode" , "SaveCount=error_count" , LAST ) ;

				lr_start_sub_transaction( "T15_Submit_BA_S06_Apply GC To Order", "T15_Submit Billing Address" );

				web_submit_data ( "T15_Submit_BA_S06_Apply GC To Order" ,
					  "Action=https://{host}/webapp/wcs/stores/servlet/AjaxOrderChangeServicePIAdd" ,
					  "Method=POST" ,
					  "Mode=HTML" ,
					  ITEMDATA ,
					  "Name=storeId" , "Value={storeId}" , ENDITEM ,
					  "Name=langId" , "Value=-1" , ENDITEM ,
					  "Name=catalogId" , "Value={catalogId}" , ENDITEM ,
					  "Name=valueFromPaymentTC" , "Value= " , ENDITEM ,
					  "Name=paymentTCId" , "Value=" , ENDITEM ,
					  "Name=payMethodId" , "Value=GiftCard" , ENDITEM ,
					  "Name=piAmount" , "Value={piAmount}" , ENDITEM ,
					  "Name=billing_address_id" , "Value={billingAddressId}" , ENDITEM ,
					  "Name=cc_brand" , "Value=GC" , ENDITEM ,
					  "Name=cc_cvc" , "Value=" , ENDITEM ,
					  "Name=account_pin" , "Value=8456" , ENDITEM ,
					  "Name=balance=", "Value={gcBalance}" , ENDITEM ,
					  "Name=account" , "Value=6006491259499906427" , ENDITEM ,
					  "Name=expire_month" , "Value=12" , ENDITEM ,
					  "Name=expire_year" , "Value=2016" , ENDITEM ,
					  "Name=check_routing_number" , "Value= " , ENDITEM ,
					  "Name=checkAccountNumber" , "Value= " , ENDITEM ,
					  "Name=checkRoutingNumber" , "Value= " , ENDITEM ,
					  "Name=requesttype" , "Value=ajax" , ENDITEM ,
					  "Name=authToken" , "Value={authToken}" ,ENDITEM ,
					  LAST ) ;

				web_submit_data("AjaxOrderChangeServiceItemUpdate",
					"Action=https://{host}/webapp/wcs/stores/servlet/AjaxOrderChangeServiceItemUpdate",
					"Method=POST",
					"RecContentType=text/html",
					"Mode=HTTP",
					ITEMDATA,
					"Name=storeId", "Value={storeId}", ENDITEM,
					"Name=catalogId", "Value={catalogId}", ENDITEM,
					"Name=langId", "Value=-1", ENDITEM,
					"Name=orderId", "Value=.", ENDITEM,
					"Name=calculationUsage", "Value=-1,-2,-3,-4,-5,-6,-7", ENDITEM,
					"Name=requesttype", "Value=ajax", ENDITEM,
					LAST);

				web_submit_data("TCPCurrentOrderPaymentSummaryView",
					"Action=https://{host}/shop/TCPCurrentOrderPaymentSummaryView?catalogId={catalogId}&orderId={orderId}&langId=-1&storeId={storeId}",
					"Method=POST",
					"RecContentType=text/html",
					"Mode=HTTP",
					ITEMDATA,
					"Name=fromPage", "Value=billingPage", ENDITEM,
					"Name=objectId", "Value=", ENDITEM,
					"Name=requesttype", "Value=ajax", ENDITEM,
					LAST);

				web_submit_data("TCPCurrentOrderGiftCardView",
					"Action=https://{host}/shop/TCPCurrentOrderGiftCardView?catalogId={catalogId}&orderId={orderId}&langId=-1&storeId={storeId}",
					"Method=POST",
					"RecContentType=text/html",
					"Mode=HTTP",
					ITEMDATA,
					"Name=giftAction", "Value=add", ENDITEM,
					"Name=objectId", "Value=", ENDITEM,
					"Name=requesttype", "Value=ajax", ENDITEM,
					LAST);

				web_submit_data("TCPMiniShopCartDisplayView1",
					"Action=https://{host}/shop/TCPMiniShopCartDisplayView1?catalogId={catalogId}&langId=-1&storeId={storeId}",
					"Method=POST",
					"RecContentType=text/html",
					"Mode=HTTP",
					ITEMDATA,
					"Name=showredEye", "Value=true", ENDITEM,
					"Name=objectId", "Value=", ENDITEM,
					"Name=requesttype", "Value=ajax", ENDITEM,
					LAST);

				web_submit_data("TCPVoucherEarnPlaceCashCheckOutView",
					"Action=https://{host}/shop/TCPVoucherEarnPlaceCashCheckOutView?catalogId={catalogId}&orderId={orderId}&langId=-1&storeId={storeId}",
					"Method=POST",
					"RecContentType=text/html",
					"Mode=HTTP",
					ITEMDATA,
					"Name=objectId", "Value=", ENDITEM,
					"Name=requesttype", "Value=ajax", ENDITEM,
					LAST);

				web_submit_data("TraditionalAjaxShippingDetailsURL",
					"Action=https://{host}/shop/TraditionalAjaxShippingDetailsURL?catalogId={catalogId}&langId=-1&storeId={storeId}",
					"Method=POST",
					"RecContentType=text/html",
					"Mode=HTTP",
					ITEMDATA,
					"Name=shipmentDetailsArea", "Value=update", ENDITEM,
					"Name=objectId", "Value=", ENDITEM,
					"Name=requesttype", "Value=ajax", ENDITEM,
					LAST);

				web_submit_data("TCPCurrentOrderPaymentSummaryView",
					"Action=https://{host}/shop/TCPCurrentOrderPaymentSummaryView?catalogId={catalogId}&orderId={orderId}&langId=-1&storeId={storeId}",
					"Method=POST",
					"RecContentType=text/html",
					"Mode=HTTP",
					ITEMDATA,
					"Name=fromPage", "Value=billingPage", ENDITEM,
					"Name=objectId", "Value=", ENDITEM,
					"Name=requesttype", "Value=ajax", ENDITEM,
					LAST);

				web_submit_data("orderTotalAsJSON",
					"Action=https://{host}/webapp/wcs/stores/servlet/orderTotalAsJSON",
					"Method=POST",
					"RecContentType=text/html",
					"Mode=HTTP",
					ITEMDATA,
					LAST);

				web_submit_data("TCPCurrentOrderGiftCardView",
					"Action=https://{host}/shop/TCPCurrentOrderGiftCardView?catalogId={catalogId}&orderId={orderId}&langId=-1&storeId={storeId}",
					"Method=POST",
					"RecContentType=text/html",
					"Mode=HTTP",
					ITEMDATA,
					"Name=giftAction", "Value=", ENDITEM,
					"Name=objectId", "Value=", ENDITEM,
					"Name=requesttype", "Value=ajax", ENDITEM,
					LAST);

				web_submit_data("orderTotalAsJSON",
					"Action=https://{host}/webapp/wcs/stores/servlet/orderTotalAsJSON",
					"Method=POST",
					"RecContentType=text/html",
					"Mode=HTTP",
					ITEMDATA,
					LAST);

				web_submit_data("TCPVoucherEarnPlaceCashCheckOutView",
					"Action=https://{host}/shop/TCPVoucherEarnPlaceCashCheckOutView?catalogId={catalogId}&orderId={orderId}&langId=-1&storeId={storeId}",
					"Method=POST",
					"Mode=HTTP",
					ITEMDATA,
					"Name=objectId", "Value=", ENDITEM,
					"Name=requesttype", "Value=ajax", ENDITEM,
					LAST);

				web_submit_data("TCPCurrentOrderPaymentInformationView",
					"Action=https://{host}/shop/TCPCurrentOrderPaymentInformationView?catalogId={catalogId}&orderId={orderId}&langId=-1&storeId={storeId}",
					"Method=POST",
					"RecContentType=text/html",
					"Mode=HTTP",
					ITEMDATA,
					"Name=objectId", "Value=", ENDITEM,
					"Name=requesttype", "Value=ajax", ENDITEM,
					LAST);

				lr_end_transaction("T15_Submit_BA_S06_Apply GC To Order",LR_AUTO);

			} //if (!strcmp(lr_eval_string("{gcBalance}"), "0.00") )
		}

//		if (randomPercent <= OFFLINE_PLUGIN+PLCC) {
			if ( atoi ( lr_eval_string ( "{pi_id_count}" ) ) == 0 ) {
					//lr_fail_trans_with_error( "payment instruction id not found after PIAdd - payment problem" ) ;
					lr_end_transaction ( "T15_Submit Billing Address" , LR_FAIL ) ;
					lr_exit(LR_EXIT_ITERATION_AND_CONTINUE, LR_PASS);
			}

			if ( atoi ( lr_eval_string ( "{error_count}" ) ) > 0 ) {
					//lr_fail_trans_with_error( "potential error response from PIAdd - payment problem" ) ;
					lr_end_transaction ( "T15_Submit Billing Address" , LR_FAIL ) ;
					lr_exit(LR_EXIT_ITERATION_AND_CONTINUE, LR_PASS);
			}
//		}

		lr_start_sub_transaction( "T15_Submit_BA_S06_OrderCalculate", "T15_Submit Billing Address" );

			web_url ( "T15_Submit_BA_OrderCalculate" ,
					  "URL=https://{host}/webapp/wcs/stores/servlet/OrderCalculate?calculationUsageId=-1&calculationUsageId=-2&calculationUsageId=-3&calculationUsageId=-4&calculationUsageId=-5&calculationUsageId=-6&calculationUsageId=-7&URL=TCPSingleShipmentOrderSummaryView&langId=-1&storeId={storeId}&catalogId={catalogId}&purchaseorder_id=&quickCheckoutProfileForPayment=false" ,
					  "Resource=0" ,
					  "Mode=HTML" ,
					  LAST ) ;

			lr_end_sub_transaction("T15_Submit_BA_S06_OrderCalculate", LR_AUTO);

			lr_start_sub_transaction( "T15_Submit_BA_S07_CreateCookieCmd", "T15_Submit Billing Address" );

			web_url ( "T15_Submit_BA_CreateCookieCmd" ,
					  "URL=https://{host}/webapp/wcs/stores/servlet/CreateCookieCmd?langId=-1&storeId={storeId}&goToPage=NoURL&catalogId={catalogId}" ,
					  "Resource=0" ,
					  "Mode=HTML" ,
					  LAST ) ;

			lr_end_sub_transaction("T15_Submit_BA_S07_CreateCookieCmd", LR_AUTO);

			createCookieCmd();

	lr_end_transaction ( "T15_Submit Billing Address" , LR_PASS) ;
	return 0;
} // end submitBillingAddress

void submitOrder()
{
	lr_think_time ( FORM_TT ) ;

	lr_start_transaction ( "T16_Submit Order" ) ;

	lr_start_sub_transaction("T16_Submit Order_S01_AjaxOrderProcessServiceOrderPrepare", "T16_Submit Order");

	web_submit_data ( "T16_Submit Order_submitOrder_AjaxOrderProcessServiceOrderPrepare" ,
					  "Action=https://{host}/webapp/wcs/stores/servlet/AjaxOrderProcessServiceOrderPrepare" ,
					  "Method=POST" ,
					  "Mode=HTML" ,
					  ITEMDATA ,
					  "Name=requesttype" , "Value=ajax" , ENDITEM ,
					  LAST ) ;

	lr_end_sub_transaction("T16_Submit Order_S01_AjaxOrderProcessServiceOrderPrepare", LR_AUTO);

			
	lr_start_sub_transaction("T16_Submit Order_S02_AjaxOrderProcessServiceOrderSubmit", "T16_Submit Order");

	web_submit_data ( "T16_Submit Order_AjaxOrderProcessServiceOrderSubmit" ,
					  "Action=https://{host}/webapp/wcs/stores/servlet/AjaxOrderProcessServiceOrderSubmit" ,
					  "Method=POST" ,
					  "Mode=HTML" ,
					  ITEMDATA ,
					  "Name=orderId" , "Value={orderId}" , ENDITEM ,
					  "Name=notifyMerchant" , "Value=0" , ENDITEM ,
					  "Name=notifyShopper" , "Value=0" , ENDITEM ,
					  "Name=notifyOrderSubmitted" , "Value=0" , ENDITEM ,
					  "Name=storeId" , "Value={storeId}" , ENDITEM ,
					  "Name=langId" , "Value=-1" , ENDITEM ,
					  "Name=catalogId" , "Value={catalogId}" , ENDITEM ,
					  "Name=chosenLocale" , "Value=en" , ENDITEM ,
					  "Name=visitorId", "Value=[CS]v1|29E7E17E8507A4B7-4000010E6006518F[CE]", ENDITEM,
					  "Name=notify_EMailSender_recipient" , "Value=" , ENDITEM ,
					  "Name=requesttype" , "Value=ajax" , ENDITEM ,
					  LAST ) ;

	lr_end_sub_transaction("T16_Submit Order_S02_AjaxOrderProcessServiceOrderSubmit", LR_AUTO);
	

	web_reg_find ( "Text=thanks for your order!" , "SaveCount=confirmationFound_count" , LAST ) ;

	lr_start_sub_transaction("T16_Submit Order_S03_TCPOrderShippingBillingConfirmationView", "T16_Submit Order");

	web_url ( "T16_Submit Order_TCPOrderShippingBillingConfirmationView" ,
			  "URL=https://{host}/webapp/wcs/stores/servlet/TCPOrderShippingBillingConfirmationView?storeId={storeId}&catalogId={catalogId}&langId=-1&orderId={orderId}&shipmentTypeId=1" ,
			  "Resource=0" ,
			  "Mode=HTML" ,
			  LAST ) ;

	lr_save_string(lr_eval_string("{orderId}"), "orderIdNumber");
	
//	if ( strlen ( lr_eval_string ( "{orderIdNumber}" ) ) != 15 && atoi(lr_eval_string ( "{confirmationFound_count}" )) != 0 ) {
	if ( strlen ( lr_eval_string ( "{orderIdNumber}" ) ) != 15 || atoi(lr_eval_string ( "{confirmationFound_count}" )) != 0 ) {
		lr_end_sub_transaction("T16_Submit Order_S03_TCPOrderShippingBillingConfirmationView", LR_PASS);
		lr_end_transaction("T16_Submit Order", LR_PASS);
	} else {
		lr_end_sub_transaction("T16_Submit Order_S03_TCPOrderShippingBillingConfirmationView", LR_FAIL);
		lr_end_transaction("T16_Submit Order", LR_FAIL);
	}
} // end submitOrder


void submitOrderGuest()
{
	lr_think_time ( FORM_TT ) ;

	lr_start_transaction ( "T16_Submit Order Guest" ) ;

	lr_start_sub_transaction("T16_Submit Order Guest_S01_AjaxOrderProcessServiceOrderPrepare", "T16_Submit Order Guest");

	web_submit_data ( "T16_Submit Order_submitOrder_AjaxOrderProcessServiceOrderPrepare" ,
					  "Action=https://{host}/webapp/wcs/stores/servlet/AjaxOrderProcessServiceOrderPrepare" ,
					  "Method=POST" ,
					  "Mode=HTML" ,
					  ITEMDATA ,
					  "Name=requesttype" , "Value=ajax" , ENDITEM ,
					  LAST ) ;

	lr_end_sub_transaction("T16_Submit Order Guest_S01_AjaxOrderProcessServiceOrderPrepare", LR_AUTO);

	lr_start_sub_transaction("T16_Submit Order Guest_S02_AjaxOrderProcessServiceOrderSubmit", "T16_Submit Order Guest");

	web_submit_data ( "T16_Submit Order_AjaxOrderProcessServiceOrderSubmit" ,
					  "Action=https://{host}/webapp/wcs/stores/servlet/AjaxOrderProcessServiceOrderSubmit" ,
					  "Method=POST" ,
					  "Mode=HTML" ,
					  ITEMDATA ,
					  "Name=orderId" , "Value={orderId}" , ENDITEM ,
					  "Name=notifyMerchant" , "Value=0" , ENDITEM ,
					  "Name=notifyShopper" , "Value=0" , ENDITEM ,
					  "Name=notifyOrderSubmitted" , "Value=0" , ENDITEM ,
					  "Name=storeId" , "Value={storeId}" , ENDITEM ,
					  "Name=langId" , "Value=-1" , ENDITEM ,
					  "Name=catalogId" , "Value={catalogId}" , ENDITEM ,
					  "Name=chosenLocale" , "Value=en" , ENDITEM ,
					  "Name=visitorId", "Value=[CS]v1|29E7E17E8507A4B7-4000010E6006518F[CE]", ENDITEM,
					  "Name=notify_EMailSender_recipient" , "Value=" , ENDITEM ,
					  "Name=requesttype" , "Value=ajax" , ENDITEM ,
					  LAST ) ;

	lr_end_sub_transaction("T16_Submit Order Guest_S02_AjaxOrderProcessServiceOrderSubmit", LR_AUTO);

//	web_reg_find ( "Text=thanks for your order!" ,  LAST ) ;
	web_reg_find ( "Text=thanks for your order!" , "SaveCount=confirmationFound_count" , LAST ) ;

	lr_start_sub_transaction("T16_Submit Order Guest_S03_TCPOrderShippingBillingConfirmation", "T16_Submit Order Guest");

	web_url ( "T16_Submit Order_TCPOrderShippingBillingConfirmationView" ,
			  "URL=https://{host}/webapp/wcs/stores/servlet/TCPOrderShippingBillingConfirmationView?storeId={storeId}&catalogId={catalogId}&langId=-1&orderId={orderId}&shipmentTypeId=1" ,
			  "Resource=0" ,
			  "Mode=HTML" ,
			  LAST ) ;

	if ( atoi ( lr_eval_string ( "{confirmationFound_count}" ) ) == 1 ) {
		lr_end_sub_transaction("T16_Submit Order Guest_S03_TCPOrderShippingBillingConfirmation", LR_PASS);
		lr_end_transaction("T16_Submit Order Guest", LR_PASS);
	} else {
//		lr_output_message("logonid: %s, orderid: %s", lr_eval_string("{logonid}"), lr_eval_string("{orderId}"));
		lr_end_sub_transaction("T16_Submit Order Guest_S03_TCPOrderShippingBillingConfirmation", LR_FAIL);
		lr_end_transaction("T16_Submit Order Guest", LR_FAIL);
	}
	

} // end submitOrderGuest

void viewReservationHistory()
{
	lr_think_time ( LINK_TT );
	//web_reg_save_param("reservationHistory", "LB=Re", "RB=ation History", "NotFound=Warning", LAST);
	//web_reg_find("Text=Reservation History", "SaveCount=reservationHistory");
	
	lr_start_transaction("T18_ViewReservationHistory");

	web_url("TCPDOMMyReservationHistoryView", 
		"URL=https://{host}/webapp/wcs/stores/servlet/TCPDOMMyReservationHistoryView?storeId={storeId}&catalogId={catalogId}&langId=-1&sortRank=&sortKey=&curentPage=1&pageLength=1000", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Mode=HTML", 
		LAST);

		lr_end_transaction("T18_ViewReservationHistory",LR_AUTO);
	
}

void viewPointsHistory()
{
	lr_think_time ( LINK_TT );
	
	lr_start_transaction("T18_ViewPointsHistory");

	web_custom_request("TCPMyPointsHistoryView", 
		"URL=https://{host}/shop/TCPMyPointsHistoryView?catalogId={catalogId}&langId=-1&storeId={storeId}", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Mode=HTML", 
		"EncType=", 
		LAST);

	lr_end_transaction("T18_ViewPointsHistory",LR_AUTO);
}

void viewOrderHistory()
{
	lr_think_time ( LINK_TT );

	web_reg_save_param("orderIDs", "LB=&orderId=", "RB=&langId=-1&storeId=", "NotFound=Warning", "ORD=ALL", LAST);
	web_reg_save_param("fromOrderDetail", "LB=/shop/", "RB=OrderDetail?catalogId=", "NotFound=Warning", LAST);
	web_reg_find("Text=Transaction Number", "SaveCount=checkOrderhistoryHistory");

	lr_start_transaction ( "T18_ViewOrderHistory" ) ;
	
	web_custom_request("T18_ViewOrderHistory",
	"URL=https://{host}/shop/TCPDOMMyOrderHistoryDisplayContent?catalogId={catalogId}&langId=-1&storeId={storeId}", //Pre-R1 Change
		"Method=POST",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		"EncType=",
		LAST);

	if ( atoi ( lr_eval_string ( "{checkOrderhistoryHistory}" ) ) > 0 ) 
		lr_end_transaction ( "T18_ViewOrderHistory" , LR_PASS) ;
	else
		lr_end_transaction ( "T18_ViewOrderHistory" , LR_FAIL) ;

}// end viewOrderHistory()

void viewOrderStatus()
{
	lr_think_time ( LINK_TT );
	
	if ( atoi( lr_eval_string("{orderIDs_count}")) > 0 ) {

		lr_save_string( lr_paramarr_random("orderIDs"),"orderId");

		lr_start_transaction("T18_ViewOrderStatus");

		web_url("T18_ViewOrderStatus",
//			"URL=http://{host}/shop/TCPOrderDetail?catalogId={catalogId}&orderId={orderId}&langId=-1&storeId={storeId}&shipmentTypeId=1",
//			"URL=http://{host}/shop/TCPDOMOrderDetail?catalogId={catalogId}&orderNumber={orderId}&companyId=1&orderId={orderId}&langId=-1&storeId={storeId}&shipmentTypeId=1",
			"URL=http://{host}/shop/{fromOrderDetail}OrderDetail?catalogId={catalogId}&orderNumber={orderId}&companyId=1&orderId={orderId}&langId=-1&storeId={storeId}&shipmentTypeId=1",
			"TargetFrame=_self",
			"Resource=0",
			"RecContentType=text/html",
			//"Referer=",
			"Snapshot=t50.inf",
			"Mode=HTML",
			LAST);

		lr_end_transaction("T18_ViewOrderStatus", LR_AUTO);

	}
}// end viewOrderStatus()


void logoff()
{
	lr_start_transaction("T21_Logoff");

	web_url("T21_Logoff",
		"URL=https://{host}/shop/Logoff?catalogId={catalogId}&rememberMe=false&myAcctMain=1&langId=-1&storeId={storeId}&URL=LogonForm",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Snapshot=t62.inf",
		"Mode=HTML",
		LAST);

	lr_end_transaction("T21_Logoff", LR_AUTO);

}

void registerUser()
{
	lr_think_time ( FORM_TT ) ;

	lr_start_transaction ( "T17_Register User" ) ;

	lr_start_sub_transaction("T17_Register User_S01_TCPAjaxEmailVerificationCmd", "T17_Register User");

	web_url ( "T17_Register User_TCPAjaxEmailVerificationCmd" ,
			  "URL=https://{host}/webapp/wcs/stores/servlet/TCPAjaxEmailVerificationCmd?email={emailAddress1}&page=registration&requesttype=ajax" ,
			  "Resource=0" ,
			  "Mode=HTML" ,
			  LAST ) ;

	lr_end_sub_transaction("T17_Register User_S01_TCPAjaxEmailVerificationCmd", LR_AUTO);

	lr_start_sub_transaction("T17_Register User_S02_PersonProcessServicePersonRegister", "T17_Register User");

	web_submit_data ( "T17_Register User_PersonProcessServicePersonRegister" ,
			  "Action=https://{host}/shop/PersonProcessServicePersonRegister" ,
			  "Method=POST" ,
			  "Mode=HTML" ,
			  ITEMDATA ,
			  "Name=logonPassword" , "Value=asdf1234" , ENDITEM ,
			  "Name=logonPasswordVerify" , "Value=asdf1234" , ENDITEM ,
			  "Name=termsCheck" , "Value=on" , ENDITEM ,
			  "Name=myAcctMain" , "Value=" , ENDITEM ,
			  "Name=new" , "Value=Y" , ENDITEM ,
			  "Name=storeId" , "Value={storeId}" , ENDITEM ,
			  "Name=catalogId" , "Value={catalogId}" , ENDITEM ,
			  "Name=URL" , "Value=Logon?reLogonURL=LogonForm&storeId={storeId}&catalogId={catalogId}&langId=-1&URL=TCPAccountVerifyView&from=Register&Country=" , ENDITEM ,
			  "Name=receiveSMS" , "Value=false" , ENDITEM ,
			  "Name=addressType" , "Value=ShippingAndBilling" , ENDITEM ,
			  "Name=errorViewName" , "Value=TCPOrderShippingBillingConfirmationView" , ENDITEM ,
			  "Name=orderId" , "Value={orderId}" , ENDITEM ,
			  "Name=guestRegistrationFlag" , "Value=true" , ENDITEM ,
			  "Name=orderConfirmFlag" , "Value=true" , ENDITEM ,
			  "Name=page" , "Value=account" , ENDITEM ,
			  "Name=registerType" , "Value=Guest" , ENDITEM ,
			  "Name=primary" , "Value=true" , ENDITEM ,
			  "Name=profileType" , "Value=Consumer" , ENDITEM ,
			  "Name=RegistrationApprovalStatus" , "Value=3" , ENDITEM ,
			  "Name=firstName" , "Value=Teddy" , ENDITEM ,
			  "Name=lastName" , "Value=Bear" , ENDITEM ,
			  "Name=logonId" , "Value={emailAddress1}" , ENDITEM ,
			  "Name=email1Verify" , "Value={emailAddress1}" , ENDITEM ,
			  "Name=address1" , "Value={guestAdr1}" , ENDITEM ,
			  "Name=address2" , "Value=" , ENDITEM ,
			  "Name=city" , "Value={guestCity}" , ENDITEM ,
			  "Name=state" , "Value={guestState}" , ENDITEM ,
			  "Name=addressField3" , "Value={guestZip}" , ENDITEM ,
			  "Name=zipCode" , "Value={guestZip}" , ENDITEM ,
			  "Name=country" , "Value={guestCountry}" , ENDITEM ,
			  "Name=phone1" , "Value=4163218351" , ENDITEM ,
			  "Name=uniform-WC_TCPUserRegistrationAddForm_FormInput_state" , "Value=" , ENDITEM ,
			  "Name=receiveSMSNotification" , "Value=false" , ENDITEM ,
			  "Name=demographicField1" , "Value=0" , ENDITEM ,
			  LAST ) ;

	lr_end_sub_transaction("T17_Register User_S02_PersonProcessServicePersonRegister", LR_AUTO);

	lr_end_transaction ( "T17_Register User" , LR_PASS) ;

	//orderHistory();

} // end registerUser

void updateQuantity()
{
	lr_think_time ( FORM_TT ) ;

	lr_start_transaction ( "T09_Update Quantity" ) ;

	lr_start_sub_transaction("T09_Update Quantity_S01_AjaxOrderChangeServiceItemUpdate", "T09_Update Quantity");

		web_submit_data("AjaxOrderChangeServiceItemUpdate",
			"Action=https://{host}/webapp/wcs/stores/servlet/AjaxOrderChangeServiceItemUpdate",
			"Method=POST",
			"TargetFrame=",
			"RecContentType=text/html",
			"Mode=HTML",
			ITEMDATA,
			"Name=orderId", "Value=.", ENDITEM,
			"Name=storeId", "Value={storeId}", ENDITEM,
			"Name=catalogId", "Value={catalogId}", ENDITEM,
			"Name=langId", "Value=-1", ENDITEM,
			"Name=from", "Value=shoppingCart", ENDITEM,
			"Name=calculationUsage", "Value=-1,-2,-5,-6,-7", ENDITEM,
			"Name=inventoryValidation", "Value=true", ENDITEM,
			"Name=orderItemId_1", "Value={orderItemId}", ENDITEM,
			"Name=quantity_1", "Value=2", ENDITEM,
			"Name=requesttype", "Value=ajax", ENDITEM,
			LAST);

	lr_end_sub_transaction("T09_Update Quantity_S01_AjaxOrderChangeServiceItemUpdate", LR_AUTO);

	lr_start_sub_transaction("T09_Update Quantity_S02_CreateCookieCmd", "T09_Update Quantity");

		web_custom_request("T09_Update Quantity_CreateCookieCmd",
			"URL=http://{host}/webapp/wcs/stores/servlet/CreateCookieCmd?langId=-1&storeId={storeId}&goToPage=NoURL&catalogId={catalogId}" ,
			"Method=GET",
			"TargetFrame=",
			"Resource=0",
			"RecContentType=text/html",
			"Mode=HTML",
			"EncType=application/x-www-form-urlencoded",
			LAST);

	lr_end_sub_transaction("T09_Update Quantity_S02_CreateCookieCmd", LR_AUTO);

	createCookieCmd();

	lr_start_sub_transaction("T09_Update Quantity_S03_TCPMiniShopCartDisplayView1", "T09_Update Quantity");

		web_submit_data("T09_Update Quantity_TCPMiniShopCartDisplayView1",
			"Action=https://{host}/webapp/wcs/stores/servlet/TCPMiniShopCartDisplayView1?catalogId={catalogId}&langId=-1&storeId={storeId}" ,
			"Method=POST",
			"TargetFrame=",
			"RecContentType=text/html",
			"Mode=HTML",
			ITEMDATA,
			"Name=showredEye", "Value=true", ENDITEM,
			"Name=objectId", "Value=", ENDITEM,
			"Name=requesttype", "Value=ajax", ENDITEM,
		LAST);

	lr_end_sub_transaction("T09_Update Quantity_S03_TCPMiniShopCartDisplayView1", LR_AUTO);

	//https://tcp-perf.childrensplace.com/shop/ShopCartDisplayView?shipmentType=single&catalogId=10551&langId=-1&storeId=10151
		lr_start_sub_transaction ("T09_Update Quantity_S04_CreateCookieCmd", "T09_Update Quantity" ) ;

		web_custom_request("UpdateQuantity_CreateCookieCmd",
			"URL=https://{host}/webapp/wcs/stores/servlet/CreateCookieCmd?langId=-1&storeId={storeId}&goToPage=NoURL&catalogId={catalogId}",
			"Method=GET",
			"Resource=0",
			"RecContentType=text/html",
			"Snapshot=t28.inf",
			"Mode=HTML",
			"EncType=application/x-www-form-urlencoded",
			LAST);

		lr_end_sub_transaction ("T09_Update Quantity_S04_CreateCookieCmd", LR_AUTO) ;

		createCookieCmd();

	lr_end_transaction ( "T09_Update Quantity" , LR_PASS) ;

} // end updateQuantity()
/*
void switchColor()
{
} // end switchColor

void emailSignUp()
{
	lr_start_transaction ( "T16_Submit Shipping Address" ) ;
	lr_end_transaction ( "T16_Submit Shipping Address" , LR_PASS) ;

} // end emailSignUp
*/

/*void browseScenario()
{
		topNav();

		sort();

		paginate();

		drill();

		productDisplay();
} // end browseScenario
*/


void buildCartDrop(int userProfile)
{
//	int orderItemIdscount = 0; //Moved to Global
	int iLoop = 0;

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CART_MERGE ) { //50%
		target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_MERGE_CART_SIZE}"));
//		iLoop = atoi(lr_eval_string("{RATIO_AVG_MERGE_CART_SIZE}")) + 2;
	}
	else {
		target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_DROP_CART_SIZE}"));
//		iLoop = atoi(lr_eval_string("{RATIO_AVG_DROP_CART_SIZE}")) + 2;
	}
	if ( strcmp(strupr(lr_eval_string("{buildCart}")),  "TRUE") == 0) {
		target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_BUILD_CART}"));
//		iLoop = atoi(lr_eval_string("{RATIO_AVG_BUILD_CART}")) + 2;
	}

	iLoop = target_itemsInCart + 2;
	
	lr_start_transaction("T20_New_Cart");
	lr_end_transaction("T20_New_Cart", LR_PASS);

//	userProfile == 0 is buildFirst before login, userProfile == 1 is loginFirst before building the cart
	if ( userProfile == 0 && atoi(lr_eval_string("{totalNumberOfItems_count}")) == 1) { // && strlen(lr_eval_string("{totalNumberOfItems_2}")) != 22) {
//		lr_start_transaction("T20_New_Cart");
//		lr_end_transaction("T20_New_Cart", LR_PASS);
		orderItemIdscount = 0;
	}
	else {
		if (atoi(lr_eval_string("{totalNumberOfItems_count}")) == 1) { // && strlen(lr_eval_string("{totalNumberOfItems_2}")) != 22) {
//			lr_start_transaction("T20_New_Cart");
//			lr_end_transaction("T20_New_Cart", LR_PASS);
			orderItemIdscount = 0;
		}
		else
			orderItemIdscount = atoi(lr_eval_string("{totalNumberOfItems_2}"));
	}

//	lr_save_string("dropCartFlow", "currentFlow");
//	if (strcmp (lr_eval_string("{currentFlow}")) < target_itemsInCart ) {
//	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= (LOGIN_PAGE_DROP + SHIP_PAGE_DROP + BILL_PAGE_DROP) )
//			lr_exit(LR_EXIT_ITERATION_AND_CONTINUE, LR_PASS);

	if (  orderItemIdscount < target_itemsInCart ) {
	
	    target_itemsInCart = target_itemsInCart - orderItemIdscount ;

//		topNav();
		
		for(index_buildCart=0; index_buildCart < target_itemsInCart ; index_buildCart++)
		{
			topNav();
			
			if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_BUILDCART_DRILLDOWN ){
				drill();
			} // end if

			paginate();			
		
			productDisplay();

			addToCart();
		
			iLoop--;
			
			if (iLoop == 0)
			    break;

			if (atc_Stat == 1)  //0-Pass 1-Fail //if the addtocart failed, set the index_buildCart to original number
				index_buildCart--;

//			lr_message("index_buildCart = %d", index_buildCart );

		} // end for loop to add o cart

//		viewCart();
	}
} // end buildCartDrop


void buildCartCheckout(int userProfile)
{
//	int orderItemIdscount = 0; //Moved to Global, if coming from LoginFirst this might already have a value
	int iLoop = 0;
	
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CART_MERGE ) {
		target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_MERGE_CART_SIZE}"));
//		iLoop = atoi(lr_eval_string("{RATIO_AVG_MERGE_CART_SIZE}")) + 2;
	}
	else {
		target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_CART_SIZE}"));
//		iLoop = atoi(lr_eval_string("{RATIO_AVG_CART_SIZE}")) + 2;
	}

	iLoop = target_itemsInCart + 2;
	
	lr_start_transaction("T20_New_Cart");
	lr_end_transaction("T20_New_Cart", LR_PASS);

//	userProfile == 0 is buildFirst before login, userProfile == 1 is loginFirst before building the cart
	if ( userProfile == 0 && atoi(lr_eval_string("{totalNumberOfItems_count}")) == 1) { //} && strlen(lr_eval_string("{totalNumberOfItems_2}")) != 22) {
//		lr_start_transaction("T20_New_Cart");
//		lr_end_transaction("T20_New_Cart", LR_PASS);
		orderItemIdscount = 0;
	}
	else {
		if (atoi(lr_eval_string("{totalNumberOfItems_count}")) == 1) { // && strlen(lr_eval_string("{totalNumberOfItems_2}")) != 22) {
//			lr_start_transaction("T20_New_Cart");
//			lr_end_transaction("T20_New_Cart", LR_PASS);
			orderItemIdscount = 0;
		}
		else
			orderItemIdscount = atoi(lr_eval_string("{totalNumberOfItems_2}"));
	}
	
	if (  orderItemIdscount < target_itemsInCart ) {
	
	    target_itemsInCart = target_itemsInCart - orderItemIdscount ;
//		lr_message("target_itemsInCart = %d", target_itemsInCart );
		
//		topNav();

		for(index_buildCart=0; index_buildCart < target_itemsInCart ; index_buildCart++)
		{
			topNav(); 

			if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_BUILDCART_DRILLDOWN ){
				drill();
			} // end if

			paginate();			

			productDisplay();

			addToCart();

			iLoop--;
			
			if (iLoop == 0)
			    break;
				
			if (atc_Stat == 1)  //0-Pass 1-Fail //if the addtocart failed, set the index_buildCart to original number
				index_buildCart--;

//			lr_message("index_buildCart = %d", index_buildCart );

		} // end for loop to add o cart

//		viewCart();
	
	}
	
} // end buildCartCheckout

void buildCartRopis()
{
/*	
//	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_RESERVATION_WISHLIST) {
		wlGetCatEntryId();
		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RESERVE_ONLINE && atoi(lr_eval_string("{catEntryIds_count}")) != 0) {
			lr_save_string( lr_paramarr_random("catEntryIds"), "catentryId");
			reserveOnline();
		}
	}
	else {
*/		productDisplay();
		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RESERVE_ONLINE && atoi(lr_eval_string("{atc_catentryIds_count}")) != 0) {
			lr_save_string( lr_paramarr_random("atc_catentryIds"), "catentryId");
			reserveOnline();
		}
}


void reserveOnline()
{
 	lr_think_time ( LINK_TT ) ;

	web_reg_save_param("storeUniqueID", "LB=storeUniqueID\":\"", "RB=\",\"itemAvailability", "NotFound=Warning", LAST);

	lr_start_transaction("T04_Product_Reserve_Online_FIND_IT");

	web_url("GetStoreAndProductInventoryInfo", 
		"URL=http://{host}/webapp/wcs/stores/servlet/GetStoreAndProductInventoryInfo?storeId={storeId}&catalogId={catalogId}&langId=-1&catentryId={catentryId}&latitude={storeLocatorLatitude}&longitude={storeLocatorLongitude}", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Mode=HTML", 
		LAST);

	lr_end_transaction("T04_Product_Reserve_Online_FIND_IT",LR_AUTO);

 	lr_think_time ( LINK_TT ) ;

	web_reg_save_param("reservationOrderID", "LB=reservationOrderId\": \"", "RB=\",", "NotFound=Warning", LAST);

	lr_start_transaction("T04_Product_Reserve_Online_Submit");

	web_url("TCPReservationSubmitCmd", 
		"URL=http://{host}/webapp/wcs/stores/servlet/TCPReservationSubmitCmd?storeId={storeId}&catalogId={catalogId}&langId=-1&quantity=1&catEntryId={catentryId}&stlocId={storeUniqueID}&firstname=manny&lastname=Paquiao&phone=2014537616&email={userEmail}&optInEmail&optInEmail=true", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Mode=HTML", 
		LAST);

	if(strlen(lr_eval_string("{reservationOrderID}")) == 9)
		lr_end_transaction("T04_Product_Reserve_Online_Submit",LR_PASS);
	else
		lr_end_transaction("T04_Product_Reserve_Online_Submit",LR_FAIL);
	
}

void inCartEdits()
{            
	viewCart(); 
	
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_UPDATE_QUANTITY )
			updateQuantity();  //		if ( atoi( lr_eval_string("{orderItemIds_count}") ) != 0 ) {

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_PROMO_APPLY )
			applyPromoCode(0);

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_DELETE_ITEM )
			deleteItem();

	if (isLoggedIn == 1) {
		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= MOVE_FROM_CART_TO_WISHLIST  ) {
//		    moveFromWishlistToCart(); //12/03 6:30pm to lower this count down 
			moveFromCartToWishlist();
				
//			if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= MOVE_FROM_WISHLIST_TO_CART )
//			    moveFromWishlistToCart();
		}
		
//		moveFromCartToWishlist(); //12/03 6:30pm to lower this count down 
	}

	viewCart();
	viewCart(); //rhy 12/06 11am
	
	return;
}


void wlGetProduct()
{
	//topNav();

	drill();

	productDisplay();
} 

void wlCreate()
{
	wlView();
	wlGetAll(); //Get all wish-lists for users

 	lr_think_time ( LINK_TT ) ;
	if (lr_paramarr_len("WishlistIDs") < 5)  {
	//================================================
	//Create a wish-list -
	//===============================================
		lr_start_transaction("T19_Wishlist Create");

		web_url("AjaxGiftListServiceCreate", // CREATE - This works for creating new, jquery value does not matter
			"URL=http://{host}/webapp/wcs/stores/servlet/AjaxGiftListServiceCreate?name=PerfWL{WL_Random_Number}&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&tcpCallBack=jQuery1113027745301206596196_1440517580196&_=1440517580198",
			"TargetFrame=",
			"Resource=0",
			"RecContentType=text/html",
			"Referer=",
			"Snapshot=t21.inf",
			"Mode=HTML",
			LAST);

		lr_end_transaction("T19_Wishlist Create", LR_AUTO);
	}

}

void wlDelete()
{
	wlView();
	wlGetAll(); //Get all wish-lists for users

 	lr_think_time ( LINK_TT ) ;
	if (lr_paramarr_len("WishlistIDs") > 1)  {
	//================================================
	//Delete a wish-list
	//================================================
		lr_start_transaction("T19_Wishlist Delete");

		web_url("AjaxGiftListServiceDeleteGiftList", //DELETE - This works for Deleting WL, jquery value does not matter, need to give the ListId
			"URL=http://{host}/webapp/wcs/stores/servlet/AjaxGiftListServiceDeleteGiftList?&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&tcpCallBack=jQuery1113008221043809317052_1440783988176&giftListId={WishlistID}&_=1440783988185",
			"TargetFrame=",
			"Resource=0",
			"RecContentType=text/html",
			"Referer=",
			"Snapshot=t21.inf",
			"Mode=HTML",
			LAST);

		lr_end_transaction("T19_Wishlist Delete", LR_AUTO);
	}
}

void wlChange()
{
	wlView();
	wlGetAll(); //Get all wish-lists for users

 	lr_think_time ( LINK_TT ) ;
	//================================================
	//Change wish-list name
	//================================================
	lr_start_transaction("T19_Wishlist Change Name");

	web_url("AjaxGiftListServiceUpdateDescription", //CHANGE - This works for changing WL name, jquery value does not matter, need to give the ListId
		"URL=http://{host}/webapp/wcs/stores/servlet/AjaxGiftListServiceUpdateDescription?&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&tcpCallBack=jQuery1113008221043809317052_1440783988176&giftListId={WishlistID}&name=Perf Changed WL_{WL_Random_Number}&type=name&_=1440783988193",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Referer=",
		"Snapshot=t21.inf",
		"Mode=HTML",
		LAST);

	lr_end_transaction("T19_Wishlist Change Name", LR_AUTO);

}

void TCPGetWishListForUsersView()
{
	lr_start_transaction("T25_Common_S01_TCPGetWishListForUsersView");

	web_custom_request("TCPGetWishListForUsersView",
		"URL=http://{host}/webapp/wcs/stores/servlet/TCPGetWishListForUsersView?tcpCallBack=jQuery1113014590106982485151_1473881708524&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&sortBy=&_=1473881708525", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Mode=HTML", 
		"EncType=application/x-www-form-urlencoded", 
		LAST);

	lr_end_sub_transaction("T25_Common_S01_TCPGetWishListForUsersView", LR_AUTO);
	
}

void wlView()
{
 	lr_think_time ( LINK_TT ) ;

	lr_start_transaction("T19_Wishlist View");

		lr_start_sub_transaction("T19_Wishlist View_S01_TCPMiniShopCartDisplayView", "T19_Wishlist View");
		
			web_url("TCPViewWL",
				"URL=http://{host}/shop/us/kids-baby-gifts-registry-wishlist",
				"TargetFrame=",
				"Resource=0",
				"RecContentType=text/html",
				"Snapshot=t41.inf",
				"Mode=HTML",
				LAST);

		lr_end_sub_transaction("T19_Wishlist View_S01_TCPMiniShopCartDisplayView", LR_AUTO);

		web_reg_save_param("WishlistIDs", "LB=\"giftListExternalIdentifier\": ", "RB=,", "ORD=All", "NotFound=Warning", LAST);
		web_reg_save_param("checkWishlistName", "LB=nameIdentifier\": \"", "RB='s Wishlist\",", "NotFound=Warning", LAST);
		//\t"nameIdentifier": "Joe's Wishlist",\n
		
		TCPGetWishListForUsersView();
		
	if ( strlen( lr_eval_string("{checkWishlistName}")) <= 0 )
		lr_end_transaction("T19_Wishlist View", LR_FAIL);
	else
		lr_end_transaction("T19_Wishlist View", LR_AUTO);
}

void wlAddItem()
{
	wlGetAll(); //Get all wish-lists for users

	target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_CART_SIZE}"));

	//lr_message("target_itemsInCart = %d", target_itemsInCart );

//	for(index_buildCart=0; index_buildCart < target_itemsInCart ; index_buildCart++)
//	{
		wlGetProduct();

		if (lr_paramarr_len("atc_catentryIds") > 0) {

			lr_save_string( lr_paramarr_random( "atc_catentryIds" ), "atc_catentryId");

			//================================================
			//Add items to a wish-list
			//================================================

			lr_start_transaction("T19_Wishlist Add Item");

			web_url("AjaxGiftListServiceAddItem", //Add item to wish-lists
				"URL=http://{host}/webapp/wcs/stores/servlet/AjaxGiftListServiceAddItem?&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&tcpCallBack=jQuery111301397454545367509_1440779623033&giftListId={WishlistID}&catEntryId_1={atc_catentryId}&quantity_1=1&_=1440779623035",
				"TargetFrame=",
				"Resource=0",
				"RecContentType=text/html",
				"Referer=",
				"Snapshot=t21.inf",
				"Mode=HTML",
				LAST);

			lr_end_transaction("T19_Wishlist Add Item", LR_AUTO);

		} // end for loop to add o cart

		lr_think_time ( LINK_TT ) ;
//	}

}

void wlAddToCart()
{
	wlView();
	wlGetAll(); //Get all wish-lists for users
	wlGetItems();

 	lr_think_time ( LINK_TT ) ;

	if (lr_paramarr_len("catEntryIds") > 0) {

		lr_save_string( lr_paramarr_random( "catEntryIds" ), "catEntryId");
		web_reg_save_param("orderId", "LB=\"orderId\": [\"", "RB=\"", "NotFound=Warning", LAST);
		web_reg_save_param("orderItemId", "LB=\"orderItemId\": [\"", "RB=\"", "NotFound=Warning", LAST);

		//================================================
		// Add Items to the cart from wish-list
		//================================================

		lr_start_transaction("T19_Wishlist Add To Cart");

		web_url("AjaxOrderChangeServiceItemAdd",
			"URL=http://{host}/webapp/wcs/stores/servlet/AjaxOrderChangeServiceItemAdd?&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&tcpCallBack=jQuery111307849840587005019_1440779729870&orderId=.&calculationUsage=-1%2C-2%2C-5%2C-6%2C-7&catEntryId={catEntryId}&quantity=1&requesttype=ajax&externalId={WishlistID}&_=1440779729880",
			"TargetFrame=",
			"Resource=0",
			"RecContentType=text/html",
			"Referer=",
			"Snapshot=t21.inf",
			"Mode=HTML",
			LAST);

		lr_end_transaction("T19_Wishlist Add To Cart", LR_AUTO);

	}

}

void wlDeleteItem()
{
	wlView();
	wlGetAll(); //Get all wish-lists for users
	wlGetItems();

 	lr_think_time ( LINK_TT ) ;
	//================================================
	//Delete items from wish-list
	//================================================
	if ( lr_paramarr_len("catEntryIds") > 0 ) { //if there is any to delete

		lr_save_string(lr_paramarr_random("wishListItemIds"), "wishListItemId");

		lr_start_transaction("T19_Wishlist Delete Item");

		web_url("AjaxGiftListServiceUpdateItem", //Delete Item from a wish-list
			"URL=http://{host}/webapp/wcs/stores/servlet/AjaxGiftListServiceUpdateItem?&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&tcpCallBack=jQuery111307849840587005019_1440779729870&giftListId={WishlistID}&giftListItemId={wishListItemId}&quantity=0&_=1440779729876",
			"TargetFrame=",
			"Resource=0",
			"RecContentType=text/html",
			"Referer=",
			"Snapshot=t21.inf",
			"Mode=HTML",
			LAST);

		lr_end_transaction("T19_Wishlist Delete Item", LR_AUTO);

	}

}

void wlGetCatEntryId()
{
	wlView();
	wlGetAll(); //Get all wish-lists for users
	wlGetItems();

 	lr_think_time ( LINK_TT ) ;
}

void wlGetItems()
{
	wlView();

 	lr_think_time ( LINK_TT ) ;

	web_reg_save_param("catEntryIds", "LB=\"itemId\": \"", "RB=\"", "NotFound=Warning", "Ord=All", LAST);
	web_reg_save_param("wishListItemIds", "LB=\"wishListItemId\": \"", "RB=\"", "NotFound=Warning", "Ord=All", LAST);

	//================================================
	//Get wish-list items for a selected wish-list
	//================================================

	lr_start_transaction("T19_Wishlist Get Items");

	web_url("TCPGetWishListItemsForSelectedListView", //Get all wish-list Items - This works for changing WL, jquery value does not matter, need to give the ListId
		"URL=http://{host}/webapp/wcs/stores/servlet/TCPGetWishListItemsForSelectedListView?&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&wishListId={WishlistID}&tcpCallBack=jQuery111308157584751024842_1440519437271&_=1440519437287",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Referer=",
		"Snapshot=t21.inf",
		"Mode=HTML",
		LAST);

	lr_end_transaction("T19_Wishlist Get Items", LR_AUTO);

}

void wlGetAll()
{
	if (lr_paramarr_len("WishlistIDs") == 0)  {//no need to call this again if it already has values
	//  giftListExternalIdentifier\": 2001,
		web_reg_save_param("WishlistIDs", "LB=\"giftListExternalIdentifier\": ", "RB=,", "ORD=All", "NotFound=Warning", LAST);
	//	web_reg_save_param("WishlistName", "LB=\"nameIdentifier\": \"", "RB=,", "ORD=All", LAST);
	//	web_reg_save_param("WishlistItemCount", "LB=\"itemCount\": ", "RB=,", "ORD=All", LAST);
	// 	lr_think_time (1) ;

		//================================================
		//Get all wish-lists for users
		//================================================
		lr_start_transaction("T19_Wishlist Get List");

			TCPGetWishListForUsersView();
		
			if (lr_paramarr_len("WishlistIDs") == 0)  {//confirmed NOT login, giftListExternalIdentifier is NOT found in the response
		
			web_reg_save_param("WishlistIDs", "LB=\"giftListId\": ", "RB=,", "ORD=All", "NotFound=Warning", LAST);
		//http://tcp-perf.childrensplace.com/webapp/wcs/stores/servlet/AjaxGiftListServiceCreate?tcpCallBack=jQuery111309054115856997669_1446156866892&registryAccessPreference=Public&requesttype=ajax&storeId=10151&catalogId=10551&langId=-1&name=Joe%27s+Wishlist&_=1446156866894	
			web_url("AjaxGiftListServiceCreate",
				"URL=http://{host}/webapp/wcs/stores/servlet/AjaxGiftListServiceCreate?tcpCallBack=jQuery111309054115856997669_1446156866892&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&name=Joe%27s+Wishlist&_=1446156866894",
				"TargetFrame=",
				"Resource=0",
				"RecContentType=text/html",
				"Referer=",
				"Snapshot=t21.inf",
				"Mode=HTML",
				LAST);
				
			if (lr_paramarr_len("WishlistIDs") == 0)  //confirmed NOT login, giftListExternalIdentifier is NOT found in the response
				lr_end_transaction("T19_Wishlist Get List", LR_FAIL);
			else
				lr_end_transaction("T19_Wishlist Get List", LR_PASS);	
		}
		else
			lr_end_transaction("T19_Wishlist Get List", LR_PASS);

		if (lr_paramarr_len("WishlistIDs") > 0)
			lr_save_string(lr_paramarr_random("WishlistIDs"), "WishlistID");
	}
	
}//wlGetAll()


void wishList()
{
	int iRandomTask = 0;
//	web_cleanup_cookies ( ) ;

//	viewHomePage();

//	wlLogon();

	iRandomTask = atoi(lr_eval_string("{RANDOM_PERCENT}"));

	if (iRandomTask <= RATIO_WL_CREATE)
		wlCreate();
	else if (iRandomTask <= RATIO_WL_DELETE + RATIO_WL_CREATE)
		wlDelete();
	else if (iRandomTask <= RATIO_WL_CHANGE + RATIO_WL_CREATE + RATIO_WL_DELETE)
		wlChange();
//	else if (iRandomTask <= RATIO_WL_DELETE_ITEM + RATIO_WL_CREATE + RATIO_WL_DELETE + RATIO_WL_CHANGE)
//		wlDeleteItem();
	else if (iRandomTask <= RATIO_WL_ADD_ITEM + RATIO_WL_CREATE + RATIO_WL_DELETE + RATIO_WL_CHANGE + RATIO_WL_DELETE_ITEM)
		wlAddItem();
	else if (iRandomTask <= RATIO_WL_ADD_ITEM + RATIO_WL_CREATE + RATIO_WL_DELETE + RATIO_WL_CHANGE + RATIO_WL_DELETE_ITEM + RATIO_WL_ADD_TO_CART)
		wlAddToCart();

}

	
void dropCartTransaction()
{
	lr_start_transaction("T20_Abandon_Cart");
	lr_end_transaction("T20_Abandon_Cart", LR_PASS);
	
	//return LR_PASS;
}

void getDataPoints()
{

	lr_param_sprintf("LOGIN_PAGE_DROP", "01. LOGIN_PAGE_DROP = %d", LOGIN_PAGE_DROP);
	lr_user_data_point(lr_eval_string("{LOGIN_PAGE_DROP}"), 0);
	lr_param_sprintf("SHIP_PAGE_DROP", "01. SHIP_PAGE_DROP = %d", SHIP_PAGE_DROP);
	lr_user_data_point(lr_eval_string("{SHIP_PAGE_DROP}"), 0);
	lr_param_sprintf("BILL_PAGE_DROP", "01. BILL_PAGE_DROP = %d", BILL_PAGE_DROP);
	lr_user_data_point(lr_eval_string("{BILL_PAGE_DROP}"), 0);

	lr_param_sprintf("RATIO_CHECKOUT_LOGIN", "02. RATIO_CHECKOUT_LOGIN = %d", RATIO_CHECKOUT_LOGIN);
	lr_user_data_point(lr_eval_string("{RATIO_CHECKOUT_LOGIN}"), 0);
	lr_param_sprintf("RATIO_CHECKOUT_GUEST", "02. RATIO_CHECKOUT_GUEST = %d", RATIO_CHECKOUT_GUEST);
	lr_user_data_point(lr_eval_string("{RATIO_CHECKOUT_GUEST}"), 0);
	
	
	lr_param_sprintf("RATIO_DROP_CART", "03. RATIO_DROP_CART = %d", RATIO_DROP_CART);
	lr_user_data_point(lr_eval_string("{RATIO_DROP_CART}"), 0);
    
	lr_param_sprintf("RATIO_CHECKOUT_LOGIN_FIRST", "04. RATIO_CHECKOUT_LOGIN_FIRST = %d", RATIO_CHECKOUT_LOGIN_FIRST);
	lr_user_data_point(lr_eval_string("{RATIO_CHECKOUT_LOGIN_FIRST}"), 0);

	lr_param_sprintf("RATIO_BUILDCART_DRILLDOWN", "04. RATIO_BUILDCART_DRILLDOWN = %d", RATIO_BUILDCART_DRILLDOWN);
	lr_user_data_point(lr_eval_string("{RATIO_BUILDCART_DRILLDOWN}"), 0);
	
	lr_param_sprintf("USE_LOW_INVENTORY", "05. USE_LOW_INVENTORY = %d", USE_LOW_INVENTORY);
	lr_user_data_point(lr_eval_string("{USE_LOW_INVENTORY}"), 0);
	
	lr_param_sprintf("OFFLINE_PLUGIN", "06. OFFLINE_PLUGIN = %d", OFFLINE_PLUGIN);
	lr_user_data_point(lr_eval_string("{OFFLINE_PLUGIN}"), 0);

	lr_param_sprintf("RATIO_PROMO_APPLY", "07. RATIO_PROMO_APPLY = %d", RATIO_PROMO_APPLY);
	lr_user_data_point(lr_eval_string("{RATIO_PROMO_APPLY}"), 0);
	lr_param_sprintf("RATIO_PROMO_MULTIUSE", "07. RATIO_PROMO_MULTIUSE = %d", RATIO_PROMO_MULTIUSE);
	lr_user_data_point(lr_eval_string("{RATIO_PROMO_MULTIUSE}"), 0);
	lr_param_sprintf("RATIO_PROMO_SINGLEUSE", "07. RATIO_PROMO_SINGLEUSE = %d", RATIO_PROMO_SINGLEUSE);
	lr_user_data_point(lr_eval_string("{RATIO_PROMO_SINGLEUSE}"), 0);
	lr_param_sprintf("RATIO_REDEEM_LOYALTY_POINTS", "07. RATIO_REDEEM_LOYALTY_POINTS = %d", RATIO_REDEEM_LOYALTY_POINTS);
	lr_user_data_point(lr_eval_string("{RATIO_REDEEM_LOYALTY_POINTS}"), 0);
	
	lr_param_sprintf("RATIO_CART_MERGE", "08. RATIO_CART_MERGE = %d", RATIO_CART_MERGE);
	lr_user_data_point(lr_eval_string("{RATIO_CART_MERGE}"), 0);

	lr_param_sprintf("RATIO_REGISTER", "09. RATIO_REGISTER = %d", RATIO_REGISTER);
	lr_user_data_point(lr_eval_string("{RATIO_REGISTER}"), 0);

	lr_param_sprintf("RATIO_UPDATE_QUANTITY", "10. RATIO_UPDATE_QUANTITY = %d", RATIO_UPDATE_QUANTITY);
	lr_user_data_point(lr_eval_string("{RATIO_UPDATE_QUANTITY}"), 0);

	lr_param_sprintf("RATIO_DELETE_ITEM", "11. RATIO_DELETE_ITEM = %d", RATIO_DELETE_ITEM);
	lr_user_data_point(lr_eval_string("{RATIO_DELETE_ITEM}"), 0);
	
	lr_param_sprintf("RATIO_SELECT_COLOR", "12. RATIO_SELECT_COLOR = %d", RATIO_SELECT_COLOR);
	lr_user_data_point(lr_eval_string("{RATIO_SELECT_COLOR}"), 0);

	lr_param_sprintf("RATIO_RESERVATION_HISTORY", "13. RATIO_RESERVATION_HISTORY = %d", RATIO_RESERVATION_HISTORY);
	lr_user_data_point(lr_eval_string("{RATIO_RESERVATION_HISTORY}"), 0);
	lr_param_sprintf("RATIO_POINTS_HISTORY", "13. RATIO_POINTS_HISTORY = %d", RATIO_POINTS_HISTORY);
	lr_user_data_point(lr_eval_string("{RATIO_POINTS_HISTORY}"), 0);
	lr_param_sprintf("RATIO_ORDER_HISTORY", "13. RATIO_ORDER_HISTORY = %d", RATIO_ORDER_HISTORY);
	lr_user_data_point(lr_eval_string("{RATIO_ORDER_HISTORY}"), 0);
	lr_param_sprintf("RATIO_ORDER_STATUS", "13. RATIO_ORDER_STATUS = %d", RATIO_ORDER_STATUS);
	lr_user_data_point(lr_eval_string("{RATIO_ORDER_STATUS}"), 0);

	lr_param_sprintf("RATIO_WISHLIST", "14. RATIO_WISHLIST = %d", RATIO_WISHLIST);
	lr_user_data_point(lr_eval_string("{RATIO_WISHLIST}"), 0);
	lr_param_sprintf("RATIO_WL_CREATE", "14. RATIO_WL_CREATE = %d", RATIO_WL_CREATE);
	lr_user_data_point(lr_eval_string("{RATIO_WL_CREATE}"), 0);
	lr_param_sprintf("RATIO_WL_DELETE", "14. RATIO_WL_DELETE = %d", RATIO_WL_DELETE);
	lr_user_data_point(lr_eval_string("{RATIO_WL_DELETE}"), 0);
	lr_param_sprintf("RATIO_WL_CHANGE", "14. RATIO_WL_CHANGE = %d", RATIO_WL_CHANGE);
	lr_user_data_point(lr_eval_string("{RATIO_WL_CHANGE}"), 0);
	lr_param_sprintf("RATIO_WL_DELETE_ITEM", "14. RATIO_WL_DELETE_ITEM = %d", RATIO_WL_DELETE_ITEM);
	lr_user_data_point(lr_eval_string("{RATIO_WL_DELETE_ITEM}"), 0);
	lr_param_sprintf("RATIO_WL_ADD_ITEM", "14. RATIO_WL_ADD_ITEM = %d", RATIO_WL_ADD_ITEM);
	lr_user_data_point(lr_eval_string("{RATIO_WL_ADD_ITEM}"), 0);
	
	lr_param_sprintf("NAV_BROWSE", "15. NAV_BROWSE = %d", NAV_BROWSE);
	lr_user_data_point(lr_eval_string("{NAV_BROWSE}"), 0);
	lr_param_sprintf("NAV_SEARCH", "15. NAV_SEARCH = %d", NAV_SEARCH);
	lr_user_data_point(lr_eval_string("{NAV_SEARCH}"), 0);
	lr_param_sprintf("NAV_CLEARANCE", "15. NAV_CLEARANCE = %d", NAV_CLEARANCE);
	lr_user_data_point(lr_eval_string("{NAV_CLEARANCE}"), 0);
	lr_param_sprintf("NAV_PLACE", "15. NAV_PLACE = %d", NAV_PLACE);
	lr_user_data_point(lr_eval_string("{NAV_PLACE}"), 0);

	lr_param_sprintf("DRILL_ONE_FACET", "16. DRILL_ONE_FACET = %d", DRILL_ONE_FACET);
	lr_user_data_point(lr_eval_string("{DRILL_ONE_FACET}"), 0);
	lr_param_sprintf("DRILL_TWO_FACET", "16. DRILL_TWO_FACET = %d", DRILL_TWO_FACET);
	lr_user_data_point(lr_eval_string("{DRILL_TWO_FACET}"), 0);
	lr_param_sprintf("DRILL_THREE_FACET", "16. DRILL_THREE_FACET = %d", DRILL_THREE_FACET);
	lr_user_data_point(lr_eval_string("{DRILL_THREE_FACET}"), 0);
	lr_param_sprintf("DRILL_SUB_CATEGORY", "16. DRILL_SUB_CATEGORY = %d", DRILL_SUB_CATEGORY);
	lr_user_data_point(lr_eval_string("{DRILL_SUB_CATEGORY}"), 0);

	lr_param_sprintf("APPLY_SORT", "17. APPLY_SORT = %d", APPLY_SORT);
	lr_user_data_point(lr_eval_string("{APPLY_SORT}"), 0);

	lr_param_sprintf("APPLY_PAGINATE", "17. APPLY_PAGINATE = %d", APPLY_PAGINATE);
	lr_user_data_point(lr_eval_string("{APPLY_PAGINATE}"), 0);

	lr_param_sprintf("PDP", "18. PDP = %d", PDP);
	lr_user_data_point(lr_eval_string("{PDP}"), 0);
	lr_param_sprintf("QUICKVIEW", "18. QUICKVIEW = %d", QUICKVIEW);
	lr_user_data_point(lr_eval_string("{QUICKVIEW}"), 0);
	
	lr_param_sprintf("RATIO_STORE_LOCATOR", "19. RATIO_STORE_LOCATOR = %d", RATIO_STORE_LOCATOR);
	lr_user_data_point(lr_eval_string("{RATIO_STORE_LOCATOR}"), 0);
	lr_param_sprintf("RATIO_SEARCH_SUGGEST", "19. RATIO_SEARCH_SUGGEST = %d", RATIO_SEARCH_SUGGEST);
	lr_user_data_point(lr_eval_string("{RATIO_SEARCH_SUGGEST}"), 0);
	
	lr_param_sprintf("PRODUCT_QUICKVIEW_SERVICE", "20. PRODUCT_QUICKVIEW_SERVICE = %d", PRODUCT_QUICKVIEW_SERVICE);
	lr_user_data_point(lr_eval_string("{PRODUCT_QUICKVIEW_SERVICE}"), 0);
	lr_param_sprintf("RESERVE_ONLINE", "21. RESERVE_ONLINE = %d", RESERVE_ONLINE);
	lr_user_data_point(lr_eval_string("{RESERVE_ONLINE}"), 0);
	
	lr_param_sprintf("MOVE_FROM_CART_TO_WISHLIST", "22. MOVE_FROM_CART_TO_WISHLIST = %d", MOVE_FROM_CART_TO_WISHLIST);
	lr_user_data_point(lr_eval_string("{MOVE_FROM_CART_TO_WISHLIST}"), 0);
	
	lr_param_sprintf("MOVE_FROM_WISHLIST_TO_CART", "22. MOVE_FROM_WISHLIST_TO_CART = %d", MOVE_FROM_WISHLIST_TO_CART);
	lr_user_data_point(lr_eval_string("{MOVE_FROM_WISHLIST_TO_CART}"), 0);
	
}

	
void moveFromWishlistToCart()
{
	lr_think_time ( LINK_TT );
	
	lr_start_transaction("T24_MoveFromWishlistToCart");

		lr_start_sub_transaction("T24_S01_AjaxOrderChangeServiceItemAdd", "T24_MoveFromWishlistToCart");

		web_custom_request("T24_S01_AjaxOrderChangeServiceItemAdd", 
			"URL=http://{host}/webapp/wcs/stores/servlet/AjaxOrderChangeServiceItemAdd?tcpCallBack=jQuery111300307750510271938_1473867270738&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&orderId=.&calculationUsage=-1%2C-2%2C-5%2C-6%2C-7&catEntryId={catEntryId}&quantity=1&externalId={wishListId}&_=1473867270742", 
			"Method=GET", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Referer=http://{host}/shop/us/kids-baby-gifts-registry-wishlist", 
			"Snapshot=t16.inf", 
			"Mode=HTML", 
			LAST);
		
		lr_end_sub_transaction("T24_S01_AjaxOrderChangeServiceItemAdd", LR_AUTO);

		lr_start_sub_transaction("T24_S02_CreateCookieCmd", "T24_MoveFromWishlistToCart");

		web_custom_request("T24_S02_CreateCookieCmd", 
			"URL=http://{host}/webapp/wcs/stores/servlet/CreateCookieCmd", 
			"Method=HEAD", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Referer=http://{host}/shop/us/kids-baby-gifts-registry-wishlist", 
			"Snapshot=t17.inf", 
			"Mode=HTML", 
			LAST);

		lr_end_sub_transaction("T24_S02_CreateCookieCmd", LR_AUTO);

		createCookieCmd();

	lr_end_transaction("T24_MoveFromWishlistToCart", LR_AUTO);
	
}

void moveFromCartToWishlist()
{
	lr_think_time ( LINK_TT );
	
	web_reg_save_param("WishlistIDs", "LB=\"giftListExternalIdentifier\": ", "RB=,", "ORD=All", "NotFound=Warning", LAST);
	
	web_custom_request("moveFromCartToWishlist_TCPGetWishListForUsersView",  //Get wishlist IDs
			"URL=http://{host}/webapp/wcs/stores/servlet/TCPGetWishListForUsersView?tcpCallBack=jQuery1113014590106982485151_1473881708524&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&sortBy=&_=1473881708525", 
			"Method=GET", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Mode=HTML", 
			"EncType=application/x-www-form-urlencoded", 
			LAST);
	
	viewCart(); //This is needed

	if ( atoi( lr_eval_string("{orderItemIds_count}") ) != 0 && atoi( lr_eval_string("{WishlistIDs_count}") ) != 0 && atoi( lr_eval_string("{catalogIds_count}") ) != 0) {
		
		lr_save_string ( lr_paramarr_random( "orderItemIds" ) , "orderItemId" ) ;
		lr_save_string ( lr_paramarr_idx( "WishlistIDs" , 1 ) , "wishListId" ) ;
		lr_save_string ( lr_paramarr_idx( "catalogIds" , 1 ) , "catEntryId" ) ;

		lr_start_transaction("T23_MoveFromCartToWishlist");
	
			lr_start_sub_transaction("T23_S01_AjaxOrderChangeServiceItemDelete", "T23_MoveFromCartToWishlist");

			web_submit_data("T23_S01_AjaxOrderChangeServiceItemDelete", 
				"Action=https://{host}/webapp/wcs/stores/servlet/AjaxOrderChangeServiceItemDelete", 
				"Method=POST", 
				"RecContentType=application/json", 
				"Mode=HTML", 
				ITEMDATA, 
				"Name=storeId", "Value={storeId}", ENDITEM, 
				"Name=catalogId", "Value={catalogId}", ENDITEM, 
				"Name=langId", "Value=-1", ENDITEM, 
				"Name=orderId", "Value={orderId}", ENDITEM, 
				"Name=orderItemId", "Value={orderItemId}", ENDITEM, 
				"Name=visitorId", "Value=[CS]v1|2BECB4F385078A07-4000010DC0038E68[CE]", ENDITEM, 
				"Name=calculationUsage", "Value=-1,-2,-5,-6,-7", ENDITEM, 
				"Name=requesttype", "Value=ajax", ENDITEM, 
				LAST);
				
			lr_end_sub_transaction("T23_S01_AjaxOrderChangeServiceItemDelete", LR_AUTO);
		
			lr_start_sub_transaction("T23_S02_AjaxGiftListServiceAddItem", "T23_MoveFromCartToWishlist");
			
			web_custom_request("T23_S02_AjaxGiftListServiceAddItem", 
				"URL=https://{host}/webapp/wcs/stores/servlet/AjaxGiftListServiceAddItem?tcpCallBack=jQuery111303919879446517399_1473869598542&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&giftListId={wishListId}&catEntryId_1={catEntryId}&quantity_1=1&_=1473869598545", 
				"Method=GET", 
				"Resource=0", 
				"RecContentType=text/html", 
				"Mode=HTML", 
				"EncType=application/x-www-form-urlencoded", 
				LAST);
		
			lr_end_sub_transaction("T23_S02_AjaxGiftListServiceAddItem", LR_AUTO);

			lr_start_sub_transaction("T23_S03_CreateCookieCmd", "T23_MoveFromCartToWishlist");

			web_custom_request("T23_S03_CreateCookieCmd", 
				"URL=https://{host}/webapp/wcs/stores/servlet/CreateCookieCmd?langId=-1&storeId={storeId}&goToPage=NoURL&catalogId={catalogId}", 
				"Method=GET", 
				"Resource=0", 
				"RecContentType=text/html", 
				"Mode=HTML", 
				"EncType=application/x-www-form-urlencoded", 
				LAST);
		
			lr_end_sub_transaction("T23_S03_CreateCookieCmd", LR_AUTO);

			createCookieCmd();

			lr_start_sub_transaction("T23_S04_TCPMiniShopCartDisplayView1", "T23_MoveFromCartToWishlist");
			
			web_submit_data("T23_S04_TCPMiniShopCartDisplayView1", 
				"Action=https://{host}/shop/TCPMiniShopCartDisplayView1?catalogId={catalogId}&langId=-1&storeId={storeId}", 
				"Method=POST", 
				"RecContentType=text/html", 
				"Mode=HTML", 
				ITEMDATA, 
				"Name=showredEye", "Value=true", ENDITEM, 
				"Name=objectId", "Value=", ENDITEM, 
				"Name=requesttype", "Value=ajax", ENDITEM, 
				LAST);
		
			lr_end_sub_transaction("T23_S04_TCPMiniShopCartDisplayView1", LR_AUTO);

	//		web_set_sockets_option("SSL_VERSION", "TLS");
		
			lr_start_sub_transaction("T23_S05_ShopCartDisplayView", "T23_MoveFromCartToWishlist");
			
			web_submit_data("T23_S05_ShopCartDisplayView", 
				"Action=https://{host}/shop/ShopCartDisplayView?shipmentType=single&catalogId={catalogId}&langId=-1&storeId={storeId}", 
				"Method=POST", 
				"RecContentType=text/html", 
				"Mode=HTML", 
				ITEMDATA, 
				"Name=showredEye", "Value=true", ENDITEM, 
				"Name=objectId", "Value=", ENDITEM, 
				"Name=requesttype", "Value=ajax", ENDITEM, 
				LAST);
			lr_end_sub_transaction("T23_S05_ShopCartDisplayView", LR_AUTO);
	
		lr_end_transaction("T23_MoveFromCartToWishlist",LR_AUTO);
	}

//	viewCart();
	
}	

int getPromoCode() { //action is either "get" or "delete"
    
    int row, rc, loopCtr = 0;
/*
    int rNum;
    unsigned short updateStatus;
    char **colnames = NULL;
    char **rowdata = NULL;
	PVCI2 pvci = 0;
*/
    char FieldValue[50];
    int totalCoupon = lr_get_attrib_long("SinglePromoCodeCount");
	char  *VtsServer = "10.56.29.36";
	int   nPort = 8888;
/*    
	vtc_clear_row(pvci, rNum, &updateStatus); //delete the code from the datafile
    vtc_free_list(colnames);
    vtc_free_list(rowdata);
    vtc_disconnect( pvci );	
*/	
    pvci = vtc_connect(VtsServer,nPort,VTOPT_KEEP_ALIVE);
//   	lr_log_message("rc=%d\n loopCtr=%d\n totalCoupon=%d\n", pvci,loopCtr,totalCoupon);
        
//	while (!rc) {
	
	while (pvci != 0) {
   		
        rNum = rand() % totalCoupon + 1;
        loopCtr++;
        vtc_query_row(pvci, rNum, &colnames, &rowdata);
		//lr_output_message("Query Row Names : %s", colnames[0]);
		//lr_output_message("Query Row Data  : %s", rowdata[0]);            
        
        if ( strcmp(rowdata[0], "") != 0) { //if row is not blank, save it to a parameter then delete it from the table

        	lr_save_string(rowdata[0], "promocode"); //save the code
//        	lr_output_message("Promo Code  : %s", lr_eval_string("{promocode}") );
        	vtc_clear_row(pvci, rNum, &updateStatus); //delete the code from the datafile

            break;
        }
    }

    if ( strcmp(lr_eval_string("{promocode}"), "") == 0) {
        lr_error_message("Single-Use promo code out of data.");
//        return LR_FAIL;
    }
	    vtc_free_list(colnames);
	    vtc_free_list(rowdata);
		rc = vtc_disconnect( pvci );

    return LR_PASS;
    
}

void newsLetterSignup()
{
	lr_think_time ( LINK_TT ) ;
	
	web_set_sockets_option("SSL_VERSION", "TLS1.1");

	lr_start_transaction("T26_NewsLetterSignup");

		lr_start_sub_transaction("T26_NewsLetterSignup_S01_email-subscribe", "T26_NewsLetterSignup");

		web_url("email-subscribe", 
			"URL=https://{host}/shop/us/content/email-subscribe", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Referer=", 
			"Snapshot=t3.inf", 
			"Mode=HTML", 
			LAST);
			
		lr_end_sub_transaction("T26_NewsLetterSignup_S01_email-subscribe", LR_AUTO);
		
		lr_start_sub_transaction("T26_NewsLetterSignup_S02_TCPAjaxEmailVerificationCmd", "T26_NewsLetterSignup");

		web_submit_data("TCPAjaxEmailVerificationCmd", 
			"Action=https://{host}/webapp/wcs/stores/servlet/TCPAjaxEmailVerificationCmd", 
			"Method=POST", 
			"RecContentType=application/json", 
			"Referer=https://{host}/shop/us/content/email-subscribe", 
			"Snapshot=t7.inf", 
			"Mode=HTML", 
			"EncodeAtSign=YES", 
			ITEMDATA, 
			"Name=email", "Value={emailVerification}@gmail.com", ENDITEM, 
			"Name=page", "Value=newsletterSignUp", ENDITEM, 
			"Name=requesttype", "Value=ajax", ENDITEM, 
			LAST);

		lr_end_sub_transaction("T26_NewsLetterSignup_S02_TCPAjaxEmailVerificationCmd", LR_AUTO);

//		web_reg_find("Text=You will receive your first email from us shortly.", LAST);

		lr_start_sub_transaction("T26_NewsLetterSignup_S03_AddEmailCmd", "T26_NewsLetterSignup");

		web_submit_data("AddEmailCmd", 
			"Action=https://{host}/shop/us/content/AddEmailCmd", 
			"Method=POST", 
			"RecContentType=text/html", 
			"Referer=https://{host}/shop/us/content/email-subscribe", 
			"Snapshot=t8.inf", 
			"Mode=HTML", 
			"EncodeAtSign=YES", 
			ITEMDATA, 
			"Name=emailaddr", "Value={emailVerification}@gmail.com", ENDITEM, 
			"Name=storeId", "Value={storeId}", ENDITEM, 
			"Name=catalogId", "Value={catalogId}", ENDITEM, 
			"Name=langId", "Value=-1", ENDITEM, 
			"Name=URL", "Value=email-confirmation", ENDITEM, 
			"Name=response", "Value=valid::false:false", ENDITEM, 
			LAST);

		lr_end_sub_transaction("T26_NewsLetterSignup_S03_AddEmailCmd", LR_AUTO);

	lr_end_transaction("T26_NewsLetterSignup", LR_AUTO);
}

void createCookieCmd()
{
	lr_start_transaction("T28_CreateCookieCmd");

	web_custom_request("CreateCookieCmd", 
		"URL=https://{host}/webapp/wcs/stores/servlet/CreateCookieCmd?langId=-1&storeId={storeId}&goToPage=NoURL&catalogId={catalogId}", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Mode=HTML", 
		"EncType=application/x-www-form-urlencoded", 
		LAST);

	lr_end_transaction("T28_CreateCookieCmd", LR_AUTO);
		
}