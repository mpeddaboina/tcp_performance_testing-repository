#include "..\..\browseWorkloadModel.c"

int index , length , i, randomPercent, isLoggedIn;
char *searchString ;
char *nav_by ; // T02 - Category Navigation selection
char *drill_by ; // T03 - Facet / SubCategory drill selection
char *sort_by; // T03 - Sort selection
char *product_by; // T04 - Product Display Method
//int form_TT, link_TT, typeSpeed_TT;
typedef long time_t;
time_t t;
//int authcookielen , authtokenlen ;
//authcookielen = 0;

void webURL(char *Url, char *pageName)
{
	lr_save_string(Url, "Url");
	lr_save_string(pageName, "pageName");
//	lr_save_string(textCheck, "textCheck");
//	web_reg_find("SaveCount=txtCheckCount", "Text/IC={textCheck}", LAST);
	
	web_url ( lr_eval_string("{pageName}") ,
    	     "URL={Url}" ,
             "Resource=0" ,
             "Mode=HTML" ,
             LAST ) ;
/*	
	if ( atoi(lr_eval_string("{txtCheckCount}")) > 0 )
		return 0;
	else
		return 1;
*/
			 
}//end webURL
/*
int web_api(char *apiCall, char *method)
{	
	web_add_header("storeId", lr_eval_string("{storeId}"));
	lr_param_sprintf ( "apiURL" , "https://api-%s/%s" , lr_eval_string("{host}"), apiCall ) ;
	web_url("apiCall",  
//		"URL=https://api-{host}/getCountryListAndHeaderInfo", 
		"URL={apiURL}", 
		"RecContentType=text/html", 
		"Mode=HTML", 
		LAST);

	return 0;
}
*/
webURL2(char *Url, char *pageName, char *textCheck)
{
	lr_save_string(Url, "Url");
	lr_save_string(pageName, "pageName");
	lr_save_string(textCheck, "textCheck");
	web_reg_find("SaveCount=txtCheckCount", "Text/IC={textCheck}", LAST);
	
	//web_reg_find("SaveCount=noProducts", "Text/IC=We're sorry, there are no products available for purchase at this time. Please check back later.", LAST);
	
	web_url ( lr_eval_string("{pageName}") ,
    	     "URL={Url}" ,
             "Resource=0" ,
             "Mode=HTML" ,
             LAST ) ;

	if ( atoi(lr_eval_string("{txtCheckCount}")) > 0 )
		return LR_PASS;
	else
		return LR_FAIL;
			 
}//end webURL


void endIteration()
{
	//lr_message ( "in endIteration" ) ;
}

void homePageCorrelations()
{/*
	web_reg_save_param ( "categories" ,
				 "LB=\"url\": \"http://{host}{store_home_page}c/" , 
				 "RB=/" ,
				 "ORD=ALL" , "Convert=HTML_TO_TEXT" , "NotFound=Warning", LAST ) ;
	*/			 
	 web_reg_save_param ( "clearances" ,
			 "LB=\"url\": \"http://{host}/shop/SearchDisplay?" , //works in prod and perf
			 "RB=\"" ,
			 "ORD=ALL" , "Convert=HTML_TO_TEXT" , "NotFound=Warning", LAST ) ;
				 
web_reg_save_param_json(
        "ParamName=categories",
        "QueryString=$.headerInfo[:1].*.categoryAttributes.url",
        "SelectAll=Yes",
        "SEARCH_FILTERS",
        "Scope=Body",
        "LAST"); 
}

void homePageCorrelations_OLD() //correlations needed for category navigations
{
   	web_reg_save_param ( "catalogId" ,
	                     "LB=catalogId=" ,
	                     "RB=&",
						 "ORD=1",
	                     "NotFound=Warning", LAST ) ;

	web_reg_save_param ( "storeId" ,
	                     "LB=storeId=" ,
	                     "RB=&",
						 "ORD=1",
	                     "NotFound=Warning", LAST ) ;

	web_reg_save_param ( "categories" ,
				 "LB=\t\t\t\t<a href=\"http://{host}{store_home_page}c/" , 
				 "RB=\"" ,
				 "ORD=ALL" , "Convert=HTML_TO_TEXT" , "NotFound=Warning", LAST ) ;
				 
	
				 web_reg_save_param ( "clearances" ,
						 "LB=<a href=\"http://{host}/shop/SearchDisplay?" , //works in prod and perf
						 "RB=\"" ,
						 "ORD=ALL" , "Convert=HTML_TO_TEXT" , "NotFound=Warning", LAST ) ;
	
	// include correlation for Place Shop (in PERF environment)
	web_reg_save_param ( "searchAutoSuggestURL" ,
						 "LB=setAutoSuggestURL('",
						 "RB='" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=Warning", LAST ) ;

	web_reg_save_param ( "searchCachedSuggestionsURL" ,
						 "LB=setCachedSuggestionsURL('" ,
						 "RB='" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=Warning", LAST ) ;

//						 <a href="http://tcp-perf.childrensplace.com/shop/us/content/mvp-shop-spring-2014" 
   	web_reg_save_param ( "placeShopCategory" ,
						 "LB={store_home_page}content/" ,
	                     "RB=\"",
						 "ORD=All",
	                     "NotFound=Warning", LAST ) ;

}//end homePageCorrelations()


void categoryPageCorrelations() //correlations needed for facet and subcategory navigations
{


	web_reg_save_param ( "subcategories" ,
	                     "LB=class='notSelected' href='http://{host}" ,
	                     "RB='>" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 LAST ) ;
/*						 
web_reg_save_param_json(
        "ParamName=subcategories",
        "QueryString=$..*.subCategories.*.categoryAttributes.url",
        "SelectAll=Yes",
        "SEARCH_FILTERS",
        "Scope=Body",
        "LAST"); 
*/

						 
	web_reg_save_param ( "facetsURL" ,
				  		 "LB=refreshAfterFilterGotSelected('http://{host}" ,
//				  		 "LB=onclick=\"refreshFacetsFilter('http://{host}" ,
	                   	 "RB='" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 LAST ) ;

	web_reg_save_param_regexp( "ParamName=facetsID" ,
	                           "RegExp=facetCheckBox\" id =\"(.*?)\" value=\"(.*?)\"" ,
			                   "Ordinal=ALL" ,
			                   "Notfound=warning" ,
			                   SEARCH_FILTERS ,
			                   "Group=2" ,
			                   LAST ) ;

	web_reg_save_param ( "sortURL" ,
				  		 "LB=<option value=\"http://{host}" ,
	                   	 "RB=\"" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 LAST ) ;

	web_reg_save_param ( "searchSubCategories" ,
				  		 "LB=href='http://{host}" ,
	                   	 "RB='>" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 LAST ) ;

	web_reg_save_param ( "paginationNextURL" ,
				  		 "LB=goToResultPage('http://{host}" ,
	                   	 "RB='" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 LAST ) ;

	web_reg_save_param ( "paginationPageIndex" ,
				  		 "LB=catalogSearchResultDisplay_Context','" ,
	                   	 "RB='" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 LAST ) ;

	web_reg_save_param ( "paginationShowAllURL" ,
				  		 "LB=viewall-right-bottom\">\r\n\t\t\t\t\t                <a href=\"http://{host}" ,
	                   	 "RB=\"" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 LAST ) ;
						 
}//end categoryPageCorrelations()


void subCategoryPageCorrelations() //correlations needed for Product Display & Quick View navigations
{

	web_reg_save_param ( "pdpURL" ,
//	                     "LB=productRow name\">\r\n\t\t\t\r\n\t\t\t\t<a href=\"http://{host}" , //tcp-perf
	                     "LB=productRow name\">\r\n\t\r\n\t\t\t\r\n\t\t\t\t\t\r\n\t\t\t\t<a href=\"http://{host}" , //uatlive3
	                     "RB=\" id" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 LAST ) ;

	web_reg_save_param ( "quickviewURL" ,
	                     "LB=openModal2\" link=\"http://{host}" ,
						 "RB=\" href=" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 LAST ) ;
						 
	web_reg_find("SaveCount=noProducts", "Text/IC=We're sorry, there are no products available for purchase at this time. Please check back later.", LAST);

	
}//end subCategoryPageCorrelations()


void productDisplayPageCorrelations()
{

	web_reg_save_param ( "skuSelectionSwitchProductID" ,
	                     "LB=confirmSwitchProduct(event,'product_" ,
	                     "RB='" ,
	                     "notFound=Warning",
	                     "ORD=ALL" ,
	                     LAST ) ;

	web_reg_save_param ( "skuSelectionProductID" ,
	                     "LB=input type=\"hidden\" id=\"prodId\" value=\"" ,
	                     "RB=\"" ,
	                     "notFound=Warning",
	                     LAST ) ;
						 
} //end productDisplayPageCorrelations()


//void Correlations()
void addToCartCorrelations()
{
	web_reg_save_param_regexp( "ParamName=atc_catentryIds" ,
	                           "RegExp=\"catentry_id\" : \"(.*?)\",\r\n\t\t\t\t\t\t\t\t\t\t\t\"item_sku\" : \"(.*?)\",\r\n\t\t\t\t\t\t\t\t\t\t\t\"qty\" : \"([1-9][0-9]*)\"" ,
			                   "Ordinal=ALL" ,
			                   "Notfound=warning" ,
			                   SEARCH_FILTERS ,
			                   "Group=1" ,
			                   LAST ) ;

 	web_reg_save_param ( "atc_comment" ,
                    	 "LB=<input type=\"hidden\" name=\"comment\" value=\"" ,
                    	 "RB=\"" ,
                    	 "Notfound=warning" ,
                    	 LAST ) ;

} // end addToCartCorrelations()

void viewHomePage()
{
	//lr_message ( "in viewHomePage" ) ;
	//homePageCorrelations();
	homePageCorrelations_OLD();
	web_add_auto_header("storeId", lr_eval_string("{storeId}"));
	lr_param_sprintf ( "homePageUrl" , "http://%s%shome" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}") ) ;
	lr_start_transaction( "T01_Home Page" ) ;
	
		if ( webURL2(lr_eval_string ( "{homePageUrl}" ), "T01_Home Page" , "BEGIN TCPHomePageContentAreaESpot.jsp" ) == LR_PASS)
			{
//				web_api("getCountryListAndHeaderInfo"); //https://api-uatlive3.childrensplace.com/getCountryListAndHeaderInfo
/*				
					web_custom_request("getCountryListAndHeaderInfo", 
						"URL=https://api-{host}/getCountryListAndHeaderInfo", 
						"Method=OPTIONS", 
						"Resource=0", 
						"Referer=https://{host}/shop/us/home", 
						"Snapshot=t14.inf", 
						"Mode=HTTP", 
						LAST);

					web_custom_request("getCountryListAndHeaderInfo_2", 
						"URL=https://api-{host}/getCountryListAndHeaderInfo", 
						"Method=OPTIONS", 
						"Resource=0", 
						"Referer=https://{host}/shop/us/home", 
						"Snapshot=t15.inf", 
						"Mode=HTTP", 
						LAST);

					web_custom_request("getAvailableOffers", 
						"URL=https://api-{host}/payment/getAvailableOffers", 
						"Method=OPTIONS", 
						"Resource=0", 
						"Referer=https://{host}/shop/us/home", 
						"Snapshot=t19.inf", 
						"Mode=HTTP", 
						LAST);

					web_custom_request("getESpot", 
						"URL=https://api-{host}/getESpot", 
						"Method=OPTIONS", 
						"Resource=0", 
						"Referer=https://{host}/shop/us/home", 
						"Snapshot=t20.inf", 
						"Mode=HTTP", 
						LAST);

					web_custom_request("getOrderDetails", 
						"URL=https://api-{host}/getOrderDetails", 
						"Method=OPTIONS", 
						"Resource=0", 
						"Referer=https://{host}/shop/us/home", 
						"Snapshot=t21.inf", 
						"Mode=HTTP", 
						LAST);

					web_custom_request("getRegisteredUserDetailsInfo", 
						"URL=https://api-{host}/payment/getRegisteredUserDetailsInfo", 
						"Method=OPTIONS", 
						"Resource=0", 
						"Referer=https://{host}/shop/us/home", 
						"Snapshot=t22.inf", 
						"Mode=HTTP", 
						LAST);
*/
					web_custom_request("getCountryListAndHeaderInfo_3", 
						"URL=https://api-{host}/getCountryListAndHeaderInfo", 
						"Method=GET", 
						"Resource=0", 
						"RecContentType=application/json", 
						"Referer=https://{host}/shop/us/home", 
						"Snapshot=t23.inf", 
						LAST);

	homePageCorrelations();
	
					web_custom_request("getCountryListAndHeaderInfo_4", 
						"URL=https://api-{host}/getCountryListAndHeaderInfo", 
						"Method=GET", 
						"Resource=0", 
						"RecContentType=application/json", 
						"Referer=https://{host}/shop/us/home", 
						"Snapshot=t25.inf", 
						LAST);

					web_custom_request("getAvailableOffers_2", 
						"URL=https://api-{host}/payment/getAvailableOffers", 
						"Method=GET", 
						"Resource=0", 
						"RecContentType=application/json", 
						"Referer=https://{host}/shop/us/home", 
						"Snapshot=t27.inf", 
						LAST);

					web_custom_request("getESpot_2", 
						"URL=https://api-{host}/getESpot", 
						"Method=GET", 
						"Resource=0", 
						"RecContentType=application/json", 
						"Referer=https://{host}/shop/us/home", 
						"Snapshot=t29.inf", 
						LAST);

					web_custom_request("getOrderDetails_2", 
						"URL=https://api-{host}/getOrderDetails", 
						"Method=GET", 
						"Resource=0", 
						"RecContentType=application/json", 
						"Referer=https://{host}/shop/us/home", 
						"Snapshot=t32.inf", 
						LAST);

					web_custom_request("getRegisteredUserDetailsInfo_2", 
						"URL=https://api-{host}/payment/getRegisteredUserDetailsInfo", 
						"Method=GET", 
						"Resource=0", 
						"RecContentType=application/json", 
						"Referer=https://{host}/shop/us/home", 
						"Snapshot=t33.inf", 
						LAST);

/*
					web_custom_request("getCoupon", 
						"URL=https://api-{host}/payment/getCoupon", 
						"Method=OPTIONS", 
						"Resource=0", 
						"Referer=https://{host}/shop/us/home", 
						"Snapshot=t41.inf", 
						"Mode=HTTP", 
						LAST);

					web_custom_request("getPointsService", 
						"URL=https://api-{host}/payment/getPointsService", 
						"Method=OPTIONS", 
						"Resource=0", 
						"Referer=https://{host}/shop/us/home", 
						"Snapshot=t43.inf", 
						"Mode=HTTP", 
						LAST);
*/
					web_custom_request("getCoupon_2", 
						"URL=https://api-{host}/payment/getCoupon", 
						"Method=GET", 
						"Resource=0", 
						"RecContentType=application/json", 
						"Referer=https://{host}/shop/us/home", 
						"Snapshot=t47.inf", 
						LAST);

					web_custom_request("getPointsService_2", 
						"URL=https://api-{host}/payment/getPointsService", 
						"Method=GET", 
						"Resource=0", 
						"RecContentType=application/json", 
						"Referer=https://{host}/shop/us/home", 
						"Snapshot=t48.inf", 
						LAST);


				lr_end_transaction( "T01_Home Page" , LR_AUTO ) ;
			}
			
		else{
					web_custom_request("getCountryListAndHeaderInfo", 
						"URL=https://api-{host}/getCountryListAndHeaderInfo", 
						"Method=OPTIONS", 
						"Resource=0", 
						"Referer=https://{host}/shop/us/home", 
						"Snapshot=t14.inf", 
						"Mode=HTTP", 
						LAST);

					web_custom_request("getCountryListAndHeaderInfo_2", 
						"URL=https://api-{host}/getCountryListAndHeaderInfo", 
						"Method=OPTIONS", 
						"Resource=0", 
						"Referer=https://{host}/shop/us/home", 
						"Snapshot=t15.inf", 
						"Mode=HTTP", 
						LAST);

					web_custom_request("getAvailableOffers", 
						"URL=https://api-{host}/payment/getAvailableOffers", 
						"Method=OPTIONS", 
						"Resource=0", 
						"Referer=https://{host}/shop/us/home", 
						"Snapshot=t19.inf", 
						"Mode=HTTP", 
						LAST);

					web_custom_request("getESpot", 
						"URL=https://api-{host}/getESpot", 
						"Method=OPTIONS", 
						"Resource=0", 
						"Referer=https://{host}/shop/us/home", 
						"Snapshot=t20.inf", 
						"Mode=HTTP", 
						LAST);

					web_custom_request("getOrderDetails", 
						"URL=https://api-{host}/getOrderDetails", 
						"Method=OPTIONS", 
						"Resource=0", 
						"Referer=https://{host}/shop/us/home", 
						"Snapshot=t21.inf", 
						"Mode=HTTP", 
						LAST);

					web_custom_request("getRegisteredUserDetailsInfo", 
						"URL=https://api-{host}/payment/getRegisteredUserDetailsInfo", 
						"Method=OPTIONS", 
						"Resource=0", 
						"Referer=https://{host}/shop/us/home", 
						"Snapshot=t22.inf", 
						"Mode=HTTP", 
						LAST);

					web_custom_request("getCountryListAndHeaderInfo_3", 
						"URL=https://api-{host}/getCountryListAndHeaderInfo", 
						"Method=GET", 
						"Resource=0", 
						"RecContentType=application/json", 
						"Referer=https://{host}/shop/us/home", 
						"Snapshot=t23.inf", 
						LAST);

					web_custom_request("getCountryListAndHeaderInfo_4", 
						"URL=https://api-{host}/getCountryListAndHeaderInfo", 
						"Method=GET", 
						"Resource=0", 
						"RecContentType=application/json", 
						"Referer=https://{host}/shop/us/home", 
						"Snapshot=t25.inf", 
						LAST);

					web_custom_request("getAvailableOffers_2", 
						"URL=https://api-{host}/payment/getAvailableOffers", 
						"Method=GET", 
						"Resource=0", 
						"RecContentType=application/json", 
						"Referer=https://{host}/shop/us/home", 
						"Snapshot=t27.inf", 
						LAST);

					web_custom_request("getESpot_2", 
						"URL=https://api-{host}/getESpot", 
						"Method=GET", 
						"Resource=0", 
						"RecContentType=application/json", 
						"Referer=https://{host}/shop/us/home", 
						"Snapshot=t29.inf", 
						LAST);

					web_custom_request("getOrderDetails_2", 
						"URL=https://api-{host}/getOrderDetails", 
						"Method=GET", 
						"Resource=0", 
						"RecContentType=application/json", 
						"Referer=https://{host}/shop/us/home", 
						"Snapshot=t32.inf", 
						LAST);

					web_custom_request("getRegisteredUserDetailsInfo_2", 
						"URL=https://api-{host}/payment/getRegisteredUserDetailsInfo", 
						"Method=GET", 
						"Resource=0", 
						"RecContentType=application/json", 
						"Referer=https://{host}/shop/us/home", 
						"Snapshot=t33.inf", 
						LAST);


					web_custom_request("getCoupon", 
						"URL=https://api-{host}/payment/getCoupon", 
						"Method=OPTIONS", 
						"Resource=0", 
						"Referer=https://{host}/shop/us/home", 
						"Snapshot=t41.inf", 
						"Mode=HTTP", 
						LAST);

					web_custom_request("getPointsService", 
						"URL=https://api-{host}/payment/getPointsService", 
						"Method=OPTIONS", 
						"Resource=0", 
						"Referer=https://{host}/shop/us/home", 
						"Snapshot=t43.inf", 
						"Mode=HTTP", 
						LAST);

					web_custom_request("getCoupon_2", 
						"URL=https://api-{host}/payment/getCoupon", 
						"Method=GET", 
						"Resource=0", 
						"RecContentType=application/json", 
						"Referer=https://{host}/shop/us/home", 
						"Snapshot=t47.inf", 
						LAST);

					web_custom_request("getPointsService_2", 
						"URL=https://api-{host}/payment/getPointsService", 
						"Method=GET", 
						"Resource=0", 
						"RecContentType=application/json", 
						"Referer=https://{host}/shop/us/home", 
						"Snapshot=t48.inf", 
						LAST);			
						
			//lr_error_message("T01_Home Page Text Check Failed - {homePageUrl}");
			lr_fail_trans_with_error( lr_eval_string("T01_Home Page Text Check Failed - {homePageUrl}") ) ;
			lr_end_transaction( "T01_Home Page" , LR_FAIL ) ;
		}
//		https://api-uatlive3.childrensplace.com/getCountryListAndHeaderInfo
		
} // end viewHomePage

int getTopCategories()
{

	web_reg_save_param("aaaaaa", "LB=url\": \"", "RB=\"", "ORD=All", "NotFound=Warning", LAST);
	
//	lr_start_transaction("T0?-getTopCategories");
	
	web_submit_data("TCPLocaleSelection",
	    "Action=http://{host}/shop/{store}/getTopCategories",
		"Method=POST", 
		"Resource=0", 
		"Mode=HTML", 
		ITEMDATA,
            "Name=storeId", "Value={storeId}", ENDITEM,
            "Name=catalogId", "Value={catalogId}", ENDITEM,
		LAST);
		
//	if( strcmp(lr_eval_string("{redirectToStore}"), "us") == 0 || strcmp(lr_eval_string("{redirectToStore}"), "ca") == 0)
//		return lr_end_transaction("T0?-getTopCategories", LR_PASS);
//	else
//		return lr_end_transaction("T0?-getTopCategories", LR_FAIL);	
	return 0;
}


void navByBrowse()
{
	lr_think_time ( LINK_TT ) ;
		
//	index = rand ( ) % lr_paramarr_len( "categories" ) + 1 ;
//	lr_param_sprintf ( "cateogryPageUrl" , "http://%s%ssearch%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}") , lr_paramarr_idx ( "categories" , index ) ) ;
//	lr_param_sprintf ( "cateogryPageUrl" , "http://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/") , lr_paramarr_random ( "categories" ) ) ; 
	
//	lr_save_string( lr_paramarr_random ( "categories" ), "category");
	
	while( strcmp(lr_eval_string("{storeId}"), "10152") == 0 && (
	      strcmp(lr_eval_string("{category}"), "childrens-shoes-kids-shoes-canada") == 0 || 
		  strcmp(lr_eval_string("{category}"), "kids-accessories-canada" ) == 0 ) ) 
	{
		lr_save_string( lr_paramarr_random ( "categories" ), "category");
	}

		index = rand () % 6 + 1 ;
		switch(index)
		{
			case 1: lr_save_string( "girls-clothing", "category"); break;
			case 2: lr_save_string( "toddler-girl-clothes", "category"); break;
			case 3: lr_save_string( "boys-clothing", "category"); break;
			case 4: lr_save_string( "toddler-boy-clothes", "category"); break;
			case 5: lr_save_string( "baby-clothes", "category"); break;
			case 6: lr_save_string( "kids-clearance-clothing", "category"); break;
//			case 6: lr_save_string( "childrens-shoes-kids-shoes", "category"); break;
//			case 7: lr_save_string( "kids-accessories-us", "category"); break;
			default: break;
		}

	lr_param_sprintf ( "cateogryPageUrl" , "http://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/") , lr_eval_string( "{category}" ) ) ; 
	
	categoryPageCorrelations();
	subCategoryPageCorrelations(); /// rhy added 11/23/2015, 11/27 without this pdpURL will not have any value

	lr_continue_on_error(1);

	lr_start_transaction( "T02_Category Display" ) ;
	
//		webURL(lr_eval_string ( "{cateogryPageUrl}" ), "T02_Category Display" );
	if (webURL2(lr_eval_string ( "{cateogryPageUrl}" ), "T02_Category Display", "End - SEO H1/Body Contents for Category page" ) == LR_PASS ) {
	
		if ( isLoggedIn == 1 ) {
			//TCPGetWishListForUsersView(); //1204 9pm to bring down this number
		}
		
		if ((lr_paramarr_len("subcategories")) == 0)
		{
			lr_fail_trans_with_error( lr_eval_string("T02_Category Display Text Check Failed - {cateogryPageUrl}") ) ;
			lr_end_transaction ( "T02_Category Display" , LR_FAIL ) ; // confirm.
		} // end if
		else
			lr_end_transaction ( "T02_Category Display" , LR_PASS ) ;
	}
	else {
		lr_fail_trans_with_error( lr_eval_string("T02_Category Display Text Check Failed - {cateogryPageUrl}") ) ;
		lr_end_transaction ( "T02_Category Display" , LR_FAIL ) ; // confirm.
	}
	
	lr_continue_on_error(0);
	
} // end navByBrowse


void navByClearance()
{
	//lr_message ( "In navByClearance" ) ;
 	lr_think_time ( LINK_TT ) ;
	//index = rand ( ) % lr_paramarr_len( "clearances" ) + 1 ;
//	findNewArrivals(); //NOT NEEDED ANYMORE, noProducts handling already in place
//	lr_param_sprintf ( "clearancePageUrl" , "http://%s/shop/SearchDisplay?%s" , lr_eval_string("{host}"),  lr_paramarr_idx ( "clearances" , index ) ) ;
	lr_param_sprintf ( "clearancePageUrl" , "http://%s/shop/SearchDisplay?%s" , lr_eval_string("{host}"),  lr_paramarr_random( "clearances" ) ) ; 
	//lr_param_sprintf ( "clearancePageUrl" , "%s", "http://tcp-perf.childrensplace.com/shop/SearchDisplay?storeId=10151&catalogId=10551&langId=-1&beginIndex=0&searchSource=Q&sType=SimpleSearch&showResultsPage=true&pageView=image&facet=ads_f10001_ntk_cs:%22New Arrivals%22&categoryId=47502" );

	categoryPageCorrelations();

	subCategoryPageCorrelations(); /// rhy added 11/23/2015, 11/27 without this pdpURL will not have any vale
	web_reg_find("SaveCount=noProducts", "Text/IC=We're sorry, there are no products available for purchase at this time. Please check back later.", LAST);
	web_reg_find("Text=FILTER BY", "SaveCount=FilterBy_Count", LAST );
//	web_reg_save_param("Mikemoro12", "LB=<div class=\"categoryContent\">", "RB=there are no products available for purchase at this time. Please check back later","NotFound=Empty","Convert=HTML_TO_TEXT" ,"Ord=1",LAST);
	lr_start_transaction ( "T02_Clearance Display" ) ;
	
		webURL(lr_eval_string ( "{clearancePageUrl}" ), "T02_Clearance Display" );

//		if ( isLoggedIn == 1 ) {
//			TCPGetWishListForUsersView(); //1205 3pm to bring down this number
//		}
	
	if ( atoi(lr_eval_string("{noProducts}")) == 1 ) {
		lr_end_transaction ( "T02_Clearance Display" , LR_FAIL ) ;
//		lr_error_message("T02_Clearance Display  : %s", lr_eval_string("{clearancePageUrl}")); //Pavan Change
//		lr_output_message("T02_Clearance Display : %s", lr_eval_string("{clearancePageUrl}")); //Pavan Change
		lr_param_sprintf ( "failedPageUrl" , "T02_Clearance Display Failure : %s", lr_eval_string("{clearancePageUrl}") ) ;
		writeFailuresToFile();		

	}
/*	
	else if ( atoi(lr_eval_string("{FilterBy_Count}")) == 0 )
	{
		lr_output_message("Not Found FILTER BY: %s", lr_eval_string ( "{clearancePageUrl}" ) ); //no product on the category 
		lr_end_transaction ( "T02_Clearance Display" , LR_FAIL ) ;
	} // end if
*/	else
		lr_end_transaction ( "T02_Clearance Display" , LR_PASS ) ;


} // end navByClearance

findNewArrivals() // used in navByClearance()
{
	int offset = 1;

    char * position;

    char * str = "";

    char * search_str = "Arrivals"; //try not to pick New Arrival Clearance Products

    while( offset >= 1 ) {

		str = lr_paramarr_random( "clearances" );
		//lr_message("str is %s:", str); 
	    position = (char *)strstr(str, search_str);

	    offset = (int)(position - str + 1);
    }

    lr_save_string(str, "clearance");

	return 0;
}

void typeAheadSearch()
{
	//lr_message ( "In typeAheadSearch" ) ;
    for (i = 0; i < length; i++)
    {
       	//lr_message("source: %c", searchString[i]);
       	if (i == 0)
           	lr_param_sprintf ("SEARCH_STRING_PARAM", "%c", searchString[i]);
       	else
           	lr_param_sprintf ("SEARCH_STRING_PARAM", "%s%c", lr_eval_string("{SEARCH_STRING_PARAM}"), searchString[i]);
           	//lr_message("Partial SEARCH_STRING_PARAM: %s", lr_eval_string("{SEARCH_STRING_PARAM}"));

		if (i == 1) lr_save_string(lr_eval_string("{SEARCH_STRING_PARAM}"),"forDidYouMean");
			
//     	type ahead search call after every 3rd characters
		if (i == 2 || i == 5 || i == 8 || i == 11 || i == 14 || i == 17 || i == 20 )
       	{
    		lr_think_time ( TYPE_SPEED );

			web_reg_save_param ( "autoSelectOption" , "LB=title=\"" ,"RB=\" id='autoSelectOption_" , "Notfound=warning" , "ORD=All", LAST ) ;
			
		
			lr_start_transaction ( "T02_Search_ajaxAutoSuggestion" ) ;

			web_submit_data("TCPAJAXAutoSuggestView",
				"Action=http://{host}/webapp/wcs/stores/servlet/{searchAutoSuggestURL}&term={SEARCH_STRING_PARAM}&showHeader=true&count=4",
				"Method=POST",
				"RecContentType=text/html",
				"Snapshot=t45.inf",
				"Mode=HTML",
				ITEMDATA,
				"Name=objectId", "Value=", ENDITEM,
				"Name=requesttype", "Value=ajax", ENDITEM,
				LAST);

			lr_end_transaction ( "T02_Search_ajaxAutoSuggestion" , LR_PASS ) ;

			//if there is a suggestion, capture it, exit the loop, use the suggestion for submitCompleteSearch
			if (atoi(lr_eval_string("{autoSelectOption_count}")) > 0) {
				searchString = lr_eval_string("{autoSelectOption_1}");
				break;
			} 
			
       	} // end iF
   	 } // end for
} // end typeAheadSearch()


void submitCompleteSearch() // 	Search for complete string should be placed
{
	//lr_message ( "In submitCompleteSearch" ) ;
 	lr_think_time (LINK_TT);
	categoryPageCorrelations();
	subCategoryPageCorrelations();
	productDisplayPageCorrelations();
	
//	lr_param_sprintf ("SEARCH_STRING", "%s", searchString); 
	lr_param_sprintf ("SEARCH_STRING", "%s", lr_eval_string("{originalSearchString}"));  //
	
//	web_convert_param("SEARCH_STRING", "SourceEncoding=PLAIN", "TargetEncoding=URL", LAST);
	web_reg_find("Text= 0 matches", "SaveCount=searchResults_Count", LAST );
	
// 	web_reg_save_param ( "searchCount" , "LB=<span class=\"search-count\">\r\n														", "RB=\r\n													</span>" , "Notfound=warning" , LAST ) ;
/*
    web_reg_find("Text= 0 matches",
        "SaveCount=zeroMatch_Count",
        LAST );	
*/		
	lr_start_transaction ( "T02_Search" );
//https://tcp-perf.childrensplace.com/webapp/wcs/stores/servlet/en/usstore/content/placeshop-pajama-2014
	web_url("submitSearch",
		"URL=https://{host}/shop/SearchDisplay?storeId={storeId}&catalogId={catalogId}&langId=-1&pageSize=100&beginIndex=0&searchSource=Q&sType=SimpleSearch&resultCatEntryType=2&showResultsPage=true&pageView=image&custSrch=search&searchTerm={SEARCH_STRING}&TCPSearchSubmit=", 
		"Resource=0",
		"RecContentType=text/html",
		"Snapshot=t59.inf",
		"Mode=HTML",
		LAST);
		
	if ( isLoggedIn == 1 ) {
		TCPGetWishListForUsersView();
	}
	
	if (atoi(lr_eval_string("{searchResults_Count}")) > 0) { //} && strlen(lr_eval_string("{searchResults}")) != 15 ) {
		lr_end_transaction ( "T02_Search" , LR_FAIL ) ; 
	} 
	else {
		lr_end_transaction ( "T02_Search" , LR_PASS ) ;
	}
	
	return;
		
} // end submitCompleteSearch
		
		
void submitCompleteSearchDidYouMean() // 	Search with "Did You Mean"
{
	//lr_message ( "In submitCompleteSearch" ) ;
 	lr_think_time (LINK_TT);
	categoryPageCorrelations();
	subCategoryPageCorrelations();
	productDisplayPageCorrelations();
	
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_SEARCH_SUGGEST )
		lr_param_sprintf ("SEARCH_STRING", "%s", lr_eval_string("{forDidYouMean}"));
	else
		lr_param_sprintf ("SEARCH_STRING", "%s", searchString);
	
	web_reg_find("Text= 0 matches", "SaveCount=searchResults_Count", LAST );
	web_reg_save_param("didYouMeanId", "LB=http://{host}/shop/SearchDisplay?searchTermScope=&searchType=1002&filterTerm=&orderBy=&maxPrice=&showResultsPage=true&langId=-1&departmentId=&sType=", 
	"RB=\" class=\"result\">", "ORD=All", "NotFound=Warning", LAST);


	lr_start_transaction ( "T02_Search" ) ;

	// AND-258 - Search | Did You Mean? 
	web_url("submitSearch", 
		"URL=https://{host}/shop/SearchDisplay?storeId={storeId}&catalogId={catalogId}&langId=-1&pageSize=100&beginIndex=0&searchSource=Q&sType=SimpleSearch&resultCatEntryType=2&showResultsPage=true&pageView=image&custSrch=search&searchTerm={SEARCH_STRING}&TCPSearchSubmit=", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Mode=HTML", 
		LAST);
		
	lr_end_transaction ( "T02_Search" , LR_PASS ) ;

	if ( atoi(lr_eval_string("{didYouMeanId_count}")) > 0 ) { //&& strlen(lr_eval_string("{didYouMean}")) != 12 ) {
		
		lr_think_time (LINK_TT);
		categoryPageCorrelations();
		subCategoryPageCorrelations();
		productDisplayPageCorrelations();

		lr_save_string( lr_paramarr_random("didYouMeanId"), "searchStringId");

		web_reg_find("Text= 0 matches", "SaveCount=searchResults_Count", LAST );
		
		lr_start_transaction("T02_Search_DidYouMean");

		web_url("SelectDidYouMeanSuggestion", 
			"URL=http://{host}/shop/SearchDisplay?searchTermScope=&searchType=1002&filterTerm=&orderBy=&maxPrice=&showResultsPage=true&langId=-1&departmentId=&sType={searchStringId}", 
			"TargetFrame=", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Mode=HTML", 
			LAST);

		if (atoi(lr_eval_string("{searchResults_Count}")) == 0 )  //&& strlen(lr_eval_string("{searchResults}")) != 15 )
			lr_end_transaction ( "T02_Search_DidYouMean" , LR_FAIL ) ; 
		else
			lr_end_transaction ( "T02_Search_DidYouMean" , LR_PASS ) ;
	}
	
} // end submitCompleteSearch
		
writeToFile1() 
{
	char *ip;
    char fullpath[1024], * filename1 = "\\e$\\Performance\\Scripts\\2016_R1\\03_PERF\\Datafiles\\TestData\\Roshni01132017.dat";
	long file1;
//	char * filename1 = "\\\\10.56.29.36\\e$\\Performance\\Scripts\\2016_R1\\03_PERF\\Datafiles\\TestData\\searchStrNoResult.dat";
    strcpy(fullpath, "\\\\" );
	ip = lr_get_host_name( );
    strcat(fullpath, ip);
	strcat(fullpath, filename1);
       
    if ((file1 = fopen(fullpath, "a+")) == NULL) {
        lr_output_message ("Unable to open %s", fullpath);
        return -1;
    }
	
	fprintf(file1, "%s\n", lr_eval_string("{SEARCH_STRING}"));
	
    fclose(file1);
	return 0;
}

writeToFile2() 
{
	long file;
	char *ip;
    char fullpath[1024], * filename2 = "\\e$\\Performance\\Scripts\\2016_R1\\03_PERF\\Datafiles\\TestData\\searchStrWithResult.dat";
    strcpy(fullpath, "\\\\" );
	ip = lr_get_host_name( );
    strcat(fullpath,  ip);
	strcat(fullpath, filename2);
//	lr_output_message ("fullpath after strcpy: %s", fullpath);
       
    if ((file = fopen(fullpath, "a+")) == NULL) {
        lr_output_message ("Unable to open %s", fullpath);
        return -1;
    }

	fprintf(file, "%s\n", lr_eval_string("{SEARCH_STRING}"));
	
    fclose(file);
	return 0;
		
}

void navBySearch()
{
	//lr_message ( "In navBySearch" ) ;

	int searchSuggest = atoi(lr_eval_string("{RANDOM_PERCENT}"));

	lr_think_time ( TYPE_SPEED );

	lr_start_transaction ( "T02_Search_ajaxCachedSuggestionView" ) ;

	web_submit_data("TCPAJAXCachedSuggestionsView",
		"Action=http://{host}/webapp/wcs/stores/servlet/{searchCachedSuggestionsURL}",
		"Method=POST",
		"RecContentType=text/html",
		"Snapshot=t44.inf",
		"Mode=HTML",
		ITEMDATA,
		"Name=objectId", "Value=", ENDITEM,
		"Name=requesttype", "Value=ajax", ENDITEM,
		LAST);

	lr_end_transaction ( "T02_Search_ajaxCachedSuggestionView" , LR_PASS ) ;

	searchString = lr_eval_string("{searchStr}"); //this is the search string from the data file
	lr_save_string(searchString,"originalSearchString");
	
    length = (int)strlen(searchString);

    if (length >= 3) //type ahead only if search term is greater than 3 characters
    	typeAheadSearch();
	
	if ( searchSuggest <= RATIO_SEARCH_SUGGEST )
		submitCompleteSearchDidYouMean();
	else
		submitCompleteSearch();

} //end navBySearch


void navByPlace()
{
	//lr_message ( "In navByPlace" ) ;
	if ( atoi( lr_eval_string( "{placeShopCategory_count}")) != 0 ) {
		
		lr_save_string( lr_paramarr_random("placeShopCategory"), "placeShop");
		
		if ( strcmp( lr_eval_string("{placeShop}") ,"placeShops-pjplace-2014" ) == 0) 
			lr_save_string( lr_paramarr_random("placeShopCategory"), "placeShop");
		
		lr_start_transaction ( "T02_PlaceShop" ) ;

		web_url("placeShop",
			"URL=http://{host}{store_home_page}content/{placeShop}",
			"Resource=0",
			"RecContentType=text/html",
			LAST);
		
		lr_end_transaction ( "T02_PlaceShop" , LR_PASS ) ;
	}
/*	
 <a href="http://tcp-perf.childrensplace.com/shop/us/content/mvp-shop-spring-2014" 

   	web_reg_save_param ( "placeShopCategory" ,
	                     "LB=/shop/us/content/" ,
	                     "RB=\"",
						 "ORD=All",
	                     "NotFound=Warning", LAST ) ;
	*/
} //end navByPlace


// T02_Category / Clearance / Search / PlaceShop Display
// select navigation method from NAV_BY parameter, "stop" will end iteration. This is a mandatory step to continue further and cannot be passed
void topNav()
{
//	nav_by = lr_eval_string("{RATIO_NAV_BY}") ;

	randomPercent = atoi(lr_eval_string("{RANDOM_PERCENT}"));

	if ( randomPercent < NAV_BROWSE ){
		//lr_message ( "callingNavByBrowse" ) ;
		navByBrowse();
		} // end if
	else if ( randomPercent < (NAV_BROWSE + NAV_CLEARANCE) ) {
		//lr_message ( "callingNavByClearance" ) ;
		navByClearance();
		} // end if
	else if ( randomPercent < (NAV_BROWSE + NAV_CLEARANCE + NAV_SEARCH)) {
		//lr_message ( "callingNavBySearch" ) ;
		navBySearch();
		} // end else-if
	else if (randomPercent < (NAV_BROWSE + NAV_CLEARANCE + NAV_SEARCH + NAV_PLACE)) {
		//lr_message ( "callingNavByPlace" ) ;
		navByPlace();
		} // end else-if

} // end topNav


void paginate()
{
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) < APPLY_PAGINATE ) {
		
		if (( lr_paramarr_len ( "paginationNextURL" ) > 0 ) ) {
			
			lr_think_time ( LINK_TT ) ;
			
		//	categoryPageCorrelations();
			subCategoryPageCorrelations();
			productDisplayPageCorrelations();
			index = rand ( ) % lr_paramarr_len( "paginationNextURL" ) + 1 ;
			lr_param_sprintf ("paginationURL", "http://%s%s&beginIndex=%s", lr_eval_string("{host}"), lr_paramarr_idx ( "paginationNextURL" , index ), lr_paramarr_idx ( "paginationPageIndex" , index ));
			lr_param_sprintf ("paginationIndex", "paginationPageIndex_%d", index);

			lr_start_transaction ( "T03_Paginate" ) ;
			
				web_submit_data("AjaxCatalogSearchResultView",
					"Action={paginationURL}",
					"Method=POST",
					"RecContentType=text/html",
					"Mode=HTML",
					ITEMDATA,
					"Name=searchResultsPageNum", "Value=-{paginationIndex}", ENDITEM,
					"Name=searchResultsView", "Value=", ENDITEM,
					"Name=searchResultsURL", "Value={paginationURL}", ENDITEM,
					"Name=objectId", "Value=", ENDITEM,
					"Name=requesttype", "Value=ajax", ENDITEM,
					LAST);
					
			lr_end_transaction ( "T03_Paginate" , LR_PASS ) ;
		}
	}
	
} // end pagination


void sortResults()
{
	//lr_message ( "in sortResults" ) ;
 	lr_think_time ( LINK_TT ) ;
	if (( lr_paramarr_len ( "sortURL" ) > 0 )) {
		categoryPageCorrelations();
		subCategoryPageCorrelations();
		productDisplayPageCorrelations();
	//	index = rand ( ) % lr_paramarr_len( "sortURL" ) + 1 ;
	//	lr_param_sprintf ( "drillUrl" , "http://%s%s" , lr_eval_string("{host}") , lr_paramarr_idx ( "sortURL" , index ) ) ;
		lr_param_sprintf ( "drillUrl" , "http://%s%s" , lr_eval_string("{host}") , lr_paramarr_random ( "sortURL" ) ) ;

		lr_start_transaction ( "T03_Sort Results" ) ;
			webURL(lr_eval_string ( "{drillUrl}" ), "T03_Sort Results" );
		lr_end_transaction ( "T03_Sort Results" , LR_PASS ) ;
	} // end if
} // end sortResults

void sort()
{
// T03_Sort Results
// Optional step - Apply Sort. This step will be skipped if the return value is "pass",
// sort_by = lr_eval_string("{SORT_BY}") ;

//	randomPercent = atoi(lr_eval_string("{RANDOM_PERCENT}"));

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) < APPLY_SORT ){
		//lr_message ( "Applying Sort" ) ;
		sortResults();
		} // end if
	else {
		//lr_message ( "Skipping Sort" ) ;
		return;
		} // end if

} // end sort

void drillOneFacet()
{
	//lr_message ( "in drillOneFacet" ) ;
 	lr_think_time ( LINK_TT ) ;

	if (( lr_paramarr_len ( "facetsID" ) > 0 ) ) {
		categoryPageCorrelations();
		subCategoryPageCorrelations();
		productDisplayPageCorrelations();
		index = rand ( ) % lr_paramarr_len( "facetsID" ) + 1 ;
		lr_param_sprintf ( "drillUrlOneFacet" , "http://%s%s&facet=%s" , lr_eval_string("{host}") , lr_paramarr_idx ( "facetsURL" , index ) , lr_paramarr_idx ( "facetsID" , index ) ) ;
//		lr_param_sprintf ( "drillUrlOneFacet" , "http://%s/shop/SearchDisplay?%s&facet=%s" , lr_eval_string("{host}") , lr_paramarr_idx ( "facetsURL" , index ) , lr_paramarr_idx ( "facetsID" , index ) ) ;
//		lr_param_sprintf ( "drillUrlOneFacet" , "http://%s/shop/SearchDisplay?%s&facet=%s" , lr_eval_string("{host}") , lr_paramarr_random ( "facetsURL" ) , lr_paramarr_random ( "facetsID" ) ) ;

		lr_start_transaction ( "T03_Facet Display_1facet" ) ;

			webURL(lr_eval_string ( "{drillUrlOneFacet}" ), "T03_Facet Display_1facet" );

			if ( isLoggedIn == 1 ) {
				//TCPGetWishListForUsersView(); //1206 12am 
				//wlGetAll();
			}

		lr_end_transaction ( "T03_Facet Display_1facet" , LR_PASS ) ;

	} // end if
} // end drillOneFacet


void drillTwoFacets()
{
	//lr_message ( "in drillTwoFacets" ) ;
	drillOneFacet();
 	lr_think_time ( LINK_TT ) ;
	if (( lr_paramarr_len ( "facetsID" ) > 0 ) ) {
		categoryPageCorrelations();
		subCategoryPageCorrelations();
		productDisplayPageCorrelations();
//		index = rand ( ) % lr_paramarr_len( "facetsID" ) + 1 ;
//		lr_param_sprintf ( "drillUrlTwoFacets" , "%s&facet=%s" , lr_eval_string("{drillUrlOneFacet}") , lr_paramarr_idx ( "facetsID" , index ) ) ;
		lr_param_sprintf ( "drillUrlTwoFacets" , "%s&facet=%s" , lr_eval_string("{drillUrlOneFacet}") , lr_paramarr_random ( "facetsID"  ) ) ;
		
		lr_start_transaction ( "T03_Facet Display_2facets" ) ;

			webURL(lr_eval_string ( "{drillUrlTwoFacets}" ), "T03_Facet Display_2facets" );

			if ( isLoggedIn == 1 ) {
				TCPGetWishListForUsersView();
				//wlGetAll();
			}

		lr_end_transaction ( "T03_Facet Display_2facets" , LR_PASS ) ;

	} // end if
} // end drillTowFacets


void drillThreeFacets()
{
	//lr_message ( "in drillThreeFacets" ) ;
	drillTwoFacets();
 	lr_think_time ( LINK_TT ) ;
	if (( lr_paramarr_len ( "facetsID" ) > 0 ) ) {
		categoryPageCorrelations();
		subCategoryPageCorrelations();
		productDisplayPageCorrelations();
//		index = rand ( ) % lr_paramarr_len( "facetsID" ) + 1 ;
//		lr_param_sprintf ( "drillUrlThreeFacets" , "%s&facet=%s" , lr_eval_string("{drillUrlTwoFacets}") , lr_paramarr_idx ( "facetsID" , index ) ) ;
		lr_param_sprintf ( "drillUrlThreeFacets" , "%s&facet=%s" , lr_eval_string("{drillUrlTwoFacets}") , lr_paramarr_random ( "facetsID" ) ) ;
		
		lr_start_transaction ( "T03_Facet Display_3facets" ) ;

			webURL(lr_eval_string ( "{drillUrlThreeFacets}" ), "T03_Facet Display_3facets" );

			if ( isLoggedIn == 1 ) {
				TCPGetWishListForUsersView();
				//wlGetAll();
			}

		lr_end_transaction ( "T03_Facet Display_3facets" , LR_PASS ) ;

	}// end if
}// end drillThreeFacets

void getSubCategory()
{
    extern char * strtok(char * string, const char * delimiters ); 
    char path[1000] = "";
    char separators[] = "&"; 
    char * token;
    char fullpath[1024];
    int counter = 0;
	
	while (counter==0)
	{	
		lr_save_string(lr_paramarr_random ( "subcategories" ), "subCategory");
		strcpy(path, lr_eval_string ( "{subCategory}" ));
    // Get the first token
		token = (char *)strtok(path, separators); 
		lr_output_message ("Token is %s",  token );
		if (token != NULL) {
			lr_output_message ("subCategory is outfit %s",  lr_eval_string ( "{subCategory}" ) );
		}
		else {
			lr_output_message ("subCategory will use is %s",  lr_eval_string ( "{subCategory}" ) );
			break;
		}
	}
/*	
// While valid tokens are returned
    while (token != NULL ) { 
//        lr_output_message ("%s", token );
// Get the next token
        token = (char *)strtok(NULL, separators); 
        
        if(token != NULL) {
	        if (strlen(token) > 10) {
	        	counter++;
	        	lr_param_sprintf ("count", "%d", counter);
	        	lr_save_string(token, lr_eval_string("upcId{count}") );
	        }
        }
    }
*/	
	
}

void drillSubCategory()
{
	//lr_message ( "in drillSubCategory" ) ;
 	lr_think_time ( LINK_TT ) ;
	categoryPageCorrelations();
	subCategoryPageCorrelations();
	productDisplayPageCorrelations();

	if (( lr_paramarr_len ( "subcategories" ) > 0 )) {
//		getSubCategory();
//		lr_param_sprintf ( "drillUrl" , "http://%s%s" , lr_eval_string("{host}") , lr_eval_string ( "{subCategory}" ) ) ;
		lr_param_sprintf ( "drillUrl" , "http://%s%s" , lr_eval_string("{host}") , lr_paramarr_random ( "subcategories" ) ) ;
		lr_continue_on_error(1);
		
		lr_start_transaction ( "T03_Sub-Category Display" ) ;

//			webURL(lr_eval_string ( "{drillUrl}" ), "T03_Sub-Category Display" );
		if ( webURL2(lr_eval_string ( "{drillUrl}" ), "T03_Sub-Category Display", "FILTER BY" ) == LR_PASS) {

			if ( isLoggedIn == 1 ) {
				TCPGetWishListForUsersView();
				//wlGetAll();
			}

			if ( atoi(lr_eval_string("{noProducts}")) == 1 ) {
				lr_fail_trans_with_error( lr_eval_string("T03_Sub-Category Display Text Check Failed - {drillUrl}") ) ;
				lr_end_transaction ( "T03_Sub-Category Display" , LR_FAIL ) ;
			}
			else
				lr_end_transaction ( "T03_Sub-Category Display" , LR_PASS ) ;
		}
		else {
				lr_fail_trans_with_error( lr_eval_string("T03_Sub-Category Display Text Check Failed - {drillUrl}") ) ;
				lr_end_transaction ( "T03_Sub-Category Display" , LR_FAIL ) ;
		}
		
		lr_continue_on_error(0);

	} // end if
	else if ((lr_paramarr_len( "searchSubCategories") > 0)) {
		lr_param_sprintf ( "drillUrl" , "http://%s%s" , lr_eval_string("{host}") , lr_paramarr_random ( "searchSubCategories" ) ) ;

		lr_continue_on_error(1);
		
		lr_start_transaction ( "T03_Sub-Category Display" ) ;

//			webURL(lr_eval_string ( "{drillUrl}" ), "T03_Sub-Category Display" );
		if ( webURL2(lr_eval_string ( "{drillUrl}" ), "T03_Sub-Category Display", "FILTER BY" ) == LR_PASS) {

			if ( isLoggedIn == 1 ) {
				TCPGetWishListForUsersView();
				//wlGetAll();
			}

			if ( atoi(lr_eval_string("{noProducts}")) == 1 ) {
				lr_fail_trans_with_error( lr_eval_string("T03_Sub-Category Display Text Check Failed - {drillUrl}") ) ;
				lr_end_transaction ( "T03_Sub-Category Display" , LR_FAIL ) ;
			}
			else
				lr_end_transaction ( "T03_Sub-Category Display" , LR_PASS ) ;
		}
		else {
				lr_fail_trans_with_error( lr_eval_string("T03_Sub-Category Display Text Check Failed - {drillUrl}") ) ;
				lr_end_transaction ( "T03_Sub-Category Display" , LR_FAIL ) ;
		}

	} // end else-if
	
	lr_continue_on_error(0);
	
}// end drillSubCategory


void drill()
{
// T03_Drill Facets or SubCategory
// Apply drill method from DRILL_BY parameter after performing some validations, "stop" will end iteration, "pass" will continue to PDP / Quick View step
	if (( lr_paramarr_len( "subcategories" ) == 0 ) && (lr_paramarr_len( "searchSubCategories") == 0) && ( lr_paramarr_len ( "facetsID" ) == 0 ) ) {
		//lr_message ( "no subcategories, no facet, break" ) ;
//		lr_exit(LR_EXIT_ITERATION_AND_CONTINUE, LR_PASS);
		return;
	} // end if
	else if ( lr_paramarr_len ( "subcategories" ) == 0 && (lr_paramarr_len( "searchSubCategories") == 0)) {
		drill_by = "ONE_FACET" ;
		//lr_message ( "no subcategories" ) ;
		} // end else-if
	else if ( lr_paramarr_len ( "facetsID" ) == 0 ) {
		drill_by = "SUB_CATEGORY" ;
		//lr_message ( "no facets" ) ;
		} // end else-if
	else {
		//lr_message ( "Select drill method from DRILL_BY parameter" ) ;
		//drill_by = lr_eval_string("{DRILL_BY}") ;

		randomPercent = atoi(lr_eval_string("{RANDOM_PERCENT}"));

		if ( randomPercent <= DRILL_SUB_CATEGORY ){
			drill_by = "SUB_CATEGORY" ;
			} // end if
		else if ( randomPercent <= (DRILL_SUB_CATEGORY + DRILL_TWO_FACET) ) {
			drill_by = "TWO_FACET" ;
			} // end else-if
		else if ( randomPercent <= (DRILL_SUB_CATEGORY + DRILL_TWO_FACET + DRILL_THREE_FACET)) {
			drill_by = "THREE_FACET" ;
			} // end else-if
		else if (randomPercent <= (DRILL_SUB_CATEGORY + DRILL_TWO_FACET + DRILL_THREE_FACET + DRILL_ONE_FACET)) {
			drill_by = "ONE_FACET" ;
			} // end else-if

	} // end else-if - T03 validations complete - drill_by mode selected

	if (strcmp(drill_by, "ONE_FACET") == 0) {
		//lr_message ( "applying DRILL_ONE_FACET" ) ;
		drillOneFacet();
		} // end if
	else if (strcmp(drill_by, "TWO_FACET") == 0) {
		//lr_message ( "applying DRILL_TWO_FACET" ) ;
		drillTwoFacets();
		} // end else-if
	else if (strcmp(drill_by, "THREE_FACET") == 0) {
		//lr_message ( "applying DRILL_THREE_FACET" ) ;
		drillThreeFacets();
		} // end else-if
	else if (strcmp(drill_by, "SUB_CATEGORY") == 0) {
		//lr_message ( "applying DRILL_SUB_CATEGORY" ) ;
		drillSubCategory();
		} // end else-if
	else if (strcmp(drill_by, "stop") == 0) {
		//lr_message ( "stop iteration @ T03_subcategory / facet transaction" ) ;
//		lr_exit(LR_EXIT_ITERATION_AND_CONTINUE, LR_PASS);
		} // end else-if
	else if (strcmp(drill_by, "pass") == 0){
//		lr_exit(LR_EXIT_ITERATION_AND_CONTINUE, LR_PASS);
		//lr_message ( "skip subcateogry & facet step and continue to PDP / QuickView step" ) ;
	} // end else-if - end of T03_Drill Facet or SubCategory transaction
} // end drill


void productDetailView()
{
	//lr_message ( "in productDisplay" ) ;
 	lr_think_time ( LINK_TT ) ;
	
	if (( lr_paramarr_len ( "pdpURL" ) > 0 )) {
		
		TCPIShippingView();
		
		productDisplayPageCorrelations();
		lr_param_sprintf ( "drillUrl" , "http://%s%s" , lr_eval_string("{host}") , lr_paramarr_random ( "pdpURL" ) ) ;
		lr_continue_on_error(1);
		
		lr_start_transaction ( "T04_Product Display Page" ) ;
		
//		webURL(lr_eval_string ( "{drillUrl}" ), "T04_Product Display Page" );
		if (webURL2(lr_eval_string ( "{drillUrl}" ), "T04_Product Display Page", "Mouse over image to zoom in") == LR_PASS) {
	
	//		if ( strlen(lr_eval_string("{skuSelectionProductID}")) == 23 ) { //means there is has no value
			
				addToCartCorrelations();
				
	//			if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) < RATIO_TCPSkuSelectionView ) {

					lr_start_sub_transaction ("T04_Product Display Page - TCPSkuSelectionView", "T04_Product Display Page" );

					web_custom_request("TCPSKUSelectionView",
						"URL=http://{host}/webapp/wcs/stores/servlet/TCPSKUSelectionView?langId=-1&storeId={storeId}&catalogId={catalogId}&LimQty=undefined&TCPWebOnlyFlag=&productId={skuSelectionProductID}",
						"Method=GET",
						"Resource=0",
						"RecContentType=text/html",
						"Snapshot=t18.inf",
						"Mode=HTML",
						"EncType=application/x-www-form-urlencoded",
						LAST);
							
					lr_end_sub_transaction ( "T04_Product Display Page - TCPSkuSelectionView", LR_PASS );
	//			}

				if ( isLoggedIn == 1 ) {
					TCPGetWishListForUsersView();
					//wlGetAll();
				}
	//		}
			lr_save_string("PDP","PDP_or_PQV");
			lr_end_transaction ( "T04_Product Display Page" , LR_PASS ) ;
		
			if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) < PRODUCT_QUICKVIEW_SERVICE )
				productQuickviewService();
		}
		else {
			lr_fail_trans_with_error( lr_eval_string("T04_Product Display Text Check Failed - {drillUrl}") ) ;
			lr_end_transaction ( "T04_Product Display Page" , LR_FAIL ) ;
		}
		lr_continue_on_error(0);

	} // end else-if
	
} // end productDisplay


void productQuickView()
{
	//lr_message ( "in productQuickView" ) ;
 	lr_think_time ( LINK_TT ) ;
	if (( lr_paramarr_len ( "quickviewURL" ) > 0 )) {
		
		productDisplayPageCorrelations();
//		index = rand ( ) % lr_paramarr_len( "quickviewURL" ) + 1 ;
//		lr_param_sprintf ( "drillUrl" , "http://%s%s" , lr_eval_string("{host}") , lr_paramarr_idx ( "quickviewURL" , index ) ) ;
		lr_param_sprintf ( "drillUrl" , "http://%s%s" , lr_eval_string("{host}") , lr_paramarr_random ( "quickviewURL" ) ) ;
		
		lr_start_transaction ( "T04_Product Quickview Page" ) ;
		
			webURL(lr_eval_string ( "{drillUrl}" ), "T04_Product Quickview Page" );
//		if (webURL2(lr_eval_string ( "{drillUrl}" ), "T04_Product Quickview Page", "VIEW PRODUCT DETAILS" ) == LR_PASS) {
		
		//if ( strlen(lr_eval_string("{skuSelectionProductID}")) == 23 ) { //means this has no value
			
			
//			if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) < RATIO_TCPSkuSelectionView ) {
/*  03/01 THIS DOES NOT EXIST ANYMORE IN UATLIVE3 AND THERE ARE 2 OTHER CALLS BELOW 
				lr_start_sub_transaction ("T04_Product Quickview Page - TCPSkuSelectionView", "T04_Product Quickview Page" );
				addToCartCorrelations();
				web_custom_request("TCPSKUSelectionView",
					"URL=http://{host}/webapp/wcs/stores/servlet/TCPSKUSelectionView?langId=-1&storeId={storeId}&catalogId={catalogId}&LimQty=undefined&TCPWebOnlyFlag=&productId={skuSelectionProductID}",
					"Method=GET",
					"Resource=0",
					"RecContentType=text/html",
					"Snapshot=t18.inf",
					"Mode=HTML",
					"EncType=application/x-www-form-urlencoded",
					LAST);

				lr_end_sub_transaction ( "T04_Product Quickview Page - TCPSkuSelectionView", LR_PASS );
*/
				lr_start_sub_transaction ("T04_Product Quickview Page - GetSwatchesAndSizeInfo", "T04_Product Quickview Page" );

				web_custom_request("GetSwatchesAndSizeInfo",
				//https://uatlive3.childrensplace.com/webapp/wcs/stores/servlet/?storeId=10151&catalogId=10551&langId=-1&productId=800358
					"URL=http://{host}/webapp/wcs/stores/servlet/GetSwatchesAndSizeInfo?langId=-1&storeId={storeId}&catalogId={catalogId}&productId={skuSelectionProductID}",
					"Method=GET",
					"Resource=0",
					"RecContentType=text/html",
					"Snapshot=t18.inf",
					"Mode=HTML",
					"EncType=application/x-www-form-urlencoded",
					LAST);

				lr_end_sub_transaction ( "T04_Product Quickview Page - GetSwatchesAndSizeInfo", LR_PASS );

				addToCartCorrelations();
				lr_start_sub_transaction ("T04_Product Quickview Page - TCPGetSKUDetailsView", "T04_Product Quickview Page" );

				web_custom_request("TCPGetSKUDetailsView",
				//https://uatlive3.childrensplace.com/webapp/wcs/stores/servlet/TCPGetSKUDetailsView?storeId=10151&catalogId=10551&langId=-1&productId=800358
					"URL=http://{host}/webapp/wcs/stores/servlet/TCPGetSKUDetailsView?langId=-1&storeId={storeId}&catalogId={catalogId}&productId={skuSelectionProductID}",
					"Method=GET",
					"Resource=0",
					"RecContentType=text/html",
					"Snapshot=t18.inf",
					"Mode=HTML",
					"EncType=application/x-www-form-urlencoded",
					LAST);

				lr_end_sub_transaction ( "T04_Product Quickview Page - TCPGetSKUDetailsView", LR_PASS );
				
//			}

			//if ( isLoggedIn == 1 ) 
				//TCPGetWishListForUsersView();//1204 5pm to reduce TCPGetWishListForUsersView by 75k
//				wlGetAll();
			lr_save_string("PQV","PDP_or_PQV");

			lr_end_transaction ( "T04_Product Quickview Page" , LR_PASS ) ;
/*		}
		else {
			lr_fail_trans_with_error( lr_eval_string("T04_Product Quickview Text Check Failed - {drillUrl}") ) ;
			lr_end_transaction ( "T04_Product Quickview Page" , LR_FAIL ) ;
		}
*/		
		lr_continue_on_error(0);


	} // end if
} // end ProductQuickView

void productQuickviewService() //Add this call right after a Product Display Page Call
{
		web_reg_find("SaveCount=swatchesAndSizeInfo", "Text/IC=itemTCPBogo",LAST);
		
		lr_continue_on_error(1);

		lr_start_transaction ("T04_Product Quickview Service - GetSwatchesAndSizeInfo" );

			web_custom_request("GetSwatchesAndSizeInfo",
//				"URL=http://int1.childrensplace.com/webapp/wcs/stores/servlet/GetSwatchesAndSizeInfo?storeId=10151&productId=140257",
				"URL=http://{host}/webapp/wcs/stores/servlet/GetSwatchesAndSizeInfo?storeId={storeId}&productId={skuSelectionProductID}",
				"Method=GET",
				"Mode=HTML",
				LAST);

		if(atoi(lr_eval_string("{swatchesAndSizeInfo}")) > 0)
			lr_end_transaction ( "T04_Product Quickview Service - GetSwatchesAndSizeInfo", LR_PASS );	
		else {
			lr_end_transaction ( "T04_Product Quickview Service - GetSwatchesAndSizeInfo", LR_FAIL );	
			lr_error_message("T04_Product Quickview Service - GetSwatchesAndSizeInfo  Failure : %s", lr_eval_string("http://{host}/webapp/wcs/stores/servlet/GetSwatchesAndSizeInfo?storeId={storeId}&productId={skuSelectionProductID}" )); //Pavan Change
			lr_output_message("T04_Product Quickview Service - GetSwatchesAndSizeInfo Failure : %s", lr_eval_string("http://{host}/webapp/wcs/stores/servlet/GetSwatchesAndSizeInfo?storeId={storeId}&productId={skuSelectionProductID}" )); //Pavan Change
			lr_param_sprintf ( "failedPageUrl" , "T03_Sub-Category Display Failure : %s", lr_eval_string("http://{host}/webapp/wcs/stores/servlet/GetSwatchesAndSizeInfo?storeId={storeId}&productId={skuSelectionProductID}") ) ;
			writeFailuresToFile();		
		}
		lr_continue_on_error(0);

		web_reg_find("SaveCount=detailsView", "Text/IC=inventoryFlag",LAST);

		lr_start_transaction ("T04_Product Quickview Service - TCPGetSKUDetailsView" );

			web_custom_request("TCPGetSKUDetailsView",
//				"URL=http://int2.childrensplace.com/webapp/wcs/stores/servlet/TCPGetSKUDetailsView?storeId=10151&catalogId=10551&langId=-1&productId=198946",
				"URL=http://{host}/webapp/wcs/stores/servlet/TCPGetSKUDetailsView?storeId={storeId}&catalogId={catalogId}&langId=-1&productId={skuSelectionProductID}",
				"Method=GET",
				"Mode=HTML",
				LAST);

	
		if(atoi(lr_eval_string("{detailsView}")) > 0)
			lr_end_transaction ( "T04_Product Quickview Service - TCPGetSKUDetailsView", LR_PASS );	
		else
			lr_end_transaction ( "T04_Product Quickview Service - TCPGetSKUDetailsView", LR_FAIL );	

}

void productDisplay()
{
// T04_Product Display or ProductQuickView
// select view method from PRODUCT_BY parameter, "stop" will end iteration. This is a mandatory step to continue further and cannot be passed

	//randomPercent = atoi(lr_eval_string("{RANDOM_PERCENT}"));

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= PDP ){
		//lr_message ( "Attempting ProductDisplay" ) ;
		productDetailView(); //calls the TCPGetWishListForUsersView 2x
		} // end if
	else {
		//lr_message ( "Attempting productQuickView" ) ;
		productQuickView(); //calls the TCPGetWishListForUsersView 1x
	} // end else
//https://tcp-perf.childrensplace.com/webapp/wcs/stores/servlet/TCPGetWishListForUsersView?tcpCallBack=jQuery111308376552302807496_1474915413936&registryAccessPreference=Public&requesttype=ajax&storeId=10151&catalogId=10551&langId=-1&_=1474915413938

} // end ProductDisplay



void switchColor()
{
} // end switchColor

void emailSignUp()
{
	lr_start_transaction ( "T16_Submit Shipping Address" ) ;
	lr_end_transaction ( "T16_Submit Shipping Address" , LR_PASS) ;

} // end emailSignUp


void StoreLocator()
{

 	lr_think_time ( LINK_TT ) ;

	lr_start_transaction("T22_StoreLocator");

	web_custom_request("TCPStoreLocatorGetJsonOnStoresByLatLngView", 
		"URL=https://{host}/shop/TCPStoreLocatorGetJsonOnStoresByLatLngView?storeId={storeId}&catalogId={catalogId}&langId=-1&latitude={storeLocatorLatitude}&longitude={storeLocatorLongitude}&distance={storeLocatorDistance}", 
		"Method=GET", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=https://{host}/shop/AjaxStoreLocatorDisplayView?catalogId={catalogId}&langId=-1&storeId={storeId}", 
		"Snapshot=t6.inf", 
		"Mode=HTML", 
		"EncType=application/x-www-form-urlencoded", 
		LAST);

	lr_end_transaction("T22_StoreLocator", LR_AUTO);
	
	lr_start_transaction("T22_StoreLocator_Find");

	web_submit_data("AjaxStoreLocatorResultsView", 
		"Action=http://{host}/shop/AjaxStoreLocatorResultsView?catalogId={catalogId}&langId=-1&orderId=&storeId={storeId}", 
		"Method=POST", 
		"TargetFrame=", 
		"RecContentType=text/html", 
		"Referer=http://{host}/shop/AjaxStoreLocatorDisplayView?catalogId={catalogId}&langId=-1&storeId={storeId}", 
		"Snapshot=t326.inf", 
		"Mode=HTML", 
		ITEMDATA, 
		"Name=distance", "Value={storeLocatorDistance}", ENDITEM, 
		"Name=latitude", "Value={storeLocatorLatitude}", ENDITEM, 
		"Name=longitude", "Value={storeLocatorLongitude}", ENDITEM, 
		"Name=displayStoreInfo", "Value=false", ENDITEM, 
		"Name=fromPage", "Value=StoreLocator", ENDITEM, 
		"Name=objectId", "Value=", ENDITEM, 
		"Name=requesttype", "Value=ajax", ENDITEM, 
		LAST);

	lr_end_transaction("T22_StoreLocator_Find", LR_AUTO);

}

void TCPIShippingView()
{
	lr_think_time ( LINK_TT ) ;
	
	//http://tcp-perf.childrensplace.com/shop/TCPIShippingView?catalogId={catalogId}&langId=-1&storeId={storeId}
	
	lr_start_transaction("T27_TCPIShippingView");
	
	web_custom_request("TCPIShippingView",
		"URL=https://{host}/shop/TCPIShippingView?langId=-1&storeId={storeId}&catalogId={catalogId}",
		"Method=GET",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		"EncType=application/x-www-form-urlencoded",
		LAST);
		
	lr_end_transaction("T27_TCPIShippingView", LR_AUTO);
	//http://tcp-perf.childrensplace.com/webapp/wcs/stores/servlet/TCPGetWishListForUsersView?tcpCallBack=jQuery172028522137515077084_1476374790095&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1
	TCPGetWishListForUsersViewBrowse();
	
}

void TCPGetWishListForUsersViewBrowse()
{
	lr_start_transaction("T25_Common_S01_TCPGetWishListForUsersView");

	web_custom_request("TCPGetWishListForUsersView",
		"URL=http://{host}/webapp/wcs/stores/servlet/TCPGetWishListForUsersView?tcpCallBack=jQuery1113014590106982485151_1473881708524&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&sortBy=&_=1473881708525", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Mode=HTML", 
		"EncType=application/x-www-form-urlencoded", 
		LAST);

	lr_end_sub_transaction("T25_Common_S01_TCPGetWishListForUsersView", LR_AUTO);
	
}


writeFailuresToFile() 
{
	char *ip;
//    char fullpath[1024], * filename1 = "\\e$\\Performance\\0117_Perflive_pageWithNoResult.txt"; // cache prime
//    char fullpath[1024], * filename1 = "\\e$\\Performance\\0118_1515_BT026.txt"; // 
	char fullpath[1024], * filename1 = lr_eval_string("\\e$\\Performance\\{runDate}.txt"); // 
	long file1;
    strcpy(fullpath, "\\\\" );
	ip = lr_get_host_name( );
    strcat(fullpath, ip);
	strcat(fullpath, filename1);
       
    if ((file1 = fopen(fullpath, "a+")) == NULL) {
        lr_output_message ("Unable to open %s", fullpath);
        return -1;
    }
	else {
		//fprintf(file1, "%s\n", lr_eval_string("{failedPageUrl}"));
		fprintf(file1, "%s\n", lr_eval_string("{runTime} | {failedPageUrl}"));	                                                  
	}
    fclose(file1);
    
	return 0;
}


