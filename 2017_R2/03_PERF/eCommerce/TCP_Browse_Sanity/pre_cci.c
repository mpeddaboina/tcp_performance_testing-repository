# 1 "e:\\performance\\scripts\\2017\\03_perf\\ecommerce\\tcp_browse_sanity\\\\combined_TCP_Browse_Sanity.c"
# 1 "globals.h" 1



 
 
# 1 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h" 1
 
 












 











# 103 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"








































































	

 



















 
 
 
 
 


 
 
 
 
 
 














int     lr_start_transaction   (char * transaction_name);
int lr_start_sub_transaction          (char * transaction_name, char * trans_parent);
long lr_start_transaction_instance    (char * transaction_name, long parent_handle);
int   lr_start_cross_vuser_transaction		(char * transaction_name, char * trans_id_param); 



int     lr_end_transaction     (char * transaction_name, int status);
int lr_end_sub_transaction            (char * transaction_name, int status);
int lr_end_transaction_instance       (long transaction, int status);
int   lr_end_cross_vuser_transaction	(char * transaction_name, char * trans_id_param, int status);


 
typedef char* lr_uuid_t;
 



lr_uuid_t lr_generate_uuid();

 


int lr_generate_uuid_free(lr_uuid_t uuid);

 



int lr_generate_uuid_on_buf(lr_uuid_t buf);

   
# 266 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"
int lr_start_distributed_transaction  (char * transaction_name, lr_uuid_t correlator, long timeout  );

   







int lr_end_distributed_transaction  (lr_uuid_t correlator, int status);


double lr_stop_transaction            (char * transaction_name);
double lr_stop_transaction_instance   (long parent_handle);


void lr_resume_transaction           (char * trans_name);
void lr_resume_transaction_instance  (long trans_handle);


int lr_update_transaction            (const char *trans_name);


 
void lr_wasted_time(long time);


 
int lr_set_transaction(const char *name, double duration, int status);
 
long lr_set_transaction_instance(const char *name, double duration, int status, long parent_handle);


int   lr_user_data_point                      (char *, double);
long lr_user_data_point_instance                   (char *, double, long);
 



int lr_user_data_point_ex(const char *dp_name, double value, int log_flag);
long lr_user_data_point_instance_ex(const char *dp_name, double value, long parent_handle, int log_flag);


int lr_transaction_add_info      (const char *trans_name, char *info);
int lr_transaction_instance_add_info   (long trans_handle, char *info);
int lr_dpoint_add_info           (const char *dpoint_name, char *info);
int lr_dpoint_instance_add_info        (long dpoint_handle, char *info);


double lr_get_transaction_duration       (char * trans_name);
double lr_get_trans_instance_duration    (long trans_handle);
double lr_get_transaction_think_time     (char * trans_name);
double lr_get_trans_instance_think_time  (long trans_handle);
double lr_get_transaction_wasted_time    (char * trans_name);
double lr_get_trans_instance_wasted_time (long trans_handle);
int    lr_get_transaction_status		 (char * trans_name);
int	   lr_get_trans_instance_status		 (long trans_handle);

 



int lr_set_transaction_status(int status);

 



int lr_set_transaction_status_by_name(int status, const char *trans_name);
int lr_set_transaction_instance_status(int status, long trans_handle);


typedef void* merc_timer_handle_t;
 

merc_timer_handle_t lr_start_timer();
double lr_end_timer(merc_timer_handle_t timer_handle);


 
 
 
 
 
 











 



int   lr_rendezvous  (char * rendezvous_name);
 




int   lr_rendezvous_ex (char * rendezvous_name);



 
 
 
 
 
char *lr_get_vuser_ip (void);
void   lr_whoami (int *vuser_id, char ** sgroup, int *scid);
char *	  lr_get_host_name (void);
char *	  lr_get_master_host_name (void);

 
long     lr_get_attrib_long	(char * attr_name);
char *   lr_get_attrib_string	(char * attr_name);
double   lr_get_attrib_double      (char * attr_name);

char * lr_paramarr_idx(const char * paramArrayName, unsigned int index);
char * lr_paramarr_random(const char * paramArrayName);
int    lr_paramarr_len(const char * paramArrayName);

int	lr_param_unique(const char * paramName);
int lr_param_sprintf(const char * paramName, const char * format, ...);


 
 
static void *ci_this_context = 0;






 








void lr_continue_on_error (int lr_continue);
char *   lr_decrypt (const char *EncodedString);


 
 
 
 
 
 



 







 















void   lr_abort (void);
void lr_exit(int exit_option, int exit_status);
void lr_abort_ex (unsigned long flags);

void   lr_peek_events (void);


 
 
 
 
 


void   lr_think_time (double secs);

 


void lr_force_think_time (double secs);


 
 
 
 
 



















int   lr_msg (char * fmt, ...);
int   lr_debug_message (unsigned int msg_class,
									    char * format,
										...);
# 505 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"
void   lr_new_prefix (int type,
                                 char * filename,
                                 int line);
# 508 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"
int   lr_log_message (char * fmt, ...);
int   lr_message (char * fmt, ...);
int   lr_error_message (char * fmt, ...);
int   lr_output_message (char * fmt, ...);
int   lr_vuser_status_message (char * fmt, ...);
int   lr_error_message_without_fileline (char * fmt, ...);
int   lr_fail_trans_with_error (char * fmt, ...);

 
 
 
 
 
# 532 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"

 
 
 
 
 





int   lr_next_row ( char * table);
int lr_advance_param ( char * param);



														  
														  

														  
														  

													      
 


char *   lr_eval_string (char * str);
int   lr_eval_string_ext (const char *in_str,
                                     unsigned long const in_len,
                                     char ** const out_str,
                                     unsigned long * const out_len,
                                     unsigned long const options,
                                     const char *file,
								     long const line);
# 566 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"
void   lr_eval_string_ext_free (char * * pstr);

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
int lr_param_increment (char * dst_name,
                              char * src_name);
# 589 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"













											  
											  

											  
											  
											  

int	  lr_save_var (char *              param_val,
							  unsigned long const param_val_len,
							  unsigned long const options,
							  char *			  param_name);
# 613 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"
int   lr_save_string (const char * param_val, const char * param_name);



int   lr_set_custom_error_message (const char * param_val, ...);

int   lr_remove_custom_error_message ();


int   lr_free_parameter (const char * param_name);
int   lr_save_int (const int param_val, const char * param_name);
int   lr_save_timestamp (const char * tmstampParam, ...);
int   lr_save_param_regexp (const char *bufferToScan, unsigned int bufSize, ...);

int   lr_convert_double_to_integer (const char *source_param_name, const char * target_param_name);
int   lr_convert_double_to_double (const char *source_param_name, const char *format_string, const char * target_param_name);

 
 
 
 
 
 
# 692 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"
void   lr_save_datetime (const char *format, int offset, const char *name);









 











 
 
 
 
 






 



char * lr_error_context_get_entry (char * key);

 



long   lr_error_context_get_error_id (void);


 
 
 

int lr_table_get_rows_num (char * param_name);

int lr_table_get_cols_num (char * param_name);

char * lr_table_get_cell_by_col_index (char * param_name, int row, int col);

char * lr_table_get_cell_by_col_name (char * param_name, int row, const char* col_name);

int lr_table_get_column_name_by_index (char * param_name, int col, 
											char * * const col_name,
											int * col_name_len);
# 753 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"

int lr_table_get_column_name_by_index_free (char * col_name);

 
 
 
 
# 768 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"
int   lr_zip (const char* param1, const char* param2);
int   lr_unzip (const char* param1, const char* param2);

 
 
 
 
 
 
 
 

 
 
 
 
 
 
int   lr_param_substit (char * file,
                                   int const line,
                                   char * in_str,
                                   int const in_len,
                                   char * * const out_str,
                                   int * const out_len);
# 792 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"
void   lr_param_substit_free (char * * pstr);


 
# 804 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"





char *   lrfnc_eval_string (char * str,
                                      char * file_name,
                                      long const line_num);
# 812 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"


int   lrfnc_save_string ( const char * param_val,
                                     const char * param_name,
                                     const char * file_name,
                                     long const line_num);
# 818 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"

int   lrfnc_free_parameter (const char * param_name );







typedef struct _lr_timestamp_param
{
	int iDigits;
}lr_timestamp_param;

extern const lr_timestamp_param default_timestamp_param;

int   lrfnc_save_timestamp (const char * param_name, const lr_timestamp_param* time_param);

int lr_save_searched_string(char * buffer, long buf_size, unsigned int occurrence,
			    char * search_string, int offset, unsigned int param_val_len, 
			    char * param_name);

 
char *   lr_string (char * str);

 
# 917 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"

int   lr_save_value (char * param_val,
                                unsigned long const param_val_len,
                                unsigned long const options,
                                char * param_name,
                                char * file_name,
                                long const line_num);
# 924 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"


 
 
 
 
 











int   lr_printf (char * fmt, ...);
 
int   lr_set_debug_message (unsigned int msg_class,
                                       unsigned int swtch);
# 946 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"
unsigned int   lr_get_debug_message (void);


 
 
 
 
 

void   lr_double_think_time ( double secs);
void   lr_usleep (long);


 
 
 
 
 
 




int *   lr_localtime (long offset);


int   lr_send_port (long port);


# 1022 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"



struct _lr_declare_identifier{
	char signature[24];
	char value[128];
};

int   lr_pt_abort (void);

void vuser_declaration (void);






# 1051 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"


# 1063 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"
















 
 
 
 
 







int    _lr_declare_transaction   (char * transaction_name);


 
 
 
 
 







int   _lr_declare_rendezvous  (char * rendezvous_name);

 
 
 
 
 


int vtc_connect(char * servername, int portnum, int options);
int vtc_disconnect(int pvci);
int vtc_get_last_error(int pvci);
int vtc_query_column(int pvci, char * columnName, int columnIndex, char * *outvalue);
int vtc_query_row(int pvci, int rowIndex, char * **outcolumns, char * **outvalues);
int vtc_send_message(int pvci, char * column, char * message, unsigned short *outRc);
int vtc_send_if_unique(int pvci, char * column, char * message, unsigned short *outRc);
int vtc_send_row1(int pvci, char * columnNames, char * messages, char * delimiter, unsigned char sendflag, unsigned short *outUpdates);
int vtc_update_message(int pvci, char * column, int index , char * message, unsigned short *outRc);
int vtc_update_message_ifequals(int pvci, char * columnName, int index,	char * message, char * ifmessage, unsigned short 	*outRc);
int vtc_update_row1(int pvci, char * columnNames, int index , char * messages, char * delimiter, unsigned short *outUpdates);
int vtc_retrieve_message(int pvci, char * column, char * *outvalue);
int vtc_retrieve_messages1(int pvci, char * columnNames, char * delimiter, char * **outvalues);
int vtc_retrieve_row(int pvci, char * **outcolumns, char * **outvalues);
int vtc_rotate_message(int pvci, char * column, char * *outvalue, unsigned char sendflag);
int vtc_rotate_messages1(int pvci, char * columnNames, char * delimiter, char * **outvalues, unsigned char sendflag);
int vtc_rotate_row(int pvci, char * **outcolumns, char * **outvalues, unsigned char sendflag);
int vtc_increment(int pvci, char * column, int index , int incrValue, int *outValue);
int vtc_clear_message(int pvci, char * column, int index , unsigned short *outRc);
int vtc_clear_column(int pvci, char * column, unsigned short *outRc);
int vtc_ensure_index(int pvci, char * column, unsigned short *outRc);
int vtc_drop_index(int pvci, char * column, unsigned short *outRc);
int vtc_clear_row(int pvci, int rowIndex, unsigned short *outRc);
int vtc_create_column(int pvci, char * column,unsigned short *outRc);
int vtc_column_size(int pvci, char * column, int *size);
void vtc_free(char * msg);
void vtc_free_list(char * *msglist);

int lrvtc_connect(char * servername, int portnum, int options);
int lrvtc_disconnect();
int lrvtc_query_column(char * columnName, int columnIndex);
int lrvtc_query_row(int columnIndex);
int lrvtc_send_message(char * columnName, char * message);
int lrvtc_send_if_unique(char * columnName, char * message);
int lrvtc_send_row1(char * columnNames, char * messages, char * delimiter, unsigned char sendflag);
int lrvtc_update_message(char * columnName, int index , char * message);
int lrvtc_update_message_ifequals(char * columnName, int index, char * message, char * ifmessage);
int lrvtc_update_row1(char * columnNames, int index , char * messages, char * delimiter);
int lrvtc_retrieve_message(char * columnName);
int lrvtc_retrieve_messages1(char * columnNames, char * delimiter);
int lrvtc_retrieve_row();
int lrvtc_rotate_message(char * columnName, unsigned char sendflag);
int lrvtc_rotate_messages1(char * columnNames, char * delimiter, unsigned char sendflag);
int lrvtc_rotate_row(unsigned char sendflag);
int lrvtc_increment(char * columnName, int index , int incrValue);
int lrvtc_noop();
int lrvtc_clear_message(char * columnName, int index);
int lrvtc_clear_column(char * columnName); 
int lrvtc_ensure_index(char * columnName); 
int lrvtc_drop_index(char * columnName); 
int lrvtc_clear_row(int rowIndex);
int lrvtc_create_column(char * columnName);
int lrvtc_column_size(char * columnName);



 
 
 
 
 

 
int lr_enable_ip_spoofing();
int lr_disable_ip_spoofing();


 




int lr_convert_string_encoding(char * sourceString, char * fromEncoding, char * toEncoding, char * paramName);


 
int lr_db_connect (char * pFirstArg, ...);
int lr_db_disconnect (char * pFirstArg,	...);
int lr_db_executeSQLStatement (char * pFirstArg, ...);
int lr_db_dataset_action(char * pFirstArg, ...);
int lr_checkpoint(char * pFirstArg,	...);
int lr_db_getvalue(char * pFirstArg, ...);







 
 



















# 6 "globals.h" 2

# 1 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/web_api.h" 1







# 1 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/as_web.h" 1
























































 




 








 
 
 

  int
	web_add_filter(
		const char *		mpszArg,
		...
	);									 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_add_auto_filter(
		const char *		mpszArg,
		...
	);									 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
	
  int
	web_add_auto_header(
		const char *		mpszHeader,
		const char *		mpszValue);

  int
	web_add_header(
		const char *		mpszHeader,
		const char *		mpszValue);
  int
	web_add_cookie(
		const char *		mpszCookie);
  int
	web_cleanup_auto_headers(void);
  int
	web_cleanup_cookies(void);
  int
	web_concurrent_end(
		const char * const	mpszReserved,
										 
		...								 
	);
  int
	web_concurrent_start(
		const char * const	mpszConcurrentGroupName,
										 
										 
		...								 
										 
	);
  int
	web_create_html_param(
		const char *		mpszParamName,
		const char *		mpszLeftDelim,
		const char *		mpszRightDelim);
  int
	web_create_html_param_ex(
		const char *		mpszParamName,
		const char *		mpszLeftDelim,
		const char *		mpszRightDelim,
		const char *		mpszNum);
  int
	web_custom_request(
		const char *		mpszReqestName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	spdy_custom_request(
		const char *		mpszReqestName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_disable_keep_alive(void);
  int
	web_enable_keep_alive(void);
  int
	web_find(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_get_int_property(
		const int			miHttpInfoType);
  int
	web_image(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_image_check(
		const char *		mpszName,
		...);
  int
	web_java_check(
		const char *		mpszName,
		...);
  int
	web_link(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

	
  int
	web_global_verification(
		const char *		mpszArg1,
		...);							 
										 
										 
										 
										 
										 
  int
	web_reg_find(
		const char *		mpszArg1,
		...);							 
										 
										 
										 
										 
										 
										 
										 
				
  int
	web_reg_save_param(
		const char *		mpszParamName,
		...);							 
										 
										 
										 
										 
										 
										 

  int
	web_convert_param(
		const char * 		mpszParamName, 
										 
		...);							 
										 
										 


										 

										 
  int
	web_remove_auto_filter(
		const char *		mpszArg,
		...
	);									 
										 
				
  int
	web_remove_auto_header(
		const char *		mpszHeaderName,
		...);							 
										 



  int
	web_remove_cookie(
		const char *		mpszCookie);

  int
	web_save_header(
		const char *		mpszType,	 
		const char *		mpszName);	 
  int
	web_set_certificate(
		const char *		mpszIndex);
  int
	web_set_certificate_ex(
		const char *		mpszArg1,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_set_connections_limit(
		const char *		mpszLimit);
  int
	web_set_max_html_param_len(
		const char *		mpszLen);
  int
	web_set_max_retries(
		const char *		mpszMaxRetries);
  int
	web_set_proxy(
		const char *		mpszProxyHost);
  int
	web_set_pac(
		const char *		mpszPacUrl);
  int
	web_set_proxy_bypass(
		const char *		mpszBypass);
  int
	web_set_secure_proxy(
		const char *		mpszProxyHost);
  int
	web_set_sockets_option(
		const char *		mpszOptionID,
		const char *		mpszOptionValue
	);
  int
	web_set_option(
		const char *		mpszOptionID,
		const char *		mpszOptionValue,
		...								 
	);
  int
	web_set_timeout(
		const char *		mpszWhat,
		const char *		mpszTimeout);
  int
	web_set_user(
		const char *		mpszUserName,
		const char *		mpszPwd,
		const char *		mpszHost);

  int
	web_sjis_to_euc_param(
		const char *		mpszParamName,
										 
		const char *		mpszParamValSjis);
										 

  int
	web_submit_data(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	spdy_submit_data(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_submit_form(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										  
										 
										 
										 
										 
										 
										  
										 
										 
										 
										 
										 
										 
										 
										  
										 
										 
										 
										 
										 
										  
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_url(
		const char *		mpszUrlName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	spdy_url(
		const char *		mpszUrlName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int 
	web_set_proxy_bypass_local(
		const char * mpszNoLocal
		);

  int 
	web_cache_cleanup(void);

  int
	web_create_html_query(
		const char* mpszStartQuery,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int 
	web_create_radio_button_param(
		const char *NameFiled,
		const char *NameAndVal,
		const char *ParamName
		);

  int
	web_convert_from_formatted(
		const char * mpszArg1,
		...);							 
										 
										 
										 
										 
										 
										
  int
	web_convert_to_formatted(
		const char * mpszArg1,
		...);							 
										 
										 
										 
										 
										 

  int
	web_reg_save_param_ex(
		const char * mpszParamName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_reg_save_param_xpath(
		const char * mpszParamName,
		...);							
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_reg_save_param_json(
		const char * mpszParamName,
		...);							
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_reg_save_param_regexp(
		 const char * mpszParamName,
		 ...);							
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_js_run(
		const char * mpszCode,
		...);							
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_js_reset(void);

  int
	web_convert_date_param(
		const char * 		mpszParamName,
		...);










# 737 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/as_web.h"


# 750 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/as_web.h"



























# 788 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/as_web.h"

 
 
 


  int
	FormSubmit(
		const char *		mpszFormName,
		...);
  int
	InitWebVuser(void);
  int
	SetUser(
		const char *		mpszUserName,
		const char *		mpszPwd,
		const char *		mpszHost);
  int
	TerminateWebVuser(void);
  int
	URL(
		const char *		mpszUrlName);
























# 856 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/as_web.h"


  int
	web_rest(
		const char *		mpszReqestName,
		...);							 
										 
										 
										 
										 

 
 
 






# 9 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/web_api.h" 2

















 







 














  int
	web_reg_add_cookie(
		const char *		mpszCookie,
		...);							 
										 

  int
	web_report_data_point(
		const char *		mpszEventType,
		const char *		mpszEventName,
		const char *		mpszDataPointName,
		const char *		mpszLAST);	 
										 
										 
										 

  int
	web_text_link(
		const char *		mpszStepName,
		...);

  int
	web_element(
		const char *		mpszStepName,
		...);

  int
	web_image_link(
		const char *		mpszStepName,
		...);

  int
	web_static_image(
		const char *		mpszStepName,
		...);

  int
	web_image_submit(
		const char *		mpszStepName,
		...);

  int
	web_button(
		const char *		mpszStepName,
		...);

  int
	web_edit_field(
		const char *		mpszStepName,
		...);

  int
	web_radio_group(
		const char *		mpszStepName,
		...);

  int
	web_check_box(
		const char *		mpszStepName,
		...);

  int
	web_list(
		const char *		mpszStepName,
		...);

  int
	web_text_area(
		const char *		mpszStepName,
		...);

  int
	web_map_area(
		const char *		mpszStepName,
		...);

  int
	web_eval_java_script(
		const char *		mpszStepName,
		...);

  int
	web_reg_dialog(
		const char *		mpszArg1,
		...);

  int
	web_reg_cross_step_download(
		const char *		mpszArg1,
		...);

  int
	web_browser(
		const char *		mpszStepName,
		...);

  int
	web_control(
		const char *		mpszStepName,
		...);

  int
	web_set_rts_key(
		const char *		mpszArg1,
		...);

  int
	web_save_param_length(
		const char * 		mpszParamName,
		...);

  int
	web_save_timestamp_param(
		const char * 		mpszParamName,
		...);

  int
	web_load_cache(
		const char *		mpszStepName,
		...);							 
										 

  int
	web_dump_cache(
		const char *		mpszStepName,
		...);							 
										 
										 

  int
	web_reg_find_in_log(
		const char *		mpszArg1,
		...);							 
										 
										 

  int
	web_get_sockets_info(
		const char *		mpszArg1,
		...);							 
										 
										 
										 
										 

  int
	web_add_cookie_ex(
		const char *		mpszArg1,
		...);							 
										 
										 
										 

  int
	web_hook_java_script(
		const char *		mpszArg1,
		...);							 
										 
										 
										 

 
 
 
 
 
 
 
 
 
 
 
 
  int
	web_reg_async_attributes(
		const char *		mpszArg,
		...
	);

 
 
 
 
 
 
  int
	web_sync(
		 const char *		mpszArg1,
		 ...
	);

 
 
 
 
  int
	web_stop_async(
		const char *		mpszArg1,
		...
	);

 
 
 
 
 

 
 
 

typedef enum WEB_ASYNC_CB_RC_ENUM_T
{
	WEB_ASYNC_CB_RC_OK,				 

	WEB_ASYNC_CB_RC_ABORT_ASYNC_NOT_ERROR,
	WEB_ASYNC_CB_RC_ABORT_ASYNC_ERROR,
										 
										 
										 
										 
	WEB_ASYNC_CB_RC_ENUM_COUNT
} WEB_ASYNC_CB_RC_ENUM;

 
 
 

typedef enum WEB_CONVERS_CB_CALL_REASON_ENUM_T
{
	WEB_CONVERS_CB_CALL_REASON_BUFFER_RECEIVED,
	WEB_CONVERS_CB_CALL_REASON_END_OF_TASK,

	WEB_CONVERS_CB_CALL_REASON_ENUM_COUNT
} WEB_CONVERS_CB_CALL_REASON_ENUM;

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

typedef
int														 
	(*RequestCB_t)();

typedef
int														 
	(*ResponseBodyBufferCB_t)(
		  const char *		aLastBufferStr,
		  int				aLastBufferLen,
		  const char *		aAccumulatedStr,
		  int				aAccumulatedLen,
		  int				aHttpStatusCode);

typedef
int														 
	(*ResponseCB_t)(
		  const char *		aResponseHeadersStr,
		  int				aResponseHeadersLen,
		  const char *		aResponseBodyStr,
		  int				aResponseBodyLen,
		  int				aHttpStatusCode);

typedef
int														 
	(*ResponseHeadersCB_t)(
		  int				aHttpStatusCode,
		  const char *		aAccumulatedHeadersStr,
		  int				aAccumulatedHeadersLen);



 
 
 

typedef enum WEB_CONVERS_UTIL_RC_ENUM_T
{
	WEB_CONVERS_UTIL_RC_OK,
	WEB_CONVERS_UTIL_RC_CONVERS_NOT_FOUND,
	WEB_CONVERS_UTIL_RC_TASK_NOT_FOUND,
	WEB_CONVERS_UTIL_RC_INFO_NOT_FOUND,
	WEB_CONVERS_UTIL_RC_INFO_UNAVIALABLE,
	WEB_CONVERS_UTIL_RC_INVALID_ARGUMENT,

	WEB_CONVERS_UTIL_RC_ENUM_COUNT
} WEB_CONVERS_UTIL_RC_ENUM;

 
 
 

  int					 
	web_util_set_request_url(
		  const char *		aUrlStr);

  int					 
	web_util_set_request_body(
		  const char *		aRequestBodyStr);

  int					 
	web_util_set_formatted_request_body(
		  const char *		aRequestBodyStr);


 
 
 
 
 

 
 
 
 
 

 
 
 
 
 
 
 
 

 
 
  int
web_websocket_connect(
		 const char *	mpszArg1,
		 ...
		 );


 
 
 
 
 																						
  int
web_websocket_send(
	   const char *		mpszArg1,
		...
	   );

 
 
 
 
 
 
  int
web_websocket_close(
		const char *	mpszArg1,
		...
		);

 
typedef
void														
(*OnOpen_t)(
			  const char* connectionID,  
			  const char * responseHeader,  
			  int length  
);

typedef
void														
(*OnMessage_t)(
	  const char* connectionID,  
	  int isbinary,  
	  const char * data,  
	  int length  
	);

typedef
void														
(*OnError_t)(
	  const char* connectionID,  
	  const char * message,  
	  int length  
	);

typedef
void														
(*OnClose_t)(
	  const char* connectionID,  
	  int isClosedByClient,  
	  int code,  
	  const char* reason,  
	  int length  
	 );
 
 
 
 
 





# 7 "globals.h" 2

# 1 "lrw_custom_body.h" 1
 




# 8 "globals.h" 2

 
 
int LINK_TT, FORM_TT, TYPE_SPEED;  
	 
	LINK_TT = 10; 
	FORM_TT = 10; 
	TYPE_SPEED = 1;
 
# 62 "globals.h"

# 1 "e:\\performance\\scripts\\2017\\03_perf\\ecommerce\\tcp_browse_sanity\\\\combined_TCP_Browse_Sanity.c" 2


# 1 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/SharedParameter.h" 1



 
 
 
 
# 100 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/SharedParameter.h"





typedef int PVCI2;
typedef int VTCERR2;

 
 
 

 
extern PVCI2    vtc_connect(char *servername, int portnum, int options);
extern VTCERR2  vtc_disconnect(int pvci);
extern VTCERR2  vtc_get_last_error(int pvci);

 
extern VTCERR2  vtc_query_column(int pvci, char *columnName, int columnIndex, char **outvalue);
extern VTCERR2  vtc_query_row(int pvci, int columnIndex, char ***outcolumns, char ***outvalues);
extern VTCERR2  vtc_send_message(int pvci, char *column, char *message, unsigned short *outRc);
extern VTCERR2  vtc_send_if_unique(int pvci, char *column, char *message, unsigned short *outRc);
extern VTCERR2  vtc_send_row1(int pvci, char *columnNames, char *messages, char *delimiter,  unsigned char sendflag, unsigned short *outUpdates);
extern VTCERR2  vtc_update_message(int pvci, char *column, int index , char *message, unsigned short *outRc);
extern VTCERR2  vtc_update_message_ifequals(int pvci, char	*columnName, int index,	char *message, char	*ifmessage,	unsigned short 	*outRc);
extern VTCERR2  vtc_update_row1(int pvci, char *columnNames, int index , char *messages, char *delimiter, unsigned short *outUpdates);
extern VTCERR2  vtc_retrieve_message(int pvci, char *column, char **outvalue);
extern VTCERR2  vtc_retrieve_messages1(int pvci, char *columnNames, char *delimiter, char ***outvalues);
extern VTCERR2  vtc_retrieve_row(int pvci, char ***outcolumns, char ***outvalues);
extern VTCERR2  vtc_rotate_message(int pvci, char *column, char **outvalue, unsigned char sendflag);
extern VTCERR2  vtc_rotate_messages1(int pvci, char *columnNames, char *delimiter, char ***outvalues, unsigned char sendflag);
extern VTCERR2  vtc_rotate_row(int pvci, char ***outcolumns, char ***outvalues, unsigned char sendflag);
extern VTCERR2  vtc_increment(int pvci, char *column, int index , int incrValue, int *outValue);
extern VTCERR2  vtc_clear_message(int pvci, char *column, int index , unsigned short *outRc);
extern VTCERR2  vtc_clear_column(int pvci, char *column, unsigned short *outRc);

extern VTCERR2  vtc_clear_row(int pvci, int rowIndex, unsigned short *outRc);

extern VTCERR2  vtc_create_column(int pvci, char *column,unsigned short *outRc);
extern VTCERR2  vtc_column_size(int pvci, char *column, int *size);
extern VTCERR2  vtc_ensure_index(int pvci, char *column, unsigned short *outRc);
extern VTCERR2  vtc_drop_index(int pvci, char *column, unsigned short *outRc);

extern VTCERR2  vtc_noop(int pvci);

 
extern void vtc_free(char *msg);
extern void vtc_free_list(char **msglist);

 


 




 




















 




 
 
 

extern VTCERR2  lrvtc_connect(char *servername, int portnum, int options);
extern VTCERR2  lrvtc_disconnect();
extern VTCERR2  lrvtc_query_column(char *columnName, int columnIndex);
extern VTCERR2  lrvtc_query_row(int columnIndex);
extern VTCERR2  lrvtc_send_message(char *columnName, char *message);
extern VTCERR2  lrvtc_send_if_unique(char *columnName, char *message);
extern VTCERR2  lrvtc_send_row1(char *columnNames, char *messages, char *delimiter,  unsigned char sendflag);
extern VTCERR2  lrvtc_update_message(char *columnName, int index , char *message);
extern VTCERR2  lrvtc_update_message_ifequals(char *columnName, int index, char 	*message, char *ifmessage);
extern VTCERR2  lrvtc_update_row1(char *columnNames, int index , char *messages, char *delimiter);
extern VTCERR2  lrvtc_retrieve_message(char *columnName);
extern VTCERR2  lrvtc_retrieve_messages1(char *columnNames, char *delimiter);
extern VTCERR2  lrvtc_retrieve_row();
extern VTCERR2  lrvtc_rotate_message(char *columnName, unsigned char sendflag);
extern VTCERR2  lrvtc_rotate_messages1(char *columnNames, char *delimiter, unsigned char sendflag);
extern VTCERR2  lrvtc_rotate_row(unsigned char sendflag);
extern int     lrvtc_increment(char *columnName, int index , int incrValue);
extern VTCERR2  lrvtc_clear_message(char *columnName, int index);
extern VTCERR2  lrvtc_clear_column(char *columnName);
extern VTCERR2  lrvtc_clear_row(int rowIndex);
extern VTCERR2  lrvtc_create_column(char *columnName);
extern int     lrvtc_column_size(char *columnName);
extern VTCERR2  lrvtc_ensure_index(char *columnName);
extern VTCERR2  lrvtc_drop_index(char *columnName);

extern VTCERR2  lrvtc_noop();

 
 
 

                               


 
 
 





















# 3 "e:\\performance\\scripts\\2017\\03_perf\\ecommerce\\tcp_browse_sanity\\\\combined_TCP_Browse_Sanity.c" 2

# 1 "vuser_init.c" 1
# 1 "..\\..\\browseFunctions.c" 1
# 1 "..\\..\\browseWorkloadModel.c" 1
int NAV_BROWSE, NAV_SEARCH, NAV_CLEARANCE, NAV_PLACE;  
int DRILL_ONE_FACET, DRILL_TWO_FACET, DRILL_THREE_FACET, DRILL_SUB_CATEGORY;  
int PDP, QUICKVIEW, PRODUCT_QUICKVIEW_SERVICE, RESERVE_ONLINE;  
int APPLY_SORT, RATIO_TCPSkuSelectionView;  
int APPLY_PAGINATE, RATIO_STORE_LOCATOR, RATIO_SEARCH_SUGGEST;  

	 

	NAV_BROWSE = 45;  
	NAV_SEARCH = 5;  
	NAV_CLEARANCE = 50;  
	NAV_PLACE = 0;  

	 
	DRILL_ONE_FACET = 35;  
	DRILL_TWO_FACET = 10;  
	DRILL_THREE_FACET = 5;  
	DRILL_SUB_CATEGORY = 50;

	 
	APPLY_SORT = 25;

	 
	APPLY_PAGINATE = 15;  

	 
	 
	PDP = 72;  
	QUICKVIEW = 28;  
	
	RATIO_STORE_LOCATOR  = 5;
	RATIO_SEARCH_SUGGEST = 0;  

	PRODUCT_QUICKVIEW_SERVICE = 15;  
	RESERVE_ONLINE = 100;  
	
	RATIO_TCPSkuSelectionView = 20;
# 1 "..\\..\\browseFunctions.c" 2


int index , length , i, randomPercent, isLoggedIn;
char *searchString ;
char *nav_by ;  
char *drill_by ;  
char *sort_by;  
char *product_by;  
 
typedef long time_t;
time_t t;
 
 

void webURL(char *Url, char *pageName)
{
	lr_save_string(Url, "Url");
	lr_save_string(pageName, "pageName");
	web_url ( lr_eval_string("{pageName}") ,
    	     "URL={Url}" ,
             "Resource=0" ,
             "Mode=HTML" ,
             "LAST" ) ;
} 

void endIteration()
{
	 
}

void homePageCorrelations()  
{
	web_reg_save_param ( "clearances" ,
 
						 "LB=<a href=\"http://{host}/shop/SearchDisplay?" ,  
						 "RB=\"" ,
						 "ORD=ALL" , "Convert=HTML_TO_TEXT" , "NotFound=Warning", "LAST" ) ;

	web_reg_save_param ( "categories" ,
						 "LB=<a href=\"http://{host}{store_home_page}search" , 
						 "RB=\"" ,
						 "ORD=ALL" , "Convert=HTML_TO_TEXT" , "NotFound=Warning", "LAST" ) ;

	 

	
 
 
 
 
 
 
 
 
 

 
 
 
 

 
 
 
 
 
 
 
 
 
	
	
	 
 
# 87 "..\\..\\browseFunctions.c"
	web_reg_save_param ( "searchAutoSuggestURL" ,
						 "LB=setAutoSuggestURL('",
						 "RB='" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=Warning", "LAST" ) ;

	web_reg_save_param ( "searchCachedSuggestionsURL" ,
						 "LB=setCachedSuggestionsURL('" ,
						 "RB='" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=Warning", "LAST" ) ;

 

   	web_reg_save_param ( "placeShopCategory" ,
						 "LB={store_home_page}content/" ,
	                     "RB=\"",
						 "ORD=All",
	                     "NotFound=Warning", "LAST" ) ;

	web_set_sockets_option("SSL_VERSION","TLS");  
} 


void categoryPageCorrelations()  
{


	web_reg_save_param ( "subcategories" ,
	                     "LB=class='notSelected' href='http://{host}" ,
	                     "RB='>" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 "LAST" ) ;

	web_reg_save_param ( "facetsURL" ,
				  		 "LB=refreshAfterFilterGotSelected('http://{host}" ,
 
	                   	 "RB='" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 "LAST" ) ;

	web_reg_save_param_regexp( "ParamName=facetsID" ,
	                           "RegExp=facetCheckBox\" id =\"(.*?)\" value=\"(.*?)\"" ,
			                   "Ordinal=ALL" ,
			                   "Notfound=warning" ,
			                   "SEARCH_FILTERS" ,
			                   "Group=2" ,
			                   "LAST" ) ;

	web_reg_save_param ( "sortURL" ,
				  		 "LB=<option value=\"http://{host}" ,
	                   	 "RB=\"" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 "LAST" ) ;

	web_reg_save_param ( "searchSubCategories" ,
				  		 "LB=href='http://{host}" ,
	                   	 "RB='>" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 "LAST" ) ;

	web_reg_save_param ( "paginationNextURL" ,
				  		 "LB=goToResultPage('http://{host}" ,
	                   	 "RB='" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 "LAST" ) ;

	web_reg_save_param ( "paginationPageIndex" ,
				  		 "LB=catalogSearchResultDisplay_Context','" ,
	                   	 "RB='" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 "LAST" ) ;

	web_reg_save_param ( "paginationShowAllURL" ,
				  		 "LB=viewall-right-bottom\">\r\n\t\t\t\t\t                <a href=\"http://{host}" ,
	                   	 "RB=\"" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 "LAST" ) ;
						 
} 


void subCategoryPageCorrelations()  
{
	web_reg_save_param ( "pdpURL" ,
	                     "LB=productRow name\">\r\n\t\t\t\r\n\t\t\t\t<a href=\"http://{host}" ,
	                     "RB=\" id" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 "LAST" ) ;

	web_reg_save_param ( "quickviewURL" ,
	                     "LB=openModal2\" link=\"http://{host}" ,
						 "RB=\" href=" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 "LAST" ) ;
						 
	web_reg_find("SaveCount=noProducts", "Text/IC=We're sorry, there are no products available for purchase at this time. Please check back later.", "LAST");

	
} 


void productDisplayPageCorrelations()
{

	web_reg_save_param ( "skuSelectionSwitchProductID" ,
	                     "LB=confirmSwitchProduct(event,'product_" ,
	                     "RB='" ,
	                     "notFound=Warning",
	                     "ORD=ALL" ,
	                     "LAST" ) ;

	web_reg_save_param ( "skuSelectionProductID" ,
	                     "LB=input type=\"hidden\" id=\"prodId\" value=\"" ,
	                     "RB=\"" ,
	                     "notFound=Warning",
	                     "LAST" ) ;
						 
}  


 
void addToCartCorrelations()
{
	web_reg_save_param_regexp( "ParamName=atc_catentryIds" ,
	                           "RegExp=\"catentry_id\" : \"(.*?)\",\r\n\t\t\t\t\t\t\t\t\t\t\t\"item_sku\" : \"(.*?)\",\r\n\t\t\t\t\t\t\t\t\t\t\t\"qty\" : \"([1-9][0-9]*)\"" ,
			                   "Ordinal=ALL" ,
			                   "Notfound=warning" ,
			                   "SEARCH_FILTERS" ,
			                   "Group=1" ,
			                   "LAST" ) ;

 	web_reg_save_param ( "atc_comment" ,
                    	 "LB=<input type=\"hidden\" name=\"comment\" value=\"" ,
                    	 "RB=\"" ,
                    	 "Notfound=warning" ,
                    	 "LAST" ) ;

}  

void viewHomePage()
{
	 
	homePageCorrelations();
	lr_param_sprintf ( "homePageUrl" , "http://%s%shome" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}") ) ;
	lr_start_transaction( "T01_Home Page" ) ;
		webURL(lr_eval_string ( "{homePageUrl}" ), "T01_Home Page" );
    lr_end_transaction( "T01_Home Page" , 2 ) ;
}  


void navByBrowse()
{
	 
 	lr_think_time ( LINK_TT ) ;
 
 
	lr_param_sprintf ( "cateogryPageUrl" , "http://%s%ssearch%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}") , lr_paramarr_random ( "categories" ) ) ;  

 

	categoryPageCorrelations();
	subCategoryPageCorrelations();  
 
	lr_start_transaction( "T02_Category Display" ) ;
	
		webURL(lr_eval_string ( "{cateogryPageUrl}" ), "T02_Category Display" );
		
		if ( isLoggedIn == 1 ) {
			 
		}
		
	if ((lr_paramarr_len("subcategories")) == 0)
	{
		lr_end_transaction ( "T02_Category Display" , 1 ) ;  
		lr_error_message("T02_Category Display  Failure : %s", lr_eval_string("{cateogryPageUrl}" ));  
		lr_output_message("T02_Category Display Failure : %s", lr_eval_string("{cateogryPageUrl}" ));  
		lr_param_sprintf ( "failedPageUrl" , "MikeCateg T02_Category Display : Failure on Invalid: %s", lr_eval_string("{cateogryPageUrl}") ) ;
		writeFailuresToFile();		
		return;
	}  
	else
		lr_end_transaction ( "T02_Category Display" , 0 ) ;

}  


void navByClearance()
{
	 
 	lr_think_time ( LINK_TT ) ;
	 
 
 
	lr_param_sprintf ( "clearancePageUrl" , "http://%s/shop/SearchDisplay?%s" , lr_eval_string("{host}"),  lr_paramarr_random( "clearances" ) ) ;  
	 

 

	categoryPageCorrelations();

	subCategoryPageCorrelations();  
	web_reg_find("SaveCount=noProducts", "Text/IC=We're sorry, there are no products available for purchase at this time. Please check back later.", "LAST");
	web_reg_find("Text=FILTER BY", "SaveCount=FilterBy_Count", "LAST" );
 
	lr_start_transaction ( "T02_Clearance Display" ) ;
	
		webURL(lr_eval_string ( "{clearancePageUrl}" ), "T02_Clearance Display" );

 
 
 
	
	if ( atoi(lr_eval_string("{noProducts}")) == 1 ) {
		lr_end_transaction ( "T02_Clearance Display" , 1 ) ;
		lr_error_message("T02_Clearance Display  :%s", lr_eval_string("{clearancePageUrl}"));  
		lr_output_message("T02_Clearance Display :%s", lr_eval_string("{clearancePageUrl}"));  
		lr_param_sprintf ( "failedPageUrl" , "MikeClearence T02_Clearance Display : Failure on Invalid: %s", lr_eval_string("{clearancePageUrl}") ) ;
		writeFailuresToFile();		

	}
 	else






		lr_end_transaction ( "T02_Clearance Display" , 0 ) ;


}  

findNewArrivals()  
{
	int offset = 1;

    char * position;

    char * str = "";

    char * search_str = "Arrivals";  

    while( offset >= 1 ) {

		str = lr_paramarr_random( "clearances" );
		 
	    position = (char *)strstr(str, search_str);

	    offset = (int)(position - str + 1);
    }

    lr_save_string(str, "clearance");

	return 0;
}

void typeAheadSearch()
{
	 
    for (i = 0; i < length; i++)
    {
       	 
       	if (i == 0)
           	lr_param_sprintf ("SEARCH_STRING_PARAM", "%c", searchString[i]);
       	else
           	lr_param_sprintf ("SEARCH_STRING_PARAM", "%s%c", lr_eval_string("{SEARCH_STRING_PARAM}"), searchString[i]);
           	 

		if (i == 1) lr_save_string(lr_eval_string("{SEARCH_STRING_PARAM}"),"forDidYouMean");
			
 
		if (i == 2 || i == 5 || i == 8 || i == 11 || i == 14 || i == 17 || i == 20 )
       	{
    		lr_think_time ( TYPE_SPEED );

			web_reg_save_param ( "autoSelectOption" , "LB=title=\"" ,"RB=\" id='autoSelectOption_" , "Notfound=warning" , "ORD=All", "LAST" ) ;
			
		
			lr_start_transaction ( "T02_Search_ajaxAutoSuggestion" ) ;

			web_submit_data("TCPAJAXAutoSuggestView",
				"Action=http://{host}/webapp/wcs/stores/servlet/{searchAutoSuggestURL}&term={SEARCH_STRING_PARAM}&showHeader=true&count=4",
				"Method=POST",
				"RecContentType=text/html",
				"Snapshot=t45.inf",
				"Mode=HTML",
				"ITEMDATA",
				"Name=objectId", "Value=", "ENDITEM",
				"Name=requesttype", "Value=ajax", "ENDITEM",
				"LAST");

			lr_end_transaction ( "T02_Search_ajaxAutoSuggestion" , 0 ) ;

			 
			if (atoi(lr_eval_string("{autoSelectOption_count}")) > 0) {
				searchString = lr_eval_string("{autoSelectOption_1}");
				break;
			} 
			
       	}  
   	 }  
}  


void submitCompleteSearch()  
{
	 
 	lr_think_time (LINK_TT);
	categoryPageCorrelations();
	subCategoryPageCorrelations();
	productDisplayPageCorrelations();
	
 
	lr_param_sprintf ("SEARCH_STRING", "%s", lr_eval_string("{originalSearchString}"));   
	
 
	web_reg_find("Text= 0 matches", "SaveCount=searchResults_Count", "LAST" );
	
 
 		




	lr_start_transaction ( "T02_Search" );
 
	web_url("submitSearch",
		"URL=https://{host}/shop/SearchDisplay?storeId={storeId}&catalogId={catalogId}&langId=-1&pageSize=100&beginIndex=0&searchSource=Q&sType=SimpleSearch&resultCatEntryType=2&showResultsPage=true&pageView=image&custSrch=search&searchTerm={SEARCH_STRING}&TCPSearchSubmit=", 
		"Resource=0",
		"RecContentType=text/html",
		"Snapshot=t59.inf",
		"Mode=HTML",
		"LAST");
		
	if ( isLoggedIn == 1 ) {
		TCPGetWishListForUsersView();
	}
	
	if (atoi(lr_eval_string("{searchResults_Count}")) > 0) {  
		lr_end_transaction ( "T02_Search" , 1 ) ; 
	} 
	else {
		lr_end_transaction ( "T02_Search" , 0 ) ;
	}
	
	return;
		
}  
		
		
void submitCompleteSearchDidYouMean()  
{
	 
 	lr_think_time (LINK_TT);
	categoryPageCorrelations();
	subCategoryPageCorrelations();
	productDisplayPageCorrelations();
	
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_SEARCH_SUGGEST )
		lr_param_sprintf ("SEARCH_STRING", "%s", lr_eval_string("{forDidYouMean}"));
	else
		lr_param_sprintf ("SEARCH_STRING", "%s", searchString);
	
	web_reg_find("Text= 0 matches", "SaveCount=searchResults_Count", "LAST" );
	web_reg_save_param("didYouMeanId", "LB=http://{host}/shop/SearchDisplay?searchTermScope=&searchType=1002&filterTerm=&orderBy=&maxPrice=&showResultsPage=true&langId=-1&departmentId=&sType=", 
	"RB=\" class=\"result\">", "ORD=All", "NotFound=Warning", "LAST");


	lr_start_transaction ( "T02_Search" ) ;

	 
	web_url("submitSearch", 
		"URL=https://{host}/shop/SearchDisplay?storeId={storeId}&catalogId={catalogId}&langId=-1&pageSize=100&beginIndex=0&searchSource=Q&sType=SimpleSearch&resultCatEntryType=2&showResultsPage=true&pageView=image&custSrch=search&searchTerm={SEARCH_STRING}&TCPSearchSubmit=", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Mode=HTML", 
		"LAST");
		
	lr_end_transaction ( "T02_Search" , 0 ) ;

	if ( atoi(lr_eval_string("{didYouMeanId_count}")) > 0 ) {  
		
		lr_think_time (LINK_TT);
		categoryPageCorrelations();
		subCategoryPageCorrelations();
		productDisplayPageCorrelations();

		lr_save_string( lr_paramarr_random("didYouMeanId"), "searchStringId");

		web_reg_find("Text= 0 matches", "SaveCount=searchResults_Count", "LAST" );
		
		lr_start_transaction("T02_Search_DidYouMean");

		web_url("SelectDidYouMeanSuggestion", 
			"URL=http://{host}/shop/SearchDisplay?searchTermScope=&searchType=1002&filterTerm=&orderBy=&maxPrice=&showResultsPage=true&langId=-1&departmentId=&sType={searchStringId}", 
			"TargetFrame=", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Mode=HTML", 
			"LAST");

		if (atoi(lr_eval_string("{searchResults_Count}")) == 0 )   
			lr_end_transaction ( "T02_Search_DidYouMean" , 1 ) ; 
		else
			lr_end_transaction ( "T02_Search_DidYouMean" , 0 ) ;
	}
	
}  
		
writeToFile1() 
{
	char *ip;
    char fullpath[1024], * filename1 = "\\e$\\Performance\\Scripts\\2016_R1\\03_PERF\\Datafiles\\TestData\\Roshni01132017.dat";
	long file1;
 
    strcpy(fullpath, "\\\\" );
	ip = lr_get_host_name( );
    strcat(fullpath, ip);
	strcat(fullpath, filename1);
       
    if ((file1 = fopen(fullpath, "a+")) == 0) {
        lr_output_message ("Unable to open %s", fullpath);
        return -1;
    }
	
	fprintf(file1, "%s\n", lr_eval_string("{SEARCH_STRING}"));
	
    fclose(file1);
	return 0;
}

writeToFile2() 
{
	long file;
	char *ip;
    char fullpath[1024], * filename2 = "\\e$\\Performance\\Scripts\\2016_R1\\03_PERF\\Datafiles\\TestData\\searchStrWithResult.dat";
    strcpy(fullpath, "\\\\" );
	ip = lr_get_host_name( );
    strcat(fullpath,  ip);
	strcat(fullpath, filename2);
 
       
    if ((file = fopen(fullpath, "a+")) == 0) {
        lr_output_message ("Unable to open %s", fullpath);
        return -1;
    }

	fprintf(file, "%s\n", lr_eval_string("{SEARCH_STRING}"));
	
    fclose(file);
	return 0;
		
}

void navBySearch()
{
	 

	int searchSuggest = atoi(lr_eval_string("{RANDOM_PERCENT}"));

	lr_think_time ( TYPE_SPEED );

	lr_start_transaction ( "T02_Search_ajaxCachedSuggestionView" ) ;

	web_submit_data("TCPAJAXCachedSuggestionsView",
		"Action=http://{host}/webapp/wcs/stores/servlet/{searchCachedSuggestionsURL}",
		"Method=POST",
		"RecContentType=text/html",
		"Snapshot=t44.inf",
		"Mode=HTML",
		"ITEMDATA",
		"Name=objectId", "Value=", "ENDITEM",
		"Name=requesttype", "Value=ajax", "ENDITEM",
		"LAST");

	lr_end_transaction ( "T02_Search_ajaxCachedSuggestionView" , 0 ) ;

	searchString = lr_eval_string("{searchStr}");  
	lr_save_string(searchString,"originalSearchString");
	
    length = (int)strlen(searchString);

    if (length >= 3)  
    	typeAheadSearch();
	
	if ( searchSuggest <= RATIO_SEARCH_SUGGEST )
		submitCompleteSearchDidYouMean();
	else
		submitCompleteSearch();

}  


void navByPlace()
{
	 
	if ( atoi( lr_eval_string( "{placeShopCategory_count}")) != 0 ) {
		
		lr_save_string( lr_paramarr_random("placeShopCategory"), "placeShop");
		
		if ( strcmp( lr_eval_string("{placeShop}") ,"placeShops-pjplace-2014" ) == 0) 
			lr_save_string( lr_paramarr_random("placeShopCategory"), "placeShop");
		
		lr_start_transaction ( "T02_PlaceShop" ) ;

		web_url("placeShop",
			"URL=http://{host}{store_home_page}content/{placeShop}",
			"Resource=0",
			"RecContentType=text/html",
			"LAST");
		
		lr_end_transaction ( "T02_PlaceShop" , 0 ) ;
	}
 
# 629 "..\\..\\browseFunctions.c"
}  


 
 
void topNav()
{
 

	randomPercent = atoi(lr_eval_string("{RANDOM_PERCENT}"));

	if ( randomPercent < NAV_BROWSE ){
		 
		navByBrowse();
		}  
	else if ( randomPercent < (NAV_BROWSE + NAV_CLEARANCE) ) {
		 
		navByClearance();
		}  
	else if ( randomPercent < (NAV_BROWSE + NAV_CLEARANCE + NAV_SEARCH)) {
		 
		navBySearch();
		}  
	else if (randomPercent < (NAV_BROWSE + NAV_CLEARANCE + NAV_SEARCH + NAV_PLACE)) {
		 
		navByPlace();
		}  

}  


void paginate()
{
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) < APPLY_PAGINATE ) {
		
		if (( lr_paramarr_len ( "paginationNextURL" ) > 0 ) ) {
			
			lr_think_time ( LINK_TT ) ;
			
		 
			subCategoryPageCorrelations();
			productDisplayPageCorrelations();
			index = rand ( ) % lr_paramarr_len( "paginationNextURL" ) + 1 ;
			lr_param_sprintf ("paginationURL", "http://%s%s&beginIndex=%s", lr_eval_string("{host}"), lr_paramarr_idx ( "paginationNextURL" , index ), lr_paramarr_idx ( "paginationPageIndex" , index ));
			lr_param_sprintf ("paginationIndex", "paginationPageIndex_%d", index);

			lr_start_transaction ( "T03_Paginate" ) ;
			
				web_submit_data("AjaxCatalogSearchResultView",
					"Action={paginationURL}",
					"Method=POST",
					"RecContentType=text/html",
					"Mode=HTML",
					"ITEMDATA",
					"Name=searchResultsPageNum", "Value=-{paginationIndex}", "ENDITEM",
					"Name=searchResultsView", "Value=", "ENDITEM",
					"Name=searchResultsURL", "Value={paginationURL}", "ENDITEM",
					"Name=objectId", "Value=", "ENDITEM",
					"Name=requesttype", "Value=ajax", "ENDITEM",
					"LAST");
					
			lr_end_transaction ( "T03_Paginate" , 0 ) ;
		}
	}
	
}  


void sortResults()
{
	 
 	lr_think_time ( LINK_TT ) ;
	if (( lr_paramarr_len ( "sortURL" ) > 0 )) {
		categoryPageCorrelations();
		subCategoryPageCorrelations();
		productDisplayPageCorrelations();
	 
	 
		lr_param_sprintf ( "drillUrl" , "http://%s%s" , lr_eval_string("{host}") , lr_paramarr_random ( "sortURL" ) ) ;

		lr_start_transaction ( "T03_Sort Results" ) ;
			webURL(lr_eval_string ( "{drillUrl}" ), "T03_Sort Results" );
		lr_end_transaction ( "T03_Sort Results" , 0 ) ;
	}  
}  

void sort()
{
 
 
 

 

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) < APPLY_SORT ){
		 
		sortResults();
		}  
	else {
		 
		return;
		}  

}  

void drillOneFacet()
{
	 
 	lr_think_time ( LINK_TT ) ;

	if (( lr_paramarr_len ( "facetsID" ) > 0 ) ) {
		categoryPageCorrelations();
		subCategoryPageCorrelations();
		productDisplayPageCorrelations();
		index = rand ( ) % lr_paramarr_len( "facetsID" ) + 1 ;
		lr_param_sprintf ( "drillUrlOneFacet" , "http://%s%s&facet=%s" , lr_eval_string("{host}") , lr_paramarr_idx ( "facetsURL" , index ) , lr_paramarr_idx ( "facetsID" , index ) ) ;
 
 

		lr_start_transaction ( "T03_Facet Display_1facet" ) ;

			webURL(lr_eval_string ( "{drillUrlOneFacet}" ), "T03_Facet Display_1facet" );

			if ( isLoggedIn == 1 ) {
				 
				 
			}

		lr_end_transaction ( "T03_Facet Display_1facet" , 0 ) ;

	}  
}  


void drillTwoFacets()
{
	 
	drillOneFacet();
 	lr_think_time ( LINK_TT ) ;
	if (( lr_paramarr_len ( "facetsID" ) > 0 ) ) {
		categoryPageCorrelations();
		subCategoryPageCorrelations();
		productDisplayPageCorrelations();
 
 
		lr_param_sprintf ( "drillUrlTwoFacets" , "%s&facet=%s" , lr_eval_string("{drillUrlOneFacet}") , lr_paramarr_random ( "facetsID"  ) ) ;
		
		lr_start_transaction ( "T03_Facet Display_2facets" ) ;

			webURL(lr_eval_string ( "{drillUrlTwoFacets}" ), "T03_Facet Display_2facets" );

			if ( isLoggedIn == 1 ) {
				TCPGetWishListForUsersView();
				 
			}

		lr_end_transaction ( "T03_Facet Display_2facets" , 0 ) ;

	}  
}  


void drillThreeFacets()
{
	 
	drillTwoFacets();
 	lr_think_time ( LINK_TT ) ;
	if (( lr_paramarr_len ( "facetsID" ) > 0 ) ) {
		categoryPageCorrelations();
		subCategoryPageCorrelations();
		productDisplayPageCorrelations();
 
 
		lr_param_sprintf ( "drillUrlThreeFacets" , "%s&facet=%s" , lr_eval_string("{drillUrlTwoFacets}") , lr_paramarr_random ( "facetsID" ) ) ;
		
		lr_start_transaction ( "T03_Facet Display_3facets" ) ;

			webURL(lr_eval_string ( "{drillUrlThreeFacets}" ), "T03_Facet Display_3facets" );

			if ( isLoggedIn == 1 ) {
				TCPGetWishListForUsersView();
				 
			}

		lr_end_transaction ( "T03_Facet Display_3facets" , 0 ) ;

	} 
} 


void drillSubCategory()
{
	 
 	lr_think_time ( LINK_TT ) ;
	categoryPageCorrelations();
	subCategoryPageCorrelations();
	productDisplayPageCorrelations();

	if (( lr_paramarr_len ( "subcategories" ) > 0 )) {
 
 
		lr_param_sprintf ( "drillUrl" , "http://%s%s" , lr_eval_string("{host}") , lr_paramarr_random ( "subcategories" ) ) ;
		
		lr_start_transaction ( "T03_Sub-Category Display" ) ;

			webURL(lr_eval_string ( "{drillUrl}" ), "T03_Sub-Category Display" );

			if ( isLoggedIn == 1 ) {
				TCPGetWishListForUsersView();
				 
			}

		if ( atoi(lr_eval_string("{noProducts}")) == 1 ) {
			lr_end_transaction ( "T03_Sub-Category Display" , 1 ) ;
 
		}
		else
			lr_end_transaction ( "T03_Sub-Category Display" , 0 ) ;

	}  
	else if ((lr_paramarr_len( "searchSubCategories") > 0)) {
 
 
		lr_param_sprintf ( "drillUrl" , "http://%s%s" , lr_eval_string("{host}") , lr_paramarr_random ( "searchSubCategories" ) ) ;

		lr_start_transaction ( "T03_Sub-Category Display" ) ;

			webURL(lr_eval_string ( "{drillUrl}" ), "T03_Sub-Category Display" );

			if ( isLoggedIn == 1 ) {
				TCPGetWishListForUsersView();
				 
			}

		if ( atoi(lr_eval_string("{noProducts}")) == 1 ) {
			lr_end_transaction ( "T03_Sub-Category Display" , 1 ) ;
 
		}
		else
			lr_end_transaction ( "T03_Sub-Category Display" , 0 ) ;

	}  
	
} 


void drill()
{
 
 
	if (( lr_paramarr_len( "subcategories" ) == 0 ) && (lr_paramarr_len( "searchSubCategories") == 0) && ( lr_paramarr_len ( "facetsID" ) == 0 ) ) {
		 
 
		return;
	}  
	else if ( lr_paramarr_len ( "subcategories" ) == 0 && (lr_paramarr_len( "searchSubCategories") == 0)) {
		drill_by = "ONE_FACET" ;
		 
		}  
	else if ( lr_paramarr_len ( "facetsID" ) == 0 ) {
		drill_by = "SUB_CATEGORY" ;
		 
		}  
	else {
		 
		 

		randomPercent = atoi(lr_eval_string("{RANDOM_PERCENT}"));

		if ( randomPercent <= DRILL_SUB_CATEGORY ){
			drill_by = "SUB_CATEGORY" ;
			}  
		else if ( randomPercent <= (DRILL_SUB_CATEGORY + DRILL_TWO_FACET) ) {
			drill_by = "TWO_FACET" ;
			}  
		else if ( randomPercent <= (DRILL_SUB_CATEGORY + DRILL_TWO_FACET + DRILL_THREE_FACET)) {
			drill_by = "THREE_FACET" ;
			}  
		else if (randomPercent <= (DRILL_SUB_CATEGORY + DRILL_TWO_FACET + DRILL_THREE_FACET + DRILL_ONE_FACET)) {
			drill_by = "ONE_FACET" ;
			}  

	}  

	if (strcmp(drill_by, "ONE_FACET") == 0) {
		 
		drillOneFacet();
		}  
	else if (strcmp(drill_by, "TWO_FACET") == 0) {
		 
		drillTwoFacets();
		}  
	else if (strcmp(drill_by, "THREE_FACET") == 0) {
		 
		drillThreeFacets();
		}  
	else if (strcmp(drill_by, "SUB_CATEGORY") == 0) {
		 
		drillSubCategory();
		}  
	else if (strcmp(drill_by, "stop") == 0) {
		 
 
		}  
	else if (strcmp(drill_by, "pass") == 0){
 
		 
	}  
}  


void productDetailView()
{
	 
 	lr_think_time ( LINK_TT ) ;
	
	if (( lr_paramarr_len ( "pdpURL" ) > 0 )) {
		
		TCPIShippingView();
		
		productDisplayPageCorrelations();
 
 
		lr_param_sprintf ( "drillUrl" , "http://%s%s" , lr_eval_string("{host}") , lr_paramarr_random ( "pdpURL" ) ) ;
		
		lr_start_transaction ( "T04_Product Display Page" ) ;
		
		webURL(lr_eval_string ( "{drillUrl}" ), "T04_Product Display Page" );
	
 
		
			addToCartCorrelations();
			
 

				lr_start_sub_transaction ("T04_Product Display Page - TCPSkuSelectionView", "T04_Product Display Page" );

				web_custom_request("TCPSKUSelectionView",
					"URL=http://{host}/webapp/wcs/stores/servlet/TCPSKUSelectionView?langId=-1&storeId={storeId}&catalogId={catalogId}&LimQty=undefined&TCPWebOnlyFlag=&productId={skuSelectionProductID}",
					"Method=GET",
					"Resource=0",
					"RecContentType=text/html",
					"Snapshot=t18.inf",
					"Mode=HTML",
					"EncType=application/x-www-form-urlencoded",
					"LAST");
						
				lr_end_sub_transaction ( "T04_Product Display Page - TCPSkuSelectionView", 0 );
 

			if ( isLoggedIn == 1 ) {
				TCPGetWishListForUsersView();
				 
			}
	 

		lr_end_transaction ( "T04_Product Display Page" , 0 ) ;
		
		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) < PRODUCT_QUICKVIEW_SERVICE )
			productQuickviewService();

	}  
	
}  


void productQuickView()
{
	 
 	lr_think_time ( LINK_TT ) ;
	if (( lr_paramarr_len ( "quickviewURL" ) > 0 )) {
		
		productDisplayPageCorrelations();
 
 
		lr_param_sprintf ( "drillUrl" , "http://%s%s" , lr_eval_string("{host}") , lr_paramarr_random ( "quickviewURL" ) ) ;
		
		lr_start_transaction ( "T04_Product Quickview Page" ) ;
		
			webURL(lr_eval_string ( "{drillUrl}" ), "T04_Product Quickview Page" );
		
		 
			addToCartCorrelations();
			
 

				lr_start_sub_transaction ("T04_Product Quickview Page - TCPSkuSelectionView", "T04_Product Quickview Page" );

				web_custom_request("TCPSKUSelectionView",
					"URL=http://{host}/webapp/wcs/stores/servlet/TCPSKUSelectionView?langId=-1&storeId={storeId}&catalogId={catalogId}&LimQty=undefined&TCPWebOnlyFlag=&productId={skuSelectionProductID}",
					"Method=GET",
					"Resource=0",
					"RecContentType=text/html",
					"Snapshot=t18.inf",
					"Mode=HTML",
					"EncType=application/x-www-form-urlencoded",
					"LAST");

				lr_end_sub_transaction ( "T04_Product Quickview Page - TCPSkuSelectionView", 0 );
 

			 
				 
 
		
		lr_end_transaction ( "T04_Product Quickview Page" , 0 ) ;


	}  
}  

void productQuickviewService()  
{
		web_reg_find("SaveCount=swatchesAndSizeInfo", "Text/IC=itemTCPBogo","LAST");
		
		lr_start_transaction ("T04_Product Quickview Service - GetSwatchesAndSizeInfo" );

			web_custom_request("GetSwatchesAndSizeInfo",
 
				"URL=http://{host}/webapp/wcs/stores/servlet/GetSwatchesAndSizeInfo?storeId={storeId}&productId={skuSelectionProductID}",
				"Method=GET",
				"Mode=HTML",
				"LAST");


		if(atoi(lr_eval_string("{swatchesAndSizeInfo}")) > 0)
			lr_end_transaction ( "T04_Product Quickview Service - GetSwatchesAndSizeInfo", 0 );	
		else
			lr_end_transaction ( "T04_Product Quickview Service - GetSwatchesAndSizeInfo", 1 );	
	

		web_reg_find("SaveCount=detailsView", "Text/IC=inventoryFlag","LAST");

		lr_start_transaction ("T04_Product Quickview Service - TCPGetSKUDetailsView" );

			web_custom_request("TCPGetSKUDetailsView",
 
				"URL=http://{host}/webapp/wcs/stores/servlet/TCPGetSKUDetailsView?storeId={storeId}&catalogId={catalogId}&langId=-1&productId={skuSelectionProductID}",
				"Method=GET",
				"Mode=HTML",
				"LAST");

	
		if(atoi(lr_eval_string("{detailsView}")) > 0)
			lr_end_transaction ( "T04_Product Quickview Service - TCPGetSKUDetailsView", 0 );	
		else
			lr_end_transaction ( "T04_Product Quickview Service - TCPGetSKUDetailsView", 1 );	

}

void productDisplay()
{
 
 

	 

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= PDP ){
		 
		productDetailView();  
		}  
	else {
		 
		productQuickView();  
	}  
 

}  



void switchColor()
{
}  

void emailSignUp()
{
	lr_start_transaction ( "T16_Submit Shipping Address" ) ;
	lr_end_transaction ( "T16_Submit Shipping Address" , 0) ;

}  


void StoreLocator()
{

 	lr_think_time ( LINK_TT ) ;

	lr_start_transaction("T22_StoreLocator");

	web_url("Stores", 
		"URL=http://{host}/shop/AjaxStoreLocatorDisplayView?catalogId={catalogId}&langId=-1&storeId={storeId}", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=http://{host}/shop/us/home", 
		"Snapshot=t325.inf", 
		"Mode=HTML", 
		"LAST");

	lr_end_transaction("T22_StoreLocator", 2);
	
	lr_start_transaction("T22_StoreLocator_Find");

	web_submit_data("AjaxStoreLocatorResultsView", 
		"Action=http://{host}/shop/AjaxStoreLocatorResultsView?catalogId={catalogId}&langId=-1&orderId=&storeId={storeId}", 
		"Method=POST", 
		"TargetFrame=", 
		"RecContentType=text/html", 
		"Referer=http://{host}/shop/AjaxStoreLocatorDisplayView?catalogId={catalogId}&langId=-1&storeId={storeId}", 
		"Snapshot=t326.inf", 
		"Mode=HTML", 
		"ITEMDATA", 
		"Name=distance", "Value={storeLocatorDistance}", "ENDITEM", 
		"Name=latitude", "Value={storeLocatorLatitude}", "ENDITEM", 
		"Name=longitude", "Value={storeLocatorLongitude}", "ENDITEM", 
		"Name=displayStoreInfo", "Value=false", "ENDITEM", 
		"Name=fromPage", "Value=StoreLocator", "ENDITEM", 
		"Name=objectId", "Value=", "ENDITEM", 
		"Name=requesttype", "Value=ajax", "ENDITEM", 
		"LAST");

	lr_end_transaction("T22_StoreLocator_Find", 2);

}

void TCPIShippingView()
{
	lr_think_time ( LINK_TT ) ;
	
	 
	
	lr_start_transaction("T27_TCPIShippingView");
	
	web_custom_request("TCPIShippingView",
		"URL=https://{host}/shop/TCPIShippingView?langId=-1&storeId={storeId}&catalogId={catalogId}",
		"Method=GET",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		"EncType=application/x-www-form-urlencoded",
		"LAST");
		
	lr_end_transaction("T27_TCPIShippingView", 2);
	 
	TCPGetWishListForUsersViewBrowse();
	
}

void TCPGetWishListForUsersViewBrowse()
{
	lr_start_transaction("T25_Common_S01_TCPGetWishListForUsersView");

	web_custom_request("TCPGetWishListForUsersView",
		"URL=http://{host}/webapp/wcs/stores/servlet/TCPGetWishListForUsersView?tcpCallBack=jQuery1113014590106982485151_1473881708524&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&sortBy=&_=1473881708525", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Mode=HTML", 
		"EncType=application/x-www-form-urlencoded", 
		"LAST");

	lr_end_sub_transaction("T25_Common_S01_TCPGetWishListForUsersView", 2);
	
}


writeFailuresToFile() 
{
	char *ip;
    char fullpath[1024], * filename1 = "\\e$\\Performance\\0116_Perflive_pageWithNoResult.txt";
	long file1;
    strcpy(fullpath, "\\\\" );
	ip = lr_get_host_name( );
    strcat(fullpath, ip);
	strcat(fullpath, filename1);
        






    if ((file1 = fopen(fullpath, "a+")) == 0) {
        lr_output_message ("Unable to open %s", fullpath);
        return -1;
    }
 
 
 
		fprintf(file1, "%s\n", lr_eval_string("{failedPageUrl}"));
		 

    fclose(file1);
    
	return 0;
}
# 1 "vuser_init.c" 2


vuser_init()
{	
 	
# 15 "vuser_init.c"
	return 0;
}
# 4 "e:\\performance\\scripts\\2017\\03_perf\\ecommerce\\tcp_browse_sanity\\\\combined_TCP_Browse_Sanity.c" 2

# 1 "Action.c" 1
Action()
{
	int j, jLoop, k, kLoop, jCounter, kCounter;
	web_set_max_html_param_len ( "3072" ) ;
	web_cleanup_cookies ( ) ;
	lr_save_string ( lr_get_attrib_string ( "TestEnvironment") , "host" ) ; 
 
 
 
 
 
	
 
 
	
	if ( atoi( lr_eval_string("{RANDOM_PERCENT}")) <= lr_get_attrib_long("US_Traffic_Percentage") ) {
		lr_save_string( "/shop/us/", "store_home_page" );
		lr_save_string("10151", "storeId");
		lr_save_string("10551", "catalogId");
	} else {
		lr_save_string( "/shop/ca/", "store_home_page" );
		lr_save_string("10152", "storeId");
		lr_save_string("10552", "catalogId");
	}
	
	viewHomePage();

	 
	 
	jLoop = lr_paramarr_len( "categories" );
	if (jLoop != 0) {
		jCounter++;
		for (j=1; j < jLoop; j++) {
			
			lr_param_sprintf ( "cateogryPageUrl" , "http://%s%ssearch%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}") , lr_paramarr_idx ( "categories" , j ) ) ;
			 
			 
			web_reg_find("SaveCount=noProducts", "Text/IC=We're sorry, there are no products available for purchase at this time. Please check back later.", "LAST");
			web_reg_save_param("noProductPage", "LB=stringArray = '", "RB='.split", "NotFound=Warning", "LAST");
			categoryPageCorrelations();
			
			web_url ( "Check Categories" ,
		    	     "URL={cateogryPageUrl}" ,
		             "Resource=0" ,
		             "Mode=HTML" ,
		             "LAST" ) ;
			
			lr_think_time(3);
			if(atoi(lr_eval_string("{noProducts}")) == 1) {
				lr_save_string("Categories", "HeaderName");
				lr_save_int(jCounter++, "iCounter");
				writeToFile(1);
			}
			else {
				 
				 
				 
				kLoop = lr_paramarr_len( "subcategories" );
				if (kLoop != 0) {
					kCounter++;
					for (k=1; k < kLoop; k++) {
						
						lr_param_sprintf ( "subCateogryPageUrl" , "http://%s%s" , lr_eval_string("{host}") , lr_paramarr_idx ( "subcategories" , k ) ) ;
						
						web_reg_find("SaveCount=noProducts", "Text/IC=We're sorry, there are no products available for purchase at this time. Please check back later.", "LAST");
						web_reg_save_param("noProductPage", "LB=stringArray = '", "RB='.split", "NotFound=Warning", "LAST");
						categoryPageCorrelations();
						
						web_url ( "Check Sub-Categories" ,
					    	     "URL={subCateogryPageUrl}" ,
					             "Resource=0" ,
					             "Mode=HTML" ,
					             "LAST" ) ;
						
						lr_think_time(3);
						if(atoi(lr_eval_string("{noProducts}")) == 1) {
							lr_output_message("PavanDusi: BAD URL %s", lr_eval_string("{subCateogryPageUrl}"));
							lr_save_string("Sub-Categories", "HeaderName");
							lr_save_int(kCounter++, "iCounter");
							writeToFile(1);
						}else{
							lr_output_message("PavanDusi: GOOD URL %s", lr_eval_string("{subCateogryPageUrl}"));
						}
					}
				}
			}
		}
	}

			
	 
	lr_save_string("Clearances", "HeaderName");
	 
	jLoop = lr_paramarr_len( "clearances" );
	if (jLoop != 0) {
		jCounter++;
		for (j=1; j < jLoop; j++) {
			
			lr_param_sprintf ( "clearancePageUrl" , "http://%s/shop/SearchDisplay?%s" , lr_eval_string("{host}"),  lr_paramarr_idx ( "clearances" , j) ) ;
			
			web_reg_find("SaveCount=noProducts", "Text/IC=We're sorry, there are no products available for purchase at this time. Please check back later.", "LAST");
			web_reg_save_param("noProductPage", "LB=stringArray = '", "RB='.split", "NotFound=Warning", "LAST");
			
			web_url ( "Check Clearance" ,
		    	     "URL={clearancePageUrl}" ,
		             "Resource=0" ,
		             "Mode=HTML" ,
		             "LAST" ) ;
			
			lr_think_time(3);
			if(atoi(lr_eval_string("{noProducts}")) == 1) {
				lr_save_int(jCounter++, "iCounter");
				writeToFile(1);
			}
		}
	}
		
	return 0;
}

writeToFile(int header) 
{

    char fullpath[1024];
	long file1;
	char *filename1 = "e:\\Performance\\Scripts\\2017\\CA_Perflive_pageWithNoResult.txt";
	strcpy(fullpath, filename1);
       
    if ((file1 = fopen(fullpath, "a+")) == 0) {
        lr_output_message ("Unable to open %s", fullpath);
        return -1;
    }
 
 
 
		fprintf(file1, "%s\n", lr_eval_string("{HeaderName}->{noProductPage}"));
		 
	
    fclose(file1);
    
	return 0;
}
# 5 "e:\\performance\\scripts\\2017\\03_perf\\ecommerce\\tcp_browse_sanity\\\\combined_TCP_Browse_Sanity.c" 2

# 1 "vuser_end.c" 1
vuser_end()
{
	return 0;
}
# 6 "e:\\performance\\scripts\\2017\\03_perf\\ecommerce\\tcp_browse_sanity\\\\combined_TCP_Browse_Sanity.c" 2

