dropCart()
{
	lr_user_data_point("Abandon Cart Rate", 1);
	lr_user_data_point("Checkout Rate", 0);
	lr_save_string("dropCartFlow", "currentFlow");
	
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CHECKOUT_GUEST ) {
		dropCartGuest();
	} 
	else {
		lr_save_string( lr_eval_string("{logonid_drop}"), "userEmail" );

		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CHECKOUT_LOGIN_FIRST ) 
		{
			dropCartLoginFirst();
        }
        else { 
			dropCartBuildCartFirst();
		} 
	}
	return 0;
}

dropCartGuest()
{
	lr_save_string("1", "totalNumberOfItems_count");
	buildCartDrop(0);
//	lr_exit(LR_EXIT_MAIN_ITERATION_AND_CONTINUE, LR_PASS);

	inCartEdits();

	if (proceedAsGuest() == LR_PASS)
	{
		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= LOGIN_PAGE_DROP )
		{		dropCartTransaction();
				return 0;
		}

		if ( submitShipping() == LR_PASS)
		{
			if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= BILL_PAGE_DROP )
			{	dropCartTransaction();
				return 0;
			}

			if (BillingAsGuest() == LR_PASS)
				dropCartTransaction();
		}
	}
	
	return 0;		
}

dropCartLoginFirst()
{
	int viewHistory = atoi(lr_eval_string("{RANDOM_PERCENT}"));
//	lr_exit(LR_EXIT_MAIN_ITERATION_AND_CONTINUE, LR_PASS);
	
	if (loginFromHomePage() == LR_PASS)
	{
	    viewCart();

	    buildCartDrop(1);
	//lr_exit(LR_EXIT_MAIN_ITERATION_AND_CONTINUE, LR_PASS);

		if ( strcmp(strupr(lr_eval_string("{buildCart}")),  "TRUE") != 0) 
			inCartEdits();
        
		if (proceedToCheckout_ShippingView() == LR_PASS) //proceedToCheckout_ShippingView(); //proceedAsRegistered();
		{

	        if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= SHIP_PAGE_DROP )
			{	dropCartTransaction();
				return 0;
			}
	        
			if (submitShippingRegistered() == LR_PASS)
			{
    
	            if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= BILL_PAGE_DROP )
				{		dropCartTransaction();
						return 0;
				}

				if (submitBillingRegistered() == LR_PASS)
					dropCartTransaction();

            }
        }
	}
	
	return 0;            
	
}

dropCartBuildCartFirst()
{
	lr_save_string("1", "totalNumberOfItems_count");
	
	buildCartDrop(0);
//	lr_exit(LR_EXIT_MAIN_ITERATION_AND_CONTINUE, LR_PASS);

	inCartEdits();
    
//    proceedToCheckout();
 
    if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= LOGIN_PAGE_DROP )
	{	dropCartTransaction();
		return 0;
	}

	if (login() == LR_PASS) 
	{

	    if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= SHIP_PAGE_DROP )
		{	dropCartTransaction();
			return 0;
		}

		if (submitShippingBrowseFirst() == LR_PASS) 
		{
	        if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= BILL_PAGE_DROP )
			{		dropCartTransaction();
					return 0;
			}

		     if (submitBillingRegistered() == LR_PASS ) 
				dropCartTransaction();
        }
    }
	
	return 0;
}