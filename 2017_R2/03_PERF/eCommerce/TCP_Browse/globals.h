#ifndef _GLOBALS_H
#define _GLOBALS_H

//--------------------------------------------------------------------
// Include Files
#include "lrun.h"
#include "web_api.h"
#include "lrw_custom_body.h"
//--------------------------------------------------------------------
// Global Variables
int LINK_TT, FORM_TT, TYPE_SPEED; //Think Time
	//set global think time values for link clicks, form entry pages and type speed for typeAhead search.
	LINK_TT = 10;//increase to 20 
	FORM_TT = 10;//increase to 20 
	TYPE_SPEED = 1;
/*
int NAV_BROWSE, NAV_SEARCH, NAV_CLEARANCE, NAV_PLACE; //2nd level (T02) NAV_BY ratio
int DRILL_ONE_FACET, DRILL_TWO_FACET, DRILL_THREE_FACET, DRILL_SUB_CATEGORY; //3rd level (T03) DRILL_BY ratio
int PDP, QUICKVIEW; //Product Display vs Product QuickView ratio (T04)
int APPLY_SORT; //Sort ratio
int APPLY_PAGINATE; //Pagination ratio
int LINK_TT, FORM_TT, TYPE_SPEED; //Think Time

int index , length , i, randomPercent;
char *searchString ;
char *nav_by ; // T02 - Category Navigation selection
char *drill_by ; // T03 - Facet / SubCategory drill selection
char *sort_by; // T03 - Sort selection
char *product_by; // T04 - Product Display Method
int form_TT, link_TT, typeSpeed_TT;
//typedef long time_t;
//time_t t;
//int authcookielen , authcookielen = 0;

	//set global think time values for link clicks, form entry pages and type speed for typeAhead search.
	LINK_TT = 10;
	FORM_TT = 10;
	TYPE_SPEED = 1;

	//set 2nd level navigation ratio for T02 transaction. Sum must be 100. Valid value is 0 to 100.
	NAV_BROWSE = 45;
	NAV_SEARCH = 5;
	NAV_CLEARANCE = 50;
	NAV_PLACE = 0;

	//set drill navigation ratio for T03 transaction. Sum must be 100. Valid value is 0 to 100.
	DRILL_ONE_FACET = 25;
	DRILL_TWO_FACET = 15;
	DRILL_THREE_FACET = 10;
	DRILL_SUB_CATEGORY = 50;

	//set the % for SORT during browse activity. Valid value is 0 to 100. Used only in Browse scripts,
	APPLY_SORT = 25;

	//set the % for PAGINATION during browse activity, this will apply pagination only if additional pages are available. Valid value is 0 to 100. Used only in Browse scripts.
	APPLY_PAGINATE = 100;

	//set PDP vs PQV view ratio (T04). Sum must be 100. Valid value is 0 to 100.
	PDP = 67;
	QUICKVIEW = 33;
*/
#endif // _GLOBALS_H