//This is the 18K Version
Action()
{
	int j;
	web_set_max_html_param_len ( "3072" ) ;
	web_cleanup_cookies ( ) ;
	lr_save_string ( lr_get_attrib_string ( "TestEnvironment") , "host" ) ; 
	lr_save_string ( lr_get_attrib_string ( "APIEnvironment") , "api_host" ) ; 
//	web_set_sockets_option("SSL_VERSION", "TLS"); //PERFLIVE
	web_set_sockets_option("SSL_VERSION", "TLS1.1"); //PERFLIVE
//	lr_message("LINK_TT=%d, FORM_TT=%d, TYPE_SPEED=%d,", LINK_TT, FORM_TT, TYPE_SPEED);
//	NAV_BROWSE = 0;
//	NAV_SEARCH = 0; //
//	NAV_CLEARANCE = 100; //12/19
//	NAV_PLACE = 0;
	lr_save_string("10151", "storeId");
	lr_save_string("10551", "catalogId");
	
	if ( atoi( lr_eval_string("{RANDOM_PERCENT}")) <= lr_get_attrib_long("US_Traffic_Percentage") ) {
		lr_save_string( "/shop/us/", "store_home_page" );
		lr_save_string("10151", "storeId");
		lr_save_string("10551", "catalogId");
	} else {
		lr_save_string( "/shop/ca/", "store_home_page" );
		lr_save_string("10152", "storeId");
		lr_save_string("10552", "catalogId");
	}
	viewHomePage();
			
	for (j=0; j < lr_get_attrib_long("Browsing_Iteration"); j++) {
		
		topNav();
	
		drill();
	
		paginate();
		
		productDisplay();
	}
	
	return 0;
}