checkOut()
{
	lr_user_data_point("Abandon Cart Rate", 0);
	lr_user_data_point("Checkout Rate", 1);
	lr_save_string("checkoutRopisFlow", "currentFlow");
	
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CHECKOUT_GUEST ) {
		
		checkoutGuest();
	} 
	else {
		
  		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CHECKOUT_LOGIN_FIRST ) { //login first
			
			checkoutLoginFirst();
        }
        else { 
			
			checkoutBuildCartFirst();
        } 
	}

	return 0;
}

checkoutGuest() 
{
	lr_save_string("1", "totalNumberOfItems_count");

	topNav();
	
	buildCartRopis();
/*
	inCartEdits();
		
	proceedToCheckout();

	proceedAsGuest();

	selectShipMode();
	submitShippingAddressAsGuest();

	selectBillingAddress();   
	submitBillingAddressAsGuest(); 

	submitOrder();

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_REGISTER ) {
		registerUser();	
	}		

	if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ORDER_HISTORY ) {
		loginForOrderHistory();
		viewOrderHistory();
		if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ORDER_STATUS ) 
			viewOrderStatus();
			
		logoff();
	}

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_STORE_LOCATOR ) {
		StoreLocator();
	}
*/
	return 0;
}

checkoutLoginFirst()
{
    loginFromHomePage(); 

    topNav();
	
	buildCartRopis();

/*
    if ( atoi(lr_eval_string("{RANDOM_WL_PERCENT}")) <= RATIO_WISHLIST )
        wishList();
        
	inCartEdits();

	if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_REDEEM_LOYALTY_POINTS ) 
		convertAndApplyPoints();
	
    proceedToCheckout_ShippingView();
    
    if (selectShippingAddress() == LR_PASS ) {
    	//selectShippingAddress();
		//selectShipMode(); // see if it still works without this 02/22
        submitShippingAddress();

        if (selectBillingAddress() == LR_PASS ) {
        //	selectBillingAddress();
            if (submitBillingAddress() == LR_PASS ) {
            //submitBillingAddress();
    
	            submitOrder();
	            
				if( viewHistory <= RATIO_POINTS_HISTORY ) {
					viewPointsHistory();
				}
	            else if( viewHistory <= (RATIO_ORDER_HISTORY + RATIO_POINTS_HISTORY) ) {
	            	viewOrderHistory();
	//            	lr_message("RATIO_ORDER_STATUS: %d", RATIO_ORDER_STATUS);
	                if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ORDER_STATUS ) 
	                    viewOrderStatus();
            	}//if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ORDER_HISTORY ) {
				else if( viewHistory <= (RATIO_ORDER_HISTORY + RATIO_POINTS_HISTORY + RATIO_RESERVATION_HISTORY) ) {
	            	viewReservationHistory();
	            }
        	}//if (submitBillingAddress() == LR_PASS ) {
        }//if (selectBillingAddress() == LR_PASS ) {
    }//if (selectShippingAddress() == LR_PASS ) {

    if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_STORE_LOCATOR ) {
		StoreLocator();
	}
*/
   	viewReservationHistory();
   	
	return 0;
}

checkoutBuildCartFirst()
{
	lr_save_string("1", "totalNumberOfItems_count");
	
	topNav();
	
	buildCartRopis();
    
    login();
    
/*
	inCartEdits();

    //proceedToCheckout(); //0502
    proceedToCheckout_ShippingView();
    login();

    if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_REDEEM_LOYALTY_POINTS ) {
        convertAndApplyPoints();
        proceedToCheckout_ShippingView();
    }
//=======================================================================
    if (selectShippingAddress() == LR_PASS ) {
    	//selectShippingAddress();
//            selectShipMode(); // see if it still works without this 02/22
        submitShippingAddress();

        if (selectBillingAddress() == LR_PASS ) {
        //	selectBillingAddress();
            if (submitBillingAddress() == LR_PASS ) {
            //submitBillingAddress();
    
	            submitOrder();
	            
				if( viewHistory <= RATIO_POINTS_HISTORY ) {
					viewPointsHistory();
				}
				else if( viewHistory <= (RATIO_ORDER_HISTORY + RATIO_POINTS_HISTORY) ) {
	            	viewOrderHistory();
	//            	lr_message("RATIO_ORDER_STATUS: %d", RATIO_ORDER_STATUS);
	                if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ORDER_STATUS ) 
	                    viewOrderStatus();
            	}
				else if( viewHistory <= (RATIO_ORDER_HISTORY + RATIO_POINTS_HISTORY + RATIO_RESERVATION_HISTORY) ) {
	            	viewReservationHistory();
	            }
        	}//if (submitBillingAddress() == LR_PASS ) {
        }//if (selectBillingAddress() == LR_PASS ) {
    }//if (selectShippingAddress() == LR_PASS ) {

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_STORE_LOCATOR ) {
		StoreLocator();
	}
*/
	viewReservationHistory();
	
	return 0;
} // END - if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CHECKOUT_LOGIN_FIRST ) { //login first		
