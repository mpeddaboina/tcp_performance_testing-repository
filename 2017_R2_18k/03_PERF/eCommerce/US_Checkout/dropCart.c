dropCart()
{
	lr_user_data_point("Abandon Cart Rate", 1);
	lr_user_data_point("Checkout Rate", 0);
	lr_save_string("dropCartFlow", "currentFlow");
	
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CHECKOUT_GUEST ) {

		dropCartGuest();
	} 
	else {
		
		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CHECKOUT_LOGIN_FIRST ) { //login first

			dropCartLoginFirst();
 
        }
        else { //build cart first

			dropCartBuildCartFirst();
            
        }
		
	} 

	return 0;
}

dropCartGuest()
{
	buildCartDrop(0); //tempATC();
	
	inCartEdits();
	
	if (proceedAsGuest() == LR_PASS)
	{
		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= SHIP_PAGE_DROP )
		{		dropCartTransaction();
				return 0;
		}
		
		if ( submitShipping() == LR_PASS)  //former submitShippingAddressAsGuest()
		{
			if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= BILL_PAGE_DROP )
			{		dropCartTransaction();
					return 0;
			}
			if (BillingAsGuest() == LR_PASS) //submitBillingAddressAsGuest()
			{
				dropCartTransaction();
				
			}
		}
	}
	
	return 0;
}

dropCartLoginFirst()
{
	int viewHistory = atoi(lr_eval_string("{RANDOM_PERCENT}"));
	
	if (loginFromHomePage() == LR_PASS)
	{
	    viewCart();

		buildCartDrop(1);

		if ( strcmp(strupr(lr_eval_string("{buildCart}")),  "TRUE") != 0) 
			inCartEdits();

        if ( atoi(lr_eval_string("{RANDOM_WL_PERCENT}")) <= RATIO_WISHLIST )
            wishList();

		if (proceedToCheckout_ShippingView() == LR_PASS) 
		{
    	
	        if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= SHIP_PAGE_DROP )
			{		dropCartTransaction();
					return 0;
			}

			if (submitShippingRegistered() == LR_PASS)
			{
	            if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= BILL_PAGE_DROP )
				{		dropCartTransaction();
						return 0;
				}

				if (submitBillingRegistered() == LR_PASS)
					dropCartTransaction();

            }
        }

	}
	
	return 0;
}

dropCartBuildCartFirst()
{
	int viewHistory = atoi(lr_eval_string("{RANDOM_PERCENT}"));
	
    buildCartCheckout(0);//

	inCartEdits();
	
    if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= LOGIN_PAGE_DROP )
	{		dropCartTransaction();
			return 0;
	}

	if (login() == LR_PASS) 
	{
	    if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= SHIP_PAGE_DROP )
		{		dropCartTransaction();
				return 0;
		}
		if (submitShippingBrowseFirst() == LR_PASS) 
		{
	        if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= BILL_PAGE_DROP )
			{		dropCartTransaction();
					return 0;
			}
		     if (submitBillingRegistered() == LR_PASS ) {
				dropCartTransaction();
			}
		}
		            
	}

 	return 0;
}