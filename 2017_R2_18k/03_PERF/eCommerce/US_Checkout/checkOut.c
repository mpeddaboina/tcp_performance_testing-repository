checkOut()
{
	lr_user_data_point("Abandon Cart Rate", 0);
	lr_user_data_point("Checkout Rate", 1);
	lr_save_string("checkoutFlow", "currentFlow");
	
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CHECKOUT_GUEST ) {
		
		checkoutGuest();
	} 
	else {
		
  		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CHECKOUT_LOGIN_FIRST ) { //login first
			
			checkoutLoginFirst();
        }
        else { 
			
			checkoutBuildCartFirst();
        } 
	}

	return 0;
}

checkoutGuest() 
{
	buildCartCheckout(0); //tempATC();
	
	if (strcmp( lr_eval_string("{addBopisToCart}"), "true") != 0 ) 
		inCartEdits();
		
	if (proceedAsGuest() == LR_PASS)
	{
		if (strcmp( lr_eval_string("{addBopisToCart}"), "true") == 0 && strcmp( lr_eval_string("{addEcommToCart}"), "false") == 0 ) //if bopis ONLY, no need to submitShipping
		{
			if (BillingAsGuest() == LR_PASS) //submitBillingAddressAsGuest()
			{
				if( submitOrderAsGuest() == LR_PASS)
				{
					if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_REGISTER ) {
						createAccount(); // former //registerUser();
					}
					if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_STORE_LOCATOR ) {
						StoreLocator();
					}
					
					forgetPassword();	
				}
			}
		}
		else {
			if ( submitShipping() == LR_PASS)  //former submitShippingAddressAsGuest()
			{
				if (BillingAsGuest() == LR_PASS) //submitBillingAddressAsGuest()
				{
					if( submitOrderAsGuest() == LR_PASS)
					{
						if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_REGISTER ) {
							createAccount(); // former //registerUser();
						}
						if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_STORE_LOCATOR ) {
							StoreLocator();
						}
						
//						forgetPassword();	
					}
				}
			}
		}
	}
	
	return 0;
}

checkoutLoginFirst()
{
	int viewHistory = atoi(lr_eval_string("{RANDOM_PERCENT}"));
	
	if (loginFromHomePage() == LR_PASS)
	{
	    viewCart();
	    
    	buildCartCheckout(1);
    	
		if (strcmp( lr_eval_string("{addBopisToCart}"), "true") != 0 ) 
			inCartEdits();
		
	    if ( atoi(lr_eval_string("{RANDOM_WL_PERCENT}")) <= RATIO_WISHLIST )
	        wishList();
	    
	    if (proceedToCheckout_ShippingView() == LR_PASS) 
	    {
			if (strcmp( lr_eval_string("{addBopisToCart}"), "true") == 0 && strcmp( lr_eval_string("{addEcommToCart}"), "false") == 0 ) //if bopis ONLY, no need to submitShipping
			{
				if (submitBillingRegistered() == LR_PASS)
					submitOrderRegistered();
			}
			else
			{
				if (submitShippingRegistered() == LR_PASS)
				{
					if (submitBillingRegistered() == LR_PASS)
						submitOrderRegistered();
				}
			}
	    }
	    
        if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 20 ) {
        	viewOrderHistory();
        	
            if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ORDER_STATUS ) 
                viewOrderStatus();

	        if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_RESERVATION_HISTORY ) 
	           	viewReservationHistory();

//            if( viewHistory <= RATIO_POINTS_HISTORY ) {
//				viewPointsHistory();
//			}
        }
            	
	    if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_STORE_LOCATOR ) {
			StoreLocator();
		}
	    
	    if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 50 ) //1205 - need to bring down 50%
	   		newsLetterSignup();
	    
		forgetPassword();	
	    
	}
	
	return 0;
}

checkoutBuildCartFirst()
{
	int viewHistory = atoi(lr_eval_string("{RANDOM_PERCENT}"));
	
    buildCartCheckout(0);//

	inCartEdits(); 
	
	if (login() == LR_PASS) // login->Proceed to Checkout
	{
		if (strcmp( lr_eval_string("{addBopisToCart}"), "true") == 0 && strcmp( lr_eval_string("{addEcommToCart}"), "false") == 0 ) //if bopis ONLY, no need to submitShipping
		{
			if (submitBillingRegistered() == LR_PASS)
				submitOrderRegistered();
		}
		else if (submitShippingBrowseFirst() == LR_PASS)
		{
			if (submitBillingRegistered() == LR_PASS)
				submitOrderRegistered();
		}

	    if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 20 )
	    {
	    	viewOrderHistory();
	    	
	        if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ORDER_STATUS ) 
	            viewOrderStatus();
	
	        if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_RESERVATION_HISTORY ) 
	           	viewReservationHistory();
	
//	        if( viewHistory <= RATIO_POINTS_HISTORY ) {
//				viewPointsHistory();
//			}
	    }
	}
    if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_STORE_LOCATOR ) {
		StoreLocator();
	}
    
    if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 50 ) //1205 - need to bring down 50%
   		newsLetterSignup();
	   		

	return 0;

	
} // END - if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CHECKOUT_LOGIN_FIRST ) { //login first		
