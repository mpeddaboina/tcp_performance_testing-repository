int NAV_BROWSE, NAV_SEARCH, NAV_CLEARANCE, NAV_PLACE; //2nd level (T02) NAV_BY ratio
int DRILL_ONE_FACET, DRILL_TWO_FACET, DRILL_THREE_FACET, DRILL_SUB_CATEGORY; //3rd level (T03) DRILL_BY ratio
int PDP, QUICKVIEW, PRODUCT_QUICKVIEW_SERVICE, RESERVE_ONLINE, MIXED_CART_RATIO, ECOMM_CART_RATIO; //Product Display vs Product QuickView ratio (T04)
int APPLY_SORT, RATIO_TCPSkuSelectionView; //Sort ratio
int APPLY_PAGINATE, RATIO_STORE_LOCATOR, RATIO_SEARCH_SUGGEST, API_SUB_TRANSACTION_SWITCH; //Pagination ratio //USE_LOW_INVENTORY, 

	//set 2nd level navigation ratio for T02 transaction. Sum must be 100. Valid value is 0 to 100.

	NAV_BROWSE = 95; //45; //46; 
	NAV_SEARCH = 5; //8; 
	NAV_CLEARANCE = 0; //50; //45;
	NAV_PLACE = 0; //1;

	//set drill navigation ratio for T03 transaction. Sum must be 100. Valid value is 0 to 100.
	DRILL_ONE_FACET = 35; //25;
	DRILL_TWO_FACET = 10; //15;
	DRILL_THREE_FACET = 5; //10;
	DRILL_SUB_CATEGORY = 50;

	//set the % for SORT during browse activity. Valid value is 0 to 100. Used only in Browse scripts,
	APPLY_SORT = 25;

	//set the % for PAGINATION during browse activity, this will apply pagination only if additional pages are available. Valid value is 0 to 100. Used only in Browse scripts.
	APPLY_PAGINATE = 75; //50; //100;

	//set PDP vs PQV view ratio (T04). Sum must be 100. Valid value is 0 to 100.
	//82/18 as per the 11/30/2015 Omniture data
	PDP = 67; //82;
	QUICKVIEW = 33; //18;
	
	RATIO_STORE_LOCATOR  = 5;
	RATIO_SEARCH_SUGGEST = 0; //50; //Did You Mean? descoped on 06/15
	RATIO_TCPSkuSelectionView = 20;	

	PRODUCT_QUICKVIEW_SERVICE = 15; //30; //50;
	RESERVE_ONLINE = 100; //ropis
	ECOMM_CART_RATIO = 95;
	BOPIS_CART_RATIO = 5;//
	API_SUB_TRANSACTION_SWITCH = 1; // 1 = ON; 0 = OFF
