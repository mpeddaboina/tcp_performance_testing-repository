int NAV_BROWSE, NAV_SEARCH, NAV_CLEARANCE, NAV_PLACE; //2nd level (T02) NAV_BY ratio
int DRILL_ONE_FACET, DRILL_TWO_FACET, DRILL_THREE_FACET, DRILL_SUB_CATEGORY; //3rd level (T03) DRILL_BY ratio
int PDP, QUICKVIEW; //Product Display vs Product QuickView ratio (T04)
int APPLY_SORT; //Sort ratio
int APPLY_PAGINATE; //Pagination ratio
int CART_PAGE_DROP, LOGIN_PAGE_DROP, SHIP_PAGE_DROP, BILL_PAGE_DROP, REVIEW_PAGE_DROP; //Checkout Funnel ratio for DropCart Scripts.
int LINK_TT, FORM_TT, TYPE_SPEED; //Think Time
int RATIO_CHECKOUT_GUEST, RATIO_CHECKOUT_LOGIN, RATIO_CHECKOUT_LOGIN_FIRST; // User Type
int OFFLINE_PLUGIN, VISA, MASTER, AMEX, PLCC, GIFT, DISCOVER; // Payment Method
int RATIO_PROMO_APPLY, RATIO_PROMO_MULTIUSE, RATIO_PROMO_SINGLEUSE, RATIO_PROMO_COUPON_REMOVE; // Promo Type 
int RATIO_REGISTER, RATIO_UPDATE_QUANTITY, RATIO_DELETE_ITEM, RATIO_SELECT_COLOR, RATIO_ORDER_HISTORY;// Registration, Update_Quantity, Delete_Item from cart
int RATIO_CART_MERGE, RATIO_DROP_CART, RATIO_WISHLIST, cartMerge, currentCartSize;
int	RATIO_WL_VIEW, RATIO_WL_CREATE,	RATIO_WL_DELETE, RATIO_WL_CHANGE, RATIO_WL_DELETE_ITEM, RATIO_WL_ADD_ITEM, RATIO_WL_ADD_TO_CART;

	//set 2nd level navigation ratio for T02 transaction. Sum must be 100. Valid value is 0 to 100.
	NAV_BROWSE = 47;
	NAV_SEARCH = 3;
	NAV_CLEARANCE = 50;
	NAV_PLACE = 0;

	//set drill navigation ratio for T03 transaction. Sum must be 100. Valid value is 0 to 100.
	DRILL_ONE_FACET = 35;
	DRILL_TWO_FACET = 10;
	DRILL_THREE_FACET = 5;
	DRILL_SUB_CATEGORY = 50;

	//set the % for SORT during browse activity. Valid value is 0 to 100. Used only in Browse scripts,
	APPLY_SORT = 25;

	//set the % for PAGINATION during browse activity, this will apply pagination only if additional pages are available. Valid value is 0 to 100. Used only in Browse scripts.
	APPLY_PAGINATE = 100;

	//set PDP vs PQV view ratio (T04). Sum must be 100. Valid value is 0 to 100.
	PDP = 67;
	QUICKVIEW = 33;

	//set drop point ratio for checkout funnel. Sum must be 100. All variables must either 0 or +ve value.
	LOGIN_PAGE_DROP = 11;
	SHIP_PAGE_DROP = 46;
	BILL_PAGE_DROP = 13;
	REVIEW_PAGE_DROP = 100;

	//set global think time values for link clicks, form entry pages and type speed for typeAhead search.
	LINK_TT = 10;
	FORM_TT = 10;
	TYPE_SPEED = 1;

	//set user checkout type T10 transaction. Sum must be 100. All variables must have either 0 or +ve value.
	RATIO_CHECKOUT_LOGIN = 70;
	RATIO_CHECKOUT_GUEST = 30;

    //set users checkout that starts with login flow
    RATIO_CHECKOUT_LOGIN_FIRST = 50;

	//PayMethod
	OFFLINE_PLUGIN = 100;
	VISA = 0;
	MASTER = 0;
	AMEX = 0;
	PLCC = 0;
	GIFT = 0;
	DISCOVER = 0;

	//PromoType
	RATIO_PROMO_APPLY = 100; //governs if a promo is to be applied. Valid value is 0 to 100.
	RATIO_PROMO_MULTIUSE = 100; // if a promo is to be applied, governs what % is multiuse. Sum of RATIO_PROMO_MULTIUSE and RATIO_PROMO_SINGLEUSE must be 100. All variables must have either 0 or +ve value.
	RATIO_PROMO_SINGLEUSE = 0; // if a promo is to be applied, governs what % is singleuse. Sum of RATIO_PROMO_MULTIUSE and RATIO_PROMO_SINGLEUSE must be 100. All variables must have either 0 or +ve value.
	RATIO_PROMO_COUPON_REMOVE = 10; //governs what % of carts will remove coupon. Valid value is 0 to 100.
	/*
	Follwing variables values change for each script and therefore are defined as a Parameter in the script.
	RATIO_AVG_CART_SIZE
	*/

//% of Guest user completing checkout that also register
	RATIO_REGISTER = 25; //80;

	//Ratio for Update Quantity
	RATIO_UPDATE_QUANTITY = 100;

	//Ratio for Delete an item from cart
	RATIO_DELETE_ITEM = 100; //60;

	//Ratio for Delete an item from cart
	RATIO_SELECT_COLOR = 50;

	//Ratio for Order history / View Order Detail
	RATIO_ORDER_HISTORY = 50;
	RATIO_ORDER_STATUS = 50;

	//Ratio for Cart Merge
	//RATIO_CART_MERGE = 50;

	RATIO_DROP_CART = 45; //40;

	//Ratio for Wishlist
	RATIO_WISHLIST		 = 50; //50; //25; //12; //8;

	RATIO_WL_CREATE      = 60; //30
	RATIO_WL_DELETE      = 2; //5
	RATIO_WL_CHANGE      = 3; //10
	RATIO_WL_DELETE_ITEM = 5; //5
	RATIO_WL_ADD_ITEM    = 25; //25
//	RATIO_WL_ADD_TO_CART = 5; //15 //25

