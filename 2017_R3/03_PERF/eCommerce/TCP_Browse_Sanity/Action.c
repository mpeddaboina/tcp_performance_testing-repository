Action()
{
	int j, jLoop, k, kLoop, jCounter, kCounter;
	web_set_max_html_param_len ( "3072" ) ;
	web_cleanup_cookies ( ) ;
	lr_save_string ( lr_get_attrib_string ( "TestEnvironment") , "host" ) ; 
//	lr_message("LINK_TT=%d, FORM_TT=%d, TYPE_SPEED=%d,", LINK_TT, FORM_TT, TYPE_SPEED);
//	NAV_BROWSE = 100;
//	NAV_SEARCH = 0; //
//	NAV_CLEARANCE = 0;
//	NAV_PLACE = 0;
	
//	lr_save_string("10151", "storeId");
//	lr_save_string("10551", "catalogId");
	
	if ( atoi( lr_eval_string("{RANDOM_PERCENT}")) <= lr_get_attrib_long("US_Traffic_Percentage") ) {
		lr_save_string( "/shop/us/", "store_home_page" );
		lr_save_string("10151", "storeId");
		lr_save_string("10551", "catalogId");
	} else {
		lr_save_string( "/shop/ca/", "store_home_page" );
		lr_save_string("10152", "storeId");
		lr_save_string("10552", "catalogId");
	}
	
	viewHomePage();

	//Check Categories
	//writeToFile(0);
	jLoop = lr_paramarr_len( "categories" );
	if (jLoop != 0) {
		jCounter++;
		for (j=1; j < jLoop; j++) {
			
			lr_param_sprintf ( "cateogryPageUrl" , "http://%s%ssearch%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}") , lr_paramarr_idx ( "categories" , j ) ) ;
			//lr_param_sprintf ( "cateogryPageUrl" , "http://%s/shop/CategoryDisplay?%s" , lr_eval_string("{host}"),  lr_paramarr_idx ( "categories" , j ) ) ;
			//http://tcp-perf.childrensplace.com/shop/CategoryDisplay?urlRequestType=Base&catalogId=10551&categoryId=47511&pageView=image&urlLangId=-1&beginIndex=0&langId=-1&top=Y&storeId=10151
			web_reg_find("SaveCount=noProducts", "Text/IC=We're sorry, there are no products available for purchase at this time. Please check back later.", LAST);
			web_reg_save_param("noProductPage", "LB=stringArray = '", "RB='.split", "NotFound=Warning", LAST);
			categoryPageCorrelations();
			
			web_url ( "Check Categories" ,
		    	     "URL={cateogryPageUrl}" ,
		             "Resource=0" ,
		             "Mode=HTML" ,
		             LAST ) ;
			
			lr_think_time(3);
			if(atoi(lr_eval_string("{noProducts}")) == 1) {
				lr_save_string("Categories", "HeaderName");
				lr_save_int(jCounter++, "iCounter");
				writeToFile(1);
			}
			else {
				//Check Sub-Categories
				//checkSubCategories();
				//writeToFile(0);
				kLoop = lr_paramarr_len( "subcategories" );
				if (kLoop != 0) {
					kCounter++;
					for (k=1; k < kLoop; k++) {
						
						lr_param_sprintf ( "subCateogryPageUrl" , "http://%s%s" , lr_eval_string("{host}") , lr_paramarr_idx ( "subcategories" , k ) ) ;
						
						web_reg_find("SaveCount=noProducts", "Text/IC=We're sorry, there are no products available for purchase at this time. Please check back later.", LAST);
						web_reg_save_param("noProductPage", "LB=stringArray = '", "RB='.split", "NotFound=Warning", LAST);
						categoryPageCorrelations();
						
						web_url ( "Check Sub-Categories" ,
					    	     "URL={subCateogryPageUrl}" ,
					             "Resource=0" ,
					             "Mode=HTML" ,
					             LAST ) ;
						
						lr_think_time(3);
						if(atoi(lr_eval_string("{noProducts}")) == 1) {
							lr_output_message("PavanDusi: BAD URL %s", lr_eval_string("{subCateogryPageUrl}"));
							lr_save_string("Sub-Categories", "HeaderName");
							lr_save_int(kCounter++, "iCounter");
							writeToFile(1);
						}else{
							lr_output_message("PavanDusi: GOOD URL %s", lr_eval_string("{subCateogryPageUrl}"));
						}
					}
				}
			}
		}
	}

			
	//Check Clearances
	lr_save_string("Clearances", "HeaderName");
	//writeToFile(0);
	jLoop = lr_paramarr_len( "clearances" );
	if (jLoop != 0) {
		jCounter++;
		for (j=1; j < jLoop; j++) {
			
			lr_param_sprintf ( "clearancePageUrl" , "http://%s/shop/SearchDisplay?%s" , lr_eval_string("{host}"),  lr_paramarr_idx ( "clearances" , j) ) ;
			
			web_reg_find("SaveCount=noProducts", "Text/IC=We're sorry, there are no products available for purchase at this time. Please check back later.", LAST);
			web_reg_save_param("noProductPage", "LB=stringArray = '", "RB='.split", "NotFound=Warning", LAST);
			
			web_url ( "Check Clearance" ,
		    	     "URL={clearancePageUrl}" ,
		             "Resource=0" ,
		             "Mode=HTML" ,
		             LAST ) ;
			
			lr_think_time(3);
			if(atoi(lr_eval_string("{noProducts}")) == 1) {
				lr_save_int(jCounter++, "iCounter");
				writeToFile(1);
			}
		}
	}
		
	return 0;
}

writeToFile(int header) 
{

    char fullpath[1024];
	long file1;
	char *filename1 = "e:\\Performance\\Scripts\\2017\\CA_Perflive_pageWithNoResult.txt";
	strcpy(fullpath, filename1);
       
    if ((file1 = fopen(fullpath, "a+")) == NULL) {
        lr_output_message ("Unable to open %s", fullpath);
        return -1;
    }
//	if (header == 0)
//		fprintf(file1, "%s\n", lr_eval_string("{HeaderName}"));
//	else
		fprintf(file1, "%s\n", lr_eval_string("{HeaderName}->{noProductPage}"));
		//fprintf(file1, "%s\n", lr_eval_string("{noProductPage}"));
	
    fclose(file1);
    
	return 0;
}
