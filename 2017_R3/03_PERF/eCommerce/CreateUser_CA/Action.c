Action()
{
    lr_save_string ("api-perf.childrensplace.com/v3", "api_host");
	lr_save_string("10152", "storeId");
	lr_save_string("10552", "catalogId");

    lr_save_string (lr_eval_string ("TCPPERFV_CAD_{ts}@childrensplace.com"), "emailAddress");

	web_set_sockets_option("SSL_VERSION", "TLS");
	lr_start_transaction("Register CA");

	addHeader();
	web_custom_request("addCustomerRegistration", 
		"URL=https://{api_host}/addCustomerRegistration", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Mode=HTTP", 
		"EncType=application/json", 
		"Body={\"firstName\":\"Joe\",\"lastName\":\"User\",\"zipCode\":\"M5B2H1\",\"logonId\":\"{emailAddress}\",\"logonPassword\":\"Asdf!234\",\"phone1\":\"2014531513\",\"rememberCheck\":true,\"rememberMe\":true,\"catalogId\":\"10552\",\"langId\":\"-1\",\"storeId\":\"10152\"}", 
		LAST);
		lr_end_transaction("Register CA", LR_AUTO);

	addHeader();
	web_reg_find("TEXT/IC=LoyaltyWebsiteInd", "SaveCount=apiCheck", LAST);
	web_custom_request("getPointsService", 
		"URL=https://{api_host}/payment/getPointsService", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		LAST);
		
	addHeader();
	web_reg_find("TEXT/IC=resourceName", "SaveCount=apiCheck", LAST);
	web_custom_request("getRegisteredUserDetailsInfo", 
		 "URL=https://{api_host}/payment/getRegisteredUserDetailsInfo", 
		 "Method=GET", 
		 "Resource=0", 
		 "RecContentType=application/json", 
		 "Mode=HTML", 
		 "EncType=application/json", 			
		 LAST);

	login();
	
addAddress();	
	
	return 0;
	
	
/*
	web_url("uatlive3.childrensplace.com", 
		"URL=http://uatlive3.childrensplace.com/", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=", 
		"Snapshot=t6.inf", 
		"Mode=HTML", 
		LAST);

	web_url("LogonForm", 
		"URL=https://uatlive3.childrensplace.com/shop/LogonForm?new=Y&catalogId=10552&myAcctMain=&langId=-1&storeId=10152", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=http://uatlive3.childrensplace.com/shop/us/home", 
		"Snapshot=t21.inf", 
		"Mode=HTML", 
		LAST);

	lr_think_time(26);

	web_submit_data("TCPAjaxEmailVerificationCmd", 
		"Action=https://uatlive3.childrensplace.com/webapp/wcs/stores/servlet/TCPAjaxEmailVerificationCmd", 
		"Method=POST", 
		"RecContentType=application/json", 
		"Referer=https://uatlive3.childrensplace.com/shop/LogonForm?new=Y&catalogId=10552&myAcctMain=&langId=-1&storeId=10152", 
		"Snapshot=t29.inf", 
		"Mode=HTML", 
		"EncodeAtSign=YES", 
		ITEMDATA, 
		"Name=email", "Value={emailAddress}", ENDITEM, //emailAddress
//		"Name=email", "Value=TCPPERF_US2_JOE1234@CHILDRENSPLACE.COM", ENDITEM, //emailAddress
		"Name=page", "Value=registration", ENDITEM, 
		"Name=requesttype", "Value=ajax", ENDITEM, 
		LAST);
	
	web_submit_data("PersonProcessServicePersonRegister_2", 
		"Action=https://uatlive3.childrensplace.com/shop/PersonProcessServicePersonRegister", 
		"Method=POST", 
		"RecContentType=text/html", 
		"Referer=https://uatlive3.childrensplace.com/shop/PersonProcessServicePersonRegister", 
		"Snapshot=t37.inf", 
		"Mode=HTML", 
		"EncodeAtSign=YES", 
		ITEMDATA, 
		"Name=myAcctMain", "Value=", ENDITEM, 
		"Name=new", "Value=Y", ENDITEM, 
		"Name=storeId", "Value=10152", ENDITEM, 
		"Name=catalogId", "Value=10552", ENDITEM, 
		"Name=URL", "Value=Logon?reLogonURL=LogonForm&storeId=10152&catalogId=10552&langId=&URL=TCPAccountVerifyView&from=Register&Country=", ENDITEM, 
		"Name=receiveSMS", "Value=false", ENDITEM, 
		"Name=addressType", "Value=ShippingAndBilling", ENDITEM, 
		"Name=errorViewName", "Value=LogonForm", ENDITEM, 
		"Name=rememberMe", "Value=true", ENDITEM, 
		"Name=page", "Value=account", ENDITEM, 
		"Name=registerType", "Value=R", ENDITEM, 
		"Name=primary", "Value=true", ENDITEM, 
		"Name=profileType", "Value=Consumer", ENDITEM, 
		"Name=RegistrationApprovalStatus", "Value=1", ENDITEM, 
		"Name=myAcctMain", "Value=1", ENDITEM, 
		"Name=new", "Value=Y", ENDITEM, 
		"Name=storeId", "Value=10152", ENDITEM, 
		"Name=catalogId", "Value=10552", ENDITEM, 
		"Name=firstName", "Value=JOE", ENDITEM, 
		"Name=lastName", "Value=USER", ENDITEM, 
		"Name=logonId", "Value={emailAddress}", ENDITEM, 
		"Name=email1Verify", "Value={emailAddress}", ENDITEM, 
		"Name=logonPassword", "Value=Asdf!234", ENDITEM, 
		"Name=logonPasswordVerify", "Value=Asdf!234", ENDITEM, 
		"Name=zipCode", "Value=M1V1X9", ENDITEM, 
		"Name=storeName", "Value=The Children's Place", ENDITEM, 
		"Name=country", "Value=", ENDITEM, 
		"Name=addressField3", "Value=M1V1X9", ENDITEM, 
		"Name=phone1", "Value=2014537616", ENDITEM, 
		"Name=response", "Value=accept_all::false:false", ENDITEM, 
		LAST);

	web_url("AjaxLogonForm", 
		"URL=https://uatlive3.childrensplace.com/shop/AjaxLogonForm?catalogId=10552&myAcctMain=1&langId=-1&storeId=10152", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=https://uatlive3.childrensplace.com/webapp/wcs/stores/servlet/TCPAccountVerifyView?catalogId=10552&storeId=10152&langId=-1&krypto="
		"ViygN7Y0dkIyTIuhDc0vk7HEdlJjDgHd5g3e%2BOz32KVbClTv0YUv8%2FKaoZDnXIFnLZ7%2BgJvUlfRFQHhRHooT4PCQSaYmcX%2F23p9z3efLxQmRxpxqtxsRiat56mvgQ1ZkMj6tAgS26ITwCUbIppYMF2%2BieNGpFa8JeBUiJTrPMk592rFGoKeJDD64es1QfKYuF9qgJPNGN%2FGrOjBsKXwBR2M7YXJBIeSKgrxHaYSXVrLhWmsUeAFaDpNonxRbw%2FoXHiAHy%2F%2F2p6ickFuc6%2FspwRAVRaVRpbpIVP2Zz7Bkl%2Fc85xkXEd%2BuOhgLTWq0XwpOKvoJswtbJos0G8tKBoiBsO3oTR%2FVabo5Yzadx7loHAcPFdWjeWKf8JVxS62BdYG64XpgOJ9de6fl03nPEHl0E0npuiXO%2BLixXxvNFXE%2By8R7Wy4Izc3IZ6EJc%2FfMlq9oO%2FGMzx7fiKF9UN1P32tu"
		"sc4R2lTo7UGqPmc5cpycgxAwbEjBc8pm7to0gmjNiQ0AOTKRFVxxYdfuopEySfri9IqofkYpPFT%2FZuqx9Wbien5DKEsjLnpvtnd2aM4ODoGQDkUcsPG9O9eNgb7B4fRgDkCsMguTUMKPPjXdOZoePxzzR8IhqSaj3ZKhoVK1egjB0UKtA18pHQAWOb9HyjHtwmNDZhjpl%2Bn%2BcnDXRQNOE3%2FpOR0E2paEllNNGT70EEx0ewE69s1W%2FEoCvMZMzRiYTfZe6JvKdAkNSgi%2B%2BCzE1K9O6C1XF%2Frp0%2BQEHH%2FCbiMQQwQySmFGIzs2uulgveFQEwHjBWEDlhedIR59ME4JrxOrk9dOPkQyl4XF4O%2BXTronqh%2Fs43rkwDv9e9%2FJRQnRhdUGUfXDpz5G7oMfxzfbvfVq5XCTtx1Ek3Vyapgja%2Fse%2Fqu3VQzbN7Xc1hzViI7vXWhqQQPMFHGI0uiw%2BzYTGx"
		"F4p4s9SDY73RuT4ZiV2dnVszx2tduK41KyixAhy2%2FAjVtGQY6u9KD9yvEZk9jMKdS33ftzb3EFyctze9IzV70DkbbaNCrpbJ3MBGshrxh1X67cuhFvJzCWkJ9oGB%2B2XIzPUUiquFrIQe9NseHqM%2FCllcRBq%2FCGQo0ban7W6u9FyvVw6rkteTzMIK1Z%2FKTgFsi5Zz9X737U4iaiA1XB9SghN0rRdRB8dPUDMIBj6jH1pGsw5S1fcCQe1eEPR7l0Y638Rsqs7EzZ9LihYJKGhglDJbD0yHwL5SWjWO4RlpxeXdOtxN1VLWCz1zMy71x6B4PIB1AtPOSSohNyY77%2FCdoJGoOC%2BXrwPhdG0%2B2309uyyhDYfygpFY5gHGni%2FmR6Skw%3D&ddkey=https%3APersonProcessServicePersonRegister", 
		"Snapshot=t41.inf", 
		"Mode=HTML", 
		LAST);


	web_url("TCPAddCreditCardView", 
		"URL=https://uatlive3.childrensplace.com/shop/TCPAddCreditCardView?catalogId=10552&langId=-1&storeId=10152", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=https://uatlive3.childrensplace.com/shop/AjaxLogonForm?catalogId=10552&myAcctMain=1&langId=-1&storeId=10152", 
		"Snapshot=t52.inf", 
		"Mode=HTML", 
		LAST);

	web_url("TCPAVSResponseView", 
		"URL=https://uatlive3.childrensplace.com/webapp/wcs/stores/servlet/TCPAVSResponseView?callback=TCPAVSHandler&storeId=10152&catalogId=10552&langId=-1&company=&firstName=JOE&lastName=USER&address1=46+Oakhaven+Dr&address2=&city=Scarborough&state=ON&country=US&zip=M1V1X9&suite=&cvsLocalEndPoint=http%3A%2F%2F10.2.0.222%3A9082%2Fadv%2Fservices%2FAddressVerification&action=addressSearch&formpart=TCPsaveProfileUpdate-profileUpdate", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=https://uatlive3.childrensplace.com/shop/AjaxLogonForm?catalogId=10552&myAcctMain=1&langId=-1&storeId=10152", 
		"Snapshot=t56.inf", 
		"Mode=HTML", 
		LAST);

	web_custom_request("AjaxCreditCardProcess", 
		"URL=https://uatlive3.childrensplace.com/webapp/wcs/stores/servlet/AjaxCreditCardProcess", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=https://uatlive3.childrensplace.com/shop/AjaxLogonForm?catalogId=10552&myAcctMain=1&langId=-1&storeId=10152", 
		"Snapshot=t58.inf", 
		"Mode=HTML", 
		"EncType=application/x-www-form-urlencoded; charset=UTF-8", 
		"Body=&storeId=10152&catalogId=10552&langId=-1&addressId=&creditCardId=&storeId=10152&langId=-1&catalogId=10552&page=quickcheckout&returnPage=&authToken=263463020%252CCr%252BZnxdxf6PffZpRFeeBXfNVivw%253D&orderId=&valueFromProfileOrder=Y&URL=AjaxLogonForm&billing_addressType=Billing&billing_nickName=Billing_10152_Thu+Feb+09+15%3A45%3A34+EST+2017&pay_payment_method=COMPASSVISA%7CVISA&pay_payMethodId=COMPASSVISA%7CVISA&pay_cc_brand=COMPASSVISA%7CVISA&billing_addressField3=M1V1X9&billing_country=US&"
		"logonId={emailAddress}&chosenLocale=en&billing_firstName=JOE&billing_lastName=USER&billing_address1=46+Oakhaven+Dr&billing_address2=&billing_city=Scarborough&billing_state=ON&addressField3=M1V1X9&billing_zipCode=M1V1X9&country=US&billing_phone1=2014537616&curr_year=2017&curr_month=2&curr_date=9&payMethodId=COMPASSVISA%7CVISA&pay_account=4012000033330026&pay_expire_month=12&pay_expire_year=2017", 
		LAST);//TCPPERF_US2_JOE1234%40CHILDRENSPLACE.COM
//GUEST_ZIP,GUEST_ADR1,GUEST_CITY,GUEST_STATE,GUEST_COUNTRY
//M1V1X9,46 Oakhaven Dr,Scarborough,ON,CA

	web_custom_request("TCPMyPaymentInfoDisplayView", 
		"URL=https://uatlive3.childrensplace.com/webapp/wcs/stores/servlet/TCPMyPaymentInfoDisplayView", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=https://uatlive3.childrensplace.com/shop/AjaxLogonForm?catalogId=10552&myAcctMain=1&langId=-1&storeId=10152", 
		"Snapshot=t59.inf", 
		"Mode=HTML", 
		"EncType=application/x-www-form-urlencoded; charset=UTF-8", 
		"Body=&storeId=10152&catalogId=10552&langId=-1&", 
		LAST);

	return 0;
*/
}



void addHeader()
{
	web_add_header("storeId", "10152" );
	web_add_header("catalogId", "10552" );
	web_add_header("langId", "-1");
	
}

void addAddress()
{
//	M5B2H1,220 YONGE STREET,TORONTO,ON,CA
	addHeader();
	web_custom_request("addAddress",
		"URL=https://{api_host}/payment/addAddress", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Mode=HTTP", 
		"EncType=application/json",
		"Body={\"contact\":[{\"addressLine\":[\"220 YONGE STREET\",\"\",\"\"],\"attributes\":[{\"key\":\"addressField3\",\"value\":\"M5B2H1\"}],\"addressType\":\"ShippingAndBilling\",\"city\":\"TORONTO\",\"country\":\"CA\",\"firstName\":\"Joe\",\"lastName\":\"User\",\"nickName\":\"{nickName}\",\"phone1\":\"2014531513\",\"email1\":\"{emailAddress}\",\"phone1Publish\":\"false\",\"primary\":\"false\",\"state\":\"ON\",\"zipCode\":\"M5B2H1\",\"xcont_addressField3\":\"M5B2H1\",\"fromPage\":\"\"}]}", 
		LAST);
}

void login()
{
	addHeader();

lr_start_transaction("Login Canada");
	web_custom_request("logon", 
		"URL=https://{api_host}/logon", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Mode=HTML", 
		"EncType=application/json",	"Body={\"storeId\":\"{storeId}\",\"logonId1\":\"{emailAddress}\",\"logonPassword1\":\"Asdf!234\",\"rememberCheck\":false,\"rememberMe\":false,\"requesttype\":\"ajax\",\"reLogonURL\":\"TCPAjaxLogonErrorView\",\"URL\":\"TCPAjaxLogonSuccessView\",\"registryAccessPreference\":\"Public\",\"calculationUsageId\":-1,\"createIfEmpty\":1,\"deleteIfEmpty\":\"*\",\"fromOrderId\":\"*\",\"toOrderId\":\".\",\"updatePrices\":0}", 
		LAST);
lr_end_transaction("Login Canada", LR_AUTO);
	
lr_think_time(5);
	
	addHeader();
	web_custom_request("getRegisteredUserDetailsInfo", 
		"URL=https://{api_host}/payment/getRegisteredUserDetailsInfo", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Mode=HTML", 
		"EncType=application/json", 			
		LAST);

	addHeader();
	web_add_header("locStore", "True");
	web_add_header("pageName", "fullOrderInfo");
	
	web_custom_request("getOrderDetails", 
		"URL=https://{api_host}/getOrderDetails", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Mode=HTML", 
		"EncType=application/json", 			
		LAST);

	addHeader();
	web_custom_request("getCoupon", 
		"Method=GET", 
		"URL=https://{api_host}/payment/getCoupon", 
		"Resource=0", 
		"RecContentType=application/json", 
		LAST);

	addHeader();
		
	web_reg_find("TEXT/IC=LoyaltyWebsiteInd", "SaveCount=apiCheck", LAST);
	web_custom_request("getPointsService", 
		"Method=GET", 
		"URL=https://{api_host}/payment/getPointsService", 
		"Resource=0", 
		"RecContentType=application/json", 
		LAST);
	
}