#ifndef _GLOBALS_H
#define _GLOBALS_H

//--------------------------------------------------------------------
// Include Files
#include "lrun.h"
#include "web_api.h"
#include "lrw_custom_body.h"

//--------------------------------------------------------------------
// Global Variables
int LINK_TT, FORM_TT, TYPE_SPEED; //Think Time
	//set global think time values for link clicks, form entry pages and type speed for typeAhead search.
	LINK_TT = 10;
	FORM_TT = 10;
	TYPE_SPEED = 1;
int authcookielen , authtokenlen ;
authcookielen = 0;	
int orderItemIdscount = 0;

#endif // _GLOBALS_H