this["App"] = this["App"] || {};
this["App"]["Templates"] = this["App"]["Templates"] || {};

this["App"]["Templates"]["confirm"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  
  return "<div class=\"modal confirm\"><span class=\"read-only\">Are you sure?</span> ";
  }

function program3(depth0,data) {
  
  
  return "<div class=\"confirm\">";
  }

function program5(depth0,data) {
  
  
  return " <a href=\"#close\" class=\"close\"></a> ";
  }

function program7(depth0,data) {
  
  var buffer = "", stack1;
  buffer += " <a href=\"#no\" class=\"confirm-no\">";
  if (stack1 = helpers.noLabel) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.noLabel); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + "</a> <a href=\"#yes\" class=\"confirm-yes\">";
  if (stack1 = helpers.yesLabel) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.yesLabel); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + "</a> ";
  return buffer;
  }

  stack1 = helpers['if'].call(depth0, (depth0 && depth0.isModal), {hash:{},inverse:self.program(3, program3, data),fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " ";
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.isModal), {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "<p>";
  if (stack1 = helpers.message) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.message); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</p><p>";
  stack1 = helpers.unless.call(depth0, (depth0 && depth0.isOptionEnabled), {hash:{},inverse:self.noop,fn:self.program(7, program7, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</p></div></div>";
  return buffer;
  });

this["App"]["Templates"]["product-simplified"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, stack2, options, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, self=this, functionType="function";

function program1(depth0,data,depth1) {
  
  var buffer = "", stack1, stack2, options;
  buffer += " ";
  options = {hash:{},inverse:self.noop,fn:self.programWithDepth(2, program2, data, depth1),data:data};
  stack2 = ((stack1 = helpers.compare || (depth0 && depth0.compare)),stack1 ? stack1.call(depth0, (depth0 && depth0.requestedQuantity), ">", (depth0 && depth0.quantityBought), options) : helperMissing.call(depth0, "compare", (depth0 && depth0.requestedQuantity), ">", (depth0 && depth0.quantityBought), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " ";
  return buffer;
  }
function program2(depth0,data,depth2) {
  
  var buffer = "", stack1, options;
  buffer += " ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.increase || (depth2 && depth2.increase)),stack1 ? stack1.call(depth0, depth2, "countUnboughtElements", options) : helperMissing.call(depth0, "increase", depth2, "countUnboughtElements", options)))
    + " ";
  return buffer;
  }

function program4(depth0,data) {
  
  
  return "<ol><li>Price Each</li><li>Quantity</li></ol>";
  }

function program6(depth0,data,depth1) {
  
  var buffer = "", stack1, stack2, options;
  buffer += " ";
  options = {hash:{},inverse:self.programWithDepth(17, program17, data, depth1),fn:self.program(7, program7, data),data:data};
  stack2 = ((stack1 = helpers.compare || (depth0 && depth0.compare)),stack1 ? stack1.call(depth0, (depth0 && depth0.requestedQuantity), ">", (depth0 && depth0.quantityBought), options) : helperMissing.call(depth0, "compare", (depth0 && depth0.requestedQuantity), ">", (depth0 && depth0.quantityBought), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " ";
  return buffer;
  }
function program7(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "<article itemprop=\"itemListElement\" itemscope=\"\" itemtype=\"http://schema.org/Product\" data-product-id=\"";
  if (stack1 = helpers.giftListItemID) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.giftListItemID); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" data-item-id=\"";
  if (stack1 = helpers.wishListItemId) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.wishListItemId); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" data-catentryid=\"";
  if (stack1 = helpers.itemId) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.itemId); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" data-qty-requested=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.parseInt || (depth0 && depth0.parseInt)),stack1 ? stack1.call(depth0, (depth0 && depth0.requestedQuantity), options) : helperMissing.call(depth0, "parseInt", (depth0 && depth0.requestedQuantity), options)))
    + "\" data-qty-bought=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.parseInt || (depth0 && depth0.parseInt)),stack1 ? stack1.call(depth0, (depth0 && depth0.quantityBought), options) : helperMissing.call(depth0, "parseInt", (depth0 && depth0.quantityBought), options)))
    + "\" data-size=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.getSize || (depth0 && depth0.getSize)),stack1 ? stack1.call(depth0, (depth0 && depth0.size), (depth0 && depth0.itemId), options) : helperMissing.call(depth0, "getSize", (depth0 && depth0.size), (depth0 && depth0.itemId), options)))
    + "\" data-fit=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.getFit || (depth0 && depth0.getFit)),stack1 ? stack1.call(depth0, (depth0 && depth0.size), (depth0 && depth0.itemId), options) : helperMissing.call(depth0, "getFit", (depth0 && depth0.size), (depth0 && depth0.itemId), options)))
    + "\"><figure>";
  options = {hash:{},inverse:self.noop,fn:self.program(8, program8, data),data:data};
  stack2 = ((stack1 = helpers.compare || (depth0 && depth0.compare)),stack1 ? stack1.call(depth0, (depth0 && depth0.webOnlyFlagUSStore), "==", "1", options) : helperMissing.call(depth0, "compare", (depth0 && depth0.webOnlyFlagUSStore), "==", "1", options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " <a href=\"";
  if (stack2 = helpers.productURL) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = (depth0 && depth0.productURL); stack2 = typeof stack2 === functionType ? stack2.call(depth0, {hash:{},data:data}) : stack2; }
  buffer += escapeExpression(stack2)
    + "\"><img itemprop=\"image\" alt=\"";
  if (stack2 = helpers.productName) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = (depth0 && depth0.productName); stack2 = typeof stack2 === functionType ? stack2.call(depth0, {hash:{},data:data}) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" src=\"/wcsstore/GlobalSAS/images/tcp/products/500/";
  if (stack2 = helpers.thumbnail) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = (depth0 && depth0.thumbnail); stack2 = typeof stack2 === functionType ? stack2.call(depth0, {hash:{},data:data}) : stack2; }
  buffer += escapeExpression(stack2)
    + ".jpg\"></a><figcaption></figcaption></figure><div class=\"info\"><h2 itemprop=\"name\"><a href=\"";
  if (stack2 = helpers.productURL) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = (depth0 && depth0.productURL); stack2 = typeof stack2 === functionType ? stack2.call(depth0, {hash:{},data:data}) : stack2; }
  buffer += escapeExpression(stack2)
    + "\">";
  if (stack2 = helpers.productName) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = (depth0 && depth0.productName); stack2 = typeof stack2 === functionType ? stack2.call(depth0, {hash:{},data:data}) : stack2; }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</a></h2><p itemprop=\"description\" class=\"product-details\"><strong>Size:</strong> ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.getSize || (depth0 && depth0.getSize)),stack1 ? stack1.call(depth0, (depth0 && depth0.size), (depth0 && depth0.itemId), options) : helperMissing.call(depth0, "getSize", (depth0 && depth0.size), (depth0 && depth0.itemId), options)))
    + "<br><strong>Color:</strong> ";
  if (stack2 = helpers.color) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = (depth0 && depth0.color); stack2 = typeof stack2 === functionType ? stack2.call(depth0, {hash:{},data:data}) : stack2; }
  buffer += escapeExpression(stack2)
    + " <img src=\"/wcsstore/GlobalSAS/images/tcp/products/swatches/";
  if (stack2 = helpers.partNumber) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = (depth0 && depth0.partNumber); stack2 = typeof stack2 === functionType ? stack2.call(depth0, {hash:{},data:data}) : stack2; }
  buffer += escapeExpression(stack2)
    + ".jpg\"><br><span class=\"style-number\"><strong>Style &#35;:</strong> ";
  if (stack2 = helpers.partNumber) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = (depth0 && depth0.partNumber); stack2 = typeof stack2 === functionType ? stack2.call(depth0, {hash:{},data:data}) : stack2; }
  buffer += escapeExpression(stack2)
    + "</span></p><div class=\"qty\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.parseInt || (depth0 && depth0.parseInt)),stack1 ? stack1.call(depth0, (depth0 && depth0.quantityBought), options) : helperMissing.call(depth0, "parseInt", (depth0 && depth0.quantityBought), options)))
    + " of ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.parseInt || (depth0 && depth0.parseInt)),stack1 ? stack1.call(depth0, (depth0 && depth0.requestedQuantity), options) : helperMissing.call(depth0, "parseInt", (depth0 && depth0.requestedQuantity), options)))
    + "<br>purchased</div><div class=\"price\">$";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.toFixed || (depth0 && depth0.toFixed)),stack1 ? stack1.call(depth0, (depth0 && depth0.offerPrice), options) : helperMissing.call(depth0, "toFixed", (depth0 && depth0.offerPrice), options)))
    + " ";
  stack2 = helpers['if'].call(depth0, (depth0 && depth0.listPrice), {hash:{},inverse:self.noop,fn:self.program(10, program10, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</div></div><div class=\"actions\">";
  options = {hash:{},inverse:self.program(15, program15, data),fn:self.program(12, program12, data),data:data};
  stack2 = ((stack1 = helpers.compare || (depth0 && depth0.compare)),stack1 ? stack1.call(depth0, (depth0 && depth0.availableQuantity), ">", 0, options) : helperMissing.call(depth0, "compare", (depth0 && depth0.availableQuantity), ">", 0, options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</div></article>";
  return buffer;
  }
function program8(depth0,data) {
  
  
  return " <span class=\"online-only\"></span> ";
  }

function program10(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "<em>Regularly: $";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.toFixed || (depth0 && depth0.toFixed)),stack1 ? stack1.call(depth0, (depth0 && depth0.listPrice), options) : helperMissing.call(depth0, "toFixed", (depth0 && depth0.listPrice), options)))
    + "</em>";
  return buffer;
  }

function program12(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += " <a href=\"#delete-item\" class=\"delete-item\"></a> <a href=\"#add-to-cart\" class=\"add-to-cart\">Add to Bag</a><div class=\"my-wishlist\"><span class=\"read-only\">Click here to move this item to another list.</span><div class=\"component add-to-wishlist move-to-wishlist\" data-component=\"MoveWishlist\"><a href=\"#more-to\" class=\"add-to-default-wishlist collapsed\">MOVE TO</a> <a href=\"#see-more\" class=\"see-more\">MOVE TO</a></div></div>";
  options = {hash:{},inverse:self.noop,fn:self.program(13, program13, data),data:data};
  stack2 = ((stack1 = helpers.compare || (depth0 && depth0.compare)),stack1 ? stack1.call(depth0, (depth0 && depth0.webOnlyFlagUSStore), "!=", "1", options) : helperMissing.call(depth0, "compare", (depth0 && depth0.webOnlyFlagUSStore), "!=", "1", options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " ";
  return buffer;
  }
function program13(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "<div id=\"ropis-cta\" class=\"js-ropis-cta ropis-wishlist\" data-product-id=\"";
  if (stack1 = helpers.productId) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.productId); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" data-caption=\"reserve online\" data-item-tcp-product-ind=\"";
  if (stack1 = helpers.itemTCPProductInd) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.itemTCPProductInd); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + "\"></div>";
  return buffer;
  }

function program15(depth0,data) {
  
  
  return " <span class=\"read-only\">Click here to delete this item.</span> <a href=\"#delete-item\" class=\"delete-item\"></a><p class=\"oos\">Due to overwhelming popularity, this product is out of stock.</p>";
  }

function program17(depth0,data,depth2) {
  
  var buffer = "", stack1, options;
  buffer += " ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.set || (depth2 && depth2.set)),stack1 ? stack1.call(depth0, depth2, "hasBoughtElements", "true", options) : helperMissing.call(depth0, "set", depth2, "hasBoughtElements", "true", options)))
    + " ";
  return buffer;
  }

function program19(depth0,data) {
  
  var buffer = "", stack1;
  buffer += " ";
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.showNoProductsMessage), {hash:{},inverse:self.noop,fn:self.program(20, program20, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " ";
  return buffer;
  }
function program20(depth0,data) {
  
  
  return "<article class=\"no-product\"><p>Wow! All of the items from this Wishlist have been purchased... continue shopping to add new items.</p></article>";
  }

function program22(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "<section itemtype=\"http://schema.org/ItemList\" class=\"product-list already-bought\"><h3>Items Purchased from this Wishlist</h3><ol><li>Price Each</li><li>Quantity</li></ol>";
  stack1 = helpers.each.call(depth0, depth0, {hash:{},inverse:self.noop,fn:self.program(23, program23, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</section>";
  return buffer;
  }
function program23(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += " ";
  options = {hash:{},inverse:self.noop,fn:self.program(24, program24, data),data:data};
  stack2 = ((stack1 = helpers.compare || (depth0 && depth0.compare)),stack1 ? stack1.call(depth0, (depth0 && depth0.quantityBought), ">=", (depth0 && depth0.requestedQuantity), options) : helperMissing.call(depth0, "compare", (depth0 && depth0.quantityBought), ">=", (depth0 && depth0.requestedQuantity), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " ";
  return buffer;
  }
function program24(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "<article itemprop=\"itemListElement\" itemscope=\"\" itemtype=\"http://schema.org/Product\" data-product-id=\"";
  if (stack1 = helpers.giftListItemID) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.giftListItemID); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" data-item-id=\"";
  if (stack1 = helpers.wishListItemId) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.wishListItemId); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" data-catentryid=\"";
  if (stack1 = helpers.itemId) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.itemId); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + "\"><figure>";
  options = {hash:{},inverse:self.noop,fn:self.program(8, program8, data),data:data};
  stack2 = ((stack1 = helpers.compare || (depth0 && depth0.compare)),stack1 ? stack1.call(depth0, (depth0 && depth0.webOnlyFlagUSStore), "==", "1", options) : helperMissing.call(depth0, "compare", (depth0 && depth0.webOnlyFlagUSStore), "==", "1", options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " <a href=\"";
  if (stack2 = helpers.productURL) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = (depth0 && depth0.productURL); stack2 = typeof stack2 === functionType ? stack2.call(depth0, {hash:{},data:data}) : stack2; }
  buffer += escapeExpression(stack2)
    + "\"><img itemprop=\"image\" alt=\"";
  if (stack2 = helpers.productName) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = (depth0 && depth0.productName); stack2 = typeof stack2 === functionType ? stack2.call(depth0, {hash:{},data:data}) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" src=\"/wcsstore/GlobalSAS/images/tcp/products/500/";
  if (stack2 = helpers.thumbnail) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = (depth0 && depth0.thumbnail); stack2 = typeof stack2 === functionType ? stack2.call(depth0, {hash:{},data:data}) : stack2; }
  buffer += escapeExpression(stack2)
    + ".jpg\"></a><figcaption></figcaption></figure><div class=\"info\"><h2 itemprop=\"name\"><a href=\"";
  if (stack2 = helpers.productURL) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = (depth0 && depth0.productURL); stack2 = typeof stack2 === functionType ? stack2.call(depth0, {hash:{},data:data}) : stack2; }
  buffer += escapeExpression(stack2)
    + "\">";
  if (stack2 = helpers.productName) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = (depth0 && depth0.productName); stack2 = typeof stack2 === functionType ? stack2.call(depth0, {hash:{},data:data}) : stack2; }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</a></h2><p itemprop=\"description\" class=\"product-details\"><strong>Size:</strong> ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.getSize || (depth0 && depth0.getSize)),stack1 ? stack1.call(depth0, (depth0 && depth0.size), (depth0 && depth0.itemId), options) : helperMissing.call(depth0, "getSize", (depth0 && depth0.size), (depth0 && depth0.itemId), options)))
    + "<br><strong>Color:</strong> ";
  if (stack2 = helpers.color) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = (depth0 && depth0.color); stack2 = typeof stack2 === functionType ? stack2.call(depth0, {hash:{},data:data}) : stack2; }
  buffer += escapeExpression(stack2)
    + " <img src=\"/wcsstore/GlobalSAS/images/tcp/products/swatches/";
  if (stack2 = helpers.partNumber) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = (depth0 && depth0.partNumber); stack2 = typeof stack2 === functionType ? stack2.call(depth0, {hash:{},data:data}) : stack2; }
  buffer += escapeExpression(stack2)
    + ".jpg\"><br><span class=\"style-number\"><strong>Style &#35;:</strong> ";
  if (stack2 = helpers.partNumber) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = (depth0 && depth0.partNumber); stack2 = typeof stack2 === functionType ? stack2.call(depth0, {hash:{},data:data}) : stack2; }
  buffer += escapeExpression(stack2)
    + "</span></p><div class=\"qty\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.parseInt || (depth0 && depth0.parseInt)),stack1 ? stack1.call(depth0, (depth0 && depth0.requestedQuantity), options) : helperMissing.call(depth0, "parseInt", (depth0 && depth0.requestedQuantity), options)))
    + " purchased</div><div class=\"price\">$";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.toFixed || (depth0 && depth0.toFixed)),stack1 ? stack1.call(depth0, (depth0 && depth0.offerPrice), options) : helperMissing.call(depth0, "toFixed", (depth0 && depth0.offerPrice), options)))
    + " ";
  stack2 = helpers['if'].call(depth0, (depth0 && depth0.listPrice), {hash:{},inverse:self.noop,fn:self.program(10, program10, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</div></div></article>";
  return buffer;
  }

  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.set || (depth0 && depth0.set)),stack1 ? stack1.call(depth0, depth0, "hasBoughtElements", "false", options) : helperMissing.call(depth0, "set", depth0, "hasBoughtElements", "false", options)))
    + " ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.set || (depth0 && depth0.set)),stack1 ? stack1.call(depth0, depth0, "countUnboughtElements", "0", options) : helperMissing.call(depth0, "set", depth0, "countUnboughtElements", "0", options)))
    + "<section itemtype=\"http://schema.org/ItemList\" class=\"product-list\">";
  stack2 = helpers.each.call(depth0, depth0, {hash:{},inverse:self.noop,fn:self.programWithDepth(1, program1, data, depth0),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " ";
  options = {hash:{},inverse:self.noop,fn:self.program(4, program4, data),data:data};
  stack2 = ((stack1 = helpers.compare || (depth0 && depth0.compare)),stack1 ? stack1.call(depth0, (depth0 && depth0.countUnboughtElements), ">", 0, options) : helperMissing.call(depth0, "compare", (depth0 && depth0.countUnboughtElements), ">", 0, options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " ";
  stack2 = helpers.each.call(depth0, depth0, {hash:{},inverse:self.noop,fn:self.programWithDepth(6, program6, data, depth0),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " ";
  options = {hash:{},inverse:self.noop,fn:self.program(19, program19, data),data:data};
  stack2 = ((stack1 = helpers.compare || (depth0 && depth0.compare)),stack1 ? stack1.call(depth0, (depth0 && depth0.countUnboughtElements), "==", 0, options) : helperMissing.call(depth0, "compare", (depth0 && depth0.countUnboughtElements), "==", 0, options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</section>";
  options = {hash:{},inverse:self.noop,fn:self.program(22, program22, data),data:data};
  stack2 = ((stack1 = helpers.compare || (depth0 && depth0.compare)),stack1 ? stack1.call(depth0, (depth0 && depth0.hasBoughtElements), "==", "true", options) : helperMissing.call(depth0, "compare", (depth0 && depth0.hasBoughtElements), "==", "true", options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  return buffer;
  });

this["App"]["Templates"]["wishlist-add-to"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<div class=\"my-wishlist template\"><span class=\"transaction-messaging\"></span> <span class=\"read-only\">Click the MOVE TO WISHLIST button to add to your default Wishlist.</span><div class=\"component add-to-wishlist\" data-component=\"AddWishlist\"><a href=\"#add_to_wishlist\" class=\"add-to-default-wishlist collapsed\"></a> <a href=\"#see_more\" class=\"see-more\">see more</a></div><div class=\"modal\"><div class=\"login\" data-component=\"Login\"><span class=\"read-only\">Fill the form below to log in</span><form><legend></legend><span class=\"transaction-messaging\"></span><fieldset><label for=\"email\"><input type=\"text\" placeholder=\"Email address\" name=\"email\" aria-label=\"email\"></label><label for=\"password\"><input type=\"password\" placeholder=\"Password\" name=\"password\" aria-label=\"password\"></label><label for=\"submit\"><input type=\"submit\" value=\"Log in\" name=\"submit\" aria-label=\"submit\"></label></fieldset><a href=\"#forgot-password\" class=\"forgot-password\">forgot password?</a><p>Don&#39;t have an account? <a href=\"#create-account\" class=\"create-account\">create an account</a></p></form></div><div id=\"espot\" name=\"espot\" class=\"espot\"></div><a href=\"#close\" class=\"close\"></a></div></div>";
  });

this["App"]["Templates"]["wishlist-create-new"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<form class=\"add-new-wishlist\"><div class=\"result\"></div><fieldset><legend>Create new Wishlist</legend><label class=\"wishlist-name\" for=\"wishlist-name\"><input type=\"text\" name=\"wishlist-name\" placeholder=\"&lt;User Name&gt;'s Wishlist\" value=\"\" aria-label=\"wishlist-name\"></label><label class=\"reset\" for=\"reset\"><input type=\"reset\" value=\"Cancel\" class=\"cancel-new-wishlist-cta\" aria-label=\"reset\" name=\"reset\"></label><label class=\"submit\" for=\"submit\"><input type=\"submit\" value=\"Create\" class=\"create-new-wishlist-cta\" aria-label=\"submit\" name=\"submit\"></label></fieldset></form>";
  });

this["App"]["Templates"]["wishlist-item"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, stack2, options, functionType="function", escapeExpression=this.escapeExpression, self=this, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += " ";
  if (stack1 = helpers.itemCount) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.itemCount); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + " Items ";
  return buffer;
  }

function program3(depth0,data) {
  
  
  return " 1 Item ";
  }

function program5(depth0,data) {
  
  
  return " <span>|</span> Default ";
  }

  buffer += "<li class=\"";
  if (stack1 = helpers.className) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.className); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" data-id=\"";
  if (stack1 = helpers.giftListExternalIdentifier) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.giftListExternalIdentifier); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" data-list-count=\"";
  if (stack1 = helpers.listCount) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.listCount); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" data-status=\"";
  if (stack1 = helpers.status) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.status); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + "\"><a href=\"#active-wishlist\" class=\"active-wishlist\">";
  if (stack1 = helpers.nameIdentifier) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.nameIdentifier); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</a><p>";
  options = {hash:{},inverse:self.program(3, program3, data),fn:self.program(1, program1, data),data:data};
  stack2 = ((stack1 = helpers.compare || (depth0 && depth0.compare)),stack1 ? stack1.call(depth0, (depth0 && depth0.itemCount), "!=", "1", options) : helperMissing.call(depth0, "compare", (depth0 && depth0.itemCount), "!=", "1", options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " ";
  stack2 = helpers['if'].call(depth0, (depth0 && depth0.inputChecked), {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</p><span class=\"read-only\">Click to \"Edit\" this wishlist. You can change the name of this Wishlist, delete this Wishlist, or change Wishlist to Default.</span> <a href=\"#edit\" class=\"edit\"></a></li>";
  return buffer;
  });

this["App"]["Templates"]["wishlist-move-to"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += " ";
  options = {hash:{},inverse:self.program(2, program2, data),fn:self.program(2, program2, data),data:data};
  stack2 = ((stack1 = helpers.compare || (depth0 && depth0.compare)),stack1 ? stack1.call(depth0, (depth0 && depth0.state), "==", "Default", options) : helperMissing.call(depth0, "compare", (depth0 && depth0.state), "==", "Default", options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " ";
  return buffer;
  }
function program2(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "<li data-id=\"";
  if (stack1 = helpers.giftListExternalIdentifier) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.giftListExternalIdentifier); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + "\"><a href=\"#";
  if (stack1 = helpers.nameIdentifier) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.nameIdentifier); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\">";
  if (stack1 = helpers.nameIdentifier) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.nameIdentifier); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</a></li>";
  return buffer;
  }

function program4(depth0,data) {
  
  
  return "<li class=\"create-wishlist\"><a href=\"#create-new-wishlist\">Create new Wishlist</a></li>";
  }

  buffer += "<div class=\"move-to-wishlist-options\"><span class=\"transaction-messaging\"></span><ul>";
  stack1 = helpers.each.call(depth0, depth0, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " ";
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.enabledNew), {hash:{},inverse:self.noop,fn:self.program(4, program4, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</ul></div>";
  return buffer;
  });

this["App"]["Templates"]["wishlist-update"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  
  return " <a href=\"#set-default-wishlist\" class=\"set-default-wishlist\">Set as Default</a> ";
  }

function program3(depth0,data) {
  
  
  return " <a href=\"#delete-wishlist\" class=\"delete-wishlist\">Delete</a> ";
  }

  buffer += "<div class=\"modal no-veil edit-wishlist\" data-id=\"";
  if (stack1 = helpers.wishlistId) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.wishlistId); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + "\"><a href=\"#close\" class=\"close\"><span>close</span></a> <span class=\"read-only\">Click below to edit Wishlist name, change to or from default, or delete.</span><form><fieldset><legend>Edit your Wishlist</legend><label for=\"wishlist-name\"><input name=\"wishlist-name\" type=\"text\" value=\"";
  if (stack1 = helpers.name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.name); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" placeholder=\"Whislist's Name\" aria-label=\"wishlist-name\"></label>";
  stack1 = helpers.unless.call(depth0, (depth0 && depth0.isDefault), {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " ";
  stack1 = helpers.unless.call(depth0, (depth0 && depth0.isUnique), {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "<label for=\"update\"><input name=\"update\" type=\"submit\" value=\"Update\"></label></fieldset></form></div>";
  return buffer;
  });

this["App"]["Templates"]["wishlist"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, helperMissing=helpers.helperMissing, self=this;

function program1(depth0,data,depth1) {
  
  var buffer = "", stack1, stack2, options;
  buffer += " ";
  options = {hash:{},inverse:self.programWithDepth(4, program4, data, depth1),fn:self.programWithDepth(2, program2, data, depth1),data:data};
  stack2 = ((stack1 = helpers.compare || (depth0 && depth0.compare)),stack1 ? stack1.call(depth0, (depth0 && depth0.status), "==", "Default", options) : helperMissing.call(depth0, "compare", (depth0 && depth0.status), "==", "Default", options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " ";
  return buffer;
  }
function program2(depth0,data,depth2) {
  
  var buffer = "", stack1, stack2, options;
  buffer += " ";
  options = {hash:{},data:data};
  stack2 = ((stack1 = helpers.template || (depth0 && depth0.template)),stack1 ? stack1.call(depth0, "wishlist-item", depth0, "className", "selected", "inputChecked", "checked", "listCount", (depth2 && depth2.length), options) : helperMissing.call(depth0, "template", "wishlist-item", depth0, "className", "selected", "inputChecked", "checked", "listCount", (depth2 && depth2.length), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " ";
  return buffer;
  }

function program4(depth0,data,depth2) {
  
  var buffer = "", stack1, stack2, options;
  buffer += " ";
  options = {hash:{},data:data};
  stack2 = ((stack1 = helpers.template || (depth0 && depth0.template)),stack1 ? stack1.call(depth0, "wishlist-item", depth0, "listCount", (depth2 && depth2.length), options) : helperMissing.call(depth0, "template", "wishlist-item", depth0, "listCount", (depth2 && depth2.length), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " ";
  return buffer;
  }

  buffer += "<h2>Your Wishlists <span class=\"read-only\">Click below to review your Wishlists or create a new one.</span></h2><ol class=\"wishlist-list\">";
  stack1 = helpers.each.call(depth0, depth0, {hash:{},inverse:self.noop,fn:self.programWithDepth(1, program1, data, depth0),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</ol>";
  return buffer;
  });