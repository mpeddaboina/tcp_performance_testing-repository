$.ajaxSetup({cache:true});
$('head').append('<link rel="stylesheet" href="/wcsstore/GlobalSAS/css/tcp/pdp-recommendation.css" type="text/css" />');
// $('head').append('<link rel="stylesheet" href="/wcsstore/GlobalSAS/css/tcp/quick-view/quickView.css" type="text/css" />');
// $('head').append('<script type="text/javascript" src="/wcsstore/GlobalSAS/javascript/tcp/quick-view/templatesQuickView.js" />');
$('head').append('<script type="text/javascript" src="/wcsstore/GlobalSAS/javascript/tcp/Restjsapi/pdp-recommendations.js" />');

// $.getScript('/wcsstore/GlobalSAS/javascript/tcp/quick-view/quickView.js', function() {
//
// });

$(".product-thumbnails").insertAfter(".zoom-text");
$("<section class='product-recommendations bis' id='generic-recommendations-next'></section>").insertAfter(".product-recommendations");

var quickViewAppInitialized = false;

$(document).off("click.quick-view").on("click.quick-view", ".js-quick-view-modal, #WC_CQ_Container_PDP .product-image a", function(e) {
	if (!quickViewAppInitialized) {
		Rapp.init();
		quickViewAppInitialized = true;
	}
    e.preventDefault();
    e.stopPropagation();
	var $qvLink = $(this).hasClass('js-quick-view-modal') ? $(this) : $(this).closest('li').find('.js-quick-view-modal');
	var productId = Rapp.Util.QueryString.get("productId", $qvLink.attr("link") || $qvLink.attr("href"));
    Rapp.Ropis.ProductQuickView.open(productId);
    return false;
});

//Remove text ADD TO WHISHLIST
$(".add-to-default-wishlist").html("");
$(".add-to-default-wishlist").attr("title","Add to Wishlist");