
//-----------------------------------------------------------------
// Licensed Materials - Property of IBM
//
// WebSphere Commerce
//
// (C) Copyright IBM Corp. 2007, 2010 All Rights Reserved.
//
// US Government Users Restricted Rights - Use, duplication or
// disclosure restricted by GSA ADP Schedule Contract with
// IBM Corp.
//-----------------------------------------------------------------

/**
 *@fileOverview This javascript file defines all the javascript functions used to display
 *and handle the information messages, error messages.
 */

if(typeof(MessageHelper) == "undefined" || !MessageHelper || !MessageHelper.topicNamespace){

/**
 * @class The MessageHelper class contains variables and functions that are used
 * to initialize, display and handle informational and error message.
 */
	MessageHelper = {
		isTCPError:false,
		timeout:600000,
		setTimeout:function(value) {
			this.timeout = value;
		},
		getTimeout:function() {
			return this.timeout;
		},
		setIsTCPError:function(value) {
			this.isTCPError = value;
		},
		getIsTCPError:function() {
			return this.isTCPError;
		},
			
		/**A variable that contains all the messages to be displayed*/
		messages: {},
		/**This map contains error message for fields  */
		errorMessageKey : {
			firstName: "ERROR_FirstNameEmpty",
			lastName: "ERROR_LastNameEmpty",
			billing_firstName: "ERROR_FirstNameEmpty",
			billing_lastName: "ERROR_LastNameEmpty",
			logonId :"ERROR_INVALIDEMAILFORMAT",
			email1Verify:"ERROR_EmailVerifyEmpty",
			logonPassword:"ERROR_PasswordEmpty",
			logonPasswordVerify:"ERROR_VerifyPasswordEmpty",
			addressField3:"ERROR_ZipCodeEmpty",
			billing_phone1:"ERROR_PhonenumberEmpty",
			billing_address1:"ERROR_AddressEmpty",
			billing_city:"ERROR_CityEmpty",
			billing_state:"ERROR_StateEmpty",
			billing_country:"ERROR_CountryEmpty",
			phone1:"ERROR_PhonenumberEmpty",
			address1:"ERROR_AddressEmpty",
			city:"ERROR_CityEmpty",
			state:"ERROR_StateEmpty",
			country:"ERROR_CountryEmpty",
			singleShipmentShippingMode:"ERROR_ShippingMethodEmpty",
			pay_temp_account:"REQUIRED_FIELD_ENTER"
			},
		
		/**
     * internal variable to keep track of the current element id that has
     * an error tooltip assigned to it */
		identifier: "",

    /**
     * returns the current year
     * @return (int) the current year
     */
		getCurrentYear: function(){
			return new Date().getFullYear();
		}, 

     /**
     * returns the current month. January is 1, and December is 12.
     * @return (int) the current month
     */
		getCurrentMonth: function(){
			return new Date().getMonth()+1;
		}, 

     /**
     * returns the current day of the current month, starting from 1.
     * @return (int) the current day
     */
		getCurrentDay: function(){
			return new Date().getDate();
		}, 

    /**
     *
     *summary: retrieves the value of the property from a render context
		 *description: This function retrieves the value of the property whose name is propertName
		 *from the given context.
     *
     * @param (wc.render.Content) content The context in which the properties
     * belong to.
     * @param (string) propertyName The property to be retrieved
		 * @return (string) null if the context is null. undefined if the property is not found.
		 * otherwise, the value of the property int he given context.
     */
		getRenderContextProperty : function(/*wc.render.Context*/context, /*String*/propertyName){
			
			console.debug("enter getRenderContextProperty with propertyName = "+propertyName);
			if(context == null){
				console.debug("context is null. Return null...");
				return null;
			}
			
			var result = context.properties[propertyName];
			console.debug("the found property value is: "+result);
			
			return result;	
		}, 
				
		/**
     * This function is used to initialize the messages object with all the 
     * required messages. It is used to setup a JS object with any key/value.
     * @param (string) key The key used to access this message.
     * @param (string) msg The message in the correct language.
     *
     */
		setMessage:function(key, msg) {
			this.messages[key] = msg;
		},

	
	
	/**
	 * Use dojo.fadeIn and dojo.fadeOut to display error and informative messages in the store.
	 * @param (int) topOffset how far from the top the message display area will be displayed. 
	 */
		showHideMessageArea:function(topOffset){
			cursor_clear();
			if (topOffset==null || topOffset==undefined) {
				topOffset = 0;
			}
			var node = dojo.byId("MessageArea");
			
			// place the display section at a location relative to the 'page' element
			var page = dojo.byId("page");
			if(page != null){
				var coords = dojo.coords(page, true);
				var width = coords.w;
				if(dojo.isSafari || dojo.isChrome){
					width = dojo.style('page', 'width');
				}
				dojo.style(node, {
					"width": width + 20 + "px",
					"left": coords.x - 10 + "px",
					"top": (coords.y + topOffset) + "px"
				});
			}
			
			var fadeInAnimArgsArray = new Array();
			fadeInAnimArgsArray["node"] = node;
			fadeInAnimArgsArray["duration"] = 200;
			fadeInAnimArgsArray["delay"] = 0;
			
			var fadeOutAnimArgsArray = new Array();
			fadeOutAnimArgsArray["node"] = node;
			fadeOutAnimArgsArray["duration"] = 500;
			fadeOutAnimArgsArray["delay"] = this.getTimeout();
			fadeOutAnimArgsArray["onEnd"] = function(){
				dojo.style(node, "display", "none");
				dojo.style(node, "opacity", 100);
			};
			
			// set message area to alpha and then make it display block
			dojo.style(node, "opacity", 0);
			dojo.style(node, "display", "block");
			
			// fade in
			var fadeInAnim = dojo.fadeIn(fadeInAnimArgsArray);
			
			// fade out and when end the display set to none and opacity set to 100
			var fadeOutAnim = dojo.fadeOut(fadeOutAnimArgsArray);
			
			// sequence run fade in and out
			dojo.fx.chain([fadeInAnim, fadeOutAnim]).play();			
		},
	
	/**
	 * Use dojo.fadeOut to hide error and informative messages in the store.
	 */
		hideMessageArea:function(){
			cursor_clear();
			var node = dojo.byId("MessageArea");
			var fadeOutAnimArgsArray = new Array();
			fadeOutAnimArgsArray["node"] = node;
			fadeOutAnimArgsArray["duration"] = 500;
			fadeOutAnimArgsArray["onEnd"] = function(){
				dojo.style(node, "display", "none");
				dojo.style(node, "opacity", 100);
			};
			dojo.fadeOut(fadeOutAnimArgsArray).play();
			dojo.byId('ErrorMessageText').innerHTML = "";
		},
		
    /**
     * This function is used to display the error messages to the user. 
     * @param (string) msg The error/information message to be displayed
     * @param (int) topOffset how far from the top the message display area will be displayed. 
     * @param (boolean) showType whether or not the message type should be appended to the actual message
     *
     * @return (element) a HTML element that contains the error message. 
     *
     */
		displayErrorMessage:function(msg, topOffset,showType){	
			if($('#ErrorArea').length > 0){
				this.tcpDisplayStatusMessage(msg);
			}else{
			if (topOffset==null || topOffset==undefined) {
				topOffset = 0;
			}
			
			if (showType == undefined || showType==null || showType==true){
				//if showType is undefined, keep the FEP1 behaviour.  
				if(this.messages["ERROR_MESSAGE_TYPE"]!=null && this.messages["ERROR_MESSAGE_TYPE"]!='undefined'){
					var MsgType = this.messages["ERROR_MESSAGE_TYPE"]; 
					msg = MsgType + msg;
				}				
			}
			
			this.setMessageAreaStyle('2');
			dojo.byId('ErrorMessageText').innerHTML = msg;
			this.showHideMessageArea(topOffset);
			dojo.byId('MessageArea').focus();
			setTimeout("dojo.byId('ErrorMessageText').focus()", 1000);
			}
		},

	
	/**
	 * Sets the style for the message area on the page.
	 * @param (String) styleId The style Id.
	 */
		setMessageAreaStyle:function(styleId){
			if(dojo.byId('MessageArea') != null){dojo.byId('MessageArea').className = 'msgpopup' + styleId;}
		},
		
		
    /**
     * This function is used to display the informative messages to the user.
     * @param (string) msg The status message to be displayed.
     * @param (int) topOffset how far from the top of the browser the message will be displayed. 
     * @return (element) a HTML element that contains the status message.
     */
		displayStatusMessage:function(msg,topOffset){
			if (topOffset==null || topOffset==undefined) {
				topOffset = 0;
			}
			this.setMessageAreaStyle('1');
			dojo.byId('ErrorMessageText').innerHTML = msg;
			this.showHideMessageArea(topOffset);
			dojo.byId('MessageArea').focus();
			setTimeout("dojo.byId('ErrorMessageText').focus()",1000);
		},

    /**
     * This function is used to hide and clear the message display area in
     * the page.
     */
		hideAndClearMessage:function(){
			if (dojo.byId('ErrorMessageText') != null){
				dojo.byId('ErrorMessageText').innerHTML = "";
			}
			if (dojo.byId('MessageArea') != null){
				dojo.byId('MessageArea').style.display = "none";
			}
		},
	
	/**
	 * This function is used to re-adjust the coordinates of the message display area on the page. Its location is relative to the "page" element.
	 * @param (int) topOffset how far from the top the message display area will be displayed. 
	 */
		adjustCoordinates:function(topOffset){
			if(dojo.style("MessageArea", "display") != "none"){
				var page = dojo.byId("page");
				var node = dojo.byId("MessageArea");
				if(page != null && node != null){
					var coords = dojo.coords(page, true);
					var width = coords.w;
					if(dojo.isSafari){
						width = dojo.style('page', 'width');
					}
					
					if (topOffset==null || topOffset==undefined) {
						topOffset = 0;
					}
					
					dojo.style(node, {
						"width": width + 20+ "px",
						"left": coords.x - 10 + "px",
						"top": (coords.y + topOffset) + "px"
					});
				}
			}
		},

    /**
     * This function will show the an error message tooltip
     * around the input field with the problem.
     *
     * The function assumes the "serviceResponse" is the
     * JSON object from a WebSphere Commerce exception. The error
     * field is in the serviceResponse.errorMessageParam and the
     * error message is in the serviceResponse.errorMessage.
     *
     * @see MessageHelper.formErrorHandleClient
     * @param (object) serviceResponse The JSON object with the error data.
     * @param (string) formName The name of the form where the error field is.
     * 
     */
		formErrorHandle:function(serviceResponse,formName){

			this.formErrorHandleClient(serviceResponse.errorMessageParam, serviceResponse.errorMessage);

	  	},


		/**
     * This function will show the an error message tooltip
     * around the input field with the problem.
     *
     * This function will check for the emptiness of the required
     * filed and displays the "errorMessage" related to that field as a tooltip.
     * The tooltip will be closed on focus lost.
     *
     * @param (string) id The identifier for the filed in the form.
     * @param (string) errorMessage The message that should be displayed to the user.
     */
		formErrorHandleClient:function(id,errorMessage){
	  		if(this.getIsTCPError()){
				this.tcpFormErrorHandleClientTimeSpan(id,errorMessage,this.getTimeout());
			}else{
				var element = dojo.byId(id);
				if (errorMessage == null){	
					console.debug("formErrorHandleClient: The error message is null.");
					return;
				}
				if(element){
					if (this.identifier != (id + "_tooltip")) {
						this.identifier = id + "_tooltip";
						var node = document.createElement('span');
						var imgDirPath = getImageDirectoryPath();
						if(dojo.isIE < 7)
						{
							node.innerHTML = errorMessage + "<iframe id='errorMessageIFrame' scrolling='no' frameborder='0' src='" + imgDirPath + "images/empty.gif'></iframe>";
						}
						else
						{
							node.innerHTML = errorMessage;
						}											
						var tooltip = new dijit.Tooltip({connectId: [id]}, node);
						tooltip.startup();
						console.log("created", tooltip, tooltip.id);
						element.focus();
						tooltip.open(element); // force to have this for IE if the error is on a link (i.e. <a>)
						dojo.connect(element, "onblur",  tooltip, "close"); // force to have this for IE if the error is on a link (i.e. <a>)
						dojo.connect(element, "onblur",  tooltip, "destroy");
						dojo.connect(element, "onblur",  this, "clearCurrentIdentifier");
						/* Tooltip widget connects onmouseover event of the above element to _onMouseOver function. 
						When tooltip is associated with the dropdown select box, tooltip will be displayed initially next to the 
						select box. But when user expands the dropdown box and moves the mouse over the options in the select box,
						onmouseover event will be triggered which calls _onMouserOver function.._onMouseOver function will display the tooltip
						again next to the cursor. So when user keeps moving the mouse over the options in select box
						the tooltip widget also moves along with the cursor. To avoid this override _onMouseOver function
						with empty implementation. 
						*/
						tooltip._onMouseOver = this.emptyFunc;
					}
				}
			}
		},


		/**
     * This function clears the internal variable that has the element id
     * with the error tooltip.
     * 
     */
		clearCurrentIdentifier:function(){
		
			this.identifier = "";
	  },

     /**
      * This function is used to override any of the default functions
      * associated with the events. Ex: Tooltip widget tracks onMouseOver event
      * and display the tooltip. To remove this association,
      * tooltip widgets onMouseOver function will be overridden by this empty
      * function.
      * 
      * It is an empty implementation which does nothing.
      *
      * @param (string) event  The event which triggers this function. 
      */
	  emptyFunc:function(event){
		 
	  },



    /**
     * Checks whether a string contains a double byte character.
     *
     * @param (string) target the string to be checked
     * @return (boolean) true if target contains a double byte char;
     * false otherwise
     */
		containsDoubleByte:function (target) {
		
				var str = new String(target);
				var oneByteMax = 0x007F;

				for (var i=0; i < str.length; i++){
					chr = str.charCodeAt(i);
					if (chr > oneByteMax) {
						return true;
					}
				}
				return false;
		},

    /**
     * This function validate email address. It does not allow double byte
     * characters in the email address.
     *
     * @return (boolean) true if the email address is valid; false otherwise
     *
     * @param (string) strEmail the email address string to be validated
     */
		isValidEmail:function(strEmail){
			if (this.containsDoubleByte(strEmail)){
				return false;
			}
            /** Use regular expression to validate the email value**/
			var re = /^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/;
			if (!strEmail.match(re)) {//if (!re.test(strEmail)) {
				return false;
			} else {
				return true;
			}
		  /**Use WCS default way to validate,we(TCP) use the above code, rather than the code below**/
			
			/*if (this.containsDoubleByte(strEmail)){
				return false;
			}
		
			if(strEmail.length == 0) {
				return true;
			} else if (strEmail.length < 5) {
					 return false;
				}else{
					if (strEmail.indexOf(" ") > 0){
								return false;
						}else{
							if (strEmail.indexOf("@") < 1) {
										return false;
								}else{
									if (strEmail.lastIndexOf(".") < (strEmail.indexOf("@") + 2)){
												return false;
										}else{
												if (strEmail.lastIndexOf(".") >= strEmail.length-2){
													return false;
												}
										}
								}
						}
				}
				return true;*/
		},

		/**
     * This function will check if the number of bytes of the string
     * is within the maxlength specified.
     *
     * @param (string) UTF16String the UTF-16 string
     * @param (int) maxlength the maximum number of bytes allowed in your input
     *
     * @return (boolean) false is this input string is larger than maxlength
		 */
		isValidUTF8length: function(UTF16String, maxlength) {
			if (this.utf8StringByteLength(UTF16String) > maxlength) return false;
			else return true;
		},

    /**
     * This function will count the number of bytes represented in a UTF-8
     * string.
     *
     * @param (string) UTF16String the UTF-16 string you want a byte count of
     * @return (int) the integer number of bytes represented in a UTF-8 string
     */
		utf8StringByteLength: function(UTF16String) {

			if (UTF16String === null) return 0;
			
			var str = String(UTF16String);
			var oneByteMax = 0x007F;
			var twoByteMax = 0x07FF;
			var byteSize = str.length;
			
			for (i = 0; i < str.length; i++) {
				chr = str.charCodeAt(i);
				if (chr > oneByteMax) byteSize = byteSize + 1;
				if (chr > twoByteMax) byteSize = byteSize + 1;
			}  
			return byteSize;
		},

	    /**
	     * this function will check whether the text is a numeric or not.
	     * 
	     * @param allowDot is a boolean wich specifies whether to consider
	     * the '.' or not.
	     *
	     * @return (boolean) true if text is numeric
	     */
			IsNumeric : function (text,allowDot)
			{
				if(allowDot) var ValidChars = "0123456789.";
				else var ValidChars = "0123456789";
			  
				var IsNumber=true;
				var Char;

			 
				for (i = 0; i < text.length && IsNumber == true; i++) 
				{ 
					Char = text.charAt(i); 
					if (ValidChars.indexOf(Char) == -1) 
					{
						IsNumber = false;
					}
				}
				return IsNumber;   
			},

		    /**
		     * this function will check whether the text is a numeric or not.
		     * 
		     * @param allowDot is a boolean wich specifies whether to consider
		     * the '.' or not.
		     *
		     * @return (boolean) true if text is numeric
		     */
				IsContainNumber : function (text)
				{
					var ValidChars = "0123456789";
				  
					var IsNumber=false;
					var Char;

				 
					for (i = 0; i < text.length && IsNumber == false; i++) 
					{ 
						Char = text.charAt(i); 
						if (ValidChars.indexOf(Char) != -1) 
						{
							IsNumber = true;
						}
					}
					return IsNumber;   
				},
		
				
				
				/*
				 * Check if passed to this method string contains special chars.
				 * 
				 */
				isValidCharStr: function(iValue){				
					
					  
						  // Search for:
						  // All char A-Z
						  // @ sign
						  // . sign
						  // Space, tab and return
						  // # sign
						  // ' sign
						  // - sign
						  // _ sign
						    
						  var iValidate = new RegExp(/^[a-z@.#_\'\-\s\d\/]*$/i);						  
						  return iValidate.test(iValue);
				},	
				
				isEmpty: function(id){
					var validateWhiteSpace =  new RegExp(/^\s+$/);
						
					var element = $('#' + id);
					var value = $(element).val();
					if(validateWhiteSpace.test(value)){
						element.addClass('error-message');
					}else{
						
					}
					
					
					
				},
				
				/*
				 * Check if passed to this method string contains only alphabet
				 * 
				 */
				isContainsOnlyAlphabet: function(iValue){				
					
					  
						  // Search for:
						  // All char A-Z
						  // All char a-z
					var iValidate = new RegExp(/^[A-Za-z ]*$/i);					  
						  return iValidate.test(iValue);
				},		
				
	    /**
	     * This function is used to display the informative messages to the user.
	     * @param (string) msg The status message to be displayed.
	     * @param (int) topOffset how far from the top of the browser the message will be displayed. 
	     * @return (element) a HTML element that contains the status message.
	     */
			tcpDisplayStatusMessage:function(msg,topOffset){
					
			this.enableContinueButton();

			$('#ErrorArea').children('span').html(msg);
			$('#ErrorArea').css('display','block');

			try {
				// to scroll up to error messge
				$('html, body').animate({ scrollTop: $('#ErrorArea').offset().top }, 'slow');
			}catch (err) {
				console.log(err);
			}

			setTimeout(
				function(){
					$('#ErrorArea').css('display','none');
				}
				,this.getTimeout()
			);
			
			},

	    /**
	     * This function is used to hide and clear the message display area in
	     * the page.
	     */
			tcpHideAndClearMessage:function(){
				if($('#ErrorArea').length > 0){
				$('#ErrorArea').children('span').html('');
				$('#ErrorArea').css('display','none');
				}
			},

    /**
     *
     *This function will check for a valid Phone Number
     *
     *@param (string) text The string to check
     *
     *@return (boolean) true if text is a phone number, ie if each character of
     *input is one of 0123456789() -+ 
     */
		IsValidPhone : function (text)
		{
		
			var ValidChars = "0123456789()-+ ";
		  
			var IsValid=true;
			var Char;
		 
			for (i = 0; i < text.length && IsValid == true; i++) 
			{ 
				Char = text.charAt(i); 
				if (ValidChars.indexOf(Char) == -1) 
				{
					IsValid = false;
				}
			}
			return IsValid;   
		},
		
		
		/**
	     *
	     *This function will check for a valid Phone Number
	     *
	     *Validate ten digits by  using regular expression.
	     *
	     *@param (string) text The string to check
	     *
	     *@return (boolean) true if text is a phone number, ie if the text is 10 digits
	     */
			IsValidPhone2 : function (text){
				text=text.replace(/\+/g,"");
				text=text.replace(/\-/g,"");
				text=text.replace(/\./g,"");
				text=text.replace(/\(/g,"");
				text=text.replace(/\)/g,"");
				var re=/^\d{10}$/;
				return re.test(text);
			},		
		
		/**
	     * This function will show the an error message in TCP customized div
	     * near the input field with the problem.
	     *
	     * This function will check for the emptiness of the required
	     * filed and displays the "errorMessage" related to that field in TCP customized div.
	     * The div's class is "label-error".
	     * The errorMessage will hide after specified time.
	     *
	     * @param (string) id The identifier for the filed in the form.
	     * @param (string) errorMessage The message that should be displayed to the user.
	     * @param (string) timeSpan How long the the error message should show.
	     */		
		
		
		tcpFormErrorHandleClientTimeSpan:function(id,errorMessage,timeSpan){
				
				
			var element = $('#' + id);
			if (errorMessage == null){	
				console.debug("tcpFormErrorHandleClient: The error message is null.");
				return;
			}
			// This is to fix problem with displaying message for drop down.
			if (element[0] && element[0].type == "select-one"){
				var element = $('#uniform-' + id);
			}		
			if(element){
				if (element.parent().children(".label-error") == null || element.parent().children(".label-error").length == 0){
					element = $('#' + id);
					if(element){
						if (element.parent().children(".label-error") == null || element.parent().children(".label-error").length == 0){
							//console.debug("tcpFormErrorHandleClient: labeal-error area doesn't exist.");
							this.displayErrorMessage(errorMessage);
							return;
						}
						element.parent().children(".label-error").html(errorMessage).css('display','block');
						element.addClass('error-message');
						setTimeout(function(){
							element.parent().children(".label-error").hide();
							element.removeClass('error-message');
							}
						,timeSpan);
					}
				}
				element.parent().children(".label-error").html(errorMessage).css('display','block');
				
				element.addClass('error-message');
				$('#' + id).addClass('error-message');
				setTimeout(function(){
					element.parent().children(".label-error").hide();
					element.removeClass('error-message');
					$('#' + id).removeClass('error-message');
				}
				,timeSpan);
			}else{
				element = $('#' + id);
				if(element){
					if (element.parent().children(".label-error") == null || element.parent().children(".label-error").length == 0){
						console.debug("tcpFormErrorHandleClient: label-error area doesn't exist.");
						return;
					}
					element.parent().children(".label-error").html(errorMessage).css('display','block');
					element.addClass('error-message');
					setTimeout(function(){
						element.parent().children(".label-error").hide();
						element.removeClass('error-message');
						}
					,timeSpan);
				}
			}
		},		
		tcpHideErrorHandleClientSpan:function(evt,id){
			var element = $('#' + id);
		
			
			if(id&&!(id.indexOf("address2") > -1)&&evt && evt.keyCode == 9){
				var reWhiteSpace = new RegExp(/^\s+$/);	 
				var value = element.val();
				if(value == "" || reWhiteSpace.test(value)){
					element.addClass('error-message');
					 errorKey=MessageHelper.errorMessageKey[$('#'+id).attr('name')];
					if(errorKey!=undefined ){
						
						element.parent().children(".label-error").text(MessageHelper.messages[errorKey]);
						}
					element.parent().children(".label-error").show();
					
				}else{
					MessageHelper.hideErrorMsgCore(element,id);
				}
			}else{
				MessageHelper.hideErrorMsgCore(element,id);
			}			
					 
	},
		
		hideErrorMsgCore:function(element,id){
			if(element.parent()!=null){
				if(element.parent().children(".label-error").length!=0){
					element.parent().children(".label-error").hide();
					element.removeClass('error-message');
				}else{
						element.parent().parent().children(".label-error").hide();
						element.removeClass('error-message');
					}
			}
			if (typeof id != 'undefined' && id !='' && element[0] && element[0].type == "select-one"){
					var element = $('#uniform-' + id);
			}		
			if(element){
				if (element.parent().children(".label-error") != null&&element.parent().children(".label-error").length!= 0){
					element.parent().children(".label-error").hide();
					element.removeClass('error-message');
				} else{
					element.parent().parent().children(".label-error").hide();
					element.removeClass('error-message');
				}
			}
		},
		
		/**
	     * This function will show the an error message in TCP customized div
	     * near the input field with the problem.
	     *
	     * This function will check for the emptiness of the required
	     * filed and displays the "errorMessage" related to that field in TCP customized div.
	     * The div's class is "label-error".
	     * The errorMessage will hide after specified time.
	     *
	     * @param (string) id The identifier for the filed in the form.
	     * @param (string) errorMessage The message that should be displayed to the user.
	     */
		tcpFormErrorHandleClient:function(id,errorMessage){
			this.enableContinueButton();
			this.tcpFormErrorHandleClientTimeSpan(id,errorMessage,this.getTimeout());
		},
		
		/**
		 * This function will enable continue button.
		 * Search ad hoc button with class name 'contiunelink'. Then set disabled false for them.
		 */
		 enableContinueButton:function(){
		  	$('.contiunelink').each(function(){			  	          
		           		$(this).attr('blocked','false');  
		           		$(this).removeClass('button-dark-grey').addClass('button-blue');
		        });
			
		},
		
		/**
		 * This function disable continue button.
		 * Search ad hoc button with class name 'contiunelink'. Check whether they have been touched.
		 * 
		 * @return (boolean) if false: Current action is multiple click. Need roll back.
		 */
		checkContinueButton:function(){
			
		  	$('.contiunelink').each(function(){     
           		if($(this).attr('blocked') == 'true'){return false;};
        });	  	
		  	return true;
			
		},
		
		/**
		 * This function disable continue button.
		 * Search ad hoc button with class name 'contiunelink'. Then set disabled with true for them.
		 * 
		 * @return (boolean) if false: Current action is multiple click. Need roll back.
		 */
		disableContinueButton:function(){
		  	$('.contiunelink').each(function(){			  	          
           		$(this).attr('blocked','true'); 
           		$(this).removeClass('button-blue').addClass('button-dark-grey');
        });
		},

		/**
		 * This function will clear all form error message
		 * Call this function on click on submit button
		 * 
		 * @return (boolean) if false: Current action is multiple click. Need roll back.
		 */
		clearFormElementErrorMessages:function(){
			try {
				$("input, select").removeClass('error-message');
				$(".label-error").css('display','none'); 
			}catch (err) {
				console.log(err);
			}
		},
		
		isValidZip: function(iValue,country){				
			
				var isValid = false;
				//var usZipRegEx=/^\d{5}(-\d{4})?$/;
				//var cnZipRegEx=/^[ABCEGHJKLMNPRSTVXYabceghjklmnprstvxy]{1}\d{1}[A-Za-z]{1} *\d{1}[A-Za-z]{1}\d{1}$/;				
				var usZipRegEx=/^[0-9]+$/;
				//var zipRegEx=/^[a-zA-Z0-9\d\s]+$/;
			    
				if(country.indexOf('US') != -1 && usZipRegEx.test(iValue)){
				  isValid = true; 
				}
				else if(country.indexOf('CA') !=1 && iValue.length ==6){					
					
						/*Canadian postal codes don't contain the letters F,I,O,Q,U,W,Z. Adding this regex to exactly validate the zip code*/
						var zipPart1=iValue.substr(0,3);
						var zipPart2 = iValue.substr(3,iValue.length);
						iValue = zipPart1+" "+zipPart2;
						var canadaZipRegex = new RegExp(/^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$/i);
					    if (canadaZipRegex.test(iValue)){
					    	isValid=true;					    
						}
				}

				return isValid;
		},	
		validateEmail : function(email,verifyEmail,page){

			
			MessageHelper.currentEmailElementId = email.id;
			if( email.value == "" ) {
				MessageHelper.tcpFormErrorHandleClient(email.id, MessageHelper.messages["ERROR_INVALIDEMAILFORMAT"]);
				
			}
			
			if( verifyEmail.value == "" ) {
				MessageHelper.tcpFormErrorHandleClient(verifyEmail.id, MessageHelper.messages["ERROR_INVALIDEMAILFORMAT"]);
				return false;
			}
			if(email.value != verifyEmail.value ) {
				MessageHelper.tcpFormErrorHandleClient(verifyEmail.id,MessageHelper.messages["EMAILREENTER_DO_NOT_MATCH"]);
				return false;
			}
			
			if(!this.isValidUTF8length(email.value,256)){

				MessageHelper.tcpFormErrorHandleClient(email.id, MessageHelper.messages["ERROR_EmailTooLong"]);
				MessageHelper.tcpFormErrorHandleClient(verifyEmail.id, MessageHelper.messages["ERROR_EmailTooLong"]);

				return false;
			}
			
			if(!this.isValidEmail(email.value)) {

				MessageHelper.tcpFormErrorHandleClient(email.id, MessageHelper.messages["ERROR_INVALIDEMAILFORMAT"]);
				MessageHelper.tcpFormErrorHandleClient(verifyEmail.id, MessageHelper.messages["ERROR_INVALIDEMAILFORMAT"]);
				return false;
			}
			
			MessageHelper.tcpHideErrorHandleClientSpan('',email.id);
			MessageHelper.tcpHideErrorHandleClientSpan('',verifyEmail.id);
			
			var params = [];
			params.email= email.value;
			if(page != null)
				params.page = page;
			else
				params.page = "";
			wc.service.invoke("TCPAjaxBriteVerifyService",params);
			
			return;
		}
	};
}
