//-----------------------------------------------------------------
// Licensed Materials - Property of IBM
//
// WebSphere Commerce
//
// (C) Copyright IBM Corp. 2007, 2009 All Rights Reserved.
//
// US Government Users Restricted Rights - Use, duplication or
// disclosure restricted by GSA ADP Schedule Contract with
// IBM Corp.
//-----------------------------------------------------------------

/** 
 * @fileOverview This javascript is used by UserRegistrationAddForm.jsp and CheckoutLogon.jsp.
 * @version 1.0
 */

  /* Import dojo classes. */
dojo.require("wc.service.common");

/**
 *  The functions defined in the class helps the customer to register with the store. Another function enables a returning 
 *  customer to Sign in for quickcheckout from the shopping cart page.
 *
 *  @class This LogonForm class defines all the functions and variables used to validate the information provided by the
 *	customer to register with the store.To register, a customer creates a logon ID and password. Then, the customer provides their first name, 
 *  last name, street address, city, country/region, state/province, ZIP/postal code, e-mail address and phone number. Other registration options 
 *  include promotional e-mails, preferred language and currency, age, gender, and the remember me option. 
 */
TCPLogonForm ={
	/** Flag which indicates whether 'AjaxMyAccount' is enabled or not. The value of this variable is automatically populated
	 *  based on the change flow option in the store by the setAjaxVar function.
	 */
	ajaxMyAccountEnabled: "false",
	
	//Flag to turn of BV from client side
	isClientSideBVEnabled_NEWS_SIGN_UP : true,
	
	/**
	 * This function validates the logon ID and password for returning customers to sign in and complete the checkout process.
	 * @param {string} form The name of the form containing logon ID and password fields.
	 */
	SubmitAjaxLogin:function(form){
		reWhiteSpace = new RegExp(/^\s+$/);

		if(form.logonId1 != null && reWhiteSpace.test(form.logonId1.value) || form.logonId1.value == ""){ 
			MessageHelper.formErrorHandleClient(form.logonId1.id,MessageHelper.messages["REQUIRED_FIELD_ENTER"]); return;}
			
		if(form.logonPassword1 != null && reWhiteSpace.test(form.logonPassword1.value) || form.logonPassword1.value == ""){ 
			MessageHelper.formErrorHandleClient(form.logonPassword1.id,MessageHelper.messages["REQUIRED_FIELD_ENTER"]); return;}
		
		/*For Handling multiple clicks. */
		if(!submitRequest()){
			return;
		}
				
		form.submit();
		form.reset();

	},
	/** used in Email subscription page **/
	
	   emailSubscribePage: function() {
	      reWhiteSpace = new RegExp(/^\s+$/);
	      var email = document.getElementById('email_subscribe_page').value;
	      MessageHelper.currentEmailElementId = 'email_subscribe_page';
	      	//only if both the server side and client side are enabled verify email address
			if(isBVEnabled_NEWS_SIGN_UP && TCPBriteVerify.isClientSideBVEnabled_NEWS_SIGN_UP ) {
			
				if (!MessageHelper.isValidCharStr(email)) {
				
					MessageHelper.tcpFormErrorHandleClient(MessageHelper.currentEmailElementId, MessageHelper.messages["ERROR_INVALIDEMAILFORMAT"]);
					return false;			
				} else if (!MessageHelper.isValidEmail(email)) {
				
					MessageHelper.tcpFormErrorHandleClient(MessageHelper.currentEmailElementId, MessageHelper.messages["ERROR_INVALIDEMAILFORMAT"]);
					return false;			
				}
				

				var params = [];
				params.email= email;
				params.page = "newsletterSignUp";
				wc.service.invoke("TCPAjaxBriteVerifyService_signup",params);
			}
			else {
			      var atpos=email.indexOf("@");
			      var dotpos=email.lastIndexOf(".");
			      if (!MessageHelper.isValidUTF8length(email, 256) || reWhiteSpace.test(email) || !MessageHelper.isValidEmail(email)) {
			        alert("Please enter a valid email address.");
			        return false;
			      } 
				  else {
			       document.forms["signupFormPage_1"].submit();
			      }
			}
	    },

	
	 
	 validateEmail: function(e) {
	    if (e.keyCode == 13) {
	    	TCPLogonForm.emailSubscribePage();
	    }
	 },
	/** 
	 * This function is called when the Submit button is clicked on the Registration page. All the fields containing customer
	 * information are validated and PersonProcessServicePersonRegistration is called. 
	 * @param {string} form The name of the registration form containing all the customer information.
	 */
	prepareSubmitRegister:function (form)
	{
		var validateOK = true;
		//var zipCodeInvalid = false;
		
	    reWhiteSpace = new RegExp(/^\s+$/);	    
	    var rePwdRule = /(?=.*\d)(?=.*[a-zA-Z]).{8,}/;
	    /**
	     * The below js checks should only happen when the flow is not from Order confirmation page
	     */
		if(form.orderConfirmFlag == null || form.orderConfirmFlag == undefined){
			
	    /** Uses the common validation function defined in AddressHelper class for validating first name, 
		 *  last name, street address, city, country/region, state/province, ZIP/postal code, e-mail address and phone number. 
		 */	    

	    form.firstName.value = trim(form.firstName.value);
		if(form.firstName.value == "" || reWhiteSpace.test(form.firstName.value)){ 
			MessageHelper.tcpFormErrorHandleClient(form.firstName.id, MessageHelper.messages["ERROR_FirstNameEmpty"]);
			if(validateOK) {form.firstName.focus();}
			
			validateOK = false;
		}
		if(MessageHelper.IsContainNumber(form.firstName.value)){ 
			MessageHelper.tcpFormErrorHandleClient(form.firstName.id, MessageHelper.messages["ERROR_NAMECONTAINNUMBER"]); 
			if(validateOK) {form.firstName.focus();}
			validateOK = false;
		}
		if(!MessageHelper.isContainsOnlyAlphabet(form.firstName.value)){ 
			MessageHelper.tcpFormErrorHandleClient(form.firstName.id, MessageHelper.messages["ERROR_FirstNameHasSpecialChar"]); 
			if(validateOK) {form.firstName.focus();}
			validateOK = false; 
		}
		
		form["lastName"].value = trim(form["lastName"].value);
		if(form["lastName"].value == "" || reWhiteSpace.test(form["lastName"].value)){ 
			MessageHelper.tcpFormErrorHandleClient(form["lastName"].id, MessageHelper.messages["ERROR_LastNameEmpty"]);
			if(validateOK) {form["lastName"].focus();}
			validateOK = false;
		}
		if(!MessageHelper.isValidUTF8length(form["lastName"].value, 128)){ 
			MessageHelper.tcpFormErrorHandleClient(form["lastName"].id, MessageHelper.messages["ERROR_LastNameTooLong"]);
			if(validateOK) {form["lastName"].focus();}
			validateOK = false;
		}
		if(MessageHelper.IsContainNumber(form["lastName"].value)){ 
			MessageHelper.tcpFormErrorHandleClient(form["lastName"].id, MessageHelper.messages["ERROR_NAMECONTAINNUMBER"]); 
			if(validateOK) {form["lastName"].focus();}
			validateOK = false;
		}
		if(!MessageHelper.isContainsOnlyAlphabet(form["lastName"].value)){ 
			MessageHelper.tcpFormErrorHandleClient(form["lastName"].id, MessageHelper.messages["ERROR_LastNameHasSpecialChar"]); 
			if(validateOK) {form["lastName"].focus();}
			validateOK = false;
		}		
		
					
		if(isBVEnabled) {
			if(MessageHelper.currentEmailElementId != "") {
				if(validateOK) {form.logonId.focus();}
				validateOK = false;
			}
		} 
		
		form["logonId"].value = trim(form["logonId"].value);
		if((form["logonId"].value == "" || reWhiteSpace.test(form["logonId"].value))){
			MessageHelper.tcpFormErrorHandleClient(form["logonId"].id, MessageHelper.messages["ERROR_INVALIDEMAILFORMAT"]);
			if(validateOK) {form.logonId.focus();}
			validateOK = false;
		}
		if(!MessageHelper.isValidUTF8length(form["logonId"].value, 256)){ 
			MessageHelper.tcpFormErrorHandleClient(form["logonId"].id, MessageHelper.messages["ERROR_EmailTooLong"]);
			if(validateOK) {form.logonId.focus();}
			validateOK = false;
		}
		if(!MessageHelper.isValidEmail(form["logonId"].value)){
			MessageHelper.tcpFormErrorHandleClient(form["logonId"].id, MessageHelper.messages["ERROR_INVALIDEMAILFORMAT"]);
			if(validateOK) {form.logonId.focus();}
			validateOK = false;
		}
	
		
		
		
		form["email1Verify"].value = trim(form["email1Verify"].value);
		if((form["email1Verify"].value == "" || reWhiteSpace.test(form["email1Verify"].value))){
			MessageHelper.tcpFormErrorHandleClient(form["email1Verify"].id, MessageHelper.messages["ERROR_EmailVerifyEmpty"]);
			if(validateOK) {form.email1Verify.focus();}
			validateOK = false;
		}
		if(form.logonId.value!= form.email1Verify.value){ 
			MessageHelper.tcpFormErrorHandleClient(form.email1Verify.id,MessageHelper.messages["EMAILREENTER_DO_NOT_MATCH"]);
			if(validateOK) {form.email1Verify.focus();}
			validateOK = false;
		}		
	
		}
	    
	    
		if(form.logonPassword != null && reWhiteSpace.test(form.logonPassword.value) || form.logonPassword.value == ""){ 
			MessageHelper.tcpFormErrorHandleClient(form.logonPassword.id,MessageHelper.messages["ERROR_PasswordEmpty"]); 
			if(validateOK) {form.logonPassword.focus();}
			validateOK = false;}
		if(form.logonPasswordVerify != null && reWhiteSpace.test(form.logonPasswordVerify.value) || form.logonPasswordVerify.value == ""){ 
			MessageHelper.tcpFormErrorHandleClient(form.logonPasswordVerify.id,MessageHelper.messages["ERROR_VerifyPasswordEmpty"]); 
			if(validateOK) {form.logonPassword.focus();}
			validateOK = false;}
		if(form.logonPassword.value!= form.logonPasswordVerify.value){ 
			MessageHelper.tcpFormErrorHandleClient(form.logonPasswordVerify.id,MessageHelper.messages["PWDREENTER_DO_NOT_MATCH"]);
			if(validateOK) {form.logonPassword.focus();}
			validateOK = false;
		}	
		if(form.logonPassword != null && form.logonPassword.value != "" && !rePwdRule.test(form.logonPassword.value)){
			MessageHelper.tcpFormErrorHandleClient(form.logonPassword.id, MessageHelper.messages["PWORD_EXPIRED"]);
			if(validateOK) {form.logonPassword.focus();}
			validateOK = false;
		}

		
		
		
		/*if(form.challengeQuestion != null && reWhiteSpace.test(form.challengeQuestion.value) || form.challengeQuestion.value == ""){ 
			MessageHelper.tcpFormErrorHandleClient(form.challengeQuestion.id,MessageHelper.messages["ERROR_ChallengeQuestionEmpty"]); 
			if(validateOK) {form.challengeQuestion.focus();}
			validateOK = false;}

		if(form.challengeAnswer != null && reWhiteSpace.test(form.challengeAnswer.value) || form.challengeAnswer.value == ""){ 
			MessageHelper.tcpFormErrorHandleClient(form.challengeAnswer.id,MessageHelper.messages["ERROR_ChallengeAnswerEmpty"]); 
			if(validateOK) {form.challengeAnswer.focus();}
			validateOK = false;}
		if(!MessageHelper.isValidUTF8length(form["challengeAnswer"].value, 256)){ 
			MessageHelper.tcpFormErrorHandleClient(form["challengeAnswer"].id, MessageHelper.messages["ERROR_ChallengeAnswerTooLong"]);
			if(validateOK) {form.challengeAnswer.focus();}
			validateOK = false;
		}
		if(!MessageHelper.isValidCharStr(form["challengeAnswer"].value)){ 
			MessageHelper.tcpFormErrorHandleClient(form["challengeAnswer"].id, MessageHelper.messages["ERROR_ChallengeAnswerHasSpecialChar"]); 
			if(validateOK) {form.address1.focus();}
			validateOK = false;
		}*/




		/*form["address1"].value = trim(form["address1"].value);
		form["address2"].value = trim(form["address2"].value);
		if(form["address1"].value == "" || reWhiteSpace.test(form["address1"].value)){ 
			MessageHelper.tcpFormErrorHandleClient(form["address1"].id, MessageHelper.messages["ERROR_AddressEmpty"]);
			if(validateOK) {form.address1.focus();}
			validateOK = false;
		}
		if(!MessageHelper.isValidUTF8length(form["address1"].value, 100)){ 
			MessageHelper.tcpFormErrorHandleClient(form["address1"].id, MessageHelper.messages["ERROR_AddressTooLong"]); 
			if(validateOK) {form.address1.focus();}
			validateOK = false;
		}
		if(!MessageHelper.isValidCharStr(form["address1"].value)){ 
			MessageHelper.tcpFormErrorHandleClient(form["address1"].id, MessageHelper.messages["ERROR_Address1HasSpecialChar"]); 
			if(validateOK) {form.address1.focus();}
			validateOK = false;
		}		
		

		
		
		
		if(!MessageHelper.isValidUTF8length(form["address2"].value, 50)){ 
			MessageHelper.tcpFormErrorHandleClient(form["address2"].id, MessageHelper.messages["ERROR_AddressTooLong"]);
			if(validateOK) {form.address1.focus();}
			validateOK = false;
		}	
		if(!MessageHelper.isValidCharStr(form["address2"].value)){ 
			MessageHelper.tcpFormErrorHandleClient(form["address2"].id, MessageHelper.messages["ERROR_Address2HasSpecialChar"]); 
			if(validateOK) {form.address2.focus();}
			validateOK = false;
		}		
		
		
				
		
		form["city"].value = trim(form["city"].value);
		if((form["city"].value == "" || reWhiteSpace.test(form["city"].value))){ 
			MessageHelper.tcpFormErrorHandleClient(form["city"].id, MessageHelper.messages["ERROR_CityEmpty"]);
			if(validateOK) {form.city.focus();}
			validateOK = false;
		}
		if(!MessageHelper.isValidUTF8length(form["city"].value, 128)){
			MessageHelper.tcpFormErrorHandleClient(form["city"].id, MessageHelper.messages["ERROR_CityTooLong"]);
			if(validateOK) {form.city.focus();}
			validateOK = false;
		}
		if(!MessageHelper.isValidCharStr(form["city"].value)){
			MessageHelper.tcpFormErrorHandleClient(form["city"].id, MessageHelper.messages["ERROR_CityHasSpecialChar"]);
			if(validateOK) {form.city.focus();}
			validateOK = false;
		}		
		
		
		
				
		var state = form["state"];
		if(state.value == null || state.value == "" || reWhiteSpace.test(state.value)){
			MessageHelper.tcpFormErrorHandleClient(state.id, MessageHelper.messages["ERROR_StateEmpty"]);
			if(validateOK) {form.state.focus();}
			validateOK = false;
		}
		if(!MessageHelper.isValidUTF8length(state.value, 128)){
			MessageHelper.tcpFormErrorHandleClient(state.id, MessageHelper.messages["ERROR_StateTooLong"]);
			if(validateOK) {form.state.focus();}
			validateOK = false;
		}
		*/
		
		
		if(form.orderConfirmFlag == null || form.orderConfirmFlag == undefined){
		form["zipCode"].value = trim(form["zipCode"].value);		
		/*if(form["zipCode"].value=="" || reWhiteSpace.test(form["zipCode"].value)){ 
			MessageHelper.tcpFormErrorHandleClient(form["zipCode"].id, MessageHelper.messages["ERROR_ZipCodeEmpty"]);
			zipCodeInvalid = true;
			if(validateOK) {form.addressField3.focus();}
			validateOK = false;
		}*/		
		
		
		/*if(!MessageHelper.isValidUTF8length(form["zipCode"].value, 40)){ 
			MessageHelper.tcpFormErrorHandleClient(form["zipCode"].id, MessageHelper.messages["ERROR_ZipCodeTooLong"]);
			if(validateOK) {form.addressField3.focus();}
			validateOK = false;
		}
		if(!MessageHelper.isValidCharStr(form["zipCode"].value)){ 
			MessageHelper.tcpFormErrorHandleClient(form["zipCode"].id, MessageHelper.messages["ERROR_ZipCodeHasSpecialChar"]);
			if(validateOK) {form.addressField3.focus();}
			validateOK = false;
		}*/
		validateOK = AddressHelper.performZipValidation(form,validateOK);
						
		/*var country = form["country"];
		country.value = trim(country.value);
		if(country.value == null || country.value == "" || reWhiteSpace.test(country.value)){
			MessageHelper.tcpFormErrorHandleClient(state.id, MessageHelper.messages["ERROR_CountryEmpty"]);
			if(validateOK) {form.country.focus();}
			validateOK = false;
		}*/
		
		
		
		
		
		form["phone1"].value = trim(form["phone1"].value);
		form["phone1"].value = form["phone1"].value.replace(/[^0-9]/g, "");
		if(form["phone1"].value == "" || reWhiteSpace.test(form["phone1"].value)){
			MessageHelper.tcpFormErrorHandleClient(form["phone1"].id, MessageHelper.messages["ERROR_PhonenumberEmpty"]);
			if(validateOK) {form.phone1.focus();}
			validateOK = false;
		}
		if(!MessageHelper.isValidUTF8length(form["phone1"].value, 10)){ 
			MessageHelper.tcpFormErrorHandleClient(form["phone1"].id, MessageHelper.messages["ERROR_PhoneTooLong"]);
			if(validateOK) {form.phone1.focus();}
			validateOK = false;
		}
		if(!MessageHelper.IsValidPhone2(form["phone1"].value)){
			MessageHelper.tcpFormErrorHandleClient(form["phone1"].id, MessageHelper.messages["ERROR_INVALIDPHONE"]);
			if(validateOK) {form.phone1.focus();}
			validateOK = false;
		}
		if(!MessageHelper.isValidCharStr(form["phone1"].value)){
			MessageHelper.tcpFormErrorHandleClient(form["phone1"].id, MessageHelper.messages["ERROR_PhoneHasSpecialChar"]);
			if(validateOK) {form.phone1.focus();}
			validateOK = false;
		}		
		
		
		
		/* Checks whether the customer has registered for promotional e-mails. */
	    
		/*
		if(form.demographicField1 &&  form.demographicField1.checked){
		    form.demographicField1.value = 1;
		}
		else {
			form.demographicField1.value = 0;
		}		
		*/
		}
		if(window.location.href.indexOf('TCPOrderShippingBillingConfirmationView') >= 0){
			if(form.termsCheck && !form.termsCheck.checked){
				validateOK = false;
				
			}
		}
		else{
			if($('#email_terms_checkbox > input') && !$('#email_terms_checkbox > input').attr('checked')){
				validateOK = false;
				
			}
								
		}
		
		if (validateOK == false)
			return false;
		
		
		setCurrentId('WC_TCPUserRegistrationAddForm_Link_Submit_1');
		cursor_wait();
		
		if(!submitRequest()){
			return;
		}
		
		form.submit();
		//form.reset();
		
		/*try{
			executeSearch(form,5000,'TCPUserRegistrationAddForm');
		}catch(e){
			setCurrentId('WC_TCPUserRegistrationAddForm_Link_Submit_1');
			cursor_wait();*/
			
			/* For Handling multiple clicks. */
			/*if(!submitRequest()){
				return;
			}
			form.submit();
		}*/
	},
	
	/** 
	 * This function is called when the Login button is clicked on the Login page. All the fields containing customer
	 * information are validated and PersonProcessServicePersonRegistration is called. 
	 * @param {string} form The name of the registration form containing all the customer information.
	 */
	prepareSubmitLogon:function (form)
	{
		var validateOK = true;
		
	    reWhiteSpace = new RegExp(/^\s+$/);	    

	
		/** Uses the common validation function defined in AddressHelper class for validating first name, 
		 *  last name, street address, city, country/region, state/province, ZIP/postal code, e-mail address and phone number. 
		 */	    

		form["logonId1"].value = trim(form["logonId1"].value);
		if((form["logonId1"].value == "" || reWhiteSpace.test(form["logonId1"].value))){
			MessageHelper.tcpFormErrorHandleClient(form["logonId1"].id, MessageHelper.messages["ERROR_INVALIDEMAILFORMAT"]);
			if(validateOK) {form.logonId1.focus();}
			validateOK = false;
		}
		if(!MessageHelper.isValidUTF8length(form["logonId1"].value, 256)){ 
			MessageHelper.tcpFormErrorHandleClient(form["logonId1"].id, MessageHelper.messages["ERROR_EmailTooLong"]);
			if(validateOK) {form.logonId1.focus();}
			validateOK = false;
		}
		
		// allow super user to login with "wcsadmin"
		if (form["logonId1"].value!="wcsadmin" && reWhiteSpace.test(form["logonId1"].value))
		{
			if(!MessageHelper.isValidEmail(form["logonId1"].value)){
				MessageHelper.tcpFormErrorHandleClient(form["logonId1"].id, MessageHelper.messages["ERROR_LOGIN_INVALIDEMAILFORMAT"]);
				if(validateOK) {form.logonId1.focus();}
				validateOK = false;
			}
		}

		if(form.logonPassword1 != null && reWhiteSpace.test(form.logonPassword1.value) || form.logonPassword1.value == ""){ 
			MessageHelper.tcpFormErrorHandleClient(form.logonPassword1.id,MessageHelper.messages["ERROR_PasswordEmpty"]);
			if(validateOK) {form.logonPassword1.focus();}
			validateOK = false;
			}

		if (validateOK == false){
			$('.page-error').css('display','none');
			return false;
		}
			
		
		setCurrentId('WC_AccountDisplay_links_2');
		cursor_wait();		
		
		/* For Handling multiple clicks. */
		if(!submitRequest()){
			return;
		}
		
		var emailId = form["logonId1"].value;
		form.submit();
		//form.reset();
	},

	/**
	 * Sets the flag which indicates whether 'AjaxMyAccount' feature is enabled or not.
	 * Based on this, relevant code is generated.
	 * @param {boolean} temp A value that is set temporarily.
	 */

	setAjaxVar: function(temp)
	{
		this.ajaxMyAccountEnabled = temp;
	},
	
	/**
	 * Gets the flag which indicates whether 'AjaxMyAccount' feature is enabled or not.
	 * Based on this, relevant code is generated.
	 * 
	 * @return {boolean} value of the flag.
	 */

	getAjaxVar: function()
	{
		return(this.ajaxMyAccountEnabled);
	},
	
	/**
	 *  This function validates the customer's input for age. If the user is under age, pop up a message to ask the user to review the store policy.
	 *  @param {string} The name of the form containing personal information of the customer.
	 */
	validateAge: function(form){
		
		var birth_year = parseInt(form.birth_year.value,10);
		var birth_month = parseInt(form.birth_month.value,10);
		var birth_date = parseInt(form.birth_date.value,10);
		
		if (birth_year == 0 || birth_month == 0 || birth_date == 0) {
			return;
		}
		
		var curr_year = parseInt(form.curr_year.value,10);
		var curr_month = parseInt(form.curr_month.value,10);
		var curr_date = parseInt(form.curr_date.value,10);
		
		/*Check whether age is less than 13, if so, pop up a message to ask the user to review the store policy. */
		if((curr_year - birth_year) < 13){
			alert(MessageHelper.messages["AGE_WARNING_ALERT"]);
		}else if((curr_year - birth_year) == 13){
			if(curr_month < birth_month){
				alert(MessageHelper.messages["AGE_WARNING_ALERT"]);
			}else if((curr_month == birth_month) && (curr_date < birth_date)){
				alert(MessageHelper.messages["AGE_WARNING_ALERT"]);
			}
		}
	},
	
	/**
	  *	This function is used when "Age" option is changed.
	  * This will show one alert message if the user age is under 13.
	  * @param {string} form The name of the registration form containing customer's age.
	  */

	checkAge:function (form)
	{
		if(form.age.value==1) {
			alert(MessageHelper.messages["AGE_WARNING_ALERT"]);
		}
	},
	
	/**
	  *	This function is used to disable copy/cut/paste on an input.
	  * @param {e} Event.
	  */	
	copyCutPasteChecker:function(e) {
		e.preventDefault(); //disable cut,copy,paste
		console.log(this.id+":"+e.data.message);
		MessageHelper.tcpFormErrorHandleClientTimeSpan(this.id, MessageHelper.messages[e.data.message],1000); 
	},
	/**
	 * This Function is used to change Value For Enable Mobile Capture Check box
	 */
	enableMobileCapture:function () {
		
		if(document.getElementById('demographicField3').checked){
			document.getElementById('demographicField3').value="Y";
		}else{
			document.getElementById('demographicField3').value="N";
		}
	}
}
