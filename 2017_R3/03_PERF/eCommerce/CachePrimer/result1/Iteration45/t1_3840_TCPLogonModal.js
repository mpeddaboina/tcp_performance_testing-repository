TCPLogonModalJS={
	

	buildModalLink:function() 
	{
		$('a.openModalLogon').each(function() {
			var $link = $(this);
			$link.click(function() {
				setCurrentId($link.attr('id'));
				if(!submitRequest()){
					return;
				}
				
				if($("#logonModalDialog")){
					$("#logonModalDialog").empty();
					$("#logonModalDialog").remove();
				}
				showProgressBar();
				
				setTimeout(function(){
					$("#progress_bar").hide();
					cursor_clear();
				},2000);
				
				var $dialog = $('<div id="logonModalDialog"></div>').load(
						$link.attr('href').replace('#',''),
				  function (responseText, textStatus, XMLHttpRequest) {  
					cursor_clear();
					if (textStatus == "success") {
						$('.bundlePageModal').css('display','none'); 
						$('.ui-dialog-title').html($link.attr('title'));
						$dialog.dialog('open');
					} 
					if (textStatus == "error") {     
						if (typeof console == "undefined") {
						    window.console = {
						        debug: function () {}
						    };
						}
						console.debug("error loading logon modal");
					}   
				}).dialog({
					autoOpen: false,
					modal:true,
					resizable:false,
					draggable:false,
					close: function() {
					 // Remove the dialog elements
						$(this).dialog("destroy");
		                // Remove the left over element (the original div element)
						$(this).remove();
					},
					width: 'auto',
					height: 'auto',
					title:'<img id="bundlePageModal" class="bundlePageModal" src="'+getImageDirectoryPath()+'images/tcp/ajax-loading.gif" />'   
				});
				return false;		
			
			});	
		});
	},

	/** 
	 * This function is called when the Login button is clicked on the Login Modal page. All the fields containing customer
	 * information are validated and PersonProcessServicePersonRegistration is called. 
	 * @param {string} form The name of the registration form containing all the customer information.
	 */
	prepareSubmit:function (form)
	{
		var validateOK = true;
		
	    reWhiteSpace = new RegExp(/^\s+$/);	    

	
		/** Uses the common validation function defined in AddressHelper class for validating first name, 
		 *  last name, street address, city, country/region, state/province, ZIP/postal code, e-mail address and phone number. 
		 */	    

		form["logonId"].value = trim(form["logonId"].value);
		MessageHelper.tcpHideErrorHandleClientSpan(form["logonId"].id);
		if((form["logonId"].value == "" || reWhiteSpace.test(form["logonId"].value))){
			MessageHelper.tcpFormErrorHandleClient(form["logonId"].id, MessageHelper.messages["ERROR_EmailEmpty"]);
			if(validateOK) {form.logonId.focus();}
			validateOK = false;
		}
		if(!MessageHelper.isValidUTF8length(form["logonId"].value, 256)){ 
			MessageHelper.tcpFormErrorHandleClient(form["logonId"].id, MessageHelper.messages["ERROR_EmailTooLong"]);
			if(validateOK) {form.logonId.focus();}
			validateOK = false;
		}
		
		// allow super user to login with "wcsadmin"
		if (form["logonId"].value!="wcsadmin")
		{
			if(!MessageHelper.isValidEmail(form["logonId"].value)){
				MessageHelper.tcpFormErrorHandleClient(form["logonId"].id, MessageHelper.messages["ERROR_INVALIDEMAILFORMAT"]);
				if(validateOK) {form.logonId.focus();}
				validateOK = false;
			}
		}
		MessageHelper.tcpHideErrorHandleClientSpan(form.logonPassword.id);
		if(form.logonPassword != null && reWhiteSpace.test(form.logonPassword.value) || form.logonPassword.value == ""){ 
			MessageHelper.tcpFormErrorHandleClient(form.logonPassword.id,MessageHelper.messages["ERROR_PasswordEmpty"]);
			if(validateOK) {form.logonPassword.focus();}
			validateOK = false;
			}

		if (validateOK == false)
			return false;
		
		setCurrentId('WC_TCPLogonModal_links_1');
		
			 form["URL"].value = 
				 'AjaxLogonForm?catalogId='+CommonControllersDeclarationJS.catalogId+
				 '&langId='+CommonControllersDeclarationJS.langId+
				 '&storeId='+CommonControllersDeclarationJS.storeId;
		form.submit();
	},
	
	
	logOff:function (form) {
		$.ajax({
			type: "POST",
			url: getAbsoluteURL() + 'Logoff',
			data: {
		        "storeId":CommonControllersDeclarationJS.storeId,
				"catalogId":CommonControllersDeclarationJS.catalogId,
				"langId":CommonControllersDeclarationJS.langId
	  		}, 
				
			dataType: "html",//json
			timeout: 210000,
			success: function(result){
				form.submit();
			},
			error: function(){
				cursor_clear();
				//console.debug("error: " + this.status);
				return false;
			}
		});
		
	}
	
	
};
$("#WC_TCPCachedCommonToolbar_FormLink_LogonModal1").ready(TCPLogonModalJS.buildModalLink);