/*
 * This JS contains the variables and methods which are used to make the CQ Call and constructs the HTML from response.
 */
CQuotientJS = {

    cq: window.CQuotient || (window.CQuotient = {}),
    htmlResponse: '',
    modalURL: '',
    serverName: '',
    assetsDir: '',
    clientId: '',
    usstoreId: '',
    castoreId: '',
    enableQuickView: '',

    /*
     * This method helps in initialize the cq object
     */
    init: function() {
        this.cq.clientId = this.clientId;
        this.cq.widget = this.cq.widgets || (this.cq.widgets = []);
    },

    getCookiesByPreffix: function(key) {
        var pluses = /\+/g,


            encode = function(s) {
                return encodeURIComponent(s);
            },

            decode = function(s) {
                return decodeURIComponent(s);
            },

            stringifyCookieValue = function(value) {
                return encode(String(value));
            },

            parseCookieValue = function(s) {
                if (s.indexOf('"') === 0) {
                    // This is a quoted cookie as according to RFC2068, unescape...
                    s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
                }

                try {
                    // Replace server-side written pluses with spaces.
                    // If we can't decode the cookie, ignore it, it's unusable.
                    // If we can't parse the cookie, ignore it, it's unusable.
                    s = decodeURIComponent(s.replace(pluses, ' '));
                    return s;
                } catch (e) {}
            },

            read = function(s, converter) {
                var value = parseCookieValue(s);
                return $.isFunction(converter) ? converter(value) : value;
            },

            cookies = document.cookie ? document.cookie.split('; ') : [],
            values = {};

        for (var i = 0, l = cookies.length; i < l; i++) {
            var parts = cookies[i].split('='),
                name = decode(parts.shift()),
                cookie = parts.join('=');

            if (key && name.length >= key.length && key === name.substr(0, key.length)) {
                // If second argument (value) is a function it's a converter...
                result = read(cookie);
                values[name] = result;
            }
        }

        return values;
    },

    getUserId: function() {
        var cookieValue = this.getCookiesByPreffix("WC_USERACTIVITY_");

        for (var idx in cookieValue) {
            if (cookieValue.hasOwnProperty(idx)) {
                return idx.split("_").pop();
            }
        }

        return null;
    },

    /*
     * This method fetches the recommendations for the PDP widget
     */
    fetchPDPRecommendations: function(productId, price, markupId, noOfProds) {
        this.init();

        CQuotientJS.fetchPDPRecommendationsNext(productId, price, "generic-recommendations-next", noOfProds);

        if (storeId == this.usstoreId) {
            productId = "01_" + productId;
        }
        if (storeId == this.castoreId) {
            productId = "02_" + productId;
        }
        this.cq.widgets.push({
            recommenderName: 'pdp2',
            parameters: {
                anchors: [{
                    id: productId,
                    currentPrice: price
                }],
                cookieId: getCookie('s_vi'),
                userId: this.getUserId()
            },
            callback: function(data) {
                var xcross = $(CQuotientJS.buildResponseHTML(data.pdp2.recs.slice(0, 4), data.pdp2.displayMessage));
                var horizontal = $(CQuotientJS.buildResponseHTML(data.pdp2.recs.slice(4), data.pdp2.displayMessage));
                $('#' + markupId).append(xcross);
                $("<section class='product-recommendations bis'></section>").append(horizontal).insertAfter('#' + markupId);
                $("a.openModal2").removeClass("openModal2").addClass("js-quick-view-modal");
                //@malvarez: Removing the following line which was hiding the quick view button on the second recommendations.
                //$(".bis:not(#generic-recommendations-next) .quick-view").toggle();
            },
            errorCallback: function(data) {
                $('#' + markupId).hide();
                $("<section class='product-recommendations bis'></section>").hide();
            }
        });
        console.log('Exiting PDP Fetch call');
    },

    fetchPDPRecommendationsNext: function(productId, price, markupId, noOfProds) {
        this.init();
        if (storeId == this.usstoreId) {
            productId = "01_" + productId;
        }
        if (storeId == this.castoreId) {
            productId = "02_" + productId;
        }
        this.cq.widgets.push({
            recommenderName: 'pdp',
            parameters: {
                anchors: [{
                    id: productId,
                    currentPrice: price
                }],
                cookieId: getCookie('s_vi'),
                userId: this.getUserId()
            },
            callback: function(data) {
                var responseHTML = $(CQuotientJS.buildResponseHTML(data.pdp.recs, data.pdp.displayMessage));
                $('#' + markupId).append(responseHTML);

                // ugliest legacy pluggin below
                // var  noOfProdsinResponse = data.pdp.recs.length;
                // CQuotientJS.initBXSlider(markupId, responseHTML, noOfProds, noOfProdsinResponse	);
            },
            errorCallback: function(data) {
                $('#' + markupId).hide();
            }
        });
    },

    /*
     * This method fetches the recommendations for the shopping Bag widget
     */
    fetchshoppingBagRecommendations: function(productId, price, markupId, requestProds, noOfProds) {
        console.log('Entering shoppingBag Fetch call');
        var productIds = productId.split(',');
        var prices = price.split(',');
        var jsonReqArr = [];
        var productIdPrefix = '';

        if (storeId == this.usstoreId) {
            productIdPrefix = "01_";
        }
        if (storeId == this.castoreId) {
            productIdPrefix = "02_";
        }

        // Constructing requestProds Based on the number of products mentioned
        // in property file
        for (var i = 1; i <= requestProds; i++) {
            if (i > productIds.length - 1)
                break;
            jsonReqArr.push({
                id: productIdPrefix + productIds[productIds.length - i],
                currentPrice: prices[productIds.length - i]
            });
        }

        this.init();
        this.cq.getRecs(this.clientId, 'add2cart', {
            anchors: jsonReqArr,
            cookieId: getCookie('s_vi'),
            userId: this.getUserId()
        }, function(data) {
            console.log('Success');
            var responseHTML = CQuotientJS.buildResponseHTML(
                data.add2cart.recs, data.add2cart.displayMessage);
            var noOfProdsinResponse = data.add2cart.recs.length;
            CQuotientJS.initBXSlider(markupId, responseHTML, noOfProds, noOfProdsinResponse);
        });
        console.log('Exiting shoppingBag Fetch call');
    },

    /*
     * This method fetches the recommendations for the Personalized widget used
     * in homepage and my account
     *
     */
    fetchPersonalizedRecommendations: function(uId, markupId, noOfProds) {
        console.log('Entering personalized Fetch call');

        this.init();
        var cookie_id = getCookie('s_vi');
        var recName = '';
        if (storeId == this.usstoreId) {
            recName = "personalized";
        }
        if (storeId == this.castoreId) {
            recName = "personalized_can";
        }

        this.cq.widgets.push({
            recommenderName: recName,
            parameters: {
                userId: uId,
                cookieId: cookie_id
            },
            callback: function(data) {
                console.log("success");
                if (data.personalized) {
                    var responseHTML = CQuotientJS.buildResponseHTML(
                        data.personalized.recs,
                        data.personalized.displayMessage);
                    var noOfProdsinResponse = data.personalized.recs.length;
                } else {
                    var responseHTML = CQuotientJS.buildResponseHTML(
                        data.personalized_can.recs,
                        data.personalized_can.displayMessage);
                    var noOfProdsinResponse = data.personalized_can.recs.length;
                }

                CQuotientJS.initBXSlider(markupId, responseHTML, noOfProds, noOfProdsinResponse);
            },
            errorCallback: function(data) {
                $('#' + markupId).hide();
            }
        });
        console.log('Exiting personalized Fetch call');
    },

    /*
     * This method fetches the recommendations for the Top level Category widget
     */
    fetchCategoryRecommendations: function(categoryId, uId, markupId, noOfProds) {
        console.log('Entering Category Fetch call');

        var cookie_id = getCookie('s_vi');

        this.init();
        this.cq.widgets.push({
            recommenderName: 'pdept',
            parameters: {
                anchors: [{
                    id: categoryId
                }],
                cookieId: getCookie('s_vi'),
                userId: this.getUserId()
            },
            callback: function(data) {
                var responseHTML = CQuotientJS.buildResponseHTML(
                    data.pdept.recs, data.pdept.displayMessage);
                var noOfProdsinResponse = data.pdept.recs.length;
                CQuotientJS.initBXSlider(markupId, responseHTML, noOfProds, noOfProdsinResponse);
            },
            errorCallback: function(data) {
                $('#' + markupId).hide();
            }
        });
        console.log('Exiting Category Fetch call');
    },

    /*
     * This method builds the HTML that is shown on the Widget
     */
    buildResponseHTML: function(records, displayMessage) {
        // Use DOM manipulations to build HTML
        var responseHTML = "";
        var catEntIdArray = [];

        responseHTML += '<p class="section-title recs">' + displayMessage + '</p>';
        responseHTML += '<ul id="recommendations-slider">';
        var resultRecs = records;
        for (i = 0; i < resultRecs.length; i++) {
            var sliderItem = '<li id="' + resultRecs[i].id + '">';

            // construct product image
            var productURL = resultRecs[i].product_url.replace("www.childrensplace.com", CQuotientJS.serverName);
            var productImage = '<p class="product-image"><a href="' + productURL + '">';
            var productImageURL = resultRecs[i].image_url.replace("www.childrensplace.com", "www.childrensplace.com");
            productImage += '<img alt="' + resultRecs[i].product_name + '" src="' + productImageURL + '" />';
            productImage += '</a></p>';
            sliderItem += productImage;

            //construct product name
            var productName = '<p class="product-name"><a href="' + productURL + '">';
            productName += resultRecs[i].product_name;
            productName += '</a></p>';
            sliderItem += productName;

            //construct product price
            var productPrice = '<p class="product-price">';

            //add current price
            productPrice += '<span class="sale-price-rec">$' + resultRecs[i].promo_price + '</span>&nbsp;';

            // add regular price
            productPrice += '<span class="former-price-rec">Was : ' + resultRecs[i].original_price + '</span>';
            productPrice += '</p>';
            sliderItem += productPrice;

            // construct Quick View link - Populate the link value with the correct
            // URL for Quick View
            // CQuotientJS.modalURL.replace("productIdValue",resultRecs[i].id);
            if (CQuotientJS.enableQuickView) {
                var productQVURL = CQuotientJS.modalURL.replace("productIdValue", resultRecs[i].wcs_catentry_id);
                productQVURL = productQVURL.replace("www.childrensplace.com", CQuotientJS.serverName);
                var quickView = '<p class="quick-view"><a class="openModal2" link="' + productQVURL + '">QUICK VIEW+ <span class="off-screen">Opens Dialog</span></a></p>';
                sliderItem += quickView;
            }

            // close the slider item
            sliderItem += '</li>';
            responseHTML += sliderItem;
            catEntIdArray.push(resultRecs[i].wcs_catentry_id);
        }
        // close responseHTML
        responseHTML += '</ul>';
        CQuotientJS.htmlResponse = responseHTML;

        CQuotientJS.buildTealiumTags(catEntIdArray);
        return responseHTML;
    },

    /*
     * This method Sets up the BX slider used.
     */
    initBXSlider: function(markupId, responseHTML, slideSize, noOfProdsinResponse) {
        $('#' + markupId).html(responseHTML);
        $('#' + markupId + ' #recommendations-slider').bxSlider({
            minSlides: slideSize,
            maxSlides: slideSize,
            slideWidth: 225,
            slideMargin: 5,
            pager: false,
            moveSlides: 1,
            controls: false, // noOfProdsinResponse > slideSize,
            onSliderLoad: function() {
                var minHeight = "340px";
                if (CQuotientJS.enableQuickView != true) {
                    minHeight = "240px";
                }
                $('#' + markupId + " .bx-viewport").css("min-height", minHeight);
            }
        });

        // initialize quick view modals
        $.ajaxSetup({cache:true});
        $.getScript(CQuotientJS.assetsDir + "javascript/tcp/TCPdialogs.js", null);
    },

    /*
     * This method builds the tealium tags that are needed on the page.
     */
    buildTealiumTags: function(catEntryArray) {
        var productObj = {
            'product_on_page': []
        };
        for (var j = 0; j < catEntryArray.length; j++) {
            productObj.product_on_page.push(catEntryArray[j]);
        }

        TCP.Tealium.merge(utag_data, TCP.Tealium.clean(productObj));
    }

};