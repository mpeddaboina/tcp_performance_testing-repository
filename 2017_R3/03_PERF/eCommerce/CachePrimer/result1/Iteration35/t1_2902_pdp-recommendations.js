RecommendationsJs = {
	buildRecomendationsHtml: function (products) {
		var html = '<div class="pdp-recommendations-head"><h2> You May Also Like  </h2></div>';
		html += '<ul class="recommended">';
		for (i = 0; i < products.length; i++) {
			var prod = products[i];
			html += '<li itemscope="itemscope" itemtype="http://schema.org/Product" id="slide_' + i + '_' + prod.id + '">';
			html += '<a href="' + prod.product_url + '">';
			html += '<img src="' + prod.image_url + '"  alt="' + prod.product_name + '" itemprop="image" ></a>';
			html += '<h3 class="product-name"><a href="' + prod.product_url + '">' + prod.product_name + '</a></h3>';
			html += '<div class="pricing"><strong itemprop="price"> $' + prod.promo_price + '</strong>';
			html += '<span> &nbsp; Was: ' + prod.original_price + '</span></div>';
			html += '<a class="openModal2 quick-view" link="' + unescape(prod.product_quick_view_url) + '">QUICK VIEW+ <span class="off-screen">Opens Dialog</span></a>';
			html += '</li>';
		}
		html += '</ul>';
		return html;
	},
	init: function () {
        // Deactivating Outfits Slider until further instructions (AND-204)
        return;
        /*
		(function ($) {
			$(document).ready(function () {
				if (!window.categoriesAndPartNumbers) {
					return;
				}
				$.ajax({
					url: "/shop/TCPOutfitSliderDisplay",
					data: {
						categoriesAndPartNumbers: window.categoriesAndPartNumbers,
						//categoriesAndPartNumbers: '47501,47526|2000560_41,2005758_AU,2002718_64,1125070_10,2009936_10',
						storeId: window.storeId || 10151,
						catalogId: window.catalogId || 10551,
						langId: -1
					},
					success: function (result) {
						if (!result || !result.products || result.products.length === 0) {
							return;
						}
						var relatedProducts = [];
						for (i = 0; i < result.products.length && i < 5; i++) {
							var relatedProduct = {
								id: i + 1,
								product_url: result.products[i].productURL,
								product_quick_view_url: result.products[i].productQuickViewURL,
								image_url: '/wcsstore/GlobalSAS/images/tcp/products/500/' + result.products[i].productPartNumber + '.jpg',
								product_name: result.products[i].productName,
								promo_price: result.products[i].offerPrice,
								original_price: result.products[i].listPrice,
								wcs_catentry_id: i + 1
							};
							relatedProducts.push(relatedProduct);
						}
						$('.js-outfit-slider').html(RecommendationsJs.buildRecomendationsHtml(relatedProducts));
					}
				});
			});
		})($);
		*/
	}
};

// CHANGE RECOMMENDATIONS WHEN CHANGING PRODUCTS IN PDP
try {
	if (window.switchProduct) {
		var oldSwitchProduct = switchProduct,
			recsCache = {};

		window.switchProduct = function(productId) {
			oldSwitchProduct(productId);

			if (window.productDetails) {
				var cq = window.CQuotient || (window.CQuotient = {}),
					locale = window.storeId == "10152" ? "02_" : "01_",
					partNumber = (window.productDetails[productId] || {}).partNumber,
					price = (window.productDetails[productId] || {}).offerPrice || (window.productDetails[productId] || {}).listPrice,
					parameters = {
						anchors: [{
							id: locale + partNumber,
							currentPrice: price
						}],
						cookieId: 	getCookie("s_vi"),
						userId: 	CQuotientJS.getUserId(),
						pageId: 	"PDP"
					},

					loadRecomendations = function(data) {
						try {
							if (data && data.pdp2 && data.pdp2.recs && data.pdp2.recs.length) {
								var xcross = $(CQuotientJS.buildResponseHTML(data.pdp2.recs.slice(0, 4), data.pdp2.displayMessage));
								$("#WC_CQ_Container_PDP").empty().append(xcross);
								$("a.openModal2").removeClass("openModal2").addClass("js-quick-view-modal");
								recsCache[partNumber] = data;
							}
						} catch (errores) {
							console.error(errores.stack);
						}
					};

				if (recsCache[partNumber]) {
					loadRecomendations(recsCache[partNumber]);
				} else {
					if (!cq.getRecs) {
						cq.widgets.push({
							recommenderName: "pdp2",
							parameters: parameters,
							callback: loadRecomendations,
							errorCallback: function(data) {
								console.error();
							}
						});
					} else {
						cq.getRecs(
							cq.clientId,
							"pdp2",
							parameters,
							loadRecomendations
						);
					}
				}
			}
		}
	}
} catch(ex) {
	console.log(ex);
}
