BazaarVoice = {





    processLogin: function(data) {
        try {

            if (data.errorMessage !== undefined) {
                $(".modal .transaction-messaging").addClass("error active").text(data.errorMessage);
            }

            if (data.responseCode === "LoginSuccess") {


                location.reload();



            }
        } catch (e) {
            console.log(e);
        }

    },
    showLoginModal: function() {
        try {

            $(".modal .espot").hide();

            var $modal = $(".my-wishlist:visible .modal").eq(0).show();


            $modal.off("click.submitLogin").on("click.submitLogin", ".login input:eq(2)", function(e) {
                e.preventDefault();
                App.Login.login($modal.find(".login input[name=email]").val(), $modal.find(".login input[name=password]").val(), BazaarVoice.processLogin, function(data) {
                    var msg = data ? data.errorMessage : null;
                    $(".modal .transaction-messaging").html(msg || App.Messages[App.Config.defaultLocale].errRetrievingInformation);
                    $(".modal .transaction-messaging").addClass("error active").html($(".modal .transaction-messaging").text());
                });

            });
            $modal.off("keyup.enter").on("keyup.enter", ".login input:eq(1)", function(e) {
                if (e.keyCode == 13) {
                    BazaarVoice.processLogin();
                }
            });

            $(".modal a.close").unbind('click').bind('click', function() {
                $(".modal .espot").show();
                var errorMessageElement = $(".modal .transaction-messaging");
                errorMessageElement.text("");
                errorMessageElement.removeClass("error active");

                $modal.hide();
                modal = null;

            });
            $(".modal .login a").bind("click", function(e) {
                var text = $(e.currentTarget).text().toUpperCase();
                var forgotPwd = "forgot password?";
                var createAccount = "create an account";
                console.log("store id:", storeId);
                if (text === forgotPwd.toUpperCase()) {
                    window.location.href = "/shop/ResetPasswordGuestErrorView?state=forgetpassword&catalogId=10551&langId=-1&storeId=" + storeId;
                } else if (text === createAccount.toUpperCase()) {
                    window.location.href = "/shop/LogonForm?catalogId=10551&myAcctMain=1&langId=-1&storeId=" + storeId + "#tabs-2";
                }
            });
        } catch (e) {
            console.log(e);
        }

    },

    get firstname() {
        return getCookie("tcp_firstname") || null;
    },

    isRegistered: function() {
        return getCookie("tcp_isregistered") || null;
    },
    isRegisteredAndLoggedin: function() {
        return (BazaarVoice.isRegistered != null && BazaarVoice.firstname != null);
    },


    redirectToLogin: function() {
        var currentUrl = window.location.href;
        if (currentUrl.indexOf("storeId=10151") > 0 || currentUrl.indexOf("/shop/us") > 0) {

            window.location.href = "/shop/LogonForm?catalogId=10551&myAcctMain=1&langId=-1&storeId=10151";
        } else {

            window.location.href = "/shop/LogonForm?catalogId=10552&myAcctMain=1&langId=-1&storeId=10152";
        }

    },

    isWishListEnabled: true,

    login: function() {
        try {

            $('#BVRRContainer').bind("DOMSubtreeModified", function() {
                console.log("BVRRContainer loaded");
                if ($("#BVRRContainer").find("ul#BVSEOSDK_meta").length == 0) {
                    $("#BVRRContainer").append($("#BVSEOContainer").html());
                    $("#BVSEOContainer").empty();
                }
                if ($("#BVRRContainer").find("button.bv-write-review").length > 0 || $("#BVRRContainer").find(".bv-write-review-label").length > 0) {
                    $('#BVRRContainer').unbind("DOMSubtreeModified");

                    if (!BazaarVoice.isRegisteredAndLoggedin()) {
                        if (BazaarVoice.isWishListEnabled) {
                            $(".my-wishlist .modal form legend").text("LOG IN");
                            $("button.bv-write-review").unbind().bind('click', function(e) {
                                e.preventDefault();
                                BazaarVoice.showLoginModal();
                                return false;
                            });
                            $(".bv-write-review-label").unbind().bind('click', function(e) {
                                e.preventDefault();
                                BazaarVoice.showLoginModal();
                                return false;
                            });


                        } else {

                            $("button.bv-write-review").unbind("click");
                            $("button.bv-write-review").bind('click', function(e) {
                                e.preventDefault();
                                BazaarVoice.redirectToLogin();
                                return false;

                            });


                        }
                    }

                }
            });


        } catch (err) {
            console.log(err);
        }

    }
}
