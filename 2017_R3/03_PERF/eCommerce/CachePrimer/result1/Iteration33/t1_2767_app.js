/*! 2017-04-18 14:50 */
 window.AppReleaseVersion = "2017-04-18 14:50";
 (function($) {

// Source: src/js/app.js
App = typeof App == 'undefined' ? {} : App;

$.extend(App, {
	$: $,

	UI: {
		elements: {
			defaultErrorContainer: ".error.wl"
		}
	},

	Components: {
        // placeholder namespace for all components
    },

    QueryString: {
        get: function(name, href) {
            return this.hash(href)[name];
        },

        hash: function(href) {
            var base = href ? href : (document.location.hash.length > 0 ? document.location.hash.substr(1) : document.location.href.split("?")[1]);
            base = (base && base.indexOf("#") > -1) ? base.substr(0, base.indexOf("#")) : base;
            base = (base && base.indexOf("?") > -1) ? base.substr(base.indexOf("?") + 1) : base;

            var sp = base ? base.replace(/\&amp\;/g, "&").split("&") : [],
                h = {},
                i = 0;

            $.each(sp, function(i, item) {
                var kv = item.split("=");
                h[kv[0]] = decodeURIComponent(kv[1]);
            });

            return h;
        },

        replace: function(k, v, href) {
            var h = this.hash(href),
                base = href ? href.split("?")[0] : "";

            base = base.split("#")[0]; // remove hashtag (just in case )
            h[k] = decodeURIComponent(v);

            return base + "?" + $.param(h);
        }
    },

	init: function ($elements) {

		// window.onerror = null;

		window.on = $.on;
		window.off = $.off;

		var $ajax = $.ajax;
		App.ajax = function(opts) {
			if (opts.data && !opts.suffixAsQS) {
				opts.data = $.extend({}, App.Transport.SUFFIX, opts.data);
			} else if (opts.suffixAsQS) {
				var amp = opts.url.indexOf("?") > -1 ? "&": "?";
				opts.url += amp + App.Transport.getCommandSuffix();
			}

			return $ajax.apply(this, arguments);
		};

		$elements = $elements ? $elements.find("[data-component]") : $(".component");
		$elements.each( function() {
			if (!$(this).data("component")) {
				return;
			}
			var n = $(this).data("component").split(","),
				c = null,
				i = 0;

			for (i = 0; i < n.length; i++) {
				try {
					App.Error.log(App.Messages.initializing + n[i]);

					n[i] = n[i].trim();
					c = App[n[i]];

					if (c) {
						if (c.destroy) {
							if (!App.destroyable) {
								App.destroyable = [];
							}
							App.destroyable.push(n[i]);
						}

						// if not already initialized for component c
						if (!$(this).data(n[i])) {
							if (typeof c === "function") {
								var instance = new c();
								$(this).data(n[i], true);
								instance.init($(this));
							} else {
								$(this).data(n[i], true);
								c.init($(this));
							}
						}
					} else {
						App.Error.log(App.Messages.componentNotFound + n[i]);
					}
				} catch (e) {
					App.Error.log(App.Messages.errorInitializingComponent + n[i]);
				}
			}
		});

         /**
         * Prevent tab focus on background when the modal is open
         */
        $(document).off("keydown.modal").on("keydown.modal", function(e) {
            $modal = $("[data-component=Login]");
            if ($modal.is(":visible")) {
                var $target = $(e.target),
                    $focusables = $modal.find("a,[tabindex],input"),
                    $first = $focusables.first(),
                    $last = $focusables.last();

                if (e.keyCode == 9) {

                    if ($target[0] != $last[0] && $target.closest($modal).length > 0) {
                        $target.focus();
                    } else {
                        e.preventDefault();
                        $first.focus();
                    }

                }
            }
        });
	},


	// init: function () {},

    destroy: function() {
        if (!this.destroyable) {
            return;
        }
        var i;
        for (i = 0; i < this.destroyable.length; i++) {
            // console.log("DESTROY: " + this.destroyable[i]);
            App.Components[this.destroyable[i]].destroy();
        }
        this.destroyable = [];
    },

	initRopis: function() {
		if (window.Rapp) {
			if (!Rapp.initialized) {
				Rapp.init();
			} else {
				Rapp.initCTA();
				Rapp.initBopisCTA();
			}
			return;
		}

		if (window.$) {
			window.$legacyjQuery = $.noConflict();
		}

		var script = document.createElement("script");
		script.type = "text/javascript";
		script.src = "/wcsstore/GlobalSAS/javascript/tcp/ropis/lib.js";
		$("head").append(script);

		var script = document.createElement("script");
		script.type = "text/javascript";
		script.src = "/wcsstore/GlobalSAS/javascript/tcp/ropis/app.js";
		$("head").append(script);

		var script = document.createElement("script");
		script.type = "text/javascript";
		script.src = "/wcsstore/GlobalSAS/javascript/tcp/ropis/templates.js";
		$("head").append(script);

		if ($(document).find("head > link [href='/wcsstore/GlobalSAS/css/tcp/ropis.page.css']").length === 0) {
			$(document).find("head").append("<link rel=\"stylesheet\" href=\"/wcsstore/GlobalSAS/css/tcp/ropis.page.css\" />");
		}

		setTimeout(function() {
			if (window.$legacyjQuery) {
				window.$ = window.jQuery = window.$legacyjQuery;
			}

			if (!Rapp.initialized) {
				Rapp.init();
			} else {
				Rapp.initCTA();
				Rapp.initBopisCTA();
			}
		}, 5000);
	}
});

$(function() { App.init(); });
;
// Source: src/js/comp/addWishlist.js
$.extend(App, {
    AddWishlist: {
        _wishlist: null,

        get wishlist() {
            return this._wishlist;
        },
        set wishlist(w) {
            this._wishlist = w;
        },

        init: function(callback) {
            try {
                this.initialize(callback);
                /* I'm not sure why we need to trigger twice
                if (window.appInsertContext != "PLP") {
                    this.initialize(callback);
                } else if (window.appInsertContext == "PLP" && this.wishlist != null) {
                    this.initialize(callback);
                }
				*/
            } catch (e) {
                console.log(e);
            }
        },

        initialize: function(callback) {
            App.Wishlist.getWishlist(function(data) {
                App.AddWishlist.loadListOptions(data);

                var $cart = $("table.bag-data"),
                    $myWishlist;

                App.AddWishlist.isCartPage = $cart.length > 0;

                if (App.AddWishlist.isCartPage && !App.AddWishlist.previousTrigger) {
                    App.Wishlist.initCache();
                    $myWishlist = $(".my-wishlist");

                    if ($myWishlist.length > 1) {
                        // more than the template
                        $myWishlist.not(".template").remove();
                    }

                    $myWishlist = $(".my-wishlist").eq(0).addClass("template").hide();
                    $myWishlist.find(".modal .login p").replaceWith("<p>You must have an account to create a wishlist. <a href=\"#create-account\" class=\"create-account\">Create an account</a></p>");

                    // I'm in cart page, duplicate add to wishlist buttons
                    $cart.find("td.item_remove").each(function() {
                        $(this).append($myWishlist.clone(true).removeClass("template").show());
                    });

                    $(document).off("added-to-wishlist").on("added-to-wishlist", function(e) {
                        try {
                            var js = $(e.target).parents(".item_remove").find("p a").attr("href").substr(11),
                                $tr = $(e.target).parents("tr");

                            $tr.find("td").each(function() {
                                $(this).html($("<div class='wl-animate' style='overflow: hidden;'></div>").append($(this).html()));
                            });

                            $tr.find("td").animate({ 
                                paddingTop: 0 ,
                                paddingBottom: 0
                            }, { 
                                duration: 1000 
                            }).find(".wl-animate").animate({ 
                                height: 0 
                            }, 
                            { 
                                duration: 1000, 
                                complete: function() {
                                    $tr.hide();
                                }
                            });

                            eval(js);
                        } catch(ex) {
                            console.log(ex);
                            console.log(ex.stack);
                        }
                    });

                    $cart = $myWishlist = null;
                }

                typeof callback === "function" && callback(data);
            });
        },

        setItemSize: function() {
            try {
                var fit = App.AddWishlist.getSelectedFit();
                if (fit !== null) {
                    categoryDisplayJS.setSelectedAttribute('Fit', fit);
                }
                categoryDisplayJS.setSelectedAttribute('Size', App.AddWishlist.getSelectedSize());
                var entitledSON = window["entitledItem_" + App.AddWishlist.getSelectedColor()];
                categoryDisplayJS.setEntitledItems(entitledSON);
            } catch (e) {
                console.log(e);
                console.log(e.stack);
            }
        },

        getSelectedColor: function() {
            var $modal = $(".root-product-quick-view");

            if ($modal.length > 0) {
                return $modal.find("[name=productColor]:checked").val() || null;
            } else {
                return ($("#prod-swatches .selected").attr("id") || "").substring(15);
            }
        },

        getSelectedSize: function() {
            var $modal = $(".root-product-quick-view"),
                $tr, idx;

            if ($modal.length > 0) {
                return $modal.find("[name=productSize]:checked").val() || null;
            } else {
                if (App.AddWishlist.previousTrigger) {
                    $tr = $(App.AddWishlist.previousTrigger).parents("tr");

                    if ($tr.length > 0) {
                        idx = $.inArray($tr[0], $tr.parent().children("tr")) + 1;
                        return $tr.parents("table").find("[name=catalogId_" + idx + "]").val();
                    }
                }

                return $('#select_TCPSize .selected a').html() ? $('#select_TCPSize .selected a').html().trim() : null;
            }
        },

        getSelectedFit: function() {
            var $modal = $(".root-product-quick-view");

            if ($modal.length > 0) {
                return $modal.find("[name=productFit]:checked").val() || null;
            } else {
                return $('#select_TCPFit .selected a').html() ? $('#select_TCPFit .selected a').html().trim() : null;
            }
        },

        getItemId: function() {
            var $modal = $(".root-product-quick-view"),
                $tr, idx;

            if ($modal.length > 0) {
                return $modal.find("[name=productSize]:checked").val() || null;
            } else {
                if (App.AddWishlist.previousTrigger) {
                    $tr = $(App.AddWishlist.previousTrigger).parents("tr");

                    if ($tr.length > 0) {
                        idx = $.inArray($tr[0], $tr.parent().children("tr")) + 1;
                        return $tr.parents("table").find("[name=catalogId_" + idx + "]").val();
                    }
                }

                return categoryDisplayJS.getCatalogEntryId();
            }
        },

        getDefaultName: function() {
            return App.User.firstname + "'s Wishlist";
        },

        createDefaultWishlist: function() {
            var defaultName = App.AddWishlist.getDefaultName(),
                item, notFound = false;
            item = App.AddWishlist.getItemId();

            App.AddWishlist.init(function(data) {
                $.each(data, function(index, value) {
                    if (value.status == "Default") {
                        var $tr = $(App.AddWishlist.previousTrigger).parents("tr"),
                            quantity = $tr.find(".item-quantity select").val() || $("#quantity").val() || $(".product-information-form").find("[name=productQuantity]").val() || 1;

                        App.Wishlist.addItemToWishlist(value.giftListExternalIdentifier, item, quantity, function(d) {
                            if (d && d.giftListItemId !== undefined) {
                                App.AddWishlist.successHandler(d);
                            } else {
                                App.AddWishlist.errorHandler(d);
                            }
                        }, function(e) {
                            App.AddWishlist.errorHandler(e);
                        });
                        return false;
                    } else {
                        notFound = true;
                    }
                });

                if (!data || data.length == 0 || notFound) {
                    App.AddWishlist.createDefaultWishlistZeroSets(item);
                }
            });
        },

        createDefaultWishlistZeroSets: function(item) {
            if (item === undefined) {
                item = App.AddWishlist.getItemId();
            }

            App.Wishlist.createWishlistWithItem(App.AddWishlist.getDefaultName(), item, function(data) {
                if (data && data.giftListItemId) {
                    App.AddWishlist.successHandler(data);
                } else if (data && data.errorMessage) {
                    App.AddWishlist.errorHandler(data);
                }

                return;
            });
        },

        processLogin: function(data) {
            try {
                if (data.errorMessage !== undefined) {
                    var msg = data ? data.errorMessage : null;
                    var errorSelector = ".modal:visible .transaction-messaging";
                    // If quickview is visible, changes de selector to asign the login error. 
                    if ($(".productLarge").find(".quick-view").length > 0) {
                        errorSelector = ".overlay-slot .modal .transaction-messaging";
                    }
                    $(errorSelector).addClass("error active").html(unescape(msg) || App.Messages[App.Config.defaultLocale].errRetrievingInformation);
                    $(errorSelector).html($(errorSelector).text());
                }

                if (data.responseCode === "LoginSuccess") {
                    $(".my-wishlist .modal").hide();
                    App.AddWishlist.createDefaultWishlist();
                    App.User.enableNavigationForRegisteredUsers();

                    // negrada de legacy
                    if (dijit.byId("MiniShoppingCart") !== undefined) {
                        dijit.byId("MiniShoppingCart").refresh();
                    }
                }
            } catch (e) {
                console.log(e);
            }
        },

        errorHandler: function(err) {
            App.AddWishlist.previousTrigger = App.AddWishlist.previousTrigger || document;

            if (err && err.errorCode == "CMN1039E") {
                App.Wishlist.forceReload();
                return;
            }

            var $myWishlist = $(App.AddWishlist.previousTrigger).parents(".my-wishlist"),
                $p = $myWishlist.find(".move-to-wishlist-options").addClass("enable-message"),
                $add, r, close = false;

            if ($p.length == 0) {
                $myWishlist.find(".add-to-default-wishlist").after(App.Templates["wishlist-move-to"]([]));
                $p = $myWishlist.find(".move-to-wishlist-options").addClass("enable-message");
                close = true;
            }

            $add = $myWishlist.find(".add-to-default-wishlist");
            if ($add.hasClass("collapsed")) {
                $add.toggleClass("expanded collapsed");
            }

            var errorOut = "Yikes! We couldn't add that item to your Wishlist. Please try again.";

            if (err && err.errorMessageKey === "_ERR_MORE_ITEM_IN_WISHLIST_ERROR") {
                errorOut = "This Wishlist has been filled to the max... and that's a lot of wishes! To continue adding, remove older items you no longer want.";
            }

            if (err && err.errorMessageKey === "_ERR_MORE_WISHLIST_ERROR") {
                errorOut = "Yikes! You've reached the maximum number of Wishlists. Please add to an existing list or delete a list to create a new one.";
            }

            r = $p.find(".transaction-messaging:eq(0)").removeClass("success").html(errorOut).removeClass("success").addClass("error active expanded");

            if ($p.find("ul li").length == 0) {
                r.addClass("no-wishlist");
            }

            App.AddWishlist.previousTrigger = null;

            $(document).off("mouseleave");

            setTimeout(function() {
                $p.removeClass("enable-message");
                r.empty().removeClass("sucesss error no-wishlist active");

                if (close) {
                    $add.toggleClass("expanded collapsed");
                    $(document).off("mouseleave").on("mouseleave", ".move-to-wishlist-options", function(e) {
                        $(this).siblings(".add-to-default-wishlist").removeClass("expanded").addClass("collapsed");
                    });
                }
            }, 10000);
        },

        getSelectedOption: function(list, selected) {
            var found = null;
            $.each(list, function(index, value) {
                if (value.giftListExternalIdentifier == selected.giftListId) {
                    found = value.nameIdentifier;
                }
            });
            return found || list[0].nameIdentifier;
        },

        successHandler: function(d) {
            App.AddWishlist.previousTrigger = App.AddWishlist.previousTrigger || document;
            var $myWishlist = $(App.AddWishlist.previousTrigger).parents(".my-wishlist");

            if (!App.AddWishlist.isCartPage) {
                App.AddWishlist.init(function() {
                    var $p = $myWishlist.find(".move-to-wishlist-options").addClass("enable-message"),
                        $add = $p.prevAll(".add-to-default-wishlist:eq(0)"),
                        usOrCa = (window.storeId === "10151") ? "us" : "ca",
                        r = $p.find(".transaction-messaging:eq(0)")
                            .html("You've successfully added an item to \"" + App.AddWishlist.getSelectedOption(App.AddWishlist._wishlist, d) + "\"! <a href='/shop/" + usOrCa + "/kids-baby-gifts-registry-wishlist?wishlistId=" + d.externalId[0] + "'>View your wishlist</a>")
                            .addClass("success active");

                    if ($add.hasClass("collapsed")) {
                        $add.toggleClass("expanded collapsed");
                    }

                    // triggering tracking omniture
                    try {
                        Rapp.OmnitureAnalytics.TrackLinkEvents.event52({
                            CurrentLocation : App.Tracking.getFriendlyLocation(),
                            wishlistName    : App.AddWishlist.getSelectedOption(App.AddWishlist._wishlist, d)
                        });
                    } catch (e) {
                        console.log(e);
                        console.log(e.stack);
                    }
                    
                    $(App.AddWishlist.previousTrigger).trigger("added-to-wishlist");
                    App.AddWishlist.previousTrigger = null;

                    setTimeout(function() {
                        $p.removeClass("enable-message");
                        r.removeClass("active").empty();
                    }, 10000);
                });
            } else {
                // triggering tracking omniture
                try {
                    Rapp.OmnitureAnalytics.TrackLinkEvents.event52({
                        CurrentLocation : App.Tracking.getFriendlyLocation(),
                        wishlistName    : App.AddWishlist.getSelectedOption(App.AddWishlist._wishlist, d)
                    });
                } catch (e) {
                    console.log(e);
                    console.log(e.stack);
                }
                
                $(App.AddWishlist.previousTrigger).trigger("added-to-wishlist");
                App.AddWishlist.previousTrigger = null;
            }
        },

        getItemSKU:function(event){
            var itemSku = "";
            try{
                //QuickView, will capture on PDP quick view as well
                itemSku = $("p.item-number").html() ? $("p.item-number").html().split(" ")[2].split("_")[0] : itemSku;
                //PDP, need to check for empty string becuse item-number exists and will mistake for quickview on pdp
                itemSku = $("#partNumber").length  && itemSku === ""  ? $("#partNumber").html().split(" ")[3].split("_")[0] : itemSku;
                //Cart, We need event passed in. Im grabing the row the item was in and refering to the datalay to get its SKU
                itemSku = event && itemSku === ""? utag_data.product_sku[$(event.target).closest("tr").parent().find("tr").index($(event.target).closest("tr"))] : itemSku;
                return itemSku;
            }catch(ex){
                console.log(ex);
                return "No SKU";
            }
        },

        displayLegacyError: function() {
            var $modal = $(".root-product-quick-view");

            if ($modal.length > 0) {
                Rapp.$($modal.find(".product-information-form [type=submit]")[0]).click();
            } else {
                $('#error_Size').css('display', 'block');
                setTimeout(function() {
                    $('#error_Size').css('display', 'none');
                }, 5000);
            }
        },

        loadListOptions: function(data) {
            App.AddWishlist.wishlist = data;

            // not logged in
            $(document).off("click.add-to")
                .off("click.move-to")
                .off("click.toggle-add-new-wishlist")
                .off("click.toggle-option")
                .off("click.enable-login");

            $(".my-wishlist .add-to-default-wishlist").removeClass("no-wishlist");

            if (!App.User.isRegisteredAndLoggedin()) {
                $(".my-wishlist .add-to-default-wishlist").addClass("no-wishlist");
                $(".my-wishlist .modal form legend").text("LOG IN");

                $(document).on("click.enable-login", ".add-to-default-wishlist", function(e) {
                    e.preventDefault();
                    App.AddWishlist.previousTrigger = this;

                    if (App.AddWishlist.getSelectedSize() !== null) {
                        App.AddWishlist.setItemSize();

                        try {                           
                            Rapp.OmnitureAnalytics.TrackLinkEvents.event63({
                                CurrentLocation    : App.Tracking.getFriendlyLocation(),
                                itemSku            : App.AddWishlist.getItemSKU(e)
                            });
                            //window.Rapp.OmnitureAnalytics.TrackLinkEvents.TrackEvent("event26");
                        } catch (e) {
                            console.log(e);
                            console.log(e.stack);
                        }

                        var $modal = $(".my-wishlist:visible .modal").eq(0).show();
                        $modal.closest(".ui-dialog").addClass("login-opened");


                        $modal.off("submit.submitLogin").on("submit.submitLogin", ".login form", function(e) {
                            e.preventDefault();
                            App.Login.login($modal.find(".login input[name=email]").val(), $modal.find(".login input[name=password]").val(), App.AddWishlist.processLogin, App.AddWishlist.processLogin);
                        }).off("click.close-modal").on("click.close-modal", "a.close", function(e) {
                            e.preventDefault();
                            $modal.hide();
                            $modal.closest(".ui-dialog").removeClass("login-opened");

                            $modal = null;
                        }).off("click.navigate-forgot-password").on("click.navigate-forgot-password", "a.forgot-password", function(e) {
                            e.preventDefault();
                            window.location.href = "/shop/ResetPasswordGuestErrorView?state=forgetpassword&catalogId=" + window.catalogId + "&langId=-1&storeId=" + window.storeId;
                        }).off("click.navigate-create-account").on("click.navigate-create-account", "a.create-account", function(e) {
                            e.preventDefault();
                            window.location.href = "/shop/LogonForm?new=Y&catalogId=" + window.catalogId + "&myAcctMain=&langId=-1&storeId=" + window.storeId + "#tabs-2";
                        });

                    } else {
                        App.AddWishlist.displayLegacyError();
                    }
                });

                return;
            }

            if (App.AddWishlist.wishlist.length === 0) {
                $(".my-wishlist .add-to-default-wishlist").addClass("no-wishlist");
            }

            $(document).on("click.toggle-option", ".add-to-default-wishlist", function(e) {
                e.preventDefault();
                App.AddWishlist.previousTrigger = this;

                // create default, then add to default
                if (App.AddWishlist.getSelectedSize() !== null) {
                    App.AddWishlist.setItemSize();

                    App.AddWishlist.createDefaultWishlist();
                } else {
                    App.AddWishlist.displayLegacyError();
                }

                try { 
                    Rapp.OmnitureAnalytics.TrackLinkEvents.event63({
                        CurrentLocation    : App.Tracking.getFriendlyLocation(),
                        itemSku            : App.AddWishlist.getItemSKU(e)
                    });
                }catch (e) {
                    console.log(e);
                    console.log(e.stack);
                }
            });

            // logged in with 1 or + records in memory (check cap)
            $(document).on("click.toggle-option", ".see-more", function(e) {
                e.preventDefault();
                App.AddWishlist.previousTrigger = this;

                if ($(".add-to-wishlist.move-to-wishlist").length !== 0) { // move-to scenario
                    $(this).prev().toggleClass("collapsed expanded");
                } else {
                    if (App.AddWishlist.getSelectedSize() !== null) {
                        $(this).prev().toggleClass("collapsed expanded");
                    } else {
                        App.AddWishlist.previousTrigger = null;
                        App.AddWishlist.displayLegacyError();
                    }
                }
            });

            // hide on out when out.
            $(document).off("mouseleave").on("mouseleave", ".move-to-wishlist-options", function(e) {
                $(this).siblings(".add-to-default-wishlist").removeClass("expanded").addClass("collapsed");
            });

            App.AddWishlist.wishlist.enabledNew = true;
            // $(".move-to-wishlist-options").removeClass("no-wishlist");
            $(".move-to-wishlist-options").remove();
            var cout = App.Templates["wishlist-move-to"](App.AddWishlist.wishlist);
            $(".add-to-wishlist").append(cout);

            App.Wishlist.setupCreateWishlist({
                target: ".add-to-wishlist li.create-wishlist",
                appendCallback: "append",
                reloadCallback: function(a, newWishlist) {

                    if (newWishlist && newWishlist.errorMessage) {
                        App.AddWishlist.errorHandler(newWishlist);
                        return false;
                    }

                    App.AddWishlist.setItemSize();
                    App.AddWishlist.previousTrigger = $(".add-to-wishlist li.create-wishlist");

                    try {
                        Rapp.OmnitureAnalytics.TrackLinkEvents.event63({
                            CurrentLocation    : App.Tracking.getFriendlyLocation()
                        });
                    } catch (e) {
                        console.log(e);
                        console.log(e.stack);
                    }

                    var $tr = $(App.AddWishlist.previousTrigger).parents("tr"),
                        quantity = $tr.find(".item-quantity select").val() || $("#quantity").val() || $(".product-information-form").find("[name=productQuantity]").val() || 1;

                    App.Wishlist.addItemToWishlist(newWishlist.externalId[0], App.AddWishlist.getItemId(), quantity, function(data) {
                        if (data && data.giftListItemId !== undefined) {
                            App.AddWishlist.successHandler(data);
                        } else {
                            App.AddWishlist.errorHandler(data);
                        }
                        App.Wishlist.clearCache();
                    }, function(err) {
                        App.AddWishlist.errorHandler(err);
                    });
                }
            });

            $(document).on("click.toggle-add-new-wishlist", ".move-to-wishlist-options li.create-wishlist a", function(e) {
                e.preventDefault();
                $(this).parent().find(".add-new-wishlist").toggle();
                $(this).hide();

                if (App.AddWishlist.wishlist.length >= window.wishlist_limit) {
                    $(".add-to-wishlist").addClass("max-limit");
                    $(this).parent().find(".result").addClass("inline-error active").html("Yikes! You've reached the maximum number of Wishlists. Please add to an existing list or delete a list to create a new one.");
                } else {
                    $(".add-to-wishlist").removeClass("max-limit");
                    $(this).parent().find(".result").removeClass("inline-error active").empty();
                }
            }).on("click.add-to", ".add-to-wishlist:not(.move-to-wishlist) li:not(.create-wishlist) a", function(e) {
                e.preventDefault();
                App.AddWishlist.previousTrigger = this;

                if (App.AddWishlist.getSelectedSize() !== null) {
                    App.AddWishlist.setItemSize();

                    try {                        
                        Rapp.OmnitureAnalytics.TrackLinkEvents.event63({
                            CurrentLocation    : App.Tracking.getFriendlyLocation(),
                            itemSku            : App.AddWishlist.getItemSKU(e)
                        });
                    } catch (e) {
                        console.log(e);
                        console.log(e.stack);
                    }

                    var $tr = $(App.AddWishlist.previousTrigger).parents("tr"),
                        quantity = $tr.find(".item-quantity select").val() || $("#quantity").val() || $(".product-information-form").find("[name=productQuantity]").val() || 1;

                    App.Wishlist.addItemToWishlist($(this).parent().data("id"), App.AddWishlist.getItemId(), quantity, function(data) {
                        if (data && data.giftListItemId !== undefined) {
                            App.AddWishlist.successHandler(data);
                        } else {
                            App.AddWishlist.errorHandler(data);
                        }
                    }, function(err) {
                        App.AddWishlist.errorHandler(err);
                    });
                } else {
                    App.AddWishlist.displayLegacyError();
                }
            }).on("click.move-to", ".move-to-wishlist li:not(.create-wishlist) a", function(e) {
                e.preventDefault();
                App.AddWishlist.previousTrigger = this;

                var wishlistId = $(this).parent().data("id"),
                    p = $(this).parents("article"),
                    deleteId = p.data("item-id"),
                    itemId = p.data("catentryid"),
                    qty = p.data("qty-requested");

                App.Wishlist.addItemToWishlist(wishlistId, itemId, qty, function(data) {

                    App.Wishlist.deleteItemFromwishlist(deleteId, function(r) {
                        // anything?
                    }, function(e) {
                        console.log(e);
                    });
                }, function(err) {
                    App.AddWishlist.errorHandler(err);
                });
            });

            var activeWishList = App.Wishlist.getActiveWishlist();
            if (activeWishList) {
                $(".move-to-wishlist .move-to-wishlist-options [data-id='" + activeWishList + "']").remove();
            }
        }
    }
});

App.MoveToWishlist = App.AddWishlist;;
// Source: src/js/comp/auth.js
$.extend(App, {
	Auth: {
		
		_username: null,
		_password: null,
		_token: null,
		_guestAccessKey: null,
		
		get guestAccesskey () { return this._guestAccessKey; },
		// get token () { return (App.Config.publishing !== "local") ? this._token || JSON.parse($.cookie("tcp-auth")) : null; },
		
		get token () { return; },
		
		get username () { return this._username; },
		get password () { return this._password; },
		set username (u) { this._username = u; },
		set password (p) { this._password = p; },
		set token (t) { this._token = t; },
		set guestAccessKey (k) { this._guestAccessKey = k; },
		
		init: function () {
			this.setFPOCredentials();
		},
		
		isGuest: function () {
			return (this.guestAccesskey !== null) ? true : false ;
		},
		
		fetchToken: function () {
			try {
				if (this.username === undefined || this.password === undefined) 
					return;
				
				var p = JSON.stringify({"logonId": this.username, "logonPassword": this.password});
				App.ajax({
					url: App.Transport.getCommandURL("getToken"),
					method: "POST",
					data: p,
					dataType: App.Config.defaultDataType,
					contentType: App.Config.defaultContentType,
					jsonp: App.Config.defaultJSONCallback,
					success: function (data) {
						$.removeCookie("tcp-auth");
						this.token = JSON.stringify({"WCToken": data.WCToken, "WCTrustedToken": data.WCTrustedToken});
						$.cookie("tcp-auth", this.token);
						window.location.href = "wishlist.html";
					},
					error: function () {
						App.Error.render(App.UI.elements.defaultErrorContainer, "errRetrievingInformation");
					}
				});	
			} catch (e) {
				App.Error.log(e);
			}
		},
		
		/* FPO: int1 */
		setFPOCredentials: function () {
			this.username = "NMAN@CHILDRENSPLACE.COM";
			this.password = "Brasil123";
			this.fetchToken();
		}
	}
});

;
// Source: src/js/comp/bitly.js
$.extend(App, {
	BitlyAPI: {
		cache: {},

		shorten: function(longUrl, callback, errorCallback) {
			return callback({
				data: {
					url: longUrl
				}
			});

			/*
			var  	baseURL = "//api.bitly.com/v3/shorten",
					domain = "j.mp",
					format = "json",
					apiKey = "R_05224d3ad805408995e3cba84d12105d";
					login = "jo1119",
					self = this;

			if (this.cache[longUrl] && this.cache[longUrl].loaded) {
				callback(this.cache[longUrl].short);
				return;
			}

			if (!this.cache[longUrl]) {
				this.cache[longUrl] = {
					loaded: false,
					callbacks: [callback]
				};
			} else {
				this.cache[longUrl].callbacks.push(callback);
				return;
			}

			App.ajax({
				dataType: "json",
				url: baseURL,
				data: {
					"format": format,
					"longUrl": longUrl,
					"domain": domain,
					"apiKey": apiKey,
					"login": login
				},

				success: function(data) {
					self.cache[longUrl].short = data;
					self.cache[longUrl].loaded = true;

					var callbacks = self.cache[longUrl].callbacks, i, c;

					for (i=0, c = callbacks.length; i < c; i++) {
						callbacks[i](data);
					}
				},
				error: errorCallback || function(e) {
					console.log(e);
				}
			});
			*/
		}
	}
});
;
// Source: src/js/comp/login.js
$.extend(App, {
    Login: {

        init: function($modal, successCallback, errorCallback) {
            $modal.wrap("<div class='my-wishlist-login' style='display:block; margin: 0;'></div>");

            var self = this;
            self.successCallback = successCallback || function() {
                document.location.reload();
            };
            self.errorCallback = errorCallback || function(data) {
                var msg = data ? data.errorMessage : null;
                $(".modal .transaction-messaging").addClass("error active").html(unescape(msg) || App.Messages[App.Config.defaultLocale].errRetrievingInformation);
                $(".modal .transaction-messaging").html($(".modal .transaction-messaging").text());
            };

            $modal.off("submit.submitLogin").on("submit.submitLogin", ".login form", function(e) {
                e.preventDefault();
                self.login($modal.find(".login input[name=email]").val(), $modal.find(".login input[name=password]").val());
            }).off("click.close-modal").on("click.close-modal", "a.close", function(e) {
                e.preventDefault();
                $modal.removeClass("active");
            }).off("click.navigate-forgot-password").on("click.navigate-forgot-password", "a.forgot-password", function(e) {
                e.preventDefault();
                window.location.href = "/shop/ResetPasswordGuestErrorView?state=forgetpassword&catalogId=" + window.catalogId + "&langId=-1&storeId=" + window.storeId;
            }).off("click.navigate-create-account").on("click.navigate-create-account", "a.create-account", function(e) {
                e.preventDefault();
                window.location.href = "/shop/LogonForm?new=Y&catalogId=" + window.catalogId + "&myAcctMain=&langId=-1&storeId=" + window.storeId + "#tabs-2";
            });

            $(document).off("click.app-login").on("click.app-login", ".app-login", function(e) {
                e.preventDefault();
                $modal.addClass("active");
            });

            $modal.find(".login input[name=email]").keyup(function() {
                $(this.form).find(".transaction-messaging").removeClass("error active").empty();
            });

            $modal.find(".login input[name=password]").keyup(function() {
                $(this.form).find(".transaction-messaging").removeClass("error active").empty();
            });

            $modal.find(".close").click(function() {
                $(".modal .transaction-messaging").removeClass("error").removeClass("active").html('');
            });
        },

        processLogin: function(data, options) {
            try {
                if (data.errorMessage !== undefined) {
                    options.errorCallback(data);
                    return false;
                }
                if (data.responseCode === "LoginSuccess") {
                    $("#qty").html(App.User.getMinicartCount());
                    if (window.userLoggedIn) { window.userLoggedIn(); }
                    options.callback && options.callback(data);
                    try{Rapp.OmnitureAnalytics.TrackLinkEvents.event14({email:options.username});}
                    catch(ex){console.log(ex);}
                } else {
                    options.errorCallback && options.errorCallback(data);
                }
            } catch (e) {
                console.log(e);
            }
        },

        login: function(u, p, callback, errCallback) {
            try {
                var options = {
                    "username": u,
                    "password": p,
                    "callback": callback || this.successCallback,
                    "errorCallback": errCallback || this.errorCallback
                }
                this._login(options);
            } catch (e) {
                App.Error.log(e);
            }
        },

        _login: function(options) {
            try {

                var selector = ".product-details";

                if ($(".my-wishlist-login").length > 0) {
                    selector = ".my-wishlist-login";
                } else if ($(".product-quick-view").length > 0) {
                    selector = ".product-quick-view";
                } else if ($(".my-wishlist:visible").length > 0) {
                    selector = ".my-wishlist:visible";
                }

                $(selector + " .modal .close, .modal:visible .login .close, .modal:visible .login ~ .close").click(function() {
                    $(selector + " .modal .transaction-messaging, .modal:visible .login .transaction-messaging").removeClass("error active").html('');
                });

                $(selector + " .login input[name=email], .modal:visible .login input[name=email]").keyup(function() {
                    $(this.form).find(".transaction-messaging").removeClass("error active").empty();
                });

                $(selector + " .login input[name=password], .modal:visible .login input[name=password]").keyup(function() {
                    $(this.form).find(".transaction-messaging").removeClass("error active").empty();
                });

                var $activeModal = $(".modal:visible");

                if (options.username == "" && options.password == "") {
                    $activeModal.find(".transaction-messaging").html("Please enter your username and password").addClass("error active");
                    return false;
                } else {
                    if (options.username == "") {
                        $activeModal.find(".transaction-messaging").html("Please enter your username").addClass("error active");
                        return false;
                    }

                    if (options.password == "") {
                        $activeModal.find(".transaction-messaging").html("Please enter your password").addClass("error active");
                        return false;
                    }
                }

                if ($activeModal) {
                    $activeModal.find(".transaction-messaging").empty().removeClass("error active");
                }

                var self = this,
                    postData = {
                        logonId1: options.username,
                        logonPassword1: options.password
                    },
                    getData = {
                        URL: "TCPAjaxLogonSuccessView",
                        calculationUsageId: -1,
                        createIfEmpty: 1,
                        deleteIfEmpty: "*",
                        emc: "",
                        emcUserId: "",
                        fromOrderId: "*",
                        toOrderId: ".",
                        updatePrices: "0",
                        reLogonURL: "TCPAjaxLogonErrorView"
                    },
                    endpoint = App.Transport.getCommandURL("logon", true);

                if (endpoint.indexOf("?") == -1) {
                    endpoint += "?";
                }

                for (var v in getData) {
                    if (getData.hasOwnProperty(v)) {
                        endpoint += "&" + v + "=" + encodeURIComponent(getData[v]);
                    }
                }

                App.ajax({
                    url: endpoint,
                    data: postData,
                    cache: false,
                    dataType: App.Config.defaultDataType,
                    contentType: App.Config.defaultContentType,
                    jsonp: App.Config.defaultJSONCallback,
                    crossDomain: true,
                    suffixAsQS: true,
                    success: function(d) {
                        self.processLogin(d, options);
                    },
                    error: options.errorCallback
                });
            } catch (e) {
                App.Error.log(e);
            }
        }

    }
});
;
// Source: src/js/comp/product.js
$.extend(App, {
    Product: {
        getProducts: function(wishlist, sortBy, callback) {
            try {
                App.ajax({
                    url: App.Transport.getCommandURL("products"),
                    data: {
                        wishListId: wishlist,
                        sortBy: sortBy
                    },
                    method: "GET",
                    dataType: App.Config.defaultDataType,
                    contentType: App.Config.defaultContentType,
                    jsonp: App.Config.defaultJSONCallback,
                    success: function(data) {
                        if (typeof callback !== "undefined" && typeof callback === "function") {
                            callback(data);
                        } else {
                            return data;
                        }
                    },
                    error: function(e) {
                        // they like to send an empty response however doesn't really mean there's an error...
                        if (e.status !== 200) {
                            App.Error.render(App.UI.elements.defaultErrorContainer, "errRetrievingInformation");
                        }
                    }
                });
            } catch (e) {
                App.Error.log(e);
            }
        }
    }
});;
// Source: src/js/comp/product.simplified.js
// nadie lo usa
/*
$.extend(App.Product, {
	Simplified: {
		container: null,
		
		load: function (data, context) {
			try {
				this.container = context;
				if (data === undefined) {
					
					this.container.html('<article><p class="no-product">' + App.Messages[App.Config.defaultLocale].noProductsInWishlist + '</p></article>');

				} else {
					$.each(data.item, function (index, value) {						
						var productId = value.productId
						productId += (App.Config.isStatic) ? ".json" : null;
						App.ajax({
							url: App.Transport.getCommandURL("products") + productId,
							method: "GET",
							dataType: App.Config.defaultDataType,
							contentType: App.Config.defaultContentType,
							jsonp: App.Config.defaultJSONCallback,
							success: function (product) {
								App.Product.Simplified.updateDetails(product, index);
							},
							error: function (e) {
								App.Error.render(App.UI.elements.defaultErrorContainer, "errRetrievingInformation");
							}
						});
					}); 
				}
			} catch (e) {
				App.Error.log(e);
			}
		},
		
		updateDetails: function (product, index) {
			var p = product.CatalogEntryView[0];
			var prefix = "." + p.partNumber;
			var temp_hack = /\w+_\w+/g.exec(p.thumbnail);
			this.container.find("article:eq(" + index + ")").addClass(p.partNumber);
			this.container.find(prefix + " header h2").html(p.name);
			this.container.find(prefix + " figure img").attr("src", App.Config.imagesBasePath + temp_hack + ".jpg");
			this.container.find(prefix + " figure img").attr("alt", p.fullImageAltDescription);
			this.container.find(prefix + " figure img").off("click.navigate-pdp").on("click.navigate-pdp", function (e) {
				e.preventDefault();
				console.log("navigate to pdp");
			});
			this.container.find(prefix + " figure figcaption").html(p.fullImageAltDescription);
			var desc = "<span class=\"size\"><strong>Size:</strong>" + p.Attributes[0].Values[0].values + "</span>";
			desc += "<span class=\"color\"><strong>Color:</strong>" + null + "</span>";
			desc += "<span class=\"price\">" + p.Price[1].priceValue + "</span>"
			desc += "<span class=\"qty\">0 of 1 purchased</span>"
			this.container.find(prefix + " p.product-details").html(desc);			
		}
	}
});
*/;
// Source: src/js/comp/shoppingBag.js
$.extend(App, {
	ShoppingBag: {
		get elements() {
			if (this._elements === undefined) {
				this._elements = $.cookie("tcp_wishlist2bag") ? $.cookie("tcp_wishlist2bag").split(",") : [];
			}
			return this._elements;
		},
		
		add: function (element) {
			this.elements.push(element);
			return this._save();
		},

		remove: function (element) {
			var i, ii, filteredElements = [];

			for (i=0, ii=this.elements.length; i<ii; i++) {
				if (this.elements[i] != element) {
					filteredElements.push(this.elements[i]);
				}
			}

			this.elements = filteredElements;
			return this._save();
		},

		_save: function () {
			try {
				$.cookie("tcp_wishlist2bag", this.elements.join(","), {
					path: "/",
					domain: ".childrensplace.com"
				});
			} catch (e) {
				App.Error.log(e);
			}

			return this;
		},

		init: function () {
			$("#order_details .wishlist-item").removeClass("wishlist-item");
			
			$.each(this.elements, function(index, item) {
				$("tr[data-item-id='" + item + "'] .item-descript").addClass("wishlist-item");
			});

			$("body").prepend(App.Templates["wishlist-add-to"]());
			App.AddWishlist.init();
		}
	}
});;
// Source: src/js/comp/site-map.js
$.extend(App, {
    SiteMap: {
        init: function() {
            var $siteMap = $(".tree");
            $siteMap.off("click.enable-site-map").on("click.enable-site-map", ".openClose", function(e) {
                $(this).parent().toggleClass("open");
            });

            $.each($(".level4"), function (index, branch){
            	var subBranch = $(branch).find("li");
            	if (subBranch.length === 0)	 {
            		$(branch).siblings("a").addClass("no-leaf");
            		$(branch).siblings(".openClose").remove();
            		$(branch).remove();
            	} 
            	if (subBranch.length >= 5 && subBranch.length != 6) {
            		$(branch).append("<li class='see-more'><a>See more</a></li>").addClass("collapsed");
            	}
            });
			$(document).off("click.see-more").on("click.see-more", ".level4 .see-more a", function(e){
				e.preventDefault();
				var $parent = $(this).parents(".level4").toggleClass("expanded collapsed");
				if ($parent.hasClass("expanded")) {
					$(this).html("See Less");
				} else { 
					$(this).html("See More"); 
				}
			});

            $siteMap.append("<div class=\"kids\"><span>kids</span></div>");         
            $siteMap.append("<div class=\"toddler\"><span>toddler</span></div>");
            $siteMap.append("<div class=\"others\"></div>");

            $.each($(".level2 .supercategory_h2"), function (index, value) {
                var title = $(value).children("a").last().text().toLowerCase();
                if (title == "girls clothing" || title == "girl's clothing" || title == "girl's clothes" || title == "girls clothes" || title == "boys clothes" || title == "kids shoes" || title == "kids accessories") {
                   $(".kids").append($(value).parent());
                } else if (title.indexOf("toddler") != -1) {
                    $(".toddler").append($(value).parent());
                } else {
                    $(".others").append($(value).parent());    
                }
            });

            $.each($(".others .level2 .supercategory_h2"), function (index, value) {
                if ($(value).children("a").last().text().toLowerCase().indexOf("baby") >= 0 && $(value).parent().parent().find(".baby").length == 0) {
                    $(value).parent().before("<span class=\"baby\">Baby</span>");
                }

                if ($(value).children("a").last().text().toLowerCase().indexOf("place shops") >= 0 && $(value).parent().parent().find(".place-shops").length == 0) {
                    $(value).parent().before("<span class=\"place-shops\">Place Shops</span>");
                }

                if ($(value).children("a").last().text().toLowerCase().indexOf("clearance") >= 0 && $(value).parent().parent().find(".clearance").length == 0) {
                    $(value).parent().before("<span class=\"clearance\">Clearance</span>");
                }


                if ($(value).children("a").last().text().toLowerCase().indexOf("more") >= 0 && $(value).parent().parent().find(".more").length == 0) {
                    $(value).children("a").last().hide();
                    $(value).parent().before("<span class=\"more\">More</span>").addClass("minified");
                }

            });
            
        }
    }
});;
// Source: src/js/comp/tracking.js
$.extend(App, {
	Tracking: {
	
		/*
		Event52 wishlist_add
		Event53 wishlist_remove 
		Event54 wishlist_delete 
		Event55 wishlist_open 
		Event56 wishlist_view_owner 
		Event57 wishlist_view_guest 
		eVar55 add_to_wishlist_location 
		eVar2 add_to_cart_location 
		eVar56 wishlist_name 
		scAdd cart_add 
		*/


		mapLocationToURI: function(uri) {
			var uri = uri || document.location.href, 
				rv;
			if (uri.indexOf("/search/") > 0) {
				rv = "PLP";
			} else if (uri.indexOf("/p/") > 0 || uri.indexOf("ProductDisplay") > 0) {
				rv = "PDP";
			} else if (uri.indexOf("SearchDisplay") > 0) {
				rv = "SearchResult";
			} else if (uri.indexOf("AjaxOrderItemDisplayView") > 0){
				rv = "CartPage";
			}
			return rv;
		},

		getFriendlyLocation: function() {
			var location = App.Tracking.mapLocationToURI(), 
				rv = "undefiend";
			switch (location) {
				case "PLP":
					rv = "quick view product listing page";
					break;				
				case "PDP":
					rv = "product detail page";					
					break;				
				case "SearchResult":
					rv = "quick view product search result page";
					break;
				case "CartPage":
					rv = "cart view page";
					break;
			}

			return rv;
		},

		getCategoryFromLegacyTree: function () {
			var category = App.Tracking.mapLocationToURI();
			if (category === "PDP") {
				return $(".site-breadcrumbs ul li:last a").text();
			} else if (category === "PLP") {
				return $(".site-breadcrumbs ul li:last").text();
			} else if (category === "SearchResult") {
				return $(".site-breadcrumbs ul li:last").text();
			}
		},

		utag: function (data) {
			try {
				return utag.link(data);	
			} catch (e) {
				console.debug(e);
			}
		}

	}
});;
// Source: src/js/comp/user.js
$.extend(App, {
	User: {

		get firstname() {
			return $.cookie("tcp_firstname") || null;
		},

		get isRegistered() {
			return $.cookie("tcp_isregistered") || null;
		},

		getPoints: function() {
			return $.cookie("pntsAvail") || 0;
		},

		get isGenericUserEnabled () {
			return !!$.cookie("WC_USERACTIVITY_-1002");
		},

		getMinicartCount: function() {
			return ($.cookie("minicartcookie") || "0").split("|")[0];
		},

		isRegisteredAndLoggedin: function() {
			// return (this.isRegistered != undefined && this.firstname != undefined && !this.isGenericUserEnabled);
			return this.firstname != undefined;
		},

		login: function(u, p) {

		},
	
		enableNavigationForRegisteredUsers: function() {
			// left hand side
			$(".toolbar-links #WC_TCPCachedCommonToolbar_FormLink_LogonModal1").remove();
			$(".toolbar-links .mpr").remove();
			$(".toolbar-links .create-account-tcp").remove();
			$("#toolbar-right .rewards-popover").remove();

			var link = '<li><a href="/shop/AjaxLogonForm?catalogId=' + window.catalogId + '&myAcctMain=1&langId=-1&storeId=' + window.storeId + '">My Account</a></li>';
			$("#customer-name").html('<span class="greeting"><a href="/shop/AjaxLogonForm?catalogId=10551&amp;myAcctMain=1&amp;langId=-1&amp;storeId=10151" />Hi  <span id="fname">' + this.firstname + '</span></span> (<a onclick = "javascript:TCP.Tealium.Link.userLogout();javascript:resetLogoutCookie();" href="/shop/Logoff?catalogId=' + window.catalogId + '&rememberMe=false&myAcctMain=1&langId=-1&storeId=' + window.storeId + '&URL=LogonForm">Log Out</a>)').after(link);
			
			// righ hand side 
			var cout = '<ul id="toolbar-points">';
				cout += '<li><a href="/shop/AjaxLogonForm?catalogId=10551&amp;myAcctMain=1&amp;langId=-1&amp;storeId=10151"><img src="/wcsstore/GlobalSAS/images/tcp/header/myplace_Rewards_logo.png" alt="myPLACE Rewards" title="myPLACE Rewards"></a></li>';
				cout += '<li class="points-total"><a href="/shop/AjaxLogonForm?catalogId=' + window.catalogId + '&amp;myAcctMain=1&amp;langId=-1&amp;storeId=' + window.storeId + '">Points: ' + this.getPoints() + '</a></li>';
				cout += '</ul>';
				// cout += '<a class="get-wishlist" href="/webapp/wcs/stores/servlet/TCPViewWL?catalogId=10551&langId=-1&productId=16003&storeId=10151">' + App.Messages[App.Config.defaultLocale].wishlistMainCTA + '</a>';

			$("#toolbar-right").prepend(cout);
		}
	}
});


;
// Source: src/js/comp/wishlist.js
$.extend(App, {
	Wishlist: {
		_requesttye: "ajax",
		_activeWishlist: null,
		_lastSelected: null,
		_moveToTemplate: null,
		_onlyPublic: true,
		_activeProductList: null,
		_wishlistCount: 0,
		_editedWishlist: [],
		_activeWishlistGuestKey: null,
		_activeSuccessMessageSign: null,

		UI: {
			elements: {
				createWishlistCTA: ".create-new-wishlist",
				editWishlistUpdateCTA: ".edit-wishlist-update",
				navContainer: ".users-wishlits"
			}
		},

		get count () { return this._wishlistCount; },
		set count (c) { this._wishlistCount = c; },

		get activeProductList () { return this._activeProductList; },
		set activeProductList (p) { this._activeProductList = p; },

		initCache: function() {
			this.useCache = true;
		},

		clearCache: function() {
			this.useCache = false;
			this.cache = null;
		},

		init: function(langId, catalogId, storeId) {
			try {
				App.Transport.loader = $("<div class=\"loader\">we're loading your wishlist. one moment please.</div>");
				App.Transport.enableLoader($(".wishlist"));

				if (!App.User.isRegisteredAndLoggedin() && App.QueryString.get("guestWishListId") === undefined) {
					// one shouldnt ever reach this stage ... just in case
					document.location = $(".greeting a").attr("href");
					return;
				}

				if (App.QueryString.get("guestWishListId")) {
					$(".my-wishlist").addClass("my-wishlist-guest");
				}

				this.setupCreateWishlist();
				this.renderWishlist();

				$(document).off("click.edit-wishlist-cta").on("click.edit-wishlist-cta", App.Wishlist.UI.elements.editWishlistUpdateCTA, function (e) {
					e.preventDefault();
					App.Wishlist.updateWishlist();
				}).off("click.create-new-wishlist").on("click.create-new-wishlist", App.Wishlist.UI.elements.createWishlistCTA, function (e) {
					e.preventDefault();
					$(App.Wishlist.UI.elements.createWishlistCTA).hide();
					$(".users-wishlits .add-new-wishlist").show();
				});

				this.initSorting();
				this.initShareOptions();

				$(document).off("click.navigate-help").on("click.navigate-help", ".take-wishlist-ui-tour", function(e) {
					e.preventDefault();
					var target = "/shop/us/content/help-center-page/#page=us/online_ordering/wishlist.html";
					if (window.storeId == "10152") {
						target = "/shop/ca/content/help-center-page/?page=canada/online_ordering/wishlist.html";
					}
					window.location.href = target;
				});

				App.Transport.disableLoader($(".wishlist"));
			} catch (e) {
				App.Error.log(e);
			}
		},

		forceReload: function() {
			Delete_Cookie('tcp_firstname', '/', ".childrensplace.com");
			Delete_Cookie('tcp_isregistered', '/', ".childrensplace.com");
			window.location.reload();
		},

		initSorting: function() {
			var self = this;

			$(".share-options select").off("change.sort").on("change.sort", function() {
				self.getProducts(self.getActiveWishlist());
			});
		},

		hideAllshareOptions: function () {
			$(".share-options .active").removeClass("active");
		},

		initShareOptions: function() {

			if (window.isFacebookEnabled !== false) {
				$(".facebook").addClass("active");
			}


			$(document).off("click.toggle-share-email").on("click.toggle-share-email", ".share-options .email", function(e) {
				e.preventDefault();
				$(this).toggleClass("active").siblings(":not(.email-envelop)").removeClass("active");
				$(this).siblings(".email-envelop").toggleClass("active");

				$(document).off("click.close-share-email").on("click.close-share-email", ".my-wishlist", function(e) {
					console.log($(e.target).parent());
					if ($(e.target).closest(".email-envelop").length == 0 && $(e.target).parent().parent().length > 0) {
						$(".share-options .email").removeClass("active");
						$(".email-envelop").removeClass("active");
						$(document).off("click.close-share-email");
					}
				});

			}).off("click.toggle-share-link").on("click.toggle-share-link", ".share-options .link", function(e) {
				e.preventDefault();
				$(this).toggleClass("active").siblings(":not(.link-envelop)").removeClass("active");
				$(this).siblings(".link-envelop").toggleClass("active");
				App.Tracking.utag({
				    "event_name": "wishlist_link_share",
				    "wishlist_name": $(".wishlist header h1").text().toString(),
				});

				$(document).off("click.close-share-link").on("click.close-share-link", ".my-wishlist", function(e) {
					console.log($(e.target).parent());
					if ($(e.target).closest(".link-envelop").length == 0 && $(e.target).parent().parent().length > 0) {
						$(".share-options .link").removeClass("active");
						$(".link-envelop").removeClass("active");
						$(document).off("click.close-share-link");
					}
				});

			}).off("click.add-recipient").on("click.add-recipient", ".email-envelop form a.plus", function(e) {
				e.preventDefault();
				var $prev = $(this).prev(), $input;
				if ($prev.index()+1  == 4) {
					$(this).parent().addClass("more-blocked");
				}
				if ($prev.index()+1  >= 5) {
					return;
				}
				$input = $prev.clone(true);
				$input.find("input").val("");
				$(this).before($input);
			}).off("click.remove-recipient").on("click.remove-recipient", ".email-envelop form a.minus", function(e) {
				e.preventDefault();
				$(this).parent().parent().removeClass("more-blocked");
				$(this).parent().remove();
			});



			// $(".share-options .email-envelop").mouseleave(App.Wishlist.hideAllshareOptions);
			// $(".share-options .link-envelop").mouseleave(App.Wishlist.hideAllshareOptions);

			this.initEmailShare();
		},

		emailFormReset: function () {
			var t = $(".email-envelop form");
			t.find("input[type=text]").val("");
			t.find("textarea").val("");
			t.find(".to label").not(":eq(0)").remove();

		},

		getShareLink: function(callback, flag) {

			var url = document.location.href.split("?")[0],
				now = new Date(),
				month = (now.getMonth() + 101).toString().substr(1),
				day = (now.getDate() + 100).toString().substr(1),
				params = {
					guestWishListId: App.Wishlist.getActiveWishlistGuestKey(), //,
					guestWishListName: encodeURI($(".wishlist header h1").text()),
					cid: "wishlist-_-" + flag + month + day + now.getFullYear()
				};
			url += "?" + $.param(params);

			App.BitlyAPI.shorten(url, function(data) {
				callback(data.data.url);
			}, function(error) {
				callback(url);
			});
		},

		initLinkShare: function() {
			this.getShareLink(function(url) {
				$(".link-envelop input").val(url);
			}, "copy_link-_-");
		},

		initFacebookShare: function() {
			this.getShareLink(function(url) {
				$(".share-options a.facebook").attr({
					href: "http://www.facebook.com/sharer.php?u=" + encodeURIComponent(url),
					target: "_new"
				});
				$(document).off("click.opne-fb-share").on("click.opne-fb-share", ".share-options a.facebook", function(e) {
					App.Tracking.utag({
					    "event_name": "wishlist_fb_share",
					    "wishlist_name": $(".wishlist header h1").text().toString(),
					});
				});
			}, "fb_link-_-");
		},

		initEmailShare: function() {
			// validation??
			var self = this;

			$(".email-envelop form").validate({
			    ignore: ".ignore, [type=hidden]",
			    onkeyup: false,
			    onclick: false,
			    onfocusout: false,

			    rules: {
			        "email-from": {
			            required: true,
			            email: true
			        },

			        "email-to": {
			            required: true,
			            email: true
			        },

			        "email-message": {
			            required: true
			        }
			    },

			    messages: {
			        "email-from": "The \"From\" email address you provided is invalid.",
			        "email-to": "The \"To\" email address you provided is invalid.",
			        "email-message": "A message is required."
			    },

			    showErrors: function(errorMap, errorList) {
					try {
						var i, ii, $input, error = "Please correct the following errors:<br />", $msg;
						for (i = 0, ii = errorList.length; i < ii; i++) {
							$msg = $(errorList[i].element).parents("form").parent().find(".transaction-messaging").removeClass("success error active");
							error += "&bull; " + errorList[i].message + "<br />";
						}
						if (ii > 0) {
							$msg.addClass("error active").html(error);
						}
					} catch(e) {
						console.log(e);
					}
			    },

			    submitHandler: function(form) {
				    var
						$form = $(form),
						$parent = $form.parent(),
						$msg = $parent.find(".transaction-messaging").empty().removeClass("success error active");

					App.ajax({
						url: App.Transport.getCommandURL("mailTo"),
						method: "GET",
						data: {
							giftListId: self.getActiveWishlist(),
							senderName: App.User.firstname,
							senderEmail: $form.find("[name='email-from']").val(),
							recipientEmail: $form.find("[name='email-to']").map(function() { return $(this).val(); }).get().join(),
							emailSubject: $(".wishlist h1").eq(0).text().trim(),
							emailSubjectAppend: "Has Shared Their Wishlist With You",
							emailTemplate: "SOA_EMAIL_TEMPLATE",
							wishlistMessage: $form.find("textarea").val()
						},
						dataType: App.Config.defaultDataType,
						contentType: App.Config.defaultContentType,
						jsonp: App.Config.defaultJSONCallback,

						success: function (data) {
							App.Tracking.utag({
							    "event_name": "wishlist_email_share",
							    "wishlist_name": $(".wishlist header h1").text().toString(),
							});

							form.reset();
							$msg.addClass("success active").html(App.Messages[App.Config.defaultLocale].emailSubmitSuccess);
							App.Wishlist.emailFormReset();
							setTimeout(function() {
								$msg.empty().removeClass("success error active");
							}, 10000);
						},

						error: function(e) {
							form.reset();
							$msg.addClass("error active").html(App.Messages[App.Config.defaultLocale].emailSubmitError);
							App.Error.render(App.UI.elements.defaultErrorContainer, "errRetrievingInformation");
						}
					});
			    }
			});
		},


		setupCreateWishlist: function(options) {
			if (options && options.reloadCallback) {
				var cback = options.reloadCallback;

				options.reloadCallback = function(rv) {
					App.Wishlist.renderWishlist(null, cback, rv);
				};
			}

			options = $.extend({
				target: ".take-wishlist-ui-tour",
				appendCallback: "before",
				reloadCallback: App.Wishlist.renderWishlist
			}, options);

			$(options.target)[options.appendCallback](App.Templates["wishlist-create-new"]());

			$(".add-new-wishlist .wishlist-name input").attr("placeholder", App.User.firstname + "'s Wishlist");
			App.UI.checkbox();

			$(document).off("click.cancel-wishlist-main-nav").on("click.cancel-wishlist-main-nav", ".add-new-wishlist .cancel-new-wishlist-cta", function (e) {
				e.preventDefault();

				var $form = $(this).parents("form"),
					$result = $form.find(".result");

				$form.find(".wishlist-name input").val("");
				$result.empty();
				$form.hide().prev().show();
			});

			$(document).off("click.create-wishlist-main-nav").on("click.create-wishlist-main-nav", ".add-new-wishlist .create-new-wishlist-cta", function (e) {
				e.preventDefault();

				var $form = $(this).parents("form"),
					$result = $form.find(".result"),
					$input = $form.find(".wishlist-name input"),
					wishlistName = $input.val() || App.User.firstname + "'s Wishlist";

				$result.empty();

				App.Wishlist.createWishlist(wishlistName, $result, function(rv) {

					options.reloadCallback(rv);

					// ISSUE DR109
					$(".users-wishlits .add-new-wishlist").hide();
					$(".users-wishlits .create-new-wishlist").show();
					$(".users-wishlits .add-new-wishlist .result").text("");
					$(".wishlist-name input[name='wishlist-name']").val("");
				});

			});
		},

		deleteItemFromwishlist: function (itemId, callback, errorCallback) {
			var errCback = errorCallback || function(e) {
				App.Error.render(App.UI.elements.defaultErrorContainer, "errRetrievingInformation");
			}, activeWishlist = App.Wishlist.getActiveWishlist();

			App.ajax({
				url: App.Transport.getCommandURL("deleteItemFromwishlist"),
				data: {
					giftListId: activeWishlist,
					giftListItemId: itemId,
					quantity: 0
				},
				method: "GET",
				dataType: App.Config.defaultDataType,
				contentType: App.Config.defaultContentType,
				jsonp: App.Config.defaultJSONCallback,
				success: function(r) {
					if (r.errorCode == "CMN1039E" || r.errorMessageKey == "USR.CWXFR0101I") {
						App.Wishlist.forceReload();
						return;
					} else {
						App.Wishlist.renderWishlist(activeWishlist);
						callback.apply(this, arguments);
					}
				},
				error: errCback
			});
		},

		updateWishlist: function (stack, callback, errorCallback) {
			var self = this;

			$(stack).each(function (index, value) {
				var endpoint;

				if (value.type === "name") {
					endpoint = App.Transport.getCommandURL("updateWishlistName");
				} else if (value.type === "default") {
					endpoint = App.Transport.getCommandURL("changeStatus");
				}

				try {
					App.ajax({
						url: endpoint,
						data: value,
						method: "GET",
						dataType: App.Config.defaultDataType,
						contentType: App.Config.defaultContentType,
						jsonp: App.Config.defaultJSONCallback,
						success: function (data) {
							App.Wishlist.renderWishlist();
							callback && callback(data);
						},
						error: errorCallback || function(e) {
							App.Error.render(App.UI.elements.defaultErrorContainer, "errRetrievingInformation");
						}
					});
				}  catch (e) {
					App.Error.log(e);
				}
			});
		},

		addToBagSuccessHanlder: function ($ref) {
			try {
				var countryCode = window.storeId === "10152" ? "ca" : "us",
          			msg = "You've successfully added an item to your bag!<a href=\"/" + countryCode + "/bag\">View bag</a>",
					$modal = $ref.parent().parent();

				App.Util.confirm({
					container: $modal,
					message: msg,
					isModal: true,
					isOptionEnabled: true
				});

				App.Wishlist._activeSuccessMessageSign = null;
				$modal.find(".modal").addClass("confirm success-add-to-bag");

				App.Wishlist._activeSuccessMessageSign = setTimeout(function () {
					$modal.find(".modal").remove();
					App.Wishlist._activeSuccessMessageSign = null;
				}, 4000);

				window.updateMinibagCount && window.updateMinibagCount();
			} catch (e) {
				App.Error.log(e);
			}
		},

		addToBagErrorHanlder: function () {
			var t = $("<p class=\"error-add-to-bag\">We're sorry, you have reached the 15 quantity limit for this item. Please add another item.</p>");

			App.Wishlist._activeSuccessMessageSign = null;
			$(".error-add-to-bag").remove();

			if (App.Wishlist._activeSuccessMessageSign === null) {
				$(".my-wishlist .error").before(t);
				App.Wishlist._activeSuccessMessageSign = setTimeout(function () {
					$(".error-add-to-bag").remove();
					App.Wishlist._activeSuccessMessageSign = null;
				}, 10000);
			}
			$("#Wishlist_View_Empty_Landing").scrollTop();
		},

		addToBag: function (wishListItem, catEntryId, callback, errorCallback, ref) {

			var errCback = errorCallback || function(e) {
					App.Error.render(App.UI.elements.defaultErrorContainer, "errRetrievingInformation");
				}, addedToBag = function() {
					var args = arguments;
					App.ajax({
						url: App.Transport.getCommandURL("addedToBag"),
						type: "HEAD",
        				async: true,
						success: function() {
							$("#qty").html(App.User.getMinicartCount());
							callback.apply(this, args);
						},
						error: errCback
					});
				};

			App.ajax({
				url: App.Transport.getCommandURL("addToBag"),
				data: {
					orderId: ".",
					calculationUsage: "-1,-2,-5,-6,-7",
					catEntryId: catEntryId,
					quantity: 1,
					requesttype: "ajax",
					externalId: wishListItem,
				},
				method: "GET",
				dataType: App.Config.defaultDataType,
				contentType: App.Config.defaultContentType,
				jsonp: App.Config.defaultJSONCallback,
				success: function(r) {
					if (r.errorCode || r.errorMessageKey) {
						if (r.errorMessageKey === "_ERR_MORE_THAN_15_ITEM_IN_CART_ERROR") {
							App.Wishlist.addToBagErrorHanlder();
						} else if (r.errorCode == "CMN1039E") {
							App.Wishlist.forceReload();
							return;
						} else {
							errCback.apply(this, arguments);
						}
					} else {
						App.ShoppingBag.add(r.orderItemId[0]);
						App.Wishlist.addToBagSuccessHanlder(ref);
						addedToBag.apply(this,arguments);
					}
				},
				error: errCback
			});

		},

		deleteWishlist: function(id, callback, errorCallback) {
			App.ajax({
				url: App.Transport.getCommandURL("deleteWishlist"),
				data: {
					giftListId: id
				},
				method: "GET",
				dataType: App.Config.defaultDataType,
				contentType: App.Config.defaultContentType,
				jsonp: App.Config.defaultJSONCallback,
				success: function(d) {
					App.Tracking.utag({
					    "event_name": "wishlist_delete",
					    "wishlist_name": $(".wishlist header h1").text()
					});
					App.Wishlist.renderWishlist();
					callback && callback(d);
				},
				error: errorCallback || function () {
					App.Error.render(App.UI.elements.defaultErrorContainer, "errRetrievingInformation");
				}
			});
		},

		setActiveWishlist: function(id) {
			this._activeWishlist = id;
			this.initLinkShare();
			this.initFacebookShare();
		},

		setActiveWishlistGuestKey: function(val) {
			this._activeWishlistGuestKey = val;
		},

		getActiveWishlistGuestKey: function() {
			return this._activeWishlistGuestKey;
		},

		getActiveWishlist: function() {
			return this._activeWishlist;
		},

		getMoveTo: function() {
			return this._moveToTemplate;
		},

		setMoveTo: function(t) {
			this._moveToTemplate = t;
		},

		createWishlist: function (name, errorContainer, callback) {
			var wishlistCap = App.Config.wishlist.wishlist;
			if (wishlistCap <= App.Wishlist.count) {
				App.Error.inline(errorContainer, "maxWishlistCapReached");
				return;
			} else {
				if (errorContainer) {
					App.Error.reset(errorContainer);
				}

				App.ajax({
					url: App.Transport.getCommandURL("createWishlist"),
					data: {
						name: name
					},
					method: "GET",
					dataType: App.Config.defaultDataType,
					contentType: App.Config.defaultContentType,
					jsonp: App.Config.defaultJSONCallback,
					success: function(data) {
						if (App.Wishlist.cache && data && data.giftListId) {
							App.Wishlist.cache.push({
								status: "Default",
								giftListExternalIdentifier: data.giftListId[0],
								nameIdentifier: data.giftListName[0],
								accessSpecifier: "Public",
								itemCount: 1
							});
						} else if (data && data.giftListId) {
							App.Wishlist.cache = [{
								status: "Default",
								giftListExternalIdentifier: data.giftListId[0],
								nameIdentifier: data.giftListName[0],
								accessSpecifier: "Public",
								itemCount: 1
							}];
						}

						callback(data);
					},
					error: function () {
						App.Error.render(App.UI.elements.defaultErrorContainer, "errRetrievingInformation");
					}
				});
			}
		},

		getWishlist: function (callback) {
			try {
				var self = this, data = {},
				ajaxCallback = function(r) {
					if (self.useCache) {
						self.cache = r;
					}
					callback(r);
				};

				if (self.useCache && self.cache) {
					callback(self.cache);
					return;
				}

				if (App.QueryString.get("guestAccessKey")) {
					data.guestAccessKey = App.QueryString.get("guestAccessKey");
				}

				if (!App.User.isRegisteredAndLoggedin()) {
					ajaxCallback([]);
					return;
				}

				data.sortBy = $(".share-options select").val();
				App.ajax({
					url: App.Transport.getCommandURL("getWishlist"),
					data: data,
					method: "GET",
					dataType: App.Config.defaultDataType,
					contentType: App.Config.defaultContentType,
					jsonp: App.Config.defaultJSONCallback,
					success: ajaxCallback,
					error: function(e) {
						App.Error.render(App.UI.elements.defaultErrorContainer, "errRetrievingInformation");
					}
				});
			}  catch (e) {
				App.Error.log(e);
			}
		},

		addItemToWishlist: function (wishlistId, itemId, qty, callback, errorCallback) {
			try {
				var errCback = errorCallback || function(e) {
						App.Error.render(App.UI.elements.defaultErrorContainer, "errRetrievingInformation");
					},
					productQty = qty || 1;


				App.ajax({
					url: App.Transport.getCommandURL("addItemToWishlist"),
					data: {
						giftListId: wishlistId,
						catEntryId_1: itemId,
						quantity_1: productQty,
					},
					method: "GET",
					dataType: App.Config.defaultDataType,
					contentType: App.Config.defaultContentType,
					jsonp: App.Config.defaultJSONCallback,
					success: function(r) {
						if (r.hasOwnProperty("errorCode")) {
							if (r.errorCode == "CMN1039E" || r.errorMessageKey == "USR.CWXFR0101I") {
								App.Wishlist.forceReload();
								return;
							}
							errCback.apply(this, arguments);
						} else {
							callback.apply(this, arguments);
						}
					},
					error: errCback
				});
			} catch (e) {
				App.Error.log(e);
			}
		},

		createWishlistWithItem: function(wishlistName, itemId, callback, errorCallback) {
			try {
				var errCback = errorCallback || function(e) {
						App.Error.render(App.UI.elements.defaultErrorContainer, "errRetrievingInformation");
					};
				App.ajax({
					url: App.Transport.getCommandURL("createWishlist"),
					data: {
						name: wishlistName,
						catEntryId_1: itemId,
						quantity_1: 1
					},
					method: "GET",
					dataType: App.Config.defaultDataType,
					contentType: App.Config.defaultContentType,
					jsonp: App.Config.defaultJSONCallback,
					success: function(r) {
						if (r.hasOwnProperty("errorCode")) {
							if (r.errorCode == "CMN1039E" || r.errorMessageKey == "USR.CWXFR0101I") {
								App.Wishlist.forceReload();
								return;
							}
							errCback.apply(this, arguments);
						} else {
							if (App.Wishlist.cache && r && r.giftListId) {
								App.Wishlist.cache.push({
									status: "Default",
									giftListExternalIdentifier: r.giftListId[0],
									nameIdentifier: r.giftListName[0],
									accessSpecifier: "Public",
									itemCount: 1
								});
							} else if (r && r.giftListId) {
								App.Wishlist.cache = [{
									status: "Default",
									giftListExternalIdentifier: r.giftListId[0],
									nameIdentifier: r.giftListName[0],
									accessSpecifier: "Public",
									itemCount: 1
								}];
							}

							callback.apply(this, arguments);
						}
					},
					error: errCback
				});
			} catch (e) {
				App.Error.log(e);
			}
		},

		setEditWishlistOptionModalCTA: function(modal) {

			modal.off("click.close-modal").on("click.close-modal", ".close", function(e){
				e.preventDefault();
				modal.remove();
				modal = null;
			}).off("click.commit-wishlist-changes").on("click.commit-wishlist-changes", "input[type=submit]", function(e){
				e.preventDefault();

				var editedWishlistNewName = $(this.form).find("[type=text]").val(),
					c = {
						giftListId: $(this).parents(".modal").data("id"),
						name: (!editedWishlistNewName) ?  App.User.firstname + "'s Wishlist" : editedWishlistNewName,
						type: "name"
					};

				App.Wishlist._editedWishlist.push(c);
				App.Wishlist.updateWishlist(App.Wishlist._editedWishlist, function (data) {
					if (data && data.errorCode == "CMN1039E") {
						App.Wishlist.forceReload();
						return;
					}
					modal.remove();
					modal = null;
				}, null);
				App.Wishlist._editedWishlist = [];
			}).off("click.edited-field-default").on("click.edited-field-default", ".set-default-wishlist", function (e){
				e.preventDefault();

				var c = {
					giftListId: $(this).parents(".modal").data("id"),
					check: true,
					type: "default",
					giftListState: "Default"
				};

				App.Wishlist._editedWishlist.push(c);
				App.Wishlist.updateWishlist(App.Wishlist._editedWishlist, function (data)  {
					if (data && data.errorCode == "CMN1039E") {
						App.Wishlist.forceReload();
						return;
					}
					modal.remove();
					modal = null;
				}, null);
				App.Wishlist._editedWishlist = [];
			}).off("click.delete-wishlist").on("click.delete-wishlist", ".delete-wishlist", function (e){
				e.preventDefault();

				var msg = App.Messages[App.Config.defaultLocale],
					$modal = $(this).parents(".modal"); // NOT the same as modal var

				// are you sure u wanna delete it?
				App.Util.confirm({
					container: $modal,
					message: msg.wishlistDeleteMessage($modal.find("[type=text]").val()),
					yesLabel: msg.wishlistDeleteButton,
					noLabel: msg.wishlistCancelButton,
					isModal: false,
					yesCallback: function() {
						// yes
						App.Wishlist.deleteWishlist($modal.data("id"), function(r) {

							if (r.errorCode == "CMN1039E" || r.errorMessageKey == "USR.CWXFR0101I") {
								App.Wishlist.forceReload();
								return;
							}

							if (r.giftListState[0] == "Deleted") {
								modal.remove();
								modal = null;
							}
						});

						$modal.removeClass("confirm-delete");
					},

					noCallback: function() {
						$modal.removeClass("confirm-delete");
					}
				});

				$modal.addClass("confirm-delete");
			});
		},

		getEmptyMessageSlot: function() {
		    var slotContent = "",
		    	$slotContainer = $(".empty-wishlist-espot");

		    if (!App.QueryString.get("guestWishListId")) {
		        slotContent = $("#Wishlist_Registered_Empty_Landing").eq(0).clone(true).show();
		    } else {
		        slotContent = $("#Wishlist_View_Empty_Landing").eq(0).clone(true).show();
		    }

		    if ($slotContainer.length == 0) {
		    	$slotContainer = $(".product-list").eq(0).addClass("empty-wishlist-espot");
				$(".share-options").addClass("no-sharing");
		    	// hide share
		    }

			$slotContainer.empty().append(slotContent);
		    $(".empty-wishlist-espot .no-product").removeClass("no-product");
		},

		removeEmptyMessageSlot: function () {
			$(".wishlist .empty-wishlist-espot").remove();
			$(".share-options").removeClass("no-sharing");
		},

		isEmptyMessageSlotEnabled: function (data) {
			if (!App.QueryString.get("guestWishListId")) {
				// user logged in seeing own lists
				return data.length == 0;
			} else {
				// user seeing guest list
				if (data.length == 0) {
					return true;
				}

				var bought = 0, i, ii;
				for (i = 0, ii = data.length; i < ii; i++) {
					if (data[i].quantityBought * 1 >= data[i].requestedQuantity * 1) {
						bought++;
					}
				}

				return bought == data.length;
			}
		},

		getProducts: function(wishlistId, productCount) {
			// App.Wishlist.setActiveWishlist(wishlistId);

			var callback = function (data) {
				// App.Wishlist.setActiveWishlist(wishlistId);
				var isEmptyMessageSlotEnabled = App.Wishlist.isEmptyMessageSlotEnabled(data);

				App.Wishlist.removeEmptyMessageSlot();
				$(".product-list").remove();
				data.showNoProductsMessage = !isEmptyMessageSlotEnabled;
				$(".wishlist").append(App.Templates["product-simplified"](data));

				if (isEmptyMessageSlotEnabled) {
					$(".custom-dropdown select").attr('disabled', true);
					App.Wishlist.getEmptyMessageSlot();
				}

				if (App.QueryString.get("guestWishListId")) {
					App.Wishlist.cqGuestUsers();
				} else {
					if (data.length > 0) {
						App.Wishlist.cqRegisterUsers();
					} else {
						App.Wishlist.cqGuestUsers();
					}
				}

				if (App.Wishlist.count > 1) {
					App.MoveToWishlist.init();
					App.Wishlist.clearCache();
				} else {
					$(".wishlist .add-to-wishlist.move-to-wishlist").addClass("disabled").parent().addClass("wishlist-move-disabled");
				}

				App.initRopis();

			};

			if (productCount !== 0) {
				App.Product.getProducts(App.Wishlist.getActiveWishlist(), $(".share-options select").val(), callback);
			} else {
				callback([]);
			}
		},

		cqGuestUsers: function() {
			try {
				$("script[src*='cdn.cquotient.com/js/v2/gretel.min.js']").remove();

				App.Wishlist.cqClearContainer();
				CQuotientJS.fetchPersonalizedRecommendations(
					"",
					"WC_CQ_Container",
					5);

				var gretel = $(document.createElement('script'));
				gretel.attr("src", "//cdn.cquotient.com/js/v2/gretel.min.js").attr("type", "text/javascript");
				$("body").append(gretel);

			} catch (e) {
				console.debug(e);
			}
		},

		cqClearContainer: function () {
			if ($(".product-list #WC_CQ_Container").length == 0) {
				$(".product-list").last().append("<div id=\"WC_CQ_Container\"></div>");
			} else {
				$(".product-list #WC_CQ_Container").empty();
			}
		},

		cqRegisterUsers: function()  {
			try {

				if ($("script[src*='cdn.cquotient.com/js/v2/gretel.min.js']").length === 0) {
					var gretel = $(document.createElement('script'));
					gretel.attr("src", "//cdn.cquotient.com/js/v2/gretel.min.js").attr("type", "text/javascript");
					$("body").append(gretel);
				}

				App.Wishlist.cqClearContainer();

				var styles = "," + $("section .style-number").map(function (index, item) {
						return $(this).text().trim().split(" ").pop();
					}).toArray().join(),

					prices = "," + $(".price").map(function (index, item) {
						return $(this).text().trim().split(" ")[0];
					}).toArray().join();

				CQuotientJS.fetchshoppingBagRecommendations(
					styles,
					prices,
					"WC_CQ_Container",
					4,
					4);
			} catch (e) {
				console.debug(e);
			}
		},

		renderWishlist: function(activeWishlist, callback, newWishlist) {
			try {
				activeWishlist = activeWishlist && activeWishlist.giftListId ? activeWishlist.giftListId[0] : activeWishlist;
				var load = function (data) {
					try {

						console.debug(data);



						if (!data || data.length == 0) {
							if (App.QueryString.get("guestWishListId")) {
								data = [{
									"status": "Default",
									"giftListExternalIdentifier": App.QueryString.get("guestWishListId"),
									"nameIdentifier": decodeURIComponent(App.QueryString.get("guestWishListName")),
									"accessSpecifier": "Public",
									"itemCount": -1
								}];
							} else {
								App.Wishlist.createWishlist(App.AddWishlist.getDefaultName(), null, function(rv) {
									if (rv.errorCode == "CMN1039E" || rv.errorMessageKey == "USR.CWXFR0101I") {
										App.Wishlist.forceReload();
										return;
									}
									activeWishlist = rv.giftListId[0];
									App.Wishlist.renderWishlist(activeWishlist, callback);
								});
								return;
							}
						}

						var $addNew = $(App.Wishlist.UI.elements.navContainer + " .create-new-wishlist"),
							$maybeWishlist = $addNew.prev();

						// App.Wishlist.initCache(); // so we trigger only 1 call

						if ($maybeWishlist.is(".wishlist-list")) {
							$maybeWishlist.prev().remove(); // h2
							$maybeWishlist.remove(); 		// ol
						}

						$addNew.before(App.Templates.wishlist(data));

						$(document).off("click.trigger-modal-edit-wishlist").on("click.trigger-modal-edit-wishlist", ".wishlist-list a.edit", function(e) {
							e.preventDefault();

							var $myWishlist = $(this).parents(".my-wishlist"), t, $modal, p = $(this).parent();

							$myWishlist.find(".modal").remove();
							t = {
								name: p.find(".active-wishlist").text(),
								wishlistId: p.data("id"),
								isUnique: (p.data("list-count") <= 1),
								isDefault: (p.data("status") === "Default")
							};
							$myWishlist.append(App.Templates["wishlist-update"](t));
							$modal = $myWishlist.find(".modal").css({
								top: $(this).offset().top,
								left: $(this).offset().left + $(this).outerWidth(true) + 10
							}).show();

							App.Wishlist.setEditWishlistOptionModalCTA($modal);

						});

						App.Wishlist.setMoveTo(App.Templates["wishlist-move-to"](data));
						App.Wishlist.count = data.length;

						if (App.Wishlist.count >= window.wishlist_limit) {
							$(".users-wishlits").addClass("max-limit");
							$(".users-wishlits").find(".result").addClass("inline-error active").html("Yikes! You've reached the maximum number of Wishlists. Please add to an existing list or delete a list to create a new one.");
						} else {
							$(".users-wishlits").removeClass("max-limit");
							$(".users-wishlits").find(".result").removeClass("inline-error active").empty();
						}

						$(document).off("click.delete-item").on("click.delete-item", ".delete-item", function (e) {
							e.preventDefault();

							try {
								var $article = $(this).parents("article"),
									msg = App.Messages[App.Config.defaultLocale];

								App.Util.confirm({
									container: $article,
									message: msg.wishlistDeleteItemMessage($article.find(".info h2").text().trim()),
									yesLabel: msg.wishlistDeleteButton,
									noLabel: msg.wishlistCancelButton,
									yesCallback: function() {
										try {
											var item = $article.data("item-id"),
												trackingData = {
												    "event_name": "wishlist_remove",
												    "wishlist_name": $(".wishlist header h1").text().toString(),
												    "product_id": [$article.data("catentryid").toString()],
												    "product_name": [$article.find(".info h2").text().toString()],
												    "product_price": [$article.find(".price").text().split(" ")[0]],
												    "product_image_url": [$article.find("figure img").attr("src")]
												};
											App.Wishlist.deleteItemFromwishlist(item, function(r) {
												App.Tracking.utag(trackingData);
											}, function (e) {
												console.log(e);
											});
										} catch (e) {
											console.log(e);
										}
									}
								});
							} catch (e) {
								App.Error.log(e);
							}
						});

						$(document).off("click.add-to-cart").on("click.add-to-cart", ".add-to-cart", function (e) {
							e.preventDefault();
							var $article = $(this).parent().parent(),
								item = $article.data("item-id"),
								catEntryId =  $article.data("catentryid"),
								trackingData = {
									"add_to_cart_location": "wishlist",
								    "product_id": [catEntryId],
								    "product_name": [$article.find(".info h2").text().toString()],
								    "product_quantity": ["1"],
								    "product_price": [$article.find(".price").text().split(" ")[0]],
								    "event_name": "cart_add"
								};

							App.Wishlist.addToBag(item, catEntryId, function (data) {
								App.Tracking.utag(trackingData);
							}, function(err) {
								console.log(data);
							}, $(this));
						});


						$(App.Wishlist.UI.elements.navContainer).off("click.load-simplified").on("click.load-simplified", "li a.active-wishlist", function(e) {
							e.preventDefault();

							//hide share options (all)
							App.Wishlist.hideAllshareOptions();

							// tracking tag
							if (e.originalEvent !== undefined) {
								App.Tracking.utag({
									"event_name": "wishlist_open",
									"wishlist_name": data[$(this).parent().index()].nameIdentifier
								});
							}

							$(".wishlist.component h1").html(data[$(this).parent().index()].nameIdentifier);
							App.Wishlist.setActiveWishlistGuestKey(data[$(this).parent().index()].giftListExternalIdentifier);
							App.Wishlist.setActiveWishlist(data[$(this).parent().index()].giftListExternalIdentifier);

							App.Wishlist.getProducts(data[$(this).parent().index()].giftListExternalIdentifier, data[$(this).parent().index()].itemCount);

							$(App.Wishlist.UI.elements.navContainer).find(".selected").removeClass();
							$(App.Wishlist.UI.elements.navContainer).find("[data-id='" + App.Wishlist.getActiveWishlist() + "']").addClass("selected");

							$(".wishlist.component").addClass("active");
						});

						// selecting default WL or active WL
						if (!activeWishlist) {
							activeWishlist = App.QueryString.get("wishlistId") || App.QueryString.get("guestWishListId");
						}

						if (activeWishlist) {
							$(App.Wishlist.UI.elements.navContainer).find(".selected").removeClass("selected");
							$(App.Wishlist.UI.elements.navContainer).find("li[data-id='" + activeWishlist + "']").addClass("selected");
						}

						$(App.Wishlist.UI.elements.navContainer).find(".selected a.active-wishlist").trigger("click");
						callback && callback(data, newWishlist);

					} catch (e) {
						console.log(e);
					}
				};

				if (App.QueryString.get("guestWishListId")) {
					load();
				} else {
					App.Wishlist.getWishlist(load);
				}

			} catch (e) {
				App.Error.log(e);
			}
		}
	}
});
;
// Source: src/js/comp/wishlist.services.js
$.extend(App.Wishlist, {
	Services: {

		_genericTransactionError: function (e) {
			return App.Error.render(App.UI.elements.defaultErrorContainer, "errRetrievingInformation");
		},

		//	Summary It deletes a given known item from the known wishlist
		_deleteItemFromwishlist: function (wishlistArgs) {
			try {
				App.ajax({
					url: App.Transport.getCommandURL("deleteItemFromwishlist"),
					data: {
						giftListId: wishlistArgs.wishlistId,
						giftListItemId: wishlistArgs.itemId,
						quantity: 0
					},
					dataType: App.Config.defaultDataType,
					contentType: App.Config.defaultContentType,
					jsonp: App.Config.defaultJSONCallback,
					success: function(rv) {
						if (rv.errorCode) {
							wishlistArgs.errorCallback.apply(this, arguments);
						} else {
							wishlistArgs.callback.apply(this, arguments);
						}
					},
					error: wishlistArgs.errorCallback
				}); 
			}  catch (e) {
				App.Error.log(e);	
			}
		},

		//	Summary: It changes the "name" for a known wishlist
		_updateWishlistName: function (wishlistArgs) {
			try {
				App.ajax({
					url: App.Transport.getCommandURL("updateWishlistName"),
					data: {
						giftListId: wishlistArgs.wishlistId,
						name: wishlistArgs.name,
						type: "name"
					},
					dataType: App.Config.defaultDataType,
					contentType: App.Config.defaultContentType,
					jsonp: App.Config.defaultJSONCallback,
					success: function(rv) {
						if (rv.errorCode) {
							wishlistArgs.errorCallback.apply(this, arguments);
						} else {
							wishlistArgs.callback.apply(this, arguments);
						}
					},
					error: wishlistArgs.errorCallback
				});			
			}  catch (e) {
				App.Error.log(e);	
			}			
		},
		
		// Summary: changes the wishlist status to Default. Default's WL are being used to hold items when no wishlist is being selected while added 
		_changeStatus: function(wishlistArgs) {
			try {
				App.ajax({
					url: App.Transport.getCommandURL("changeStatus"),
					data: {
						giftListId: wishlistArgs.wishlistId,
						check: "true",
						giftListState: "Default",
						type: "default"
					},
					dataType: App.Config.defaultDataType,
					contentType: App.Config.defaultContentType,
					jsonp: App.Config.defaultJSONCallback,
					success: function(rv) {
						if (rv.errorCode) {
							wishlistArgs.errorCallback.apply(this, arguments);
						} else {
							wishlistArgs.callback.apply(this, arguments);
						}
					},
					error: wishlistArgs.errorCallback
				});					
			}  catch (e) {
				App.Error.log(e);	
			}				
		},

		// Summary: it deletes a known WL.
		_deleteWishlist: function(wishlistArgs) {
			try {
				App.ajax({
					url: App.Transport.getCommandURL("deleteWishlist"),
					data: {
						giftListId: wishlistArgs.wishlistId
					},
					dataType: App.Config.defaultDataType,
					contentType: App.Config.defaultContentType,
					jsonp: App.Config.defaultJSONCallback,
					success: function(rv) {
						if (rv.errorCode) {
							wishlistArgs.errorCallback.apply(this, arguments);
						} else {
							wishlistArgs.callback.apply(this, arguments);
						}
					},
					error: wishlistArgs.errorCallback
				});
			}  catch (e) {
				App.Error.log(e);	
			}			
		},	

		// Summary: It creates an empty WL given a specific name
		_createWishlist: function (wishlistArgs) {	
			try {
				App.ajax({
					url: App.Transport.getCommandURL("createWishlist"),
					data: {
						name: wishlistArgs.name
					},
					dataType: App.Config.defaultDataType,
					contentType: App.Config.defaultContentType,
					jsonp: App.Config.defaultJSONCallback,
					success: function(rv) {
						if (rv.errorCode) {
							wishlistArgs.errorCallback.apply(this, arguments);
						} else {
							wishlistArgs.callback.apply(this, arguments);
						}
					},
					error: wishlistArgs.errorCallback
				});
			}  catch (e) {
				App.Error.log(e);	
			}					
		},

		// Retrieves a WL given an Id
		_getWishlist: function (wishlistArgs) {
			try {
				App.ajax({
					url: App.Transport.getCommandURL("getWishlist"),
					data: {
						guestAccessKey: wishlistArgs.wishlistId,
						sortBy: wishlistArgs.sortBy
					},
					dataType: App.Config.defaultDataType,
					contentType: App.Config.defaultContentType,
					jsonp: App.Config.defaultJSONCallback,						
					success: function(rv) {
						if (rv.errorCode) {
							wishlistArgs.errorCallback.apply(this, arguments);
						} else {
							wishlistArgs.callback.apply(this, arguments);
						}
					},
					error: wishlistArgs.errorCallback
				});
			}  catch (e) {
				App.Error.log(e);	
			}
		},

		// Summary: attachs an item(s) to a known wishlist 
		_addItemToWishlist: function (wishlistArgs) {
			try {
				App.ajax({
					url: App.Transport.getCommandURL("addItemToWishlist"),
					data: {
						giftListId: wishlistArgs.wishlistId,
						catEntryId_1: wishlistArgs.itemId,
						quantity_1: wishlistArgs.qty || 1,
					},
					dataType: App.Config.defaultDataType,
					contentType: App.Config.defaultContentType,
					jsonp: App.Config.defaultJSONCallback,
					success: function(rv) {
						if (rv.errorCode) {
							wishlistArgs.errorCallback.apply(this, arguments);
						} else {
							wishlistArgs.callback.apply(this, arguments);
						}
					},
					error: wishlistArgs.errorCallback
				});
			} catch (e) {
				App.Error.log(e);
			}
		},

		// Summary: creates a wishlist while attaching an item(s)
		_createWishlistWithItem: function(wishlistArgs) {
			try {
				App.ajax({
					url: App.Transport.getCommandURL("createWishlist"),
					data: {
						name: wishlistArgs.name,
						catEntryId_1: wishlistArgs.itemId,
						quantity_1: wishlistArgs.qty || 1
					},
					dataType: App.Config.defaultDataType,
					contentType: App.Config.defaultContentType,
					jsonp: App.Config.defaultJSONCallback,						
					success: function(rv) {
						if (rv.errorCode) {
							wishlistArgs.errorCallback.apply(this, arguments);
						} else {
							wishlistArgs.callback.apply(this, arguments);
						}
					},
					error: wishlistArgs.errorCallback
				});
			} catch (e) {
				App.Error.log(e);
			}
		},

		// Summary: add an item to the shopping bag
		_addToBag: function (wishlistArgs) {
			try {
				App.ajax({
					url: App.Transport.getCommandURL("addToBag"),
					data: {
						orderId: ".",																		// Hardcode comes from the specification
						calculationUsage: "-1,-2,-5,-6,-7",													// Hardcode comes from the specification
						catEntryId: wishlistArgs.catEntryId,												// Item ID
						quantity: 1,																		// We're allowing to add 1 item to time to the cart
						externalId: wishlistArgs.wishListId, 
					},
					dataType: App.Config.defaultDataType,
					contentType: App.Config.defaultContentType,
					jsonp: App.Config.defaultJSONCallback,
					success: function(rv) {
						if (rv.errorCode) {
							wishlistArgs.errorCallback.apply(this, arguments);
						} else {
							wishlistArgs.callback.apply(this, arguments);
							window.updateMinibagCount && window.updateMinibagCount();
						}
					},
					error: wishlistArgs.errorCallback
				}); 				
			} catch (e) {
				App.Error.log(e);
			}
		},

		// Summary: send an email 
		_mailTo: function (envelopArgs) {
			try {
				App.ajax({
					url: App.Transport.getCommandURL("mailTo"),
					data: {
						giftListId: envelopArgs.wishlistId,
						senderName: envelopArgs.userName,
						senderEmail: envelopArgs.from,
						recipientEmail: envelopArgs.toList,														// Comma separated list no leading trails
						emailSubject: envelopArgs.subject,
						emailSubjectAppend: envelopArgs.subjectAppend,
						emailTemplate: envelopArgs.template || "SOA_EMAIL_TEMPLATE",							// fallback to default given the specification
						wishlistMessage: envelopArgs.message
					},
					dataType: App.Config.defaultDataType,
					contentType: App.Config.defaultContentType,
					jsonp: App.Config.defaultJSONCallback,
					success: function(rv) {
						if (rv.errorCode) {
							envelopArgs.errorCallback.apply(this, arguments);
						} else {
							envelopArgs.callback.apply(this, arguments);
						}
					},
					error: envelopArgs.errorCallback
				});					

			} catch (e) {
				App.Error.log(e);
			}
		}
	}
});
;
// Source: src/js/util/config.js
$.extend(App, {
    Config: {
		
		// APP LEVEL
		isDevModeEnabled: true,
		defaultCountry: "US",
		defaultLocale: "en_US",
		currency: "$",

		// TRANSPORT
		useOnlyHttps: false,
		isStatic: true,
		publishing: "remote", 													
		defaultDataType: "JSONP",
		defaultJSONCallback: 'tcpCallBack',
		defaultContentType: "application/json",

		// COMPONENT PREF
		wishlist: {
			wishlist: window.wishlist_limit || 5,
			products: window.wishlist_product_limit || 50
		},
		
		// ENDPOINTS
		environmentURL: {
			"remote": document.location.hostname + "/",
			"local": "localhost:9000/"
		},
		contextualPath: {
			"remote": "webapp/wcs/stores/servlet/",
			"local": "services/"
		},
		environmentSuffix: "",
		commands: {
			"remote": {
				createWishlist: "AjaxGiftListServiceCreate",
				changeStatus:"AjaxGiftListServiceChangeGiftListStatus",
				updateWishlistName: "AjaxGiftListServiceUpdateDescription",
				deleteWishlist: "AjaxGiftListServiceDeleteGiftList",
				getToken: "",
				getWishlist: "TCPGetWishListForUsersView",
				products: "TCPGetWishListItemsForSelectedListView",
				espot: "MarketingSpotData",
				deleteItemFromwishlist: "AjaxGiftListServiceUpdateItem",
				logon: "Logon",
				addItemToWishlist: "AjaxGiftListServiceAddItem",
				addToBag: "AjaxOrderChangeServiceItemAdd",
				addedToBag: "CreateCookieCmd",
				mailTo: "TCPAjaxWishlistShare"

			},
			"local": {
				getToken: "loginidentity.json",
				getWishlist: "wishlist.json",
				products: "productview/byId/",
				updateWishlist: "wishlist/"
			}
		},
		
		// IMAGES
		imagesBasePath: "/wcsstore/GlobalSAS/images/tcp/products/500/",
	}
});;
// Source: src/js/util/error.js
$.extend(App, {
	Error: {
		log: function (e) {
			if (App.Config.isDevModeEnabled) {
				console && console.log(e.stack);
			}
		},
		reset: function (containerReference) {
			if (!containerReference && containerReference === null) {
				throw new Error;
				return;
			}
			$(containerReference).removeClass("inline-error");
		},
		render: function (containerReference, errorCode, callback) {
			try {
				if (!containerReference && containerReference === null) {
					containerReference = App.UI.elements.defaultErrorContainer;
				}
				$(containerReference).html(App.Messages[App.Config.defaultLocale][errorCode]);
				$(containerReference).show();
				if ( typeof callback !== "undefined" && typeof callback === "function") {
					this.delegate(callback);
				}
			} catch (e) {
				App.Error.log(e);
			}
		},
		inline: function (containerReference, errorCode, callback) {
			try {
				if (!containerReference && containerReference === null) {
					throw new Error;
					return;
				}
				$(containerReference).addClass("inline-error");
				$(containerReference).html(App.Messages[App.Config.defaultLocale][errorCode]);
				$(containerReference).show();
				if ( typeof callback !== "undefined" && typeof callback === "function") {
					this.delegate(callback);
				}
			} catch (e) {
				App.Error.log(e);
			}			
		},
		delegate: function(c) {
			if ( typeof c !== "undefined" && typeof c === "function") {
				c();
			} else {
				throw new Error;
				return;
			}
		}
	}	
});
;
// Source: src/js/util/gui.js
$.extend(App, {
	GUI : {
		Modal: {
			isVeilEnabled: false,
			
			show: function (container) {
				try {
					if (!this.isVeilEnabled) {
						$(container).show();
						this.isVeilEnabled = true;		
					}
				} catch (e) {
					App.Error.log(e);
				}
			},
			
			hide: function (container) {
				try {
					if (this.isVeilEnabled) {
						$(container).hide();
						this.isVeilEnabled = false;
					}
				} catch (e) {
					App.Error.log(e);
				}
			},
			
			setup: function (container, title, enableCloseButton, context) {
				try {
					var t = $(container + " header h3"),
						b = $(container + " header a");
					(title !== null) ?  t.text(title) : t.text(App.GUI.Messages.en_US.defaultModalTile);
					
					(enableCloseButton === true) ? b.off("click-close-modal").on("click-close-modal", App.GUI.Modal.hide(container)) : b.hide();
					
					if (context !== undefined) {
						$(container).append(context);
					}
				} catch (e) {
					App.Error.log(e);
				}
			}
		}
	}	
});;
// Source: src/js/util/helpers.js
Handlebars.registerHelper("cardNameToCardType", window.cardNameToCardType = function(name) {
    name = (name || "").toLowerCase();

    switch(name) {
        case "anntaylorloft mastercard":
        case "anntaylorloftmastercard":
            return "anntaylorLoftMasterCard";
        case "anntaylorloft card":
        case "anntaylorloftcard":
            return "anntaylorLoftCard";
        case "american express":
        case "americanexpress":
            return "americanExpress";
        case "diners club":
        case "dinersclub":
            return "dinersClub";
        case "mastercard":
            return "masterCard";
        case "discover":
        case "jcb":
        case "visa":
        default:
			return name;
    }
});

Handlebars.registerHelper("getSize", function(Size, itemId) {
    try {
        return Size[itemId].TCPSize;
    } catch (e) {
       console.error(e);
    }
});

Handlebars.registerHelper("getFit", function(Size, itemId) {
    try {
        return Size[itemId].TCPFit;
    } catch (e) {
       console.error(e);
    }
});

Handlebars.registerHelper("parseInt", function(num) {
    return parseInt(num);
});

Handlebars.registerHelper("foreach", function(arr, options) {
    var elements = null;

    if (!$.isArray(arr)) {
        var cp = [],
            index = null;

        for (index in arr) {
            cp.push({
                key: index,
                value: arr[index]
            });
        }

        elements = cp;
    } else {
        elements = arr;
    }

    if (options.inverse && !elements.length)
        return options.inverse(this);

    return $.map(elements, function(item, index) {
        item.$index = index;
        item.$first = index === 0;
        item.$last = index === elements.length - 1;
        return options.fn(item);
    }).join("");
});

Handlebars.registerHelper("for", function(from, to, incr, block) {
    var accum = "",
        i;

    for (i = from; i < to; i += incr) {
        accum += block.fn(i);
    }

    return accum;
});

Handlebars.registerHelper("phoneFormat", window.phoneFormat = function(str) {
    var v = str.replace(/[\D]/g, ""),
        f = v;
        max=10;
             if (v.length > max)
            {
                v= v.substring(0, max);
            }
            if (v.length > 3) {
                f = "(" + v.substr(0, 3) + ") ";

                if (v.length > 6) {
                    f += v.substr(3, 3) + "-" + v.substr(6);
                } else {
                    f += v.substr(3);
                }
            }
    return f;
});

Handlebars.registerHelper("maskCCForType", window.maskCCForType = function(suffix, type, args) {
    if ((arguments.length < 3 && (!arguments[arguments.length - 1].toLowerCase)) || !suffix || suffix.length < 4) {
        return "";
    }

    var c = "*";

    if (arguments.length === 4 || (arguments.length === 3 && arguments[2].toLowerCase)) {
        c = args;
    }

    type = (type || "").toLowerCase();

    switch (type) {
        case "american express":
        case "americanexpress":
        case "amex":
            if (c != "*") {
                return c + c + c + c + "-" + c + c + c + c + c + c + "-" + c + suffix;
            } else {
                return "************" + suffix;
            }
        default:
            if (c != "*") {
                return c + c + c + c + "-" + c + c + c + c + "-" + c + c + c + c + "-" + suffix;
            } else {
                return "***********" + suffix;
            }
    }
});

Handlebars.registerHelper("substr", function(str, ini, len) {
    ini = parseInt(ini);
    len = parseInt(len);

    if (str && str.length >= ini + len) {
        return str.substr(ini, len);
    } else {
        return str;
    }
});

// to check if orginal string has a substr within it

Handlebars.registerHelper('contains', function(origStr, operator, subStr, options) {

    if(origStr == undefined || subStr == undefined)
        throw new Error("Cannot have undefined strings");

    if(typeof origStr != "string" || typeof subStr != "string")
        throw new Error("Needs to send string values");

    var lc = origStr.toLowerCase(),
        slc = subStr.toLowerCase();

    if (lc.includes) {
        result = lc.includes(slc);
    } else if (lc.contains) {
        result = lc.contains(slc);
    } else {
        result = lc.indexOf(slc) > -1;
    }

    if(operator == 'not' && result)
        return options.inverse(this);

    return options.fn(this);

});

Handlebars.registerHelper("template", function(name, parameters) {
    var p = {};
    if (parameters) {
        p = $.extend(p, parameters);
        p.tabindex = "-1";
    }

	for (var i = 2; i < arguments.length; i += 2) {
		p[arguments[i]] = arguments[i + 1];
	}

    return App.Templates[name](p);
});

Handlebars.registerHelper("set", function(node, key, value) {
    if (!value && value !== false) {
        value = key;
        key = node;
        node = this;
    }

    node[key] = value;
});

Handlebars.registerHelper("toLowerCase", function(value) {
    return value.toString().toLowerCase();
});

Handlebars.registerHelper("supDecimals", function(value) {
    value = window.noZeroFixed(value).toString().split(".");
    return value[0] + "<sup>" + value[1] + "</sup>";
});

Handlebars.registerHelper("encodeURIComponent", function(v) {
    return encodeURIComponent(v);
});

Handlebars.registerHelper("iframe", function(url) {
    return "<iframe src=\"" + url + "\" frameborder=\"no\" marginwidth=\"0\" marginheight=\"0\"></iframe>";
});

Handlebars.registerHelper("eachInMap", function(map, block) {
    var out = "";
    Object.keys(map).map(function(prop) {
        out += block.fn({
            key: prop,
            value: map[prop]
        });
    });
    return out;
});

Handlebars.registerHelper("wrapDays", function(str) {
    str = str.replace(/Monday/gi, "<span>M</span>");
    str = str.replace(/Tuesday/gi, "<span>T</span>");
    str = str.replace(/Wednesday/gi, "<span>W</span>");
    str = str.replace(/Thursday/gi, "<span>Th</span>");
    str = str.replace(/Friday/gi, "<span>F</span>");
    str = str.replace(/Saturday/gi, "<span>S</span>");
    str = str.replace(/Sunday/gi, "<span>Su</span>");

    return str;
});

Handlebars.registerHelper("noWhiteSpace", window.noWhiteSpace = function(value) {
    return value ? value.replace(/ /gi, "") : value;
});

Handlebars.registerHelper("entityDecode", window.entityDecode = function(value) {
    value = (value || "").replace(/&amp;/gi, "&");
    return $("<textarea />").html(value).val();
});

Handlebars.registerHelper("promosClass", function(obj) {
    if ((!obj.promoMessages || obj.promoMessages.length === 0) &&
        (!obj.finalSaleMessage || obj.finalSaleMessage.length === 0) &&
        (!obj.explicitPromoMessages || obj.explicitPromoMessages.length === 0)
    ) {
        return "no-inline-promos";
    }

    return "";
});

Handlebars.registerHelper("pad-and-increase", function(value, ch, location, length) {
    value = (parseInt(value) + 1).toString();
    length = parseInt(length);

    if (value.length >= length) {
        return value;
    }

    var i, c, p = "";

    for (i = 0, c = length - value.length; i < c; i++) {
        p += ch;
    }

    return (location == "left") ? p + value : value + p;
});

Handlebars.registerHelper("ratingChars", function(value) {
    value = value * 1.0;

    var // empty = "&#xf11f;",
    empty = "<b class='empty'>&#xf121;</b> ", // "<b class='empty'>&#xf11f;</b>",
        // half = "<b class='half-full'>&#xf161;</b> <b class='half-empty'>&#xf162;</b>",
        half = "<b class='half-full'>&#xf161;</b>",
        full = "<b class='full'>&#xf121;</b> ",
        rv = "";

    for (var i = 0; i < 5; i++) {
        if (i < value && i + 1 <= value) {
            rv += full;
        } else if (i < value && i + 1 > value) {
            rv += half;
        } else {
            rv += empty;
        }
    }

    return rv;
});

Handlebars.registerHelper("mask", function(value, visible, c) {
    visible = visible * 1;
    c = c || "*";

    if (value.length <= visible) {
        return value;
    }

    var rv = "",
        x = value.length - visible;

    for (var i = 0; i < x; i++) {
        rv += c;
    }

    rv += value.substr(x);
    return rv;
});

// to avoid duplicates
Handlebars.registerHelper("setUsed", function(node, key) {
    if (!key) {
        key = node;
        node = this;
    }

    if (!node.usedKeys) {
        node.usedKeys = {}
    }

    node.usedKeys[key] = 1;
});

// to avoid duplicates
Handlebars.registerHelper("ifUnused", function(node, key, options) {
    if (!node.usedKeys) {
        node.usedKeys = {}
    }

    if (node.usedKeys[key] === 1) {
        return options.inverse(this);
    } else {
        return options.fn(this);
    }
});

Handlebars.registerHelper("isColorEnabled", function(max, index, colors, options) {
    max = max * 1;
    index = index * 1;

    if (index < (max-1) || (index == (max-1) && colors.length == max)) {
        return options.fn(this);
    } else {
        return options.inverse(this);
    }
});

Handlebars.registerHelper("compare", function(lvalue, operator, rvalue, options) {
    var operators, result;

    if (arguments.length < 3) {
        throw new Error("Handlerbars Helper 'compare' needs 3 parameters");
    }

    if (!isNaN(lvalue)) {
        lvalue = parseFloat(lvalue);
    }

    if (!isNaN(rvalue)) {
        rvalue = parseFloat(rvalue);
    }

    if (options === undefined) {
        options = rvalue;
        rvalue = operator;
        operator = "===";
    }

    operators = {
        '==': function(l, r) {
            if (r === "true") {
                return l === true || l === "true";
            } else if (r === "false") {
                return l === false || l === "false";
            } else {
                return l == r;
            }
        },
        '===': function(l, r) {
            return l === r;
        },
        '!=ic': function(l, r) {
            l = l && l.toLowerCase ? l.toLowerCase() : l;
            r = r && r.toLowerCase ? r.toLowerCase() : r;

            return l != r;
        },
        '!=': function(l, r) {
            return l != r;
        },
        '!==': function(l, r) {
            return l !== r;
        },
        '<': function(l, r) {
            return l < r;
        },
        '>': function(l, r) {
            return l > r;
        },
        '<=': function(l, r) {
            return l <= r;
        },
        '>=': function(l, r) {
            return l >= r;
        },
        '&&': function(l, r) {
            return !!l && !! r;
        },
        '||': function(l, r) {
            return !!l || !! r;
        },
        'typeof': function(l, r) {
            if (r == 'number') {
                return !isNaN(parseFloat(l));
            } else {
                return typeof l == r;
            }
        },
        '!typeof': function(l, r) {
            if (r == 'number') {
                return isNaN(parseFloat(l));
            } else {
                return typeof l !== r;
            }
        }
    };

    if (!operators[operator]) {
        throw new Error("Handlerbars Helper 'compare' doesn't know the operator " + operator);
    }

    result = operators[operator](lvalue, rvalue);

    if (result) {
        return options.fn(this);
    } else {
        return options.inverse(this);
    }
});

Handlebars.registerHelper("updateRecipe", function(url, recipe, profileId) {
    var rv = App.QueryString.replace("recipeName", recipe, window.urlHelper(url));

    if (profileId) {
        rv = App.QueryString.replace("profileId", profileId, rv);
    }

    return rv;
});

Handlebars.registerHelper("queryStringParam", function(param, url) {
    return App.QueryString.get(param, url);
});

Handlebars.registerHelper('url', window.urlHelper = function(url) {
    url = $("<i></i>").html(url).text();

    if (url.length < 2 || url.substr(0, 2) == "//") {
        return url;
    }

    if (url.indexOf("http://") < 0 && url.indexOf("https://") < 0) {
        return "https://" + url;
    }

    return url;
});

Handlebars.registerHelper('removeEntities', function(str) {
    return $("<span></span>").html(str).text();
});

Handlebars.registerHelper('formatSimpleDate', function(str,zeroes) {
    zeroes = typeof zeroes === 'undefined' ? true : zeroes;

    var date = str.split(" "), arr, mm, dd, yyyy;
    // added to handle order status nologgedin date mangling

    if (date.length == 1) {
        arr = date[0].split("/");
        mm = arr[0] * 1;
        dd = arr[1] * 1;
        yyyy = arr[2] * 1;

    } else {
        arr = date[0].split(/[\-|\/]/);
        mm = arr[1] * 1;
        dd = arr[2] * 1;
        yyyy = arr[0] * 1;
    }

    if (arr[2].length > 2) {
        mm = arr[0] * 1;
        dd = arr[1] * 1;
        yyyy = arr[2] * 1;
    }

    return ((mm < 10 && zeroes) ? "0" : "") + mm + "/" + ((dd < 10 && zeroes) ? "0" : "") + dd + "/" + yyyy;
});

Handlebars.registerHelper('toFixed', window.noZeroFixed = function(lvalue, cents) {
    if (!lvalue && lvalue !== 0) {
        return "";
    }

    var r, p = "";

    lvalue = lvalue.toString();

    if (cents && !isNaN(cents)) {
        lvalue = lvalue + "." + window.toCents(cents);
    }

    if (typeof lvalue === 'string' && lvalue.substr(0, 1) === "$") {
        p = "$";
        r = parseFloat(lvalue.substr(1).replace(/\,/gi, ""));
    } else {
        r = parseFloat(lvalue.replace ? lvalue.replace(/\,/gi, "") : lvalue);
    }

    // return p + ((r == Math.round(r)) ? r.toFixed(0) : r.toFixed(2));
    return p + r.toFixed(2);
});

Handlebars.registerHelper('toCents', window.toCents = function(lvalue) {
    lvalue = parseInt(lvalue);
    return (lvalue < 10) ? "0" + lvalue : lvalue;
});

Handlebars.registerHelper('toSupFixed', window.noZeroFixedSup = function(lvalue, cents) {
    if (App.Config.priceFormat == "nosup") {
        return window.noZeroFixed(lvalue, cents);
    }

    var r, p = "", rd, d;

    if (cents && !isNaN(cents)) {
        lvalue = lvalue + "." + window.toCents(cents);
    }

    if (typeof lvalue === 'string' && lvalue.substr(0, 1) === "$") {
        p = "<sup>$</sup>";
        r = parseFloat(lvalue.substr(1).replace(/\,/gi, ""));
    } else {
        r = parseFloat(lvalue.replace ? lvalue.replace(/\,/gi, "") : lvalue);
    }

    rd = Math.floor(r);

    if (r == rd) {
        return p + rd;
    } else {
        d = Math.round((r - rd) * 100);

        if (d < 10) {
            d = "0" + d;
        }

        return p + rd + "<sup>" + d + "</sup>";
    }
});

Handlebars.registerHelper('metersToMiles', function(lvalue) {
    if (isNaN(lvalue)) {
        return "0.00";
    }

    return (parseFloat(lvalue) / 1609.344).toFixed(2);
});

Handlebars.registerHelper('feetToMiles', function(lvalue) {
    if (isNan(lvalue)) {
        return "0.00";
    }

    return (parseFloat(lvalue) * 0.000189394).toFixed(2);
});

Handlebars.registerHelper('math', function(lvalue, operator, rvalue) {
    var operators, result;

    if (arguments.length < 3) {
        throw new Error("Handlerbars Helper 'math' needs 3 parameters");
    }

    lvalue = parseFloat(lvalue.substr && lvalue.substr(0, 1) === "$" ? lvalue.substr(1) : lvalue);
    rvalue = parseFloat(rvalue.substr && rvalue.substr(0, 1) === "$" ? rvalue.substr(1) : rvalue);

    operators = {
        '*': function(l, r) {
            return l * r;
        },
        '+': function(l, r) {
            return l + r;
        },
        '-': function(l, r) {
            return l - r;
        },
        '/': function(l, r) {
            return l / r;
        },
        '%': function(l, r) {
            return l % r;
        },
        '=': function(l, r) {
            return r;
        },
        'max': function(l, r) {
            if (l > r) {
                return l;
            } else {
                return r;
            }
        },
        'min': function(l, r) {
            if (l > r) {
                return r;
            } else {
                return l;
            }
        }
    };

    if (!operators[operator]) {
        throw new Error("Handlerbars Helper 'math' doesn't know the operator " + operator);
    }

    // apply the global toFixed helper
    return window.noZeroFixed(operators[operator](lvalue, rvalue));
});


// Workaround to allow for conditionals in Handlebars.

Handlebars.registerHelper("xif", function (expression, options) {
    return Handlebars.helpers["x"].apply(this, [expression, options]) ? options.fn(this) : options.inverse(this);
  });

// Conditional Expressions in Handlebars
// Iterate through an object and return true if any of the object's attributes are defined
Handlebars.registerHelper("x", function (expression, options) {
  var fn = function(){}, result;

  // in a try block in case the expression have invalid javascript
  try {
    // create a new function using Function.apply, notice the capital F in Function
    fn = Function.apply(
      this,
      [
        'window', // or add more '_this, window, a, b' you can add more params if you have references for them when you call fn(window, a, b, c);
        'return ' + expression + ';' // edit that if you know what you're doing
      ]
    );
  } catch (e) {
    console.warn('[warning] {{x ' + expression + '}} is invalid javascript', e);
  }

  // then let's execute this new function, and pass it window, like we promised
  // so you can actually use window in your expression
  // i.e expression ==> 'window.config.userLimit + 10 - 5 + 2 - user.count' //
  // or whatever
  try {
    // if you have created the function with more params
    // that would like fn(window, a, b, c)
    result = fn.bind(this)(window);
  } catch (e) {
    console.warn('[warning] {{x ' + expression + '}} runtime error', e);
  }
  // return the output of that result, or undefined if some error occured
  return result;
});


// Output logs to console
Handlebars.registerHelper("debug", function(optionalValue) {
    console.log("Current Context");
    console.log("====================");
    console.log(this);
    if (optionalValue) {
        console.log("Value");
        console.log("====================");
        console.log(optionalValue);
    }
});

Handlebars.registerHelper("increase", function(node, key) {
    if (!key && key !== false) {
        key = node;
        node = this;
    }

    node[key]++;
});
;
// Source: src/js/util/messages.js
$.extend(App, { 
	Messages: {
		en_US: {
			/* Generic Error */
			defaultModalTile: "",
			errRetrievingInformation: "Error retrieving information from server",
			componentNotFound: "Component not found:",
			errorInitializingComponent: "Error initializing component:",
			initializing: "Initializing: ",
									
			/* Wishlist UI */
			noProductsInWishlist: "Oh no! Your wishlist is empty!",
			addToBagFromWishlist: "Add to Bag",
			moveToBagFromWishlist: "move",
			deleteFromWishlist: "delete",
			itemLastUpdatedFromWishlist: "",
			editWishlistCTAModeOffCopy: "Manage your wishlists",
			editWishlistCTAModeOnCopy: "Click here to exit the edit mode",
			editWishlistHelpModeOnCopy: "Placeholder: Copy with help about the functionality",
			maxWishlistCapReached: "Yikes! You've reached the maximum number of Wishlists. Please add to an existing list or delete a list to create a new one.",
			maxItemsCapReached: "This Wishlist has been filled to the max... and that's a lot of wishes! To continue adding, remove older items you no longer want.",
			missingInfo: "-- MISSING INFO --",

			/* Wishlist Modal(s) */
			createANewWishlist: "Create a new Wishlist",
			manageYourWishlist: "Manage your Wishlist",


			/* main toolbar */
			wishlistMainCTA: "wishlist",

			wishlistDeleteMessage: function(item) {
				return "Are you sure you want to delete <br /><strong>" + item + "</strong>?";
			},
			wishlistDeleteItemMessage: function(item) {
				return "Are you sure you want to remove<br /><strong>" + item + "</strong>?";
			},
			wishlistDeleteButton: "Yes",
			wishlistCancelButton: "No",

			/* confirmation box */
			defaultConfirmMessage: "Are you sure?",
			confirmYesLabel: "Yes",
			confirmNoLabel: "No",

			emailSubmitSuccess: "Thanks! Your email has been sent.",
			emailSubmitError: "There was an error sending the email. Please try again."
		}
	}
});;
// Source: src/js/util/transport.js
$.extend(App, {
	Transport : {	

		_loaderReference: null,

		SUFFIX: {
			registryAccessPreference: "Public",
			requesttype: "ajax",
			storeId: window.storeId || "10151",
			catalogId: window.catalogId || "10551",
			langId: -1
		},	
		
		getCommandURL: function (cmd, forceHTTPS) {
			var hostProtocol = (("https:" == document.location.protocol) ? "https" : "http"), rv;
			hostProtocol = (forceHTTPS) ? "https" : hostProtocol;
			
			rv = hostProtocol + "://";
			rv += App.Config.environmentURL[App.Config.publishing] + App.Config.contextualPath[App.Config.publishing];
			rv += App.Config.commands[App.Config.publishing][cmd]; 
			return rv;
		},
		
		getCommandSuffix: function () {
			return $.param(this.SUFFIX);
		},

		set loader ($loaderContainer) { this._loaderReference = $loaderContainer; },
		get loader () { return this._loaderReference; },

		enableLoader: function ($refHolderContainer) {
			try {
				if (!$refHolderContainer.hasClass("loading")){
					$refHolderContainer.addClass("loading");
				} 
				if ($refHolderContainer.find(".loader").length == 0 && this.loader != null) {
					$refHolderContainer.append(this.loader);
				}
				this.loader.addClass("active");
			} catch (e) {
				App.Error.log(e);
			}
		},

		disableLoader: function ($refHolderContainer) {
			try {
				$refHolderContainer.find(".loader").remove();
				$refHolderContainer.removeClass("loading");
			} catch (e) {
				App.Error.log(e);
			}
		}

	}
});;
// Source: src/js/util/ui-checkbox.js
$.extend(App.UI, {
    checkbox: function($elements, cls) {
        $elements = typeof $elements == 'undefined' || typeof $elements == 'function' ? $("input[type=checkbox]") : $($elements);
        cls = cls || "custom-checkbox";

        $elements.each(function() {
            var $this = $(this);

            if (!$this.parent().hasClass(cls)) {
                $this.wrap("<div class='" + cls + "'></div>");
            }
            
            $this.off("change.checkbox").on("change.checkbox", function() {
                if ($(this).is(":checked")) {
                    $this.parent().addClass("checked");
                } else {
                    $this.parent().removeClass("checked");
                }
            }).off("focus.checkbox").on("focus.checkbox", function() {
                $this.parent().addClass("focused");
            }).off("blur.checkbox").on("blur.checkbox", function() {
                $this.parent().removeClass("focused");
            }).trigger("change.checkbox");
        });
    }
});
;
// Source: src/js/util/ui-radio.js
$.extend(App.UI, {
    radio: function($elements, cls) {
        $elements = typeof $elements == 'undefined' || typeof $elements == 'function' ? $("input[type=radio]") : $($elements);
        cls = cls || "custom-radio";

        $elements.each(function() {
            var $this = $(this);

            if (!$this.parent().hasClass(cls)) {
                $this.wrap("<div class='" + cls + "'></div>");
            }

            $this.off("change.radio").on("change.radio", function() {
            	if ($(this).is(":checked")) {
                	$(this.form).find("[name=" + this.name + "]").parent().removeClass("checked");
                    $this.parent().addClass("checked");
                } else {
                    $this.parent().removeClass("checked");
                }
            }).trigger("change.radio");
        });
    }
});;
// Source: src/js/util/util.js
$.extend(App, {
	Util : {
		sortBy:	function ($input) {
			try {
				var order = ["WHITE","YELLOW","ORANGE","RED","PINK","PURPLE","METALLIC","BLUE","GREEN","MULTI","BEIGE","GRAY","TAN","BROWN","BLACK"],
					c = $input.children();
				if (c.length > 2) {
					c.sort(function(a, b) {
					    a = (order.indexOf($(a).children().children().attr("alt")) === -1) ? order.length : order.indexOf($(a).children().children().attr("alt"));
			    		b = (order.indexOf($(b).children().children().attr("alt")) === -1) ? order.length : order.indexOf($(b).children().children().attr("alt"));
			    		
			    		if(a > b) {
					        return 1;
					    } else if(a < b) {
					        return -1;
					    } else {
					        return 0;
					    }
					});
				} 
				$input.append(c);
			} catch (e) {
				console.log(e);
			}
		},

		confirm: function(options) {
			try {
				options = $.extend({
					container: document,
					isModal: true,
					message: App.Messages[App.Config.defaultLocale].confirmDefault,
					yesLabel: App.Messages[App.Config.defaultLocale].yesLabel,
					yesLabel: App.Messages[App.Config.defaultLocale].noLabel
				}, options);

				options.container.append(App.Templates.confirm(options));
				var $elem = options.container.find(".confirm"),
					close = function(e) {
						e.preventDefault();
						$elem.remove();
						$elem = null;
					};

				$elem.on("click.no", ".confirm-no" + (options.isModal ? ", .close" : ""), function(e) {
					close(e);
					options.noCallback && options.noCallback();
				}).on("click.yes", ".confirm-yes", function(e) {
					close(e);
					options.yesCallback && options.yesCallback();
				});
			} catch (e) {
				console.log(e);
			}
		},

		entityDecode: function(value) {
			try {
			    var newVal;
			    while (value != newVal) {
			        newVal = value;
			        value = $("<textarea />").html(newVal).val();
			    }
			    return newVal;
			} catch (e) {
			    console.log(value);
			    console.log(e);
			    console.log(e.stack);
			}
		}

	}
});

}(window.jQuery || window.$));
window.$legacyJQuery = (window.jQuery || window.$).noConflict(true);
window.jQuery = window.$ = window.$legacyJQuery; 