# 1 "e:\\performance\\scripts\\2017_r3\\03_perf\\ecommerce\\tcp_browse\\\\combined_TCP_Browse.c"
# 1 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h" 1
 
 












 











# 103 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"








































































	

 



















 
 
 
 
 


 
 
 
 
 
 














int     lr_start_transaction   (char * transaction_name);
int lr_start_sub_transaction          (char * transaction_name, char * trans_parent);
long lr_start_transaction_instance    (char * transaction_name, long parent_handle);
int   lr_start_cross_vuser_transaction		(char * transaction_name, char * trans_id_param); 



int     lr_end_transaction     (char * transaction_name, int status);
int lr_end_sub_transaction            (char * transaction_name, int status);
int lr_end_transaction_instance       (long transaction, int status);
int   lr_end_cross_vuser_transaction	(char * transaction_name, char * trans_id_param, int status);


 
typedef char* lr_uuid_t;
 



lr_uuid_t lr_generate_uuid();

 


int lr_generate_uuid_free(lr_uuid_t uuid);

 



int lr_generate_uuid_on_buf(lr_uuid_t buf);

   
# 266 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"
int lr_start_distributed_transaction  (char * transaction_name, lr_uuid_t correlator, long timeout  );

   







int lr_end_distributed_transaction  (lr_uuid_t correlator, int status);


double lr_stop_transaction            (char * transaction_name);
double lr_stop_transaction_instance   (long parent_handle);


void lr_resume_transaction           (char * trans_name);
void lr_resume_transaction_instance  (long trans_handle);


int lr_update_transaction            (const char *trans_name);


 
void lr_wasted_time(long time);


 
int lr_set_transaction(const char *name, double duration, int status);
 
long lr_set_transaction_instance(const char *name, double duration, int status, long parent_handle);


int   lr_user_data_point                      (char *, double);
long lr_user_data_point_instance                   (char *, double, long);
 



int lr_user_data_point_ex(const char *dp_name, double value, int log_flag);
long lr_user_data_point_instance_ex(const char *dp_name, double value, long parent_handle, int log_flag);


int lr_transaction_add_info      (const char *trans_name, char *info);
int lr_transaction_instance_add_info   (long trans_handle, char *info);
int lr_dpoint_add_info           (const char *dpoint_name, char *info);
int lr_dpoint_instance_add_info        (long dpoint_handle, char *info);


double lr_get_transaction_duration       (char * trans_name);
double lr_get_trans_instance_duration    (long trans_handle);
double lr_get_transaction_think_time     (char * trans_name);
double lr_get_trans_instance_think_time  (long trans_handle);
double lr_get_transaction_wasted_time    (char * trans_name);
double lr_get_trans_instance_wasted_time (long trans_handle);
int    lr_get_transaction_status		 (char * trans_name);
int	   lr_get_trans_instance_status		 (long trans_handle);

 



int lr_set_transaction_status(int status);

 



int lr_set_transaction_status_by_name(int status, const char *trans_name);
int lr_set_transaction_instance_status(int status, long trans_handle);


typedef void* merc_timer_handle_t;
 

merc_timer_handle_t lr_start_timer();
double lr_end_timer(merc_timer_handle_t timer_handle);


 
 
 
 
 
 











 



int   lr_rendezvous  (char * rendezvous_name);
 




int   lr_rendezvous_ex (char * rendezvous_name);



 
 
 
 
 
char *lr_get_vuser_ip (void);
void   lr_whoami (int *vuser_id, char ** sgroup, int *scid);
char *	  lr_get_host_name (void);
char *	  lr_get_master_host_name (void);

 
long     lr_get_attrib_long	(char * attr_name);
char *   lr_get_attrib_string	(char * attr_name);
double   lr_get_attrib_double      (char * attr_name);

char * lr_paramarr_idx(const char * paramArrayName, unsigned int index);
char * lr_paramarr_random(const char * paramArrayName);
int    lr_paramarr_len(const char * paramArrayName);

int	lr_param_unique(const char * paramName);
int lr_param_sprintf(const char * paramName, const char * format, ...);


 
 
static void *ci_this_context = 0;






 








void lr_continue_on_error (int lr_continue);
char *   lr_decrypt (const char *EncodedString);


 
 
 
 
 
 



 







 















void   lr_abort (void);
void lr_exit(int exit_option, int exit_status);
void lr_abort_ex (unsigned long flags);

void   lr_peek_events (void);


 
 
 
 
 


void   lr_think_time (double secs);

 


void lr_force_think_time (double secs);


 
 
 
 
 



















int   lr_msg (char * fmt, ...);
int   lr_debug_message (unsigned int msg_class,
									    char * format,
										...);
# 505 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"
void   lr_new_prefix (int type,
                                 char * filename,
                                 int line);
# 508 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"
int   lr_log_message (char * fmt, ...);
int   lr_message (char * fmt, ...);
int   lr_error_message (char * fmt, ...);
int   lr_output_message (char * fmt, ...);
int   lr_vuser_status_message (char * fmt, ...);
int   lr_error_message_without_fileline (char * fmt, ...);
int   lr_fail_trans_with_error (char * fmt, ...);

 
 
 
 
 
# 532 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"

 
 
 
 
 





int   lr_next_row ( char * table);
int lr_advance_param ( char * param);



														  
														  

														  
														  

													      
 


char *   lr_eval_string (char * str);
int   lr_eval_string_ext (const char *in_str,
                                     unsigned long const in_len,
                                     char ** const out_str,
                                     unsigned long * const out_len,
                                     unsigned long const options,
                                     const char *file,
								     long const line);
# 566 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"
void   lr_eval_string_ext_free (char * * pstr);

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
int lr_param_increment (char * dst_name,
                              char * src_name);
# 589 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"













											  
											  

											  
											  
											  

int	  lr_save_var (char *              param_val,
							  unsigned long const param_val_len,
							  unsigned long const options,
							  char *			  param_name);
# 613 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"
int   lr_save_string (const char * param_val, const char * param_name);



int   lr_set_custom_error_message (const char * param_val, ...);

int   lr_remove_custom_error_message ();


int   lr_free_parameter (const char * param_name);
int   lr_save_int (const int param_val, const char * param_name);
int   lr_save_timestamp (const char * tmstampParam, ...);
int   lr_save_param_regexp (const char *bufferToScan, unsigned int bufSize, ...);

int   lr_convert_double_to_integer (const char *source_param_name, const char * target_param_name);
int   lr_convert_double_to_double (const char *source_param_name, const char *format_string, const char * target_param_name);

 
 
 
 
 
 
# 692 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"
void   lr_save_datetime (const char *format, int offset, const char *name);









 











 
 
 
 
 






 



char * lr_error_context_get_entry (char * key);

 



long   lr_error_context_get_error_id (void);


 
 
 

int lr_table_get_rows_num (char * param_name);

int lr_table_get_cols_num (char * param_name);

char * lr_table_get_cell_by_col_index (char * param_name, int row, int col);

char * lr_table_get_cell_by_col_name (char * param_name, int row, const char* col_name);

int lr_table_get_column_name_by_index (char * param_name, int col, 
											char * * const col_name,
											int * col_name_len);
# 753 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"

int lr_table_get_column_name_by_index_free (char * col_name);

 
 
 
 
# 768 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"
int   lr_zip (const char* param1, const char* param2);
int   lr_unzip (const char* param1, const char* param2);

 
 
 
 
 
 
 
 

 
 
 
 
 
 
int   lr_param_substit (char * file,
                                   int const line,
                                   char * in_str,
                                   int const in_len,
                                   char * * const out_str,
                                   int * const out_len);
# 792 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"
void   lr_param_substit_free (char * * pstr);


 
# 804 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"





char *   lrfnc_eval_string (char * str,
                                      char * file_name,
                                      long const line_num);
# 812 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"


int   lrfnc_save_string ( const char * param_val,
                                     const char * param_name,
                                     const char * file_name,
                                     long const line_num);
# 818 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"

int   lrfnc_free_parameter (const char * param_name );







typedef struct _lr_timestamp_param
{
	int iDigits;
}lr_timestamp_param;

extern const lr_timestamp_param default_timestamp_param;

int   lrfnc_save_timestamp (const char * param_name, const lr_timestamp_param* time_param);

int lr_save_searched_string(char * buffer, long buf_size, unsigned int occurrence,
			    char * search_string, int offset, unsigned int param_val_len, 
			    char * param_name);

 
char *   lr_string (char * str);

 
# 917 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"

int   lr_save_value (char * param_val,
                                unsigned long const param_val_len,
                                unsigned long const options,
                                char * param_name,
                                char * file_name,
                                long const line_num);
# 924 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"


 
 
 
 
 











int   lr_printf (char * fmt, ...);
 
int   lr_set_debug_message (unsigned int msg_class,
                                       unsigned int swtch);
# 946 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"
unsigned int   lr_get_debug_message (void);


 
 
 
 
 

void   lr_double_think_time ( double secs);
void   lr_usleep (long);


 
 
 
 
 
 




int *   lr_localtime (long offset);


int   lr_send_port (long port);


# 1022 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"



struct _lr_declare_identifier{
	char signature[24];
	char value[128];
};

int   lr_pt_abort (void);

void vuser_declaration (void);






# 1051 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"


# 1063 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/lrun.h"
















 
 
 
 
 







int    _lr_declare_transaction   (char * transaction_name);


 
 
 
 
 







int   _lr_declare_rendezvous  (char * rendezvous_name);

 
 
 
 
 


int vtc_connect(char * servername, int portnum, int options);
int vtc_disconnect(int pvci);
int vtc_get_last_error(int pvci);
int vtc_query_column(int pvci, char * columnName, int columnIndex, char * *outvalue);
int vtc_query_row(int pvci, int rowIndex, char * **outcolumns, char * **outvalues);
int vtc_send_message(int pvci, char * column, char * message, unsigned short *outRc);
int vtc_send_if_unique(int pvci, char * column, char * message, unsigned short *outRc);
int vtc_send_row1(int pvci, char * columnNames, char * messages, char * delimiter, unsigned char sendflag, unsigned short *outUpdates);
int vtc_update_message(int pvci, char * column, int index , char * message, unsigned short *outRc);
int vtc_update_message_ifequals(int pvci, char * columnName, int index,	char * message, char * ifmessage, unsigned short 	*outRc);
int vtc_update_row1(int pvci, char * columnNames, int index , char * messages, char * delimiter, unsigned short *outUpdates);
int vtc_retrieve_message(int pvci, char * column, char * *outvalue);
int vtc_retrieve_messages1(int pvci, char * columnNames, char * delimiter, char * **outvalues);
int vtc_retrieve_row(int pvci, char * **outcolumns, char * **outvalues);
int vtc_rotate_message(int pvci, char * column, char * *outvalue, unsigned char sendflag);
int vtc_rotate_messages1(int pvci, char * columnNames, char * delimiter, char * **outvalues, unsigned char sendflag);
int vtc_rotate_row(int pvci, char * **outcolumns, char * **outvalues, unsigned char sendflag);
int vtc_increment(int pvci, char * column, int index , int incrValue, int *outValue);
int vtc_clear_message(int pvci, char * column, int index , unsigned short *outRc);
int vtc_clear_column(int pvci, char * column, unsigned short *outRc);
int vtc_ensure_index(int pvci, char * column, unsigned short *outRc);
int vtc_drop_index(int pvci, char * column, unsigned short *outRc);
int vtc_clear_row(int pvci, int rowIndex, unsigned short *outRc);
int vtc_create_column(int pvci, char * column,unsigned short *outRc);
int vtc_column_size(int pvci, char * column, int *size);
void vtc_free(char * msg);
void vtc_free_list(char * *msglist);

int lrvtc_connect(char * servername, int portnum, int options);
int lrvtc_disconnect();
int lrvtc_query_column(char * columnName, int columnIndex);
int lrvtc_query_row(int columnIndex);
int lrvtc_send_message(char * columnName, char * message);
int lrvtc_send_if_unique(char * columnName, char * message);
int lrvtc_send_row1(char * columnNames, char * messages, char * delimiter, unsigned char sendflag);
int lrvtc_update_message(char * columnName, int index , char * message);
int lrvtc_update_message_ifequals(char * columnName, int index, char * message, char * ifmessage);
int lrvtc_update_row1(char * columnNames, int index , char * messages, char * delimiter);
int lrvtc_retrieve_message(char * columnName);
int lrvtc_retrieve_messages1(char * columnNames, char * delimiter);
int lrvtc_retrieve_row();
int lrvtc_rotate_message(char * columnName, unsigned char sendflag);
int lrvtc_rotate_messages1(char * columnNames, char * delimiter, unsigned char sendflag);
int lrvtc_rotate_row(unsigned char sendflag);
int lrvtc_increment(char * columnName, int index , int incrValue);
int lrvtc_noop();
int lrvtc_clear_message(char * columnName, int index);
int lrvtc_clear_column(char * columnName); 
int lrvtc_ensure_index(char * columnName); 
int lrvtc_drop_index(char * columnName); 
int lrvtc_clear_row(int rowIndex);
int lrvtc_create_column(char * columnName);
int lrvtc_column_size(char * columnName);



 
 
 
 
 

 
int lr_enable_ip_spoofing();
int lr_disable_ip_spoofing();


 




int lr_convert_string_encoding(char * sourceString, char * fromEncoding, char * toEncoding, char * paramName);


 
int lr_db_connect (char * pFirstArg, ...);
int lr_db_disconnect (char * pFirstArg,	...);
int lr_db_executeSQLStatement (char * pFirstArg, ...);
int lr_db_dataset_action(char * pFirstArg, ...);
int lr_checkpoint(char * pFirstArg,	...);
int lr_db_getvalue(char * pFirstArg, ...);







 
 



















# 1 "e:\\performance\\scripts\\2017_r3\\03_perf\\ecommerce\\tcp_browse\\\\combined_TCP_Browse.c" 2

# 1 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/SharedParameter.h" 1



 
 
 
 
# 100 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/SharedParameter.h"





typedef int PVCI2;
typedef int VTCERR2;

 
 
 

 
extern PVCI2    vtc_connect(char *servername, int portnum, int options);
extern VTCERR2  vtc_disconnect(int pvci);
extern VTCERR2  vtc_get_last_error(int pvci);

 
extern VTCERR2  vtc_query_column(int pvci, char *columnName, int columnIndex, char **outvalue);
extern VTCERR2  vtc_query_row(int pvci, int columnIndex, char ***outcolumns, char ***outvalues);
extern VTCERR2  vtc_send_message(int pvci, char *column, char *message, unsigned short *outRc);
extern VTCERR2  vtc_send_if_unique(int pvci, char *column, char *message, unsigned short *outRc);
extern VTCERR2  vtc_send_row1(int pvci, char *columnNames, char *messages, char *delimiter,  unsigned char sendflag, unsigned short *outUpdates);
extern VTCERR2  vtc_update_message(int pvci, char *column, int index , char *message, unsigned short *outRc);
extern VTCERR2  vtc_update_message_ifequals(int pvci, char	*columnName, int index,	char *message, char	*ifmessage,	unsigned short 	*outRc);
extern VTCERR2  vtc_update_row1(int pvci, char *columnNames, int index , char *messages, char *delimiter, unsigned short *outUpdates);
extern VTCERR2  vtc_retrieve_message(int pvci, char *column, char **outvalue);
extern VTCERR2  vtc_retrieve_messages1(int pvci, char *columnNames, char *delimiter, char ***outvalues);
extern VTCERR2  vtc_retrieve_row(int pvci, char ***outcolumns, char ***outvalues);
extern VTCERR2  vtc_rotate_message(int pvci, char *column, char **outvalue, unsigned char sendflag);
extern VTCERR2  vtc_rotate_messages1(int pvci, char *columnNames, char *delimiter, char ***outvalues, unsigned char sendflag);
extern VTCERR2  vtc_rotate_row(int pvci, char ***outcolumns, char ***outvalues, unsigned char sendflag);
extern VTCERR2  vtc_increment(int pvci, char *column, int index , int incrValue, int *outValue);
extern VTCERR2  vtc_clear_message(int pvci, char *column, int index , unsigned short *outRc);
extern VTCERR2  vtc_clear_column(int pvci, char *column, unsigned short *outRc);

extern VTCERR2  vtc_clear_row(int pvci, int rowIndex, unsigned short *outRc);

extern VTCERR2  vtc_create_column(int pvci, char *column,unsigned short *outRc);
extern VTCERR2  vtc_column_size(int pvci, char *column, int *size);
extern VTCERR2  vtc_ensure_index(int pvci, char *column, unsigned short *outRc);
extern VTCERR2  vtc_drop_index(int pvci, char *column, unsigned short *outRc);

extern VTCERR2  vtc_noop(int pvci);

 
extern void vtc_free(char *msg);
extern void vtc_free_list(char **msglist);

 


 




 




















 




 
 
 

extern VTCERR2  lrvtc_connect(char *servername, int portnum, int options);
extern VTCERR2  lrvtc_disconnect();
extern VTCERR2  lrvtc_query_column(char *columnName, int columnIndex);
extern VTCERR2  lrvtc_query_row(int columnIndex);
extern VTCERR2  lrvtc_send_message(char *columnName, char *message);
extern VTCERR2  lrvtc_send_if_unique(char *columnName, char *message);
extern VTCERR2  lrvtc_send_row1(char *columnNames, char *messages, char *delimiter,  unsigned char sendflag);
extern VTCERR2  lrvtc_update_message(char *columnName, int index , char *message);
extern VTCERR2  lrvtc_update_message_ifequals(char *columnName, int index, char 	*message, char *ifmessage);
extern VTCERR2  lrvtc_update_row1(char *columnNames, int index , char *messages, char *delimiter);
extern VTCERR2  lrvtc_retrieve_message(char *columnName);
extern VTCERR2  lrvtc_retrieve_messages1(char *columnNames, char *delimiter);
extern VTCERR2  lrvtc_retrieve_row();
extern VTCERR2  lrvtc_rotate_message(char *columnName, unsigned char sendflag);
extern VTCERR2  lrvtc_rotate_messages1(char *columnNames, char *delimiter, unsigned char sendflag);
extern VTCERR2  lrvtc_rotate_row(unsigned char sendflag);
extern int     lrvtc_increment(char *columnName, int index , int incrValue);
extern VTCERR2  lrvtc_clear_message(char *columnName, int index);
extern VTCERR2  lrvtc_clear_column(char *columnName);
extern VTCERR2  lrvtc_clear_row(int rowIndex);
extern VTCERR2  lrvtc_create_column(char *columnName);
extern int     lrvtc_column_size(char *columnName);
extern VTCERR2  lrvtc_ensure_index(char *columnName);
extern VTCERR2  lrvtc_drop_index(char *columnName);

extern VTCERR2  lrvtc_noop();

 
 
 

                               


 
 
 





















# 2 "e:\\performance\\scripts\\2017_r3\\03_perf\\ecommerce\\tcp_browse\\\\combined_TCP_Browse.c" 2

# 1 "globals.h" 1



 
 

# 1 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/web_api.h" 1







# 1 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/as_web.h" 1
























































 




 








 
 
 

  int
	web_add_filter(
		const char *		mpszArg,
		...
	);									 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_add_auto_filter(
		const char *		mpszArg,
		...
	);									 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
	
  int
	web_add_auto_header(
		const char *		mpszHeader,
		const char *		mpszValue);

  int
	web_add_header(
		const char *		mpszHeader,
		const char *		mpszValue);
  int
	web_add_cookie(
		const char *		mpszCookie);
  int
	web_cleanup_auto_headers(void);
  int
	web_cleanup_cookies(void);
  int
	web_concurrent_end(
		const char * const	mpszReserved,
										 
		...								 
	);
  int
	web_concurrent_start(
		const char * const	mpszConcurrentGroupName,
										 
										 
		...								 
										 
	);
  int
	web_create_html_param(
		const char *		mpszParamName,
		const char *		mpszLeftDelim,
		const char *		mpszRightDelim);
  int
	web_create_html_param_ex(
		const char *		mpszParamName,
		const char *		mpszLeftDelim,
		const char *		mpszRightDelim,
		const char *		mpszNum);
  int
	web_custom_request(
		const char *		mpszReqestName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	spdy_custom_request(
		const char *		mpszReqestName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_disable_keep_alive(void);
  int
	web_enable_keep_alive(void);
  int
	web_find(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_get_int_property(
		const int			miHttpInfoType);
  int
	web_image(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_image_check(
		const char *		mpszName,
		...);
  int
	web_java_check(
		const char *		mpszName,
		...);
  int
	web_link(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

	
  int
	web_global_verification(
		const char *		mpszArg1,
		...);							 
										 
										 
										 
										 
										 
  int
	web_reg_find(
		const char *		mpszArg1,
		...);							 
										 
										 
										 
										 
										 
										 
										 
				
  int
	web_reg_save_param(
		const char *		mpszParamName,
		...);							 
										 
										 
										 
										 
										 
										 

  int
	web_convert_param(
		const char * 		mpszParamName, 
										 
		...);							 
										 
										 


										 

										 
  int
	web_remove_auto_filter(
		const char *		mpszArg,
		...
	);									 
										 
				
  int
	web_remove_auto_header(
		const char *		mpszHeaderName,
		...);							 
										 



  int
	web_remove_cookie(
		const char *		mpszCookie);

  int
	web_save_header(
		const char *		mpszType,	 
		const char *		mpszName);	 
  int
	web_set_certificate(
		const char *		mpszIndex);
  int
	web_set_certificate_ex(
		const char *		mpszArg1,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_set_connections_limit(
		const char *		mpszLimit);
  int
	web_set_max_html_param_len(
		const char *		mpszLen);
  int
	web_set_max_retries(
		const char *		mpszMaxRetries);
  int
	web_set_proxy(
		const char *		mpszProxyHost);
  int
	web_set_pac(
		const char *		mpszPacUrl);
  int
	web_set_proxy_bypass(
		const char *		mpszBypass);
  int
	web_set_secure_proxy(
		const char *		mpszProxyHost);
  int
	web_set_sockets_option(
		const char *		mpszOptionID,
		const char *		mpszOptionValue
	);
  int
	web_set_option(
		const char *		mpszOptionID,
		const char *		mpszOptionValue,
		...								 
	);
  int
	web_set_timeout(
		const char *		mpszWhat,
		const char *		mpszTimeout);
  int
	web_set_user(
		const char *		mpszUserName,
		const char *		mpszPwd,
		const char *		mpszHost);

  int
	web_sjis_to_euc_param(
		const char *		mpszParamName,
										 
		const char *		mpszParamValSjis);
										 

  int
	web_submit_data(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	spdy_submit_data(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_submit_form(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										  
										 
										 
										 
										 
										 
										  
										 
										 
										 
										 
										 
										 
										 
										  
										 
										 
										 
										 
										 
										  
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_url(
		const char *		mpszUrlName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	spdy_url(
		const char *		mpszUrlName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int 
	web_set_proxy_bypass_local(
		const char * mpszNoLocal
		);

  int 
	web_cache_cleanup(void);

  int
	web_create_html_query(
		const char* mpszStartQuery,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int 
	web_create_radio_button_param(
		const char *NameFiled,
		const char *NameAndVal,
		const char *ParamName
		);

  int
	web_convert_from_formatted(
		const char * mpszArg1,
		...);							 
										 
										 
										 
										 
										 
										
  int
	web_convert_to_formatted(
		const char * mpszArg1,
		...);							 
										 
										 
										 
										 
										 

  int
	web_reg_save_param_ex(
		const char * mpszParamName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_reg_save_param_xpath(
		const char * mpszParamName,
		...);							
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_reg_save_param_json(
		const char * mpszParamName,
		...);							
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_reg_save_param_regexp(
		 const char * mpszParamName,
		 ...);							
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_js_run(
		const char * mpszCode,
		...);							
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_js_reset(void);

  int
	web_convert_date_param(
		const char * 		mpszParamName,
		...);










# 737 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/as_web.h"


# 750 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/as_web.h"



























# 788 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/as_web.h"

 
 
 


  int
	FormSubmit(
		const char *		mpszFormName,
		...);
  int
	InitWebVuser(void);
  int
	SetUser(
		const char *		mpszUserName,
		const char *		mpszPwd,
		const char *		mpszHost);
  int
	TerminateWebVuser(void);
  int
	URL(
		const char *		mpszUrlName);
























# 856 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/as_web.h"


  int
	web_rest(
		const char *		mpszReqestName,
		...);							 
										 
										 
										 
										 

 
 
 






# 9 "E:\\Program Files (x86)\\HP\\Performance Center Host\\include/web_api.h" 2

















 







 














  int
	web_reg_add_cookie(
		const char *		mpszCookie,
		...);							 
										 

  int
	web_report_data_point(
		const char *		mpszEventType,
		const char *		mpszEventName,
		const char *		mpszDataPointName,
		const char *		mpszLAST);	 
										 
										 
										 

  int
	web_text_link(
		const char *		mpszStepName,
		...);

  int
	web_element(
		const char *		mpszStepName,
		...);

  int
	web_image_link(
		const char *		mpszStepName,
		...);

  int
	web_static_image(
		const char *		mpszStepName,
		...);

  int
	web_image_submit(
		const char *		mpszStepName,
		...);

  int
	web_button(
		const char *		mpszStepName,
		...);

  int
	web_edit_field(
		const char *		mpszStepName,
		...);

  int
	web_radio_group(
		const char *		mpszStepName,
		...);

  int
	web_check_box(
		const char *		mpszStepName,
		...);

  int
	web_list(
		const char *		mpszStepName,
		...);

  int
	web_text_area(
		const char *		mpszStepName,
		...);

  int
	web_map_area(
		const char *		mpszStepName,
		...);

  int
	web_eval_java_script(
		const char *		mpszStepName,
		...);

  int
	web_reg_dialog(
		const char *		mpszArg1,
		...);

  int
	web_reg_cross_step_download(
		const char *		mpszArg1,
		...);

  int
	web_browser(
		const char *		mpszStepName,
		...);

  int
	web_control(
		const char *		mpszStepName,
		...);

  int
	web_set_rts_key(
		const char *		mpszArg1,
		...);

  int
	web_save_param_length(
		const char * 		mpszParamName,
		...);

  int
	web_save_timestamp_param(
		const char * 		mpszParamName,
		...);

  int
	web_load_cache(
		const char *		mpszStepName,
		...);							 
										 

  int
	web_dump_cache(
		const char *		mpszStepName,
		...);							 
										 
										 

  int
	web_reg_find_in_log(
		const char *		mpszArg1,
		...);							 
										 
										 

  int
	web_get_sockets_info(
		const char *		mpszArg1,
		...);							 
										 
										 
										 
										 

  int
	web_add_cookie_ex(
		const char *		mpszArg1,
		...);							 
										 
										 
										 

  int
	web_hook_java_script(
		const char *		mpszArg1,
		...);							 
										 
										 
										 

 
 
 
 
 
 
 
 
 
 
 
 
  int
	web_reg_async_attributes(
		const char *		mpszArg,
		...
	);

 
 
 
 
 
 
  int
	web_sync(
		 const char *		mpszArg1,
		 ...
	);

 
 
 
 
  int
	web_stop_async(
		const char *		mpszArg1,
		...
	);

 
 
 
 
 

 
 
 

typedef enum WEB_ASYNC_CB_RC_ENUM_T
{
	WEB_ASYNC_CB_RC_OK,				 

	WEB_ASYNC_CB_RC_ABORT_ASYNC_NOT_ERROR,
	WEB_ASYNC_CB_RC_ABORT_ASYNC_ERROR,
										 
										 
										 
										 
	WEB_ASYNC_CB_RC_ENUM_COUNT
} WEB_ASYNC_CB_RC_ENUM;

 
 
 

typedef enum WEB_CONVERS_CB_CALL_REASON_ENUM_T
{
	WEB_CONVERS_CB_CALL_REASON_BUFFER_RECEIVED,
	WEB_CONVERS_CB_CALL_REASON_END_OF_TASK,

	WEB_CONVERS_CB_CALL_REASON_ENUM_COUNT
} WEB_CONVERS_CB_CALL_REASON_ENUM;

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

typedef
int														 
	(*RequestCB_t)();

typedef
int														 
	(*ResponseBodyBufferCB_t)(
		  const char *		aLastBufferStr,
		  int				aLastBufferLen,
		  const char *		aAccumulatedStr,
		  int				aAccumulatedLen,
		  int				aHttpStatusCode);

typedef
int														 
	(*ResponseCB_t)(
		  const char *		aResponseHeadersStr,
		  int				aResponseHeadersLen,
		  const char *		aResponseBodyStr,
		  int				aResponseBodyLen,
		  int				aHttpStatusCode);

typedef
int														 
	(*ResponseHeadersCB_t)(
		  int				aHttpStatusCode,
		  const char *		aAccumulatedHeadersStr,
		  int				aAccumulatedHeadersLen);



 
 
 

typedef enum WEB_CONVERS_UTIL_RC_ENUM_T
{
	WEB_CONVERS_UTIL_RC_OK,
	WEB_CONVERS_UTIL_RC_CONVERS_NOT_FOUND,
	WEB_CONVERS_UTIL_RC_TASK_NOT_FOUND,
	WEB_CONVERS_UTIL_RC_INFO_NOT_FOUND,
	WEB_CONVERS_UTIL_RC_INFO_UNAVIALABLE,
	WEB_CONVERS_UTIL_RC_INVALID_ARGUMENT,

	WEB_CONVERS_UTIL_RC_ENUM_COUNT
} WEB_CONVERS_UTIL_RC_ENUM;

 
 
 

  int					 
	web_util_set_request_url(
		  const char *		aUrlStr);

  int					 
	web_util_set_request_body(
		  const char *		aRequestBodyStr);

  int					 
	web_util_set_formatted_request_body(
		  const char *		aRequestBodyStr);


 
 
 
 
 

 
 
 
 
 

 
 
 
 
 
 
 
 

 
 
  int
web_websocket_connect(
		 const char *	mpszArg1,
		 ...
		 );


 
 
 
 
 																						
  int
web_websocket_send(
	   const char *		mpszArg1,
		...
	   );

 
 
 
 
 
 
  int
web_websocket_close(
		const char *	mpszArg1,
		...
		);

 
typedef
void														
(*OnOpen_t)(
			  const char* connectionID,  
			  const char * responseHeader,  
			  int length  
);

typedef
void														
(*OnMessage_t)(
	  const char* connectionID,  
	  int isbinary,  
	  const char * data,  
	  int length  
	);

typedef
void														
(*OnError_t)(
	  const char* connectionID,  
	  const char * message,  
	  int length  
	);

typedef
void														
(*OnClose_t)(
	  const char* connectionID,  
	  int isClosedByClient,  
	  int code,  
	  const char* reason,  
	  int length  
	 );
 
 
 
 
 





# 7 "globals.h" 2

# 1 "lrw_custom_body.h" 1
 




# 8 "globals.h" 2

 
 
int LINK_TT, FORM_TT, TYPE_SPEED;  
	 
	LINK_TT = 10; 
	FORM_TT = 10; 
	TYPE_SPEED = 1;
 
# 62 "globals.h"

# 3 "e:\\performance\\scripts\\2017_r3\\03_perf\\ecommerce\\tcp_browse\\\\combined_TCP_Browse.c" 2

# 1 "vuser_init.c" 1
# 1 "..\\..\\browseFunctions.c" 1
# 1 "..\\..\\browseWorkloadModel.c" 1
int NAV_BROWSE, NAV_SEARCH, NAV_CLEARANCE, NAV_PLACE;  
int DRILL_ONE_FACET, DRILL_TWO_FACET, DRILL_THREE_FACET, DRILL_SUB_CATEGORY;  
int PDP, QUICKVIEW, PRODUCT_QUICKVIEW_SERVICE, RESERVE_ONLINE, MIXED_CART_RATIO, ECOMM_CART_RATIO;  
int APPLY_SORT, RATIO_TCPSkuSelectionView;   
int APPLY_PAGINATE, RATIO_STORE_LOCATOR, RATIO_SEARCH_SUGGEST, API_SUB_TRANSACTION_SWITCH, ESPOT_FLAG;  

	 

	NAV_BROWSE = 95;  
	NAV_SEARCH = 5;
	NAV_CLEARANCE = 0; 
	NAV_PLACE = 0;

	 
	DRILL_ONE_FACET = 35;  
	DRILL_TWO_FACET = 10;  
	DRILL_THREE_FACET = 5;  
	DRILL_SUB_CATEGORY = 50;

	 
	APPLY_SORT = 25;

	 
	APPLY_PAGINATE = 15;  

	 
	 
	PDP = 72; 
	QUICKVIEW = 28; 
	
	RATIO_STORE_LOCATOR  = 5;
	RATIO_SEARCH_SUGGEST = 0;  
	RATIO_TCPSkuSelectionView = 20;	

	PRODUCT_QUICKVIEW_SERVICE = 15;  
	RESERVE_ONLINE = 100;  
	ECOMM_CART_RATIO = 80;  
	BOPIS_CART_RATIO = 20;  
	API_SUB_TRANSACTION_SWITCH = 1;  
	BROWSE_OPTIONS_FLAG = 1;  
	ESPOT_FLAG = 0;
	MULTI_USE_COUPON_FLAG = 0;


# 1 "..\\..\\browseFunctions.c" 2


int index , length , i, randomPercent, isLoggedIn;
char *searchString ;
char *nav_by ;  
char *drill_by ;  
char *sort_by;  
char *product_by;  
 
typedef long time_t;
time_t t;
 
 

void addHeader()
{
	web_add_header("storeId", lr_eval_string("{storeId}") );
	web_add_header("catalogId", lr_eval_string("{catalogId}") );
	web_add_header("langId", "-1");
	
}

void webURL(char *Url, char *pageName)
{
	lr_save_string(Url, "Url");
	lr_save_string(pageName, "pageName");
	web_url ( lr_eval_string("{pageName}") ,
    	     "URL={Url}" ,
             "Resource=0" ,
             "Mode=HTML" ,
             "LAST" ) ;
} 

void api_getESpot_second(){
	if (ESPOT_FLAG == 1) {
		if (isLoggedIn == 1) {  
			addHeader();	
			web_add_header("espotName","GlobalHeaderBannerAboveHeader,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1");
			web_reg_find("TEXT/IC=espotName", "SaveCount=apiCheck", "LAST");
			 
			web_custom_request("getESpots", 
				"URL=https://{api_host}/getESpot", 
				"Method=GET", 
				"Resource=0", 
				"RecContentType=application/json", 
				"LAST");
			 
			 
			 
			 
		}
	}
}

void TCPGetWishListForUsersView()
{
	lr_start_transaction("T25_Common_S01_TCPGetWishListForUsersView");

	web_custom_request("TCPGetWishListForUsersView",
		"URL=http://{host}/webapp/wcs/stores/servlet/TCPGetWishListForUsersView?tcpCallBack=jQuery1113014590106982485151_1473881708524&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&sortBy=&_=1473881708525", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Mode=HTML", 
		"EncType=application/x-www-form-urlencoded", 
		"LAST");

	lr_end_sub_transaction("T25_Common_S01_TCPGetWishListForUsersView", 2);
	
}


int tcp_api(char *apiCall, char *method)
{	int rc;
	lr_param_sprintf ( "apiTransactionName" , "T30_API %s" , apiCall ) ;
	lr_param_sprintf ( "apiURL" , "https://%s/%s" , lr_eval_string("{api_host}"), apiCall ) ;
	lr_param_sprintf ( "apiMethod" , "%s" , method ) ;
	web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", "LAST");  
	web_reg_save_param("errorMessage", "LB=\"errorMessage\": \"", "RB=\",\n", "NotFound=Warning", "LAST");  

	lr_start_transaction(lr_eval_string("{apiTransactionName}"));
	
	web_custom_request(lr_eval_string(apiCall),  
		"URL={apiURL}", 
		"Method={apiMethod}", 
		"RecContentType=text/html", 
		"Mode=HTML", 
		"LAST");
	 
	
 
		if ( strcmp( lr_eval_string("{errorCode}") ,"") != 0 && strcmp(lr_eval_string("{errorMessage}") ,"") != 0 ) {
			lr_fail_trans_with_error( lr_eval_string("{apiTransactionName} Failed with  Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_transaction( lr_eval_string("{apiTransactionName}"), 1);	
		}
		else
			lr_end_transaction( lr_eval_string("{apiTransactionName}"), 0);	
	 
	 
		 
		 
	 
	return 0;
}

void callAPI()
{	lr_continue_on_error(1);

	if (ESPOT_FLAG == 1) 
	{
		tcp_api2("getESpot", "GET", lr_eval_string("{mainTransaction}") );                              
	}
	
 
 
	if (strcmp(lr_eval_string("{callerId}"), "home") != 0) 
	{

		web_add_header("locStore", "False");
		web_add_header("pageName", "orderSummary");
		web_reg_save_param("cartCount", "LB=CartCount\": ", "RB=,", "NotFound=Warning", "LAST");			 
		tcp_api2("getOrderDetails", "GET", lr_eval_string("{mainTransaction}") );                       
	}
	else{

		web_add_header("locStore", "False");
		web_add_header("pageName", "orderSummary");
		web_reg_save_param("cartCount", "LB=CartCount\": ", "RB=,", "NotFound=Warning", "LAST");			 
		tcp_api2("getOrderDetails", "GET", lr_eval_string("{mainTransaction}") );                       
		lr_save_string("0,","cartCount");
		
	}


	tcp_api2("payment/getRegisteredUserDetailsInfo", "GET",  lr_eval_string("{mainTransaction}") );  
	
	tcp_api2("payment/getPointsService", "GET",  lr_eval_string("{mainTransaction}") );              
	
	if (isLoggedIn == 1)
	{
		tcp_api2("tcporder/getAllCoupons", "GET", lr_eval_string("{mainTransaction}") );                
	}
	
 
	api_getESpot_second();
	lr_continue_on_error(0);
}

void call_OPTIONS(char *apiCall)
{
	lr_param_sprintf ( "apiURL" , "https://%s/%s" , lr_eval_string("{api_host}"), apiCall ) ;
	web_add_header("Accept", "*/*");
	web_add_header("Access-Control-Request-Method", "GET");
	web_add_header("Origin", "https://{host}");
	
	if (strcmp(apiCall, "payment/getPointsService") == 0 ){
		
		web_add_header("Access-Control-Request-Headers", "catalogid,langid,storeid");

	}else if (strcmp(apiCall, "getOrderDetails") == 0 ){
		
		web_add_header("Access-Control-Request-Headers", "calc,catalogid,langid,locstore,pagename,storeid");
		
	}else if (strcmp(apiCall, "getESpot") == 0 ){
		
		web_add_header("Access-Control-Request-Headers", "catalogid,devicetype,espotname,langid,storeid");
			
	}else if (strcmp(apiCall, "tcporder/getAllCoupons") == 0 ){
		
		web_add_header("Access-Control-Request-Headers", "catalogid,langid,storeid");
		
	}else if (strcmp(apiCall, "payment/getRegisteredUserDetailsInfo") == 0 ){
		
		web_add_header("Access-Control-Request-Headers", "catalogid,langid,storeid");
		
	}else if (strcmp(apiCall, "payment/giftOptionsCmd") == 0 ){
		
		web_add_header("Access-Control-Request-Headers", "catalogid,langid,storeid");
		
	}else if (strcmp(apiCall, "payment/getCreditCardDetails") == 0 ){
		
		web_add_header("Access-Control-Request-Headers", "catalogid,isrest,langid,storeid");
		
	}else if (strcmp(apiCall, "payment/getAddressFromBook") == 0 ){
		
		web_add_header("Access-Control-Request-Headers", "catalogid,frompage,langid,storeid");
		
	}
 	
# 240 "..\\..\\browseFunctions.c"
}

void executeOptions()
{
		web_custom_request("OPTIONS", 
			"URL={apiURL}", 
			"Method=OPTIONS", 
			"Resource=0", 
			"LAST");
}

int tcp_api2(char *apiCall, char *method, char *mainTransaction)
{	int rc;
	
	if (strcmp(apiCall, "getESpot") != 0 )
	{
		web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", "LAST");  
		web_reg_save_param("errorMessage", "LB=\"errorMessage\": \"", "RB=\",\n", "NotFound=Warning", "LAST");  
	}
	
	if (strcmp(apiCall, "payment/getRegisteredUserDetailsInfo") == 0 )
	{
		web_reg_save_param ( "addressId" , "LB=\"addressId\": \"" , "RB=\",\n" , "ORD=1", "NotFound=Warning", "LAST" ) ;  
		web_reg_save_param ( "addressIdAll" , "LB=\"addressId\": \"" , "RB=\",\n" , "ORD=ALL", "NotFound=Warning", "LAST" ) ;  
		lr_save_string("resourceName","CheckString");
	}	
	if (strcmp(apiCall, "payment/getPointsService") == 0 ){
		lr_save_string("LoyaltyWebsiteInd","CheckString");
	}else if (strcmp(apiCall, "getOrderDetails") == 0 ){
		lr_save_string("{","CheckString");
	}else if (strcmp(apiCall, "getESpot") == 0 ){
		lr_save_string("espotName","CheckString");
		 
		                           
		web_add_header("espotName","GlobalHeaderBannerAboveHeader,GlobalHeaderBannerWithinHeader,GlobalHeaderLogo,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,create_account_drawer,MinicartLoginBanner,MinicartRegisterBanner,drawer_myaccount_overview_banner,coupon_help,bag_empty_banner,drawer_empty_bag,MinicartLoginFromFavoritesBanner,bag_ledger_promo_banner,gift_services_espot,CheckoutConfirmationBanner,CheckoutConfirmationMPRPromo,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1");
		 
		web_add_header("deviceType","desktop");

	}else if (strcmp(apiCall, "tcporder/getAllCoupons") == 0 ){
		lr_save_string("availableCoupons","CheckString");
	}else if (strcmp(apiCall, "tcporder/getXAppConfigValues") == 0 ){
		lr_save_string("xAppAttrValues","CheckString");
	}
	
	if (API_SUB_TRANSACTION_SWITCH == 1)  
	{
		lr_param_sprintf ( "apiMainTransactionName" , "%s" , mainTransaction ) ;
		lr_param_sprintf ( "apiTransactionName" , "%s_%s" , lr_eval_string("{apiMainTransactionName}"), apiCall ) ;
		
		if (strcmp(lr_eval_string("{apiMainTransactionName}"),"") !=0 )
			lr_start_sub_transaction(lr_eval_string("{apiTransactionName}"), lr_eval_string("{apiMainTransactionName}"));
		else
			lr_start_transaction(lr_eval_string("T30_API {apiTransactionName}"));
		
				# 304 "..\\..\\browseFunctions.c"


		lr_param_sprintf ( "apiURL" , "https://%s/%s" , lr_eval_string("{api_host}"), apiCall ) ;
		lr_param_sprintf ( "apiMethod" , "%s" , method ) ;
		
		web_add_header("storeId", lr_eval_string("{storeId}") );
		web_add_header("catalogId", lr_eval_string("{catalogId}") );
		web_add_header("langId", "-1");
		web_reg_find("TEXT/IC={CheckString}","Fail=NotFound", "SaveCount=Mycount","LAST");

		web_custom_request(lr_eval_string(apiCall),  
			"URL={apiURL}", 
			"Method={apiMethod}", 
			"RecContentType=text/html", 
			"Mode=HTML", 
			"LAST");
			
		if ( strcmp( lr_eval_string("{errorCode}") ,"") != 0 && strcmp(lr_eval_string("{errorMessage}") ,"") != 0  && (atoi(lr_eval_string("{Mycount}"))==0) ) 
		{
			lr_fail_trans_with_error( lr_eval_string("{apiTransactionName} Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			if (strcmp(lr_eval_string("{apiMainTransactionName}"),"") !=0 )
				lr_end_sub_transaction( lr_eval_string("{apiTransactionName}"), 1);	
			else
				lr_end_transaction( lr_eval_string("T30_API {apiTransactionName}"), 1);	
		}
		else 
		{
			if (atoi(lr_eval_string("{Mycount}")) > 0 )  {
				if (strcmp(lr_eval_string("{apiMainTransactionName}"),"") !=0 )
					lr_end_sub_transaction( lr_eval_string("{apiTransactionName}"), 2);	
				else
					lr_end_transaction( lr_eval_string("T30_API {apiTransactionName}"), 2);	
			}
			else{
				if (strcmp(lr_eval_string("{apiMainTransactionName}"),"") !=0 )
					lr_end_sub_transaction( lr_eval_string("{apiTransactionName}"), 1);	
				else
					lr_end_transaction( lr_eval_string("T30_API {apiTransactionName}"), 1);	
			}
		}
		
	}
	else
	{
		lr_start_transaction(lr_eval_string("{apiTransactionName}"));
		
		web_custom_request(lr_eval_string(apiCall),  
			"URL={apiURL}", 
			"Method={apiMethod}", 
			"RecContentType=text/html", 
			"Mode=HTML", 
			"LAST");
		rc = web_get_int_property(1);
		
		if (rc == 200) {
			if ( strcmp( lr_eval_string("{errorCode}") ,"") != 0 && strcmp(lr_eval_string("{errorMessage}") ,"") != 0 ) {
				lr_fail_trans_with_error( lr_eval_string("{apiTransactionName} Failed with  Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
				lr_end_transaction( lr_eval_string("T30_API {apiTransactionName}"), 1);	
			}
			else {
				lr_end_transaction( lr_eval_string("T30_API {apiTransactionName}"), 0);	
			}
		}
		else {
			lr_fail_trans_with_error( lr_eval_string("T30_API {apiTransactionName} Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_transaction( lr_eval_string("T30_API {apiTransactionName}"), 1);	
		}
	}		
		
	return 0;
}

webURL2(char *Url, char *pageName, char *textCheck)
{
	lr_save_string(Url, "Url");
	lr_save_string(pageName, "pageName");
	lr_save_string(textCheck, "textCheck");
	web_reg_find("SaveCount=txtCheckCount", "Text/IC={textCheck}", "LAST");
	
	web_reg_find("SaveCount=noProducts", "Text/IC=We're sorry, there are no products available for purchase at this time. Please check back later.", "LAST");
	
	web_url ( lr_eval_string("{pageName}") ,
    	     "URL={Url}" ,
             "Resource=0" ,
             "Mode=HTML" ,
             "LAST" ) ;

	if ( atoi(lr_eval_string("{txtCheckCount}")) > 0 )
		return 0;
	else
		return 1;
			 
} 


void endIteration()
{
	 
}

void homePageCorrelations()
{
 				 
# 432 "..\\..\\browseFunctions.c"
		
}

void homePageCorrelations_OLD()  
{
 						 






 						






	
	web_reg_save_param ( "categories" ,
				 "LB=\t\t\t\t<a href=\"http://{host}{store_home_page}c/" , 
				 "RB=\"" ,
				 "ORD=ALL" , "Convert=HTML_TO_TEXT" , "NotFound=Warning", "LAST" ) ;
							
 






	 		 




		
 







	
 	





	 
	
 	
 
# 508 "..\\..\\browseFunctions.c"
} 


void categoryPageCorrelations()  
{

	web_reg_save_param_regexp( "ParamName=subcategories" ,
								"RegExp=class=\"leftNavLevel0Title block\">[^<]*?<a id='[^']*?' title='[^']*?'[^h]*?href='(.*?)'" ,
							    
	                            
			                   "Ordinal=ALL" ,
			                   "Notfound=warning" ,				   
			                   "SEARCH_FILTERS" ,
							   "IgnoreRedirections=Yes",
			                    
							   "RequestUrl=*{host}*",
							  "LAST" ) ;
						 
	web_reg_save_param ( "facetsURL" ,
						 "LB=<li onclick=\"javascript: refreshAfterFilterGotSelected('http://{host}" ,
	                   	 "RB='" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 "LAST" ) ;

	web_reg_save_param_regexp( "ParamName=facetsID" ,
	                           "RegExp=facetCheckBox\" id =\"(.*?)\" value=\"(.*?)\"" ,
			                   "Ordinal=ALL" ,
			                   "Notfound=warning" ,
			                   "SEARCH_FILTERS" ,
			                   "Group=2" ,
			                   "LAST" ) ;

	web_reg_save_param ( "sortURL" ,
				  		 "LB=<option value=\"http://{host}" ,
	                   	 "RB=\"" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 "LAST" ) ;

	web_reg_save_param ( "searchSubCategories" ,
				  		 "LB=href='http://{host}" ,
	                   	 "RB='>" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 "LAST" ) ;

	web_reg_save_param ( "paginationNextURL" ,
				  		 "LB=goToResultPage('http://{host}" ,
	                   	 "RB='" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 "LAST" ) ;

	web_reg_save_param ( "paginationPageIndex" ,
				  		 "LB=catalogSearchResultDisplay_Context','" ,
	                   	 "RB='" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 "LAST" ) ;

	web_reg_save_param ( "paginationShowAllURL" ,
				  		 "LB=viewall-right-bottom\">\r\n\t\t\t\t\t                <a href=\"http://{host}" ,
	                   	 "RB=\"" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 "LAST" ) ;
						 
} 


void subCategoryPageCorrelations()  
{

	if (strcmp( lr_eval_string("{host}"), "tcp-perf.childrensplace.com" ) == 0)
	{	web_reg_save_param ( "pdpURL" ,
			 "LB=productRow name\">\r\n\t\t\t\r\n\t\t\t\t<a href=\"http://{host}" ,  
 
			 "RB=\" id" ,
			 "ORD=ALL" ,
			 "Convert=HTML_TO_TEXT" ,
			 "NotFound=warning" ,
			 "LAST" ) ;
	}
	else {
		web_reg_save_param ( "pdpURL" ,
 
			 "LB=productRow name\">\r\n\t\r\n\t\t\t\r\n\t\t\t\t\t\r\n\t\t\t\t<a href=\"http://{host}" ,  
			 "RB=\" id" ,
			 "ORD=ALL" ,
			 "Convert=HTML_TO_TEXT" ,
			 "NotFound=warning" ,
			 "LAST" ) ;
	}
	
	web_reg_save_param ( "quickviewURL" ,
	                     "LB=openModal2\" link=\"http://{host}" ,
						 "RB=\" href=" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 "LAST" ) ;
						 
	web_reg_find("SaveCount=noProducts", "Text/IC=We're sorry, there are no products available for purchase at this time. Please check back later.", "LAST");

	
} 


void productDisplayPageCorrelations()
{

	web_reg_save_param ( "skuSelectionSwitchProductID" ,
	                     "LB=confirmSwitchProduct(event,'product_" ,
	                     "RB='" ,
	                     "notFound=Warning",
	                     "ORD=ALL" ,
	                     "LAST" ) ;

	web_reg_save_param ( "skuSelectionProductID" ,
	                     "LB=input type=\"hidden\" id=\"prodId\" value=\"" ,
	                     "RB=\"" ,
	                     "notFound=Warning",
	                     "LAST" ) ;

	 
	web_reg_save_param("productId", "LB=\"productId\" : \"", "RB=\",", "ORD=ALL","NotFound=Warning", "LAST");  
	web_reg_save_param("TCPProductInd", "LB=\"TCPProductInd\" : \"", "RB=\"", "ORD=ALL", "NotFound=Warning", "LAST");  
	web_reg_save_param("bopisCatEntryId", "LB=\"catentry_id\" : \"", "RB=\"", "ORD=ALL", "NotFound=Warning", "LAST");  
	web_reg_save_param("bopisQty", "LB=\"qty\" : \"", "RB=\"", "ORD=ALL", "NotFound=Warning", "LAST");  
	
}  


 
void addToCartCorrelations()
{
	
	web_reg_save_param_regexp( "ParamName=atc_catentryIds" ,
	                           "RegExp=\"catentry_id\" : \"(.*?)\",\r\n\t\t\t\t\t\t\t\t\t\t\t\"item_sku\" : \"(.*?)\",\r\n\t\t\t\t\t\t\t\t\t\t\t\"qty\" : \"([1-9][0-9]*)\"" ,
			                   "Ordinal=ALL" ,
			                   "Notfound=warning" ,
			                   "SEARCH_FILTERS" ,
			                   "Group=1" ,
			                   "LAST" ) ;

 	web_reg_save_param ( "atc_comment" ,
                    	 "LB=<input type=\"hidden\" name=\"comment\" value=\"" ,
                    	 "RB=\"" ,
                    	 "Notfound=warning" ,
                    	 "LAST" ) ;

}  
 
# 789 "..\\..\\browseFunctions.c"

void viewHomePage()
{
	lr_save_string("home", "callerId");
	lr_continue_on_error(1);
	homePageCorrelations();

	web_global_verification("Text/IC=h1 role=\"main\">The store has encountered a problem processing the last request",  "Fail=Found","ID=FindStoreProblem", "LAST" ); 
	
	lr_param_sprintf ( "homePageUrl" , "http://%s%shome" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}") ) ;
	lr_save_string("T01_Home Page", "mainTransaction");
	lr_start_transaction( "T01_Home Page" ) ;
	
	if ( webURL2(lr_eval_string ( "{homePageUrl}" ), "T01_Home Page" , "BEGIN TCPTopCategoriesDisplay.jsp" ) == 0)
	{
		callAPI();
		lr_end_transaction( "T01_Home Page" , 0 ) ;
	}	
	else{
		callAPI();
		lr_fail_trans_with_error( lr_eval_string("T01_Home Page Text Check Failed - {homePageUrl}") ) ;
		lr_end_transaction( "T01_Home Page" , 1 ) ;
	}
	
	if (isLoggedIn == 1)  
	{
		 
		 

	}

	BROWSE_OPTIONS_FLAG = 1;
	
	lr_save_string("Other", "callerId");
	
	lr_continue_on_error(0);
	 
  
}

int getTopCategories()
{
	web_reg_save_param("aaaaaa", "LB=url\": \"", "RB=\"", "ORD=All", "NotFound=Warning", "LAST");
	
 
	
	web_submit_data("TCPLocaleSelection",
	    "Action=http://{host}/shop/{store}/getTopCategories",
		"Method=POST", 
		"Resource=0", 
		"Mode=HTML", 
		"ITEMDATA",
            "Name=storeId", "Value={storeId}", "ENDITEM",
            "Name=catalogId", "Value={catalogId}", "ENDITEM",
		"LAST");
		
 
 
 
 
	return 0;
}


void navByBrowse()
{
	lr_think_time ( LINK_TT ) ;

 

	if ( strcmp(lr_eval_string("{storeId}"), "10152") == 0 )
	{
		index = rand () % 7 + 1 ;
		switch(index)
		{
			case 1: lr_save_string( "girls-clothing-ca", "category"); break;
			case 2: lr_save_string( "toddler-girl-clothes-ca", "category"); break;
			case 3: lr_save_string( "boys-clothing-ca", "category"); break;
			case 4: lr_save_string( "toddler-boy-clothes-ca", "category"); break;
			case 5: lr_save_string( "baby-clothes-ca", "category"); break;
 
			case 6: lr_save_string( "childrens-shoes-kids-shoes-canada", "category"); break;
			case 7: lr_save_string( "kids-accessories-canada", "category"); break;
			default: break;
		}
	}
	else 
	{
		if ( strcmp(lr_eval_string("{host}"), "uatlive3.childrensplace.com") == 0) 
		{
			index = rand () % 5 + 1 ;
			switch(index)
			{
				case 1: lr_save_string( "girls-clothing", "category"); break;
				case 2: lr_save_string( "toddler-girl-clothes", "category"); break;
				case 3: lr_save_string( "boys-clothing", "category"); break;
				case 4: lr_save_string( "toddler-boy-clothes", "category"); break;
				case 5: lr_save_string( "baby-clothes", "category"); break;
	 
	 
	 
				default: break;
			}
		}
		else 
		{
			index = rand () % 7 + 1 ;
			switch(index)
			{
				case 1: lr_save_string( "girls-clothing", "category"); break;
				case 2: lr_save_string( "toddler-girl-clothes", "category"); break;
				case 3: lr_save_string( "boys-clothing", "category"); break;
				case 4: lr_save_string( "toddler-boy-clothes", "category"); break;
				case 5: lr_save_string( "baby-clothes", "category"); break;
	 
				case 6: lr_save_string( "childrens-shoes-kids-shoes", "category"); break;
				case 7: lr_save_string( "kids-accessories-us", "category"); break;
				default: break;
			}
		}
	}	
	
	lr_param_sprintf ( "cateogryPageUrl" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/") , lr_eval_string( "{category}" ) ) ; 

	web_convert_param("cateogryPageUrl", "SourceEncoding=HTML","TargetEncoding=PLAIN", "LAST" );
	
	categoryPageCorrelations();
	subCategoryPageCorrelations();  

	lr_continue_on_error(1);

	lr_save_string("T02_Category Display", "mainTransaction");
	lr_start_transaction( "T02_Category Display" ) ;
	
 
	if (webURL2(lr_eval_string ( "{cateogryPageUrl}" ), "T02_Category Display", "End - SEO H1/Body Contents for Category page" ) == 0 ) 
	{
		callAPI();
		if (isLoggedIn == 1)  
		{
			 
			 
			 

		}


 
			 
 
		
		lr_end_transaction ( "T02_Category Display" , 0 ) ;
	}
	else if ((lr_paramarr_len("subcategories")) == 0)
	{
			lr_fail_trans_with_error( lr_eval_string("T02_Category Display Failed subcategories==0 for- {cateogryPageUrl}") ) ;
			lr_end_transaction ( "T02_Category Display" , 1 ) ;  
	}  
	else {
		lr_fail_trans_with_error( lr_eval_string("T02_Category Display Text Check Failed for - {cateogryPageUrl}") ) ;
		lr_end_transaction ( "T02_Category Display" , 1 ) ;  
	}
	
	lr_continue_on_error(0);
	
}  


void navByClearance()
{
 	lr_think_time ( LINK_TT ) ;
	 
 
 
 
	 

	lr_param_sprintf ( "clearancePageUrl" , "%s" ,  lr_eval_string("{categories_3}") ) ;  

	categoryPageCorrelations();

	subCategoryPageCorrelations();  
	web_reg_find("SaveCount=noProducts", "Text/IC=We're sorry, there are no products available for purchase at this time. Please check back later.", "LAST");
	web_reg_find("Text=FILTER BY", "SaveCount=FilterBy_Count", "LAST" );
	
	lr_save_string("T02_Clearance Display", "mainTransaction");
	lr_start_transaction ( "T02_Clearance Display" ) ;
	
	 
	if (webURL2(lr_eval_string ( "{clearancePageUrl}" ), "T02_Clearance Display", "<!-- BEGIN TCPSearchSetup.jspf-->" ) == 0 ) 
	{
		callAPI();
		if (isLoggedIn == 1)  
		{
			 
			 
		}

			
		if ( atoi(lr_eval_string("{noProducts}")) == 1 ) 
		{
			lr_param_sprintf ( "failedPageUrl" , "T02_Clearance Display Failure : %s", lr_eval_string("{clearancePageUrl}") ) ;
			lr_fail_trans_with_error( lr_eval_string("T02_Clearance Display Text Check Failed for - {failedPageUrl}") ) ;
			lr_end_transaction ( "T02_Clearance Display" , 1 ) ;

		}
		else
			lr_end_transaction ( "T02_Clearance Display" , 0 ) ;

	} 
	else
	{
		callAPI();
		if (isLoggedIn == 1)  
		{
			 
			 
		}

		lr_end_transaction ( "T02_Clearance Display" , 1 ) ;
	}
}  


findNewArrivals()  
{
	int offset = 1;

    char * position;

    char * str = "";

    char * search_str = "Arrivals";  

    while( offset >= 1 ) {

		str = lr_paramarr_random( "clearances" );
		 
	    position = (char *)strstr(str, search_str);

	    offset = (int)(position - str + 1);
    }

    lr_save_string(str, "clearance");

	return 0;
}

void typeAheadSearch()
{
	lr_continue_on_error(1);

	 
    for (i = 0; i < length; i++)
    {
       	 
       	if (i == 0)
           	lr_param_sprintf ("SEARCH_STRING_PARAM", "%c", searchString[i]);
       	else
           	lr_param_sprintf ("SEARCH_STRING_PARAM", "%s%c", lr_eval_string("{SEARCH_STRING_PARAM}"), searchString[i]);
           	 

		if (i == 1) lr_save_string(lr_eval_string("{SEARCH_STRING_PARAM}"),"forDidYouMean");
			
 
		if (i == 2 || i == 5 || i == 8 || i == 11 || i == 14 || i == 17 || i == 20 )
       	{
    		lr_think_time ( TYPE_SPEED );
			 
			 
			
 
 
 
 
 
 
 
			web_reg_save_param_json(
				"ParamName=autoSelectOptionTerm",
				"QueryString=$.autosuggestions..termsArray..term",
				"SelectAll=Yes",
				"SEARCH_FILTERS",
				"NotFound=Warning",
			"LAST");
			web_reg_save_param_json(
				"ParamName=autoSelectOptionCategory",
				"QueryString=$.category..response..categoryUrl",
				"SelectAll=Yes",
				"SEARCH_FILTERS",
				"NotFound=Warning",
			"LAST");	
			web_add_header("isRest","true");
			web_add_header("Content-Type","application/json");
			web_add_header("term","{SEARCH_STRING_PARAM}");
			web_add_header("coreName","MC_10001_CatalogEntry_en_US");
			addHeader();
			
			lr_start_transaction ( "T02_Search_getAutoSuggestion" ) ;
			web_custom_request("getAutoSuggestions", 
						"URL=https://{api_host}/getAutoSuggestions", 
						"Method=GET", 
						"Resource=0", 
						"RecContentType=application/json", 
						"LAST");

 
# 1107 "..\\..\\browseFunctions.c"

				lr_end_transaction ( "T02_Search_getAutoSuggestion" , 0 ) ;
			 
 
			if (atoi(lr_eval_string("{autoSelectOptionTerm_count}")) > 0) {
				searchString = lr_paramarr_idx("autoSelectOptionTerm", 1);
				break;
			}
			else if (atoi(lr_eval_string("{autoSelectOptionCategory_count}")) > 0) {
				searchString = lr_paramarr_idx("autoSelectOptionCategory", 1);
				break;
			} 
 
	
 
 
 
 
			
       	}  
   	 }  
 	lr_continue_on_error(0);

}  


void submitCompleteSearch()  
{
	lr_think_time (LINK_TT);
	categoryPageCorrelations();
	subCategoryPageCorrelations();
	productDisplayPageCorrelations();
	
 
	lr_param_sprintf ("SEARCH_STRING", "%s", lr_eval_string("{originalSearchString}"));   
	
 
	web_reg_find("Text= 0 matches", "SaveCount=searchResults_Count", "LAST" );
	
 
 		





	lr_start_transaction ( "T02_Search" );
	web_url("submitSearch", 
		  "URL=https://{host}/shop/SearchDisplay?searchTerm={SEARCH_STRING}&storeId={storeId}&catalogId={catalogId}&langId=-1&pageSize=100&beginIndex=0&searchSource=Q&sType=SimpleSearch&resultCatEntry=2&showResultsPage=true&pageView=image&custSrch=search",
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Mode=HTML", 
		"LAST");
		
		lr_save_string("T02_Search", "mainTransaction");
		callAPI();
		if (isLoggedIn == 1)  
		{
			 
			 
		}

		
	if ( isLoggedIn == 1 ) {
		TCPGetWishListForUsersView();
	}
	
	if (atoi(lr_eval_string("{searchResults_Count}")) > 0) {  
		lr_end_transaction ( "T02_Search" , 1 ) ; 
	} 
	else {
		lr_end_transaction ( "T02_Search" , 0 ) ;
	}
	
	return;
		
}  
		
		
void submitCompleteSearchDidYouMean()  
{
	 
 	lr_think_time (LINK_TT);
	categoryPageCorrelations();
	subCategoryPageCorrelations();
	productDisplayPageCorrelations();
	
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_SEARCH_SUGGEST )
		lr_param_sprintf ("SEARCH_STRING", "%s", lr_eval_string("{forDidYouMean}"));
	else
		lr_param_sprintf ("SEARCH_STRING", "%s", searchString);
	
	web_reg_find("Text= 0 matches", "SaveCount=searchResults_Count", "LAST" );
	web_reg_find("Text=The store has encountered a problem", "SaveCount=storeProblem_Count", "LAST" );  
	web_reg_save_param("didYouMeanId", "LB=http://{host}/shop/SearchDisplay?searchTermScope=&searchType=1002&filterTerm=&orderBy=&maxPrice=&showResultsPage=true&langId=-1&departmentId=&sType=", 
	"RB=\" class=\"result\">", "ORD=All", "NotFound=Warning", "LAST");

	    
	lr_start_transaction ( "T02_Search" ) ;

	 
	  
	web_url("submitSearch", 
		 
		"URL=https://{host}/shop/SearchDisplay?searchTerm={{SEARCH_STRING}}&storeId={storeId}&catalogId={catalogId}&langId=-1&pageSize=100&beginIndex=0&searchSource=Q&sType=SimpleSearch&resultCatEntry=2&showResultsPage=true&pageView=image&custSrch=search"
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Mode=HTML", 
		"LAST");
		
		lr_save_string("T02_Search", "mainTransaction");
		callAPI();
		if (isLoggedIn == 1)  
		{
			 
			 
		}

		
	if (atoi(lr_eval_string("{searchResults_Count}")) == 0 || atoi(lr_eval_string("{storeProblem_Count}")) == 0 )
		lr_end_transaction ( "T02_Search" , 1 ) ; 
	else
		lr_end_transaction ( "T02_Search" , 0 ) ;

}  
		
writeToFile1() 
{
	char *ip;
    char fullpath[1024], * filename1 = "\\e$\\Performance\\LogOutput\\LowInventory_US_v2.dat";
	long file1;
 
    strcpy(fullpath, "\\\\" );
	ip = lr_get_host_name( );
    strcat(fullpath, ip);
	strcat(fullpath, filename1);
       
    if ((file1 = fopen(fullpath, "a+")) == 0) {
        lr_output_message ("Unable to open %s", fullpath);
        return -1;
    }
	
	fprintf(file1, "%s\n", lr_eval_string("{logLowCatEntryId}"));
	
    fclose(file1);
	return 0;
}

writeToFile2() 
{
	char *ip;
    char fullpath[1024], * filename1 = "\\e$\\Performance\\LogOutput\\LowInventory_CA_v2.dat";
	long file1;
 
    strcpy(fullpath, "\\\\" );
	ip = lr_get_host_name( );
    strcat(fullpath, ip);
	strcat(fullpath, filename1);
       
    if ((file1 = fopen(fullpath, "a+")) == 0) {
        lr_output_message ("Unable to open %s", fullpath);
        return -1;
    }
	
	fprintf(file1, "%s\n", lr_eval_string("{logLowCatEntryId}"));
	
    fclose(file1);
	return 0;
}

void navBySearch()
{
	 

	int searchSuggest = atoi(lr_eval_string("{RANDOM_PERCENT}"));

	lr_think_time ( TYPE_SPEED );
 
# 1303 "..\\..\\browseFunctions.c"
 
	searchString = lr_eval_string("{searchStr}");  
	lr_save_string(searchString,"originalSearchString");
	 
     length = (int)strlen(searchString);





    if (length >= 3)  
    	typeAheadSearch();
	
	if ( searchSuggest < RATIO_SEARCH_SUGGEST )
		submitCompleteSearchDidYouMean();
	else
		submitCompleteSearch();

	 
	
}  


void navByPlace()
{
	 
	if ( atoi( lr_eval_string( "{placeShopCategory_count}")) != 0 ) {
		
		lr_save_string( lr_paramarr_random("placeShopCategory"), "placeShop");
		
		if ( strcmp( lr_eval_string("{placeShop}") ,"placeShops-pjplace-2014" ) == 0) 
			lr_save_string( lr_paramarr_random("placeShopCategory"), "placeShop");
		
		lr_start_transaction ( "T02_PlaceShop" ) ;

		web_url("placeShop",
			"URL=http://{host}{store_home_page}content/{placeShop}",
			"Resource=0",
			"RecContentType=text/html",
			"LAST");
		
		lr_end_transaction ( "T02_PlaceShop" , 0 ) ;
	}
 
# 1355 "..\\..\\browseFunctions.c"
}  


 
 
void topNav()
{
 

	randomPercent = atoi(lr_eval_string("{RANDOM_PERCENT}"));

	if ( randomPercent < NAV_BROWSE ){
		 
		navByBrowse();
		}  
	else if ( randomPercent < (NAV_BROWSE + NAV_CLEARANCE) ) {
		 
		navByClearance();
		}  
	else if ( randomPercent < (NAV_BROWSE + NAV_CLEARANCE + NAV_SEARCH)) {
		 
		navBySearch();
		}  
	else if (randomPercent < (NAV_BROWSE + NAV_CLEARANCE + NAV_SEARCH + NAV_PLACE)) {
		 
		navByPlace();
		}  

}  


void paginate()
{
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) < APPLY_PAGINATE ) {
		
		if (( lr_paramarr_len ( "paginationNextURL" ) > 0 ) ) {
			
			lr_think_time ( LINK_TT ) ;
			
		 
			subCategoryPageCorrelations();
			productDisplayPageCorrelations();
			index = rand ( ) % lr_paramarr_len( "paginationNextURL" ) + 1 ;
			lr_param_sprintf ("paginationURL", "http://%s%s&beginIndex=%s", lr_eval_string("{host}"), lr_paramarr_idx ( "paginationNextURL" , index ), lr_paramarr_idx ( "paginationPageIndex" , index ));
			lr_param_sprintf ("paginationIndex", "paginationPageIndex_%d", index);

			lr_start_transaction ( "T03_Paginate" ) ;
			
				web_submit_data("AjaxCatalogSearchResultView",
					"Action={paginationURL}",
					"Method=POST",
					"RecContentType=text/html",
					"Mode=HTML",
					"ITEMDATA",
					"Name=searchResultsPageNum", "Value=-{paginationIndex}", "ENDITEM",
					"Name=searchResultsView", "Value=", "ENDITEM",
					"Name=searchResultsURL", "Value={paginationURL}", "ENDITEM",
					"Name=objectId", "Value=", "ENDITEM",
					"Name=requesttype", "Value=ajax", "ENDITEM",
					"LAST");
					
			lr_end_transaction ( "T03_Paginate" , 0 ) ;
		}
	}

}  


void sortResults()
{
	 
 	lr_think_time ( LINK_TT ) ;
	if (( lr_paramarr_len ( "sortURL" ) > 0 )) {
		categoryPageCorrelations();
		subCategoryPageCorrelations();
		productDisplayPageCorrelations();
	 
	 
		lr_param_sprintf ( "drillUrl" , "http://%s%s" , lr_eval_string("{host}") , lr_paramarr_random ( "sortURL" ) ) ;

		lr_start_transaction ( "T03_Sort Results" ) ;
			webURL(lr_eval_string ( "{drillUrl}" ), "T03_Sort Results" );
		lr_end_transaction ( "T03_Sort Results" , 0 ) ;
	}  
}  

void sort()
{
 
 
 

 

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) < APPLY_SORT ){
		 
		sortResults();
		}  
	else {
		 
		return;
		}  

}  

void drillOneFacet()
{
	 
 	lr_think_time ( LINK_TT ) ;

	if (( lr_paramarr_len ( "facetsID" ) > 0 ) ) {
		categoryPageCorrelations();
		subCategoryPageCorrelations();
		productDisplayPageCorrelations();
		index = rand ( ) % lr_paramarr_len( "facetsID" ) + 1 ;
		lr_param_sprintf ( "drillUrlOneFacet" , "http://%s%s&facet=%s" , lr_eval_string("{host}") , lr_paramarr_idx ( "facetsURL" , index ) , lr_paramarr_idx ( "facetsID" , index ) ) ;
 
 

		lr_start_transaction ( "T03_Facet Display_1facet" ) ;

			webURL(lr_eval_string ( "{drillUrlOneFacet}" ), "T03_Facet Display_1facet" );

			if ( isLoggedIn == 1 ) {
				 
				 
			}
			lr_save_string("T03_Facet Display_1facet", "mainTransaction");
			callAPI();
			if (isLoggedIn == 1)  
			{
				 
				 
			}


		lr_end_transaction ( "T03_Facet Display_1facet" , 0 ) ;

	}  
}  


void drillTwoFacets()
{
	 
	drillOneFacet();
 	lr_think_time ( LINK_TT ) ;
	if (( lr_paramarr_len ( "facetsID" ) > 0 ) ) {
		categoryPageCorrelations();
		subCategoryPageCorrelations();
		productDisplayPageCorrelations();
 
 
		lr_param_sprintf ( "drillUrlTwoFacets" , "%s&facet=%s" , lr_eval_string("{drillUrlOneFacet}") , lr_paramarr_random ( "facetsID"  ) ) ;
		
		lr_start_transaction ( "T03_Facet Display_2facets" ) ;

			webURL(lr_eval_string ( "{drillUrlTwoFacets}" ), "T03_Facet Display_2facets" );

			if ( isLoggedIn == 1 ) {
				TCPGetWishListForUsersView();
				 
			}
		lr_save_string("T03_Facet Display_2facets", "mainTransaction");
		callAPI();
		
		if (isLoggedIn == 1)  
		{
			 
			 
		}


		lr_end_transaction ( "T03_Facet Display_2facets" , 0 ) ;

	}  
}  


void drillThreeFacets()
{
	 
	drillTwoFacets();
 	lr_think_time ( LINK_TT ) ;
	if (( lr_paramarr_len ( "facetsID" ) > 0 ) ) {
		categoryPageCorrelations();
		subCategoryPageCorrelations();
		productDisplayPageCorrelations();
 
 
		lr_param_sprintf ( "drillUrlThreeFacets" , "%s&facet=%s" , lr_eval_string("{drillUrlTwoFacets}") , lr_paramarr_random ( "facetsID" ) ) ;
		
		lr_start_transaction ( "T03_Facet Display_3facets" ) ;

			webURL(lr_eval_string ( "{drillUrlThreeFacets}" ), "T03_Facet Display_3facets" );

			if ( isLoggedIn == 1 ) {
				TCPGetWishListForUsersView();
				 
			}
		lr_save_string("T03_Facet Display_3facets", "mainTransaction");
		callAPI();
		if (isLoggedIn == 1)  
		{
			 
			 
		}


		lr_end_transaction ( "T03_Facet Display_3facets" , 0 ) ;

	} 
} 


void drillSubCategory()
{
 	lr_think_time ( LINK_TT ) ;
	
	categoryPageCorrelations();
	subCategoryPageCorrelations();
	productDisplayPageCorrelations();

	if (( lr_paramarr_len ( "subcategories" ) > 0 )) {
		index = rand ( ) % lr_paramarr_len( "subcategories" ) + 2 ; 
		
		if (index > lr_paramarr_len( "subcategories" ) )
			index--;
		
		lr_param_sprintf ( "drillUrl" , "%s" , lr_paramarr_idx ( "subcategories", index ) ) ;
		 
		
		lr_continue_on_error(1);
		
		lr_start_transaction ( "T03_Sub-Category Display" ) ;

 
		if ( webURL2(lr_eval_string ( "{drillUrl}" ), "T03_Sub-Category Display", "SHOP BY CATEGORY" ) == 0) 
		{
			lr_save_string("T03_Sub-Category Display", "mainTransaction");
			callAPI();
			if (isLoggedIn == 1)  
			{
				 
				 
			}

		
			if ( isLoggedIn == 1 ) {
				TCPGetWishListForUsersView();
				 
			}
			
			lr_end_transaction ( "T03_Sub-Category Display" , 0 ) ;
		}
		else if ( atoi(lr_eval_string("{noProducts}")) == 1 ) 
		{
				lr_fail_trans_with_error( lr_eval_string("T03_Sub-Category Display Text Check Failed - {drillUrl} AND no products found") ) ;
				lr_end_transaction ( "T03_Sub-Category Display" , 1 ) ;
		}
		else {
				lr_fail_trans_with_error( lr_eval_string("T03_Sub-Category Display Text Check Failed - {drillUrl}") ) ;
				lr_end_transaction ( "T03_Sub-Category Display" , 1 ) ;
		}
		
		lr_continue_on_error(0);

	}  
	else if ((lr_paramarr_len( "searchSubCategories") > 0)) {
		index = rand ( ) % lr_paramarr_len( "subcategories" ) + 2 ; 
		lr_param_sprintf ( "drillUrl" , "http://%s%s" , lr_eval_string("{host}") , lr_paramarr_idx ( "searchSubCategories", index ) ) ;
		 

		lr_continue_on_error(1);
		
		lr_start_transaction ( "T03_Sub-Category Display" ) ;

 
		if ( webURL2(lr_eval_string ( "{drillUrl}" ), "T03_Sub-Category Display", "FILTER BY" ) == 0) {

			lr_save_string("T03_Sub-Category Display", "mainTransaction");
			callAPI();
			if (isLoggedIn == 1)  
			{
				 
				 
			}

				
			if ( isLoggedIn == 1 ) {
				TCPGetWishListForUsersView();
				 
			}

			lr_end_transaction ( "T03_Sub-Category Display" , 0 ) ;
		}
		else if ( atoi(lr_eval_string("{noProducts}")) == 1 ) 
		{
				lr_fail_trans_with_error( lr_eval_string("T03_Sub-Category Display Failed NO PRODUCTS FOUND for - {drillUrl}") ) ;
				lr_end_transaction ( "T03_Sub-Category Display" , 1 ) ;
		}
		else {
				lr_fail_trans_with_error( lr_eval_string("T03_Sub-Category Display Text Check Failed for - {drillUrl}") ) ;
				lr_end_transaction ( "T03_Sub-Category Display" , 1 ) ;
		}

	}  
	
	lr_continue_on_error(0);
	
} 


void drill()
{
 
 
	if (( lr_paramarr_len( "subcategories" ) == 0 ) && (lr_paramarr_len( "searchSubCategories") == 0) && ( lr_paramarr_len ( "facetsID" ) == 0 ) ) {
		 
		 
		return;
	}  
	else if ( lr_paramarr_len ( "subcategories" ) == 0 && (lr_paramarr_len( "searchSubCategories") == 0)) {
		drill_by = "ONE_FACET" ;
		 
		}  
	else if ( lr_paramarr_len ( "facetsID" ) == 0 ) {
		drill_by = "SUB_CATEGORY" ;
		 
		}  
	else {
		 
		 

		randomPercent = atoi(lr_eval_string("{RANDOM_PERCENT}"));

		if ( randomPercent <= DRILL_SUB_CATEGORY ){
			drill_by = "SUB_CATEGORY" ;
			}  
		else if ( randomPercent <= (DRILL_SUB_CATEGORY + DRILL_TWO_FACET) ) {
			drill_by = "TWO_FACET" ;
			}  
		else if ( randomPercent <= (DRILL_SUB_CATEGORY + DRILL_TWO_FACET + DRILL_THREE_FACET)) {
			drill_by = "THREE_FACET" ;
			}  
		else if (randomPercent <= (DRILL_SUB_CATEGORY + DRILL_TWO_FACET + DRILL_THREE_FACET + DRILL_ONE_FACET)) {
			drill_by = "ONE_FACET" ;
			}  

	}  

	if (strcmp(drill_by, "ONE_FACET") == 0) {
		 
		drillOneFacet();
		}  
	else if (strcmp(drill_by, "TWO_FACET") == 0) {
		 
		drillTwoFacets();
		}  
	else if (strcmp(drill_by, "THREE_FACET") == 0) {
		 
		drillThreeFacets();
		}  
	else if (strcmp(drill_by, "SUB_CATEGORY") == 0) {
		 
		drillSubCategory();
		}  
	else if (strcmp(drill_by, "stop") == 0) {
		 
 
		}  
	else if (strcmp(drill_by, "pass") == 0){
 
		 
	}  
}  

void productDetailViewBOPIS()
{
 	lr_think_time ( LINK_TT ) ;
	
	if (( lr_paramarr_len ( "pdpURL" ) > 0 )) {
		
		productDisplayPageCorrelations();

		lr_continue_on_error(1);
		
		lr_param_sprintf ( "drillUrl" , "http://%s%s" , lr_eval_string("{host}") , lr_paramarr_random ( "pdpURL" ) ) ;
		
 

		lr_start_transaction ( "T04_Product Display Page" ) ;
		
 

		if (webURL2(lr_eval_string ( "{drillUrl}" ), "T04_Product Display Page", "Mouse over image to zoom in") == 0) {
			
			addToCartCorrelations();

 
 
			lr_start_sub_transaction ("T04_Product Display Page - TCPSkuSelectionView", "T04_Product Display Page" );

			web_custom_request("TCPSKUSelectionView",
				"URL=http://{host}/webapp/wcs/stores/servlet/TCPSKUSelectionView?langId=-1&storeId={storeId}&catalogId={catalogId}&LimQty=undefined&TCPWebOnlyFlag=&productId={skuSelectionProductID}",
				"Method=GET",
				"Resource=0",
				"Mode=HTML",
				"EncType=application/x-www-form-urlencoded",
				"LAST");
					
			lr_end_sub_transaction ( "T04_Product Display Page - TCPSkuSelectionView", 0 );

			lr_save_string("T04_Product Display Page", "mainTransaction");
			callAPI();
			if (isLoggedIn == 1)  
			{
				 
				 
			}


			if ( isLoggedIn == 1 ) {
				TCPGetWishListForUsersView();
				 
			}
			
			lr_save_string("PDP","PDP_or_PQV");
			lr_end_transaction ( "T04_Product Display Page" , 0 ) ;
		
		}
		else {
			lr_fail_trans_with_error( lr_eval_string("T04_Product Display Text Check Failed - {drillUrl} - {storeId}") ) ;
			lr_end_transaction ( "T04_Product Display Page" , 1 ) ;
		}
		
		lr_continue_on_error(0);

	}  
	
} 

void productDetailView()
{
 	lr_think_time ( LINK_TT ) ;
	
	if (( lr_paramarr_len ( "pdpURL" ) > 0 )) {
		
 
		
		productDisplayPageCorrelations();

		lr_param_sprintf ( "drillUrl" , "http://%s%s" , lr_eval_string("{host}") , lr_paramarr_random ( "pdpURL" ) ) ;
		lr_continue_on_error(1);
		
		lr_start_transaction ( "T04_Product Display Page" ) ;
		
 
		if (webURL2(lr_eval_string ( "{drillUrl}" ), "T04_Product Display Page", "Mouse over image to zoom in") == 0) {

			addToCartCorrelations();
 
 
					

			lr_start_sub_transaction ("T04_Product Display Page - TCPSkuSelectionView", "T04_Product Display Page" );

			web_custom_request("TCPSKUSelectionView",
				"URL=http://{host}/webapp/wcs/stores/servlet/TCPSKUSelectionView?langId=-1&storeId={storeId}&catalogId={catalogId}&LimQty=undefined&TCPWebOnlyFlag=&productId={skuSelectionProductID}",
				"Method=GET",
				"Resource=0",
				"RecContentType=text/html",
				"Snapshot=t18.inf",
				"Mode=HTML",
				"EncType=application/x-www-form-urlencoded",
				"LAST");
					
			lr_end_sub_transaction ( "T04_Product Display Page - TCPSkuSelectionView", 0 );
			
			lr_save_string("T04_Product Display Page", "mainTransaction");
			callAPI();
			if (isLoggedIn == 1)  
			{
				 
				 
			}


			if ( isLoggedIn == 1 ) {
				TCPGetWishListForUsersView();
			}
			
			lr_save_string("PDP","PDP_or_PQV");
			lr_end_transaction ( "T04_Product Display Page" , 0 ) ;
		
		}
		else {
			lr_fail_trans_with_error( lr_eval_string("T04_Product Display Text Check Failed - {drillUrl} - {storeId}") ) ;
			lr_end_transaction ( "T04_Product Display Page" , 1 ) ;
		}
		lr_continue_on_error(0);

	}  
	
}  


void productQuickView()
{
 	lr_think_time ( LINK_TT ) ;
	
	if (( lr_paramarr_len ( "quickviewURL" ) > 0 )) {
		
		productDisplayPageCorrelations();
		
		lr_param_sprintf ( "drillUrl" , "http://%s%s" , lr_eval_string("{host}") , lr_paramarr_random ( "quickviewURL" ) ) ;
		
		lr_start_transaction ( "T04_Product Quickview Page" ) ;
		
		webURL(lr_eval_string ( "{drillUrl}" ), "T04_Product Quickview Page" );
	

		lr_start_sub_transaction ("T04_Product Quickview Page - GetSwatchesAndSizeInfo", "T04_Product Quickview Page" );

		web_custom_request("GetSwatchesAndSizeInfo",
			"URL=http://{host}/webapp/wcs/stores/servlet/GetSwatchesAndSizeInfo?langId=-1&storeId={storeId}&catalogId={catalogId}&productId={skuSelectionProductID}",
			"Method=GET",
			"Resource=0",
			"RecContentType=text/html",
			"Snapshot=t18.inf",
			"Mode=HTML",
			"EncType=application/x-www-form-urlencoded",
			"LAST");

		lr_end_sub_transaction ( "T04_Product Quickview Page - GetSwatchesAndSizeInfo", 0 );

 
 
 
		web_reg_save_param_regexp( "ParamName=atc_catentryIds" ,
	                          "RegExp={ \"(.*?)\" : \"([1-9][0-9]*)\"" ,
	                            
			                   "Ordinal=ALL" ,
			                   "Notfound=warning" ,
			                   "SEARCH_FILTERS" ,
			                   "Group=1" ,
			                   "LAST" ) ;

 
 
							   
 
	   lr_start_sub_transaction ("T04_Product Quickview Page - TCPGetSKUDetailsView", "T04_Product Quickview Page" );

		web_custom_request("TCPGetSKUDetailsView",
			"URL=http://{host}/webapp/wcs/stores/servlet/TCPGetSKUDetailsView?langId=-1&storeId={storeId}&catalogId={catalogId}&productId={skuSelectionProductID}",
			"Method=GET",
			"Resource=0",
			"RecContentType=text/html",
			"Snapshot=t18.inf",
			"Mode=HTML",
			"EncType=application/x-www-form-urlencoded",
			"LAST");

		lr_end_sub_transaction ( "T04_Product Quickview Page - TCPGetSKUDetailsView", 0 );
			

		if ( isLoggedIn == 1 ) 
			TCPGetWishListForUsersView(); 

		lr_save_string("PQV","PDP_or_PQV");

		lr_end_transaction ( "T04_Product Quickview Page" , 0 ) ;
		
		lr_continue_on_error(0);

	}  
	
}  

void productQuickviewService()  
{
	web_reg_find("SaveCount=swatchesAndSizeInfo", "Text/IC=itemTCPBogo","LAST");
	
	lr_continue_on_error(1);

	lr_start_transaction ("T04_Product Quickview Service - GetSwatchesAndSizeInfo" );

		web_custom_request("GetSwatchesAndSizeInfo",
			"URL=http://{host}/webapp/wcs/stores/servlet/GetSwatchesAndSizeInfo?storeId={storeId}&productId={skuSelectionProductID}",
			"Method=GET",
			"Mode=HTML",
			"LAST");

	if(atoi(lr_eval_string("{swatchesAndSizeInfo}")) > 0)
		lr_end_transaction ( "T04_Product Quickview Service - GetSwatchesAndSizeInfo", 0 );	
	else {
		lr_param_sprintf ( "failedPageUrl" , "T03_Sub-Category Display Failure : %s", lr_eval_string("http://{host}/webapp/wcs/stores/servlet/GetSwatchesAndSizeInfo?storeId={storeId}&productId={skuSelectionProductID}") ) ;
		lr_fail_trans_with_error( lr_eval_string("T04_Product Display Text Check Failed - {failedPageUrl} - {storeId}") ) ;
		lr_end_transaction ( "T04_Product Quickview Service - GetSwatchesAndSizeInfo", 1 );	
	}
	lr_continue_on_error(0);

	web_reg_find("SaveCount=detailsView", "Text/IC=inventoryFlag","LAST");

	lr_start_transaction ("T04_Product Quickview Service - TCPGetSKUDetailsView" );

		web_custom_request("TCPGetSKUDetailsView",
			"URL=http://{host}/webapp/wcs/stores/servlet/TCPGetSKUDetailsView?storeId={storeId}&catalogId={catalogId}&langId=-1&productId={skuSelectionProductID}",
			"Method=GET",
			"Mode=HTML",
			"LAST");


	if(atoi(lr_eval_string("{detailsView}")) > 0)
		lr_end_transaction ( "T04_Product Quickview Service - TCPGetSKUDetailsView", 0 );	
	else
		lr_end_transaction ( "T04_Product Quickview Service - TCPGetSKUDetailsView", 1 );	

}

void productDisplay()
{
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= PDP )
	{
		if (( lr_paramarr_len ( "pdpURL" ) > 0 )) 
			productDetailView();  
		else
			productQuickView();
	}
	else 
	{
		if (( lr_paramarr_len ( "quickviewURL" ) > 0 )) 
			productQuickView();  
		else
			productDetailView();
	}
	
}  

void productDisplayBOPIS()
{
	productDetailViewBOPIS();  
}


void switchColor()
{
}  

void emailSignUp()
{
	lr_start_transaction ( "T16_Submit Shipping Address" ) ;
	lr_end_transaction ( "T16_Submit Shipping Address" , 0) ;

}  


void StoreLocator()
{
 	lr_think_time ( LINK_TT ) ;
	
	
	lr_start_transaction("T22_StoreLocator");
	
		lr_save_string("T22_StoreLocator", "mainTransaction" );

		lr_start_sub_transaction("T22_StoreLocator_AjaxStoreLocatorDisplayView", "T22_StoreLocator" );
		web_url("Stores", 
		"URL=http://{host}/shop/AjaxStoreLocatorDisplayView?catalogId={catalogId}&langId=-1&storeId={storeId}", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Mode=HTML", 
		"LAST");

		web_add_header("locStore", "False");
		web_add_header("pageName", "orderSummary");
		web_reg_save_param("cartCount", "LB=CartCount\": ", "RB=,", "NotFound=Warning", "LAST");			
		tcp_api2("getOrderDetails", "GET", lr_eval_string("{mainTransaction}") );                      
 
		tcp_api2("payment/getRegisteredUserDetailsInfo", "GET",  lr_eval_string("{mainTransaction}") );
		tcp_api2("payment/getPointsService", "GET",  lr_eval_string("{mainTransaction}") );            
		lr_end_sub_transaction("T22_StoreLocator_AjaxStoreLocatorDisplayView", 2);

		lr_start_sub_transaction("T22_StoreLocator_AjaxStoreLocatorResultsView", "T22_StoreLocator" );
		web_submit_data("AjaxStoreLocatorResultsView", 
			"Action=http://{host}/shop/AjaxStoreLocatorResultsView?catalogId={catalogId}&langId=-1&orderId=&storeId={storeId}", 
			"Method=POST", 
			"TargetFrame=", 
			"RecContentType=text/html", 
			"Mode=HTML", 
			"ITEMDATA", 
			"Name=distance", "Value={storeLocatorDistance}", "ENDITEM", 
			"Name=latitude", "Value={storeLocatorLatitude}", "ENDITEM", 
			"Name=longitude", "Value={storeLocatorLongitude}", "ENDITEM", 
			"Name=displayStoreInfo", "Value=false", "ENDITEM", 
			"Name=fromPage", "Value=StoreLocator", "ENDITEM", 
			"Name=objectId", "Value=", "ENDITEM", 
			"Name=requesttype", "Value=ajax", "ENDITEM", 
			"LAST");
		lr_end_sub_transaction("T22_StoreLocator_AjaxStoreLocatorResultsView", 2);
		
		lr_start_sub_transaction("T22_StoreLocator_TCPStoreLocatorGetJsonOnStoresByLatLngView", "T22_StoreLocator" );
		web_url("TCPStoreLocatorGetJsonOnStoresByLatLngView", 
		"URL=http://{host}/shop/TCPStoreLocatorGetJsonOnStoresByLatLngView?storeId={storeId}&catalogId={catalogId}&langId=-1&latitude={storeLocatorLatitude}&longitude={storeLocatorLongitude}&distance={{storeLocatorDistance}}", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Mode=HTML", 
		"LAST");
		lr_end_sub_transaction("T22_StoreLocator_TCPStoreLocatorGetJsonOnStoresByLatLngView", 2);
	
	lr_end_transaction("T22_StoreLocator", 2);
	
	lr_start_transaction("T22_StoreLocator_Find");
	
		lr_start_sub_transaction("T22_StoreLocator_Find_StoreInfo_False", "T22_StoreLocator_Find" );
		web_submit_data("AjaxStoreLocatorResultsView", 
			"Action=http://{host}/shop/AjaxStoreLocatorResultsView?catalogId={catalogId}&langId=-1&orderId=&storeId={storeId}", 
			"Method=POST", 
			"TargetFrame=", 
			"RecContentType=text/html", 
			"Mode=HTML", 
			"ITEMDATA", 
			"Name=distance", "Value={storeLocatorDistance}", "ENDITEM", 
			"Name=latitude", "Value={storeLocatorLatitude}", "ENDITEM", 
			"Name=longitude", "Value={storeLocatorLongitude}", "ENDITEM", 
			"Name=displayStoreInfo", "Value=false", "ENDITEM", 
			"Name=fromPage", "Value=StoreLocator", "ENDITEM", 
			"Name=objectId", "Value=", "ENDITEM", 
			"Name=requesttype", "Value=ajax", "ENDITEM", 
			"LAST");
		lr_end_sub_transaction("T22_StoreLocator_Find_StoreInfo_False", 2);
	
		web_reg_save_param("moreLink", "LB=<a href=\"/", "RB=\" class=", "NotFound=Warning", "ORD=All", "LAST");
		lr_start_sub_transaction("T22_StoreLocator_Find_StoreInfo_True", "T22_StoreLocator_Find" );
		web_submit_data("AjaxStoreLocatorResultsView", 
			"Action=http://{host}/shop/AjaxStoreLocatorResultsView?catalogId={catalogId}&langId=-1&orderId=&storeId={storeId}", 
			"Method=POST", 
			"TargetFrame=", 
			"RecContentType=text/html", 
			"Mode=HTML", 
			"ITEMDATA", 
			"Name=distance", "Value={storeLocatorDistance}", "ENDITEM", 
			"Name=latitude", "Value={storeLocatorLatitude}", "ENDITEM", 
			"Name=longitude", "Value={storeLocatorLongitude}", "ENDITEM", 
			"Name=displayStoreInfo", "Value=true", "ENDITEM", 
			"Name=fromPage", "Value=StoreLocator", "ENDITEM", 
			"Name=objectId", "Value=", "ENDITEM", 
			"Name=requesttype", "Value=ajax", "ENDITEM", 
			"LAST");
		lr_end_sub_transaction("T22_StoreLocator_Find_StoreInfo_True", 2);
	
		lr_start_sub_transaction("T22_StoreLocator_Find_TCPStoreLocatorGetJsonOnStoresByLatLngView", "T22_StoreLocator_Find" );
		web_url("TCPStoreLocatorGetJsonOnStoresByLatLngView", 
		"URL=http://{host}/shop/TCPStoreLocatorGetJsonOnStoresByLatLngView?storeId={storeId}&catalogId={catalogId}&langId=-1&latitude={storeLocatorLatitude}&longitude={storeLocatorLongitude}&distance={{storeLocatorDistance}}", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Mode=HTML", 
		"LAST");
		lr_end_sub_transaction("T22_StoreLocator_Find_TCPStoreLocatorGetJsonOnStoresByLatLngView", 2);
		
	lr_end_transaction("T22_StoreLocator_Find", 2);
	
	lr_save_string( lr_paramarr_random("moreLink") , "locationLink" );
	lr_save_string("T22_StoreLocator_MoreLocation", "mainTransaction" );
		
	lr_start_transaction("T22_StoreLocator_More");
		lr_start_sub_transaction("T22_StoreLocator_MoreLocation", "T22_StoreLocator_More" );
		web_url("More", 
		"URL=http://{host}/{locationLink}", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Mode=HTML", 
		"LAST");

		web_add_header("locStore", "False");
		web_add_header("pageName", "orderSummary");
		web_reg_save_param("cartCount", "LB=CartCount\": ", "RB=,", "NotFound=Warning", "LAST");			
		tcp_api2("getOrderDetails", "GET", lr_eval_string("{mainTransaction}") );                      
 
		tcp_api2("payment/getRegisteredUserDetailsInfo", "GET",  lr_eval_string("{mainTransaction}") );
		tcp_api2("payment/getPointsService", "GET",  lr_eval_string("{mainTransaction}") );            
		lr_end_sub_transaction("T22_StoreLocator_MoreLocation", 2);
	
	lr_end_transaction("T22_StoreLocator_More", 2);
	
}

void TCPGetWishListForUsersViewBrowse()
{
	lr_think_time ( LINK_TT ) ;
	
	lr_start_transaction("T25_Common_S01_TCPGetWishListForUsersView");

	web_custom_request("TCPGetWishListForUsersView",
		"URL=http://{host}/webapp/wcs/stores/servlet/TCPGetWishListForUsersView?tcpCallBack=jQuery1113014590106982485151_1473881708524&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&sortBy=&_=1473881708525", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Mode=HTML", 
		"EncType=application/x-www-form-urlencoded", 
		"LAST");

	lr_end_sub_transaction("T25_Common_S01_TCPGetWishListForUsersView", 2);
	
}
void TCPIShippingView()
{
	lr_think_time ( LINK_TT ) ;
	
	lr_start_transaction("T27_TCPIShippingView");
	
	web_custom_request("TCPIShippingView",
		"URL=https://{host}/shop/TCPIShippingView?langId=-1&storeId={storeId}&catalogId={catalogId}",
		"Method=GET",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		"EncType=application/x-www-form-urlencoded",
		"LAST");
		
	lr_end_transaction("T27_TCPIShippingView", 2);
	
	TCPGetWishListForUsersViewBrowse();
	
}

# 1 "vuser_init.c" 2

 

vuser_init()
{	
 	
# 16 "vuser_init.c"

	lr_param_sprintf("NAV_BROWSE", "1. NAV_BROWSE = %d", NAV_BROWSE);
	lr_user_data_point(lr_eval_string("{NAV_BROWSE}"), 0);
	lr_param_sprintf("NAV_SEARCH", "2. NAV_SEARCH = %d", NAV_SEARCH);
	lr_user_data_point(lr_eval_string("{NAV_SEARCH}"), 0);
	lr_param_sprintf("NAV_CLEARANCE", "3. NAV_CLEARANCE = %d", NAV_CLEARANCE);
	lr_user_data_point(lr_eval_string("{NAV_CLEARANCE}"), 0);
	lr_param_sprintf("NAV_PLACE", "4. NAV_PLACE = %d", NAV_PLACE);
	lr_user_data_point(lr_eval_string("{NAV_PLACE}"), 0);

	lr_param_sprintf("DRILL_ONE_FACET", "5. DRILL_ONE_FACET = %d", DRILL_ONE_FACET);
	lr_user_data_point(lr_eval_string("{DRILL_ONE_FACET}"), 0);
	lr_param_sprintf("DRILL_TWO_FACET", "6. DRILL_TWO_FACET = %d", DRILL_TWO_FACET);
	lr_user_data_point(lr_eval_string("{DRILL_TWO_FACET}"), 0);
	lr_param_sprintf("DRILL_THREE_FACET", "7. DRILL_THREE_FACET = %d", DRILL_THREE_FACET);
	lr_user_data_point(lr_eval_string("{DRILL_THREE_FACET}"), 0);
	lr_param_sprintf("DRILL_SUB_CATEGORY", "8. DRILL_SUB_CATEGORY = %d", DRILL_SUB_CATEGORY);
	lr_user_data_point(lr_eval_string("{DRILL_SUB_CATEGORY}"), 0);

	return 0;
}
# 4 "e:\\performance\\scripts\\2017_r3\\03_perf\\ecommerce\\tcp_browse\\\\combined_TCP_Browse.c" 2

# 1 "Action.c" 1
 
Action()
{
	int j, k;
	web_set_max_html_param_len ( "3072" ) ;
	web_cleanup_cookies ( ) ;
	lr_save_string ( lr_get_attrib_string ( "TestEnvironment") , "host" ) ; 
	lr_save_string ( lr_get_attrib_string ( "APIEnvironment") , "api_host" ) ; 
 
	web_set_sockets_option("SSL_VERSION", "TLS1.1"); 
 
 
 
 
 
	
 
 

	if ( atoi( lr_eval_string("{RANDOM_PERCENT}")) <= lr_get_attrib_long("US_Traffic_Percentage") ) {
		lr_save_string( "/shop/us/", "store_home_page" );
		lr_save_string("10151", "storeId");
		lr_save_string("10551", "catalogId");
	} else {
		lr_save_string( "/shop/ca/", "store_home_page" );
		lr_save_string("10152", "storeId");
		lr_save_string("10552", "catalogId");
	}
 	
# 46 "Action.c"
	NO_BROWSE_RATIO = 0;
	viewHomePage();
	viewHomePage();
	viewHomePage();
	viewHomePage();

 
# 63 "Action.c"


 
		
		topNav();
	
		drill();
	
		paginate();

		productDisplay();
 	
# 88 "Action.c"
	return 0;
}
 
# 169 "Action.c"
# 5 "e:\\performance\\scripts\\2017_r3\\03_perf\\ecommerce\\tcp_browse\\\\combined_TCP_Browse.c" 2

# 1 "vuser_end.c" 1
vuser_end()
{
	return 0;
}
# 6 "e:\\performance\\scripts\\2017_r3\\03_perf\\ecommerce\\tcp_browse\\\\combined_TCP_Browse.c" 2

