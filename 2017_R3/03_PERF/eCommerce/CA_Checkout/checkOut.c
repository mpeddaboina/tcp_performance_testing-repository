checkOut()
{
	lr_user_data_point("Abandon Cart Rate", 0);
	lr_user_data_point("Checkout Rate", 1);
	lr_save_string("checkoutFlow", "currentFlow");
	
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CHECKOUT_GUEST ) {
		
		checkoutGuest();
		
	} 
	else {
		lr_save_string( lr_eval_string("{logonid}"), "userEmail" );
  		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CHECKOUT_LOGIN_FIRST ) { //login first
            
			checkoutLoginFirst();
            
        }
        else { 
            
			checkoutBuildCartFirst();
        } 

	} 

	return 0;
}

checkoutGuest()
{
	lr_save_string("1", "totalNumberOfItems_count");
	buildCartCheckout(0); //tempATC();
//	lr_exit(LR_EXIT_MAIN_ITERATION_AND_CONTINUE, LR_PASS);
	
	inCartEdits();
	
	if (proceedAsGuest() == LR_PASS)
	{
		if ( submitShipping() == LR_PASS)  //former submitShippingAddressAsGuest()
		{
			if (BillingAsGuest() == LR_PASS) //submitBillingAddressAsGuest()
			{
				if( submitOrderAsGuest() == LR_PASS)
				{
					if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ORDER_STATUS ) { 
            			viewOrderStatusGuest();
					}
					if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_REGISTER ) {
						createAccount(); 
					}
					if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_STORE_LOCATOR ) {
						StoreLocator();
					}
					
				    if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_FORGOT_PASSWORD ) 
						forgetPassword();	
				}
			}
		}
	}
/*	 //OLD
	buildCartCheckout(0);

	viewCart();//12/04 to add more ordercalculate

	inCartEdits();
		
	proceedToCheckout();

	proceedAsGuest();

	selectShipMode();
	submitShippingAddressAsGuest();

	selectBillingAddress();      //0113 - to see if it is the guest who is making the failures
	//selectBillingAddressAsGuest(); //0113 - to see if it is the guest who is making the failures
	submitBillingAddressAsGuest();

	submitOrder();//0113 - to see if it is the guest who is making the failures
	//submitOrderGuest();//0113 - to see if it is the guest who is making the failures

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_REGISTER ) 
		registerUser();
		
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_STORE_LOCATOR ) { 
		StoreLocator();
	}
*/	
	return 0;
}

checkoutLoginFirst()
{
	int viewHistory = atoi(lr_eval_string("{RANDOM_PERCENT}"));
//	lr_exit(LR_EXIT_MAIN_ITERATION_AND_CONTINUE, LR_PASS);
	
	if (loginFromHomePage() == LR_PASS)
	{
	    viewCart();
	    
	    buildCartCheckout(1); //	    tempATC();
	
		inCartEdits(); 
		
	    if ( atoi(lr_eval_string("{RANDOM_WL_PERCENT}")) <= RATIO_WISHLIST )
	        wishList();//NOT YET DONE

	/*  //0916 - rhy
		if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_REDEEM_LOYALTY_POINTS ) 
			convertAndApplyPoints();
	*/	
		if (proceedToCheckout_ShippingView() == LR_PASS) //proceedToCheckout_ShippingView(); //proceedAsRegistered();
		{
			if (submitShippingRegistered() == LR_PASS)
			{
				if (submitBillingRegistered() == LR_PASS)
				{
					if (submitBillingRegistered() == LR_PASS)
					{
						if (submitOrderRegistered() == LR_PASS) 
						{
							if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 5 ) 
				        		viewMyAccount();
						}
					}
				}
			}
	    }
	    
//	    if 	 (ONLINE_ORDER_SUBMIT==1){
//			if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 5 ) 
//        		viewMyAccount();
        	
//		}
/*        if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 20 ) {
        	viewOrderHistory();
        	
            if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ORDER_STATUS ) 
                viewOrderStatus();

//	        if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_RESERVATION_HISTORY ) 
//	           	viewReservationHistory();

//            if( viewHistory <= RATIO_POINTS_HISTORY ) {
//				viewPointsHistory();
//			}
        }
  */          	
	    if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_LOGOUT ) 
	    	logoff();
	    
	    if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_FORGOT_PASSWORD ) 
			forgetPassword();	
	    
	    if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_STORE_LOCATOR ) {
			StoreLocator();
		}
	    
	    if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 50 ) 
	   		newsLetterSignup();
	    
	}
//   	logoff();
	
	return 0;
}

checkoutBuildCartFirst()
{
	int viewHistory = atoi(lr_eval_string("{RANDOM_PERCENT}"));
	
    buildCartCheckout(0);//
//	lr_exit(LR_EXIT_MAIN_ITERATION_AND_CONTINUE, LR_PASS);

	inCartEdits(); 
	
	if (login() == LR_PASS) 
	{
		/*  //0916 - rhy
			if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_REDEEM_LOYALTY_POINTS ) 
				convertAndApplyPoints();
		*/	
		if (submitShippingBrowseFirst() == LR_PASS) 
		{
		     if (submitBillingRegistered() == LR_PASS ) {
				submitOrderRegistered();
			}
		}
/*		            
	    if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 20 ) {
	    	viewOrderHistory();
	    	
	        if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ORDER_STATUS ) 
	            viewOrderStatus();
	
	        if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_RESERVATION_HISTORY ) 
	           	viewReservationHistory();
	
//	        if( viewHistory <= RATIO_POINTS_HISTORY ) {
//				viewPointsHistory();
//			}
	    }
*/		
	    if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_LOGOUT ) 
	    	logoff();
	    
	    if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_FORGOT_PASSWORD ) 
			forgetPassword();	
	    
	}
    if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_STORE_LOCATOR ) {
		StoreLocator();
	}
    
    if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 50 ) 
   		newsLetterSignup();
    
	return 0;
}