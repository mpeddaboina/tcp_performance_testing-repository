#ifndef _GLOBALS_H
#define _GLOBALS_H

//--------------------------------------------------------------------
// Include Files
#include "lrun.h"
#include "web_api.h"
#include "lrw_custom_body.h"

//--------------------------------------------------------------------
// Global Variables
int LINK_TT, FORM_TT, TYPE_SPEED; //Think Time
	//set global think time values for link clicks, form entry pages and type speed for typeAhead search.
	LINK_TT = 10;
	FORM_TT = 10;
	TYPE_SPEED = 1;
int authcookielen , authtokenlen ;
authcookielen = 0;	
int orderItemIdscount = 0;
//	long lastResponse, request, response, total = 0;
#define OPTIONSENABLED 1

#endif // _GLOBALS_H
/*
int getPromoCode() { //action is either "get" or "delete"
    
    int row, rc, loopCtr = 0;
    unsigned short updateStatus;
    char FieldValue[50];
    char **colnames = NULL;
    char **rowdata = NULL;
    int rNum;
    int totalCoupon = lr_get_attrib_long("SinglePromoCodeCount");
	char  *VtsServer = "10.56.29.36";
	int   nPort = 8888;
	PVCI2 pvci = 0;
    
    pvci = vtc_connect(VtsServer,nPort,VTOPT_KEEP_ALIVE);
//   	lr_log_message("rc=%d\n loopCtr=%d\n totalCoupon=%d\n", pvci,loopCtr,totalCoupon);
        
//	while (!rc) {
	while (!pvci) {
   		
        rNum = rand() % totalCoupon + 1;
        loopCtr++;
        vtc_query_row(pvci, rNum, &colnames, &rowdata);
		//lr_output_message("Query Row Names : %s", colnames[0]);
		//lr_output_message("Query Row Data  : %s", rowdata[0]);            
        
        if ( strcmp(rowdata[0], "") != 0) { //if row is not blank, save it to a parameter then delete it from the table

        	lr_save_string(rowdata[0], "promocode"); //save the code
//        	lr_output_message("Promo Code  : %s", lr_eval_string("{promocode}") );
        	vtc_clear_row(pvci, rNum, &updateStatus); //delete the code from the datafile

            break;
        }
    }
	
    vtc_free_list(colnames);
    vtc_free_list(rowdata);
    rc = vtc_disconnect( pvci );

    if ( strcmp(lr_eval_string("{promocode}"), "") == 0) {
        lr_error_message("Single-Use promo code out of data.");
        return LR_FAIL;
    }

    return LR_PASS;
    
}
*/