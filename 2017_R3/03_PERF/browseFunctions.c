#include "..\..\browseWorkloadModel.c"

int index , length , i, randomPercent, isLoggedIn;
char *searchString ;
char *nav_by ; // T02 - Category Navigation selection
char *drill_by ; // T03 - Facet / SubCategory drill selection
char *sort_by; // T03 - Sort selection
char *product_by; // T04 - Product Display Method
//int form_TT, link_TT, typeSpeed_TT;
typedef long time_t;
time_t t;
//int authcookielen , authtokenlen ;
//authcookielen = 0;

void addHeader()
{
	web_add_header("storeId", lr_eval_string("{storeId}") );
	web_add_header("catalogId", lr_eval_string("{catalogId}") );
	web_add_header("langId", "-1");
	
}

void webURL(char *Url, char *pageName)
{
	lr_save_string(Url, "Url");
	lr_save_string(pageName, "pageName");
	web_url ( lr_eval_string("{pageName}") ,
    	     "URL={Url}" ,
             "Resource=0" ,
             "Mode=HTML" ,
             LAST ) ;
}//end webURL

void api_getESpot_second(){
	if (ESPOT_FLAG == 1) {
		if (isLoggedIn == 1) { //turned off for akamai caching
			addHeader();	
			web_add_header("espotName","GlobalHeaderBannerAboveHeader,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1");
			web_reg_find("TEXT/IC=espotName", "SaveCount=apiCheck", LAST);
			//lr_start_transaction("T99_API_Second_getESpot");
			web_custom_request("getESpots", 
				"URL=https://{api_host}/getESpot", 
				"Method=GET", 
				"Resource=0", 
				"RecContentType=application/json", 
				LAST);
			//if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			//	lr_end_sub_transaction("T99_API_Second_getESpot", LR_FAIL);
			//else
			//	lr_end_sub_transaction("T99_API_Second_getESpot", LR_AUTO);
		}
	}
}

void TCPGetWishListForUsersView()
{
	lr_start_transaction("T25_Common_S01_TCPGetWishListForUsersView");

	web_custom_request("TCPGetWishListForUsersView",
		"URL=http://{host}/webapp/wcs/stores/servlet/TCPGetWishListForUsersView?tcpCallBack=jQuery1113014590106982485151_1473881708524&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&sortBy=&_=1473881708525", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Mode=HTML", 
		"EncType=application/x-www-form-urlencoded", 
		LAST);

	lr_end_sub_transaction("T25_Common_S01_TCPGetWishListForUsersView", LR_AUTO);
	
}


int tcp_api(char *apiCall, char *method)
{	int rc;
	lr_param_sprintf ( "apiTransactionName" , "T30_API %s" , apiCall ) ;
	lr_param_sprintf ( "apiURL" , "https://%s/%s" , lr_eval_string("{api_host}"), apiCall ) ;
	lr_param_sprintf ( "apiMethod" , "%s" , method ) ;
	web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", LAST); //"errorCode": "",\n
	web_reg_save_param("errorMessage", "LB=\"errorMessage\": \"", "RB=\",\n", "NotFound=Warning", LAST); //"errorMessage": "",\n

	lr_start_transaction(lr_eval_string("{apiTransactionName}"));
	
	web_custom_request(lr_eval_string(apiCall),  
		"URL={apiURL}", 
		"Method={apiMethod}", 
		"RecContentType=text/html", 
		"Mode=HTML", 
		LAST);
	// rc = web_get_int_property(HTTP_INFO_RETURN_CODE);
	
//	if (rc == 200) {
		if ( strcmp( lr_eval_string("{errorCode}") ,"") != 0 && strcmp(lr_eval_string("{errorMessage}") ,"") != 0 ) {
			lr_fail_trans_with_error( lr_eval_string("{apiTransactionName} Failed with  Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_transaction( lr_eval_string("{apiTransactionName}"), LR_FAIL);	
		}
		else
			lr_end_transaction( lr_eval_string("{apiTransactionName}"), LR_PASS);	
	// }
	// else {
		// lr_fail_trans_with_error( lr_eval_string("{apiTransactionName} Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
		// lr_end_transaction( lr_eval_string("{apiTransactionName}"), LR_FAIL);	
	// }
	return 0;
}

void callAPI()
{	lr_continue_on_error(1);

	if (ESPOT_FLAG == 1) 
	{
		tcp_api2("getESpot", "GET", lr_eval_string("{mainTransaction}") );                             // turned off for akamai caching
	}
	
//	tcp_api2("tcporder/getXAppConfigValues", "GET", lr_eval_string("{mainTransaction}") );               // api_getCoupon();
//	tcp_api2("payment/getAvailableOffers", "GET", lr_eval_string("{mainTransaction}") );           // api_getAvailableOffers();
	if (strcmp(lr_eval_string("{callerId}"), "home") != 0) 
	{

		web_add_header("locStore", "False");
		web_add_header("pageName", "orderSummary");
		web_reg_save_param("cartCount", "LB=CartCount\": ", "RB=,", "NotFound=Warning", LAST);			//"CartCount": 1,\n
		tcp_api2("getOrderDetails", "GET", lr_eval_string("{mainTransaction}") );                      // api_getOrderDetails();
	}
	else{

		web_add_header("locStore", "False");
		web_add_header("pageName", "orderSummary");
		web_reg_save_param("cartCount", "LB=CartCount\": ", "RB=,", "NotFound=Warning", LAST);			//"CartCount": 1,\n
		tcp_api2("getOrderDetails", "GET", lr_eval_string("{mainTransaction}") );                      // api_getOrderDetails();
		lr_save_string("0,","cartCount");
		
	}


	tcp_api2("payment/getRegisteredUserDetailsInfo", "GET",  lr_eval_string("{mainTransaction}") ); // api_getRegisteredUserDetailsInfo();
	
	tcp_api2("payment/getPointsService", "GET",  lr_eval_string("{mainTransaction}") );             // api_getPointsService();
	
	if (isLoggedIn == 1)
	{
		tcp_api2("tcporder/getAllCoupons", "GET", lr_eval_string("{mainTransaction}") );               // api_getCoupon();
	}
	
//	tcp_api2("getOrderSummary", "GET",  lr_eval_string("{mainTransaction}") );             // api_getPointsService();
	api_getESpot_second();
	lr_continue_on_error(0);
}

void call_OPTIONS(char *apiCall)
{
	lr_param_sprintf ( "apiURL" , "https://%s/%s" , lr_eval_string("{api_host}"), apiCall ) ;
	web_add_header("Accept", "*/*");
	web_add_header("Access-Control-Request-Method", "GET");
	web_add_header("Origin", "https://{host}");
	
	if (strcmp(apiCall, "payment/getPointsService") == 0 ){
		
		web_add_header("Access-Control-Request-Headers", "catalogid,langid,storeid");

	}else if (strcmp(apiCall, "getOrderDetails") == 0 ){
		
		web_add_header("Access-Control-Request-Headers", "calc,catalogid,langid,locstore,pagename,storeid");
		
	}else if (strcmp(apiCall, "getESpot") == 0 ){
		
		web_add_header("Access-Control-Request-Headers", "catalogid,devicetype,espotname,langid,storeid");
			
	}else if (strcmp(apiCall, "tcporder/getAllCoupons") == 0 ){
		
		web_add_header("Access-Control-Request-Headers", "catalogid,langid,storeid");
		
	}else if (strcmp(apiCall, "payment/getRegisteredUserDetailsInfo") == 0 ){
		
		web_add_header("Access-Control-Request-Headers", "catalogid,langid,storeid");
		
	}else if (strcmp(apiCall, "payment/giftOptionsCmd") == 0 ){
		
		web_add_header("Access-Control-Request-Headers", "catalogid,langid,storeid");
		
	}else if (strcmp(apiCall, "payment/getCreditCardDetails") == 0 ){
		
		web_add_header("Access-Control-Request-Headers", "catalogid,isrest,langid,storeid");
		
	}else if (strcmp(apiCall, "payment/getAddressFromBook") == 0 ){
		
		web_add_header("Access-Control-Request-Headers", "catalogid,frompage,langid,storeid");
		
	}
/*	
	if (strcmp(apiCall, "payment/getPointsService") == 0 && strcmp(lr_eval_string("{payment/getPointsService}"), "0") == 0){
		
		web_add_header("Access-Control-Request-Headers", "catalogid,langid,storeid");
		executeOptions();
		lr_save_string("1", "payment/getPointsService");

	}else if (strcmp(apiCall, "getOrderDetails") == 0 ){
		
		web_add_header("Access-Control-Request-Headers", "calc,catalogid,langid,locstore,pagename,storeid");
		executeOptions();
		lr_save_string("1", "getOrderDetails");
		
	}else if (strcmp(apiCall, "getESpot") == 0 ){
		
		web_add_header("Access-Control-Request-Headers", "catalogid,devicetype,espotname,langid,storeid");
		executeOptions();
		lr_save_string("1", "getESpot");
			
	}else if (strcmp(apiCall, "tcporder/getAllCoupons") == 0 ){
		
		web_add_header("Access-Control-Request-Headers", "catalogid,langid,storeid");
		executeOptions();
		lr_save_string("1", "tcporder/getAllCoupons");
		
	}else if (strcmp(apiCall, "payment/getRegisteredUserDetailsInfo") == 0 ){
		
		web_add_header("Access-Control-Request-Headers", "catalogid,langid,storeid");
		executeOptions();
		lr_save_string("1", "payment/getRegisteredUserDetailsInfo");
		
	}else if (strcmp(apiCall, "payment/giftOptionsCmd") == 0 ){
		
		web_add_header("Access-Control-Request-Headers", "catalogid,langid,storeid");
		executeOptions();
		lr_save_string("1", "payment/giftOptionsCmd");
		
	}else if (strcmp(apiCall, "payment/getCreditCardDetails") == 0 ){
		
		web_add_header("Access-Control-Request-Headers", "catalogid,isrest,langid,storeid");
		executeOptions();
		lr_save_string("1", "payment/getCreditCardDetails");
		
	}else if (strcmp(apiCall, "payment/getAddressFromBook") == 0 ){
		
		web_add_header("Access-Control-Request-Headers", "catalogid,frompage,langid,storeid");
		executeOptions();
		lr_save_string("1", "payment/getAddressFromBook");
		
	}
*/	
}

void executeOptions()
{
		web_custom_request("OPTIONS", 
			"URL={apiURL}", 
			"Method=OPTIONS", 
			"Resource=0", 
			LAST);
}

int tcp_api2(char *apiCall, char *method, char *mainTransaction)
{	int rc;
	
	if (strcmp(apiCall, "getESpot") != 0 )
	{
		web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", LAST); //"errorCode": "",\n
		web_reg_save_param("errorMessage", "LB=\"errorMessage\": \"", "RB=\",\n", "NotFound=Warning", LAST); //"errorMessage": "",\n
	}
	
	if (strcmp(apiCall, "payment/getRegisteredUserDetailsInfo") == 0 )
	{
		web_reg_save_param ( "addressId" , "LB=\"addressId\": \"" , "RB=\",\n" , "ORD=1", "NotFound=Warning", LAST ) ; //"addressId": "1398789",\n
		web_reg_save_param ( "addressIdAll" , "LB=\"addressId\": \"" , "RB=\",\n" , "ORD=ALL", "NotFound=Warning", LAST ) ; //"addressId": "1398789",\n
		lr_save_string("resourceName","CheckString");
	}	
	if (strcmp(apiCall, "payment/getPointsService") == 0 ){
		lr_save_string("LoyaltyWebsiteInd","CheckString");
	}else if (strcmp(apiCall, "getOrderDetails") == 0 ){
		lr_save_string("{","CheckString");
	}else if (strcmp(apiCall, "getESpot") == 0 ){
		lr_save_string("espotName","CheckString");
		//Pavan Dusi added the following line 0626
		                          //GlobalHeaderBannerAboveHeader,GlobalHeaderBannerWithinHeader,GlobalHeaderLogo,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,create_account_drawer,MinicartLoginBanner,MinicartRegisterBanner,drawer_myaccount_overview_banner,coupon_help,bag_empty_banner,drawer_empty_bag,MinicartLoginFromFavoritesBanner,bag_ledger_promo_banner,gift_services_espot,CheckoutConfirmationBanner,CheckoutConfirmationMPRPromo
		web_add_header("espotName","GlobalHeaderBannerAboveHeader,GlobalHeaderBannerWithinHeader,GlobalHeaderLogo,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,create_account_drawer,MinicartLoginBanner,MinicartRegisterBanner,drawer_myaccount_overview_banner,coupon_help,bag_empty_banner,drawer_empty_bag,MinicartLoginFromFavoritesBanner,bag_ledger_promo_banner,gift_services_espot,CheckoutConfirmationBanner,CheckoutConfirmationMPRPromo,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1");
		//web_add_header("espotName","GlobalHeaderBannerAboveHeader,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1");
		web_add_header("deviceType","desktop");

	}else if (strcmp(apiCall, "tcporder/getAllCoupons") == 0 ){
		lr_save_string("availableCoupons","CheckString");
	}else if (strcmp(apiCall, "tcporder/getXAppConfigValues") == 0 ){
		lr_save_string("xAppAttrValues","CheckString");
	}
	
	if (API_SUB_TRANSACTION_SWITCH == 1) //1 = ON == Let the sub transactions show on the LRA Summary
	{
		lr_param_sprintf ( "apiMainTransactionName" , "%s" , mainTransaction ) ;
		lr_param_sprintf ( "apiTransactionName" , "%s_%s" , lr_eval_string("{apiMainTransactionName}"), apiCall ) ;
		
		if (strcmp(lr_eval_string("{apiMainTransactionName}"),"") !=0 )
			lr_start_sub_transaction(lr_eval_string("{apiTransactionName}"), lr_eval_string("{apiMainTransactionName}"));
		else
			lr_start_transaction(lr_eval_string("T30_API {apiTransactionName}"));
		
		#if OPTIONSENABLED
			if (isLoggedIn == 0)
			{
				call_OPTIONS(apiCall);
			}
			else {
				if (BROWSE_OPTIONS_FLAG == 0)
					call_OPTIONS(apiCall);
				
			}
		#endif

		lr_param_sprintf ( "apiURL" , "https://%s/%s" , lr_eval_string("{api_host}"), apiCall ) ;
		lr_param_sprintf ( "apiMethod" , "%s" , method ) ;
		
		web_add_header("storeId", lr_eval_string("{storeId}") );
		web_add_header("catalogId", lr_eval_string("{catalogId}") );
		web_add_header("langId", "-1");
		web_reg_find("TEXT/IC={CheckString}","Fail=NotFound", "SaveCount=Mycount",LAST);

		web_custom_request(lr_eval_string(apiCall),  
			"URL={apiURL}", 
			"Method={apiMethod}", 
			"RecContentType=text/html", 
			"Mode=HTML", 
			LAST);
			
		if ( strcmp( lr_eval_string("{errorCode}") ,"") != 0 && strcmp(lr_eval_string("{errorMessage}") ,"") != 0  && (atoi(lr_eval_string("{Mycount}"))==0) ) 
		{
			lr_fail_trans_with_error( lr_eval_string("{apiTransactionName} Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			if (strcmp(lr_eval_string("{apiMainTransactionName}"),"") !=0 )
				lr_end_sub_transaction( lr_eval_string("{apiTransactionName}"), LR_FAIL);	
			else
				lr_end_transaction( lr_eval_string("T30_API {apiTransactionName}"), LR_FAIL);	
		}
		else 
		{
			if (atoi(lr_eval_string("{Mycount}")) > 0 )  {
				if (strcmp(lr_eval_string("{apiMainTransactionName}"),"") !=0 )
					lr_end_sub_transaction( lr_eval_string("{apiTransactionName}"), LR_AUTO);	
				else
					lr_end_transaction( lr_eval_string("T30_API {apiTransactionName}"), LR_AUTO);	
			}
			else{
				if (strcmp(lr_eval_string("{apiMainTransactionName}"),"") !=0 )
					lr_end_sub_transaction( lr_eval_string("{apiTransactionName}"), LR_FAIL);	
				else
					lr_end_transaction( lr_eval_string("T30_API {apiTransactionName}"), LR_FAIL);	
			}
		}
		
	}
	else
	{
		lr_start_transaction(lr_eval_string("{apiTransactionName}"));
		
		web_custom_request(lr_eval_string(apiCall),  
			"URL={apiURL}", 
			"Method={apiMethod}", 
			"RecContentType=text/html", 
			"Mode=HTML", 
			LAST);
		rc = web_get_int_property(HTTP_INFO_RETURN_CODE);
		
		if (rc == 200) {
			if ( strcmp( lr_eval_string("{errorCode}") ,"") != 0 && strcmp(lr_eval_string("{errorMessage}") ,"") != 0 ) {
				lr_fail_trans_with_error( lr_eval_string("{apiTransactionName} Failed with  Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
				lr_end_transaction( lr_eval_string("T30_API {apiTransactionName}"), LR_FAIL);	
			}
			else {
				lr_end_transaction( lr_eval_string("T30_API {apiTransactionName}"), LR_PASS);	
			}
		}
		else {
			lr_fail_trans_with_error( lr_eval_string("T30_API {apiTransactionName} Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_transaction( lr_eval_string("T30_API {apiTransactionName}"), LR_FAIL);	
		}
	}		
		
	return 0;
}

webURL2(char *Url, char *pageName, char *textCheck)
{
	lr_save_string(Url, "Url");
	lr_save_string(pageName, "pageName");
	lr_save_string(textCheck, "textCheck");
	web_reg_find("SaveCount=txtCheckCount", "Text/IC={textCheck}", LAST);
	
	web_reg_find("SaveCount=noProducts", "Text/IC=We're sorry, there are no products available for purchase at this time. Please check back later.", LAST);
	
	web_url ( lr_eval_string("{pageName}") ,
    	     "URL={Url}" ,
             "Resource=0" ,
             "Mode=HTML" ,
             LAST ) ;

	if ( atoi(lr_eval_string("{txtCheckCount}")) > 0 )
		return LR_PASS;
	else
		return LR_FAIL;
			 
}//end webURL


void endIteration()
{
	//lr_message ( "in endIteration" ) ;
}

void homePageCorrelations()
{
/*	web_reg_save_param_regexp (	"ParamName=categories",
				"RegExp=<li class=\"navigation-level-one\" data-reactid=\"[0-9]*\"><a href=\"http://{host}{store_home_page}c/([^/]*?)\"",
				"Ordinal=All",
				SEARCH_FILTERS,
				"RequestUrl=*",	
				LAST );
	

	web_reg_save_param ( "categories" ,
				 "LB=\"url\": \"http://{host}{store_home_page}c/" , 
				 "RB=/" ,
				 "ORD=ALL" , "Convert=HTML_TO_TEXT" , "NotFound=Warning", LAST ) ;

	web_reg_save_param_regexp (
		"ParamName=categories",
		"RegExp=<li class=\"navigation-level-one\" data-reactid=\"[0-9]*\"><a href=\"(.*?)\"",
		"Ordinal=All",
	SEARCH_FILTERS,
		"RequestUrl=*",
	LAST );

	web_reg_save_param ( "clearances" ,
			 "LB=\"url\": \"http://{host}/shop/SearchDisplay?" , //works in prod and perf
			 "RB=\"" ,
			 "ORD=ALL" , "Convert=HTML_TO_TEXT" , "NotFound=Warning", LAST ) ;
*/				 
		
}

void homePageCorrelations_OLD() //correlations needed for category navigations
{
/*   	web_reg_save_param ( "catalogId" ,
	                     "LB=catalogId=" ,
	                     "RB=&",
						 "ORD=1",
	                     "NotFound=Warning", LAST ) ;
*/						 

/*
	web_reg_save_param_regexp (	"ParamName=categories",
						"RegExp=<li class=\"navigation-level-one\" data-reactid=\"[0-9]*\"><a href=\"(.*?)\"",
						"Ordinal=All",
						SEARCH_FILTERS,
						"RequestUrl=*",	LAST );
*/						
	
	web_reg_save_param ( "categories" ,
				 "LB=\t\t\t\t<a href=\"http://{host}{store_home_page}c/" , 
				 "RB=\"" ,
				 "ORD=ALL" , "Convert=HTML_TO_TEXT" , "NotFound=Warning", LAST ) ;
							
/*						
	web_reg_save_param ( "storeId" ,
	                     "LB=<meta name=\"CommerceSearch\" content=\"storeId_=" ,
	                     "RB=\"",
						 "ORD=1",
	                     "NotFound=Warning", LAST ) ;
*/
	/*web_reg_save_param ( "categories" ,
				 "LB=\t\t\t\t<a href=\"http://{host}{store_home_page}c/" , 
				 "RB=\"" ,
				 "ORD=ALL" , "Convert=HTML_TO_TEXT" , "NotFound=Warning", LAST ) ;
		*/		 
		
/*		web_reg_save_param_json(
        "ParamName=categories",
        "QueryString=$.headerInfo[:1].*.categoryAttributes.url",
        "SelectAll=Yes",
        "SEARCH_FILTERS",
        "Scope=Body",
        "LAST"); 
*/
	
/******* This is commented out by PD on 03/07/2017 due to change in workflow. Henceforth from R2, clearances will be categories_6
				 web_reg_save_param ( "clearances" ,
						 "LB=<a href=\"http://{host}/shop/SearchDisplay?" , //works in prod and perf
						 "RB=\"" ,
						 "ORD=ALL" , "Convert=HTML_TO_TEXT" , "NotFound=Warning", LAST ) ;
*******/	
	// include correlation for Place Shop (in PERF environment)
	
/**********************OBSOLETE: 03/07/2017 The following three web_reg_save_params are being commented**************************************/	
/*	
	web_reg_save_param ( "searchAutoSuggestURL" ,
						 "LB=setAutoSuggestURL('",
						 "RB='" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=Warning", LAST ) ;

	web_reg_save_param ( "searchCachedSuggestionsURL" ,
						 "LB=setCachedSuggestionsURL('" ,
						 "RB='" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=Warning", LAST ) ;

//						 <a href="http://tcp-perf.childrensplace.com/shop/us/content/mvp-shop-spring-2014" 
   	web_reg_save_param ( "placeShopCategory" ,
						 "LB={store_home_page}content/" ,
	                     "RB=\"",
						 "ORD=All",
	                     "NotFound=Warning", LAST ) ;
*/
}//end homePageCorrelations()


void categoryPageCorrelations() //correlations needed for facet and subcategory navigations
{

	web_reg_save_param_regexp( "ParamName=subcategories" ,
								"RegExp=class=\"leftNavLevel0Title block\">[^<]*?<a id='[^']*?' title='[^']*?'[^h]*?href='(.*?)'" ,
							   //"RegExp=class=\"leftNavLevel0Title block\">[^<]*?<a id=\"[^\"]*?\" title=\"[^\"]*?\"[^h]*?href=\"(.*?)\"" ,
	                           //"RegExp=class=\"leftNavLevel0Title block\">[^<]*?<a id=\"[^'\"]*?['\"] title=['\"][^']*?['"][^h]*?href=['\"](.*?)['\"]" ,
			                   "Ordinal=ALL" ,
			                   "Notfound=warning" ,				   
			                   SEARCH_FILTERS ,
							   "IgnoreRedirections=Yes",
			                   //"Group=1" ,
							   "RequestUrl=*{host}*",
							  LAST ) ;
						 
	web_reg_save_param ( "facetsURL" ,
						 "LB=<li onclick=\"javascript: refreshAfterFilterGotSelected('http://{host}" ,
	                   	 "RB='" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 LAST ) ;

	web_reg_save_param_regexp( "ParamName=facetsID" ,
	                           "RegExp=facetCheckBox\" id =\"(.*?)\" value=\"(.*?)\"" ,
			                   "Ordinal=ALL" ,
			                   "Notfound=warning" ,
			                   SEARCH_FILTERS ,
			                   "Group=2" ,
			                   LAST ) ;

	web_reg_save_param ( "sortURL" ,
				  		 "LB=<option value=\"http://{host}" ,
	                   	 "RB=\"" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 LAST ) ;

	web_reg_save_param ( "searchSubCategories" ,
				  		 "LB=href='http://{host}" ,
	                   	 "RB='>" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 LAST ) ;

	web_reg_save_param ( "paginationNextURL" ,
				  		 "LB=goToResultPage('http://{host}" ,
	                   	 "RB='" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 LAST ) ;

	web_reg_save_param ( "paginationPageIndex" ,
				  		 "LB=catalogSearchResultDisplay_Context','" ,
	                   	 "RB='" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 LAST ) ;

	web_reg_save_param ( "paginationShowAllURL" ,
				  		 "LB=viewall-right-bottom\">\r\n\t\t\t\t\t                <a href=\"http://{host}" ,
	                   	 "RB=\"" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 LAST ) ;
						 
}//end categoryPageCorrelations()


void subCategoryPageCorrelations() //correlations needed for Product Display & Quick View navigations
{

	if (strcmp( lr_eval_string("{host}"), "tcp-perf.childrensplace.com" ) == 0)
	{	web_reg_save_param ( "pdpURL" ,
			 "LB=productRow name\">\r\n\t\t\t\r\n\t\t\t\t<a href=\"http://{host}" , //tcp-perf
//           "LB=productRow name\">\r\n\t\r\n\t\t\t\r\n\t\t\t\t\t\r\n\t\t\t\t<a href=\"http://{host}" , //uatlive3
			 "RB=\" id" ,
			 "ORD=ALL" ,
			 "Convert=HTML_TO_TEXT" ,
			 "NotFound=warning" ,
			 LAST ) ;
	}
	else {
		web_reg_save_param ( "pdpURL" ,
//           "LB=productRow name\">\r\n\t\t\t\r\n\t\t\t\t<a href=\"http://{host}" , //tcp-perf
			 "LB=productRow name\">\r\n\t\r\n\t\t\t\r\n\t\t\t\t\t\r\n\t\t\t\t<a href=\"http://{host}" , //uatlive3
			 "RB=\" id" ,
			 "ORD=ALL" ,
			 "Convert=HTML_TO_TEXT" ,
			 "NotFound=warning" ,
			 LAST ) ;
	}
	
	web_reg_save_param ( "quickviewURL" ,
	                     "LB=openModal2\" link=\"http://{host}" ,
						 "RB=\" href=" ,
						 "ORD=ALL" ,
						 "Convert=HTML_TO_TEXT" ,
						 "NotFound=warning" ,
						 LAST ) ;
						 
	web_reg_find("SaveCount=noProducts", "Text/IC=We're sorry, there are no products available for purchase at this time. Please check back later.", LAST);

	
}//end subCategoryPageCorrelations()


void productDisplayPageCorrelations()
{

	web_reg_save_param ( "skuSelectionSwitchProductID" ,
	                     "LB=confirmSwitchProduct(event,'product_" ,
	                     "RB='" ,
	                     "notFound=Warning",
	                     "ORD=ALL" ,
	                     LAST ) ;

	web_reg_save_param ( "skuSelectionProductID" ,
	                     "LB=input type=\"hidden\" id=\"prodId\" value=\"" ,
	                     "RB=\"" ,
	                     "notFound=Warning",
	                     LAST ) ;

	//BOPIS parameters 03/22/2017
	web_reg_save_param("productId", "LB=\"productId\" : \"", "RB=\",", "ORD=ALL","NotFound=Warning", LAST); //"productId" : "120557",\r\n
	web_reg_save_param("TCPProductInd", "LB=\"TCPProductInd\" : \"", "RB=\"", "ORD=ALL", "NotFound=Warning", LAST); //"TCPProductInd" : "Clearance"
	web_reg_save_param("bopisCatEntryId", "LB=\"catentry_id\" : \"", "RB=\"", "ORD=ALL", "NotFound=Warning", LAST); //"catentry_id" : "771570"
	web_reg_save_param("bopisQty", "LB=\"qty\" : \"", "RB=\"", "ORD=ALL", "NotFound=Warning", LAST); //"qty" : "8"
	
} //end productDisplayPageCorrelations()


//void Correlations()
void addToCartCorrelations()
{
	
	web_reg_save_param_regexp( "ParamName=atc_catentryIds" ,
	                           "RegExp=\"catentry_id\" : \"(.*?)\",\r\n\t\t\t\t\t\t\t\t\t\t\t\"item_sku\" : \"(.*?)\",\r\n\t\t\t\t\t\t\t\t\t\t\t\"qty\" : \"([1-9][0-9]*)\"" ,
			                   "Ordinal=ALL" ,
			                   "Notfound=warning" ,
			                   SEARCH_FILTERS ,
			                   "Group=1" ,
			                   LAST ) ;

 	web_reg_save_param ( "atc_comment" ,
                    	 "LB=<input type=\"hidden\" name=\"comment\" value=\"" ,
                    	 "RB=\"" ,
                    	 "Notfound=warning" ,
                    	 LAST ) ;

} // end addToCartCorrelations()
/*
void api_getCountryListAndHeaderInfo(){
	
	lr_start_transaction("T30_API getCountryListAndHeaderInfo");
		web_custom_request("getCountryListAndHeaderInfo", 
			"URL=https://{api_host}/getCountryListAndHeaderInfo", 
			"Method=GET", 
			"Resource=0", 
			"RecContentType=application/json", 
			LAST);
	lr_end_transaction("T30_API getCountryListAndHeaderInfo", LR_AUTO);
	
}

void api_getAvailableOffers(){
	
	lr_start_transaction("T30_API getAvailableOffers");
			web_custom_request("getAvailableOffers", 
						"URL=https://{api_host}/payment/getAvailableOffers", 
						"Method=GET", 
						"Resource=0", 
						"RecContentType=application/json", 
						LAST);
	lr_end_transaction("T30_API getAvailableOffers", LR_AUTO);
}

void api_getESpot(){
	
	
	lr_start_transaction("T30_API getESpot");
					web_custom_request("getESpot", 
						"URL=https://{api_host}/getESpot", 
						"Method=GET", 
						"Resource=0", 
						"RecContentType=application/json", 
						LAST);
	lr_end_transaction("T30_API getESpot", LR_AUTO);

}


void api_getOrderDetails(){
	
	lr_start_transaction("T30_API getOrderDetails");
					web_custom_request("getOrderDetails", 
						"URL=https://{api_host}/getOrderDetails", 
						"Method=GET", 
						"Resource=0", 
						"RecContentType=application/json", 
						LAST);
	lr_end_transaction("T30_API getOrderDetails", LR_AUTO);

}

void api_getRegisteredUserDetailsInfo(){
	
	lr_start_transaction("T30_API getRegisteredUserDetailsInfo");
					web_custom_request("getRegisteredUserDetailsInfo", 
						"URL=https://{api_host}/payment/getRegisteredUserDetailsInfo", 
						"Method=GET", 
						"Resource=0", 
						"RecContentType=application/json", 
						LAST);
	lr_end_transaction("T30_API getRegisteredUserDetailsInfo", LR_AUTO);
	
}

void api_getAllCoupons(){
	
	web_add_header("storeId","{storeId}");
	web_add_header("catalogId","{catalogId}");
	web_add_header("langId","-1");
	lr_start_transaction("T30_API getAllCoupons");
					web_custom_request("getAllCoupons", 
						"URL=https://{api_host}/tcporder/getAllCoupons", 
						"Method=GET", 
						"Resource=0", 
						"RecContentType=application/json", 
						LAST);
	lr_end_transaction("T30_API getAllCoupons", LR_AUTO);
}

void api_getXAppConfigValues(){
	
	lr_start_transaction("T30_API getXAppConfigValues");
					web_custom_request("getXAppConfigValues", 
						"URL=https://{api_host}/tcporder/getXAppConfigValues", 
						"Method=GET", 
						"Resource=0", 
						"RecContentType=application/json", 
						LAST);
	lr_end_transaction("T30_API getXAppConfigValues", LR_AUTO);
}

void api_getCoupon(){
	
	lr_start_transaction("T30_API getCoupon");
					web_custom_request("getCoupon", 
						"URL=https://{api_host}/payment/getCoupon", 
						"Method=GET", 
						"Resource=0", 
						"RecContentType=application/json", 
						LAST);
	lr_end_transaction("T30_API getCoupon", LR_AUTO);
}

void api_getPointsService(){
	
	web_add_header("storeId","{storeId}");
	web_add_header("catalogId","{catalogId}");
	web_add_header("langId","-1");
	
	lr_start_transaction("T30_API getPointsService");
					web_custom_request("getPointsService", 
						"URL=https://{api_host}/payment/getPointsService", 
						"Method=GET", 
						"Resource=0", 
						"RecContentType=application/json", 
						LAST);
	lr_end_transaction("T30_API getPointsService", LR_AUTO);
}
*/

void viewHomePage()
{
	lr_save_string("home", "callerId");
	lr_continue_on_error(1);
	homePageCorrelations();

	web_global_verification("Text/IC=h1 role=\"main\">The store has encountered a problem processing the last request",  "Fail=Found","ID=FindStoreProblem", LAST ); 
	
	lr_param_sprintf ( "homePageUrl" , "http://%s%shome" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}") ) ;
	lr_save_string("T01_Home Page", "mainTransaction");
	lr_start_transaction( "T01_Home Page" ) ;
	
	if ( webURL2(lr_eval_string ( "{homePageUrl}" ), "T01_Home Page" , "BEGIN TCPTopCategoriesDisplay.jsp" ) == LR_PASS)
	{
		callAPI();
		lr_end_transaction( "T01_Home Page" , LR_PASS ) ;
	}	
	else{
		callAPI();
		lr_fail_trans_with_error( lr_eval_string("T01_Home Page Text Check Failed - {homePageUrl}") ) ;
		lr_end_transaction( "T01_Home Page" , LR_FAIL ) ;
	}
	
	if (isLoggedIn == 1) //IMP CHANGE BY PAVAN DUSI 06-27
	{
		//tcp_api2("tcporder/getAllCoupons", "GET", lr_eval_string("{mainTransaction}") );               // api_getCoupon();
		//api_getESpot_second();

	}

	BROWSE_OPTIONS_FLAG = 1;
	
	lr_save_string("Other", "callerId");
	
	lr_continue_on_error(0);
	///lr_exit(LR_EXIT_MAIN_ITERATION_AND_CONTINUE, LR_AUTO);
 // end viewHomePage
}

int getTopCategories()
{
	web_reg_save_param("aaaaaa", "LB=url\": \"", "RB=\"", "ORD=All", "NotFound=Warning", LAST);
	
//	lr_start_transaction("T0?-getTopCategories");
	
	web_submit_data("TCPLocaleSelection",
	    "Action=http://{host}/shop/{store}/getTopCategories",
		"Method=POST", 
		"Resource=0", 
		"Mode=HTML", 
		ITEMDATA,
            "Name=storeId", "Value={storeId}", ENDITEM,
            "Name=catalogId", "Value={catalogId}", ENDITEM,
		LAST);
		
//	if( strcmp(lr_eval_string("{redirectToStore}"), "us") == 0 || strcmp(lr_eval_string("{redirectToStore}"), "ca") == 0)
//		return lr_end_transaction("T0?-getTopCategories", LR_PASS);
//	else
//		return lr_end_transaction("T0?-getTopCategories", LR_FAIL);	
	return 0;
}


void navByBrowse()
{
	lr_think_time ( LINK_TT ) ;

//	lr_save_string( lr_paramarr_random ( "categories" ), "category");

	if ( strcmp(lr_eval_string("{storeId}"), "10152") == 0 )
	{
		index = rand () % 7 + 1 ;
		switch(index)
		{
			case 1: lr_save_string( "girls-clothing-ca", "category"); break;
			case 2: lr_save_string( "toddler-girl-clothes-ca", "category"); break;
			case 3: lr_save_string( "boys-clothing-ca", "category"); break;
			case 4: lr_save_string( "toddler-boy-clothes-ca", "category"); break;
			case 5: lr_save_string( "baby-clothes-ca", "category"); break;
//			case 6: lr_save_string( "kids-clearance-clothing", "category"); break;
			case 6: lr_save_string( "childrens-shoes-kids-shoes-canada", "category"); break;
			case 7: lr_save_string( "kids-accessories-canada", "category"); break;
			default: break;
		}
	}
	else 
	{
		if ( strcmp(lr_eval_string("{host}"), "uatlive3.childrensplace.com") == 0) 
		{
			index = rand () % 5 + 1 ;
			switch(index)
			{
				case 1: lr_save_string( "girls-clothing", "category"); break;
				case 2: lr_save_string( "toddler-girl-clothes", "category"); break;
				case 3: lr_save_string( "boys-clothing", "category"); break;
				case 4: lr_save_string( "toddler-boy-clothes", "category"); break;
				case 5: lr_save_string( "baby-clothes", "category"); break;
	//			case 6: lr_save_string( "kids-clearance-clothing", "category"); break;
	//			case 6: lr_save_string( "childrens-shoes-kids-shoes", "category"); break;
	//			case 7: lr_save_string( "kids-accessories-us", "category"); break;
				default: break;
			}
		}
		else 
		{
			index = rand () % 7 + 1 ;
			switch(index)
			{
				case 1: lr_save_string( "girls-clothing", "category"); break;
				case 2: lr_save_string( "toddler-girl-clothes", "category"); break;
				case 3: lr_save_string( "boys-clothing", "category"); break;
				case 4: lr_save_string( "toddler-boy-clothes", "category"); break;
				case 5: lr_save_string( "baby-clothes", "category"); break;
	//			case 6: lr_save_string( "kids-clearance-clothing", "category"); break;
				case 6: lr_save_string( "childrens-shoes-kids-shoes", "category"); break;
				case 7: lr_save_string( "kids-accessories-us", "category"); break;
				default: break;
			}
		}
	}	
	
	lr_param_sprintf ( "cateogryPageUrl" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/") , lr_eval_string( "{category}" ) ) ; 

	web_convert_param("cateogryPageUrl", "SourceEncoding=HTML","TargetEncoding=PLAIN", LAST );
	
	categoryPageCorrelations();
	subCategoryPageCorrelations(); /// rhy added 11/23/2015, 11/27 without this pdpURL will not have any value

	lr_continue_on_error(1);

	lr_save_string("T02_Category Display", "mainTransaction");
	lr_start_transaction( "T02_Category Display" ) ;
	
//		webURL(lr_eval_string ( "{cateogryPageUrl}" ), "T02_Category Display" );
	if (webURL2(lr_eval_string ( "{cateogryPageUrl}" ), "T02_Category Display", "End - SEO H1/Body Contents for Category page" ) == LR_PASS ) 
	{
		callAPI();
		if (isLoggedIn == 1) //IMP CHANGE BY PAVAN DUSI 06-27
		{
			//tcp_api2("tcporder/getAllCoupons", "GET", lr_eval_string("{mainTransaction}") );               // api_getCoupon();
			//api_getESpot_second();
			//api_getESpot_second();

		}


//		if ( isLoggedIn == 1 ) {
			//TCPGetWishListForUsersView(); //1204 9pm to bring down this number
//		}
		
		lr_end_transaction ( "T02_Category Display" , LR_PASS ) ;
	}
	else if ((lr_paramarr_len("subcategories")) == 0)
	{
			lr_fail_trans_with_error( lr_eval_string("T02_Category Display Failed subcategories==0 for- {cateogryPageUrl}") ) ;
			lr_end_transaction ( "T02_Category Display" , LR_FAIL ) ; // confirm.
	} // end if
	else {
		lr_fail_trans_with_error( lr_eval_string("T02_Category Display Text Check Failed for - {cateogryPageUrl}") ) ;
		lr_end_transaction ( "T02_Category Display" , LR_FAIL ) ; // confirm.
	}
	
	lr_continue_on_error(0);
	
} // end navByBrowse


void navByClearance()
{
 	lr_think_time ( LINK_TT ) ;
	//index = rand ( ) % lr_paramarr_len( "clearances" ) + 1 ;
//	findNewArrivals(); //NOT NEEDED ANYMORE, noProducts handling already in place
//	lr_param_sprintf ( "clearancePageUrl" , "http://%s/shop/SearchDisplay?%s" , lr_eval_string("{host}"),  lr_paramarr_idx ( "clearances" , index ) ) ;
//	lr_param_sprintf ( "clearancePageUrl" , "http://%s/shop/SearchDisplay?%s" , lr_eval_string("{host}"),  lr_paramarr_random( "clearances" ) ) ; 
	//lr_param_sprintf ( "clearancePageUrl" , "%s", "http://tcp-perf.childrensplace.com/shop/SearchDisplay?storeId=10151&catalogId=10551&langId=-1&beginIndex=0&searchSource=Q&sType=SimpleSearch&showResultsPage=true&pageView=image&facet=ads_f10001_ntk_cs:%22New Arrivals%22&categoryId=47502" );

	lr_param_sprintf ( "clearancePageUrl" , "%s" ,  lr_eval_string("{categories_3}") ) ; //Please continue to monitor this peice. on 03/07/2017 this was categories_3

	categoryPageCorrelations();

	subCategoryPageCorrelations(); /// rhy added 11/23/2015, 11/27 without this pdpURL will not have any vale
	web_reg_find("SaveCount=noProducts", "Text/IC=We're sorry, there are no products available for purchase at this time. Please check back later.", LAST);
	web_reg_find("Text=FILTER BY", "SaveCount=FilterBy_Count", LAST );
	
	lr_save_string("T02_Clearance Display", "mainTransaction");
	lr_start_transaction ( "T02_Clearance Display" ) ;
	
	//webURL(lr_eval_string ( "{clearancePageUrl}" ), "T02_Clearance Display" );
	if (webURL2(lr_eval_string ( "{clearancePageUrl}" ), "T02_Clearance Display", "<!-- BEGIN TCPSearchSetup.jspf-->" ) == LR_PASS ) 
	{
		callAPI();
		if (isLoggedIn == 1) //IMP CHANGE BY PAVAN DUSI 06-27
		{
			//tcp_api2("tcporder/getAllCoupons", "GET", lr_eval_string("{mainTransaction}") );               // api_getCoupon();
			//api_getESpot_second();
		}

			
		if ( atoi(lr_eval_string("{noProducts}")) == 1 ) 
		{
			lr_param_sprintf ( "failedPageUrl" , "T02_Clearance Display Failure : %s", lr_eval_string("{clearancePageUrl}") ) ;
			lr_fail_trans_with_error( lr_eval_string("T02_Clearance Display Text Check Failed for - {failedPageUrl}") ) ;
			lr_end_transaction ( "T02_Clearance Display" , LR_FAIL ) ;

		}
		else
			lr_end_transaction ( "T02_Clearance Display" , LR_PASS ) ;

	} 
	else
	{
		callAPI();
		if (isLoggedIn == 1) //IMP CHANGE BY PAVAN DUSI 06-27
		{
			//tcp_api2("tcporder/getAllCoupons", "GET", lr_eval_string("{mainTransaction}") );               // api_getCoupon();
			//api_getESpot_second();
		}

		lr_end_transaction ( "T02_Clearance Display" , LR_FAIL ) ;
	}
} // end navByClearance


findNewArrivals() // used in navByClearance()
{
	int offset = 1;

    char * position;

    char * str = "";

    char * search_str = "Arrivals"; //try not to pick New Arrival Clearance Products

    while( offset >= 1 ) {

		str = lr_paramarr_random( "clearances" );
		//lr_message("str is %s:", str); 
	    position = (char *)strstr(str, search_str);

	    offset = (int)(position - str + 1);
    }

    lr_save_string(str, "clearance");

	return 0;
}

void typeAheadSearch()
{
	lr_continue_on_error(1);

	//lr_message ( "In typeAheadSearch" ) ;
    for (i = 0; i < length; i++)
    {
       	//lr_message("source: %c", searchString[i]);
       	if (i == 0)
           	lr_param_sprintf ("SEARCH_STRING_PARAM", "%c", searchString[i]);
       	else
           	lr_param_sprintf ("SEARCH_STRING_PARAM", "%s%c", lr_eval_string("{SEARCH_STRING_PARAM}"), searchString[i]);
           	//lr_message("Partial SEARCH_STRING_PARAM: %s", lr_eval_string("{SEARCH_STRING_PARAM}"));

		if (i == 1) lr_save_string(lr_eval_string("{SEARCH_STRING_PARAM}"),"forDidYouMean");
			
//     	type ahead search call after every 3rd characters
		if (i == 2 || i == 5 || i == 8 || i == 11 || i == 14 || i == 17 || i == 20 )
       	{
    		lr_think_time ( TYPE_SPEED );
			//$.category..urlName
			//web_reg_save_param ( "autoSelectOption" , "LB=title=\"" ,"RB=\" id='autoSelectOption_" , "Notfound=warning" , "ORD=All", LAST ) ;
			
//			web_reg_save_param_json(
//				"ParamName=autoSelectOptionURL",
//				"QueryString=$.category..urlName",
//				"SelectAll=Yes",
//				"SEARCH_FILTERS",
//				"NotFound=Warning",
//			"LAST"); 
			web_reg_save_param_json(
				"ParamName=autoSelectOptionTerm",
				"QueryString=$.autosuggestions..termsArray..term",
				"SelectAll=Yes",
				"SEARCH_FILTERS",
				"NotFound=Warning",
			"LAST");
			web_reg_save_param_json(
				"ParamName=autoSelectOptionCategory",
				"QueryString=$.category..response..categoryUrl",
				"SelectAll=Yes",
				"SEARCH_FILTERS",
				"NotFound=Warning",
			"LAST");	
			web_add_header("isRest","true");
			web_add_header("Content-Type","application/json");
			web_add_header("term","{SEARCH_STRING_PARAM}");
			web_add_header("coreName","MC_10001_CatalogEntry_en_US");
			addHeader();
			
			lr_start_transaction ( "T02_Search_getAutoSuggestion" ) ;
			web_custom_request("getAutoSuggestions", 
						"URL=https://{api_host}/getAutoSuggestions", 
						"Method=GET", 
						"Resource=0", 
						"RecContentType=application/json", 
						LAST);

/*			web_submit_data("TCPAJAXAutoSuggestView",
				"Action=http://{host}/webapp/wcs/stores/servlet/{searchAutoSuggestURL}&term={SEARCH_STRING_PARAM}&showHeader=true&count=4",
				"Method=POST",
				"RecContentType=text/html",
				"Snapshot=t45.inf",
				"Mode=HTML",
				ITEMDATA,
				"Name=objectId", "Value=", ENDITEM,
				"Name=requesttype", "Value=ajax", ENDITEM,
				
				LAST);
*/

				lr_end_transaction ( "T02_Search_getAutoSuggestion" , LR_PASS ) ;
			//if there is a suggestion, capture it, exit the loop, use the suggestion for submitCompleteSearch
//			if (atoi(lr_eval_string("{autoSelectOption_count}")) > 0) {
			if (atoi(lr_eval_string("{autoSelectOptionTerm_count}")) > 0) {
				searchString = lr_paramarr_idx("autoSelectOptionTerm", 1);
				break;
			}
			else if (atoi(lr_eval_string("{autoSelectOptionCategory_count}")) > 0) {
				searchString = lr_paramarr_idx("autoSelectOptionCategory", 1);
				break;
			} 
//			else 
	
//if (atoi(lr_eval_string("{autoSelectOptionURL_count}")) > 0) {
//				searchString = lr_paramarr_idx("autoSelectOptionURL", 1);
//				break;
//			} 
			
       	} // end iF
   	 } // end for
 	lr_continue_on_error(0);

} // end typeAheadSearch()


void submitCompleteSearch() // 	Search for complete string should be placed
{
	lr_think_time (LINK_TT);
	categoryPageCorrelations();
	subCategoryPageCorrelations();
	productDisplayPageCorrelations();
	
//	lr_param_sprintf ("SEARCH_STRING", "%s", searchString); 
	lr_param_sprintf ("SEARCH_STRING", "%s", lr_eval_string("{originalSearchString}"));  //
	
//	web_convert_param("SEARCH_STRING", "SourceEncoding=PLAIN", "TargetEncoding=URL", LAST);
	web_reg_find("Text= 0 matches", "SaveCount=searchResults_Count", LAST );
	
// 	web_reg_save_param ( "searchCount" , "LB=<span class=\"search-count\">\r\n														", "RB=\r\n													</span>" , "Notfound=warning" , LAST ) ;
/*
    web_reg_find("Text= 0 matches",
        "SaveCount=zeroMatch_Count",
        LAST );	
*/		

	lr_start_transaction ( "T02_Search" );
	web_url("submitSearch", 
		  "URL=https://{host}/shop/SearchDisplay?searchTerm={SEARCH_STRING}&storeId={storeId}&catalogId={catalogId}&langId=-1&pageSize=100&beginIndex=0&searchSource=Q&sType=SimpleSearch&resultCatEntry=2&showResultsPage=true&pageView=image&custSrch=search",
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Mode=HTML", 
		LAST);
		
		lr_save_string("T02_Search", "mainTransaction");
		callAPI();
		if (isLoggedIn == 1) //IMP CHANGE BY PAVAN DUSI 06-27
		{
			//tcp_api2("tcporder/getAllCoupons", "GET", lr_eval_string("{mainTransaction}") );               // api_getCoupon();
			//api_getESpot_second();
		}

		
	if ( isLoggedIn == 1 ) {
		TCPGetWishListForUsersView();
	}
	
	if (atoi(lr_eval_string("{searchResults_Count}")) > 0) { //} && strlen(lr_eval_string("{searchResults}")) != 15 ) {
		lr_end_transaction ( "T02_Search" , LR_FAIL ) ; 
	} 
	else {
		lr_end_transaction ( "T02_Search" , LR_PASS ) ;
	}
	
	return;
		
} // end submitCompleteSearch
		
		
void submitCompleteSearchDidYouMean() // 	Search with "Did You Mean"
{
	//lr_message ( "In submitCompleteSearch" ) ;
 	lr_think_time (LINK_TT);
	categoryPageCorrelations();
	subCategoryPageCorrelations();
	productDisplayPageCorrelations();
	
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_SEARCH_SUGGEST )
		lr_param_sprintf ("SEARCH_STRING", "%s", lr_eval_string("{forDidYouMean}"));
	else
		lr_param_sprintf ("SEARCH_STRING", "%s", searchString);
	
	web_reg_find("Text= 0 matches", "SaveCount=searchResults_Count", LAST );
	web_reg_find("Text=The store has encountered a problem", "SaveCount=storeProblem_Count", LAST ); //The store has encountered a problem processing the last request. Try again later. If the problem persists, contact your site administrator.
	web_reg_save_param("didYouMeanId", "LB=http://{host}/shop/SearchDisplay?searchTermScope=&searchType=1002&filterTerm=&orderBy=&maxPrice=&showResultsPage=true&langId=-1&departmentId=&sType=", 
	"RB=\" class=\"result\">", "ORD=All", "NotFound=Warning", LAST);

	   //URL=https://{host}/shop/SearchDisplay?storeId=10151&catalogId=10551&langId=-1&pageSize=100&beginIndex=0&searchSource=Q&sType=SimpleSearch&resultCatEntryType=2&showResultsPage=true&pageView=image&custSrch=search&searchTerm=clips&TCPSearchSubmit=
	lr_start_transaction ( "T02_Search" ) ;

	// AND-258 - Search | Did You Mean? 
	 //"URL=https://{host}/shop/SearchDisplay?searchTerm={{SEARCH_STRING}}&storeId={storeId}&catalogId={catalogId}&langId=-1&pageSize=100&beginIndex=0&searchSource=Q&sType=SimpleSearch&resultCatEntry=2&showResultsPage=true&pageView=image&custSrch=search"
	web_url("submitSearch", 
		//"URL=https://{host}/shop/SearchDisplay?storeId={storeId}&catalogId={catalogId}&langId=-1&pageSize=100&beginIndex=0&searchSource=Q&sType=SimpleSearch&resultCatEntryType=2&showResultsPage=true&pageView=image&custSrch=search&searchTerm={SEARCH_STRING}&TCPSearchSubmit=", 
		"URL=https://{host}/shop/SearchDisplay?searchTerm={{SEARCH_STRING}}&storeId={storeId}&catalogId={catalogId}&langId=-1&pageSize=100&beginIndex=0&searchSource=Q&sType=SimpleSearch&resultCatEntry=2&showResultsPage=true&pageView=image&custSrch=search"
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Mode=HTML", 
		LAST);
		
		lr_save_string("T02_Search", "mainTransaction");
		callAPI();
		if (isLoggedIn == 1) //IMP CHANGE BY PAVAN DUSI 06-27
		{
			//tcp_api2("tcporder/getAllCoupons", "GET", lr_eval_string("{mainTransaction}") );               // api_getCoupon();
			//api_getESpot_second();
		}

		
	if (atoi(lr_eval_string("{searchResults_Count}")) == 0 || atoi(lr_eval_string("{storeProblem_Count}")) == 0 )
		lr_end_transaction ( "T02_Search" , LR_FAIL ) ; 
	else
		lr_end_transaction ( "T02_Search" , LR_PASS ) ;

} // end submitCompleteSearch
		
writeToFile1() 
{
	char *ip;
    char fullpath[1024], * filename1 = "\\e$\\Performance\\LogOutput\\LowInventory_US_v2.dat";
	long file1;
//	char * filename1 = "\\\\10.56.29.36\\e$\\Performance\\Scripts\\2016_R1\\03_PERF\\Datafiles\\TestData\\searchStrNoResult.dat";
    strcpy(fullpath, "\\\\" );
	ip = lr_get_host_name( );
    strcat(fullpath, ip);
	strcat(fullpath, filename1);
       
    if ((file1 = fopen(fullpath, "a+")) == NULL) {
        lr_output_message ("Unable to open %s", fullpath);
        return -1;
    }
	
	fprintf(file1, "%s\n", lr_eval_string("{logLowCatEntryId}"));
	
    fclose(file1);
	return 0;
}

writeToFile2() 
{
	char *ip;
    char fullpath[1024], * filename1 = "\\e$\\Performance\\LogOutput\\LowInventory_CA_v2.dat";
	long file1;
//	char * filename1 = "\\\\10.56.29.36\\e$\\Performance\\Scripts\\2016_R1\\03_PERF\\Datafiles\\TestData\\searchStrNoResult.dat";
    strcpy(fullpath, "\\\\" );
	ip = lr_get_host_name( );
    strcat(fullpath, ip);
	strcat(fullpath, filename1);
       
    if ((file1 = fopen(fullpath, "a+")) == NULL) {
        lr_output_message ("Unable to open %s", fullpath);
        return -1;
    }
	
	fprintf(file1, "%s\n", lr_eval_string("{logLowCatEntryId}"));
	
    fclose(file1);
	return 0;
}

void navBySearch()
{
	//lr_message ( "In navBySearch" ) ;

	int searchSuggest = atoi(lr_eval_string("{RANDOM_PERCENT}"));

	lr_think_time ( TYPE_SPEED );
/*********** 03/07/2017 Obsolete**************** Perf marked per discussion with Melvin Jose

	lr_start_transaction ( "T02_Search_ajaxCachedSuggestionView" ) ;

	web_submit_data("TCPAJAXCachedSuggestionsView",
		"Action=http://{host}/webapp/wcs/stores/servlet/{searchCachedSuggestionsURL}",
		"Method=POST",
		"RecContentType=text/html",
		"Snapshot=t44.inf",
		"Mode=HTML",
		ITEMDATA,
		"Name=objectId", "Value=", ENDITEM,
		"Name=requesttype", "Value=ajax", ENDITEM,
		LAST);

	lr_end_transaction ( "T02_Search_ajaxCachedSuggestionView" , LR_PASS ) ;
*/
//	searchString = "00889705462289"; //this is the search string from the data file
	searchString = lr_eval_string("{searchStr}"); //this is the search string from the data file
	lr_save_string(searchString,"originalSearchString");
	/****************REMOVE**********/
/*	searchString="boys";
	lr_save_string("boys",	"SEARCH_STRING_PARAM");
	lr_save_string("boys",	"originalSearchString");
	lr_save_string("boys",	"searchStr");
*/    length = (int)strlen(searchString);

    if (length >= 3) //type ahead only if search term is greater than 3 characters
    	typeAheadSearch();
	
	if ( searchSuggest < RATIO_SEARCH_SUGGEST )
		submitCompleteSearchDidYouMean();
	else
		submitCompleteSearch();

	//should we be calling callAPI here???
	
} //end navBySearch


void navByPlace()
{
	//lr_message ( "In navByPlace" ) ;
	if ( atoi( lr_eval_string( "{placeShopCategory_count}")) != 0 ) {
		
		lr_save_string( lr_paramarr_random("placeShopCategory"), "placeShop");
		
		if ( strcmp( lr_eval_string("{placeShop}") ,"placeShops-pjplace-2014" ) == 0) 
			lr_save_string( lr_paramarr_random("placeShopCategory"), "placeShop");
		
		lr_start_transaction ( "T02_PlaceShop" ) ;

		web_url("placeShop",
			"URL=http://{host}{store_home_page}content/{placeShop}",
			"Resource=0",
			"RecContentType=text/html",
			LAST);
		
		lr_end_transaction ( "T02_PlaceShop" , LR_PASS ) ;
	}
/*	
 <a href="http://tcp-perf.childrensplace.com/shop/us/content/mvp-shop-spring-2014" 

   	web_reg_save_param ( "placeShopCategory" ,
	                     "LB=/shop/us/content/" ,
	                     "RB=\"",
						 "ORD=All",
	                     "NotFound=Warning", LAST ) ;
	*/
} //end navByPlace


// T02_Category / Clearance / Search / PlaceShop Display
// select navigation method from NAV_BY parameter, "stop" will end iteration. This is a mandatory step to continue further and cannot be passed
void topNav()
{
//	nav_by = lr_eval_string("{RATIO_NAV_BY}") ;

	randomPercent = atoi(lr_eval_string("{RANDOM_PERCENT}"));

	if ( randomPercent < NAV_BROWSE ){
		//lr_message ( "callingNavByBrowse" ) ;
		navByBrowse();
		} // end if
	else if ( randomPercent < (NAV_BROWSE + NAV_CLEARANCE) ) {
		//lr_message ( "callingNavByClearance" ) ;
		navByClearance();
		} // end if
	else if ( randomPercent < (NAV_BROWSE + NAV_CLEARANCE + NAV_SEARCH)) {
		//lr_message ( "callingNavBySearch" ) ;
		navBySearch();
		} // end else-if
	else if (randomPercent < (NAV_BROWSE + NAV_CLEARANCE + NAV_SEARCH + NAV_PLACE)) {
		//lr_message ( "callingNavByPlace" ) ;
		navByPlace();
		} // end else-if

} // end topNav


void paginate()
{
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) < APPLY_PAGINATE ) {
		
		if (( lr_paramarr_len ( "paginationNextURL" ) > 0 ) ) {
			
			lr_think_time ( LINK_TT ) ;
			
		//	categoryPageCorrelations();
			subCategoryPageCorrelations();
			productDisplayPageCorrelations();
			index = rand ( ) % lr_paramarr_len( "paginationNextURL" ) + 1 ;
			lr_param_sprintf ("paginationURL", "http://%s%s&beginIndex=%s", lr_eval_string("{host}"), lr_paramarr_idx ( "paginationNextURL" , index ), lr_paramarr_idx ( "paginationPageIndex" , index ));
			lr_param_sprintf ("paginationIndex", "paginationPageIndex_%d", index);

			lr_start_transaction ( "T03_Paginate" ) ;
			
				web_submit_data("AjaxCatalogSearchResultView",
					"Action={paginationURL}",
					"Method=POST",
					"RecContentType=text/html",
					"Mode=HTML",
					ITEMDATA,
					"Name=searchResultsPageNum", "Value=-{paginationIndex}", ENDITEM,
					"Name=searchResultsView", "Value=", ENDITEM,
					"Name=searchResultsURL", "Value={paginationURL}", ENDITEM,
					"Name=objectId", "Value=", ENDITEM,
					"Name=requesttype", "Value=ajax", ENDITEM,
					LAST);
					
			lr_end_transaction ( "T03_Paginate" , LR_PASS ) ;
		}
	}

} // end pagination


void sortResults()
{
	//lr_message ( "in sortResults" ) ;
 	lr_think_time ( LINK_TT ) ;
	if (( lr_paramarr_len ( "sortURL" ) > 0 )) {
		categoryPageCorrelations();
		subCategoryPageCorrelations();
		productDisplayPageCorrelations();
	//	index = rand ( ) % lr_paramarr_len( "sortURL" ) + 1 ;
	//	lr_param_sprintf ( "drillUrl" , "http://%s%s" , lr_eval_string("{host}") , lr_paramarr_idx ( "sortURL" , index ) ) ;
		lr_param_sprintf ( "drillUrl" , "http://%s%s" , lr_eval_string("{host}") , lr_paramarr_random ( "sortURL" ) ) ;

		lr_start_transaction ( "T03_Sort Results" ) ;
			webURL(lr_eval_string ( "{drillUrl}" ), "T03_Sort Results" );
		lr_end_transaction ( "T03_Sort Results" , LR_PASS ) ;
	} // end if
} // end sortResults

void sort()
{
// T03_Sort Results
// Optional step - Apply Sort. This step will be skipped if the return value is "pass",
// sort_by = lr_eval_string("{SORT_BY}") ;

//	randomPercent = atoi(lr_eval_string("{RANDOM_PERCENT}"));

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) < APPLY_SORT ){
		//lr_message ( "Applying Sort" ) ;
		sortResults();
		} // end if
	else {
		//lr_message ( "Skipping Sort" ) ;
		return;
		} // end if

} // end sort

void drillOneFacet()
{
	//lr_message ( "in drillOneFacet" ) ;
 	lr_think_time ( LINK_TT ) ;

	if (( lr_paramarr_len ( "facetsID" ) > 0 ) ) {
		categoryPageCorrelations();
		subCategoryPageCorrelations();
		productDisplayPageCorrelations();
		index = rand ( ) % lr_paramarr_len( "facetsID" ) + 1 ;
		lr_param_sprintf ( "drillUrlOneFacet" , "http://%s%s&facet=%s" , lr_eval_string("{host}") , lr_paramarr_idx ( "facetsURL" , index ) , lr_paramarr_idx ( "facetsID" , index ) ) ;
//		lr_param_sprintf ( "drillUrlOneFacet" , "http://%s/shop/SearchDisplay?%s&facet=%s" , lr_eval_string("{host}") , lr_paramarr_idx ( "facetsURL" , index ) , lr_paramarr_idx ( "facetsID" , index ) ) ;
//		lr_param_sprintf ( "drillUrlOneFacet" , "http://%s/shop/SearchDisplay?%s&facet=%s" , lr_eval_string("{host}") , lr_paramarr_random ( "facetsURL" ) , lr_paramarr_random ( "facetsID" ) ) ;

		lr_start_transaction ( "T03_Facet Display_1facet" ) ;

			webURL(lr_eval_string ( "{drillUrlOneFacet}" ), "T03_Facet Display_1facet" );

			if ( isLoggedIn == 1 ) {
				//TCPGetWishListForUsersView(); //1206 12am 
				//wlGetAll();
			}
			lr_save_string("T03_Facet Display_1facet", "mainTransaction");
			callAPI();
			if (isLoggedIn == 1) //IMP CHANGE BY PAVAN DUSI 06-27
			{
				//tcp_api2("tcporder/getAllCoupons", "GET", lr_eval_string("{mainTransaction}") );               // api_getCoupon();
				//api_getESpot_second();
			}


		lr_end_transaction ( "T03_Facet Display_1facet" , LR_PASS ) ;

	} // end if
} // end drillOneFacet


void drillTwoFacets()
{
	//lr_message ( "in drillTwoFacets" ) ;
	drillOneFacet();
 	lr_think_time ( LINK_TT ) ;
	if (( lr_paramarr_len ( "facetsID" ) > 0 ) ) {
		categoryPageCorrelations();
		subCategoryPageCorrelations();
		productDisplayPageCorrelations();
//		index = rand ( ) % lr_paramarr_len( "facetsID" ) + 1 ;
//		lr_param_sprintf ( "drillUrlTwoFacets" , "%s&facet=%s" , lr_eval_string("{drillUrlOneFacet}") , lr_paramarr_idx ( "facetsID" , index ) ) ;
		lr_param_sprintf ( "drillUrlTwoFacets" , "%s&facet=%s" , lr_eval_string("{drillUrlOneFacet}") , lr_paramarr_random ( "facetsID"  ) ) ;
		
		lr_start_transaction ( "T03_Facet Display_2facets" ) ;

			webURL(lr_eval_string ( "{drillUrlTwoFacets}" ), "T03_Facet Display_2facets" );

			if ( isLoggedIn == 1 ) {
				TCPGetWishListForUsersView();
				//wlGetAll();
			}
		lr_save_string("T03_Facet Display_2facets", "mainTransaction");
		callAPI();
		
		if (isLoggedIn == 1) //IMP CHANGE BY PAVAN DUSI 06-27
		{
			//tcp_api2("tcporder/getAllCoupons", "GET", lr_eval_string("{mainTransaction}") );               // api_getCoupon();
			//api_getESpot_second();
		}


		lr_end_transaction ( "T03_Facet Display_2facets" , LR_PASS ) ;

	} // end if
} // end drillTowFacets


void drillThreeFacets()
{
	//lr_message ( "in drillThreeFacets" ) ;
	drillTwoFacets();
 	lr_think_time ( LINK_TT ) ;
	if (( lr_paramarr_len ( "facetsID" ) > 0 ) ) {
		categoryPageCorrelations();
		subCategoryPageCorrelations();
		productDisplayPageCorrelations();
//		index = rand ( ) % lr_paramarr_len( "facetsID" ) + 1 ;
//		lr_param_sprintf ( "drillUrlThreeFacets" , "%s&facet=%s" , lr_eval_string("{drillUrlTwoFacets}") , lr_paramarr_idx ( "facetsID" , index ) ) ;
		lr_param_sprintf ( "drillUrlThreeFacets" , "%s&facet=%s" , lr_eval_string("{drillUrlTwoFacets}") , lr_paramarr_random ( "facetsID" ) ) ;
		
		lr_start_transaction ( "T03_Facet Display_3facets" ) ;

			webURL(lr_eval_string ( "{drillUrlThreeFacets}" ), "T03_Facet Display_3facets" );

			if ( isLoggedIn == 1 ) {
				TCPGetWishListForUsersView();
				//wlGetAll();
			}
		lr_save_string("T03_Facet Display_3facets", "mainTransaction");
		callAPI();
		if (isLoggedIn == 1) //IMP CHANGE BY PAVAN DUSI 06-27
		{
			//tcp_api2("tcporder/getAllCoupons", "GET", lr_eval_string("{mainTransaction}") );               // api_getCoupon();
			//api_getESpot_second();
		}


		lr_end_transaction ( "T03_Facet Display_3facets" , LR_PASS ) ;

	}// end if
}// end drillThreeFacets


void drillSubCategory()
{
 	lr_think_time ( LINK_TT ) ;
	
	categoryPageCorrelations();
	subCategoryPageCorrelations();
	productDisplayPageCorrelations();

	if (( lr_paramarr_len ( "subcategories" ) > 0 )) {
		index = rand ( ) % lr_paramarr_len( "subcategories" ) + 2 ; 
		
		if (index > lr_paramarr_len( "subcategories" ) )
			index--;
		
		lr_param_sprintf ( "drillUrl" , "%s" , lr_paramarr_idx ( "subcategories", index ) ) ;
		//lr_param_sprintf ( "drillUrl" , "%s" , lr_paramarr_random ( "subcategories" ) ) ;
		
		lr_continue_on_error(1);
		
		lr_start_transaction ( "T03_Sub-Category Display" ) ;

//			webURL(lr_eval_string ( "{drillUrl}" ), "T03_Sub-Category Display" );
		if ( webURL2(lr_eval_string ( "{drillUrl}" ), "T03_Sub-Category Display", "SHOP BY CATEGORY" ) == LR_PASS) 
		{
			lr_save_string("T03_Sub-Category Display", "mainTransaction");
			callAPI();
			if (isLoggedIn == 1) //IMP CHANGE BY PAVAN DUSI 06-27
			{
				//tcp_api2("tcporder/getAllCoupons", "GET", lr_eval_string("{mainTransaction}") );               // api_getCoupon();
				//api_getESpot_second();
			}

		
			if ( isLoggedIn == 1 ) {
				TCPGetWishListForUsersView();
				//wlGetAll();
			}
			
			lr_end_transaction ( "T03_Sub-Category Display" , LR_PASS ) ;
		}
		else if ( atoi(lr_eval_string("{noProducts}")) == 1 ) 
		{
				lr_fail_trans_with_error( lr_eval_string("T03_Sub-Category Display Text Check Failed - {drillUrl} AND no products found") ) ;
				lr_end_transaction ( "T03_Sub-Category Display" , LR_FAIL ) ;
		}
		else {
				lr_fail_trans_with_error( lr_eval_string("T03_Sub-Category Display Text Check Failed - {drillUrl}") ) ;
				lr_end_transaction ( "T03_Sub-Category Display" , LR_FAIL ) ;
		}
		
		lr_continue_on_error(0);

	} // end if
	else if ((lr_paramarr_len( "searchSubCategories") > 0)) {
		index = rand ( ) % lr_paramarr_len( "subcategories" ) + 2 ; 
		lr_param_sprintf ( "drillUrl" , "http://%s%s" , lr_eval_string("{host}") , lr_paramarr_idx ( "searchSubCategories", index ) ) ;
		//lr_param_sprintf ( "drillUrl" , "http://%s%s" , lr_eval_string("{host}") , lr_paramarr_random ( "searchSubCategories" ) ) ;

		lr_continue_on_error(1);
		
		lr_start_transaction ( "T03_Sub-Category Display" ) ;

//			webURL(lr_eval_string ( "{drillUrl}" ), "T03_Sub-Category Display" );
		if ( webURL2(lr_eval_string ( "{drillUrl}" ), "T03_Sub-Category Display", "FILTER BY" ) == LR_PASS) {

			lr_save_string("T03_Sub-Category Display", "mainTransaction");
			callAPI();
			if (isLoggedIn == 1) //IMP CHANGE BY PAVAN DUSI 06-27
			{
				//tcp_api2("tcporder/getAllCoupons", "GET", lr_eval_string("{mainTransaction}") );               // api_getCoupon();
				//api_getESpot_second();
			}

				
			if ( isLoggedIn == 1 ) {
				TCPGetWishListForUsersView();
				//wlGetAll();
			}

			lr_end_transaction ( "T03_Sub-Category Display" , LR_PASS ) ;
		}
		else if ( atoi(lr_eval_string("{noProducts}")) == 1 ) 
		{
				lr_fail_trans_with_error( lr_eval_string("T03_Sub-Category Display Failed NO PRODUCTS FOUND for - {drillUrl}") ) ;
				lr_end_transaction ( "T03_Sub-Category Display" , LR_FAIL ) ;
		}
		else {
				lr_fail_trans_with_error( lr_eval_string("T03_Sub-Category Display Text Check Failed for - {drillUrl}") ) ;
				lr_end_transaction ( "T03_Sub-Category Display" , LR_FAIL ) ;
		}

	} // end else-if
	
	lr_continue_on_error(0);
	
}// end drillSubCategory


void drill()
{
// T03_Drill Facets or SubCategory
// Apply drill method from DRILL_BY parameter after performing some validations, "stop" will end iteration, "pass" will continue to PDP / Quick View step
	if (( lr_paramarr_len( "subcategories" ) == 0 ) && (lr_paramarr_len( "searchSubCategories") == 0) && ( lr_paramarr_len ( "facetsID" ) == 0 ) ) {
		//lr_message ( "no subcategories, no facet, break" ) ;
		//lr_exit(LR_EXIT_ITERATION_AND_CONTINUE, LR_PASS);
		return;
	} // end if
	else if ( lr_paramarr_len ( "subcategories" ) == 0 && (lr_paramarr_len( "searchSubCategories") == 0)) {
		drill_by = "ONE_FACET" ;
		//lr_message ( "no subcategories" ) ;
		} // end else-if
	else if ( lr_paramarr_len ( "facetsID" ) == 0 ) {
		drill_by = "SUB_CATEGORY" ;
		//lr_message ( "no facets" ) ;
		} // end else-if
	else {
		//lr_message ( "Select drill method from DRILL_BY parameter" ) ;
		//drill_by = lr_eval_string("{DRILL_BY}") ;

		randomPercent = atoi(lr_eval_string("{RANDOM_PERCENT}"));

		if ( randomPercent <= DRILL_SUB_CATEGORY ){
			drill_by = "SUB_CATEGORY" ;
			} // end if
		else if ( randomPercent <= (DRILL_SUB_CATEGORY + DRILL_TWO_FACET) ) {
			drill_by = "TWO_FACET" ;
			} // end else-if
		else if ( randomPercent <= (DRILL_SUB_CATEGORY + DRILL_TWO_FACET + DRILL_THREE_FACET)) {
			drill_by = "THREE_FACET" ;
			} // end else-if
		else if (randomPercent <= (DRILL_SUB_CATEGORY + DRILL_TWO_FACET + DRILL_THREE_FACET + DRILL_ONE_FACET)) {
			drill_by = "ONE_FACET" ;
			} // end else-if

	} // end else-if - T03 validations complete - drill_by mode selected

	if (strcmp(drill_by, "ONE_FACET") == 0) {
		//lr_message ( "applying DRILL_ONE_FACET" ) ;
		drillOneFacet();
		} // end if
	else if (strcmp(drill_by, "TWO_FACET") == 0) {
		//lr_message ( "applying DRILL_TWO_FACET" ) ;
		drillTwoFacets();
		} // end else-if
	else if (strcmp(drill_by, "THREE_FACET") == 0) {
		//lr_message ( "applying DRILL_THREE_FACET" ) ;
		drillThreeFacets();
		} // end else-if
	else if (strcmp(drill_by, "SUB_CATEGORY") == 0) {
		//lr_message ( "applying DRILL_SUB_CATEGORY" ) ;
		drillSubCategory();
		} // end else-if
	else if (strcmp(drill_by, "stop") == 0) {
		//lr_message ( "stop iteration @ T03_subcategory / facet transaction" ) ;
//		lr_exit(LR_EXIT_ITERATION_AND_CONTINUE, LR_PASS);
		} // end else-if
	else if (strcmp(drill_by, "pass") == 0){
//		lr_exit(LR_EXIT_ITERATION_AND_CONTINUE, LR_PASS);
		//lr_message ( "skip subcateogry & facet step and continue to PDP / QuickView step" ) ;
	} // end else-if - end of T03_Drill Facet or SubCategory transaction
} // end drill

void productDetailViewBOPIS()
{
 	lr_think_time ( LINK_TT ) ;
	
	if (( lr_paramarr_len ( "pdpURL" ) > 0 )) {
		
		productDisplayPageCorrelations();

		lr_continue_on_error(1);
		
		lr_param_sprintf ( "drillUrl" , "http://%s%s" , lr_eval_string("{host}") , lr_paramarr_random ( "pdpURL" ) ) ;
		
//		lr_save_string ( lr_eval_string("https://{host}/shop/us/p/boys-clothing/boys-clothing/boys-pants/Boys-Chino-Pants-1101084-FX"), "drillUrl"); //perflive

		lr_start_transaction ( "T04_Product Display Page" ) ;
		
//		webURL(lr_eval_string ( "{drillUrl}" ), "T04_Product Display Page" );

		if (webURL2(lr_eval_string ( "{drillUrl}" ), "T04_Product Display Page", "Mouse over image to zoom in") == LR_PASS) {
			
			addToCartCorrelations();

//	web_reg_save_param("lowCatEntryId", "LB=\"catentry_id\" : \"", "RB=\"", "ORD=ALL", "NotFound=Warning", LAST); //"catentry_id" : "771570"
//	web_reg_save_param("lowQty", "LB=\"qty\" : \"", "RB=\"", "ORD=ALL", "NotFound=Warning", LAST); //"qty" : "8"
			lr_start_sub_transaction ("T04_Product Display Page - TCPSkuSelectionView", "T04_Product Display Page" );

			web_custom_request("TCPSKUSelectionView",
				"URL=http://{host}/webapp/wcs/stores/servlet/TCPSKUSelectionView?langId=-1&storeId={storeId}&catalogId={catalogId}&LimQty=undefined&TCPWebOnlyFlag=&productId={skuSelectionProductID}",
				"Method=GET",
				"Resource=0",
				"Mode=HTML",
				"EncType=application/x-www-form-urlencoded",
				LAST);
					
			lr_end_sub_transaction ( "T04_Product Display Page - TCPSkuSelectionView", LR_PASS );

			lr_save_string("T04_Product Display Page", "mainTransaction");
			callAPI();
			if (isLoggedIn == 1) //IMP CHANGE BY PAVAN DUSI 06-27
			{
				//tcp_api2("tcporder/getAllCoupons", "GET", lr_eval_string("{mainTransaction}") );               // api_getCoupon();
				//api_getESpot_second();
			}


			if ( isLoggedIn == 1 ) {
				TCPGetWishListForUsersView();
				//wlGetAll();
			}
			
			lr_save_string("PDP","PDP_or_PQV");
			lr_end_transaction ( "T04_Product Display Page" , LR_PASS ) ;
		
		}
		else {
			lr_fail_trans_with_error( lr_eval_string("T04_Product Display Text Check Failed - {drillUrl} - {storeId}") ) ;
			lr_end_transaction ( "T04_Product Display Page" , LR_FAIL ) ;
		}
		
		lr_continue_on_error(0);

	}  
	
} 

void productDetailView()
{
 	lr_think_time ( LINK_TT ) ;
	
	if (( lr_paramarr_len ( "pdpURL" ) > 0 )) {
		
//		TCPIShippingView(); //disabled 0324
		
		productDisplayPageCorrelations();

		lr_param_sprintf ( "drillUrl" , "http://%s%s" , lr_eval_string("{host}") , lr_paramarr_random ( "pdpURL" ) ) ;
		lr_continue_on_error(1);
		
		lr_start_transaction ( "T04_Product Display Page" ) ;
		
//		webURL(lr_eval_string ( "{drillUrl}" ), "T04_Product Display Page" );
		if (webURL2(lr_eval_string ( "{drillUrl}" ), "T04_Product Display Page", "Mouse over image to zoom in") == LR_PASS) {

			addToCartCorrelations();
//	web_reg_save_param("lowCatEntryId", "LB=\"catentry_id\" : \"", "RB=\"", "ORD=ALL", "NotFound=Warning", LAST); //"catentry_id" : "771570"
//	web_reg_save_param("lowQty", "LB=\"qty\" : \"", "RB=\"", "ORD=ALL", "NotFound=Warning", LAST); //"qty" : "8"
					

			lr_start_sub_transaction ("T04_Product Display Page - TCPSkuSelectionView", "T04_Product Display Page" );

			web_custom_request("TCPSKUSelectionView",
				"URL=http://{host}/webapp/wcs/stores/servlet/TCPSKUSelectionView?langId=-1&storeId={storeId}&catalogId={catalogId}&LimQty=undefined&TCPWebOnlyFlag=&productId={skuSelectionProductID}",
				"Method=GET",
				"Resource=0",
				"RecContentType=text/html",
				"Snapshot=t18.inf",
				"Mode=HTML",
				"EncType=application/x-www-form-urlencoded",
				LAST);
					
			lr_end_sub_transaction ( "T04_Product Display Page - TCPSkuSelectionView", LR_PASS );
			
			lr_save_string("T04_Product Display Page", "mainTransaction");
			callAPI();
			if (isLoggedIn == 1) //IMP CHANGE BY PAVAN DUSI 06-27
			{
				//tcp_api2("tcporder/getAllCoupons", "GET", lr_eval_string("{mainTransaction}") );               // api_getCoupon();
				//api_getESpot_second();
			}


			if ( isLoggedIn == 1 ) {
				TCPGetWishListForUsersView();
			}
			
			lr_save_string("PDP","PDP_or_PQV");
			lr_end_transaction ( "T04_Product Display Page" , LR_PASS ) ;
		
		}
		else {
			lr_fail_trans_with_error( lr_eval_string("T04_Product Display Text Check Failed - {drillUrl} - {storeId}") ) ;
			lr_end_transaction ( "T04_Product Display Page" , LR_FAIL ) ;
		}
		lr_continue_on_error(0);

	}  
	
} // end productDisplay


void productQuickView()
{
 	lr_think_time ( LINK_TT ) ;
	
	if (( lr_paramarr_len ( "quickviewURL" ) > 0 )) {
		
		productDisplayPageCorrelations();
		
		lr_param_sprintf ( "drillUrl" , "http://%s%s" , lr_eval_string("{host}") , lr_paramarr_random ( "quickviewURL" ) ) ;
		
		lr_start_transaction ( "T04_Product Quickview Page" ) ;
		
		webURL(lr_eval_string ( "{drillUrl}" ), "T04_Product Quickview Page" );
	

		lr_start_sub_transaction ("T04_Product Quickview Page - GetSwatchesAndSizeInfo", "T04_Product Quickview Page" );

		web_custom_request("GetSwatchesAndSizeInfo",
			"URL=http://{host}/webapp/wcs/stores/servlet/GetSwatchesAndSizeInfo?langId=-1&storeId={storeId}&catalogId={catalogId}&productId={skuSelectionProductID}",
			"Method=GET",
			"Resource=0",
			"RecContentType=text/html",
			"Snapshot=t18.inf",
			"Mode=HTML",
			"EncType=application/x-www-form-urlencoded",
			LAST);

		lr_end_sub_transaction ( "T04_Product Quickview Page - GetSwatchesAndSizeInfo", LR_PASS );

//		addToCartCorrelations();
//		web_reg_save_param("atc_catentryIds", "LB=\t\t\t{ \"", "RB=\" : \"", "ORD=All", "NotFound=Warning", LAST);
//		web_reg_save_param("catentryidQTY", "LB=\" : \"", "RB=\" }\r\n", "ORD=All", "NotFound=Warning", LAST);		
		web_reg_save_param_regexp( "ParamName=atc_catentryIds" ,
	                          "RegExp={ \"(.*?)\" : \"([1-9][0-9]*)\"" ,
	                           //"RegExp=\"catentry_id\" : \"(.*?)\",\r\n\t\t\t\t\t\t\t\t\t\t\t\"item_sku\" : \"(.*?)\",\r\n\t\t\t\t\t\t\t\t\t\t\t\"qty\" : \"([1-9][0-9]*)\"" ,  //used on PDP
			                   "Ordinal=ALL" ,
			                   "Notfound=warning" ,
			                   SEARCH_FILTERS ,
			                   "Group=1" ,
			                   LAST ) ;

//		web_reg_save_param("lowCatEntryId", "LB=\"catentry_id\" : \"", "RB=\"", "ORD=ALL", "NotFound=Warning", LAST); //"catentry_id" : "771570"
//		web_reg_save_param("lowQty", "LB=\"qty\" : \"", "RB=\"", "ORD=ALL", "NotFound=Warning", LAST); //"qty" : "8"
							   
//pdp https://uatlive1.childrensplace.com/shop/us/p/girls-clothing/girls-denim-bottoms/girls-bootcut-jeans/Girls-Basic-Bootcut-Jeans---Odyssey-Wash-1150077-B4
	   lr_start_sub_transaction ("T04_Product Quickview Page - TCPGetSKUDetailsView", "T04_Product Quickview Page" );

		web_custom_request("TCPGetSKUDetailsView",
			"URL=http://{host}/webapp/wcs/stores/servlet/TCPGetSKUDetailsView?langId=-1&storeId={storeId}&catalogId={catalogId}&productId={skuSelectionProductID}",
			"Method=GET",
			"Resource=0",
			"RecContentType=text/html",
			"Snapshot=t18.inf",
			"Mode=HTML",
			"EncType=application/x-www-form-urlencoded",
			LAST);

		lr_end_sub_transaction ( "T04_Product Quickview Page - TCPGetSKUDetailsView", LR_PASS );
			

		if ( isLoggedIn == 1 ) 
			TCPGetWishListForUsersView();//1204 5pm to reduce TCPGetWishListForUsersView by 75k

		lr_save_string("PQV","PDP_or_PQV");

		lr_end_transaction ( "T04_Product Quickview Page" , LR_PASS ) ;
		
		lr_continue_on_error(0);

	} // end if
	
} // end ProductQuickView

void productQuickviewService() //Add this call right after a Product Display Page Call
{
	web_reg_find("SaveCount=swatchesAndSizeInfo", "Text/IC=itemTCPBogo",LAST);
	
	lr_continue_on_error(1);

	lr_start_transaction ("T04_Product Quickview Service - GetSwatchesAndSizeInfo" );

		web_custom_request("GetSwatchesAndSizeInfo",
			"URL=http://{host}/webapp/wcs/stores/servlet/GetSwatchesAndSizeInfo?storeId={storeId}&productId={skuSelectionProductID}",
			"Method=GET",
			"Mode=HTML",
			LAST);

	if(atoi(lr_eval_string("{swatchesAndSizeInfo}")) > 0)
		lr_end_transaction ( "T04_Product Quickview Service - GetSwatchesAndSizeInfo", LR_PASS );	
	else {
		lr_param_sprintf ( "failedPageUrl" , "T03_Sub-Category Display Failure : %s", lr_eval_string("http://{host}/webapp/wcs/stores/servlet/GetSwatchesAndSizeInfo?storeId={storeId}&productId={skuSelectionProductID}") ) ;
		lr_fail_trans_with_error( lr_eval_string("T04_Product Display Text Check Failed - {failedPageUrl} - {storeId}") ) ;
		lr_end_transaction ( "T04_Product Quickview Service - GetSwatchesAndSizeInfo", LR_FAIL );	
	}
	lr_continue_on_error(0);

	web_reg_find("SaveCount=detailsView", "Text/IC=inventoryFlag",LAST);

	lr_start_transaction ("T04_Product Quickview Service - TCPGetSKUDetailsView" );

		web_custom_request("TCPGetSKUDetailsView",
			"URL=http://{host}/webapp/wcs/stores/servlet/TCPGetSKUDetailsView?storeId={storeId}&catalogId={catalogId}&langId=-1&productId={skuSelectionProductID}",
			"Method=GET",
			"Mode=HTML",
			LAST);


	if(atoi(lr_eval_string("{detailsView}")) > 0)
		lr_end_transaction ( "T04_Product Quickview Service - TCPGetSKUDetailsView", LR_PASS );	
	else
		lr_end_transaction ( "T04_Product Quickview Service - TCPGetSKUDetailsView", LR_FAIL );	

}

void productDisplay()
{
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= PDP )
	{
		if (( lr_paramarr_len ( "pdpURL" ) > 0 )) 
			productDetailView(); //calls the TCPGetWishListForUsersView 2x
		else
			productQuickView();
	}
	else 
	{
		if (( lr_paramarr_len ( "quickviewURL" ) > 0 )) 
			productQuickView(); //calls the TCPGetWishListForUsersView 1x
		else
			productDetailView();
	}
	
} // end ProductDisplay

void productDisplayBOPIS()
{
	productDetailViewBOPIS(); //calls the TCPGetWishListForUsersView 2x
}


void switchColor()
{
} // end switchColor

void emailSignUp()
{
	lr_start_transaction ( "T16_Submit Shipping Address" ) ;
	lr_end_transaction ( "T16_Submit Shipping Address" , LR_PASS) ;

} // end emailSignUp


void StoreLocator()
{
 	lr_think_time ( LINK_TT ) ;
	
	
	lr_start_transaction("T22_StoreLocator");
	
		lr_save_string("T22_StoreLocator", "mainTransaction" );

		lr_start_sub_transaction("T22_StoreLocator_AjaxStoreLocatorDisplayView", "T22_StoreLocator" );
		web_url("Stores", 
		"URL=http://{host}/shop/AjaxStoreLocatorDisplayView?catalogId={catalogId}&langId=-1&storeId={storeId}", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Mode=HTML", 
		LAST);

		web_add_header("locStore", "False");
		web_add_header("pageName", "orderSummary");
		web_reg_save_param("cartCount", "LB=CartCount\": ", "RB=,", "NotFound=Warning", LAST);			
		tcp_api2("getOrderDetails", "GET", lr_eval_string("{mainTransaction}") );                      
//		tcp_api2("getESpot", "GET", lr_eval_string("{mainTransaction}") );                             
		tcp_api2("payment/getRegisteredUserDetailsInfo", "GET",  lr_eval_string("{mainTransaction}") );
		tcp_api2("payment/getPointsService", "GET",  lr_eval_string("{mainTransaction}") );            
		lr_end_sub_transaction("T22_StoreLocator_AjaxStoreLocatorDisplayView", LR_AUTO);

		lr_start_sub_transaction("T22_StoreLocator_AjaxStoreLocatorResultsView", "T22_StoreLocator" );
		web_submit_data("AjaxStoreLocatorResultsView", 
			"Action=http://{host}/shop/AjaxStoreLocatorResultsView?catalogId={catalogId}&langId=-1&orderId=&storeId={storeId}", 
			"Method=POST", 
			"TargetFrame=", 
			"RecContentType=text/html", 
			"Mode=HTML", 
			ITEMDATA, 
			"Name=distance", "Value={storeLocatorDistance}", ENDITEM, 
			"Name=latitude", "Value={storeLocatorLatitude}", ENDITEM, 
			"Name=longitude", "Value={storeLocatorLongitude}", ENDITEM, 
			"Name=displayStoreInfo", "Value=false", ENDITEM, 
			"Name=fromPage", "Value=StoreLocator", ENDITEM, 
			"Name=objectId", "Value=", ENDITEM, 
			"Name=requesttype", "Value=ajax", ENDITEM, 
			LAST);
		lr_end_sub_transaction("T22_StoreLocator_AjaxStoreLocatorResultsView", LR_AUTO);
		
		lr_start_sub_transaction("T22_StoreLocator_TCPStoreLocatorGetJsonOnStoresByLatLngView", "T22_StoreLocator" );
		web_url("TCPStoreLocatorGetJsonOnStoresByLatLngView", 
		"URL=http://{host}/shop/TCPStoreLocatorGetJsonOnStoresByLatLngView?storeId={storeId}&catalogId={catalogId}&langId=-1&latitude={storeLocatorLatitude}&longitude={storeLocatorLongitude}&distance={{storeLocatorDistance}}", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Mode=HTML", 
		LAST);
		lr_end_sub_transaction("T22_StoreLocator_TCPStoreLocatorGetJsonOnStoresByLatLngView", LR_AUTO);
	
	lr_end_transaction("T22_StoreLocator", LR_AUTO);
	
	lr_start_transaction("T22_StoreLocator_Find");
	
		lr_start_sub_transaction("T22_StoreLocator_Find_StoreInfo_False", "T22_StoreLocator_Find" );
		web_submit_data("AjaxStoreLocatorResultsView", 
			"Action=http://{host}/shop/AjaxStoreLocatorResultsView?catalogId={catalogId}&langId=-1&orderId=&storeId={storeId}", 
			"Method=POST", 
			"TargetFrame=", 
			"RecContentType=text/html", 
			"Mode=HTML", 
			ITEMDATA, 
			"Name=distance", "Value={storeLocatorDistance}", ENDITEM, 
			"Name=latitude", "Value={storeLocatorLatitude}", ENDITEM, 
			"Name=longitude", "Value={storeLocatorLongitude}", ENDITEM, 
			"Name=displayStoreInfo", "Value=false", ENDITEM, 
			"Name=fromPage", "Value=StoreLocator", ENDITEM, 
			"Name=objectId", "Value=", ENDITEM, 
			"Name=requesttype", "Value=ajax", ENDITEM, 
			LAST);
		lr_end_sub_transaction("T22_StoreLocator_Find_StoreInfo_False", LR_AUTO);
	
		web_reg_save_param("moreLink", "LB=<a href=\"/", "RB=\" class=", "NotFound=Warning", "ORD=All", LAST);
		lr_start_sub_transaction("T22_StoreLocator_Find_StoreInfo_True", "T22_StoreLocator_Find" );
		web_submit_data("AjaxStoreLocatorResultsView", 
			"Action=http://{host}/shop/AjaxStoreLocatorResultsView?catalogId={catalogId}&langId=-1&orderId=&storeId={storeId}", 
			"Method=POST", 
			"TargetFrame=", 
			"RecContentType=text/html", 
			"Mode=HTML", 
			ITEMDATA, 
			"Name=distance", "Value={storeLocatorDistance}", ENDITEM, 
			"Name=latitude", "Value={storeLocatorLatitude}", ENDITEM, 
			"Name=longitude", "Value={storeLocatorLongitude}", ENDITEM, 
			"Name=displayStoreInfo", "Value=true", ENDITEM, 
			"Name=fromPage", "Value=StoreLocator", ENDITEM, 
			"Name=objectId", "Value=", ENDITEM, 
			"Name=requesttype", "Value=ajax", ENDITEM, 
			LAST);
		lr_end_sub_transaction("T22_StoreLocator_Find_StoreInfo_True", LR_AUTO);
	
		lr_start_sub_transaction("T22_StoreLocator_Find_TCPStoreLocatorGetJsonOnStoresByLatLngView", "T22_StoreLocator_Find" );
		web_url("TCPStoreLocatorGetJsonOnStoresByLatLngView", 
		"URL=http://{host}/shop/TCPStoreLocatorGetJsonOnStoresByLatLngView?storeId={storeId}&catalogId={catalogId}&langId=-1&latitude={storeLocatorLatitude}&longitude={storeLocatorLongitude}&distance={{storeLocatorDistance}}", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Mode=HTML", 
		LAST);
		lr_end_sub_transaction("T22_StoreLocator_Find_TCPStoreLocatorGetJsonOnStoresByLatLngView", LR_AUTO);
		
	lr_end_transaction("T22_StoreLocator_Find", LR_AUTO);
	
	lr_save_string( lr_paramarr_random("moreLink") , "locationLink" );
	lr_save_string("T22_StoreLocator_MoreLocation", "mainTransaction" );
		
	lr_start_transaction("T22_StoreLocator_More");
		lr_start_sub_transaction("T22_StoreLocator_MoreLocation", "T22_StoreLocator_More" );
		web_url("More", 
		"URL=http://{host}/{locationLink}", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Mode=HTML", 
		LAST);

		web_add_header("locStore", "False");
		web_add_header("pageName", "orderSummary");
		web_reg_save_param("cartCount", "LB=CartCount\": ", "RB=,", "NotFound=Warning", LAST);			
		tcp_api2("getOrderDetails", "GET", lr_eval_string("{mainTransaction}") );                      
//		tcp_api2("getESpot", "GET", lr_eval_string("{mainTransaction}") );                             
		tcp_api2("payment/getRegisteredUserDetailsInfo", "GET",  lr_eval_string("{mainTransaction}") );
		tcp_api2("payment/getPointsService", "GET",  lr_eval_string("{mainTransaction}") );            
		lr_end_sub_transaction("T22_StoreLocator_MoreLocation", LR_AUTO);
	
	lr_end_transaction("T22_StoreLocator_More", LR_AUTO);
	
}

void TCPGetWishListForUsersViewBrowse()
{
	lr_think_time ( LINK_TT ) ;
	
	lr_start_transaction("T25_Common_S01_TCPGetWishListForUsersView");

	web_custom_request("TCPGetWishListForUsersView",
		"URL=http://{host}/webapp/wcs/stores/servlet/TCPGetWishListForUsersView?tcpCallBack=jQuery1113014590106982485151_1473881708524&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&sortBy=&_=1473881708525", 
		"Method=GET", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Mode=HTML", 
		"EncType=application/x-www-form-urlencoded", 
		LAST);

	lr_end_sub_transaction("T25_Common_S01_TCPGetWishListForUsersView", LR_AUTO);
	
}
void TCPIShippingView()
{
	lr_think_time ( LINK_TT ) ;
	
	lr_start_transaction("T27_TCPIShippingView");
	
	web_custom_request("TCPIShippingView",
		"URL=https://{host}/shop/TCPIShippingView?langId=-1&storeId={storeId}&catalogId={catalogId}",
		"Method=GET",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		"EncType=application/x-www-form-urlencoded",
		LAST);
		
	lr_end_transaction("T27_TCPIShippingView", LR_AUTO);
	
	TCPGetWishListForUsersViewBrowse();
	
}

